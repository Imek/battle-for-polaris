BATTLE FOR POLARIS (DEMO 0.22 ALPHA)
====================================

Introduction
============
Battle for Polaris is a space combat game that allows the player to fly
individual ships and command fleets. The objective is to capture linked 
sectors in the galaxy map, competing against and fighting one or more AI
opponents. It's currently in a very rudimentary state, and in particular
the strategy side of the gameplay (and how it melds with the combat)
is due a re-work.

The game is made up of scenarios, including tutorial and sample missions.
However, the main feature is the skirmish mode with multiple available
victory conditions.

Currently an example skirmish is available for each example map
(small/medium/large). Before attempting them it is recommended that you play
through the tutorial scenarios. There is also a control reference below.


Development Version Info
========================

The development version is currently put together for a MinGW/CodeBlocks setup,
and comes bundled with the required dependencies. Latest MinGW is needed; the
following guide is most helpful:

http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Setting+Up+An+Application+-+CodeBlocks

It should be possible to set it up with Visual Studio using the OGRE guides,
however. I'm currently planning to convert the project to use CMake, so that it
can be set up independent of development environment or platform.

CodeBlocks is an Open Source C++ IDE freely-available from
http://www.codeblocks.org/. The BfP.workspace file in the root project folder
is the main workspace file for the IDE. 


Controls
========

Mouse: Common
-------------
Motion:                 Move cursor (turn ship in chase mode)
Fire Selected:          Left button
Fire All:               Right button

Mouse Rotate:           Middle button
Zoom View:              Mouse wheel

Switch Mode:            Button 4 (if you have it)


Keyboard: Common
----------------
Switch Camera Mode:     R (Holding SHIFT reverses)

Zoom In:                T
Zoom Out:               G

Pause Game:             P


Keyboard: Chase mode
--------------------
Accelerate:             W
Decelerate:             S
Strafe Left:            A
Strafe Right:           D
Strafe Up:              SPACE
Strafe Down:            CTRL
Brake:                  Z

Roll Left:              Q
Roll Right:             E

Rotate Camera:          SHIFT

SwitchTarget Mode:      Tab (Holding SHIFT reverses)
Next Target:            F (Holding SHIFT reverses)
Next Enemy:             C (Holding SHIFT reverses)
Closest Enemy:          X
Deselect Target:        V

Switch Weapons:         \ (Holding SHIFT reverses)


Keyboard: Tactical & Map modes
------------------------------

Switch Stance:          B

Pan View Up:            W
Pan View Down:          S
Pan View Left:          A
Pan View Right:         D


Config System
=============

You can achieve various things by messing around with the configuration files.
For example, you can alter the example skirmish scenarios to be harder 
(they're pretty easy right now). You can also change the keyboard controls or
add victory conditions in the general config. It may also be of interest to
read the overview of the trigger system in the mission specification example.
The following files contain documentation on what each value does:

ai.cfg                  - AI system
physics.cfg             - Physics engine
polaris.cfg             - General game config
tutorials.cfg           - Tutorial messages
ships.cfg               - Ship types
weapons.cfg             - Weapon types
bds_demo.cfg            - Configuration of static objects that can be put in sectors
map_small.cfg           - Layout, configs and links for sectors in a game world
msn_pilot_training.cfg  - Mission specifications for the tutorial scenario
scn_pilot_training.cfg  - An example scenario that uses the small map and tutorial missions


Changelog
=========

0.22a
--
=Mostly a refactor and restructure release.
-Updated to newer versions of OGRE and CEGUI dependency libraries
-Much better aim prediction method now used.
-Project restructed; in particular, dependencies are now bundled properly.
-Some general refactor of the code structure
-Some experimental AI code (WIP, not currently used)

0.21b
--
=Some ahead-of-schedule AI tweaks and additions.
-Low-level AI tweaks and additions, including selective dodging (ships now don't try to dodge fast projectiles).
-High-level AI tweaks and additions, including random selection of actions with the same value.
-Changed explosions so that they retain the momentum of the destroyed entity.

=Bugfixes
-Fixed some major crash bugs.
-Fixed problem where ships could continue firing even when not on target.
-Fixed some other minor glitches and gameplay/logic errors.

0.2a
--
=Many added/changed features.
-Added mission system (see documented cfg files) and new tutorials.
-Changed group purchase/deployment system to use ship type and count rather than group type.
-Added ships: fighter drone and probe.
-Homing missiles added.
-Enhanced the weapon system with grouped weapons.
-Enhanced the group stance system (ships can now ignore enemies if given a move order and not set to aggressive)
-Added a quit confirmation dialogue that returns to the menu, rather than ESC just ending the program.
-Can now select ship groups by clicking on ships themselves in the tactical view.

=Many bugs fixed too.
-Fixed many crash bugs.
-Fixed glitch in which the wrong sector would be displayed in chase mode after warping.
-A lot of code tidy-up and restructuring of the design to be cleaner and more logical (this work is ongoing)

0.11a
-----
=Bugfix release.
-Fixed bug wherein switching the target mode in certain situations would cause a crash.
-Fixed bug wherein losing the last player ship could cause a crash.
-Fixed bug wherein faction death wouldn't register on the final ship being lost.
-Turning should be smoother, and should no longer get stuck in a loop.

0.1a
--
=Initial alpha release.


Author & Licence
================

Name: Joe Forster
E-mail: me@joeforster.com
Website: http://joeforster.com/
Licence: See accompanying file LICENCE.txt
