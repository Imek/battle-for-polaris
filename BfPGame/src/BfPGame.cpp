#include <BfPGame.h>

#include <Util/Logging.h>

template<> BfPGame* JFUtil::Singleton<BfPGame>::s_instance = NULL;

BfPGame::BfPGame()
: m_pState(NULL)
, m_pConfigs(NULL)
, m_pInput(NULL)
, m_pWorld(NULL)
, m_pPlayer(NULL)
, m_pPlayerEvaluator(NULL)
, m_pPlayerLogicEngine(NULL)
, m_pAI(NULL)
, m_pPhysics(NULL)
, m_bShutDown(false)
, m_pOgre(NULL)
, m_pMapScene(NULL)
, m_pPrevSectorScene(NULL)
{

}

BfPGame::~BfPGame()
{
	delete m_pOgre;
	delete m_pInput;
	delete m_pWorld;
	delete m_pConfigs;
	delete m_pState;
	delete m_pAI;
	delete m_pPhysics;

}

void BfPGame::Go()
{
	// Test call to debug logger
	Logging::Log(Logging::debug, "BfPGame", "Go called, and about to create BfPConfig!");

	// Load the configs (managed as a singleton)
	m_pConfigs = new BfPConfig();
// Validation is done in the constructor now..
//	if (!m_pConfigs->Validate())
//		throw JFUtil::Exception("BfPGame", "Go", "Invalid configuration.");

//	m_bShutDown = false;
//	m_bQuitToMenu = false;

	// OgreFramework is managed as a singleton, but we keep a pointer to it
	m_pOgre = new OgreFramework();
	m_pOgre->initOgre("Battle for Polaris");
	m_pInput = new BfPInput(m_pOgre);
	assert(GetMode() == GVM_SECTOR);

	Run();

}

bool BfPGame::Update(int i)
{
	//cout << "BEGIN GAME UPDATE" << endl;

	const GeneralConfig* pConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	unsigned iPeriod = pConf->GetControlUpdatePeriod();

	// First, check if the input wants to quit the game.
	if (m_pState && m_pInput->GetGUIMode() == GM_GAME && m_pInput->QuittingToMenu())
	{
		DestroyGame(); // This destroys the state and scenes and puts us back in the menu.
	}

	// Do game update if the GUI is showing the game and not paused
	bool bShowingGame = m_pInput->GetGUIMode() == GM_GAME && !m_pInput->Paused();
	if (bShowingGame)
	{
		if (!m_pState) InitGame();

		//cout << "World update.." << endl;
		m_pWorld->Update(iPeriod); // Updates the state too
		//cout << "AI Update.." << endl;
		m_pAI->Update(iPeriod);
		//unsigned long iStart = m_pOgre->GetTimer()->getMicrosecondsCPU();
		//cout << "Physics update.." << endl;
		m_pPhysics->Update(iPeriod);
		//out << "physics update took: " << m_pOgre->GetTimer()->getMicrosecondsCPU() - iStart << endl;
	}
	//cout << "Input update.." << endl;
	// We always do input update.
	UpdateInput(iPeriod);

	// We need to update scenes after input, as some aspects make use of the camera position.
	if (bShowingGame)
	{
		//cout << "Scenes update.." << endl;
		UpdateScenes(iPeriod);
	}

	//cout << "Framework update.." << endl;
	// We always update OGRE.
	m_pOgre->updateOgre(iPeriod);

	//cout << "Update done." << endl;
	return !m_pOgre->GetRenderWindow()->isClosed();
}

/**
 * BfPGame private functions
 */

void BfPGame::DestroyGame()
{
	m_pOgre->GetLog()->logMessage("Destroying game state...");
	// Should've been checked by the caller
	assert(m_pState);
	assert(m_pInput->GetGUIMode() == GM_GAME);

	delete m_pPhysics;
	m_pPhysics = NULL;
	delete m_pPlayerEvaluator;
	m_pPlayerEvaluator = NULL;
	delete m_pAI;
	m_pAI = NULL;
	delete m_pPlayerLogicEngine;
	m_pPlayerLogicEngine = NULL;
	delete m_pWorld; // World destructor destroys the game state (m_pState) and all faction controllers too
	m_pWorld = NULL;
	m_pPlayer = NULL;
	m_pState = NULL;

	m_pOgre->GetLog()->logMessage("... Done destroying game state.");

	m_pOgre->GetLog()->logMessage("Resetting input system...");

	m_pInput->Reset();

	m_pOgre->GetLog()->logMessage("... Done resetting input system.");

	m_pOgre->GetLog()->logMessage("Unloading world...");

	DestroyScenes();

	m_pOgre->GetLog()->logMessage("... Finished unloading world.");
}

void BfPGame::InitGame()
{
	m_pOgre->GetLog()->logMessage("Constructing initial game state...");

	// In the future, loading from save files will be done here.
	assert(!m_pState);
	m_pState = new BfPState(); // Initialises from the config (currently-selected scenario)
	assert(!m_pPhysics);
	m_pPhysics = new GamePhysics(m_pState);

	// Initialise the player faction.
	assert(!m_pPlayer);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
		assert(pFaction);
		const String& rController = pFaction->GetController();
		if (rController == "Player")
		{
			assert(!m_pPlayer); // Only one player! (should've been checked in the config)
			m_pPlayer = new FactionController(m_pState, *it);
			// For now just use a basic test evaluator for our player faction's group AI.
			// TODO: Rearrange the whole AI/FactionController system so that this can all be done in one place
			m_pPlayerEvaluator = AIEvaluatorFactory::MakeEvaluator(
				pAIConf->GetTestEvaluator(), pAIConf->GetTestInitialValues());
			assert(m_pPlayerEvaluator);
			m_pPlayer->SetEvaluator(m_pPlayerEvaluator);
			m_pPlayer->DeployStartingGroups();
		}
	}
	assert(m_pPlayer); // One player! (should've been checked in the config)

	m_pInput->SetPlayer(m_pPlayer); // Remains NULL if no player (we still do GUI stuff..)

	// Set up the AI system that sets up and controls the other players' FactionControllers
	assert(!m_pAI);
	m_pAI = new GameAI(m_pState);

	assert(!m_pPlayerLogicEngine);
	m_pPlayerLogicEngine = new AITestLogicEngine(m_pPlayer->GetObservedState());

	// Set up the WorldController that updates the world and the faction controllers
	assert(!m_pWorld);
	m_pWorld = new WorldController(m_pState);
	m_pWorld->AddFactionController(m_pPlayer->GetFactionName(), m_pPlayer);
	AIPlayerList& rAIPlayers = m_pAI->GetPlayers();
	for (AIPlayerList::iterator it = rAIPlayers.begin(); it != rAIPlayers.end(); ++it)
	{
		const String& rPlayerName = it->first;
		AIPlayer* pPlayer = it->second;
		FactionController* pController = pPlayer->GetController();
		m_pWorld->AddFactionController(rPlayerName, pController);
	}

	m_pOgre->GetLog()->logMessage("... Done constructing initial game state.");

	m_pOgre->GetLog()->logMessage("Initialising scenes...");

	// Set up all the scenes and attach the initial sector.
	CreateScenes();
	// A hacky way of getting it to pre-load the skybox for the map
	// (TODO: Is this even needed any more now that we have resource management?)

//	m_pMapScene->Attach();
//	m_pMapScene->Detach();


	BfPSceneSector* pScene = m_mSectorScenes[ GetActiveSector() ];
	m_pPrevSectorScene = pScene;
	pScene->Attach();

	m_pOgre->GetLog()->logMessage("... Scenes initialised.");

}

void BfPGame::Run()
{
	const GeneralConfig* pConf = (const GeneralConfig*)
		m_pConfigs->GetConfig("General");
	unsigned iPeriod = pConf->GetControlUpdatePeriod();
	Ogre::RenderWindow* pWindow = m_pOgre->GetRenderWindow();

	m_pOgre->GetLog()->logMessage("Begin main loop.");

	m_oTimerManager.AddTimer(iPeriod, &BfPGame::Update, this, 0, true, true);
	m_pOgre->GetOgreRoot()->addFrameListener(&m_oTimerManager);

	while(!m_pInput->ShuttingDown() && !m_pOgre->isOgreToBeShutDown() && !m_bShutDown)
	{
		if(pWindow->isClosed())
			m_bShutDown = true;


#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		Ogre::WindowEventUtilities::messagePump();
#endif
		if(pWindow->isActive())
		{
			m_pOgre->GetOgreRoot()->renderOneFrame();
		}
		else
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			Sleep(1000);
#else
			sleep(1);
#endif
		}
	}

	OgreFramework::GetInstancePtr()->GetLog()->logMessage("Main loop finished.");
	OgreFramework::GetInstancePtr()->GetLog()->logMessage("Shutdown OGRE...");
}

const String& BfPGame::GetActiveSector()
{
	// Get the player's sector if chase cam. Otherwise, get selected sector.
	// Generally shouldn't be in chase mode with no ships (except maybe for a single update cycle after dying)
	if (m_pInput->GetCameraMode() == CM_CHASE && m_pPlayer->HasShips())
		return m_pPlayer->GetFlagShip()->GetSector()->GetSectorType()->GetName();
	else
	{
		return m_pPlayer->GetSelectedSector();
	}
}

void BfPGame::CreateScenes()
{
	assert(m_pPlayer);
	assert(m_mSectorScenes.empty());
	assert(!m_pMapScene);

	// Create all the scenes (one for each sector and one for the world map)
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	cout << "Loading sectors..." << endl;

	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		BfPSceneSector* pSector = new BfPSceneSector(*it, m_pPlayer);
		m_mSectorScenes[*it] = pSector;
		pSector->CreateScene();

	}

	cout << "... Done loading sectors." << endl;


	cout << "Loading map..." << endl;
	m_pMapScene = new BfPSceneMap(m_pPlayer);
	m_pMapScene->CreateScene();
	cout << "... Done loading map." << endl;
}

void BfPGame::DestroyScenes()
{
	assert(!m_mSectorScenes.empty());
	assert(m_pMapScene);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	cout << "Destroying scenes..." << endl;

	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		assert(m_mSectorScenes.find(*it) != m_mSectorScenes.end());
		BfPSceneSector* pSector = m_mSectorScenes[*it];
		delete pSector;
	}
	m_mSectorScenes.clear();

	delete m_pMapScene;
	m_pMapScene = NULL;
	cout << "... Done destroying scenes." << endl;
}


void BfPGame::UpdateScenes(unsigned long iTime)
{
	assert(m_pMapScene && !m_mSectorScenes.empty());
	GAME_VIEW_MODE iMode = GetMode();

	// TODO: We update all scenes now so that entities are removed/added properly (or else we get duplicates) - go through scene update stuff and see if we can optimise it?
	if (iMode == GVM_SECTOR)
	{
		for (SectorScenes::iterator it = m_mSectorScenes.begin();
			 it != m_mSectorScenes.end(); ++it)
		{
			GameScene* pScene = it->second;
			pScene->UpdateScene(iTime);
		}
	}
	else
	{
		assert(iMode == GVM_MAP);
		m_pMapScene->UpdateScene(iTime);
	}

}

void BfPGame::UpdateInput(unsigned long iTime)
{
	bool bGameMode = m_pInput->GetGUIMode() == GM_GAME && !m_pInput->Paused();
	// These will all be reassigned if used
	GAME_VIEW_MODE iPrevMode = GetMode();
	GAME_VIEW_MODE iNewMode = iPrevMode;
	CAMERA_MODE iNewCamMode = GetCamMode();
	//SectorScenes::iterator it;
	BfPSceneSector* pNewSector = NULL;

//	if (bGameMode)
//	{
//		assert(m_pPrevSectorScene);
//		// Update input. If the mode changed, detach the old node and attach the new one
//		SectorScenes::iterator it = m_mSectorScenes.find( GetActiveSector() );
//		assert(it != m_mSectorScenes.end());
//	}

	m_pInput->Update(iTime);

	if (bGameMode)
	{
		SectorScenes::iterator it = m_mSectorScenes.find( GetActiveSector() );
		assert(it != m_mSectorScenes.end());
		 pNewSector = it->second;
		iNewCamMode = GetCamMode();
		iNewMode = GetMode();

		assert(iPrevMode != GVM_LAST && iNewMode != GVM_LAST &&
			   iNewCamMode != CM_LAST);
	}

	// Attach/detach the appropriate nodes if the mode has changed and/or the sector has changed.
	if (bGameMode && (iPrevMode != iNewMode || m_pPrevSectorScene != pNewSector))
	{
		// Set it as just switched if we're viewing sectors (resets the position of the map, mouse pointer..)
		if (iNewMode == GVM_SECTOR) m_pInput->SetJustSwitched();

		// Attach/Detach scenes
		if (iPrevMode == GVM_SECTOR)
			m_pPrevSectorScene->Detach();
		else if (iPrevMode == GVM_MAP)
			m_pMapScene->Detach();

		if (iNewMode == GVM_SECTOR)
			pNewSector->Attach();
		else if (iNewMode == GVM_MAP)
			m_pMapScene->Attach();
	}

	// Set the "flying flagship" flag if our camera mode is chase.
	if (bGameMode)
	{
		pNewSector->SetFlyingFlagship( iNewCamMode == CM_CHASE );
		m_pMapScene->SetAbstract( iNewCamMode == CM_LOGICENGINE );
		m_pPrevSectorScene = pNewSector;
	}

}



