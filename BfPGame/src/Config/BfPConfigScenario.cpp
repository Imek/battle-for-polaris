#include <Config/BfPConfigScenario.h>


/**
 * FactionType code
 */

StartingGroup::StartingGroup(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	String* pSector = pConfig->GetString(GetName() + "Sector");
	m_strSector = pSector ? *pSector : "Alpha";
	delete pSector;

	String* pShipType = pConfig->GetString(GetName() + "ShipType");
	m_strShipType = pShipType ? *pShipType : "Fighter";
	delete pShipType;

	unsigned* pShipCount = pConfig->GetValue<unsigned>(GetName() + "ShipCount");
	m_iShipCount = pShipCount ? *pShipCount : 1;
	delete pShipCount;

	// TODO: More validation here and elsewhere in the config constructors.
	if (m_iShipCount < 1)
		throw JFUtil::Exception("StartingGroup", "StartingGroup",
			"Invalid ship count given for starting group", strName);
}


/**
 * FactionType code
 */

FactionType::FactionType(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	String* pUnselectedColour = pConfig->GetString(GetName() + "UnselectedColour");
	m_vecUnselectedColour = pUnselectedColour ? JFUtil::VectorFromString(*pUnselectedColour) : Vector3::ZERO;
	delete pUnselectedColour;

	String* pSelectedColour = pConfig->GetString(GetName() + "SelectedColour");
	m_vecSelectedColour = pSelectedColour ? JFUtil::VectorFromString(*pSelectedColour) : Vector3::ZERO;
	delete pSelectedColour;

	String* pController = pConfig->GetString(GetName() + "Controller");
	m_strController = pController ? *pController : "DefaultController"; // Generally invalid (throw an error if not given)
	delete pController;

	m_vStartingSectors = pConfig->GetStrings(GetName() + "StartingSector");

	long* pStartingRPs = pConfig->GetValue<long>(GetName() + "StartingRPs");
	m_iStartingRPs = pStartingRPs ? *pStartingRPs : 0;
	delete pStartingRPs;

	// Starting groups
	// TODO: It's probably not necessary to check for no starting groups, as some can be deployed
	// (and we may want a scenario where the AI starts with no groups) - remove and make sure it works.
	m_vGroupNames = pConfig->GetStrings(GetName() + "StartingGroup");
	if (m_vGroupNames.empty())
		throw JFUtil::Exception("FactionType", "FactionType",
			"No starting groups given for faction", strName);
	for (StringVector::const_iterator it = m_vGroupNames.begin();
		 it != m_vGroupNames.end(); ++it)
	{
		if (m_mGroups.find(*it) != m_mGroups.end())
			throw JFUtil::Exception("FactionType", "FactionType",
				"Duplicate starting group name given for faction", strName);
		m_mGroups[*it] = new StartingGroup(*it, pConfig);
	}
}

const StartingGroup& FactionType::GetStartingGroup(const String& rName) const
{
	map<String, StartingGroup*>::const_iterator it = m_mGroups.find(rName);
	if (it == m_mGroups.end())
		throw JFUtil::Exception("FactionType", "GetStartingGroup",
			"Group not found with name", rName);
	return *it->second;
}

//bool FactionType::Validate() const
//{
//	// "NEUTRAL" is reserved for the AI.
//	if (GetName() == "NEUTRAL") return false;
//	if (!ObjectType::Validate()) return false;
//	if (m_vStartingShips.empty()) return false;
//	// We could conceivably start with no sectors (as long as we have a ship) or negative RPs.
//	return true;
//}


/**
 * ScenarioConfig code
 */

ScenarioConfig::ScenarioConfig(String strPath)
: BaseConfig(strPath)
, m_vFactionNames(m_oConfig.getMultiSetting("Faction"))
//, m_vBodyNames(m_oConfig.getMultiSetting("Body"))
{
	bool* pWipeOutVictory = GetValue<bool>("WipeOut Victory");
	m_bWipeOutVictory = pWipeOutVictory ? *pWipeOutVictory : false;
	delete pWipeOutVictory;

	bool* pAssassinationVictory = GetValue<bool>("Assassination Victory");
	m_bAssassinationVictory = pAssassinationVictory ? *pAssassinationVictory : false;
	delete pAssassinationVictory;

	bool* pHomeInvasionVictory = GetValue<bool>("Home Invasion Victory");
	m_bHomeInvasionVictory = pHomeInvasionVictory ? *pHomeInvasionVictory : false;
	delete pHomeInvasionVictory;

	String* pMapConf = GetString("MapConfigFile");
	// TODO: Finish adding validation lines like this to all configs
	if (!pMapConf) throw
		JFUtil::Exception("ScenarioConfig", "ScenarioConfig",
						  "Missing MapConfigFile entry in", strPath);
	m_pMapConfig = new MapConfig(*pMapConf);
	delete pMapConf;

	// Missions conf is allowed to be null, in which case we just don't have any triggers.
	String* pMissionsConf = GetString("MissionsConfigFile");
	m_pMissionsConfig = pMissionsConf ?
		new MissionsConfig(*pMissionsConf) : NULL;
	delete pMissionsConf;

	// Now just read in the faction settings.
	bool bHasPlayer = false;
	for (StringVector::const_iterator it = m_vFactionNames.begin();
		 it != m_vFactionNames.end(); ++it)
	{
		FactionType* pFaction  = new FactionType(*it, this);
		if (pFaction->GetController() == "Player")
		{
			if (bHasPlayer) throw JFUtil::Exception("ScenarioConfig", "ScenarioConfig",
				"More than one player-controlled faction found in scenario file", strPath);
			bHasPlayer = true;
		}
		m_mFactions[*it] = pFaction;
	}
	if (!bHasPlayer) throw JFUtil::Exception("ScenarioConfig", "ScenarioConfig",
		"No player-controlled faction found in scenario file", strPath);

	assert(m_pMapConfig);
}

ScenarioConfig::~ScenarioConfig()
{
	delete m_pMapConfig;

	delete m_pMissionsConfig;

	for (std::map<String, FactionType*>::iterator it = m_mFactions.begin();
		 it != m_mFactions.end(); ++it)
	{
		delete it->second;
	}
	m_mFactions.clear();
}

const FactionType* ScenarioConfig::GetFactionType(String strName) const
{
	std::map<String, FactionType*>::const_iterator it =
		m_mFactions.find(strName);
	return (it == m_mFactions.end() ? NULL : it->second);
}

//bool ScenarioConfig::Validate() const
//{
//	// Validate individual settings
//	if (m_fInitialMapScale < 0) return false;
//	if (m_strMapStarTexture.empty()) return false;
//	if (m_fMapTextSize <= 0) return false;
//	if (m_fSpawnDeviationStep <= 0) return false;
//	if (m_fMaxSpawnDeviation <= 0) return false;
//	if (m_fMaxWarpDist < 0) return false;
//
//
//	// We need to validate each individual thing, but also links - e.g. references to bodies in sectors.
//	for (map<String, FactionType*>::const_iterator it = m_mFactions.begin();
//		 it != m_mFactions.end(); ++it)
//	{
//		const FactionType* pFaction = it->second;
//		if (!pFaction->Validate()) return false;
//		// We need to validate the starting starting sectors;
//		// starting ships refer to ShipTypes, and are validated in BfPConfig's Validate() function.
//		const StringVector& rSectors = pFaction->GetStartingSectors();
//		for (StringVector::const_iterator it = rSectors.begin(); it != rSectors.end(); ++it)
//		{
//			if (!GetSectorType(*it)) return false;
//		}
//	}
//
//	for (map<String, SectorType*>::const_iterator it = m_mSectors.begin();
//		 it != m_mSectors.end(); ++it)
//	{
//		const SectorType* pSector = it->second;
//		if (!pSector->Validate()) return false;
//		// Links must be to sectors that exist.
//		const StringVector& rLinks = pSector->GetLinkedSectors();
//		for (StringVector::const_iterator it = rLinks.begin(); it != rLinks.end(); ++it)
//		{
//			if (!GetSectorType(*it)) return false;
//		}
//		// Validate the types of BodyInstances..
//		const BodyVector& rBodies = pSector->GetBodies();
//		for (BodyVector::const_iterator it = rBodies.begin();
//			 it != rBodies.end(); ++it)
//		{
//			const BodyInstance* pBody = *it;
//			if (!GetBodyType(pBody->GetType())) return false;
//		}
//	}
//
//	// Validate each mission
//	for (map<String,MissionConfig*>::const_iterator it = m_mMissions.begin();
//		 it != m_mMissions.end(); ++it)
//	{
//		const MissionConfig* pMission = it->second;
//		if (!pMission->Validate()) return false;
//	}
//
////	for (map<String, BodyType*>::const_iterator it = m_mBodies.begin();
////		 it != m_mBodies.end(); ++it)
////	{
////		const BodyType* pBody = it->second;
////		if (!pBody->Validate()) return false;
////	}
//	return true;
//}
