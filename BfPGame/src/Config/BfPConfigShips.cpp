#include <Config/BfPConfigShips.h>

/**
 * ShipType code
 */

ShipType::ShipType(String strName, const BaseConfig* pConfig)
: EntityType(strName, pConfig)
{
	unsigned* pRPCost = pConfig->GetValue<unsigned>(GetName() + "RPCost");
	m_iRPCost = pRPCost ? *pRPCost : 500;
	delete pRPCost;

	String* pModel = pConfig->GetString(GetName() + "Model");
	m_strModel = pModel ? *pModel : "default.mesh"; // A default doesn't exist for now
	delete pModel;

	Real* pScale = pConfig->GetValue<Real>(GetName() + "Scale");
	m_fScale = pScale ? *pScale : 1;
	delete pScale;

	String* pWarpParticles = pConfig->GetString(GetName() + "WarpParticles");
	m_strWarpParticles = pWarpParticles ? *pWarpParticles : "default";
	delete pWarpParticles;

	unsigned* pMaxSP = pConfig->GetValue<unsigned>(GetName() + "MaxSP");
	m_iMaxSP = pMaxSP ? *pMaxSP : 1; // 1hp default
	delete pMaxSP;

	unsigned* pMaxEP = pConfig->GetValue<unsigned>(GetName() + "MaxEP");
	m_iMaxEP = pMaxEP ? *pMaxEP : 1; // 1hp default
	delete pMaxEP;

	Real* pSPRate = pConfig->GetValue<Real>(GetName() + "SPRate");
	m_fSPRate = pSPRate ? *pSPRate : 0.001;
	delete pSPRate;

	Real* pEPRate = pConfig->GetValue<Real>(GetName() + "EPRate");
	m_fEPRate = pEPRate ? *pEPRate : 0.001;
	delete pEPRate;

	Real* pBrakeThrust = pConfig->GetValue<Real>(GetName() + "BrakeThrust");
	m_fBrakeThrust = pBrakeThrust ? *pBrakeThrust : 0.00002;
	delete pBrakeThrust;

	Real* pStrafeThrust = pConfig->GetValue<Real>(GetName() + "StrafeThrust");
	m_fStrafeThrust = pStrafeThrust ? *pStrafeThrust : 0.0001;
	delete pStrafeThrust;

	Real* pThrustRate = pConfig->GetValue<Real>(GetName() + "ThrustRate");
	m_fThrustRate = pThrustRate ? *pThrustRate : 00.0000001;
	delete pThrustRate;


	Real* pYawRate = pConfig->GetValue<Real>(GetName() + "YawRate");
	m_fYawRate = pYawRate ? *pYawRate : 0.0000001;
	delete pYawRate;

	Real* pPitchRate = pConfig->GetValue<Real>(GetName() + "PitchRate");
	m_fPitchRate = pPitchRate ? *pPitchRate : 0.0000001;
	delete pPitchRate;

	Real* pRollRate = pConfig->GetValue<Real>(GetName() + "RollRate");
	m_fRollRate = pRollRate ? *pRollRate : 0.0000001;
	delete pRollRate;

	Real* pMaxYaw = pConfig->GetValue<Real>(GetName() + "MaxYaw");
	m_fMaxYaw = pMaxYaw ? *pMaxYaw : 0.001;
	delete pMaxYaw;

	Real* pMaxPitch = pConfig->GetValue<Real>(GetName() + "MaxPitch");
	m_fMaxPitch = pMaxPitch ? *pMaxPitch : 0.001;
	delete pMaxPitch;

	Real* pMaxRoll = pConfig->GetValue<Real>(GetName() + "MaxRoll");
	m_fMaxRoll = pMaxRoll ? *pMaxRoll : 0.001;
	delete pMaxRoll;


	m_vWeapons = pConfig->GetStrings(GetName() + "Weapon");

	StringVector vOffsets = pConfig->GetStrings(GetName() + "WeaponOffset");
	for (StringVector::const_iterator it = vOffsets.begin(); it != vOffsets.end(); ++it)
	{
		m_vWeapOffsets.push_back( JFUtil::VectorFromString(*it) );
	}

	unsigned long* pWarpDuration = pConfig->GetValue<unsigned long>(strName + "WarpDuration");
	m_iWarpDuration = pWarpDuration ? *pWarpDuration : 6000;
	delete pWarpDuration;

	unsigned long* pWarpDelay = pConfig->GetValue<unsigned long>(strName + "WarpDelay");
	m_iWarpDelay = pWarpDelay ? *pWarpDelay : 5000;
	delete pWarpDelay;

	// AI/behaviour stuff

	String* pInitialStance = pConfig->GetString(GetName() + "InitialStance");
	m_strInitialStance = pInitialStance ? *pInitialStance : "Aggressive";
	delete pInitialStance;

	Real* pStrafeRunMinDist = pConfig->GetValue<Real>(GetName() + "StrafeRunMinDist");
	m_fStrafeRunMinDist = pStrafeRunMinDist ? *pStrafeRunMinDist : 10;
	delete pStrafeRunMinDist;

	Real* pStrafeRunMaxDist = pConfig->GetValue<Real>(GetName() + "StrafeRunMaxDist");
	m_fStrafeRunMaxDist = pStrafeRunMaxDist ? *pStrafeRunMaxDist : 50;
	delete pStrafeRunMaxDist;

	StringVector vDodgeWeaponTypes = pConfig->GetStrings(GetName() + "DodgeWeaponType");
	for (StringVector::const_iterator it = vDodgeWeaponTypes.begin();
		 it != vDodgeWeaponTypes.end(); ++it)
	{
		m_sDodgeWeaponTypes.insert(*it);
	}

}

const Vector3& ShipType::GetWeapOffset(unsigned iNo) const
{
	assert(iNo < m_vWeapons.size());
	assert(!m_vWeapOffsets.empty());
	if (iNo >= m_vWeapOffsets.size())
		return m_vWeapOffsets.back();
	else
		return m_vWeapOffsets[iNo];
}

bool ShipType::GetDodgeWeaponType(const String& rWeapType) const
{
	return (m_sDodgeWeaponTypes.find(rWeapType) != m_sDodgeWeaponTypes.end());
}


/**
 * ShipsConfig code
 */
ShipsConfig::ShipsConfig(String strPath)
: BaseConfig(strPath)
{
	m_vShips = m_oConfig.getMultiSetting("Ship Type");
	for (StringVector::const_iterator it = m_vShips.begin();
		 it != m_vShips.end(); ++it)
	{
		m_mShips[*it] = new ShipType(*it, this);
	}
}

ShipsConfig::~ShipsConfig()
{
	for (std::map<String, ShipType*>::iterator it = m_mShips.begin();
		 it != m_mShips.end(); ++it)
	{
		delete it->second;
	}
	m_mShips.clear();
}

const ShipType* ShipsConfig::GetShipType(String strName) const
{
	String strWTF = strName; // Why the hell do I have to do this
	std::map<String, ShipType*>::const_iterator it =
		m_mShips.find(strWTF);
	return (it == m_mShips.end() ? NULL : it->second);
}

bool ShipsConfig::ValidShipGroup(const StringVector& rTypes) const
{
	for (StringVector::const_iterator it = rTypes.begin();
		 it != rTypes.end(); ++it)
	{
		if (!GetShipType(*it)) return false;
	}

	return true;
}

//unsigned ShipsConfig::GetGroupCost(const StringVector& rTypes) const
//{
//	unsigned iCost = 0;
//	for (StringVector::const_iterator it = rTypes.begin();
//		 it != rTypes.end(); ++it)
//	{
//		const ShipType* pType = GetShipType(*it);
//		assert(pType);
//		iCost += pType->GetRPCost();
//	}
//	return iCost;
//}

//bool ShipsConfig::Validate() const
//{
//	for (map<String, ShipType*>::const_iterator it = m_mShips.begin();
//		 it != m_mShips.end(); ++it)
//	{
//		const ShipType* pShip = it->second;
//		if (!pShip->Validate()) return false;
//	}
//	return true;
//}
