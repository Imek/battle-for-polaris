#include <Config/BfPConfigGeneral.h>

/**
 * GeneralConfig code
 */

GeneralConfig::GeneralConfig(String strPath)
: BaseConfig(strPath)
{
	// Gameplay options
	bool* pFriendlyFire = GetValue<bool>("Friendly Fire", "Gameplay Settings");
	m_bFriendlyFire = pFriendlyFire ? *pFriendlyFire : true;
	delete pFriendlyFire;

	Real* pCollisionDmgMult = GetValue<Real>("Collision Damage Per Force");
	m_fCollisionDmgMult = pCollisionDmgMult ? *pCollisionDmgMult : 0;
	delete pCollisionDmgMult;

	unsigned* pMaxGroupSize = GetValue<unsigned>("Max Group Size", "Gameplay Settings");
	m_iMaxGroupSize = pMaxGroupSize ? *pMaxGroupSize : 9;
	delete pMaxGroupSize;

	unsigned* pDeathCamDelay = GetValue<unsigned>("Death Camera Delay", "Gameplay Settings");
	m_iDeathCamDelay = pDeathCamDelay ? *pDeathCamDelay : 2000;
	delete pDeathCamDelay;

	m_vScenarioConfigs = GetStrings("ScenarioName", "Gameplay Settings");
	StringVector vConfigPaths = GetStrings("ScenarioPath", "Gameplay Settings");

	size_t iNoConfigs = m_vScenarioConfigs.size();
	if (iNoConfigs != vConfigPaths.size())
		throw JFUtil::Exception("GeneralConfig", "GeneralConfig", "Mis-matched scenario names and configs");
	if (m_vScenarioConfigs.empty())
		throw JFUtil::Exception("GeneralConfig", "GeneralConfig", "No scenario configs found");

	for (size_t i = 0; i < iNoConfigs; ++i)
	{
		const String& rName = m_vScenarioConfigs[i];
		const String& rPath = vConfigPaths[i];
		m_mScenarioConfigs[rName] = rPath;
	}

	String* pDefaultScenario = GetString("DefaultScenario", "Gameplay Settings");
	if (!pDefaultScenario)
	{
		throw JFUtil::Exception("GeneralConfig", "GeneralConfig", "No default scenario config specified");
	}
	else
	{
		m_strDefaultScenario = *pDefaultScenario;
		delete pDefaultScenario;
		if (m_mScenarioConfigs.find(m_strDefaultScenario) == m_mScenarioConfigs.end())
		{
			throw JFUtil::Exception("GeneralConfig", "GeneralConfig", "Invalid default scenario config specified");
		}
	}

	unsigned* pControlUpdatePeriod = GetValue<unsigned>("Control Update Period", "Gameplay Settings");
	m_iControlUpdatePeriod = pControlUpdatePeriod ? *pControlUpdatePeriod : 25;
	delete pControlUpdatePeriod;

	// GUI Settings (based on the CEGUI layout)
	Real* pMenuPointerSensitivity = GetValue<Real>("Menu Pointer Sensitivity", "GUI Settings");
	m_fMenuPointerSensitivity = pMenuPointerSensitivity ? *pMenuPointerSensitivity : 0.55;
	delete pMenuPointerSensitivity;

	bool* pShowAIDebugInterface = GetValue<bool>("Show AI Debug Interface", "GUI Settings");
	m_bShowAIDebugInterface = pShowAIDebugInterface ? *pShowAIDebugInterface : false;
	delete pShowAIDebugInterface;

	Real* pTacticalSelectMinX = GetValue<Real>("Tactical Select Min X", "GUI Settings");
	m_fTacticalSelectMinX = pTacticalSelectMinX ? *pTacticalSelectMinX : 0.31;
	delete pTacticalSelectMinX;

	Real* pTacticalSelectMaxY = GetValue<Real>("Tactical Select Max Y", "GUI Settings");
	m_fTacticalSelectMaxY = pTacticalSelectMaxY ? *pTacticalSelectMaxY : 0.81;
	delete pTacticalSelectMaxY;

	Real* pTacticalSelectRadiusMult = GetValue<Real>("Tactical Select Radius Mult", "GUI Settings");
	m_fTacticalSelectRadiusMult = pTacticalSelectRadiusMult ? *pTacticalSelectRadiusMult : 5;
	delete pTacticalSelectRadiusMult;

	Real* pEdgePanDistance = GetValue<Real>("Edge Pan Distance", "GUI Settings");
	m_fEdgePanDistance = pEdgePanDistance ? *pEdgePanDistance : 5;
	delete pEdgePanDistance;

	unsigned* pMaxMessages = GetValue<unsigned>("Max Messages", "GUI Settings");
	m_iMaxMessages = pMaxMessages ? *pMaxMessages : 20;
	delete pMaxMessages;

	// Camera Settings
	bool* pEnableFreeCam = GetValue<bool>("Enable FreeCam", "Camera Settings");
	m_bEnableFreeCam = pEnableFreeCam ? *pEnableFreeCam : true;
	delete pEnableFreeCam;

	Real* pCamSpeed = GetValue<Real>("Camera Speed", "Camera Settings");
	m_fCamSpeed = pCamSpeed ? *pCamSpeed : 0;
	delete pCamSpeed;

	Real* pCamTurnSpeed = GetValue<Real>("Camera Turn Speed", "Camera Settings");
	m_fCamRotation = pCamTurnSpeed ? *pCamTurnSpeed : 0;
	delete pCamTurnSpeed;

	Real* pChaseCamInitialDist = GetValue<Real>("ChaseCam Initial Dist", "Camera Settings");
	m_fChaseCamInitialDist = pChaseCamInitialDist ? *pChaseCamInitialDist : 5;
	delete pChaseCamInitialDist;

	Real* pChaseCamMinDist = GetValue<Real>("ChaseCam Min Dist", "Camera Settings");
	m_fChaseCamMinDist = pChaseCamMinDist ? *pChaseCamMinDist : 2;
	delete pChaseCamMinDist;

	Real* pChaseCamMaxDist = GetValue<Real>("ChaseCam Max Dist", "Camera Settings");
	m_fChaseCamMaxDist = pChaseCamMaxDist ? *pChaseCamMaxDist : 8;
	delete pChaseCamMaxDist;

	Real* pChaseCamStiffness = GetValue<Real>("ChaseCam Stiffness", "Camera Settings");
	m_fChaseCamStiffness = pChaseCamStiffness ? *pChaseCamStiffness : 0.01;
	delete pChaseCamStiffness;

	Real* pChaseCamDamping = GetValue<Real>("ChaseCam Damping", "Camera Settings");
	m_fChaseCamDamping = pChaseCamDamping ? *pChaseCamDamping : 0.995;
	delete pChaseCamDamping;

	Real* pChaseCamZoomStiffness = GetValue<Real>("ChaseCam Zoom Stiffness", "Camera Settings");
	m_fChaseCamZoomStiffness = pChaseCamZoomStiffness ? *pChaseCamZoomStiffness : 0.01;
	delete pChaseCamZoomStiffness;

	Real* pChaseCamZoomSpeed = GetValue<Real>("ChaseCam Zoom Speed", "Camera Settings");
	m_fChaseCamZoomSpeed = pChaseCamZoomSpeed ? *pChaseCamZoomSpeed : 0.015;
	delete pChaseCamZoomSpeed;

	Real* pChaseCamSensitivity = GetValue<Real>("ChaseCam Pointer Sensitivity", "Camera Settings");
	m_fChaseCamSensitivity = pChaseCamSensitivity ? *pChaseCamSensitivity : 4.5;
	delete pChaseCamSensitivity;

	Real* pChaseCamAccelSensitivity = GetValue<Real>("ChaseCam Accel Sensitivity", "Camera Settings");
	m_fChaseCamAccelSensitivity = pChaseCamAccelSensitivity ? *pChaseCamAccelSensitivity : 0.0005;
	delete pChaseCamAccelSensitivity;

	Real* pChaseCamDecelSensitivity = GetValue<Real>("ChaseCam Decel Sensitivity", "Camera Settings");
	m_fChaseCamDecelSensitivity = pChaseCamDecelSensitivity ? *pChaseCamDecelSensitivity : 0.001;
	delete pChaseCamDecelSensitivity;


	Real* pTacCamInitialDist = GetValue<Real>("TacCam Initial Dist", "Camera Settings");
	m_fTacCamInitialDist = pTacCamInitialDist ? *pTacCamInitialDist : 20;
	delete pTacCamInitialDist;

	Real* pTacCamMinDist = GetValue<Real>("TacCam Min Dist", "Camera Settings");
	m_fTacCamMinDist = pTacCamMinDist ? *pTacCamMinDist : 8;
	delete pTacCamMinDist;

	Real* pTacCamMaxDist = GetValue<Real>("TacCam Max Dist", "Camera Settings");
	m_fTacCamMaxDist = pTacCamMaxDist ? *pTacCamMaxDist : 40;
	delete pTacCamMaxDist;

	Real* pTacCamZoomStiffness = GetValue<Real>("TacCam Zoom Stiffness", "Camera Settings");
	m_fTacCamZoomStiffness = pTacCamZoomStiffness ? *pTacCamZoomStiffness : 0.995;
	delete pTacCamZoomStiffness;

	Real* pTacCamZoomSpeed = GetValue<Real>("TacCam Zoom Speed", "Camera Settings");
	m_fTacCamZoomSpeed = pTacCamZoomSpeed ? *pTacCamZoomSpeed : 0.015;
	delete pTacCamZoomSpeed;

	Real* pTacCamPanSpeed = GetValue<Real>("TacCam Pan Speed", "Camera Settings");
	m_fTacCamPanSpeed = pTacCamPanSpeed ? *pTacCamPanSpeed : 0.03;
	delete pTacCamPanSpeed;

	Real* pTacCamSensitivity = GetValue<Real>("TacCam Pointer Sensitivity", "Camera Settings");
	m_fTacCamSensitivity = pTacCamSensitivity ? *pTacCamSensitivity : 4.5;
	delete pTacCamSensitivity;

	Real* pMapCamInitialDist = GetValue<Real>("MapCam Initial Dist", "Camera Settings");
	m_fMapCamInitialDist = pMapCamInitialDist ? *pMapCamInitialDist : 20;
	delete pMapCamInitialDist;

	Real* pMapCamMinDist = GetValue<Real>("MapCam Min Dist", "Camera Settings");
	m_fMapCamMinDist = pMapCamMinDist ? *pMapCamMinDist : 8;
	delete pMapCamMinDist;

	Real* pMapCamMaxDist = GetValue<Real>("MapCam Max Dist", "Camera Settings");
	m_fMapCamMaxDist = pMapCamMaxDist ? *pMapCamMaxDist : 40;
	delete pMapCamMaxDist;

	Real* pMapCamZoomStiffness = GetValue<Real>("MapCam Zoom Stiffness", "Camera Settings");
	m_fMapCamZoomStiffness = pMapCamZoomStiffness ? *pMapCamZoomStiffness : 0.995;
	delete pMapCamZoomStiffness;

	Real* pMapCamZoomSpeed = GetValue<Real>("MapCam Zoom Speed", "Camera Settings");
	m_fMapCamZoomSpeed = pMapCamZoomSpeed ? *pMapCamZoomSpeed : 0.015;
	delete pMapCamZoomSpeed;

	Real* pMapCamPanSpeed = GetValue<Real>("MapCam Pan Speed", "Camera Settings");
	m_fMapCamPanSpeed = pMapCamPanSpeed ? *pMapCamPanSpeed : 0.03;
	delete pMapCamPanSpeed;

	Real* pMapCamSensitivity = GetValue<Real>("MapCam Pointer Sensitivity", "Camera Settings");
	m_fMapCamSensitivity = pMapCamSensitivity ? *pMapCamSensitivity : 4.5;
	delete pMapCamSensitivity;


	// Key bindings
	String* pZoomInKey = GetString("ZoomInKey", "Key Bindings");
	m_strZoomInKey = pZoomInKey ? *pZoomInKey : "T";
	delete pZoomInKey;

	String* pZoomOutKey = GetString("ZoomOutKey", "Key Bindings");
	m_strZoomOutKey = pZoomOutKey ? *pZoomOutKey : "G";
	delete pZoomOutKey;

	String* pAccelKey = GetString("AccelKey", "Key Bindings");
	m_strAccelKey = pAccelKey ? *pAccelKey : "W";
	delete pAccelKey;

	String* pDecelKey = GetString("DecelKey", "Key Bindings");
	m_strDecelKey = pDecelKey ? *pDecelKey : "S";
	delete pDecelKey;

	String* pStrafeLKey = GetString("StrafeLKey", "Key Bindings");
	m_strStrafeLKey = pStrafeLKey ? *pStrafeLKey : "A";
	delete pStrafeLKey;

	String* pStrafeRKey = GetString("StrafeRKey", "Key Bindings");
	m_strStrafeRKey = pStrafeRKey ? *pStrafeRKey : "D";
	delete pStrafeRKey;

	String* pStrafeUKey = GetString("StrafeUKey", "Key Bindings");
	m_strStrafeUKey = pStrafeUKey ? *pStrafeUKey : "SPACE";
	delete pStrafeUKey;

	String* pStrafeDKey = GetString("StrafeDKey", "Key Bindings");
	m_strStrafeDKey = pStrafeDKey ? *pStrafeDKey : "CTRL";
	delete pStrafeDKey;

	String* pRollLKey = GetString("RollLKey", "Key Bindings");
	m_strRollLKey = pRollLKey ? *pRollLKey : "Q";
	delete pRollLKey;

	String* pRollRKey = GetString("RollRKey", "Key Bindings");
	m_strRollRKey = pRollRKey ? *pRollRKey : "E";
	delete pRollRKey;

	String* pBrakeKey = GetString("BrakeKey", "Key Bindings");
	m_strBrakeKey = pBrakeKey ? *pBrakeKey : "Z";
	delete pBrakeKey;

	String* pWarpKey = GetString("WarpKey", "Key Bindings");
	m_strWarpKey = pWarpKey ? *pWarpKey : "Enter";
	delete pWarpKey;

	String* pCamShiftKey = GetString("CamShiftKey", "Key Bindings");
	m_strCamShiftKey = pCamShiftKey ? *pCamShiftKey : "SHIFT";
	delete pCamShiftKey;

	String* pSwitchModeKey = GetString("SwitchModeKey", "Key Bindings");
	m_strSwitchModeKey = pSwitchModeKey ? *pSwitchModeKey : "C";
	delete pSwitchModeKey;


	String* pToggleTargetModeKey = GetString("SwitchTargetModeKey", "Key Bindings");
	m_strToggleTargetModeKey = pToggleTargetModeKey ? *pToggleTargetModeKey : "Tab";
	delete pToggleTargetModeKey;

	String* pNextTargetKey = GetString("SwitchTargetKey", "Key Bindings");
	m_strNextTargetKey = pNextTargetKey ? *pNextTargetKey : "F";
	delete pNextTargetKey;

	String* pUnselectTargetKey = GetString("UnselectTargetKey", "Key Bindings");
	m_strUnselectTargetKey = pUnselectTargetKey ? *pUnselectTargetKey : "G";
	delete pUnselectTargetKey;

	String* pClosestEnemyKey = GetString("ClosestEnemyKey", "Key Bindings");
	m_strClosestEnemyKey = pClosestEnemyKey ? *pClosestEnemyKey : "X";
	delete pClosestEnemyKey;

	String* pNextEnemyKey = GetString("SwitchEnemyKey", "Key Bindings");
	m_strNextEnemyKey = pNextEnemyKey ? *pNextEnemyKey : "C";
	delete pNextEnemyKey;

	String* pNextWeapGroupKey = GetString("SwitchWeapGroupKey", "Key Bindings");
	m_strNextWeapGroupKey = pNextWeapGroupKey ? *pNextWeapGroupKey : "T";
	delete pNextWeapGroupKey;

	String* pToggleGroupStanceKey = GetString("SwitchGroupStanceKey", "Key Bindings");
	m_strToggleGroupStanceKey = pToggleGroupStanceKey ? *pToggleGroupStanceKey : "V";
	delete pToggleGroupStanceKey;

	String* pPauseKey = GetString("PauseKey", "Key Bindings");
	m_strPauseKey = pPauseKey ? *pPauseKey : "P";
	delete pPauseKey;


	// Mouse button bindings
	unsigned* pFireGroupMButton = GetValue<unsigned>("FireGroupMButton", "Mouse Bindings");
	m_iFireGroupMButton = pFireGroupMButton ? (OIS::MouseButtonID)*pFireGroupMButton : OIS::MB_Left;
	delete pFireGroupMButton;

	unsigned* pFireAllMButton = GetValue<unsigned>("FireAllMButton", "Mouse Bindings");
	m_iFireAllMButton = pFireAllMButton ? (OIS::MouseButtonID)*pFireAllMButton : OIS::MB_Right;
	delete pFireAllMButton;

	unsigned* pCamShiftMButton = GetValue<unsigned>("CamShiftMButton", "Mouse Bindings");
	m_iCamShiftMButton = pCamShiftMButton ? (OIS::MouseButtonID)*pCamShiftMButton : OIS::MB_Middle;
	delete pCamShiftMButton;

	unsigned* pSwitchModeMButton = GetValue<unsigned>("SwitchModeMButton", "Mouse Bindings");
	m_iSwitchModeMButton = pSwitchModeMButton ? (OIS::MouseButtonID)*pSwitchModeMButton : OIS::MB_Button3;
	delete pSwitchModeMButton;
}

const String& GeneralConfig::GetScenarioConfig(const String& rName) const
{
	StringStringMap::const_iterator it =
		m_mScenarioConfigs.find(rName);
	if (it == m_mScenarioConfigs.end())
		throw JFUtil::Exception("GeneralConfig", "GetScenarioConfig",
						"Invalid world config name: " + rName);
	return (it->second);
}

