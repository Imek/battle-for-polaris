#include <Config/BfPConfigAI.h>

/**
 * ShipGroupStance code
 */

ShipGroupStance::ShipGroupStance(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	Real* pAggroRange = pConfig->GetValue<Real>(GetName() + "AggroRange");
	m_fAggroRange = pAggroRange ? *pAggroRange : 250;
	delete pAggroRange;

	Real* pAvoidThreshold = pConfig->GetValue<Real>(GetName() + "AvoidThreshold");
	m_fAvoidThreshold = pAvoidThreshold ? *pAvoidThreshold : 2.5;
	delete pAvoidThreshold;

	Real* pEvadeThreshold = pConfig->GetValue<Real>(GetName() + "EvadeThreshold");
	m_fEvadeThreshold = pEvadeThreshold ? *pEvadeThreshold : 3.0;
	delete pEvadeThreshold;

	bool* pWarpPastEnemies = pConfig->GetValue<bool>(GetName() + "WarpPastEnemies");
	m_bWarpPastEnemies = pWarpPastEnemies ? *pWarpPastEnemies : false;
	delete pWarpPastEnemies;

}


//bool ShipGroupStance::Validate() const
//{
//	if (m_fAggroRange < 0) return false;
//
//	return true;
//}

/**
 * AIPlayerType code
 */

AIPlayerType::AIPlayerType(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	String* pClass = pConfig->GetString(GetName() + "Class");
	m_strClass = pClass ? *pClass : "Basic";
	delete pClass;

	String* pLogicEngine = pConfig->GetString(GetName() + "LogicEngine");
	m_strLogicEngine = pLogicEngine ? *pLogicEngine : "Interm";
	delete pLogicEngine;

	String* pInitialModel = pConfig->GetString(GetName() + "InitialModel");
	m_strInitialModel = pInitialModel ? *pInitialModel : "IntermedA1";
	delete pInitialModel;

	String* pEvaluator = pConfig->GetString(GetName() + "Evaluator");
	m_strEvaluator = pEvaluator ? *pEvaluator : "Preset";
	delete pEvaluator;

	String* pInitialValues = pConfig->GetString(GetName() + "InitialValues");
	m_strInitialValues = pInitialValues ? *pInitialValues : "PresetA1";
	delete pInitialValues;


	m_vManagers = pConfig->GetStrings(GetName() + "Manager");

	m_vObjectives = pConfig->GetStrings(GetName() + "Obj");
	for (StringVector::const_iterator it = m_vObjectives.begin();
		 it != m_vObjectives.end(); ++it)
	{
		Real* pObjProb = pConfig->GetValue<Real>(GetName() + "Initial" + *it);
		if (!pObjProb)
			throw JFUtil::Exception("AIPlayerType", "AIPlayerType",
							"Couldn't find a matching Initial entry for an Obj entry: " + *it);
		m_mInitObjs[*it] = *pObjProb;
		delete pObjProb;
	}

}

const Real& AIPlayerType::GetObjectiveProp(const String& rObj) const
{
	StringRealMap::const_iterator it = m_mInitObjs.find(rObj);
	if (it == m_mInitObjs.end())
		throw JFUtil::Exception("AIPlayerType", "GetObjectiveProp",
						"Couldn't find a value for objective: " + rObj);
	return it->second;
}

//bool AIPlayerType::Validate() const
//{
//	if (m_strLogicEngine.empty()) return false;
//
//	for (StringVector::const_iterator it = m_vManagers.begin(); it != m_vManagers.end(); ++it)
//	{
//		const String& rStr = *it;
//		if (rStr.empty()) return false;
//	}
//
//	return true;
//}


/**
 * AIConfig code
 */
AIConfig::AIConfig(String strPath)
: BaseConfig(strPath)
{
	String* pTestLogicEngine = GetString("TestLogicEngine");
	m_strTestLogicEngine = pTestLogicEngine ? *pTestLogicEngine : "Basic";
	delete pTestLogicEngine;

	String* pTestInitialModel = GetString("TestInitialModel");
	m_strTestInitialModel = pTestInitialModel ? *pTestInitialModel : "BasicA1";
	delete pTestInitialModel;

	String* pTestEvaluator = GetString("TestEvaluator");
	m_strTestEvaluator = pTestEvaluator ? *pTestEvaluator : "Preset";
	delete pTestEvaluator;

	String* pTestInitialValues = GetString("TestInitialValues");
	m_strTestInitialValues = pTestInitialValues ? *pTestInitialValues : "PresetA1";
	delete pTestInitialValues;



	unsigned long* pTickLength = GetValue<unsigned long>("TickLength");
	m_iTickLength = pTickLength ? *pTickLength : 1000;
	delete pTickLength;

	long* pAbstractRPUnit = GetValue<long>("AbstractRPUnit");
	m_iAbstractRPUnit = pAbstractRPUnit ? *pAbstractRPUnit : 100;
	delete pAbstractRPUnit;

	int* pAbstractPercentUnit = GetValue<int>("AbstractPercentageUnit");
	m_iAbstractPercentUnit = pAbstractPercentUnit ? *pAbstractPercentUnit : 10;
	delete pAbstractPercentUnit;

	Real* pMinRPsConfidence = GetValue<Real>("MinRPsConfidence");
	m_fMinRPsConfidence = pMinRPsConfidence ? *pMinRPsConfidence : 0.2;
	delete pMinRPsConfidence;

	Real* pMaxRPsConfidence = GetValue<Real>("MaxRPsConfidence");
	m_fMaxRPsConfidence = pMaxRPsConfidence ? *pMaxRPsConfidence : 0.2;
	delete pMaxRPsConfidence;

	Real* pEstRPsConfidence = GetValue<Real>("EstRPsConfidence");
	m_fEstRPsConfidence = pEstRPsConfidence ? *pEstRPsConfidence : 0.6;
	delete pEstRPsConfidence;

	Real* pSaveRPsUpThreshold = GetValue<Real>("SaveRPsUpThreshold");
	m_fSaveRPsUpThreshold = pSaveRPsUpThreshold ? *pSaveRPsUpThreshold : 6;
	delete pSaveRPsUpThreshold;

	Real* pGroupSizeMinProbability = GetValue<Real>("GroupSizeMinProbability");
	m_fGroupSizeMinProbability = pGroupSizeMinProbability ? *pGroupSizeMinProbability : 0.5;
	delete pGroupSizeMinProbability;

	Real* pGroupSizeMaxProbability = GetValue<Real>("GroupSizeMaxProbability");
	m_fGroupSizeMaxProbability = pGroupSizeMaxProbability ? *pGroupSizeMaxProbability : 0.5;
	delete pGroupSizeMaxProbability;

	Real* pFormationSeparationMult = GetValue<Real>("FormationSeparationMult");
	m_fFormationSeparationMult = pFormationSeparationMult ? *pFormationSeparationMult : 4;
	delete pFormationSeparationMult;

	Real* pOrbitSeparationMult = GetValue<Real>("OrbitSeparationMult");
	m_fOrbitSeparationMult = pOrbitSeparationMult ? *pOrbitSeparationMult : 3;
	delete pOrbitSeparationMult;

	unsigned* pOrbitWaypointCount = GetValue<unsigned>("OrbitWaypointCount");
	m_iOrbitWaypointCount = pOrbitWaypointCount ? *pOrbitWaypointCount : 6;
	delete pOrbitWaypointCount;

	Real* pWaypointArrivedRadius = GetValue<Real>("WaypointArrivedRadius");
	m_fWaypointArrivedRadius = pWaypointArrivedRadius ? *pWaypointArrivedRadius : 5;
	delete pWaypointArrivedRadius;

	unsigned* pPredictionTimeStep = GetValue<unsigned>("PredictionTimeStep");
	m_iPredictionTimeStep = pPredictionTimeStep ? *pPredictionTimeStep : 50;
	delete pPredictionTimeStep;

	Real* pMinYawStep = GetValue<Real>("MinYawStep");
	m_fMinYawStep = pMinYawStep ? *pMinYawStep : 5;
	delete pMinYawStep;

	Real* pMaxSlowYawStep = GetValue<Real>("MaxSlowYawStep");
	m_fMaxSlowYawStep = pMaxSlowYawStep ? *pMaxSlowYawStep : 10;
	delete pMaxSlowYawStep;


	Real* pMinPitchStep = GetValue<Real>("MinPitchStep");
	m_fMinPitchStep = pMinPitchStep ? *pMinPitchStep : 5;
	delete pMinPitchStep;

	Real* pMaxSlowPitchStep = GetValue<Real>("MaxSlowPitchStep");
	m_fMaxSlowPitchStep = pMaxSlowPitchStep ? *pMaxSlowPitchStep : 10;
	delete pMaxSlowPitchStep;


	Real* pMinRollStep = GetValue<Real>("MinRollStep");
	m_fMinRollStep = pMinRollStep ? *pMinRollStep : 5;
	delete pMinRollStep;

	Real* pMaxSlowRollStep = GetValue<Real>("MaxSlowRollStep");
	m_fMaxSlowRollStep = pMaxSlowRollStep ? *pMaxSlowRollStep : 10;
	delete pMaxSlowRollStep;


	Real* pMaxFiringAngle = GetValue<Real>("MaxFiringAngle");
	m_fMaxFiringAngle = pMaxFiringAngle ? *pMaxFiringAngle : 0.25;
	delete pMaxFiringAngle;

	Real* pMinEvasionAngle = GetValue<Real>("MinEvasionAngle");
	m_fMinEvasionAngle = pMinEvasionAngle ? *pMinEvasionAngle : 0.5;
	delete pMinEvasionAngle;

	Real* pMaxEvasionAngle = GetValue<Real>("MaxEvasionAngle");
	m_fMaxEvasionAngle = pMaxEvasionAngle ? *pMaxEvasionAngle : 3.5;
	delete pMaxEvasionAngle;

	Real* pDogfightStrafeRunMult = GetValue<Real>("DogfightStrafeRunMult");
	m_fDogfightStrafeRunMult = pDogfightStrafeRunMult ? *pDogfightStrafeRunMult : 1;
	delete pDogfightStrafeRunMult;

	Real* pCollisionAvoidAngleStep = GetValue<Real>("CollisionAvoidAngleStep");
	m_fCollisionAvoidAngleStep = pCollisionAvoidAngleStep ? *pCollisionAvoidAngleStep : 20;
	delete pCollisionAvoidAngleStep;

	unsigned* pCollisionAvoidNumIterations = GetValue<unsigned>("CollisionAvoidNumIterations");
	m_iCollisionAvoidNumIterations = pCollisionAvoidNumIterations ? *pCollisionAvoidNumIterations : 8;
	delete pCollisionAvoidNumIterations;

	Real* pMaxCollisionAvoidDist = GetValue<Real>("MaxCollisionAvoidDist");
	m_fMaxCollisionAvoidDist = pMaxCollisionAvoidDist ? *pMaxCollisionAvoidDist : 10;
	delete pMaxCollisionAvoidDist;

	unsigned* pMaxCollisionAvoidTime = GetValue<unsigned>("MaxCollisionAvoidTime");
	m_iMaxCollisionAvoidTime = pMaxCollisionAvoidTime ? *pMaxCollisionAvoidTime : 4000;
	delete pMaxCollisionAvoidTime;


	String* pDefaultGroupStance = GetString("DefaultGroupStance");
	m_strDefaultGroupStance = pDefaultGroupStance ? *pDefaultGroupStance : "Aggressive";
	delete pDefaultGroupStance;

	unsigned* pThreatRecalcPeriod = GetValue<unsigned>("ThreatRecalcPeriod");
	m_iThreatRecalcPeriod = pThreatRecalcPeriod ? *pThreatRecalcPeriod : 1000;
	delete pThreatRecalcPeriod;


	// Read in the player configs
	StringVector vPlayers = m_oConfig.getMultiSetting("AIPlayerType");
	for (StringVector::const_iterator it = vPlayers.begin();
		 it != vPlayers.end(); ++it)
	{
		m_mPlayers[*it] = new AIPlayerType(*it, this);
	}

	// Read in the stance types
	m_vStanceNames = m_oConfig.getMultiSetting("ShipGroupStance");
	for (StringVector::const_iterator it = m_vStanceNames.begin();
		 it != m_vStanceNames.end(); ++it)
	{
		m_mStances[*it] = new ShipGroupStance(*it, this);
	}

}

AIConfig::~AIConfig()
{
	for (std::map<String, AIPlayerType*>::iterator it = m_mPlayers.begin();
		 it != m_mPlayers.end(); ++it)
	{
		delete it->second;
	}
	m_mPlayers.clear();
}

const AIPlayerType* AIConfig::GetAIPlayerType(const String& rName) const
{
	std::map<String, AIPlayerType*>::const_iterator it =
		m_mPlayers.find(rName);
	return (it == m_mPlayers.end() ? NULL : it->second);
}

const ShipGroupStance* AIConfig::GetShipGroupStance(const String& rName) const
{
	std::map<String, ShipGroupStance*>::const_iterator it =
		m_mStances.find(rName);
	return (it == m_mStances.end() ? NULL : it->second);
}
