#include <Config/BfPConfigWeapons.h>

/**
 * WeaponType code
 */

WeaponType::WeaponType(String strName, const BaseConfig* pConfig)
: EntityType(strName, pConfig)
{
	String* pModel = pConfig->GetString(strName + "Model");
	m_strModel = pModel ? *pModel : "proj_shot.mesh"; // 0 default
	delete pModel;

	Real* pScale = pConfig->GetValue<Real>(strName + "Scale");
	m_fScale = pScale ? *pScale : 0; // 0 default
	delete pScale;

	unsigned* pDamage = pConfig->GetValue<unsigned>(strName + "Damage");
	m_iDamage = pDamage ? *pDamage : 0; // 0dmg default
	delete pDamage;

	Real* pMomentum = pConfig->GetValue<Real>(strName + "MomentumApplied");
	m_fMomentumApplied = pMomentum ? *pMomentum : 0; // 0 default
	delete pMomentum;

	unsigned* pMaxAmmo = pConfig->GetValue<unsigned>(strName + "MaxAmmo");
	m_iMaxAmmo = pMaxAmmo ? *pMaxAmmo : 0; // 0dmg default
	delete pMaxAmmo;

	unsigned* pCooldownTime = pConfig->GetValue<unsigned>(strName + "CooldownTime");
	m_iCooldownTime = pCooldownTime ? *pCooldownTime : 1000;
	delete pCooldownTime;

	unsigned* pEnergyCost = pConfig->GetValue<unsigned>(strName + "EnergyCost");
	m_iEnergyCost = pEnergyCost ? *pEnergyCost : 0;
	delete pEnergyCost;

	// Calculate acceleration for convenience
	assert(GetMass() > 0);
	m_fAccel = GetMaxThrust() / GetMass();

	Real* pInitialSpeed = pConfig->GetValue<Real>(strName + "InitialSpeed");
	m_fInitialSpeed = pInitialSpeed ? *pInitialSpeed : 0.002;
	delete pInitialSpeed;

	Real* pRange = pConfig->GetValue<Real>(strName + "Range");
	m_fRange = pRange ? *pRange : 1000;
	delete pRange;

	// Need to work out the lifetime..
	// s = ut + 1/2 at ^ 2
	// 1/2 at ^ 2 + ut - s = 0
	// Solve quadratic formula with a = 1/2 a, b = u, c = -s
	Real fAccel = GetMaxThrust() / GetMass();
	Real a = 0.5 * fAccel;
	Real b = m_fInitialSpeed;
	Real c = -m_fRange;
	Real fLifetime;

	assert(m_fInitialSpeed > 0);
	if (fAccel == 0)
	{
		fLifetime = m_fRange / m_fInitialSpeed;
		//cout << fLifetime << " = " << m_fRange << " / " << m_fInitialSpeed;
	}
	else
	{

		// This is a nasty way of writing it, but efficiency isn't really a concern for something only run once.
		fLifetime = -b + Math::Sqrt( b*b - 4*a*c);
		//cout << "a: " << a << ", b: " << b << ", c: " << c << endl;
		fLifetime /= 2*a;

		if (fLifetime < 0)
		{
			fLifetime = -b - Math::Sqrt( b*b - 4*a*c);
			fLifetime /= 2*a;
		}
	}

	//cout << "lifetime: " << fLifetime << endl;
	assert(fLifetime > 0);
	m_iLifetime = (unsigned long) fLifetime;


	// Options: auto-tracking, homing, etc.
	bool* pAutoTracking = pConfig->GetValue<bool>(strName + "AutoTracking");
	m_bAutoTracking = pAutoTracking ? *pAutoTracking : false;
	delete pAutoTracking;

	Real* pAutoTrackingMaxAngle = pConfig->GetValue<Real>(strName + "AutoTrackingMaxAngle");
	m_fAutoTrackingMaxAngle = pAutoTrackingMaxAngle ? *pAutoTrackingMaxAngle : 360;
	delete pAutoTrackingMaxAngle;

	bool* pHoming = pConfig->GetValue<bool>(strName + "Homing");
	m_bHoming = pHoming ? *pHoming : false;
	delete pHoming;

	bool* pInfiniteAmmo = pConfig->GetValue<bool>(strName + "InfiniteAmmo");
	m_bInfiniteAmmo = pInfiniteAmmo ? *pInfiniteAmmo : false;
	delete pInfiniteAmmo;

	bool* pNoModel = pConfig->GetValue<bool>(strName + "NoModel");
	m_bNoModel = pNoModel ? *pNoModel : false;
	delete pNoModel;

	bool* pTargetable = pConfig->GetValue<bool>(strName + "Targetable");
	m_bTargetable = pTargetable ? *pTargetable : false;
	delete pTargetable;

}

//bool WeaponType::Validate() const
//{
//	if (m_strModel.empty()) return false;
//	if (!EntityType::Validate()) return false;
//	if (m_fMomentumApplied < 0) return false;
//
//	return true;
//}

/**
 * WeaponsConfig code
 */
WeaponsConfig::WeaponsConfig(String strPath)
: BaseConfig(strPath)
{
	StringVector vWeapons = m_oConfig.getMultiSetting("Weapon Type");
	for (StringVector::const_iterator it = vWeapons.begin();
		 it != vWeapons.end(); ++it)
	{
		m_mWeapons[*it] = new WeaponType(*it, this);
	}
}

WeaponsConfig::~WeaponsConfig()
{
	for (std::map<String, WeaponType*>::iterator it = m_mWeapons.begin();
		 it != m_mWeapons.end(); ++it)
	{
		delete it->second;
	}
	m_mWeapons.clear();
}

const WeaponType* WeaponsConfig::GetWeaponType(String strName) const
{
	std::map<String, WeaponType*>::const_iterator it =
		m_mWeapons.find(strName);
	return (it == m_mWeapons.end() ? NULL : it->second);
}

//bool WeaponsConfig::Validate() const
//{
//	for (map<String, WeaponType*>::const_iterator it = m_mWeapons.begin();
//		 it != m_mWeapons.end(); ++it)
//	{
//		const WeaponType* pWeap = it->second;
//		if (!pWeap->Validate()) return false;
//	}
//	return true;
//}
