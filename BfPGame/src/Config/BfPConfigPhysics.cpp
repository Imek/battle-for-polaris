#include <Config/BfPConfigPhysics.h>

/**
 * PhysicsConfig code
 */
PhysicsConfig::PhysicsConfig(String strPath)
: BaseConfig(strPath)
{
	Real* pImmovableMass = GetValue<Real>("Immovable Mass");
	m_fImmovableMass = pImmovableMass ? *pImmovableMass : 100;
	delete pImmovableMass;

}

PhysicsConfig::~PhysicsConfig() {}
