#include <Config/BfPConfigMap.h>


/**
 * BodyInstance code
 */

BodyInstance::BodyInstance(String strID, String strType, String strLoc)
: m_strID(strID), m_strType(strType), m_vecLoc( JFUtil::VectorFromString(strLoc) )
{
	assert(!m_strID.empty());
	// Needs proper validation later
	assert(!m_strType.empty());
}


/**
 * SectorType code
 */

SectorType::SectorType(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	String* pAmbientColour = pConfig->GetString(GetName() + "AmbientColour");
	m_vecAmbientColour = pAmbientColour ? JFUtil::VectorFromString(*pAmbientColour) : Vector3(1,1,1);
	delete pAmbientColour;

	String* pTexture = pConfig->GetString(GetName() + "StarTexture");
	m_strStarTexture = pTexture ? *pTexture : "default.png"; // A default doesn't exist for now
	delete pTexture;

	String* pDustParticlesConf = pConfig->GetString(GetName() + "DustParticlesConf");
	m_strDustParticlesConf = pDustParticlesConf ? *pDustParticlesConf : "default.particle"; // A default doesn't exist for now
	delete pDustParticlesConf;

	String* pDustParticlesMat = pConfig->GetString(GetName() + "DustParticlesMat");
	m_strDustParticlesMat = pDustParticlesMat ? *pDustParticlesMat : "default"; // A default doesn't exist for now
	delete pDustParticlesMat;

	m_vLinkedSectors = pConfig->GetStrings(GetName() + "LinkedSector");

	StringVector vBodyNames = pConfig->GetStrings(GetName() + "BodyName");
	StringVector vBodyTypes = pConfig->GetStrings(GetName() + "BodyType");
	StringVector vBodyLocs = pConfig->GetStrings(GetName() + "BodyLoc");
	size_t iNoBodies = vBodyNames.size();
	//cout << "size: " << iNoBodies << endl;
	if (iNoBodies != vBodyTypes.size() || vBodyTypes.size() != vBodyLocs.size())
	{
		throw JFUtil::Exception("SectorType", "SectorType",
			"Wrong number of entries for bodies in sector config", strName );
	}
	for (size_t i = 0; i < iNoBodies; ++i)
	{
		//cout << "new BodyInstance: " << vBodyNames[i] << ", " << vSectorTypes[i] << ", " << vBodyLocs[i] << endl;
		m_vBodies.push_back( new BodyInstance(vBodyNames[i], vBodyTypes[i], vBodyLocs[i]) );
	}

	StringVector vWarpPoints = pConfig->GetStrings(GetName() + "WarpPoint");
	for (StringVector::const_iterator it = vWarpPoints.begin();
		 it != vWarpPoints.end(); ++it)
	{
		m_vWarpPoints.push_back( JFUtil::VectorFromString(*it) );
	}

	StringVector vSpawnPoints = pConfig->GetStrings(GetName() + "SpawnPoint");
	for (StringVector::const_iterator it = vSpawnPoints.begin();
		 it != vSpawnPoints.end(); ++it)
	{
		m_vSpawnPoints.push_back( JFUtil::VectorFromString(*it) );
	}

	String* pMapRelPos = pConfig->GetString(GetName() + "MapRelPos");
	m_vecMapRelPos = pMapRelPos ? JFUtil::VectorFromString(*pMapRelPos) : Vector3::ZERO;
	delete pMapRelPos;

	String* pMapTextPos = pConfig->GetString(GetName() + "MapTextPos");
	m_vecMapTextPos = pMapTextPos ? JFUtil::VectorFromString(*pMapTextPos) : Vector3::ZERO;
	delete pMapTextPos;

	// Obj tyeps are currently just hard-coded strings...
	m_vecObjTypes.push_back("SHIP");
	m_vecObjTypes.push_back("PROJECTILE");

	unsigned* pRPIncome = pConfig->GetValue<unsigned>(GetName() + "RPIncome");
	m_iRPIncome = pRPIncome ? *pRPIncome : 0;
	delete pRPIncome;
}

SectorType::~SectorType()
{
	for (BodyVector::iterator it = m_vBodies.begin();
		 it != m_vBodies.end(); ++it)
	{
		delete *it;
	}
}

const Vector3& SectorType::GetWarpPointFromSector(const String& rSector) const
{
	assert(m_vWarpPoints.size() == m_vLinkedSectors.size());

	size_t i = 0;
	for (i = 0; i < m_vLinkedSectors.size(); ++i)
	{
		if (m_vLinkedSectors[i] == rSector) break;
	}
	assert(i < m_vLinkedSectors.size());

	return m_vWarpPoints[i];
}

bool SectorType::IsLinkedTo(const String& rSector) const
{
	return find(m_vLinkedSectors.begin(), m_vLinkedSectors.end(), rSector) != m_vLinkedSectors.end();
}

const BodyInstance* SectorType::GetBodyWithID(String strID) const
{
	const BodyInstance* pBody = NULL;
	for (BodyVector::const_iterator it = m_vBodies.begin(); it != m_vBodies.end(); ++it)
	{
		if ((*it)->GetID() == strID)
		{
			// Don't want duplicate names..
			assert(!pBody);
			pBody = *it;
		}
	}

	return pBody;
}

//bool SectorType::Validate() const
//{
//	if (!ObjectType::Validate()) return false;
//	if (m_strStarTexture.empty()) return false;
//	if (m_strDustParticlesConf.empty() || m_strDustParticlesMat.empty()) return false;
//
//	return true;
//}

/**
 * MapConfig code
 */

MapConfig::MapConfig(const String& rPath)
: BaseConfig(rPath)
, m_vSectors(m_oConfig.getMultiSetting("Sector"))
{
	Real* pInitialMapScale = GetValue<Real>("InitialMapScale");
	m_fInitialMapScale = pInitialMapScale ? *pInitialMapScale : 0.01;
	delete pInitialMapScale;

	unsigned long* pRPIncomeDelay = GetValue<unsigned long>("RPIncomeDelay");
	m_iRPIncomeDelay = pRPIncomeDelay ? *pRPIncomeDelay : 5000;
	delete pRPIncomeDelay;

	unsigned long* pRPMaxAmount = GetValue<unsigned long>("RPMaxAmount");
	m_iRPMaxAmount = pRPMaxAmount ? *pRPMaxAmount : 100000;
	delete pRPMaxAmount;

	unsigned long* pSectorCaptureDelay = GetValue<unsigned long>("SectorCaptureDelay");
	m_iSectorCaptureDelay = pSectorCaptureDelay ? *pSectorCaptureDelay : 10000;
	delete pSectorCaptureDelay;

	String* pMapStarTexture = GetString("MapStarTexture");
	m_strMapStarTexture = pMapStarTexture ? *pMapStarTexture : "default";
	delete pMapStarTexture;

	Real* pMapTextSize = GetValue<Real>("MapTextSize");
	m_fMapTextSize = pMapTextSize ? *pMapTextSize : 2.5;
	delete pMapTextSize;

	Real* pMapNodeSize = GetValue<Real>("MapNodeSize");
	m_fMapNodeSize = pMapNodeSize ? *pMapNodeSize : 0.1;
	delete pMapNodeSize;

	bool* bDrawUnselectedBodyMarkers = GetValue<bool>("DrawUnselectedBodyMarkers");
	m_bDrawUnselectedBodyMarkers = bDrawUnselectedBodyMarkers ? *bDrawUnselectedBodyMarkers : true;
	delete bDrawUnselectedBodyMarkers;

	Real* pSpawnDeviationStep = GetValue<Real>("SpawnDeviationStep");
	m_fSpawnDeviationStep = pSpawnDeviationStep ? *pSpawnDeviationStep : 10;
	delete pSpawnDeviationStep;

	Real* pMaxSpawnDeviation = GetValue<Real>("MaxSpawnDeviation");
	m_fMaxSpawnDeviation = pMaxSpawnDeviation ? *pMaxSpawnDeviation : 200;
	delete pMaxSpawnDeviation;

	Real* pMaxWarpDist = GetValue<Real>("MaxWarpDist");
	m_fMaxWarpDist = pMaxWarpDist ? *pMaxWarpDist : 0;
	delete pMaxWarpDist;


	String* pBodiesConf = GetString("BodiesConfigFile");
	// TODO: Finish adding validation lines like this to all configs
	if (!pBodiesConf) throw
		JFUtil::Exception("MapConfig", "MapConfig",
						  "Missing BodiesConfigFile entry in", rPath);
	m_pBodiesConf = new BodiesConfig(*pBodiesConf);
	delete pBodiesConf;

	// Make the sector types
	for (StringVector::const_iterator it = m_vSectors.begin();
		 it != m_vSectors.end(); ++it)
	{
		m_mSectors[*it] = new SectorType(*it, this);
	}

}

MapConfig::~MapConfig()
{
	for (std::map<String, SectorType*>::iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		delete it->second;
	}
	m_mSectors.clear();
}

const SectorType* MapConfig::GetSectorType(const String& rName) const
{
	String strWTF = rName; // Why the hell do I have to do this
	std::map<String, SectorType*>::const_iterator it =
		m_mSectors.find(strWTF);
	return (it == m_mSectors.end() ? NULL : it->second);
}


//bool MapConfig::Validate() const
//{
//	for (map<String, SectorType*>::const_iterator it = m_mSectors.begin();
//		 it != m_mSectors.end(); ++it)
//	{
//		const SectorType* pSector = it->second;
//		if (!pSector->Validate()) return false;
//	}
//	return true;
//}

