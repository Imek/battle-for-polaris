#include <Config/BfPConfigBodies.h>


/**
 * BodyType code
 */

BodyType::BodyType(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	String* pModel = pConfig->GetString(GetName() + "Model");
	m_strModel = pModel ? *pModel : "NOMODEL"; // Nothing drawn (except maybe light) if this isn't given
	delete pModel;

	// Basic bounding sphere has a radius; defaults to 0, which means the object doesn't collide
	Real* pBoundingRadius = pConfig->GetValue<Real>(GetName() + "BoundingRadius");
	m_fBoundingRadius = pBoundingRadius ? *pBoundingRadius : 0;
	delete pBoundingRadius;

	Real* pScale = pConfig->GetValue<Real>(GetName() + "Scale");
	m_fScale = pScale ? *pScale : 1.0;
	delete pScale;

	bool* pMajor = pConfig->GetValue<bool>(GetName() + "Major");
	m_bMajor = pMajor ? *pMajor : false;
	delete pMajor;

	bool* pBackground = pConfig->GetValue<bool>(GetName() + "Background");
	m_bBackground = pBackground ? *pBackground : false;
	delete pBackground;

	// If these are both zero, then no light is created.

	String* pLightDiffuse = pConfig->GetString(GetName() + "LightDiffuseColour");
	//if (pLightDiffuse) cout << "diffuse colour: " << *pLightDiffuse << endl;
	m_vecLightDiffuse = pLightDiffuse ? JFUtil::VectorFromString(*pLightDiffuse) : Vector3::ZERO;
	delete pLightDiffuse;

	String* pLightSpecular = pConfig->GetString(GetName() + "LightSpecularColour");
	m_vecLightSpecular = pLightSpecular ? JFUtil::VectorFromString(*pLightSpecular) : Vector3::ZERO;
	delete pLightSpecular;

}

//bool BodyType::Validate() const
//{
//	if (!ObjectType::Validate()) return false;
//	if (m_strModel.empty()) return false;
//	if (m_fScale <= 0) return false;
//	if (m_bMajor && m_bBackground) return false;
//
//	return true;
//}

/**
 * BodiesConfig code
 */

BodiesConfig::BodiesConfig(const String& rPath)
: BaseConfig(rPath)
, m_vBodyTypes(m_oConfig.getMultiSetting("Body"))
{
	for (StringVector::const_iterator it = m_vBodyTypes.begin();
		 it != m_vBodyTypes.end(); ++it)
	{
		m_mBodyTypes[*it] = new BodyType(*it, this);
	}
}

BodiesConfig::~BodiesConfig()
{
	for (std::map<String, BodyType*>::iterator it = m_mBodyTypes.begin();
		 it != m_mBodyTypes.end(); ++it)
	{
		delete it->second;
	}
	m_mBodyTypes.clear();
}

const BodyType* BodiesConfig::GetBodyType(String strName) const
{
	String strWTF = strName; // Why the hell do I have to do this
	std::map<String, BodyType*>::const_iterator it =
		m_mBodyTypes.find(strWTF);
	return (it == m_mBodyTypes.end() ? NULL : it->second);
}


//bool BodiesConfig::Validate() const
//{
//	for (map<String, BodyType*>::const_iterator it = m_mBodyTypes.begin();
//		 it != m_mBodyTypes.end(); ++it)
//	{
//		const BodyType* pBody = it->second;
//		if (!pBody->Validate()) return false;
//	}
//	return true;
//}
