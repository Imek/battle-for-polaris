#include <Config/BfPConfigMissions.h>

/**
 * MissionConfig code
 */

MissionConfig::MissionConfig(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	assert(pConfig);
	if (strName.empty())
		throw JFUtil::Exception("MissionConfig", "MissionConfig",
			"Empty mission name given");
	// Get trigger names and pass them into the constructor
	m_vTriggers = pConfig->GetStrings(strName + "Trigger");
	StringVector vEventNames;
	for (StringVector::const_iterator itTrigger = m_vTriggers.begin();
		 itTrigger != m_vTriggers.end(); ++itTrigger)
	{
		// Get the values for the trigger
		TriggerConfig* pTrigger = new TriggerConfig();
		pTrigger->m_strName = *itTrigger;
		pTrigger->m_strMission = strName;

		StringVector vValues = pConfig->GetStrings(strName + *itTrigger + "TriggerType");
		if (vValues.empty())
			throw JFUtil::Exception("MissionConfig", "MissionConfig",
				"No TriggerType entry given for trigger", *itTrigger);
		if (vValues.size() > 1)
			throw JFUtil::Exception("MissionConfig", "MissionConfig",
				"Multiple TriggerType entries given for trigger", *itTrigger);
		pTrigger->m_strTriggerType = vValues[0];

		pTrigger->m_vPrereqs = pConfig->GetStrings(strName + *itTrigger + "Prereq");

		// The timer determines the delay between the trigger first activating
		// and it being considered complete.
		unsigned long* pTimer =
			pConfig->GetValue<unsigned long>(strName + *itTrigger + "TimerDuration");
		pTrigger->m_iTime = pTimer ? *pTimer : 0;
		delete pTimer;

		// Check if there's a LimitRepeats setting for the trigger. (not implemented yet)
//		unsigned long* pRepeats =
//			pConfig->GetValue<unsigned long>(strName + *itTrigger + "LimitRepeats");
//		pTrigger->m_iRepeat = pRepeats ? (long)*pRepeats : -1;
//		delete pRepeats;

		// Make the events for each.
		vEventNames = pConfig->GetStrings(strName + *itTrigger + "Event");
		for (StringVector::const_iterator itEvent = vEventNames.begin();
			 itEvent != vEventNames.end(); ++itEvent)
		{
			EventConfig event;
			event.m_strPrefix = strName + *itTrigger + *itEvent;
			event.m_pConfig = pConfig;
			pTrigger->m_vEvents.push_back(event);
		}

		m_mTriggers[*itTrigger] = pTrigger;

	}
}

MissionConfig::~MissionConfig()
{
	for (map<String,TriggerConfig*>::iterator it = m_mTriggers.begin();
		 it != m_mTriggers.end(); ++it)
	{
		delete it->second;
	}
}

void MissionConfig::GetTriggersWithType(const String& rType, vector<const TriggerConfig*>& rList) const
{
	for (map<String,TriggerConfig*>::const_iterator it = m_mTriggers.begin();
		 it != m_mTriggers.end(); ++it)
	{
		const TriggerConfig* pTrigger = it->second;
		if (pTrigger->m_strTriggerType == rType)
			rList.push_back(pTrigger);
	}
}

const TriggerConfig& MissionConfig::GetTrigger(const String& rName) const
{
	map<String,TriggerConfig*>::const_iterator it = m_mTriggers.find(rName);
	if (it == m_mTriggers.end())
		throw JFUtil::Exception("MissionConfig", "GetTrigger",
			"Couldn't find trigger config with name", rName);
	return *it->second;
}

/**
 * MissionsConfig code
 */

MissionsConfig::MissionsConfig(const String& rPath)
: BaseConfig(rPath)
{
	// Get enabled event types
	m_vEventsEnabled = GetStrings("EventsEnabled");
	// Get each mission from the config file.
	m_vMissions = GetStrings("Mission");
	for (StringVector::const_iterator it = m_vMissions.begin();
		 it != m_vMissions.end(); ++it)
	{
		m_mMissions[*it] = new MissionConfig(*it, this);
	}
}

MissionsConfig::~MissionsConfig()
{
	for (map<String,MissionConfig*>::iterator it = m_mMissions.begin();
		 it != m_mMissions.end(); ++it)
	{
		delete it->second;
	}
	m_mMissions.clear();
}

bool MissionsConfig::GetEventsEnabled(const String& rCategory) const
{
	return find(m_vEventsEnabled.begin(), m_vEventsEnabled.end(), rCategory) != m_vEventsEnabled.end();
}

const MissionConfig& MissionsConfig::GetMission(const String& rName) const
{
	// TODO: Change all "GetType" functions in the config system to return a reference or throw an exception
	String strWTF = rName; // Why the hell do I have to do this
	map<String, MissionConfig*>::const_iterator it =
		m_mMissions.find(strWTF);
	if (it == m_mMissions.end())
		throw JFUtil::Exception("MissionsConfig", "GetMissions",
			"Couldn't find mission with name", rName);
	return *it->second;
}

vector<const TriggerConfig*> MissionsConfig::GetTriggersWithType(const String& rType) const
{
	vector<const TriggerConfig*> vTriggers;
	for (map<String,MissionConfig*>::const_iterator it = m_mMissions.begin();
		 it != m_mMissions.end(); ++it)
	{
		MissionConfig* pMission = it->second;
		pMission->GetTriggersWithType(rType, vTriggers);
	}
	return vTriggers;
}
