#include <Config/BfPConfig.h>

/**
 * BfPConfig code
 */

const String BfPConfig::GENERALCFG_FILE = "polaris.cfg";
const String BfPConfig::PHYSICSCFG_FILE = "physics.cfg";
const String BfPConfig::AICFG_FILE = "ai.cfg";
const String BfPConfig::SHIPSCFG_FILE = "ships.cfg";
const String BfPConfig::WEAPONSCFG_FILE = "weapons.cfg";

BfPConfig::BfPConfig(String strPath)
{
	m_mConfigs["General"] = new GeneralConfig(strPath + GENERALCFG_FILE);
	const GeneralConfig* pConf = (const GeneralConfig*)m_mConfigs["General"];
	m_strScenario = pConf->GetDefaultWorld();

	const StringVector& rScenarioConfigs = pConf->GetScenarioConfigs();
	for (StringVector::const_iterator it = rScenarioConfigs.begin();
		 it != rScenarioConfigs.end(); ++it)
	{
		const String& rFile = pConf->GetScenarioConfig(*it);
		m_mConfigs["Scenario_" + *it] = new ScenarioConfig(strPath + rFile);
	}

	m_mConfigs["Physics"] = new PhysicsConfig(strPath + PHYSICSCFG_FILE);
	m_mConfigs["AI"] = new AIConfig(strPath + AICFG_FILE);
	m_mConfigs["Ships"] = new ShipsConfig(strPath + SHIPSCFG_FILE);
	m_mConfigs["Weapons"] = new WeaponsConfig(strPath + WEAPONSCFG_FILE);
}

void BfPConfig::SetScenario(const String& rConf)
{
	const GeneralConfig* pConf = (const GeneralConfig*)m_mConfigs["General"];
	pConf->GetScenarioConfig(rConf); // Throws an exception if rConf was invalid
	m_strScenario = rConf;
}

const ScenarioConfig* BfPConfig::GetActiveScenario() const
{
	String strName = "Scenario_" + m_strScenario;
	return (ScenarioConfig*)GetConfig(strName);
}

//bool BfPConfig::Validate() const
//{
//	if (!(GameConfig::Validate())) return false;
//	// We need to make sure that every ShipType reference in FactionType refers to an existing ship type.
//	const ShipsConfig* pShipsConf = (const ShipsConfig*)GetConfig("Ships");
//	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
//	const StringVector& rFactions = pScenarioConf->GetFactionNames();
//	for (StringVector::const_iterator it = rFactions.begin();
//		 it != rFactions.end(); ++it)
//	{
//		const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
//		const StringVector& rStartingShips = pFaction->GetStartingShipTypes();
//		for (StringVector::const_iterator it = rStartingShips.begin();
//			 it != rStartingShips.end(); ++it)
//		{
//			if (!pShipsConf->GetShipType(*it)) return false;
//		}
//	}
//	return true;
//}
