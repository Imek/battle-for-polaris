#include <CommandAI/AILogicEngine.h>

/**
 * AILogicEngine code
 */

AILogicEngine::AILogicEngine(String strType, String strInitialModel)
: m_pPlayerModel(NULL)
, m_pObservedModel(NULL)
, m_pOldState(NULL)
, m_pCurrentState(NULL)
, m_strType(strType)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	m_fInjectionsPerTick = pAIConf->GetTickLength() / pScenarioConf->GetRPIncomeDelay();
	assert(m_fInjectionsPerTick >= 0);

	m_pPlayerModel = new AIPlayerModel(strType, strInitialModel);
	m_pObservedModel = new AIPlayerModel(strType);
}

AILogicEngine::~AILogicEngine()
{
	delete m_pPlayerModel;
	delete m_pObservedModel;
}

const AIPlayerModel* AILogicEngine::GetPlayerModel() const
{
	return m_pPlayerModel;
}

const AIPlayerModel* AILogicEngine::GetObservedModel() const
{
	return m_pObservedModel;
}

void AILogicEngine::UpdateAbstractState(
	const AbstractBfPState* pOldState, AbstractBfPState* pCurrentState)
{
	assert(pOldState && pCurrentState);
	assert(pOldState->GetMyFaction() == pCurrentState->GetMyFaction());
	//assert(pOldState->GetAgeTicks() == pCurrentState->

	#ifdef _DEBUG
		pOldState->Validate();
	#endif

	m_pOldState = pOldState;
	m_pCurrentState = pCurrentState;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Make assumptions regarding unknown information for each opposing faction
	String strMyFaction = pCurrentState->GetMyFaction();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		if (*it == strMyFaction) continue;

		EstimateIncome(*it);
		EstimateResources(*it);
		EstimateKnownShips(*it);
		EstimateUnknownShips(*it);
	}

	// Validate if we're debugging (pretty inefficient, so don't do it in release)
	#ifdef _DEBUG
		pCurrentState->Validate();
	#endif

	m_pOldState = NULL;
	m_pCurrentState = NULL;
}

void AILogicEngine::EstimateIncome(const String& rFaction)
{
	const unsigned long& rNewTick = m_pCurrentState->GetAgeTicks();

	assert(m_pOldState && m_pCurrentState);
	assert(m_pOldState->GetMyFaction() == m_pCurrentState->GetMyFaction());
	assert(m_pCurrentState->GetMyFaction() != rFaction);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Get the faction states
	const AbstractFactionState* pOldFaction = m_pOldState->GetFaction(rFaction);
	AbstractFactionState* pNewFaction = m_pCurrentState->GetFaction(rFaction);
	long iMin = pOldFaction->GetMinRPs();
	long iMax = pOldFaction->GetMaxRPs();
	long iEst = pOldFaction->GetEstimatedRPs();

	// Estimate RPs based on estimated sector ownership.
	// Calculate the amount of RP income gained for an owned sector between the two states.
	const unsigned long& rOldAge = m_pOldState->GetAgeTicks();
	const unsigned long& rNewAge = m_pCurrentState->GetAgeTicks();
	assert(rNewAge > rOldAge);
	unsigned long iIncomeInjections = rNewAge - rOldAge;
	iIncomeInjections = (unsigned long) ( (Real)iIncomeInjections * m_fInjectionsPerTick );

	const StringVector& rSectorNames = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectorNames.begin();
		 it != rSectorNames.end(); ++it)
	{
		// Update sector ownership for sectors we observed previously but not now.
		// (We don't take into account capture estimations, which are done later)
		const AbstractSectorState* pOldSector = m_pOldState->GetSector(*it);
		AbstractSectorState* pNewSector = m_pCurrentState->GetSector(*it);
		if (pOldSector->GetVisible() && !pNewSector->GetVisible())
			pNewSector->SetOwnership(pOldSector);
		const Real& rOwnership = pNewSector->GetOwnershipForFaction(rFaction);
		assert(rOwnership >= 0.0 && rOwnership <= 1.0);
		const SectorType* pType = pScenarioConf->GetSectorType(*it);
		unsigned iSectorIncome = pType->GetRPIncome() * iIncomeInjections;

		// Add to MIN the income only if the sector is definitely owned.
		if (rOwnership == 1.0) iMin += iSectorIncome;
		// Add to MAX the income if we have a chance of owning this sector
		if (rOwnership >= 0) iMax += iSectorIncome;
		// Add to EST the income for the sector * probability of ownership.
		iEst += (long)((Real)iSectorIncome * rOwnership);
	}

	pNewFaction->EstimateValues(rNewTick,
		pNewFaction->GetScore(rNewTick), iMin, iMax, iEst);


	// Validate if we're debugging (pretty inefficient, so don't do it in release)
	#ifdef _DEBUG
	m_pCurrentState->Validate();
	#endif
}


void AILogicEngine::EstimateResources(const String& rFaction)
{
//	const unsigned long& rNewTick = m_pCurrentState->GetAgeTicks();

	assert(m_pOldState && m_pCurrentState);
	assert(m_pOldState->GetMyFaction() == m_pCurrentState->GetMyFaction());
	assert(m_pCurrentState->GetMyFaction() != rFaction);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	// Get the faction states
	const AbstractFactionState* pOldFaction = m_pOldState->GetFaction(rFaction);
//	AbstractFactionState* pNewFaction = m_pCurrentState->GetFaction(rFaction);

	// For each possible ship group, get a probability of it being purchased and add/change a hypothetical ship
	const StringVector& rAllTypes = pShipsConf->GetShipTypes();
	assert(!rAllTypes.empty());
	// Maps of shiptype & size to relative probability
	StringRealMap mTypeChoices;
	UIntRealMap mSizeChoices;
	Real fTotalProb = 0;

	for (StringVector::const_iterator itType = rAllTypes.begin();
		 itType != rAllTypes.end(); ++itType)
	{
		const String& rType = *itType;
		const ShipType* pType = pShipsConf->GetShipType(rType);
		unsigned iCost = pType->GetRPCost();
		// Iterate between minimum and maximum group size
		unsigned iMin = m_pPlayerModel->GetMinGroupSize(rType);
		unsigned iMax = m_pPlayerModel->GetMaxGroupSize(rType);
		assert(iMin <= iMax);
		Real fProbToBuy;
		for (unsigned iSize = iMin; iSize <= iMax; ++iSize)
		{
			// NOTE: This system relies on ticks being fast enough to keep up with RP income.
			// Probability of buying = probability of being able to afford * probability of choosing to buy
			fProbToBuy = pOldFaction->GetProbabilityOfHaving(iCost)
				* m_pPlayerModel->EstimatePurchaseProb(rType, iSize);
			if (fProbToBuy > 0)
			{
				// Actual probability of buying this unit (can afford AND chooses to buy)
				assert(mTypeChoices.find(rType) == mTypeChoices.end());
				mTypeChoices[rType] = fProbToBuy;
				if (mSizeChoices.find(iSize) == mSizeChoices.end()) mSizeChoices[iSize] = 0;
				mSizeChoices[iSize] += fProbToBuy;
				fTotalProb += fProbToBuy;
			}

		}

	}

	if (mTypeChoices.empty()) return; // Couldn't afford anything.

	// Work out the overall probability of the new unit existing
	Real fExistence = fTotalProb / (Real)mTypeChoices.size();
	assert(fExistence > 0 && fExistence < fTotalProb); // So fTotalProb > 0!

	// Make the probabilities add to 1 instead of the given proportional values.
	for (StringRealMap::const_iterator it = mTypeChoices.begin();
		 it != mTypeChoices.end(); ++it)
	{
		mTypeChoices[it->first] = mTypeChoices[it->first] / fTotalProb;
	}
	for (UIntRealMap::const_iterator it = mSizeChoices.begin();
		 it != mSizeChoices.end(); ++it)
	{
		mSizeChoices[it->first] = mSizeChoices[it->first] / fTotalProb;
	}

	// Now do sectors.
	StringRealMap mLocation;
	// NOTE: Could add deployment sector probabilities to the player model. For now, just make them equal.
	// Get a list of sectors owned by me ( + number) and work out the probability for each
	Real fTotalOwnership = 0;
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = m_pCurrentState->GetSector(*it);
		Real fOwnership = pSector->GetOwnershipForFaction(rFaction);
		mLocation[*it] = fOwnership;
		fTotalOwnership += fOwnership;
	}
	// We have ownership probability for each and a total; make the probabilities add to 1 like before.
	for (StringRealMap::iterator it = mLocation.begin();
		 it != mLocation.end(); ++it)
	{
		it->second = it->second / fTotalOwnership;
	}

	AddHypotheticalShip(rFaction, fExistence, mTypeChoices, mSizeChoices, mLocation);
}

void AILogicEngine::AddHypotheticalShip(const String& rFaction, const Real& rExistence,
	const StringRealMap& rType, const UIntRealMap& rCount, const StringRealMap& rLocation)
{
	assert(m_pOldState && m_pCurrentState);

	// TODO: Basic just uses one hypothetical ship and merges it each time
}

void AILogicEngine::AccountForRPCost(const String& rFaction,
	const StringRealMap& rType)
{
	assert(m_pOldState && m_pCurrentState);

	// Subtract from MIN the cost of the most expensive type

	// Subtract from EST the cost of each type * that type's probability * the chance of deploying..
}



void AILogicEngine::EstimateKnownShips(const String& rFaction)
{
	assert(m_pOldState && m_pCurrentState);

	// TODO: Pick a hypothetical ship that was most likely to be in this sector.
	// If none match, log a warning message and pick the closest hypothetical ship.
	// If there are no hypothetical ships at all, log a warning and just create the ship.

	// FOR each ship seen now but not previously:

		// Call function that does Bayesian inference of that ship's path.
		// Call function that sweeps forwards, alters RP estimations, then uses that to update probs for subsequently created hyp ships



}



void AILogicEngine::InferShipPastMovement(const String& rFaction, unsigned iUnitID)
{
	// TODO: Construct a Bayesian network for the possible movement of the ship since its earliest possible creation.

	// TODO: Do a sweep of Bayesian inference backwards, calculating new probabilities of location in the ship history.

}

// @brief This function changes estimations for subsequent events for a newly-spotted ship whose past movement has been estimated.
// Then sweep forwards subtracting the RP cost and altering the probabilities for hypothetical ships created later
// (NOTE: Make sure this results in the reverse of RP estimation at the end of EstimatePurchases. (just do the reverse of that for each history tick?))
// TODO: While doing this, also alter sector ownership probs
// (NOTE: a "get unit by tick created" function is needed for this)
void AILogicEngine::PropagateInferredHistory(const String& rFaction, unsigned iUnitID)
{
	// Then sweep forwards subtracting the RP cost and altering the probabilities for hypothetical ships created later

	// FOR each tick from start to finish:

		// Look at the moves that were made by this ship from this to next (if there is a next).
		// Make a map of prev sector -> probability
		// FOR each move arriving with probability > 0:
			// Add probability to total
		// FOR each move arriving with probability > 0:
			// Set map[prevsector] = prob / total

		// Apply this map to the new player model to be applied to learning.

		// Alter sector ownership probability (again, using the same method as before once that's done)

		// Alter the historical RP values to updated ones (as we know that ship was definitely purchased)
			// This should basically be the reverse of RP estimation at the end of EstimatePurchases.

		// Get a list of (OTHER) hypothetical ships created on this tick.
		// FOR each other hypothetical ship created:
			// Give it new probabilities (basically copy EstimatePurchases again?) based on new RP info.


	// TODO: Create a tally of ship built for each tick, and put code in here to add to the spend/save ratio. (map factionname -> pair<ticks_saved, tick_spent>)

	// (NOTE: a "get unit by tick created" function is needed for this)
}


void AILogicEngine::EstimateUnknownShips(const String& rFaction)
{
	assert(m_pOldState && m_pCurrentState);

	// TODO: Estimate new positions of previously unseen/hypothetical ships

	// TODO: Sectors/tick estimation for ship speeds (just shove in a setting value for now)

	// FOR each hypothetical ship:

		// IF the ship was visible before but not now, determine from the event what happened

			// CASE 1: We left the sector (so they are still in it)
				// Do nothing, just predict it in the next step like a hypothetical ship
			// CASE 2: The ship was destroyed while we observed it
				// TODO: Need to make an AI event handler for each sector not for the whole game (telling the AI too much!)
				// Delete the ship from the state
			// CASE 2: The ship warped while we observed it
				// Set the ship location to 100% in dest sector and estimate like normal

		// FOR each possible sector of that ship:

			// Call function that moves probs around (This needs splitting by Basic/Interm!) depending on played model

}

void AILogicEngine::EstimateShipMovement(const String& rFaction, unsigned iUnitID)
{
	assert(m_pOldState && m_pCurrentState);

	// TODO: Move ship probabilities around based on player model

	// TODO: Calculate capture probability based on player model and time spent staying
}



/**
 * AILogicEngineFactory code
 */

AILogicEngine* AILogicEngineFactory::MakeLogicEngine(String strType, String strInitialModel)
{
	AILogicEngine* pEngine = NULL;

	if (strType == "Basic")
	{
		pEngine = new AILogicEngine("Basic", strInitialModel);
	}
	else
	{
		throw JFUtil::Exception("AILogicEngineFactory", "MakeLogicEngine",
			"Invalid logic engine type given (bad config?)", strType);
	}

	assert(pEngine);
	return pEngine;
}

/**
 * AITestLogicEngine code
 */

template<> AITestLogicEngine* JFUtil::Singleton<AITestLogicEngine>::s_instance = NULL;

AITestLogicEngine::AITestLogicEngine(const VisibleState* pState)
{
	m_pState = new AbstractBfPState(pState);
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	m_pEngine = AILogicEngineFactory::MakeLogicEngine(
		pAIConf->GetTestLogicEngine(),
		pAIConf->GetTestInitialModel());
}

AITestLogicEngine::AITestLogicEngine(const AITestLogicEngine& rObj)
{
	throw JFUtil::Exception("AITestLogicEngine", "AITestLogicEngine",
		"Tried to call copy constructor");
}

AITestLogicEngine::~AITestLogicEngine()
{
	delete m_pState;
	delete m_pEngine;
}

void AITestLogicEngine::UpdateState(const VisibleState* pVisibleState)
{
	assert(m_pState && pVisibleState);
	AbstractBfPState* pOldState = new AbstractBfPState(*m_pState);
	m_pState->Update(pVisibleState);
	// TODO: Uncomment once we have finished getting the basic logic engine working
	//m_pEngine->UpdateAbstractState(pOldState, m_pState);
	delete pOldState;
}
