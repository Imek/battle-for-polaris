#include <CommandAI/AIManagerPreset.h>

/**
 * AIFullManagerState public code
 */


AIFullManagerState::AIFullManagerState(const AbstractBfPState* pAbsState)
: m_pAbsState( new AbstractBfPState(*pAbsState) )
{

}


void AIFullManagerState::FromState(const AbstractBfPState* pAbsState)
{
	assert(pAbsState);
	delete m_pAbsState;
	m_pAbsState = new AbstractBfPState(*pAbsState);
}

vector<AIAction*> AIFullManagerState::GetPossibleActions(AA_TYPE* pType) const
{
	vector<AIAction*> vActions;
	//cout << "getting all possible move actions" << endl;

	if (!pType || *pType == AAT_MOVE)
		GetPossibleMoveActions(vActions);

	//cout << "getting all possible purchase actions" << endl;

	if (!pType || *pType == AAT_PURCHASE)
		GetPossiblePurchaseActions(vActions);

	//cout << "getting all possible deploy actions" << endl;

	if (!pType || *pType == AAT_DEPLOY)
		GetPossibleDeployActions(vActions);

	//cout << "got all possible actions" << endl;
	return vActions;
}

/**
 * AIFullManagerState private code
 */

void AIFullManagerState::GetPossibleMoveActions(vector<AIAction*>& rActions) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	std::stringstream sout;

	// For each currently active unit owned by my faction...
	vector<const AbstractUnitState*> vUnits = m_pAbsState->GetMyCurrentFactionUnits();
	for (vector<const AbstractUnitState*>::const_iterator itUnit = vUnits.begin();
		 itUnit != vUnits.end(); ++itUnit)
	{
		// Get the unit state and all the info we need.
		const AbstractUnitState* pUnit = *itUnit;
		assert(!pUnit->GetHypothetical()); // One of our own units cannot be hypothetical!
		unsigned iGroupID = pUnit->GetGroupID();
		sout << iGroupID;
		String strGroupID = sout.str();
		sout.str(""); sout.clear();

		const UnitHistoryEntry* pLatestEntry = pUnit->GetEntry();
		// If its latest tick is not our latest, then it no longer exists
		if (pLatestEntry->GetTick() != m_pAbsState->GetAgeTicks()) continue;

		assert(pLatestEntry->GetObserved()); // Obviously we can see all of our units..
		const String& rSector = pLatestEntry->GetObservedLoc();

		// First, add moves to the unit's current sector.
		const SectorType* pSector = pScenarioConf->GetSectorType(rSector);
		const BodyVector& rBodies = pSector->GetBodies();
		for (BodyVector::const_iterator itBody = rBodies.begin();
			 itBody != rBodies.end(); ++itBody)
		{
			const BodyInstance* pBody = *itBody;
			const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
			if (pType->IsBackground() || !pType->IsMajor()) continue;
//			cout << "move action" << endl;
//			cout << "group ID: " << strGroupID << ", sector: " << rSector << ", body: " << pBody->GetID() << endl;
			rActions.push_back( new AIMoveAction(strGroupID, rSector, pBody->GetID()) );
		}

		// Now, add moves to each body in each adjoining sector.
		const StringVector& rLinks = pSector->GetLinkedSectors();
		for (StringVector::const_iterator itLink = rLinks.begin(); itLink != rLinks.end(); ++itLink)
		{
			assert(*itLink != rSector); // Sector linked to itself??
			pSector = pScenarioConf->GetSectorType(*itLink);
			const BodyVector& rBodies = pSector->GetBodies();
			// Go through each body and add a move for each.
			for (BodyVector::const_iterator itBody = rBodies.begin();
				 itBody != rBodies.end(); ++itBody)
			{
				const BodyInstance* pBody = *itBody;
				const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
				if (pType->IsBackground() || !pType->IsMajor()) continue;
				rActions.push_back( new AIMoveAction(strGroupID, *itLink, pBody->GetID()) );
			}
		}

	}
}

void AIFullManagerState::GetPossiblePurchaseActions(vector<AIAction*>& rActions) const
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");

	const AbstractFactionState* pMyFaction =
		m_pAbsState->GetFaction( m_pAbsState->GetMyFaction() );
	assert(pMyFaction);

	// Here we assume that my faction is correct, i.e. Max=Min=Est for RP values.
	unsigned iRPs =  pMyFaction->GetEstimatedRPs();
	if (iRPs == 0) return;

	// Get "all" possible actions. The intent is for these to be ordered by priority and removed from the list when no longer possible.
	// Simplest way: we just add a set of actions for each possible type/size combination.
	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const String& rShip = *it;
		const ShipType* pShip = pShipsConf->GetShipType(rShip);
		unsigned iShipCost = pShip->GetRPCost();
		assert(iShipCost > 0);
		unsigned iMaxCount = iRPs / iShipCost;
		// The AI can never buy a group that's bigger than the maximum size!
		unsigned iMaxSize = pGameConf->GetMaxGroupSize();
		if (iMaxCount > iMaxSize) iMaxCount = iMaxSize;
		if (iMaxCount == 0) continue;
		std::stringstream ss;
		for (unsigned iCount = 1; iCount < iMaxCount; ++iCount)
		{
			ss << iCount;
			String strCount = ss.str();
			ss.str(""); ss.clear();

			// We have a ship type and a group size; see how many times we can guy it.
			unsigned iGroupCost = iShipCost * iCount;
			// We already checked before, so iGroupCost must be <= iRPs
			assert(iGroupCost <= iRPs);
			rActions.push_back( new AIPurchaseAction(rShip, strCount) );
		}


	}

}

void AIFullManagerState::GetPossibleDeployActions(vector<AIAction*>& rActions) const
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	const String& rMyFaction = m_pAbsState->GetMyFaction();
	const AbstractFactionState* pMyFaction = m_pAbsState->GetMyFactionState();
	assert(pMyFaction);

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	std::stringstream ss;
	String strCount;
	for (StringVector::const_iterator itSector = rSectors.begin();
		 itSector!= rSectors.end(); ++itSector)
	{
		const AbstractSectorState* pSector = m_pAbsState->GetSector(*itSector);
		if (pSector->GetOwnershipForFaction(rMyFaction) <= 0) continue;
		// For each ship type, make an action for each possible group size and sector.
		for (StringVector::const_iterator itType = rShipTypes.begin();
			 itType != rShipTypes.end(); ++itType)
		{
			for (unsigned iCount = 1; iCount <= pMyFaction->GetNumToDeploy(*itType); ++iCount)
			{
				ss << iCount;
				strCount = ss.str();
				ss.str(""); ss.clear();
				rActions.push_back( new AIDeployAction(*itType, strCount, *itSector) );
			}

		}
	}
}

/**
 * AIPresetResourceManager code
 */

void AIPresetResourceManager::ChooseActions(
	const AbstractBfPState* pNewState, ActionQueue& rActionQueue)
{
	if (!m_pManagerState) m_pManagerState = new AIFullManagerState(pNewState);
	else m_pManagerState->FromState(pNewState);

	// Choose purchase actions.
	ChoosePurchaseActions(rActionQueue);

	// So that we deploy the ships we purchased in the same tick,
	// we need to pretend that we have all the ships from the above purchases.
	// The AIPlayer itself will handle the sorting and filtering of impossible ones.
	AbstractBfPState* pStateCopy = new AbstractBfPState( *pNewState );
	AbstractFactionState* pFactionCopy = pStateCopy->GetMyFactionState();

	for (ActionQueue::const_iterator it = rActionQueue.begin();
		 it != rActionQueue.end(); ++it)
	{
		const AIAction* pAction = it->second;
		if (pAction->GetType() != AAT_PURCHASE) continue;
		const AIPurchaseAction* pPAction = (const AIPurchaseAction*)pAction;
		// Add the ships to our deploy list so that they become possible deploys in ChooseDeployActions
		if (it->first > 0)
		{
			cout << "blah" << endl;
		}

		pFactionCopy->AddGroupsToDeploy(pPAction->GetShipType(), pPAction->GetShipCount());
	}
	m_pManagerState->FromState(pStateCopy);
	delete pStateCopy;

	ChooseDeployActions(rActionQueue);


}

void AIPresetResourceManager::ChoosePurchaseActions(ActionQueue& rActionQueue)
{
	const AbstractBfPState* pNewState =
		((AIFullManagerState*)m_pManagerState)->GetState();
	const AbstractFactionState* pMyFaction =
		pNewState->GetFaction( pNewState->GetMyFaction() );
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");

	ActionQueue oAllActions;

	// First, evaluate all possible purchase actions.
	AA_TYPE* pType = new AA_TYPE(AAT_PURCHASE);
	vector<AIAction*> vActions = m_pManagerState->GetPossibleActions(pType);
	delete pType;
	for (vector<AIAction*>::iterator it = vActions.begin(); it != vActions.end(); ++it)
	{
		AIPurchaseAction* pAction = (AIPurchaseAction*)*it;
		int iValue = EvaluatePurchaseAction(pAction);
		pair<int, AIAction*> oEntry (iValue, pAction);
		oAllActions.insert(oEntry);
	}

	cout << "Purchase Actions:" << endl;
	for (ActionQueue::const_iterator it = oAllActions.begin();
		 it != oAllActions.end(); ++it)
	{
		cout << "  " << it->second->GetString() << " (Val: " << it->first << ")" << endl;
	}
	cout << endl;

	// And now, go through the map (highest to lowest) and add new actions to the queue,
	// discarding purchases that can't be afforded.
	unsigned iRPs = pMyFaction->GetEstimatedRPs();
	for (ActionQueue::iterator it = oAllActions.begin(); it != oAllActions.end(); ++it)
	{
		int iValue = it->first;
		AIPurchaseAction* pAction = (AIPurchaseAction*)it->second;
		const ShipType* pType = pShipsConf->GetShipType( pAction->GetShipType() );
		unsigned iCost = pType->GetRPCost() * pAction->GetShipCount();
		// Add the action to the queue if we can afford it. (the evaluator tells us to "save up"
		// for more expensive groups by giving them negative values)
		if (iRPs >= iCost && iValue > 0)
		{
			rActionQueue.insert( pair<int, AIAction*>(iValue, pAction) );
			iRPs -= iCost;
		}
		else
			delete pAction;

	}
}

void AIPresetResourceManager::ChooseDeployActions(ActionQueue& rActionQueue)
{
	// First, evaluate all possible deploy actions.
	AA_TYPE* pType = new AA_TYPE(AAT_DEPLOY);
	vector<AIAction*> vActions = m_pManagerState->GetPossibleActions(pType);
	delete pType;
	for (vector<AIAction*>::iterator it = vActions.begin(); it != vActions.end(); ++it)
	{
		AIDeployAction* pAction = (AIDeployAction*)*it;
		int iValue = EvaluateDeployAction(pAction);
		pair<int, AIAction*> oEntry (iValue, pAction);
		rActionQueue.insert(oEntry);
	}

	// We just return all possible actions sorted in this way, as ExecuteActions will
	// remove lower-valued deploy actions that are no longer possible due to higher-valued
	// ones using up the ships.
}

int AIPresetResourceManager::EvaluatePurchaseAction(AIPurchaseAction* pAction) const
{
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	// Multiply the proportion of each objective by each evaluation
	Real fVal = 0;

	StringRealMap::const_iterator itAttack = m_mObjectives.find("Attack");
	assert(itAttack != m_mObjectives.end());
	StringRealMap::const_iterator itDefend = m_mObjectives.find("Defend");
	assert(itDefend != m_mObjectives.end());
	StringRealMap::const_iterator itExplore = m_mObjectives.find("Explore");
	assert(itExplore != m_mObjectives.end());

	fVal += itAttack->second * EvaluatePurchaseActionForAttack(pAction);
	fVal += itDefend->second * EvaluatePurchaseActionForDefend(pAction);
	fVal += itExplore->second * EvaluatePurchaseActionForExplore(pAction);

	// Do the stuff we do independent of objective: scale the value against group size vs preference,
	// then see if it's below the threshold and return 0 if so.
	fVal *= m_pEvaluator->GetGroupSizePreference(pAction->GetShipType(), pAction->GetShipCount());

	int iValue = (int)fVal;
	// The "save up" threshold is the minimum effectiveness value a ship should have before we buy it.
	// If it's below the threshold, then return 0; then this action will be discarded in favour of possibly
	// A more effective ship that we can't yet afford.
	// NOTE: This'll probably need tweaking (particularly the ship values and the threshold so that the AI actually saves up for more expensive ships)
	if (iValue >= pAIConf->GetSaveRPsUpThreshold())
		return iValue;
	else
		return 0;
}

int AIPresetResourceManager::EvaluatePurchaseActionForAttack(AIPurchaseAction* pAction) const
{
//	const AIConfig* pAIConf = (const AIConfig*)
//		GameConfig::GetInstance().GetConfig("AI");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const AbstractBfPState* pState = GetManagerState()->GetState();

	// In attack, return our ship type's value against all visible enemies.
	const String& rShipType = pAction->GetShipType();
	const ShipType* pMyType = pShipsConf->GetShipType(rShipType);
	unsigned iShipCount = pAction->GetShipCount();
	const String& rMyFaction = pState->GetMyFaction();

	// always return 0 if it has no weapons
	if (pMyType->GetWeapons().size() == 0) return 0;

	// TODO: Scale values by their presence in poorly-defended enemy sectors (see EvaluateMoveActionForAttack)
	// Get the average effectiveness of this ship type against others.
	size_t iNoEnemies = 0;
	Real fTotalEff = 0;
	const AbsUnitList& mUnits = pState->GetUnits();

	for (AbsUnitList::const_iterator itUnit = mUnits.begin();
		 itUnit != mUnits.end(); ++itUnit)
	{
		const AbstractUnitState* pUnit = itUnit->second;
		if (pUnit->GetFaction() == rMyFaction) continue;
		++iNoEnemies;
		if (!pUnit->GetHypothetical())
			fTotalEff += m_pEvaluator->GetGroupValue(
				rShipType, iShipCount, 1.0,
				pUnit->GetKnownType(), pUnit->GetKnownCount(), pUnit->GetHealth());
		else
		{
			// TODO: This is basically "evaluate absunit A against absunit B", so maybe it should be a function of AIAbstractUnitState?
			// Evaluate our group against this group proportional to the probability for each typr & size
			const StringRealMap& rTypeProbs = pUnit->GetTypeProbs();
			const UIntRealMap& rSizeProbs = pUnit->GetCountProbs();
			for (StringRealMap::const_iterator itType = rTypeProbs.begin();
				 itType != rTypeProbs.end(); ++itType)
			{
				const String& rTheirType = itType->first;
				const Real& rTheirTypeProb = itType->second;
				// Trying every possible combination of size and type..
				for (UIntRealMap::const_iterator itSize = rSizeProbs.begin();
				 itSize != rSizeProbs.end(); ++itSize)
				{
					const unsigned& rTheirSize = itSize->first;
					assert(rTheirSize > 0);
					const Real& rTheirSizeProb = itSize->second;
					if (rTheirSizeProb <= 0) continue;

					fTotalEff += rTheirTypeProb * rTheirSizeProb *
						m_pEvaluator->GetGroupValue(rShipType, iShipCount, 1.0, rTheirType, rTheirSize, pUnit->GetHealth());
				}
			}
		}
	}

	if (iNoEnemies == 0) return 0; // Wait until we know what we're up against before we purchase?

	// Take the value, but return at least 5.
	int iVal = (int)((fTotalEff / (Real)iNoEnemies) * 10.0);
	return iVal > 5 ? iVal : 5;
}

int AIPresetResourceManager::EvaluatePurchaseActionForDefend(AIPurchaseAction* pAction) const
{
	// TODO: Proper defensive action evaluation (this currently is basically the same as for attack without the floor)
//	const AIConfig* pAIConf = (const AIConfig*)
//		GameConfig::GetInstance().GetConfig("AI");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AbstractBfPState* pState = GetManagerState()->GetState();

	// In attack, return our ship type's value against all visible enemies.
	const String& rShipType = pAction->GetShipType();
	const ShipType* pMyType = pShipsConf->GetShipType(rShipType);
	unsigned iShipCount = pAction->GetShipCount();
	const String& rMyFaction = pState->GetMyFaction();

	// always return 0 if it has no weapons
	if (pMyType->GetWeapons().size() == 0) return 0;

	// TODO: Scale values by their presence in poorly-defended enemy sectors (see EvaluateMoveActionForAttack)
	// Get the average effectiveness of this ship type against others.
	size_t iNoEnemies = 0;
	Real fTotalEff = 0;
	const AbsUnitList& mUnits = pState->GetUnits();

	for (AbsUnitList::const_iterator itUnit = mUnits.begin();
		 itUnit != mUnits.end(); ++itUnit)
	{
		const AbstractUnitState* pUnit = itUnit->second;
		if (pUnit->GetFaction() == rMyFaction) continue;
		++iNoEnemies;
		if (!pUnit->GetHypothetical())
			fTotalEff += m_pEvaluator->GetGroupValue(
				rShipType, iShipCount, 1.0,
				pUnit->GetKnownType(), pUnit->GetKnownCount(), pUnit->GetHealth());
		else
		{
			// TODO: This is basically "evaluate absunit A against absunit B", so maybe it should be a function of AIAbstractUnitState?
			// Evaluate our group against this group proportional to the probability for each typr & size
			const StringRealMap& rTypeProbs = pUnit->GetTypeProbs();
			const UIntRealMap& rSizeProbs = pUnit->GetCountProbs();
			for (StringRealMap::const_iterator itType = rTypeProbs.begin();
				 itType != rTypeProbs.end(); ++itType)
			{
				const String& rTheirType = itType->first;
				const Real& rTheirTypeProb = itType->second;
				// Trying every possible combination of size and type..
				for (UIntRealMap::const_iterator itSize = rSizeProbs.begin();
				 itSize != rSizeProbs.end(); ++itSize)
				{
					const unsigned& rTheirSize = itSize->first;
					assert(rTheirSize > 0);
					const Real& rTheirSizeProb = itSize->second;
					if (rTheirSizeProb <= 0) continue;

					fTotalEff += rTheirTypeProb * rTheirSizeProb *
						m_pEvaluator->GetGroupValue(rShipType, iShipCount, 1.0, rTheirType, rTheirSize, pUnit->GetHealth());
				}
			}
		}
	}

	// Work out iVal. If it's low, we should check if we have undefended sectors.
	int iVal;
	if (iNoEnemies == 0) iVal = 0; // No enemies, so nothing to evaluate against
	else iVal = (int)((fTotalEff / (Real)iNoEnemies) * 10.0);

	// If we find that we have an undefended sector, the minimum defensive value to return is 5.

	// TODO: This is one of the many things that could be done only once every tick update.
	// (create a new "AbstractObserver" class to contain precalculated things like this)
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = pState->GetSector(*it);
		if (!pSector->GetVisible()) continue;
		if (pSector->GetObservedOwner() != rMyFaction) continue;
		if (pState->GetFactionVisibleUnitCount(rMyFaction, *it) == 0)
		{
			iVal += 5;
			break;
		}
	}
	if (iVal > 10) iVal = 10; // Can never return over 10.

	return iVal;

}

int AIPresetResourceManager::EvaluatePurchaseActionForExplore(AIPurchaseAction* pAction) const
{
	//return EvaluatePurchaseActionForAttack(pAction);

	// For exploration, we don't want to do any pre-set checking for ship type names
	// (as they could change from "Probe", "Fighter", etc). Instead, consider the fastest
	// ships best for exploration

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
//	const AIConfig* pAIConf = (const AIConfig*)
//		GameConfig::GetInstance().GetConfig("AI");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const AbstractBfPState* pState = GetManagerState()->GetState();
	const String& rMyFaction = pState->GetMyFaction();

	const String& rShipType = pAction->GetShipType();
//	unsigned iShipCount = pAction->GetShipCount();

	// But first check if we already have enough ships of this type to cover all sectors we don't own.
	unsigned iNoOfThisType = pState->GetFactionKnownUnitTypeCount( pState->GetMyFaction(), rShipType );
	unsigned iNoSectors = pScenarioConf->GetSectorTypes().size();
	//cout << "iNoOfThisType: " << iNoOfThisType << ", NonVisibleSectors: " << iNoSectors - pState->GetFactionVisibleSectorCount(rMyFaction) << endl;
	if (iNoOfThisType*2 >= iNoSectors - pState->GetFactionVisibleSectorCount(rMyFaction))
		return 0; // In which case, don't buy any more. (this is to prevent the AI from buying loads of useless unarmed probes)

	// TODO: Would be more efficient to pre-calculate this kind of thing
	// (but as we only call this once a tick performance shouldn't be too badly hurt)
	Real fMinSpeed = 0;
	Real fMaxSpeed = 0;
	String strFastestType;
	bool bFirst = true;
	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const ShipType* pType = pShipsConf->GetShipType(*it);
		const Real& rSpeed = pType->GetMaxSpeed();
		if (bFirst || rSpeed < fMinSpeed) fMinSpeed = rSpeed;
		if (bFirst || rSpeed > fMaxSpeed)
		{
			fMaxSpeed = rSpeed;
			strFastestType = *it;
		}
		bFirst = false;
	}
	if (fMaxSpeed == 0) return 0; // No good ships for exploration?
	assert(fMaxSpeed > fMinSpeed);
	Real fMaxDiff = fMaxSpeed - fMinSpeed;
	const ShipType* pMyType = pShipsConf->GetShipType(rShipType);
	Real fDiff = pMyType->GetMaxSpeed() - fMinSpeed;
	assert(fDiff >= 0);
	assert(fMaxDiff >= 0);
	// Need to avoid dividing by zero
	Real fValue;
	if (fDiff == 0)
		fValue = fMinSpeed == fMaxSpeed ? 10 : 0;
	else
		fValue = fDiff / fMaxDiff;
	// fValue is now what proportion of the maximum speed our ship's speed is

	// We used to half the value for unarmed ships (probes), but now we just cap the number of probes we should buy (see above).
	//if (pMyType->GetWeapons().empty()) fValue /= 2;

	// return it as an integer up to 10.
	return (int)(fValue * 10.f);

}


int AIPresetResourceManager::EvaluateDeployAction(AIDeployAction* pAction) const
{
	// Multiply the proportion of each objective by each evaluation
	Real fVal = 0;

	StringRealMap::const_iterator itAttack = m_mObjectives.find("Attack");
	assert(itAttack != m_mObjectives.end());
	StringRealMap::const_iterator itDefend = m_mObjectives.find("Defend");
	assert(itDefend != m_mObjectives.end());
	StringRealMap::const_iterator itExplore = m_mObjectives.find("Explore");
	assert(itExplore != m_mObjectives.end());

	fVal += itAttack->second * EvaluateDeployActionForAttack(pAction);
	fVal += itDefend->second * EvaluateDeployActionForDefend(pAction);
	fVal += itExplore->second * EvaluateDeployActionForExplore(pAction);

	// Floor deploy action value at 1 so that we always at least deploy somewhere
	int iVal = (int)fVal;
	if (iVal < 1) iVal = 1;
	return iVal;
}

int AIPresetResourceManager::EvaluateDeployActionForAttack(AIDeployAction* pAction) const
{
	// TODO: A small issue here where the AI may deploy a smaller group than it could (e.g. a 3-unit group when 5 are available?)

	// NOTE: We could take the deployed unit type into account here as well
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AbstractBfPState* pState = GetManagerState()->GetState();

	// When attacking, we want to deploy our units near enemy sectors.
	// Base the value of this deployment on the best sector to attack nearby (i.e. poorly-defended enemy sectors)
	const String& rMyFaction = pState->GetMyFaction();
	const String& rShipType = pAction->GetShipType();
	//unsigned iShipCount = pAction->GetShipCount();
	const ShipType* pShipType = pShipsConf->GetShipType(rShipType);
	assert(pShipType);
	//unsigned iCount = pAction->GetShipCount();
	const String& rSector = pAction->GetSector();
	const SectorType* pSectorType = pScenarioConf->GetSectorType(rSector);
	assert(pSectorType);


	// Base attack value is 1/noenemies * mostenemies
	StringVector vDests = pSectorType->GetLinkedSectors();
	vDests.push_back(rSector);
	// Remove sectors from vDests that are friendly-owned
	for (StringVector::iterator it = vDests.begin(); it != vDests.end();)
	{
		const AbstractSectorState* pSector = pState->GetSector(*it);
		if (pSector->GetOwnershipForFaction(rMyFaction) == 1)
			it = vDests.erase(it);
		else ++it;
	}
	if (vDests.empty()) return 0; // Nowhere to attack

	StringRealMap mEnemiesInSectors;
	StringRealMap mFriendsInSectors;
	pState->GetEnemyAndFriendAmounts(mFriendsInSectors, mEnemiesInSectors, vDests);

	StringRealMap::const_iterator itMinEnemies =
		min_element(mEnemiesInSectors.begin(), mEnemiesInSectors.end());
	assert(itMinEnemies != mEnemiesInSectors.end());
	// If we got here, there are enemy or neutral sectors;
	// if there are no enemies, then we have targets of opportunity!
	if (itMinEnemies->second == 0) return 10;
	// Otherwise, the value of this deployment is inversely proportional to the least number of enemies
	Real fBestSectorValue = 1 / itMinEnemies->second;

	// TODO: Use friends in sectors to alter the base value (see EvaluateMoveActionForAttack)

	// Returned value is between 0 and 10
	return (int) ( fBestSectorValue * 10);

}

int AIPresetResourceManager::EvaluateDeployActionForDefend(AIDeployAction* pAction) const
{
	// TODO: Use our own evaluation for defend (fairly similar to EvaluateDeployActionForAttack)
	return EvaluateDeployActionForAttack(pAction);
}

int AIPresetResourceManager::EvaluateDeployActionForExplore(AIDeployAction* pAction) const
{
	// TODO: Use our own evaluation for explore (simple; proportional to number of non-visible sectors)
	// See EvaluateMoveActionForExplore
	return EvaluateDeployActionForAttack(pAction);
}




/**
 * AIPresetShipManager code
 */

void AIPresetShipManager::ChooseActions(const AbstractBfPState* pNewState, ActionQueue& rActionQueue)
{
	ActionQueue oAllActions;

	// Just get the best possible action for each group, put them in the queue and pass them up.
	if (!m_pManagerState) m_pManagerState = new AIFullManagerState(pNewState);
	else m_pManagerState->FromState(pNewState);

	// First get them all and sort them into the map.
	AA_TYPE* pType = new AA_TYPE(AAT_MOVE);
	vector<AIAction*> vActions = m_pManagerState->GetPossibleActions(pType);
	delete pType;
	for (vector<AIAction*>::iterator it = vActions.begin(); it != vActions.end(); ++it)
	{
		AIMoveAction* pAction = (AIMoveAction*)*it;
		int iValue = EvaluateMoveAction(pAction);
		pair<int, AIAction*> oEntry (iValue, pAction);
		oAllActions.insert(oEntry);
	}

//	cout << "Move Actions:" << endl;
//	for (ActionQueue::const_iterator it = oAllActions.begin();
//		 it != oAllActions.end(); ++it)
//	{
//		cout << "  " << it->second->GetString() << " (Val: " << it->first << ")" << endl;
//	}
//	cout << endl;

	// And now, go through the map (highest to lowest) and add new moves to the queue,
	// discarding lower-valued duplicates. We also randomise order of duplicate actions of the same value.

	// First we need to sort the action queue into a set of lists grouped by Group ID for each value
	map< int, map< unsigned, vector<AIMoveAction*> >, greater<int> > mActionLists;
	for (ActionQueue::iterator it = oAllActions.begin(); it != oAllActions.end(); ++it)
	{
		int iValue = it->first;
		AIMoveAction* pAction = (AIMoveAction*)it->second;
		unsigned iGroupID = pAction->GetGroupID();

		// Get the action list for this value and Group ID.
		map< unsigned, vector<AIMoveAction*> >& rMap = mActionLists[iValue];
		vector<AIMoveAction*>& rList = rMap[iGroupID];

		rList.push_back(pAction);
	}

	// Now that they're sorted, go through that new map and pick a random action from each duplicate list.
	// Discard the remainders.
	set<unsigned> sDoneGroups;
	for (map< int, map< unsigned, vector<AIMoveAction*> > >::iterator itValue = mActionLists.begin();
		 itValue != mActionLists.end(); ++itValue)
	{
		int iValue = itValue->first;
		map< unsigned, vector<AIMoveAction*> >& rMap = itValue->second;
		for (map< unsigned, vector<AIMoveAction*> >::iterator itDuplicates = rMap.begin();
			 itDuplicates != rMap.end(); ++itDuplicates)
		{
			unsigned iGroupID = itDuplicates->first;
			vector<AIMoveAction*>& rDuplicates = itDuplicates->second;
			// Pick an action at random to add to rActionQueue, but only if we haven't found a better one already.
			int iChosen = -1;
			int iNoDupes = rDuplicates.size();
			if (sDoneGroups.find(iGroupID) == sDoneGroups.end())
				iChosen = rand() % iNoDupes;
			sDoneGroups.insert(iGroupID);
			// Go through the list, inserting out choice and deleting the rest.
			for (int iDupe = 0; iDupe < iNoDupes; ++iDupe)
			{
				AIMoveAction* pAction = rDuplicates[iDupe];
				if (iDupe == iChosen)
					rActionQueue.insert( pair<int, AIAction*>(iValue, pAction) );
				else
					delete pAction;
			}
		}

	}

}

int AIPresetShipManager::EvaluateMoveAction(AIMoveAction* pAction) const
{
	// Multiply the proportion of each objective by each evaluation
	Real fVal = 0;

	StringRealMap::const_iterator itAttack = m_mObjectives.find("Attack");
	assert(itAttack != m_mObjectives.end());
	StringRealMap::const_iterator itDefend = m_mObjectives.find("Defend");
	assert(itDefend != m_mObjectives.end());
	StringRealMap::const_iterator itExplore = m_mObjectives.find("Explore");
	assert(itExplore != m_mObjectives.end());

	fVal += itAttack->second * EvaluateMoveActionForAttack(pAction);
	fVal += itDefend->second * EvaluateMoveActionForDefend(pAction);
	fVal += itExplore->second * EvaluateMoveActionForExplore(pAction);
	return (int)fVal;
}

int AIPresetShipManager::EvaluateMoveActionForAttack(AIMoveAction* pAction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");

	const AbstractBfPState* pState = GetManagerState()->GetState();
	assert(pState);

	// In attack mode, a move action towards a poorly-defended enemy sector is prioritised.
	const String& rMyFaction = pState->GetMyFaction();
	unsigned iGroupID = pAction->GetGroupID();
	const AbstractUnitState* pUnit = pState->GetUnitByGroupID(
		rMyFaction, iGroupID);
	const String& rShipType = pUnit->GetKnownType();
	const ShipType* pShipType = pShipsConf->GetShipType(rShipType);
	assert(pShipType);
	//unsigned iShipCount = pUnit->GetKnownCount();
	const String& rSector = pUnit->GetEntry()->GetObservedLoc();
	const SectorType* pSectorType = pScenarioConf->GetSectorType(rSector);
	assert(pSectorType);

	// Base attack value is 1/noenemies * mostenemies
	StringVector vDests = pSectorType->GetLinkedSectors();
	vDests.push_back(rSector);
	// Remove sectors from vDests that are friendly-owned
	for (StringVector::iterator it = vDests.begin(); it != vDests.end();)
	{
		const AbstractSectorState* pSector = pState->GetSector(*it);
		if (pSector->GetOwnershipForFaction(rMyFaction) == 1)
			it = vDests.erase(it);
		else ++it;
	}
	if (vDests.empty()) return 0; // Nowhere to attack


	StringRealMap mEnemiesInSectors;
	StringRealMap mFriendsInSectors;
	pState->GetEnemyAndFriendAmounts(mFriendsInSectors, mEnemiesInSectors, vDests, (int)iGroupID);

	if (mEnemiesInSectors.find(pAction->GetDestSector()) == mEnemiesInSectors.end() )
		return 0; // Our destination was friendly-owned, so there is no value to attacking it.
	Real fShipsHere = mEnemiesInSectors[ pAction->GetDestSector() ];
	if (fShipsHere > 10) fShipsHere = 10;

	// TODO: Improvement: use friends in sector to alter the base value

	// TODO: Improvement: evaluate individual bodies too

	// Returned value is between 0 and 10
	return (int) ( (1 - fShipsHere/10.0) * 10);
}

int AIPresetShipManager::EvaluateMoveActionForDefend(AIMoveAction* pAction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");

	const AbstractBfPState* pState = GetManagerState()->GetState();
	assert(pState);

	// In defend mode, a move action towards a poorly-defended friendly sector is prioritised.
	const String& rMyFaction = pState->GetMyFaction();
	unsigned iGroupID = pAction->GetGroupID();
	const AbstractUnitState* pUnit = pState->GetUnitByGroupID(
		rMyFaction, iGroupID);
	const String& rShipType = pUnit->GetKnownType();
	const ShipType* pShipType = pShipsConf->GetShipType(rShipType);
	assert(pShipType);
	//unsigned iShipCount = pUnit->GetKnownCount();
	const String& rSector = pUnit->GetEntry()->GetObservedLoc();
	const SectorType* pSectorType = pScenarioConf->GetSectorType(rSector);
	assert(pSectorType);

	const SectorType* pDestSectorType =
		pScenarioConf->GetSectorType(pAction->GetDestSector());
	assert(pDestSectorType);

	// Base defend value is 1/nofriends * mostfriends
	StringVector vDests = pSectorType->GetLinkedSectors();
	vDests.push_back(rSector); // Always consider our own
	// Defend mode only considers friendly-owned sectors as destinations
	for (StringVector::iterator it = vDests.begin(); it != vDests.end();)
	{
		const AbstractSectorState* pSector = pState->GetSector(*it);
		if (pSector->GetOwnershipForFaction(rMyFaction) == 0)
			it = vDests.erase(it);
		else ++it;
	}
	if (vDests.empty()) return 0; // Nowhere to defend


	StringRealMap mEnemiesInSectors;
	StringRealMap mFriendsInSectors;
	pState->GetEnemyAndFriendAmounts(mFriendsInSectors, mEnemiesInSectors, vDests, iGroupID);

	if ( mFriendsInSectors.find(pAction->GetDestSector()) == mFriendsInSectors.end() )
		return 0; // Our destination was not friendly-owned, so there is no value to defending it.
	Real fShipsThere = mFriendsInSectors[ pAction->GetDestSector() ];
	if (fShipsThere > 10) fShipsThere = 10;
	fShipsThere = 1.0 - fShipsThere/10.0; // Value is inverse of ships here between 0 and 1

	// TODO: To improve, use enemies in adjacent sectors to alter the base value

	// TODO: Another improvement is to look at the range of our ship's weapons when deciding on the target body.

	// TODO: Value of a body for defense depends more on the ship type - e.g. interceptors should be kept
	// farther from the centre to where we estimate the enemy will come from. Implement this system once
	// we've implemented the "enemies in adjacent sectors" evaluation above.

	// Simple evaluation of body: go to the one closest to the centre (most defensible..)
	Real fClosest = -1; // Closest to centre
	Real fFarthest = -1;
	Real fMyDist = -1;
	const BodyVector& rBodies = pDestSectorType->GetBodies();
	assert(!rBodies.empty()); //
	for (BodyVector::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
		assert(pType);
		if (!pType->IsMajor()) continue;
		Real fDist = pBody->GetLocation().squaredLength();
		if (pBody->GetID() == pAction->GetDestLocation()) fMyDist = fDist;

		if (fClosest < 0 || fDist < fClosest) fClosest = fDist;
		if (fDist > fFarthest) fFarthest = fDist;
		// After the first these should always be >= 0
		assert(fClosest >= 0 && fFarthest >= 0);
	}
	assert(fMyDist >= 0);
	assert(fMyDist >= fClosest && fMyDist <= fFarthest);
	fMyDist -= fClosest;
	fFarthest -= fClosest;

	Real fVal;
	// If it is the closest, avoid divide by zero
	if (fMyDist == 0)
		fVal = fShipsThere * 10.0;
	else // Otherwise use the distance as a modifier.
	{
		Real fModifier = (1.0 - fMyDist/fFarthest); // Better is closer to the centre
		// Better the fewer friends are here + better the closer we are to the centre of the sector
		fVal = fShipsThere*5.0 + fModifier*5.0;
	}

	return (int)fVal;
}

int AIPresetShipManager::EvaluateMoveActionForExplore(AIMoveAction* pAction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Evaluation of explore actions is simple - give a positive value for non-visible sectors
	const AbstractBfPState* pState = GetManagerState()->GetState();
	assert(pState);
	const AbstractSectorState* pDestSector =
		pState->GetSector( pAction->GetDestSector() );
	const SectorType* pSectorType = pDestSector->GetSectorType();

	// Basic case: look at our group. If the destination is the current sector, return 0.
	const String& rMyFaction = pState->GetMyFaction();
	const AbstractUnitState* pUnit = pState->GetUnitByGroupID(
		rMyFaction, pAction->GetGroupID());
	const UnitHistoryEntry* pNow = pUnit->GetEntry();

	if (pDestSector->GetName() == pNow->GetObservedLoc()) return 1; // Explore doesn't want to stay in the same sector
	if (pDestSector->GetVisible()) return 3; // It's visible, but at least it's not the sector I'm currently in

	// Base value for any other (non-visible) sector is 5
	Real fBaseValue = 5;

	// Sectors with less enemy ownership are more tempting (multiplier is between 1 and 2)
	Real fNeutralOrMineMult = 1 +
		pDestSector->GetNeutralOwnership() +
		pDestSector->GetOwnershipForFaction(rMyFaction);
	assert(fNeutralOrMineMult >= 1);

	// Now evaluate the object - higher value to objects farther away from  centre
	Real fFarthest = -1;
	Real fMyDist = -1;
	const BodyVector& rBodies = pSectorType->GetBodies();
	for (BodyVector::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
		assert(pType);
		if (!pType->IsMajor()) continue;
		Real fDist = pBody->GetLocation().squaredLength();
		if (pBody->GetID() == pAction->GetDestLocation()) fMyDist = fDist;
		if (fDist > fFarthest) fFarthest = fDist;

	}
	assert(fFarthest >= 0);
	assert(fMyDist >= 0);
	assert(fMyDist <= fFarthest);
	if (fFarthest == 0) return (int) (fBaseValue * fNeutralOrMineMult);

	fNeutralOrMineMult *= (fMyDist / fFarthest); // 1 if farthest


	// Returns between 5 and 10
	return (int) (fBaseValue * fNeutralOrMineMult);
}

