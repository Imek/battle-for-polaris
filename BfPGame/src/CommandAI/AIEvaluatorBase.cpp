#include <CommandAI/AIEvaluatorBase.h>

AIEvaluator::AIEvaluator(const String& rAIPlayerType)
: m_strPlayerType(rAIPlayerType)
{

}

Real AIEvaluator::GetGroupSizePreference(const String& rShipType, unsigned iGroupSize) const
{
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	// Validate
	if (iGroupSize == 0)
		throw JFUtil::Exception("AIEvaluator", "GetGroupSizePreference",
			"Zero group size provided");
	if (!pShipsConf->GetShipType(rShipType))
		throw JFUtil::Exception("AIEvaluator", "GetGroupSizePreference",
			"Invalid ship type provided",  rShipType);

	// Get minimum, ideal and maximum values, returning 0 if iGroupSize is outside min/max
	unsigned iMinSize = GetMinGroupSize(rShipType);
	if (iGroupSize < iMinSize) return 0;
	unsigned iMaxSize = GetMaxGroupSize(rShipType);
	if (iGroupSize > iMaxSize) return 0;
	// If it's the ideal size, the probability is 1.
	Real fIdealSize = GetIdealGroupSize(rShipType);
	if (iGroupSize == fIdealSize) return 1;
	// Otherwise it interpolates between min/max probabilities in the config
	// NOTE: This uses the same config values as player models do; we may want to separate them at some point
	Real fMinProb = pAIConf->GetGroupSizeMinProbability();
	assert(fMinProb >= 0.0 && fMinProb <= 1.0);
	Real fMaxProb = pAIConf->GetGroupSizeMaxProbability();
	assert(fMaxProb >= 0.0 && fMaxProb <= 1.0);

	// Before we interpolate, check if iGroupSize is equal to min or max
	if (iGroupSize == iMinSize) return fMinProb;
	else if (iGroupSize == iMaxSize) return fMaxProb;

	// Case A: group size < ideal size
	if (iGroupSize < fIdealSize)
	{
		if (fMinProb == 1.0) return 1.0;

		assert(fIdealSize > iMinSize); // if idealsize == minsize then groupsize < minsize, which we checked
		Real fMaxDiff = fIdealSize - (Real)iMinSize; // Greater than 0
		assert(fMaxDiff > 0); // Just to be sure
		Real fDiff = (Real)iGroupSize - (Real)iMinSize;
		assert(fDiff > 0.0 && fDiff < fMaxDiff);
		// Interpolate our value between fMinProb and 1
		return fMinProb + (1.0-fMinProb) * (fDiff/fMaxDiff);

	}
	else // Case B: group size > ideal size
	{
		if (fMaxProb == 1.0) return 1.0;

		assert(iGroupSize > fIdealSize);
		Real fMaxDiff = (Real)iMaxSize - fIdealSize;
		assert(fMaxDiff > 0);
		Real fDiff = (Real)iGroupSize - (Real)fIdealSize;
		assert(fDiff > 0.0 && fDiff < fMaxDiff);
		// Interpolate our value between 1 and fMaxProb
		return 1.0 - (1.0-fMaxProb) * (fDiff/fMaxDiff);
	}
}

Real AIEvaluator::GetGroupValue(
	const String& rOfType, unsigned iOfCount, const Real& rOfHealth,
	const String& rAgainstType, unsigned iAgainstCount, const Real& rAgainstHealth) const
{
	assert(iOfCount > 0 && iAgainstCount > 0);
	assert(rOfHealth >= 0 && rAgainstHealth >= 0);
	assert(rOfHealth > 0 || rAgainstHealth > 0);
	if (rOfHealth == 0) return -1;
	if (rAgainstHealth == 0) return 1;

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	const ShipType* pOfType = pShipsConf->GetShipType(rOfType);
	if (!pOfType)
		throw JFUtil::Exception("AIEvaluator", "GetGroupValue",
						"Invalid Of ship type provided");
	const ShipType* pAgainstType = pShipsConf->GetShipType(rAgainstType);
	if (!pAgainstType)
		throw JFUtil::Exception("AIEvaluator", "GetGroupValue",
						"Invalid Against ship type provided");

	Real fAverageValue = 0;
	// For each of our ships, get its average value against each of theirs. Then average those values.
	for (size_t iMyShip = 0; iMyShip < iOfCount; ++iMyShip)
	{
		Real fMyAverageValue = 0;
		for (size_t iTheirShip = 0; iTheirShip < iAgainstCount; ++iTheirShip)
		{
			fMyAverageValue += GetShipValue(rOfType, rAgainstType);
		}
		fMyAverageValue /= (Real)iAgainstCount;
		fAverageValue += fMyAverageValue;
	}
	fAverageValue /= (Real)iOfCount;
	// Health modifier can be between 1 and -1
	Real fHealthModifier = rOfHealth - rAgainstHealth;
	return fAverageValue * (1+fHealthModifier);
}
