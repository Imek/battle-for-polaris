#include <CommandAI/AISystem.h>

/**
 * GameAI code
 */

GameAI::GameAI(BfPState* pInitialState)
: m_pCurrentState(pInitialState)
{
	// Look at the world config, adding an AI player for every faction that isn't player-controlled.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
		assert(pFaction);
		const String& rController = pFaction->GetController();
		if (rController != "Player")
		{
			FactionController* pControls = new FactionController(pInitialState, *it);
			AIPlayer* pPlayer = AIPlayerFactory::MakePlayer( pFaction->GetController(), pControls );
			m_mPlayers[*it] = pPlayer;
		}
	}
}

GameAI::~GameAI()
{
	// Destroy all players
	for (AIPlayerList::iterator it = m_mPlayers.begin(); it != m_mPlayers.end(); ++it)
	{
		AIPlayer* pPlayer = it->second;
		delete pPlayer;
	}
	m_mPlayers.clear();
}

void GameAI::Update(unsigned long iTime)
{
	for (AIPlayerList::iterator it = m_mPlayers.begin(); it != m_mPlayers.end(); ++it)
	{
		AIPlayer* pPlayer = it->second;
		pPlayer->Update(iTime);
	}
}
