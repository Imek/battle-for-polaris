#include <CommandAI/AIAction.h>

/**
 * AIMoveAction code
 */

AIMoveAction::AIMoveAction(String strGroupID, String strDestSector, String strDestLocation)
: AIAction(AAT_MOVE, strGroupID, strDestSector, strDestLocation)
{
	assert(!strGroupID.empty() && !strDestSector.empty() && !strDestLocation.empty());
	// Ship ID is stored as a string; convert to int.
	std::stringstream ss;
	ss << GetParameter1();
	assert(!(ss >> m_iGroupID).fail());
}

bool AIMoveAction::CanExecute(const FactionController* pController) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	// Assume basic validity!
	const SectorType* pDestSector = pScenarioConf->GetSectorType( GetDestSector() );
	assert(pDestSector);
	assert( pDestSector->GetBodyWithID( GetDestLocation() ) );

	unsigned iGroupID = GetGroupID();
	const GroupController* pGroup = pController->GetGroupWithID(iGroupID);
	if (!pGroup || pGroup->IsDead()) return false; // Group dead or non-existent
	// NOTE: In the future when we implement some form of world path-finding, this check will not be necessary
	assert(pGroup->GetSector());
	const SectorType* pSector = pGroup->GetSector()->GetSectorType();
	assert(pSector);
	const StringVector& rLinks = pSector->GetLinkedSectors();
	// Check the destination is either in this sector or linked to this one
	if ( GetDestSector() != pSector->GetName() &&
		 find(rLinks.begin(), rLinks.end(), GetDestSector()) == rLinks.end() )
		return false;

	return true;
}

void AIMoveAction::Execute(FactionController* pController) const
{
	// TODO: Execute move action
	//ShipController* pShip = pController->GetShipByID( GetS
	assert( CanExecute(pController) );
	unsigned iGroupID = GetGroupID();
	GroupController* pGroup = pController->GetGroupWithID(iGroupID);
	assert(pGroup);

	pGroup->SetDestLocation( GetDestSector(), GetDestLocation() );
}

String AIMoveAction::GetString() const
{
	std::stringstream sout;
	sout << "M_" << GetGroupID() << "_" << GetDestSector() << "_" << GetDestLocation();
	return sout.str();
}

/**
 * AIPurchaseAction code
 */

AIPurchaseAction::AIPurchaseAction(String strType, String strCount)
: AIAction(AAT_PURCHASE, strType, strCount)
{
	assert(!strType.empty() && !strCount.empty());

	// Check that the ship type exists
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pShipType = pShipsConf->GetShipType(strType);
	assert(pShipType);

	// Ship count is stored as a string; convert to int.
	std::stringstream ss;
	ss << GetParameter2();
	assert(!(ss >> m_iShipCount).fail());
	assert(m_iShipCount > 0);

	// Make up the group
	for (size_t i=0; i<m_iShipCount; ++i) m_vGroup.push_back(strType);
}

bool AIPurchaseAction::CanExecute(const FactionController* pController) const
{
	// Check if the faction state can purchase the ships
	return pController->GetFactionState()->CanPurchaseGroup( m_vGroup );
}

void AIPurchaseAction::Execute(FactionController* pController) const
{
	// Set the types and execute (fails an assertion if the action wasn't possible!)
	pController->SetPurchaseTypes(m_vGroup);
	pController->PurchaseGroup();
}

String AIPurchaseAction::GetString() const
{
	std::stringstream sout;
	sout << "P_" << GetShipType() << "_" << m_iShipCount;
	return sout.str();
}

/**
 * AIDeployAction code
 */

AIDeployAction::AIDeployAction(String strType, String strCount, String strSector)
: AIAction(AAT_DEPLOY, strType, strCount, strSector)
{
	assert(!strType.empty() && !strCount.empty() && !strSector.empty());

	// Check that the ship type exists
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pShipType = pShipsConf->GetShipType( GetShipType() );
	assert(pShipType);

	// Ship count is stored as a string; convert to int.
	std::stringstream ss;
	ss << strCount;
	assert(!(ss >> m_iShipCount).fail());
	assert(m_iShipCount > 0);

	// Make up the group
	for (size_t i=0; i<m_iShipCount; ++i) m_vGroup.push_back( GetShipType() );

	// Check that the sector is valid
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const SectorType* pSector = pScenarioConf->GetSectorType( GetSector() );
	assert(pSector);
}

bool AIDeployAction::CanExecute(const FactionController* pController) const
{
	const BfPSectorState* pSector =
		pController->GetObservedState()->ObserveSector(GetSector());
	if (!pSector || !pSector->GetOwner()) return false; // If we can't observe or it's neutral, we mustn't own.

	// We just check if we "can" deploy here; we don't know for sure until we try (i.e. sectors we may or may not still own)
	return pController->GetFactionState()->HasGroupToDeploy(m_vGroup) &&
		*pSector->GetOwner() == pController->GetFactionName();
}

void AIDeployAction::Execute(FactionController* pController) const
{
	// Set the types and execute (fails an assertion if the action wasn't possible!)
	pController->SetSelectedSector(GetSector());
	pController->SetDeployTypes(m_vGroup);
	pController->DeployGroup();
}

String AIDeployAction::GetString() const
{
	assert(!GetParameter1().empty() && !GetParameter2().empty());
	std::stringstream sout;
	sout << "D_" << GetShipType() << "_" << GetShipCount() << "_" << GetSector();
	return sout.str();
}

/**
 * AIActionFactory code
 */

AIAction* AIActionFactory::MakeAction(String strAction)
{
	// TODO: Read in an action from a string (depending on type)
	if (strAction.size() < 2) return NULL;

	String strCode = strAction.substr(0, 2);
	if (strCode == "M_")
		return MakeMoveAction(strAction);
	else if (strCode == "P_")
		return MakePurchaseAction(strAction);
	else if (strCode == "D_")
		return MakeDeployAction(strAction);
	else
		return NULL; // Invalid type

}

AIMoveAction* AIActionFactory::MakeMoveAction(String strAction)
{
	String strGroupID;
	String strDestSector;
	String strDestLocation;

	// Format: D_strType_strSector_strLocation
	// This should've been checked in MakeAction (if it's otherwise invalid, we return NULL)
	assert(strAction.substr(0, 2) == "M_");
	size_t iPos = strAction.find_first_of("_", 2);
	if (iPos == strAction.npos) return NULL;
	strGroupID = strAction.substr(2, iPos-2);

	size_t iPos2 = strAction.find_first_of("_", iPos+1);
	if (iPos2 == strAction.npos) return NULL;
	strDestSector = strAction.substr(iPos+1, iPos2 - (iPos+1) - 1);

	strDestLocation = strAction.substr(iPos2+1);

	return new AIMoveAction(strGroupID, strDestSector, strDestLocation);
}

AIPurchaseAction* AIActionFactory::MakePurchaseAction(String strAction)
{
	String strShipType;
	String strShipCount;

	// Format: P_strType_strCount
	// This should've been checked in MakeAction
	assert(strAction.substr(0, 2) == "P_");
	size_t iPos = strAction.find_first_of("_", 2);
	if (iPos == strAction.npos) return NULL;
	strShipType = strAction.substr(2, iPos-2);

	strShipCount = strAction.substr(iPos+1);

	return new AIPurchaseAction(strShipType, strShipCount);
}

AIDeployAction* AIActionFactory::MakeDeployAction(String strAction)
{
	// TODO: All this may need testing

	// These are validated against the config in the action constructor.
	String strType;
	String strCount;
	String strSector;

	// Format: D_strType_strSector
	// This should've been checked in MakeAction (if it's otherwise invalid, we return NULL)
	assert(strAction.substr(0, 2) == "D_");
	size_t iPos = strAction.find_first_of("_", 2);
	if (iPos == strAction.npos) return NULL;
	strType = strAction.substr(2, iPos-2);

	size_t iPos2 = strAction.find_first_of("_", iPos+1);
	if (iPos2 == strAction.npos) return NULL;
	strCount = strAction.substr(iPos+1, iPos2 - (iPos+1) - 1);

	strSector = strAction.substr(iPos2+1);

	return new AIDeployAction(strType, strCount, strSector);
}
