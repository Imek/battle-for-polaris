#include <CommandAI/AIAbstractState.h>

/**
 * FactionHistoryEntry code
 */

FactionHistoryEntry::FactionHistoryEntry(unsigned long iTick, long iScore,
	long iMinRPs, long iMaxRPs, long iEstRPs)
: m_iTick(iTick)
, m_iScore(iScore)
, m_iMinRPs(iMinRPs)
, m_iMaxRPs(iMaxRPs)
, m_iEstRPs(iEstRPs)
{
	#ifdef _DEBUG
		Validate();
	#endif
}

Real FactionHistoryEntry::GetProbabilityOfHaving(const long& iAmount) const
{
	// If we're certain of how many RPs this faction has, return either 1 or 0.
	if (m_iMinRPs == m_iMaxRPs && m_iMaxRPs == m_iEstRPs)
		return (iAmount <= m_iEstRPs ? 1.0 : 0.0);

	// Otherwise, use the confidence values from the AI configuration.
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	assert(m_iMinRPs <= m_iEstRPs && m_iEstRPs <= m_iMaxRPs);

	if (iAmount < m_iMinRPs) return 1.0;
	else if (iAmount == m_iMinRPs) return pAIConf->GetMinRPsConfidence();
	else if (iAmount == m_iEstRPs) return pAIConf->GetEstRPsConfidence();
	else if (iAmount == m_iMaxRPs) return pAIConf->GetMaxRPsConfidence();
	else if (iAmount > m_iMaxRPs) return 0.0;
	else if (iAmount < m_iEstRPs) // Between minimum and estimated
	{
		assert(iAmount > m_iMinRPs);
		Real fBetween = (iAmount - m_iMinRPs) / (m_iEstRPs - m_iMinRPs);
		assert(fBetween >= 0.0 && fBetween <= 1.0);
		assert(pAIConf->GetMinRPsConfidence() > pAIConf->GetEstRPsConfidence());
		fBetween *= pAIConf->GetMinRPsConfidence() - pAIConf->GetEstRPsConfidence();
		return pAIConf->GetMinRPsConfidence() - fBetween;

	}
	else // Between estimated and maximum
	{
		assert(iAmount > m_iEstRPs && iAmount < m_iMaxRPs);
		Real fBetween = (iAmount - m_iEstRPs) / (m_iMaxRPs - m_iEstRPs);
		assert(fBetween >= 0.0 && fBetween <= 1.0);
		assert(pAIConf->GetEstRPsConfidence() > pAIConf->GetMaxRPsConfidence());
		fBetween *= pAIConf->GetEstRPsConfidence() - pAIConf->GetMaxRPsConfidence();
		return pAIConf->GetEstRPsConfidence() - fBetween;
	}

}

void FactionHistoryEntry::Update(const long& iScore,
	const long& rMinRPs, const long& rMaxRPs, const long& rEstRPs)
{
	m_iScore = iScore;
	m_iMinRPs = rMinRPs;
	m_iMaxRPs = rMaxRPs;
	m_iEstRPs = rEstRPs;

	#ifdef _DEBUG
		Validate();
	#endif
}

void FactionHistoryEntry::Update(const long& iScore, const long& rKnownRPs)
{
	m_iScore = iScore;
	m_iMinRPs = m_iMaxRPs = m_iEstRPs = rKnownRPs;

	#ifdef _DEBUG
		Validate();
	#endif
}

void FactionHistoryEntry::Validate() const
{
	if (!(m_iMinRPs <= m_iEstRPs && m_iEstRPs <= m_iMaxRPs))
		throw JFUtil::Exception("FactionHistoryEntry", "Validate",
			"RP value(s) invalid for history entry");
}

/**
 * AbstractFactionState code
 */

AbstractFactionState::AbstractFactionState(String strFaction)
: m_strName(strFaction)
, m_iLatestTick(0)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const FactionType* pType = pScenarioConf->GetFactionType(strFaction);
	assert(pType);

	ObserveValues(0, 0, pType->GetStartingRPs());

	// Add an entry of 0 for each possible ship type
	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		m_mPossibleDeploys[*it] = 0;
	}

}

AbstractFactionState::AbstractFactionState(const AbstractFactionState& rFaction)
: m_strName(rFaction.m_strName)
, m_iLatestTick(rFaction.m_iLatestTick)
, m_mPossibleDeploys(rFaction.m_mPossibleDeploys)
{
	for (FactionHistory::const_iterator it = rFaction.m_mHistory.begin();
		 it != rFaction.m_mHistory.end(); ++it)
	{
		m_mHistory[it->first] = new FactionHistoryEntry(*it->second);
	}
}


void AbstractFactionState::UpdateFromFactionState(const FactionState* pState, unsigned long iTick)
{
	assert(pState);
	if (m_strName != pState->GetName())
		throw JFUtil::Exception("AbstractFactionState", "UpdateFromFactionState",
						"Owner of faction state provided did not match mine");

	ObserveValues(iTick, pState->GetScore(), pState->GetRPs());

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const String& rType = *it;
		//const ShipType* pType = pShipsConf->GetShipType(rType);
		//unsigned iCost = pType->GetRPCost();
		// Copy pState to see how many times, if any, we can purchase this group.
		FactionState* pCopy = new FactionState( *pState );

		unsigned& rCount = m_mPossibleDeploys[rType];
		rCount = 0;
		while (pCopy->HasShipToDeploy(rType))
		{
			++rCount;
			pCopy->RemoveDeployingShip(rType);
		}

		delete pCopy;
	}
}

const long& AbstractFactionState::GetMinRPs(unsigned long iTick) const
{
	assert(iTick <= m_iLatestTick);
	FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
	assert(itEntry != m_mHistory.end());
	const FactionHistoryEntry* pEntry = itEntry->second;

	return pEntry->GetMinRPs();
}

const long& AbstractFactionState::GetMinRPs() const
{
	return GetMinRPs(m_iLatestTick);

}

const long& AbstractFactionState::GetMaxRPs(unsigned long iTick) const
{
	assert(iTick <= m_iLatestTick);
	FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
	assert(itEntry != m_mHistory.end());
	const FactionHistoryEntry* pEntry = itEntry->second;

	return pEntry->GetMaxRPs();
}

const long& AbstractFactionState::GetMaxRPs() const
{
	return GetMaxRPs(m_iLatestTick);
}

const long& AbstractFactionState::GetEstimatedRPs(unsigned long iTick) const
{
	assert(iTick <= m_iLatestTick);
	FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
	assert(itEntry != m_mHistory.end());
	const FactionHistoryEntry* pEntry = itEntry->second;

	return pEntry->GetEstRPs();
}

const long& AbstractFactionState::GetEstimatedRPs() const
{
	return GetEstimatedRPs(m_iLatestTick);
}

const long& AbstractFactionState::GetScore(unsigned long iTick) const
{
	assert(iTick <= m_iLatestTick);
	FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
	assert(itEntry != m_mHistory.end());
	const FactionHistoryEntry* pEntry = itEntry->second;

	return pEntry->GetScore();
}

const long& AbstractFactionState::GetScore() const
{
	return GetScore(m_iLatestTick);
}

Real AbstractFactionState::GetProbabilityOfHaving(unsigned long iTick, const long& rRPs) const
{
	// Work out, based on min/max/est RP values for a given tick, the probability of having at least rRPs.
	assert(iTick <= m_iLatestTick);
	FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
	assert(itEntry != m_mHistory.end());
	const FactionHistoryEntry* pEntry = itEntry->second;

	return pEntry->GetProbabilityOfHaving(rRPs);
}

Real AbstractFactionState::GetProbabilityOfHaving(const long& rRPs) const
{
	return GetProbabilityOfHaving(m_iLatestTick, rRPs);
}


void AbstractFactionState::EstimateValues(
	unsigned long iTick, const long& rScore,
	const long& rMinRPs, const long& rMaxRPs, const long& rEstRPs)
{
	FactionHistory::iterator itEntry = m_mHistory.find(iTick);
	FactionHistoryEntry* pEntry;
	if (itEntry == m_mHistory.end())
	{
		pEntry = new FactionHistoryEntry(
			iTick, rScore, rMinRPs, rMaxRPs, rEstRPs);
		m_mHistory[iTick] = pEntry;
	}
	else
	{
		pEntry = itEntry->second;
		pEntry->Update(rScore, rMinRPs, rMaxRPs, rEstRPs);
	}

	if (iTick > m_iLatestTick) m_iLatestTick = iTick;
}

void AbstractFactionState::ObserveValues(unsigned long iTick, const long& rScore, const long& rKnownRPs)
{
	EstimateValues(iTick, rScore, rKnownRPs, rKnownRPs, rKnownRPs);
}

const unsigned& AbstractFactionState::GetNumToDeploy(const String& rShipType) const
{
	std::map<String, unsigned>::const_iterator it = m_mPossibleDeploys.find(rShipType);
	if (it == m_mPossibleDeploys.end())
		throw JFUtil::Exception("AbstractFactionState", "GetNumToDeploy",
						"Couldn't find entry for group type: " + rShipType);
	return it->second;
}

void AbstractFactionState::AddGroupsToDeploy(const String& rType, unsigned iCount )
{
	// Increment number in map
	std::map<String, unsigned>::iterator it = m_mPossibleDeploys.find(rType);
	if (it == m_mPossibleDeploys.end())
		throw JFUtil::Exception("AbstractFactionState", "AddGroupsToDeploy",
						"Couldn't find value for group type: " + rType);
	it->second += iCount;
}

void AbstractFactionState::RemoveDeployingGroups(const String& rType, unsigned iCount)
{
	std::map<String, unsigned>::iterator itType = m_mPossibleDeploys.find(rType);
	assert( itType != m_mPossibleDeploys.end() );
	assert(itType->second >= iCount);
	itType->second -= iCount;
}

void AbstractFactionState::Validate() const
{
	// Make sure faction name and ships to deploy are valid.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (!pScenarioConf->GetFactionType(m_strName))
		throw JFUtil::Exception("AbstractFactionState", "Validate", "m_strName was not a valid faction");

	unsigned long iTick = 0;

	while (iTick <= m_iLatestTick)
	{
		FactionHistory::const_iterator itEntry = m_mHistory.find(iTick);
		if (itEntry == m_mHistory.end())
			throw JFUtil::Exception(
				"AbstractFactionState",
				"Validate", "Missing value in m_mHistory");
		const FactionHistoryEntry* pEntry = itEntry->second;

		pEntry->Validate();

		++iTick;
	}

}
//
//void AbstractFactionState::RoundRPs()
//{
//	// Using the config, we round the RPs down to the closest significant value.
//	const AIConfig* pAIConf = (const AIConfig*)
//		GameConfig::GetInstance().GetConfig("AI");
//	long iAbstractRPUnit = pAIConf->GetAbstractRPUnit();
//	m_iMinRPs -= m_iMinRPs % iAbstractRPUnit;
//	m_iMaxRPs -= m_iMaxRPs % iAbstractRPUnit;
//	m_iEstimatedRPs -= m_iEstimatedRPs % iAbstractRPUnit;
//}

/**
 * AbstractSectorState code
 */
AbstractSectorState::AbstractSectorState(const BfPSectorState* pSector, const String& rName)
: m_strName(rName)
, m_bVisible(pSector)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetSectorType(rName));
	const StringVector& rFactionNames = pScenarioConf->GetFactionNames();

	// Work out who owns this sector.
	String* pOwner = NULL;
	if (pSector) // Easy if observed.
	{
		assert(m_bVisible);
		assert(rName == pSector->GetSectorType()->GetName());
		pOwner = new String(
		 (pSector->GetOwner() ? *pSector->GetOwner() : "NEUTRAL") );
		m_strObservedOwner = *pOwner;
	}
	else
		m_strObservedOwner = "";

	// For each faction, assign a value for ownership (also, as this is an initial state,
	// an unobserved sector that a faction starts with is assumed to be owned by that faction)
	bool bGotOwner = false;
	for (StringVector::const_iterator it = rFactionNames.begin();
		 it != rFactionNames.end(); ++it)
	{
		Real fOwnedBy = 0.0;
		if (pOwner && *it == *pOwner)
		{
			assert(!bGotOwner);
			bGotOwner = true;
			fOwnedBy = 1.0;
		}
		else // Unobserved sector
		{
			const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
			assert(pFaction);
			const StringVector& rStarting = pFaction->GetStartingSectors();
			if (find(rStarting.begin(), rStarting.end(), *it) != rStarting.end())
			{
				assert(!bGotOwner);
				bGotOwner = true;
				fOwnedBy = 1.0;
			}
		}
		m_mOwnership[*it] = fOwnedBy;
	}

	if (!bGotOwner)
	{
		assert(!pOwner || *pOwner == "NEUTRAL");
		m_mOwnership["NEUTRAL"] = 1.0;
	}
	else
		m_mOwnership["NEUTRAL"] = 0.0;

	delete pOwner;
}

const SectorType* AbstractSectorState::GetSectorType() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const SectorType* pReturn = pScenarioConf->GetSectorType(m_strName);
	assert(pReturn);
	return pReturn;
}

void AbstractSectorState::SetOwnership(const AbstractSectorState* pToCopy)
{
	assert(pToCopy);
	m_mOwnership = pToCopy->m_mOwnership;
	#ifdef _DEBUG
		Validate();
	#endif
}

void AbstractSectorState::EstimateOwnership(const StringRealMap& rOwnership)
{
	m_bVisible = false;
	m_mOwnership = rOwnership;
	m_strObservedOwner = "";
	#ifdef _DEBUG
		Validate();
	#endif
}

void AbstractSectorState::ObserveOwnership(const String& rOwner)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	assert(pScenarioConf->GetFactionType(rOwner));
	assert(m_mOwnership.size() == rFactions.size() + 1);
	assert(m_mOwnership.find("NEUTRAL") != m_mOwnership.end());
	assert(m_mOwnership.find(rOwner) != m_mOwnership.end());

	m_mOwnership["NEUTRAL"] = 0;
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		assert(m_mOwnership.find(*it) != m_mOwnership.end());
		if (*it == rOwner) m_mOwnership[*it] = 1;
		else m_mOwnership[*it] = 0;
	}

	m_bVisible = true;
	m_strObservedOwner = rOwner;

	#ifdef _DEBUG
		Validate();
	#endif
}

void AbstractSectorState::ObserveNeutralOwnership()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	assert(m_mOwnership.size() == rFactions.size() + 1);
	assert(m_mOwnership.find("NEUTRAL") != m_mOwnership.end());

	m_mOwnership["NEUTRAL"] = 1;
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		m_mOwnership[*it] = 0;
	}

	m_bVisible = true;
	m_strObservedOwner = "NEUTRAL";

	#ifdef _DEBUG
		Validate();
	#endif
}

void AbstractSectorState::SetNotVisible()
{
	// Current values are kept as estimations; at some point the logic engine should update them.
	m_bVisible = false;
	m_strObservedOwner = "";
}

const Real& AbstractSectorState::GetOwnershipForFaction(const String& rFaction) const
{
	assert(rFaction != "NEUTRAL"); // Should use "GetNeutralOwnership" function
	StringRealMap::const_iterator it = m_mOwnership.find(rFaction);
	assert(it != m_mOwnership.end());
	return it->second;
}

const Real& AbstractSectorState::GetNeutralOwnership() const
{
	StringRealMap::const_iterator it = m_mOwnership.find("NEUTRAL");
	assert(it != m_mOwnership.end());
	return it->second;
}

const String& AbstractSectorState::GetObservedOwner() const
{
	if (!m_bVisible)
		throw JFUtil::Exception("AbstractSectorState", "GetObservedOwner",
			"Tried to get observed owner when not visible");
	assert(m_strObservedOwner != "" && !m_strObservedOwner.empty());
	return m_strObservedOwner;
}

bool AbstractSectorState::GetIsObservedNeutral() const
{
	if (!m_bVisible)
		throw JFUtil::Exception("AbstractSectorState", "GetObservedOwner",
			"Tried to get observed owner when not visible");
	assert(m_strObservedOwner != "" && !m_strObservedOwner.empty());
	return m_strObservedOwner == "NEUTRAL";
}

void AbstractSectorState::Validate() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (!pScenarioConf->GetSectorType(m_strName))
		throw JFUtil::Exception("AbstractSectorState", "Validate", "Invalid sector type");

	if (m_mOwnership.size() != pScenarioConf->GetFactionNames().size() + 1)
		throw JFUtil::Exception(
			"AbstractSectorState", "Validate",
			"Wrong number of entries in m_mOwnership (missing faction?");

	Real fTotalProb = 0;
	for (StringRealMap::const_iterator it = m_mOwnership.begin();
		 it != m_mOwnership.end(); ++it)
	{
		if (!pScenarioConf->GetFactionType(it->first) && it->first != "NEUTRAL")
			throw JFUtil::Exception(
				"AbstractSectorState", "Validate",
				"Invalid faction type in m_mOwnership");
		fTotalProb += it->second;
	}
}

/**
 * AbstractBfPState code
 */

AbstractBfPState::AbstractBfPState(const VisibleState* pState)
: m_iAgeTicks(0)
, m_iNextUnitID(0)
, m_strMyFaction(pState->GetFactionName())
//, m_pEventsLastTick(NULL)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(m_strMyFaction));

	FromState(pState);
	//assert(m_pEventsLastTick);
}

AbstractBfPState::AbstractBfPState(const AbstractBfPState& rState)
: m_iAgeTicks(rState.m_iAgeTicks)
, m_iNextUnitID(rState.m_iNextUnitID)
, m_strMyFaction(rState.m_strMyFaction)
// TODO: uncomment once we have events again
//, m_pEventsLastTick(EventFactory::CopyEventHandler(rState.m_pEventsLastTick))
//, m_pEventsLastTick(NULL)
{
	for (AbsFactionList::const_iterator it = rState.m_mFactions.begin();
		 it != rState.m_mFactions.end(); ++it)
	{
		m_mFactions[it->first] = new AbstractFactionState(*it->second);
	}
	for (AbsUnitList::const_iterator it = rState.m_mUnits.begin();
		 it != rState.m_mUnits.end(); ++it)
	{
		m_mUnits[it->first] = new AbstractUnitState(*it->second);
	}
	for (AbsSectorList::const_iterator it = rState.m_mSectors.begin();
		 it != rState.m_mSectors.end(); ++it)
	{
		m_mSectors[it->first] = new AbstractSectorState(*it->second);
	}
}

const AbstractFactionState* AbstractBfPState::GetFaction(const String& rName) const
{
	AbsFactionList::const_iterator it = m_mFactions.find(rName);
	assert(it != m_mFactions.end());
	return it->second;
}

AbstractFactionState* AbstractBfPState::GetFaction(const String& rName)
{
	AbsFactionList::iterator it = m_mFactions.find(rName);
	assert(it != m_mFactions.end());
	return it->second;
}

const AbstractFactionState* AbstractBfPState::GetMyFactionState() const
{
	AbsFactionList::const_iterator it = m_mFactions.find(m_strMyFaction);
	assert(it != m_mFactions.end());
	return it->second;
}

AbstractFactionState* AbstractBfPState::GetMyFactionState()
{
	AbsFactionList::iterator it = m_mFactions.find(m_strMyFaction);
	assert(it != m_mFactions.end());
	return it->second;
}


unsigned AbstractBfPState::GetFactionVisibleUnitCount(
	const String& rFaction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			pUnit->GetFaction() == rFaction && pEntry->GetObserved())
			++iCount;
	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionVisibleUnitCount(
	const String& rFaction, const String& rSector) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			pUnit->GetFaction() == rFaction && pEntry->GetObserved()
			&& pEntry->GetObservedLoc() == rSector)
			++iCount;
	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionKnownUnitTypeCount(const String& rFaction, const String& rType) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			pUnit->GetFaction() == rFaction && pEntry->GetObserved()
			&& pUnit->GetKnownType() == rType)
			++iCount;
	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionVisibleSectorCount(const String& rFaction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsSectorList::const_iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = it->second;
		assert(pSector);

		if (pSector->GetVisible() && pSector->GetObservedOwner() == rFaction)
			++iCount;
	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionEstimatedUnitCount(
	const String& rFaction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			pUnit->GetFaction() == rFaction && !pEntry->GetObserved())
			++iCount;
	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionEstimatedUnitCount(
	const String& rFaction, const String& rSector) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			pUnit->GetFaction() == rFaction && !pEntry->GetObserved()
			&& pEntry->GetLocProb(rSector) > 0)
			++iCount;

	}
	return iCount;
}

unsigned AbstractBfPState::GetFactionEstimatedSectorCount(const String& rFaction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	unsigned iCount = 0;
	for (AbsSectorList::const_iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = it->second;
		assert(pSector);

		if (!pSector->GetVisible() && pSector->GetOwnershipForFaction(rFaction) > 0)
			++iCount;
	}
	return iCount;
}

const AbstractUnitState* AbstractBfPState::GetUnitByGroupID(const String& rFaction, unsigned iGroupID) const
{
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		AbstractUnitState* pUnit = it->second;
		if (!pUnit->GetHypothetical() && pUnit->GetFaction() == rFaction && pUnit->GetGroupID() == iGroupID)
			return pUnit;
	}
	return NULL;
}

AbstractUnitState* AbstractBfPState::GetUnitByGroupID(const String& rFaction, unsigned iGroupID)
{
	for (AbsUnitList::iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		AbstractUnitState* pUnit = it->second;
		if (!pUnit->GetHypothetical() && pUnit->GetFaction() == rFaction && pUnit->GetGroupID() == iGroupID)
			return pUnit;
	}
	return NULL;
}

const AbstractUnitState* AbstractBfPState::GetUnit(const String& rFaction, unsigned iGroupID) const
{
	AbsUnitList::const_iterator it = m_mUnits.find(
		pair<String, unsigned>(rFaction, iGroupID) );
	assert(it != m_mUnits.end());
	return it->second;
}

AbstractUnitState* AbstractBfPState::GetUnit(const String& rFaction, unsigned iGroupID)
{
	AbsUnitList::iterator it = m_mUnits.find(
		pair<String, unsigned>(rFaction, iGroupID) );
	assert(it != m_mUnits.end());
	return it->second;
}

vector<const AbstractUnitState*> AbstractBfPState::GetFactionUnits(
	const String& rFaction, unsigned iTick) const
{
	vector<const AbstractUnitState*> vUnits;
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		if (pUnit->GetFaction() == rFaction
			&& iTick >= pUnit->GetEarliestTick()
			&& iTick <= pUnit->GetLatestTick())
			vUnits.push_back(pUnit);
	}
	return vUnits;
}

vector<const AbstractUnitState*> AbstractBfPState::GetCurrentFactionUnits(const String& rFaction) const
{
	return GetFactionUnits(rFaction, m_iAgeTicks);
}

vector<const AbstractUnitState*> AbstractBfPState::GetMyCurrentFactionUnits() const
{
	return GetCurrentFactionUnits(m_strMyFaction);
}

void AbstractBfPState::GetEnemyAndFriendAmounts(
	StringRealMap& rFriends, StringRealMap& rEnemies,
	const StringVector& rSectors, int iExcludeGroupID) const
{
	for (StringVector::const_iterator itSector = rSectors.begin();
		 itSector != rSectors.end(); ++itSector)
	{
		rEnemies[*itSector] = 0;
		rFriends[*itSector] = 0;
	}

	// Look at every unit and add its existence * its likelihood to be in this sector
	for (AbsUnitList::const_iterator itUnit = m_mUnits.begin();
		 itUnit != m_mUnits.end(); ++itUnit)
	{
		const pair<String, unsigned>& rUnitID = itUnit->first;
		const AbstractUnitState* pUnit = itUnit->second;
		assert(pUnit->GetFaction() == rUnitID.first);
		bool bFriend = (rUnitID.first == m_strMyFaction);
		// Don't add anything for the unit we said to exclude.
		if (bFriend && (int)pUnit->GetGroupID() == iExcludeGroupID)
			continue;

		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		if (pEntry->GetTick() != m_iAgeTicks) return;

		Real fExistence = pEntry->GetExistence();
		Real fExistenceHere;
		for (StringVector::const_iterator itSector = rSectors.begin();
			 itSector != rSectors.end(); ++itSector)
		{
			const String& rDest = *itSector;
			fExistenceHere = fExistence * pEntry->GetLocProb(rDest);
			assert(fExistenceHere >= 0);
			if (fExistenceHere == 0) continue;

			if (bFriend) rFriends[rDest] += fExistenceHere;
			else rEnemies[rDest] += fExistenceHere;

		}
	}
}

bool AbstractBfPState::KnownEnemyInSector(const String& rSector)
{
	//const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		const UnitHistoryEntry* pEntry = pUnit->GetEntry();
		assert(pUnit && pEntry);

		if (pEntry->GetTick() == m_iAgeTicks &&
			!pUnit->GetHypothetical() && pUnit->GetFaction() != m_strMyFaction
			&& pEntry->GetLocProb(rSector) > 0)
			return true;
	}
	return false;
}

AbstractUnitState* AbstractBfPState::GetHypotheticalUnitOfType(const String& rFaction, const StringRealMap& rTypeProbs)
{
	assert(m_mFactions.find(rFaction) != m_mFactions.end());

	for (AbsUnitList::iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		AbstractUnitState* pUnit = it->second;
		if (pUnit->GetFaction() == rFaction && pUnit->GetHypothetical() &&
			pUnit->GetTypeProbs() == rTypeProbs)
			return pUnit;
	}
	return NULL;
}

const AbstractSectorState* AbstractBfPState::GetSector(const String& rName) const
{
	AbsSectorList::const_iterator it = m_mSectors.find(rName);
	assert(it != m_mSectors.end());
	return it->second;
}

AbstractSectorState* AbstractBfPState::GetSector(const String& rName)
{
	AbsSectorList::iterator it = m_mSectors.find(rName);
	assert(it != m_mSectors.end());
	return it->second;
}

vector<const AbstractSectorState*> AbstractBfPState::GetOwnedSectors(const String& rFaction) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));
	assert(m_mFactions.find(rFaction) != m_mFactions.end());

	// Add to the list any sector with a chance of being owned by the sector.
	vector<const AbstractSectorState*> vSectors;
	for (AbsSectorList::const_iterator itSector = m_mSectors.begin();
		 itSector != m_mSectors.end(); ++itSector)
	{
		const AbstractSectorState* pSector = itSector->second;
		if (pSector->GetOwnershipForFaction(rFaction) > 0)
			vSectors.push_back(pSector);
	}

	return vSectors;
}

void AbstractBfPState::AddUnit(AbstractUnitState* pNewUnit)
{
	// Add a unit (the key is a faction and a unit ID - different from group ID!)
	assert(pNewUnit);
	const String& rFaction = pNewUnit->GetFaction();
	const unsigned& rUnitID = pNewUnit->GetUnitID();
	pair<String, unsigned> oKey (rFaction, rUnitID);

	assert(rUnitID == m_iNextUnitID);
	assert(m_mUnits.find(oKey) == m_mUnits.end());
	m_mUnits[oKey] = pNewUnit;

	// Increment the next new unit ID.
	++m_iNextUnitID;
}


void AbstractBfPState::Clear()
{
	for (AbsFactionList::iterator it = m_mFactions.begin(); it != m_mFactions.end(); ++it)
	{
		delete it->second;
	}
	m_mFactions.clear();
	for (AbsUnitList::iterator it = m_mUnits.begin(); it != m_mUnits.end(); ++it)
	{
		delete it->second;
	}
	m_mUnits.clear();
	for (AbsSectorList::iterator it = m_mSectors.begin(); it != m_mSectors.end(); ++it)
	{
		delete it->second;
	}
	m_mSectors.clear();

	// TODO: uncomment once we have events again
	//m_pEventsLastTick->Clear();
}

void AbstractBfPState::FromState(const VisibleState* pBfPState)
{
	// TODO: uncomment once we have events again
//	if (m_pEventsLastTick)
//	{
//		Clear();
//		m_pEventsLastTick = NULL;
//	}

	// When we call FromState, we just copy the visible information from the game state;
	// we don't make any inferences or estimations here.
	m_iAgeTicks = pBfPState->GetAgeInTicks();

	// TODO: events - Copy the events and clear the handler so we don't get them again.
//	assert(!m_pEventsLastTick);
//	m_pEventsLastTick = EventFactory::CopyEventHandler(&pBfPState->GetAIEvents());

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();

	// Initially, we set up abstract faction states with the starting RPs for each.
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		m_mFactions[*it] = new AbstractFactionState(*it);
	}
	// Update the RP and deployment values for our own faction.
	assert(m_mFactions.find(m_strMyFaction) != m_mFactions.end());
	AbstractFactionState* pMyFaction = m_mFactions[m_strMyFaction];
	pMyFaction->UpdateFromFactionState( pBfPState->GetFactionState(), m_iAgeTicks );

	// Get the location of each perceived ship group from the state
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	for (StringVector::const_iterator itSector = rSectors.begin();
		 itSector != rSectors.end(); ++itSector)
	{
		const BfPSectorState* pSector = pBfPState->ObserveSector(*itSector);
		// First add the sector state (could be null, but that's handled as "unobserved")
		assert( m_mSectors.find(*itSector) == m_mSectors.end() );
		m_mSectors[*itSector] = new AbstractSectorState(pSector, *itSector);

		if (pSector)
		{
			const ObjList& rShipList =  pSector->GetObjectList("SHIP");
			for (ObjList::const_iterator itShip = rShipList.begin();
				 itShip != rShipList.end(); ++itShip)
			{
				ShipState* pShip = (ShipState*) *itShip;
				const String& rOwner = pShip->GetFaction();
				assert(pShip->GetGroupID() >= 0);
				unsigned iGroupID = (unsigned)pShip->GetGroupID();
				unsigned iUnitID = GetNextUnitID();
				pair<String, unsigned> oKey (rOwner, iUnitID);

				AbsUnitList::iterator itTempGroup = m_mUnits.find(oKey);
				// We use the group ID once for each group to look it up in the constructor.
				// If pShip contains a group ID that exists, then there is no need to do anything more.
				if (itTempGroup == m_mUnits.end())
					AddUnit( new AbstractUnitState(m_iAgeTicks, rOwner, pSector, iGroupID, iUnitID) );
			}
		}

	}


}

void AbstractBfPState::Validate() const
{
	// Validation: validate each faction and each unit.
	for (AbsFactionList::const_iterator it = m_mFactions.begin();
		 it != m_mFactions.end(); ++it)
	{
		const AbstractFactionState* pFaction = it->second;
		pFaction->Validate();
	}

	for (AbsUnitList::const_iterator it = m_mUnits.begin();
		 it != m_mUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = it->second;
		pUnit->Validate();
	}

	for (AbsSectorList::const_iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = it->second;
		pSector->Validate();
	}

	// Valid if we got here
}

void AbstractBfPState::Update(const VisibleState* pNewState)
{
	// This function Does the "basic" updating of the abstract state, without trying to do any logical calculation.

	assert(pNewState);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

//	const AIConfig* pAIConf = (const AIConfig*)
//		GameConfig::GetInstance().GetConfig("AI");

	// TODO: First, get the events. (NOTE: we rely on the caller to clear the handler before updating again!)
//	delete m_pEventsLastTick;
//	m_pEventsLastTick = EventFactory::CopyEventHandler(&pNewState->GetAIEvents());


	// Use visible state to update values of this state.
	unsigned long iOldAge = m_iAgeTicks;
	m_iAgeTicks = pNewState->GetAgeInTicks();
	if (m_iAgeTicks != iOldAge + 1)
	{
		throw JFUtil::Exception("AbstractBfPState", "Update",
			"Was given a game state that wasn't 1 tick ahead of the previous");
	}

	// Use the new state of our own faction from the visible state.
	// For other factions, leave them as they are and let the logic engine do predictions.
	const FactionState* pMyFaction = pNewState->GetFactionState();
	AbsFactionList::iterator itMyAbsFaction = m_mFactions.find(m_strMyFaction);
	assert(itMyAbsFaction != m_mFactions.end());
	// The faction only holds the RP, score, and available deployment groups.
	AbstractFactionState* pMyAbsFaction = itMyAbsFaction->second;
	pMyAbsFaction->UpdateFromFactionState(pMyFaction, m_iAgeTicks);

	// Now do all the scores and units for each faction.
	AbsUnitList mUnseenUnits (m_mUnits);
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator itFaction = rFactions.begin();
		 itFaction != rFactions.end(); ++itFaction)
	{
		// Get the faction state and update the score value.
		AbsFactionList::iterator itFactionState = m_mFactions.find(*itFaction);
		assert(itFactionState != m_mFactions.end());
		AbstractFactionState* pFaction = itFactionState->second;
		pFaction->EstimateValues(m_iAgeTicks, pNewState->GetFactionScore(*itFaction),
			pFaction->GetMinRPs(), pFaction->GetMaxRPs(), pFaction->GetEstimatedRPs());

		// For each sector..
		const StringVector& rSectors = pScenarioConf->GetSectorTypes();
		for (StringVector::const_iterator itSector = rSectors.begin();
			 itSector != rSectors.end(); ++itSector)
		{
			const BfPSectorState* pSector = pNewState->ObserveSector(*itSector);
			if (!pSector) continue; // Not visible, so nothing to add
			// All visible ships for this faction in this sector, ordered by group.
			ShipGroupList mShips = pNewState->GetVisibleFactionGroups(*itFaction, *itSector);
			for (ShipGroupList::const_iterator itGroup = mShips.begin();
				 itGroup != mShips.end(); ++itGroup)
			{
				const unsigned& rGroupID = itGroup->first;
				//const ShipGroup& rGroup = itGroup->second;
				// See if we have an abstract unit that corresponds to this actual unit.
				AbstractUnitState* pUnit = GetUnitByGroupID(*itFaction, rGroupID);
				if (pUnit) // We have the unit and it's visible; just update.
				{
//					if (m_strMyFaction == "Exarchy")
//						cout << "OLD: observing " << *itFaction << "'s unit " << rGroupID << " in " << *itSector << endl;

					pUnit->ObserveState(m_iAgeTicks, false, *itSector);
					mUnseenUnits.erase( pair<String, unsigned> (*itFaction, pUnit->GetUnitID()) );
				}
				else // We have spotted a new group we didn't previously see.
				{
//					if (m_strMyFaction == "Exarchy")
//						cout << "NEW: observing " << *itFaction << "'s unit " << rGroupID << " in " << *itSector << endl;


					unsigned iUnitID = GetNextUnitID();
					pair<String, unsigned> oKey (*itFaction, iUnitID);
					AbstractUnitState* pNewUnit = new AbstractUnitState(m_iAgeTicks, *itFaction,
						pSector, rGroupID, iUnitID);
					AddUnit(pNewUnit);
					mUnseenUnits.erase( oKey );
				}

				// TODO: Remember to handle groups that remain in visible sectors but are no longer seen
				// (in logic engine update)

				// TODO: Take account of events (unitWarped and unitDestroyed) here somewhere

			}


		}


	}

	// For now, we look at each new sector state and set it to non-visible if our new state returns null for it.
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator itSector = rSectors.begin(); itSector != rSectors.end(); ++itSector)
	{
		const BfPSectorState* pObservedSector = pNewState->ObserveSector(*itSector);
		AbstractSectorState* pAbsSector = m_mSectors[*itSector];
		if (pObservedSector)
		{
			const String* pOwner = pObservedSector->GetOwner();
			if (pOwner) pAbsSector->ObserveOwnership(*pOwner);
			else pAbsSector->ObserveNeutralOwnership();
		}
		else pAbsSector->SetNotVisible();
	}

	// Go through the rest of the units (mUnseenUnits) and mark them as non-visible.
	// TODO: In the logic engine, go through units in seen sectors with an iLatestTick
	// that isn't this tick and do appropriate estimation (use destroyed/warped events!)

}
