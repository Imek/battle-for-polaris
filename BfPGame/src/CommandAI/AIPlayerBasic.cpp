#include <CommandAI/AIPlayerBasic.h>

/**
 * @brief AIPlayerBasic public code
 */

AIPlayerBasic::AIPlayerBasic(const String& rType, FactionController* pControls)
: AIPlayer(rType, pControls)
{
	const AIPlayerType* pType = GetType();
	const StringVector& rObjs = pType->GetObjectiveTypes();
	for (StringVector::const_iterator it = rObjs.begin(); it != rObjs.end(); ++it)
	{
		m_mObjectives[*it] = pType->GetObjectiveProp(*it);
	}
}

AIPlayerBasic::~AIPlayerBasic()
{

}

void AIPlayerBasic::Update(unsigned long iTime)
{


	// AIPlayer's update function updates low-level AI and calls DoTickUpdate when needed.
	AIPlayer::Update(iTime);
}

void AIPlayerBasic::DoTickUpdate()
{
	// The base class updates the abstract state with the logic engine.
	AIPlayer::DoTickUpdate();

	cout << "doing AIPlayerBasic update" << endl;
	cout << "Attack: " << m_mObjectives["Attack"] << endl;
	cout << "Defend: " << m_mObjectives["Defend"] << endl;
	cout << "Explore: " << m_mObjectives["Explore"] << endl;

	// Update the proportions of objectives based on the latest game state
	cout << "doing objective update" << endl;
	UpdateObjectives();
	cout << "calling managers" << endl;

	for (vector<AIManager*>::iterator it = m_vManagers.begin(); it != m_vManagers.end(); ++it)
	{
		AIPresetManager* pManager = (AIPresetManager*)*it;
		pManager->SetObjectives(m_mObjectives);
		pManager->ChooseActions(m_pPrevState, m_mActionQueue);
	}

	cout << "executing actions" << endl;

	// The managers have given us our actions; now time to execute them properly.
	ExecuteActions();

	cout << "done AIPlayerBasic update" << endl;
}

/**
 * @brief AIPlayerBasic private code
 */

void AIPlayerBasic::UpdateObjectives()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Simple updating of objectives looks at the ratios of friendly to enemy ships to decide the Attack/Defend amount
	assert(m_pPrevState);
	const String& rMyFaction = GetFactionName();

	Real fFriendlyTotal = 0;
	Real fEnemyTotal = 0;

	vector<const AbstractUnitState*> vUnits =
		m_pPrevState->GetFactionUnits(rMyFaction, m_pPrevState->GetAgeTicks());
	for (vector<const AbstractUnitState*>::const_iterator it = vUnits.begin();
		 it != vUnits.end(); ++it)
	{
		const AbstractUnitState* pUnit = *it;
		fFriendlyTotal += pUnit->GetEntry()->GetExistence();
	}

	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		if (*it == rMyFaction) continue;
		vUnits = m_pPrevState->GetFactionUnits(*it, m_pPrevState->GetAgeTicks());
		for (vector<const AbstractUnitState*>::const_iterator it = vUnits.begin();
			 it != vUnits.end(); ++it)
		{
			const AbstractUnitState* pUnit = *it;
			fEnemyTotal += pUnit->GetEntry()->GetExistence();
		}
	}

	assert(fFriendlyTotal >= 0 || fEnemyTotal >= 0);

	// We then look at what sectors we can/can't see to work out the amount of Explore objective
	// (but don't count sectors we know have enemies in them for exploration)
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	size_t iNoSectors = rSectors.size();
	size_t iNonVisibleSectors = 0;
	for (StringVector::const_iterator it = rSectors.begin(); it != rSectors.end(); ++it)
	{
		const AbstractSectorState* pSector = m_pPrevState->GetSector(*it);
		if (!pSector->GetVisible() && !m_pPrevState->KnownEnemyInSector(*it)) ++iNonVisibleSectors;
	}
	if (iNonVisibleSectors > iNoSectors)
		throw JFUtil::Exception("AIPlayerBasic", "UpdateObjectives", "Invalid ownership value");

	// Now assign the values!
	// NOTE: These should always add up to 1! (validate in ValidateObjectives)
	Real fNonVisibleProp = (Real)iNonVisibleSectors / iNoSectors;
	Real fVisibleProp = 1 - fNonVisibleProp;
	m_mObjectives["Explore"] = fNonVisibleProp;

	// Attack/Defend more the more sectors we can see.
	// Attack more if we outnumber the enemy. Defend more if they outnumber us.
	if (fEnemyTotal == 0)
	{
		m_mObjectives["Attack"] = fVisibleProp;
		m_mObjectives["Defend"] = 0;
	}
	else if (fFriendlyTotal == 0)
	{
		m_mObjectives["Attack"] = 0;
		m_mObjectives["Defend"] = fVisibleProp;
	}
	else
	{
		Real fTotal = fFriendlyTotal + fEnemyTotal;
		m_mObjectives["Attack"] = (fFriendlyTotal/fTotal) * fVisibleProp;
		m_mObjectives["Defend"] = (fEnemyTotal/fTotal) * fVisibleProp;
	}

	ValidateObjectives();
}

void AIPlayerBasic::ValidateObjectives() const
{
	Real fTotal = 0;
	const AIPlayerType* pType = GetType();
	const StringVector& rObjs = pType->GetObjectiveTypes();
	for (StringVector::const_iterator it = rObjs.begin(); it != rObjs.end(); ++it)
	{
		StringRealMap::const_iterator itObj = m_mObjectives.find(*it);
		if (itObj == m_mObjectives.end())
			throw JFUtil::Exception("AIPlayerBasic", "ValidateObjectives",
							"Couldn't find a value for objective: " + *it);
		fTotal += itObj->second;
	}
//	if (fTotal != 1)
//		throw JFUtil::Exception("AIPlayerBasic", "ValidateObjectives",
//						"Total objective proportions didn't add to 1");
}
