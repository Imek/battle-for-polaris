#include <CommandAI/AIPlayerModel.h>

/**
 * AIPlayerModel code
 */

AIPlayerModel::AIPlayerModel(String strType, String strInitialModel)
: BaseConfig(BfPConfig::AICFG_FILE)
, m_strType(strType)
{

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	String strKey;

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		// Purchase probability for each ship type
		strKey = strInitialModel + *it + "Prob";
		Real* pProb = GetValue<Real>(strKey);
		if (!pProb)
			throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
				"Missing initial purchase value in player model: " + strKey);
		m_mPurchaseProbs[*it] = *pProb;
		delete pProb;

		// Min/max/ideal group size for each ship type
		strKey = strInitialModel + *it + "MinGroupSize";
		unsigned* pSize = GetValue<unsigned>(strKey);
		if (!pSize)
			throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
				"Missing initial min group size value in player model: " + strKey);
		m_mMinGroupSizes[*it] = *pSize;
		delete pSize;

		strKey = strInitialModel + *it + "MaxGroupSize";
		pSize = GetValue<unsigned>(strKey);
		if (!pSize)
			throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
				"Missing initial max group size value in player model: " + strKey);
		m_mMaxGroupSizes[*it] = *pSize;
		delete pSize;

		strKey = strInitialModel + *it + "IdealGroupSize";
		Real* pIdealSize = GetValue<Real>(strKey);
		if (!pIdealSize)
			throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
				"Missing initial ideal group size value in player model: " + strKey);
		m_mIdealGroupSizes[*it] = *pIdealSize;
		delete pIdealSize;

		if ( !(m_mMinGroupSizes[*it] <= m_mIdealGroupSizes[*it] &&
			   m_mIdealGroupSizes[*it] <= m_mMaxGroupSizes[*it]) )
			throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
				"Invalid group size values (should be min<=ideal<=max) in player model: " + strKey);


	}

	strKey = strInitialModel + "SaveProb";
	Real* pSaveProb = GetValue<Real>(strKey);
	if (!pSaveProb)
		throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
			"Missing initial value in player model: " + strKey);
	m_fSaveProb = *pSaveProb;
	delete pSaveProb;

	strKey = strInitialModel + "GroupSizeLearningRate";
	Real* pGroupSizeLR = GetValue<Real>(strKey);
	if (!pGroupSizeLR)
		throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
			"Missing group size learning rate value in player model: " + strKey);
	m_fGroupSizeLRate = *pGroupSizeLR;
	delete pGroupSizeLR;

	strKey = strInitialModel + "PurchaseLearningRate";
	Real* pPurchaseLR = GetValue<Real>(strKey);
	if (!pPurchaseLR)
		throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
			"Missing purchase learning rate value in player model: " + strKey);
	m_fPurchaseLRate = *pPurchaseLR;
	delete pPurchaseLR;

	strKey = strInitialModel + "MoveLearningRate";
	Real* pMoveLR = GetValue<Real>(strKey);
	if (!pMoveLR)
		throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
			"Missing initial value in player model: " + strKey);
	m_fMoveLRate = *pMoveLR;
	delete pMoveLR;


	// Read in the sector-by-sector move values.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const String& rFrom = *it;
		const SectorType* pSector = pScenarioConf->GetSectorType(rFrom);
		assert(pSector);
		const StringVector& rLinks = pSector->GetLinkedSectors();
		for (StringVector::const_iterator itLinked = rLinks.begin();
			 itLinked != rLinks.end(); ++itLinked)
		{
			const String& rTo = *itLinked;
			assert( pScenarioConf->GetSectorType(rTo) );

			strKey = strInitialModel + rFrom + rTo + "MoveProb";
			Real* pProb = GetValue<Real>(strKey);
			// if we have a scenario with dummy AI we may not care about player models,
			// so just put in a placeholder value
			if (!pProb)
			{
				pProb = new Real(0.2f);
			}
//				throw JFUtil::Exception("AIPlayerModel", "AIPlayerModel",
//					"Missing initial value in player model: " + strKey);
			m_mMoveProbs[ pair<String, String>(rFrom, rTo) ] = *pProb;
			delete pProb;
		}

	}
}

AIPlayerModel::AIPlayerModel(String strType)
: BaseConfig(BfPConfig::AICFG_FILE)
, m_strType(strType)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		m_mPurchaseProbs[*it] = 0.0;
	}

	// We don't use the learning rate values for the "observed model"
	m_fPurchaseLRate = 0.0;
	m_fMoveLRate = 0.0;


	// Intialise all the sector-to-sector move probabilities to 0.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	String strKey;

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const String& rFrom = *it;
		const SectorType* pSector = pScenarioConf->GetSectorType(rFrom);
		assert(pSector);
		const StringVector& rLinks = pSector->GetLinkedSectors();
		for (StringVector::const_iterator itLinked = rLinks.begin();
			 itLinked != rLinks.end(); ++itLinked)
		{
			const String& rTo = *itLinked;
			assert( pScenarioConf->GetSectorType(rTo) );
			m_mMoveProbs[ pair<String, String>(rFrom, rTo) ] = 0.0;
		}

	}
}

bool AIPlayerModel::Validate() const
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	for (StringRealMap::const_iterator it = m_mPurchaseProbs.begin();
		 it != m_mPurchaseProbs.end(); ++it)
	{
		const String& rShipType = it->first;
		const Real& rProb = it->second;

		if (!pShipsConf->GetShipType(rShipType)) return false;
		if (rProb < 0 || rProb > 1) return false;
	}

	// Validate sector links and probabilities
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	for (SectorLinkProbs::const_iterator it = m_mMoveProbs.begin();
		 it != m_mMoveProbs.end(); ++it)
	{
		const pair<String, String>& rLink = it->first;
		const Real& rProb = it->second;
		if ( !pScenarioConf->GetSectorType(rLink.first) ||
			 !pScenarioConf->GetSectorType(rLink.second) ) return false;
		if (rProb < 0 || rProb > 1) return false;
	}

	return true;
}

void AIPlayerModel::DoLearningUpdate(const AIPlayerModel* pObserved)
{
	// Zero learning rates are given to observed models when constructed.
	assert(pObserved);
	assert(pObserved->GetPurchaseLRate() == 0.0 &&
		   pObserved->GetMoveLRate() == 0.0);

	// Update each probability value using the learning rate
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	Real fOneMinusLRate = 1 - m_fPurchaseLRate;
	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const Real& rObservedProb = pObserved->GetPurchaseProb(*it);
		Real& rMyProb = m_mPurchaseProbs[*it];
		rMyProb = fOneMinusLRate*rMyProb + m_fPurchaseLRate*rObservedProb;
	}

	// Do the move probabilities
	const Real& rMoveLRate = GetMoveLRate();
	Real fOneMinusMoveLRate = 1 - rMoveLRate;

	for (SectorLinkProbs::iterator it = m_mMoveProbs.begin();
		 it != m_mMoveProbs.end(); ++it)
	{
		const pair<String, String>& rLink = it->first;
		Real& rProb = it->second;
		const Real& rObservedProb =
			pObserved->GetSectorLinkProb(rLink.first, rLink.second);
		rProb = fOneMinusMoveLRate*rProb + rMoveLRate*rObservedProb;
	}

	// TODO: Update group type stuff here once we're properly using player model learning
}

void AIPlayerModel::Reset()
{
	// Reset all the probabilities to zero
	for (StringRealMap::iterator it = m_mPurchaseProbs.begin();
		 it != m_mPurchaseProbs.end(); ++it)
	{
		Real& rProb = it->second;
		rProb = 0;
	}
	for (SectorLinkProbs::iterator it = m_mMoveProbs.begin();
		 it != m_mMoveProbs.end(); ++it)
	{
		it->second = 0.0;
	}
}

const Real& AIPlayerModel::GetPurchaseProb(const String& rShipType) const
{
	StringRealMap::const_iterator it = m_mPurchaseProbs.find(rShipType);
	assert(it != m_mPurchaseProbs.end());
	return it->second;
}

const Real& AIPlayerModel::GetSaveProb() const
{
	return m_fSaveProb;
}

unsigned AIPlayerModel::GetMinGroupSize(const String& rShipType) const
{
	StringUIntMap::const_iterator it = m_mMinGroupSizes.find(rShipType);
	assert(it != m_mMinGroupSizes.end());
	return it->second;
}

unsigned AIPlayerModel::GetMaxGroupSize(const String& rShipType) const
{
	StringUIntMap::const_iterator it = m_mMaxGroupSizes.find(rShipType);
	assert(it != m_mMaxGroupSizes.end());
	return it->second;
}

const Real& AIPlayerModel::GetIdealGroupSize(const String& rShipType) const
{
	StringRealMap::const_iterator it = m_mIdealGroupSizes.find(rShipType);
	assert(it != m_mIdealGroupSizes.end());
	return it->second;
}

const Real& AIPlayerModel::GetGroupSizeLRate() const
{
	return m_fGroupSizeLRate;
}

Real AIPlayerModel::EstimatePurchaseProb(const String& rShipType, unsigned iGroupSize)
{
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	// Validate
	if (iGroupSize == 0)
		throw JFUtil::Exception("AIEvaluator", "GetGroupSizePreference",
			"Zero group size provided");
	if (!pShipsConf->GetShipType(rShipType))
		throw JFUtil::Exception("AIEvaluator", "GetGroupSizePreference",
			"Invalid ship type provided",  rShipType);

	// Get minimum, ideal and maximum values, returning 0 if iGroupSize is outside min/max
	unsigned iMinSize = GetMinGroupSize(rShipType);
	if (iGroupSize < iMinSize) return 0;
	unsigned iMaxSize = GetMaxGroupSize(rShipType);
	if (iGroupSize > iMaxSize) return 0;
	// If it's the ideal size, the probability is 1.
	Real fIdealSize = GetIdealGroupSize(rShipType);
	if (iGroupSize == fIdealSize) return 1;
	// Otherwise it interpolates between min/max probabilities in the config
	// NOTE: This uses the same config values as player models do; we may want to separate them at some point
	Real fMinProb = pAIConf->GetGroupSizeMinProbability();
	assert(fMinProb >= 0.0 && fMinProb <= 1.0);
	Real fMaxProb = pAIConf->GetGroupSizeMaxProbability();
	assert(fMaxProb >= 0.0 && fMaxProb <= 1.0);

	// Before we interpolate, check if iGroupSize is equal to min or max
	if (iGroupSize == iMinSize) return fMinProb;
	else if (iGroupSize == iMaxSize) return fMaxProb;

	// Case A: group size < ideal size
	if (iGroupSize < fIdealSize)
	{
		if (fMinProb == 1.0) return 1.0;

		assert(fIdealSize > iMinSize); // if idealsize == minsize then groupsize < minsize, which we checked
		Real fMaxDiff = fIdealSize - (Real)iMinSize; // Greater than 0
		assert(fMaxDiff > 0); // Just to be sure
		Real fDiff = (Real)iGroupSize - (Real)iMinSize;
		assert(fDiff > 0.0 && fDiff < fMaxDiff);
		// Interpolate our value between fMinProb and 1
		return fMinProb + (1.0-fMinProb) * (fDiff/fMaxDiff);

	}
	else // Case B: group size > ideal size
	{
		if (fMaxProb == 1.0) return 1.0;

		assert(iGroupSize > fIdealSize);
		Real fMaxDiff = (Real)iMaxSize - fIdealSize;
		assert(fMaxDiff > 0);
		Real fDiff = (Real)iGroupSize - (Real)fIdealSize;
		assert(fDiff > 0.0 && fDiff < fMaxDiff);
		// Interpolate our value between 1 and fMaxProb
		return 1.0 - (1.0-fMaxProb) * (fDiff/fMaxDiff);
	}

}

const Real& AIPlayerModel::GetPurchaseLRate() const
{
	return m_fPurchaseLRate;
}

const Real& AIPlayerModel::GetMoveLRate() const
{
	return m_fMoveLRate;
}

const Real& AIPlayerModel::GetSectorLinkProb(const String& rFrom, const String& rTo) const
{
	SectorLinkProbs::const_iterator it = m_mMoveProbs.find(
		pair<String, String>(rFrom, rTo) );
	assert(it != m_mMoveProbs.end());
	return it->second;
}
