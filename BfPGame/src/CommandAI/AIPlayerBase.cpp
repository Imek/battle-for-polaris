#include <CommandAI/AIPlayerBase.h>

/**
 * AIPlayer code
 */

AIPlayer::AIPlayer(const String& rType, FactionController* pControls)
: m_strType(rType)
, m_pControls(pControls)
, m_pPrevState( new AbstractBfPState(pControls->GetObservedState()) )
, m_pLogicEngine(NULL)
, m_pEvaluator(NULL)
{
	assert(m_pControls);
	// Make sure the provided name is that of a given faction, and not the player.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(GetFactionName() != "Player");
	assert(m_pPrevState);

	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	const AIPlayerType* pAIType = pAIConf->GetAIPlayerType(m_strType);
	String strName = GetFactionName();
	if (!pAIType)
	{
		std::stringstream sout;
		sout << "Invalid AI player type \"" << m_strType
			 << "\" given for faction \"" << strName << "\".";
		throw JFUtil::Exception("AIPlayer", "AIPlayer", sout.str());
	}

	// First, set up the logic engine.
	const String& rLogicEngine = pAIType->GetLogicEngineType();
	m_pLogicEngine = AILogicEngineFactory::MakeLogicEngine(rLogicEngine, pAIType->GetInitialModel());
	assert(m_pLogicEngine);
	cout << "getting here" << endl;
	// Also set up the evaluator.
	const String& rEvaluatorType = pAIType->GetEvaluator();
	const String& rInitialValues = pAIType->GetInitialValues();
	m_pEvaluator = AIEvaluatorFactory::MakeEvaluator(rEvaluatorType, rInitialValues);
	assert(m_pEvaluator);

	// Give our evaluator to the controller, and tell it it can start deploying stuff.
	pControls->SetEvaluator(m_pEvaluator);
	pControls->DeployStartingGroups();

	// Now go through the config, making managers as per the config. (Factory for managers) (done already by factory?)
	const FactionType* pFactionType = pScenarioConf->GetFactionType( strName );
	if (!pFactionType)
	{
		std::stringstream sout;
		sout << "Invalid faction name \"" << GetFactionName() << "\".";
		throw JFUtil::Exception("AIPlayer", "AIPlayer", sout.str());
	}

	// Finally, make the managers.
	const StringVector& rManagerTypes = pAIType->GetManagers();
	for (StringVector::const_iterator it = rManagerTypes.begin();
		 it != rManagerTypes.end(); ++it)
	{
		m_vManagers.push_back(AIManagerFactory::MakeManager(*it, m_pEvaluator));
	}
}

AIPlayer::~AIPlayer()
{
	// Clear the action queue (freeing all the memory used by actions)
	ClearActionQueue();

	// clear all the other managed memory.
	delete m_pPrevState;
	delete m_pLogicEngine;
	delete m_pEvaluator;
	for (vector<AIManager*>::iterator it = m_vManagers.begin(); it != m_vManagers.end(); ++it)
	{
		delete *it;
	}


	// Memory for FactionControllers are managed by the WorldController
	//delete m_pControls;
}

void AIPlayer::Update(unsigned long iTime)
{
	// If we're dead, don't do anything.
	if (m_pControls->IsDead()) return;

	// Base AIPlayer update uses the logic engine to update the abstract state.
	// Do high-level AI update if it's a new tick (turn).
	// The next abstract state is made by getting the previous one and updating it with the new visible state.

	unsigned long iNewAge = m_pControls->GetObservedState()->GetAgeInTicks();
	if (!m_pPrevState || m_pPrevState->GetAgeTicks() < iNewAge)
	{
		//cout << "doing AI update" << endl;
		//cout << "state age: " << iNewAge << endl;

		DoTickUpdate();
	}

	// NOTE: The derived class should override this and put tick stuff in there
}

void AIPlayer::DoTickUpdate()
{
	// Update our state and do the logic engine update
	AbstractBfPState* pNewAbsState = new AbstractBfPState(*m_pPrevState);
	// Updates the basic visible aspects of the state without doing any implication/inference
	pNewAbsState->Update(m_pControls->GetObservedState());
	// TODO: Call the logic engine update once that's up and running
	//m_pLogicEngine->UpdateAbstractState(m_pPrevState, pNewAbsState);
	// Clear the events now that we're done with them.
	// TODO: Add AI events for ships warping and dying for use by the logic engine
	//m_pControls->GetAIEvents().Clear();

	// Finally, delete the previous state and update m_pPrevState.
	delete m_pPrevState;
	m_pPrevState = pNewAbsState;
}

void AIPlayer::ExecuteActions()
{
	// Go through our action queue in order, from highest value, executing each action.
	// Discard actions that conflict with those already executed (i.e. that have lower value)

	// Note that here we do purchase actions before anything else (so that the resource manager
	// can purchase and deploy in the same turn)

	for (ActionQueue::const_iterator it = m_mActionQueue.begin();
		 it != m_mActionQueue.end(); ++it)
	{
		const AIAction* pAction = it->second;
		if (pAction->GetType() != AAT_PURCHASE) continue;
		if (!pAction->CanExecute(m_pControls)) continue;
		// TODO: Once we have the JFUtil/JFGame logger implemented, change all the cerr/cout statements like this.
		cout << "executing action: " << pAction->GetString() << endl;
		pAction->Execute(m_pControls);
	}

	// Doesn't matter what order the other actions are done in
	for (ActionQueue::const_iterator it = m_mActionQueue.begin();
		 it != m_mActionQueue.end(); ++it)
	{
		const AIAction* pAction = it->second;
		if (pAction->GetType() == AAT_PURCHASE) continue; // Handled above
		if (!pAction->CanExecute(m_pControls)) continue;
		// TODO: Once we have the JFUtil/JFGame logger implemented, change all the cerr/cout statements like this.
		cout << "executing action: " << pAction->GetString() << endl;
		pAction->Execute(m_pControls);
	}

	ClearActionQueue();
}

void AIPlayer::ClearActionQueue()
{
	for (ActionQueue::iterator it = m_mActionQueue.begin(); it != m_mActionQueue.end(); ++it)
		delete it->second;

	m_mActionQueue.clear();
}

const AIPlayerType* AIPlayer::GetType() const
{
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	const AIPlayerType* pType = pAIConf->GetAIPlayerType(m_strType);
	assert(pType);
	return pType;
}
