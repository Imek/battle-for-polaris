#include <CommandAI/AIAbstractStateUnit.h>

#include <Config/BfPConfig.h>
#include <Config/BfPConfigScenario.h>

/**
 * UnitHistoryEntry public code
 */

UnitHistoryEntry::UnitHistoryEntry(unsigned long iTick, bool bPurchased,
	const String& rLocation)
: m_iTick(iTick)
, m_bPurchased(bPurchased)
, m_bObserved(true)
, m_fExistence(1)
, m_strObservedLoc(rLocation)
{
	// Set the probabilities for the observed unit.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	assert(find(rSectors.begin(), rSectors.end(), rLocation) != rSectors.end());

	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		m_mLocation[*it] = *it == rLocation ? 1.0 : 0.0;
	}

	#ifdef _DEBUG
		Validate();
	#endif

}

UnitHistoryEntry::UnitHistoryEntry(unsigned long iTick, bool bPurchased,
	const Real& rExistence, const StringRealMap& rLocation)
: m_iTick(iTick)
, m_bPurchased(bPurchased)
, m_bObserved(false)
, m_fExistence(rExistence)
, m_mLocation(rLocation)
, m_strObservedLoc("")
{

	#ifdef _DEBUG
		Validate();
	#endif
}

const Real& UnitHistoryEntry::GetLocProb(const String& rSector) const
{
	StringRealMap::const_iterator it = m_mLocation.find(rSector);
	assert(it != m_mLocation.end());
	return it->second;
}

// TODO: Delete once we're sure we won't be using the merging stuff
//void UnitHistoryEntry::MergeNewShip(const StringRealMap& rNewLocProbs)
//{
//	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
//
//	const StringVector& rSectorTypes = pScenarioConf->GetSectorTypes();
//
//	assert(rSectorTypes.size() == rNewLocProbs.size());
//	assert(rSectorTypes.size() == m_mLocation.size());
//	for (StringVector::const_iterator itSector = rSectorTypes.begin();
//		 itSector != rSectorTypes.end(); ++itSector)
//	{
//		const String& rSector = *itSector;
//		assert(m_mLocation.find(rSector) != m_mLocation.end() &&
//			   rNewLocProbs.find(rSector) != rNewLocProbs.end());
//
//		StringRealMap::const_iterator itToAdd = rNewLocProbs.find(rSector);
//		assert(itToAdd != rNewLocProbs.end());
//		m_mLocation[rSector] += itToAdd->second;
//	}
//
//	m_bPurchased = true;
//}

void UnitHistoryEntry::SetObservedLoc(bool bPurchased, const String& rSector)
{
	m_bPurchased = bPurchased;
	m_bObserved = true;
	m_strObservedLoc = rSector;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	// Update the probabilities (1.0 for rSector, 0.0 for others)
	assert( find(rSectors.begin(), rSectors.end(), rSector) != rSectors.end() );
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		m_mLocation[*it] = (*it == rSector) ? 1.0 : 0.0;
	}
	assert(m_mLocation.size() == rSectors.size());

}

String UnitHistoryEntry::GetObservedLoc() const
{
	// Only call this if observed!
	if (!m_bObserved)
		throw JFUtil::Exception("UnitHistoryEntry", "GetObservedLoc",
			"Tried to call GetObservedLoc when value was estimated!");
	// If observed, th en m_strObservedLoc must be set and its probability must be 1.
	assert(!m_strObservedLoc.empty() && m_strObservedLoc != "");
	StringRealMap::const_iterator it = m_mLocation.find(m_strObservedLoc);
	assert(it != m_mLocation.end());
	assert(it->second == 1);
	return m_strObservedLoc;
}

void UnitHistoryEntry::SetEstimatedLoc(bool bPurchased, const Real& fExistence, const StringRealMap& rLocation)
{
	assert(fExistence > 0 && fExistence < 1);

	m_bObserved = false;
	m_strObservedLoc = "";
	m_fExistence = fExistence;
	m_bPurchased = bPurchased;
	m_mLocation = rLocation;
	#ifdef _DEBUG
		Validate();
	#endif
}

void UnitHistoryEntry::Validate() const
{
	// TODO: Copy validate stuff here (plus the stuff for checking that observed means 1.0/0.0/0.0/etc)
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	if (m_mLocation.size() != rSectors.size())
		throw JFUtil::Exception("UnitHistoryEntry", "Validate", "Wrong number of entries in m_mLocation");
	Real fTotalProb = 0;
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		StringRealMap::const_iterator itEntry = m_mLocation.find(*it);
		if (itEntry == m_mLocation.end())
			throw JFUtil::Exception(
				"UnitHistoryEntry",
				"Validate", "Missing entry in m_mLocation");
		const Real& rProb = itEntry->second;
		if (m_bObserved && rProb != 1.0 && rProb != 0.0)
			throw JFUtil::Exception(
				"UnitHistoryEntry",
				"Validate", "Was observed, but value in m_mLocation was not 1 or 0.");
		fTotalProb += rProb;

	}

	if (fTotalProb != 1.0)
		throw JFUtil::Exception(
			"UnitHistoryEntry",
			"Validate", "Total m_mLocation probability wasn't 1.");

}

/**
 * AbstractUnitState public code
 */

// Constructor for known ship
AbstractUnitState::AbstractUnitState(
	unsigned long iTick, const String& rFaction,
	const BfPSectorState* pSector, unsigned iGroupID, unsigned iUnitID)
: m_pGroupID(new unsigned(iGroupID))
, m_iUnitID(iUnitID)
, m_strFaction(rFaction)
// The following two values get set when we call AddShip with the first ship.
, m_fHealth(1)
, m_iEarliestTick(iTick)
, m_iLatestTick(iTick)
, m_iKnownCount(0)
{
	assert(iGroupID >= 0);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(m_strFaction));

	// Update type and location (history is done in the function)
	vector<const ShipState*> vGroup;
	const ShipState* pLeader = pSector->GetShipGroup(rFaction, iGroupID, vGroup);

	assert(pLeader && !vGroup.empty());
	// I'm pretty certain vGroup should contain the leader too.
	assert(find(vGroup.begin(), vGroup.end(), pLeader) != vGroup.end());

	// Determine the type, health and count of this group from the individual ships.
	m_iKnownCount = vGroup.size();
	Real fHealth = 0;
	String strType = pLeader->GetShipType()->GetName(); // All must match the ship type!
	for (size_t i = 0; i < m_iKnownCount; ++i)
	{
		const ShipState* pState = vGroup[i];
		fHealth += pState->GetOverallCondition(false); // Overall condition excluding EP
		if (pState->GetShipType()->GetName() != strType)
			throw JFUtil::Exception("AbstractUnitState", "AbstractUnitState",
				"Found a ship group with inconsistent ship types");
	}
	fHealth /= (Real)m_iKnownCount;
	assert(fHealth >= 0 && fHealth <= 1);
	SetType(strType, m_iKnownCount); // 1.0 of strType, 0.0 of everything else
	assert(m_iKnownCount > 0);

	ObserveState(iTick, true, pSector->GetSectorType()->GetName());


	#ifdef _DEBUG
		Validate();
	#endif
}

// Constructor for hypothetical ship
AbstractUnitState::AbstractUnitState(
	unsigned long iTick, const String& rFaction, const Real& rExistence,
	const StringRealMap& rType, const UIntRealMap& rCount, const StringRealMap& rLocation, unsigned iUnitID)
: m_pGroupID(NULL)
, m_iUnitID(iUnitID)
, m_strFaction(rFaction)
// The following two values get set when we call AddShip with the first ship.
, m_fHealth(1)
, m_iEarliestTick(iTick)
, m_iLatestTick(iTick)
, m_iKnownCount(0)
{
	SetType(rType, rCount);
	EstimateState(iTick, true, rExistence, rLocation);

	Validate();
}

void AbstractUnitState::SetType(const String& rType, unsigned iCount)
{
	assert(iCount > 0);

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();

	assert( find(rShipTypes.begin(), rShipTypes.end(), rType) != rShipTypes.end() );
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		// 1.0 if it's rType, 0.0 otherwise.
		m_mType[*it] = (*it == rType) ? 1.0 : 0.0;
	}
	// This would fail if m_mType had invalid or duplicate values at some point
	assert(m_mType.size() == rShipTypes.size());

	// Now do the count. Just clear the map and set our value to 1.
	m_mCount.clear();
	m_mCount[iCount] = 1.0;
	m_iKnownCount = iCount;

}

void AbstractUnitState::SetType(const StringRealMap& rNewProbs, const UIntRealMap& rCount)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	assert(rNewProbs.size() == pShipsConf->GetShipTypes().size());

	m_mType = rNewProbs;
	m_mCount = rCount;

}

// TODO: Delete once we're sure we won't be using the merging stuff
//void AbstractUnitState::MergeNewShip(unsigned long iTick, const StringRealMap& rNewLocProbs, const UIntRealMap& rNewCountProbs)
//{
//	// Merge (add) probabilities into the entry for the given tick.
//	assert(iTick >= m_iEarliestTick && iTick <= m_iLatestTick);
//	UnitHistory::iterator itEntry = m_mHistory.find(iTick);
//	assert(itEntry != m_mHistory.end());
//	UnitHistoryEntry* pEntry = itEntry->second;
//	assert(pEntry);
//
//	pEntry->MergeNewShip(rNewLocProbs);
//
//}

const Real& AbstractUnitState::GetTypeProb(const String& rType) const
{
	StringRealMap::const_iterator it = m_mType.find(rType);
	assert(it != m_mType.end());
	return it->second;
}

const String& AbstractUnitState::GetKnownType() const
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	if (GetHypothetical())
		throw JFUtil::Exception("AbstractUnitState", "GetKnownType",
			"Tried to call when the ship was hypothetical");

	// If not hypothetical, then one prob must be 1 and all others must be 0.
	const StringVector& rTypes = pShipsConf->GetShipTypes();
	for (StringVector::const_iterator it = rTypes.begin();
		 it != rTypes.end(); ++it)
	{
		StringRealMap::const_iterator itEntry = m_mType.find(*it);
		assert(itEntry != m_mType.end());
		const Real& rProb = itEntry->second;
		if (rProb != 0)
		{
			assert(rProb == 1);
			return *it;
		}
	}

	throw JFUtil::Exception("AbstractUnitState", "GetKnownType",
		"Couldn't find a type with probability of 1");

}


Real AbstractUnitState::GetCountProb(unsigned iCount) const
{
	UIntRealMap::const_iterator it = m_mCount.find(iCount);
	if (it == m_mCount.end()) return 0;
	return it->second;
}

const unsigned& AbstractUnitState::GetKnownCount() const
{
	if (GetHypothetical())
		throw JFUtil::Exception("AbstractUnitState", "GetKnownCount",
			"Tried to call when the ship was hypothetical");
	assert(m_iKnownCount > 0);
	return m_iKnownCount;
}

unsigned long AbstractUnitState::GetEarliestTick() const
{
	assert(m_iEarliestTick <= m_iLatestTick);
	return m_iEarliestTick;
}

unsigned long AbstractUnitState::GetLatestTick() const
{
	assert(m_iEarliestTick <= m_iLatestTick);
	return m_iLatestTick;
}

const UnitHistoryEntry* AbstractUnitState::GetEntry(unsigned long iTick) const
{
	assert(iTick >= m_iEarliestTick && iTick <= m_iLatestTick);

	UnitHistory::const_iterator it = m_mHistory.find(iTick);
	assert(it != m_mHistory.end());
	return it->second;
}

const UnitHistoryEntry* AbstractUnitState::GetEntry() const
{
	return GetEntry(m_iLatestTick);
}

void AbstractUnitState::ObserveState(unsigned long iTick, bool bPurchased, const String& rSector)
{
	// Update the history entry (may exist or not..)
	UnitHistory::iterator itEntry = m_mHistory.find(iTick);
	UnitHistoryEntry* pEntry;
	if (itEntry == m_mHistory.end())
	{
		pEntry = new UnitHistoryEntry(
			iTick, bPurchased, rSector);
		m_mHistory[iTick] = pEntry;
	}
	else
	{
		pEntry = itEntry->second;
		pEntry->SetObservedLoc(bPurchased, rSector);
	}

	// Validate and update tick values.
	if (iTick < m_iEarliestTick)
		m_iEarliestTick = iTick;
	if (iTick > m_iLatestTick)
		m_iLatestTick = iTick;
	assert(m_iEarliestTick <= m_iLatestTick);

}

void AbstractUnitState::EstimateState(unsigned long iTick, bool bPurchased,
	const Real& rExistence, const StringRealMap& rNewProbs)
{
	// Update the history entry (may exist or not..)
	UnitHistory::iterator itEntry = m_mHistory.find(iTick);
	UnitHistoryEntry* pEntry;
	if (itEntry == m_mHistory.end())
	{
		pEntry = new UnitHistoryEntry(
			iTick, bPurchased, rExistence, rNewProbs);
		m_mHistory[iTick] = pEntry;
	}
	else
	{
		pEntry = itEntry->second;
		pEntry->SetEstimatedLoc(bPurchased, rExistence, rNewProbs);
	}

	// Validate and update tick values.
	if (iTick < m_iEarliestTick)
		m_iEarliestTick = iTick;
	if (iTick > m_iLatestTick)
	{
		m_iLatestTick = iTick;
	}
	assert(m_iEarliestTick <= m_iLatestTick);

}

void AbstractUnitState::Validate() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	if (!pScenarioConf->GetFactionType(m_strFaction))
		throw JFUtil::Exception("AbstractUnitState", "Validate", "Invalid faction name: " + m_strFaction);
	if (m_iEarliestTick > m_iLatestTick)
		throw JFUtil::Exception("AbstractUnitState", "Validate", "m_iEarliestTick > m_iLatestTick");
	if (m_fHealth < 0 || m_fHealth > 1)
		throw JFUtil::Exception("AbstractUnitState", "Validate", "m_fHealth < 0  or  m_fHealth > 1");

	if (m_mType.size() != pShipsConf->GetShipTypes().size())
		throw JFUtil::Exception("AbstractUnitState", "Validate", "Wrong number of entries in m_mType");
	Real fTotalProb = 0;
	for (StringRealMap::const_iterator it = m_mType.begin();
		 it != m_mType.end(); ++it)
	{
		const Real& rProb = it->second;
		fTotalProb += rProb;
//		if (iProb % pAIConf->GetAbstractPercentUnit() != 0)
//			return false;
	}
	if (fTotalProb != 1)
		throw JFUtil::Exception(
			"AbstractUnitState",
			"Validate", "Total type probability != 1");


	// Check the history maps; there should be a record for each turn between earliest and latest.
	unsigned long iSize = m_iLatestTick-m_iEarliestTick+1;
	if (m_mHistory.size() != iSize)
		throw JFUtil::Exception(
			"AbstractUnitState", "Validate",
			"m_mHistory had the wrong number of entries.");
	for (unsigned long iTick = m_iEarliestTick; iTick <= m_iLatestTick; ++iTick)
	{
		//cout << "check tick: " << iTick << endl;
		const UnitHistory::const_iterator itEntry = m_mHistory.find(iTick);
		if (itEntry == m_mHistory.end())
			throw JFUtil::Exception(
				"AbstractUnitState", "Validate",
				"m_mHistory had a missing entry (for a given tick between earliest and latest)");
		const UnitHistoryEntry* pEntry = itEntry->second;
		pEntry->Validate();
		//cout << "checked tick" << endl;
	}
	//cout << "checked ticks" << endl;


	// If we got here, it's valid.
}
