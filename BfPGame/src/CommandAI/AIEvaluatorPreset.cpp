#include <CommandAI/AIEvaluatorPreset.h>

AIPresetEvaluator::AIPresetEvaluator(const String& rType, const String& rInitialConfig)
: AIEvaluator(rType)
, BaseConfig(BfPConfig::AICFG_FILE)
{
	const ShipsConfig* pShips = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const StringVector& rShipTypes = pShips->GetShipTypes();

	String strKey;
	for (StringVector::const_iterator itOf = rShipTypes.begin();
		 itOf != rShipTypes.end(); ++itOf)
	{
		const String& rShipType = *itOf;
		// First get the group size info
		// Min/max/ideal group size for each ship type
		strKey = rInitialConfig + rShipType + "MinGroupSize";
		unsigned* pSize = GetValue<unsigned>(strKey);
		if (!pSize)
			throw JFUtil::Exception("AIPresetEvaluator", "AIPresetEvaluator",
				"Missing initial min group size value in config: " + strKey);
		m_mMinGroupSizes[rShipType] = *pSize;
		delete pSize;

		strKey = rInitialConfig + rShipType + "MaxGroupSize";
		pSize = GetValue<unsigned>(strKey);
		if (!pSize)
			throw JFUtil::Exception("AIPresetEvaluator", "AIPresetEvaluator",
				"Missing initial max group size value in config: " + strKey);
		m_mMaxGroupSizes[rShipType] = *pSize;
		delete pSize;

		strKey = rInitialConfig + rShipType + "IdealGroupSize";
		Real* pIdealSize = GetValue<Real>(strKey);
		if (!pIdealSize)
			throw JFUtil::Exception("AIPresetEvaluator", "AIPresetEvaluator",
				"Missing initial ideal group size value in config: " + strKey);
		m_mIdealGroupSizes[rShipType] = *pIdealSize;
		delete pIdealSize;

		if ( !(m_mMinGroupSizes[rShipType] <= m_mIdealGroupSizes[rShipType] &&
			   m_mIdealGroupSizes[rShipType] <= m_mMaxGroupSizes[rShipType]) )
			throw JFUtil::Exception("AIPresetEvaluator", "AIPresetEvaluator",
				"Invalid group size values (should be min<=ideal<=max) in player model: " + strKey);


		// For this ship type, get a value against every type
		for (StringVector::const_iterator itAgainst = rShipTypes.begin();
			 itAgainst != rShipTypes.end(); ++itAgainst)
		{
			const String& rAgainstType = *itAgainst;
			strKey = rInitialConfig + rShipType + rAgainstType + "Value";
			Real* pValue = GetValue<Real>(strKey);
			if (!pValue)
				throw JFUtil::Exception("AIPresetEvaluator", "AIPresetEvaluator",
								"Missing config value for ship pair: " + strKey);
			pair<String, String> oPair (rShipType, rAgainstType);
			m_mShipValues[oPair] = *pValue;
			delete pValue;
		}

	}
}

Real AIPresetEvaluator::GetShipValue(const String& rOfType, const String& rAgainstType) const
{
	pair<String, String> oPair (rOfType, rAgainstType);
	std::map< pair<String, String>, Real >::const_iterator it = m_mShipValues.find(oPair);
	if (it == m_mShipValues.end())
		throw JFUtil::Exception("AIPresetEvaluator", "GetShipValue",
			"Couldn't find value for type \"" + rOfType + "\" against \"" + rAgainstType + "\"");
	return it->second;
}

unsigned AIPresetEvaluator::GetMinGroupSize(const String& rShipType) const
{
	StringUIntMap::const_iterator it = m_mMinGroupSizes.find(rShipType);
	assert(it != m_mMinGroupSizes.end());
	return it->second;
}

unsigned AIPresetEvaluator::GetMaxGroupSize(const String& rShipType) const
{
	StringUIntMap::const_iterator it = m_mMaxGroupSizes.find(rShipType);
	assert(it != m_mMaxGroupSizes.end());
	return it->second;
}

const Real& AIPresetEvaluator::GetIdealGroupSize(const String& rShipType) const
{
	StringRealMap::const_iterator it = m_mIdealGroupSizes.find(rShipType);
	assert(it != m_mIdealGroupSizes.end());
	return it->second;
}


bool AIPresetEvaluator::Validate() const
{
	// TODO: Evaluate configuration (evaluator)
	return true;
}
