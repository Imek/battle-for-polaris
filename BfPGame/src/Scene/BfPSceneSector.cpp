#include <Scene/BfPSceneSector.h>

/**
 * BfPSceneSector public functions
 */

BfPSceneSector::BfPSceneSector(const String& rSector, const FactionController* pPlayer)
: GameScene(GSCT_SECTOR, rSector)
, m_pPlayer(pPlayer)
, m_bFlyingFlagship(false)
, m_pDustParticles(NULL)
{
	assert(m_pPlayer);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetSectorType(rSector));
}

BfPSceneSector::~BfPSceneSector()
{
	for (std::list<ParticleEffect*>::iterator it = m_lParticleEffects.begin();
		 it != m_lParticleEffects.end();)
	{
		delete *it;
		it = m_lParticleEffects.erase(it);
	}

	if (m_pDustParticles) DestroyScene();
}

void BfPSceneSector::CreateScene()
{
	assert(!m_pDustParticles);
	assert(m_pSceneNode);

	m_pOgre->GetLog()->logMessage("Creating sector scene...");

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Draw the static parts of the sector.
	const SectorType* pSectorType = GetSectorType();

	// Set up sector particles
	//cout << "Creating particles: " << pSectorType->GetDustParticlesConf() << " " << pSectorType->GetDustParticlesMat() << endl;
	Ogre::ParticleSystem* pDustParticles =
		m_pSceneMgr->createParticleSystem( GetName() + "_DustParticles", pSectorType->GetDustParticlesConf() );
	//pDustParticles->setMaterialName( pSectorType->GetDustParticlesMat() );
	//pDustParticles->addEmitter( "Ellipsoid" );
	m_pDustParticles = m_pSceneMgr->createSceneNode(GetName() + "_DustParticles_Node");
	m_pDustParticles->attachObject(pDustParticles);
	m_pSceneNode->addChild( m_pDustParticles );

	// Create markers for destination sectors
	const StringVector& rLinks = pSectorType->GetLinkedSectors();
	for (StringVector::const_iterator it = rLinks.begin();
		 it != rLinks.end(); ++it)
	{
		const String& rFrom = pSectorType->GetName();
		const String& rLinked = *it;

		Ogre::ManualObject* pLinkMarker =
			m_pSceneMgr->createManualObject( rFrom + "to" + rLinked + "_WarpPoint");
		DrawWarpPointMarker(rFrom, rLinked, pLinkMarker);
		m_pSceneNode->attachObject(pLinkMarker);
		// Keep track of them in this map so that they can be updated.
		m_mWarpPoints[rLinked] = pLinkMarker;
	}

	// Add all of the major/minor bodies (e.g. asteroids, stations...)
	const BodyVector& rBodies = pSectorType->GetBodies();
	for (BodyVector::const_iterator it = rBodies.begin(); it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const String& rBodyID = pBody->GetID();
		const Vector3& rBodyLoc = pBody->GetLocation();
		String strBodyName = GetName() + "_" + rBodyID;

		// Make a node for each object and add it to the scene
		Ogre::SceneNode* pNode = m_pSceneMgr->createSceneNode(strBodyName);

		//cout << "Creating body: " << vBodyNames[i] << " of type: " << vBodyTypes[i] << endl;
		const BodyType* pType = pScenarioConf->GetBodyType( pBody->GetType() );
		assert(pType);

		Ogre::Light* pBGLight = NULL;
		Ogre::SceneNode* pBGNode = NULL;

		if (pType->GetModel() != "NOMODEL")
		{
			Ogre::Entity* pBody =
				m_pSceneMgr->createEntity( strBodyName + "_Entity", pType->GetModel() );
			pNode->attachObject(pBody);
			pNode->scale(pType->GetScale(), pType->GetScale(), pType->GetScale());
			pNode->translate(rBodyLoc);

			// TODO: There's probably a better way to do this (camera attached to a scene node?)
			if (pType->IsBackground())
			{
				pBGNode = pNode;
			}
		}

		// Major bodies are tracked by markers in the GUI.
		if (pType->IsMajor())
		{
			Ogre::ManualObject* pBodyMarker =
				m_pSceneMgr->createManualObject(strBodyName + "_Marker");
			DrawBodyMarker(pSectorType->GetName(), rBodyID, pBodyMarker);
			pNode->attachObject(pBodyMarker);
			// Keep track of them in this map so that they can be updated.
			m_mMajorBodies[rBodyID] = pNode;
		}

		// Set up the light, if it's not all zero (i.e. off) in the config
		const Vector3& vecDiffuse = pType->GetLightDiffuse();
		const Vector3& vecSpecular = pType->GetLightSpecular();
		if (vecDiffuse != Vector3::ZERO && vecSpecular != Vector3::ZERO)
		{
			Ogre::Light* pLight = m_pSceneMgr->createLight(strBodyName + "_Light");
			// Set the colour from the config
			pLight->setDiffuseColour(vecDiffuse.x, vecDiffuse.y, vecDiffuse.z);
			pLight->setSpecularColour(vecSpecular.x, vecSpecular.y, vecSpecular.z);

			if (pType->IsBackground())
			{
				// We need to update the position of these lights relative to the camera
				pBGLight = pLight;
				pLight->setPosition(m_pOgre->GetCamera()->getPosition());
			}

			pLight->setPosition(pLight->getPosition() + rBodyLoc);

			// Store the light but keep it switched off until the scene is attached.
			pLight->setVisible(false);
			m_vAllLights.push_back(pLight);
		}

		// Add the node if it wasn't just a light
		if (pNode) m_pSceneNode->addChild( pNode );

		// Add the BG object entry - these'll still be NULL if it wasn't a background object.
		m_vBGLights.push_back(pBGLight);
		m_vBGNodes.push_back(pBGNode);
		m_vBGObjLocs.push_back(pBody->GetLocation());

	}

	m_pOgre->GetLog()->logMessage("... Done creating sector scene.");
}

void BfPSceneSector::UpdateScene(unsigned long iTime)
{
	// TODO: Create a "fog" effect of some kind to indicate if this sector is not visible.
	UpdateMarkers(iTime);
	UpdateActiveParticles(iTime);
	UpdateBackgroundObjects(iTime);
	UpdateEntities(iTime);
}

void BfPSceneSector::DestroyScene()
{
	assert(m_pSceneNode && m_pDustParticles);

	if (IsAttached()) Detach();

	// Destroy all of our attached objects first
	for (EntityList::iterator it = m_mEntitiesLastFrame.begin(); it != m_mEntitiesLastFrame.end(); ++it)
		DestroyAttachedObjects( it->second );
	m_mEntitiesLastFrame.clear();

	// Destroy the attached objects for every body node
	const SectorType* pSector = GetSectorType();
	const BodyVector& rBodies = pSector->GetBodies();
	for (BodyVector::const_iterator it = rBodies.begin(); it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const String& rBodyID = pBody->GetID();
		String strBodyName = GetName() + "_" + rBodyID;
		Ogre::SceneNode* pNode = (Ogre::SceneNode*)m_pSceneNode->getChild(strBodyName);
		DestroyAttachedObjects(pNode); // pNode itself is destroyed below
	}
	m_mMajorBodies.clear();
	m_vBGNodes.clear();

	// Destroy our particles
	m_pSceneMgr->destroyParticleSystem(GetName() + "_DustParticles");
	m_pSceneMgr->destroySceneNode(m_pDustParticles);
	m_pDustParticles = NULL;

	// NOTE: If we find a memory leak, it may be because we're failing to properly destroy something around here somewhere
	DestroyAttachedObjects(m_pSceneNode);
	m_pSceneNode->removeAndDestroyAllChildren();
	// The GameScene destructor handles destroying the node itself
	//m_pSceneMgr->destroySceneNode(m_pSceneNode);
	//m_pSceneNode = NULL;

	// Dispose of all the point lights
	for (std::vector<Ogre::Light*>::iterator itLight = m_vAllLights.begin();
		 itLight != m_vAllLights.end(); ++itLight)
	{
		Ogre::Light* pLight = *itLight;
		assert(pLight);
		m_pSceneMgr->destroyLight(pLight);
	}
	m_vAllLights.clear();

	// These were already disposed of (bodies), so we just clear the lists
	m_vBGLights.clear();
	m_vBGObjLocs.clear();

}

void BfPSceneSector::Attach()
{
	GameScene::Attach();

	// Set up the skybox.
	const SectorType* pSectorType = GetSectorType();
	m_pSceneMgr->setSkyBox( true, pSectorType->GetStarTexture() );

	// Set up ambient light for this sector.
	const Vector3& rAmbient = pSectorType->GetAmbientColour();
	m_pSceneMgr->setAmbientLight(Ogre::ColourValue(rAmbient.x, rAmbient.y, rAmbient.z));

	// Set up the point lights.
	for (std::vector<Ogre::Light*>::iterator itLight = m_vAllLights.begin();
		 itLight != m_vAllLights.end(); ++itLight)
	{
		Ogre::Light* pLight = *itLight;
		assert(pLight);
		pLight->setVisible(true);
	}
}


void BfPSceneSector::Detach()
{
	GameScene::Detach();

	// Disable the skybox and lights
	m_pSceneMgr->setSkyBox(false, "default");

	for (std::vector<Ogre::Light*>::iterator itLight = m_vAllLights.begin();
		 itLight != m_vAllLights.end(); ++itLight)
	{
		Ogre::Light* pLight = *itLight;
		assert(pLight);
		pLight->setVisible(false);
	}
}

const SectorType* BfPSceneSector::GetSectorType() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetSectorType(GetName()));
	return pScenarioConf->GetSectorType(GetName());
}

/**
 * BfPSceneSector private functions
 */

Ogre::SceneNode* BfPSceneSector::GetNodeForEntity(String strType, unsigned iID)
{
	EntityID oID (strType, iID);
	for (EntityList::const_iterator it = m_mEntitiesLastFrame.begin();
		 it != m_mEntitiesLastFrame.end(); ++it)
	{
		if (it->first == oID) return it->second;
	}

	return NULL;
}

void BfPSceneSector::UpdateEntities(unsigned long iTime)
{
	// Nothing to do if the sector isn't visible, as we can't see the state.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const BfPSectorState* pSector = m_pPlayer->GetObservedState()->ObserveSector(GetName());


	EntityList mRemaining (m_mEntitiesLastFrame);
	// Only draw entities if visible; otherwise, everything else is removed.
	if (pSector)
	{
		// Go through each node in the state, see if it needs to be added/removed, and update it.
		const ObjList& rvShips = pSector->GetObjectList("SHIP");
		// For each ship in the state..
		for (ObjList::const_iterator it = rvShips.begin(); it != rvShips.end(); ++it)
		{
			const EntityState* pEntity = (const EntityState*)(*it);
			assert(pEntity->GetType() == "SHIP");
			EntityID oID ("SHIP", pEntity->GetID());
			const ShipState* pShip = (const ShipState*)pEntity;

			// Before we do anything, handle any particle effects that need doing.
			UpdateEntityParticles(iTime, pShip);
			UpdateShipParticles(iTime, pShip);

			Ogre::SceneNode* pNode = GetNodeForEntity(oID.first, oID.second); // Could be null
			if (pNode) // Exists in OGRE, so just update it.
			{
				// This node is in the state
				mRemaining.erase(mRemaining.find(oID));
				// Also handle any labels that need updating.
				UpdateShipLabel(iTime, pShip);
			}
			else // Doesn't exist in OGRE, so add and update it.
			{
				//cout << "ADD SHIP TO " << GetName() << endl;

				const ShipType* pType = pShip->GetShipType();
				std::stringstream sout;
				sout << GetName() << "#Ship-" << pEntity->GetID();
				String strName = sout.str();
				// Node is new and needs creating
				//cout << "strName: " << strName << endl;
				if (!m_pSceneMgr->hasSceneNode(strName))
				{
					//cout << "NEW NODE" << endl;
					pNode = m_pSceneMgr->createSceneNode(strName);
					// We shouldn't need to check if they already exist (any more..)

					Ogre::ManualObject* pShipMarker =
						m_pSceneMgr->createManualObject(strName + "_Marker");
					pShipMarker->clear();
					//DrawShipMarker(pShip, pShipMarker);
					pNode->attachObject(pShipMarker);

					Ogre::Entity* pShipEntity =
						m_pSceneMgr->createEntity( strName + "_Entity", pType->GetModel() );
					pNode->attachObject(pShipEntity);
					pNode->scale(pType->GetScale(), pType->GetScale(), pType->GetScale());

					if (pShip->GetFaction() == m_pPlayer->GetFactionName())
					{
						// Set up the label for group leaders (visibility is managed in the update)
						std::stringstream sout;
						if (pShip->IsInFlagGroup()) sout << "^";
						sout << pShip->GetGroupID();
						if (pShip->IsInFlagGroup()) sout << "^";
						Ogre::MovableText* pGroupIDLabel =
							new Ogre::MovableText(strName + "_Label", sout.str());
						//if (!pShip->IsGroupLeader())
							pGroupIDLabel->setVisible(false);
						pGroupIDLabel->setTextAlignment(
							Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE);
						pGroupIDLabel->setCharacterHeight(
							pScenarioConf->GetMapTextSize());
						pNode->attachObject(pGroupIDLabel);
					}

					m_pSceneNode->addChild( pNode );
				}
				else // The node may already exist if we were here before and the sector wasn't updated
				{
					//cout << "ALREADY EXISTS" << endl;
					pNode = m_pSceneMgr->getSceneNode(strName);
				}
				//cout << "DONE ADDING SHIP" << endl;
				assert(pNode); // not null

				//cout << "ADD " << strName << " TO " << GetName() << endl;
				m_mEntitiesLastFrame[oID] = pNode;
			}

			pNode->setOrientation(pEntity->GetOrientation());
			pNode->setPosition(pEntity->GetPosition());

		}

		// Do the same for projectiles.
		const ObjList& rvProjs = pSector->GetObjectList("PROJECTILE");
		// For each projectile in the state..
		for (ObjList::const_iterator it = rvProjs.begin(); it != rvProjs.end(); ++it)
		{
			const EntityState* pEntity = (const EntityState*)(*it);
			assert(pEntity->GetType() == "PROJECTILE");
			EntityID oID ("PROJECTILE", pEntity->GetID());
			const ProjectileState* pProj = (const ProjectileState*)pEntity;

			// Before we do anything, handle any particle effects that need doing.
			UpdateEntityParticles(iTime, pProj);

			Ogre::SceneNode* pNode = GetNodeForEntity(oID.first, oID.second); // Could be null
			if (pNode) // Exists in OGRE, so just update it.
			{
				// This node is in the state
				mRemaining.erase(mRemaining.find(oID));
			}
			else // Doesn't exist in OGRE, so add and update it.
			{
				const WeaponType* pType = pProj->GetWeaponType();
				std::stringstream sout;
				sout << GetName() << "#Projectile-" << pProj->GetID();
				String strName = sout.str();
				pNode = m_pSceneMgr->createSceneNode(strName + "_Node");
				// We only create the entity if the "NoModel" flag isn't set in the type.
				if (!pType->GetNoModel())
				{
					Ogre::Entity* pProjEntity =
						m_pSceneMgr->createEntity( strName + "_Entity", pType->GetModel() );
					pNode->attachObject(pProjEntity);
				}
				pNode->scale(pType->GetScale(), pType->GetScale(), pType->GetScale());
				//pNode->scale(pType->GetScale(), pType->GetScale(), pType->GetScale());
				m_pSceneNode->addChild( pNode );
				m_mEntitiesLastFrame[oID] = pNode;
			}

			pNode->setOrientation(pEntity->GetOrientation());
			pNode->setPosition(pEntity->GetPosition());

		}
	} // Finished drawing entities in a visible sector


	// Remaining entities were in OGRE but not the game state; that or the state was not visible.
	// In either case, they need removing from OGRE.
	for (EntityList::iterator it = mRemaining.begin(); it != mRemaining.end(); ++it)
	{
		std::stringstream sout;
		EntityID oID = it->first;
		Ogre::SceneNode* pNode = it->second;
		assert(pNode);
		DestroyAttachedObjects(pNode);
		//cout << "REMOVE " << pNode->getName() << " FROM " << GetName() << endl;
		m_pSceneNode->removeAndDestroyChild(pNode->getName());
		m_mEntitiesLastFrame.erase(m_mEntitiesLastFrame.find(oID));
	}

}

void BfPSceneSector::UpdateEntityParticles(unsigned long iTime, const EntityState* pState)
{
	const EntityType* pType = pState->GetEntityType();
	const BfPSectorState* pSector = m_pPlayer->GetObservedState()->ObserveSector(GetName());
	assert(pSector);

	static std::stringstream sout;

	// Death/hit explosion particles
	sout << GetName() << "#" << pState->GetType() << pState->GetID() << "DeathExplosion_";
	String strExplodeName = sout.str();
	sout.str(""); sout.clear();

	if (pState->GetExplodeTrigger() && !m_pSceneMgr->hasParticleSystem(strExplodeName + "Particles"))
	{
		ParticleEffect* pEffect = new ParticleEffect( strExplodeName,
			PET_EXPLOSION, pType->GetExplosionParticles(),
			new unsigned long(pType->GetExplodeDuration())
			);

		pEffect->SetPosition(pState->GetPosition());
		pEffect->SetOrientation(pState->GetOrientation());
		if (pState->GetType() == "SHIP") cout << "set particle velocity: " << pState->GetVel() << endl;
		pEffect->SetVelocity(pState->GetVel());
		pEffect->Attach(m_pSceneNode);

		m_lParticleEffects.push_back(pEffect);
	}

	// Engine/trail particles
	sout << GetName() << "#" << pType->GetTrailParticles() <<  "#" << pState->GetType() << pState->GetID() << "TrailEffect_";
	String strTrailName = sout.str();
	sout.str(""); sout.clear();
	// Create it if it doesn't already exist.
	if (!m_pSceneMgr->hasParticleSystem(strTrailName + "Particles"))
	{
		ParticleEffect* pEffect = new EngineParticles (
			strTrailName, PET_TRAIL, pType->GetTrailParticles(),
			pState->GetType(), pState->GetID() );

		if (pState->GetType() == "PROJECTILE")
		{
			const WeaponType* pWeap = (const WeaponType*)pType;
			pEffect->SetDuration(pWeap->GetLifetime());
		}

		pEffect->SetPosition(pState->GetPosition());
		pEffect->SetOrientation(pState->GetOrientation());
		pEffect->Attach(m_pSceneNode);

		m_lParticleEffects.push_back(pEffect);
	}

}

void BfPSceneSector::UpdateShipParticles(unsigned long iTime, const ShipState* pShip)
{
	const ShipType* pShipType = pShip->GetShipType();

	// Warping particles
	if (pShip->GetWarpTrigger())
	{
		std::stringstream sout;
		sout << GetName() << "#Ship" << pShip->GetID() << "WarpDriveEffect_";
		ParticleEffect* pEffect = new EngineParticles( sout.str(), PET_WARP,
			pShipType->GetWarpParticles(), "SHIP", pShip->GetID() );
		pEffect->SetDuration(pShipType->GetWarpDuration());
		sout.str(""); sout.clear();

		pEffect->SetPosition(pShip->GetPosition());
		pEffect->SetOrientation(pShip->GetOrientation());
		pEffect->Attach(m_pSceneNode);

		m_lParticleEffects.push_back(pEffect);
	}

}

void BfPSceneSector::UpdateShipLabel(unsigned long iTime, const ShipState* pShip)
{
	//assert(m_pPlayer->HasShips());
	if (pShip->GetFaction() != m_pPlayer->GetFactionName())
		return;

	// Get the label for this ship
	std::stringstream sout;
	sout << GetName() << "#Ship-" << pShip->GetID();
	String strName = sout.str();
	sout.str(""); sout.clear();

	assert(m_pSceneMgr->hasSceneNode(strName));
	Ogre::SceneNode* pNode = m_pSceneMgr->getSceneNode(strName);
	Ogre::MovableText* pLabel = (Ogre::MovableText*)
		pNode->getAttachedObject(strName + "_Label");

	// Update the label (ship leaders and flag groups can be reassigned, so it needs updating)
	// Y dist from camera - if negative, the ship is behind the camera.
	const Vector3& rCamPos = m_pOgre->GetCamera()->getPosition();
	const Vector3& rShipPos = pShip->GetPosition();
	Vector3 vecCamDir = rShipPos - rCamPos;
	vecCamDir.normalise();
	Real fYDist = (rShipPos - rCamPos).dotProduct(vecCamDir);
	vecCamDir = pShip->GetOrientation().Inverse() * vecCamDir;

	pLabel->setVisible(
		!m_bFlyingFlagship && !pShip->IsDead() && !pShip->GetWarping() &&
		pShip->IsGroupLeader() && fYDist > 0);
	if (!pLabel->getVisible())
		return;

	if (pShip->IsInFlagGroup()) sout << "^";
	sout << pShip->GetGroupID();
	if (pShip->IsInFlagGroup()) sout << "^";
	pLabel->setCaption(sout.str());


}

void BfPSceneSector::UpdateActiveParticles(unsigned long iTime)
{
	static Vector3 vecCalc;

	Real fTime = (Real)iTime;

	// Update dust particles
	const Vector3& rCamPos = m_pOgre->GetCamera()->getPosition();
	m_pDustParticles->setPosition(rCamPos);

	// Update other particles, removing them if they're expired
	for (std::list<ParticleEffect*>::iterator it = m_lParticleEffects.begin();
		 it != m_lParticleEffects.end(); )
	{
		ParticleEffect* pEffect = *it;
		pEffect->Update(iTime);
		const BfPSectorState* pSector =
			m_pPlayer->GetObservedState()->ObserveSector(GetName());

		// An engine needs to adjust its intensity depending on the ship's state.
		Real fIntensity = 0;
		PARTICLE_EFFECT_TYPE iType = pEffect->GetType();
		if (iType == PET_TRAIL || iType == PET_WARP)
		{
			EngineParticles* pEngine = (EngineParticles*)pEffect;
			if (pSector)
			{
				//cout << "get ship state: " << pEngine->GetEntityID() << " from " << GetName() << endl;
				const EntityState* pEntity = pEngine->GetState(pSector);
				if (pEntity)
				{
					// Trail particles for ships alter their intensity based on that ship's thrust
					if (iType == PET_TRAIL && pEntity->GetType() == "SHIP")
					{
						const ShipState* pShip = (const ShipState*)pEntity;
						fIntensity = pShip->GetThrust() / pShip->GetShipType()->GetMaxThrust();
					}
					else fIntensity = 1;
					//cout << "set engine intensity: " << fIntensity << endl;
					pEngine->SetPosition(pEntity->GetPosition());
					pEngine->SetOrientation(pEntity->GetOrientation());
				}
			}

			pEngine->SetIntensity(fIntensity); // Turn off engine particles when the sector or ship is no longer visible

		}
		else if (pEffect->GetVelocity() != Vector3::ZERO) // Some other particle type; translate all particles by the stored relative velocity.
		{
			vecCalc = pEffect->GetVelocity() * fTime;
			pEffect->TranslateAllParticles(vecCalc);
		}

		if (pEffect->GetFinished())
		{
			pEffect->Detach();
			it = m_lParticleEffects.erase(it);
			delete pEffect;
		}
		else
			++it;
	}
}

void BfPSceneSector::UpdateBackgroundObjects(unsigned long iTime)
{
	const Vector3& rCamPos = m_pOgre->GetCamera()->getPosition();

	// Update background stuff
	size_t iNoBGObjs = m_vBGNodes.size();
	assert(m_vBGNodes.size() == m_vBGLights.size() &&
		   m_vBGLights.size() == m_vBGObjLocs.size());

	for (size_t i = 0; i < iNoBGObjs; ++i)
	{
		Ogre::SceneNode* pNode = m_vBGNodes[i];
		Ogre::Light* pLight = m_vBGLights[i];
		Ogre::Vector3& rLoc = m_vBGObjLocs[i];

		if (pNode)
			pNode->setPosition(rCamPos + rLoc);
		if (pLight)
			pLight->setPosition(rCamPos + rLoc);

	}

}

void BfPSceneSector::UpdateMarkers(unsigned long iTime)
{
	// Update static sector markers
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSector = GetSectorType();
	const String& rSector = pSector->GetName();

	// Update warp point markers
	const StringVector& rLinks = pSector->GetLinkedSectors();
	for (StringVector::const_iterator it = rLinks.begin();
		 it != rLinks.end(); ++it)
	{
		const String& rDest = *it;
		assert(m_mWarpPoints.find(rDest) != m_mWarpPoints.end());
		Ogre::ManualObject* pMarker = m_mWarpPoints[rDest];

		DrawWarpPointMarker(rSector, rDest, pMarker);
	}

	// Update body markers
	const BodyVector& rBodies = pSector->GetBodies();
	for (BodyVector::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const String& rBody = pBody->GetID();
		const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
		assert(pType);
		if (pType->IsMajor())
		{
			BodyList::iterator itBody = m_mMajorBodies.find(rBody);
			assert(itBody != m_mMajorBodies.end());
			Ogre::SceneNode* pNode = itBody->second;
			assert(pNode);

			std::stringstream sout;
			sout << GetName() << "_" << rBody << "_Marker";
			Ogre::ManualObject* pMarker =
				(Ogre::ManualObject*)pNode->getAttachedObject(sout.str());
			DrawBodyMarker(rSector, rBody, pMarker);
		}
	}


	// Update all the dynamic entities in the sector (if the sector is visible)
	const BfPSectorState* pSectorState =
		m_pPlayer->GetObservedState()->ObserveSector(GetName());
	if (!pSectorState) return;

	for (EntityList::iterator it = m_mEntitiesLastFrame.begin();
		 it != m_mEntitiesLastFrame.end(); ++it)
	{
		EntityID oID = it->first;
		const EntityState* pEntity = (const EntityState*)
			pSectorState->GetObjState(oID.first, oID.second);
		if (!pEntity) continue;
		Ogre::SceneNode* pNode = it->second;

		if (((ObjState*)pEntity)->GetType() == "SHIP")
		{
			const ShipState* pShip = (const ShipState*)pEntity;
			std::stringstream sout;
			sout << GetName() << "#Ship-" << pEntity->GetID() << "_Marker";
			Ogre::ManualObject* pMarker =
				(Ogre::ManualObject*)pNode->getAttachedObject(sout.str());
			DrawShipMarker(pShip, pMarker);
		}
	}

}




void BfPSceneSector::DrawShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const
{
	if (pShip->GetFaction() == m_pPlayer->GetFactionName())
		DrawFactionShipMarker(pShip, pMarker);
	else
		DrawEnemyShipMarker(pShip, pMarker);
}

void BfPSceneSector::DrawFactionShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const
{
	// If the faction has no ships left, then don't draw; this is a dead ship that hasn't been removed yet.
	if (!m_pPlayer->HasShips()) return;

	assert(pMarker);
	//assert(pShip->GetFaction() == m_pPlayer->GetFactionName());

	// We don't want to see the marker on our own ship if we're flying it.

	if (pShip->GetID() == m_pPlayer->GetFlagShip()->GetState()->GetID() &&
		m_bFlyingFlagship)
	{
		pMarker->clear();
		return;
	}

	// First, determine the material name to use (for the colour of the marker).
	std::stringstream sout;
	sout << pShip->GetFaction() << "/";

	unsigned iGroupID = pShip->GetGroupID();
	const unsigned* pSelectedGroup = m_pPlayer->GetSelectedGroup();
	bool bSelected = ( pSelectedGroup && *pSelectedGroup == pShip->GetGroupID() );
	if (bSelected)
	{
		sout << "Selected";
	}
	else
	{
		const ShipState* pFlagShip = m_pPlayer->GetFlagShip()->GetState();
		if (pFlagShip->GetGroupID() == iGroupID)
			sout << "Flag";
	}
	sout << "Group";
	if (pShip->IsGroupLeader())
		sout << "Leader";
	else
		sout << "Follower";

	String strMaterial = sout.str();

	// Now redraw the marker in the right position and with the right colour.
	// The marker for a ship is a diamond.
	const ShipType* pShipType = pShip->GetShipType();
	Real fRadius = pShipType->GetBoundingRadius() * 2;
	static Vector3 vecLeft;
	static Vector3 vecTop;
	static Vector3 vecRight;
	static Vector3 vecBottom;
	static Quaternion qRot;
	// We need to fix the orientation, as this is a child of the ship's node.
	Vector3 vecCameraUp = pShip->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getUp();
	Vector3 vecCameraRight = pShip->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getRight();

	vecLeft = -vecCameraRight * fRadius;
	vecTop = vecCameraUp * fRadius;
	vecRight = vecCameraRight * fRadius;
	vecBottom = -vecCameraUp * fRadius;

	pMarker->clear();
	pMarker->begin(strMaterial, Ogre::RenderOperation::OT_LINE_STRIP);
		pMarker->position(vecLeft);
		pMarker->position(vecTop);
		pMarker->position(vecRight);
		pMarker->position(vecBottom);
		pMarker->position(vecLeft);

	pMarker->end();

	// Draw waypoint/destination/target markers.

}

void BfPSceneSector::DrawEnemyShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const
{
	//cout << "updating marker " << pMarker->getName() << endl;
	//cout << ".." << endl;
	// NOTE: For now, just use cube markers like those for faction ships
	assert(pMarker);
	assert(pShip->GetFaction() != m_pPlayer->GetFactionName());

	// First, determine the material name to use (for the colour of the marker).
	std::stringstream sout;
	sout << pShip->GetFaction() << "/";

	bool bSelected = false;
	if (m_pPlayer->HasShips())
	{
		const GroupController* pMyFlagGroup = m_pPlayer->GetFlagShipGroup();
		bSelected = (
			pMyFlagGroup && pMyFlagGroup->GetLeader()->HasTargetShip() &&
			pMyFlagGroup->GetLeader()->GetTargetShip() == pShip->GetID() );
	}

	if (bSelected && m_bFlyingFlagship)
		sout << "Selected";
	else if (pShip->IsInFlagGroup())
		sout << "Flag";

	sout << "Group";
	if (pShip->IsGroupLeader())
		sout << "Leader";
	else
		sout << "Follower";

	String strMaterial = sout.str();

	// Now redraw the marker in the right position and with the right colour.
	// (For now it's a diamond like for friendly ships)
	const ShipType* pShipType = pShip->GetShipType();
	Real fRadius = pShipType->GetBoundingRadius() * 2;
	static Vector3 vecLeft;
	static Vector3 vecTop;
	static Vector3 vecRight;
	static Vector3 vecBottom;
	static Quaternion qRot;
	// We need to fix the orientation, as this is a child of the ship's node.
	Vector3 vecCameraUp = pShip->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getUp();
	Vector3 vecCameraRight = pShip->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getRight();

	vecLeft = -vecCameraRight * fRadius;
	vecTop = vecCameraUp * fRadius;
	vecRight = vecCameraRight * fRadius;
	vecBottom = -vecCameraUp * fRadius;

	//cout << pShip->GetID() << ": " << strMaterial << endl;

	pMarker->clear();
	pMarker->begin(strMaterial, Ogre::RenderOperation::OT_LINE_LIST);
		pMarker->position(vecLeft);
		pMarker->position(vecTop);

		pMarker->position(vecTop);
		pMarker->position(vecRight);

		pMarker->position(vecRight);
		pMarker->position(vecBottom);

		pMarker->position(vecBottom);
		pMarker->position(vecLeft);

		// If we have ships, we have a flag ship.
		AddAimPredictMarker(pShip, pMarker);

	pMarker->end();

	//cout << "done" << endl;
}

void BfPSceneSector::AddAimPredictMarker(const ShipState* pTarget, Ogre::ManualObject* pMarker) const
{
	static Vector3 vecTopLeft;
	static Vector3 vecBottomRight;
	static Vector3 vecBottomLeft;
	static Vector3 vecTopRight;
	static Vector3 vecPredictPos;

	assert(pTarget);
	assert(pMarker);
	// If we have no ships or we're not flying (i.e. not in chase cam mode), don't draw any aim marker.
	if (!m_pPlayer->HasShips() || !m_bFlyingFlagship) return;

	const ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	const ShipState* pPlayerState = pPlayerShip->GetState();
	assert(pPlayerState);
	// If we don't have an active weapon, we don't have a target, or pTarget isn't our target, don't draw a marker.
	if (!pPlayerState->HasActiveWeapon() || !pPlayerShip->HasTargetShip()) return;
	unsigned iTarget = pPlayerShip->GetTargetShip();
	if (iTarget != pTarget->GetID()) return;

	// NOTE: For now we only draw the aim predict marker for the first active weapon.
	// Should be fine, unless in the future we have custom weapon groups of mixed type
	// (in which case we may want to draw multiple markers colour-coded per weapon type)
	const std::vector<unsigned>& rWeaps = pPlayerState->GetActiveWeapons();
	assert(!rWeaps.empty()); // Should've returned above if it was
	const WeaponType* pWeap = pPlayerState->GetWeaponState(*rWeaps.begin()).GetType();

	// Draw a marker that shows where the target will be if we were aiming correctly and fired at this instant
	bool bInRange;
	// LeadTarget should populate our prediction vector with the correct values.
	UtilControl::LeadTarget(
		pPlayerState->GetPosition(), pPlayerState->GetVel(),
		pWeap->GetAccel(), pWeap->GetInitialSpeed(), pWeap->GetLifetime(),
		pTarget->GetPosition(), pTarget->GetVel(), &bInRange, &vecPredictPos);
	vecPredictPos -= pTarget->GetPosition(); // Relative to the ship whose marker is being drawn!
	cout << "vecPredictPos: " << vecPredictPos << endl;
	// No marker if not in range.
	if (!bInRange)
	{
		return;
	}

	const ShipType* pTargetType = pTarget->GetShipType();
	Real fDistSq = vecPredictPos.squaredLength(); // How far the predicted position is from the current
	Real fSize = pTargetType->GetBoundingRadius();
	if (fDistSq < fSize*fSize*2)
	{
		return; // Aim marker is so close to the target marker it's not needed
	}
	fSize /= 2;

	// Draw an X marker that is half the enemy marker itself, oriented to the camera.
	// (undo orientation of the ship itself as we're a child of its scene node)
	vecPredictPos = pTarget->GetOrientation().Inverse() * vecPredictPos;
	Vector3 vecCameraUp = pTarget->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getUp();
	Vector3 vecCameraRight = pTarget->GetOrientation().Inverse() *
		OgreFramework::GetInstance().GetCamera()->getRight();

	vecTopLeft = vecPredictPos;
	vecTopLeft += (-vecCameraRight+vecCameraUp) * fSize;
	vecBottomRight = vecPredictPos;
	vecBottomRight += (vecCameraRight-vecCameraUp) * fSize;
	vecBottomLeft = vecPredictPos;
	vecBottomLeft += (-vecCameraRight-vecCameraUp) * fSize;
	vecTopRight = vecPredictPos;
	vecTopRight += (vecCameraRight+vecCameraUp) * fSize;

	// When this is called we're already within the pMarker begin/end thing (OT_LINE_LIST)
	pMarker->position(vecTopLeft);
	pMarker->position(vecBottomRight);

	pMarker->position(vecBottomLeft);
	pMarker->position(vecTopRight);
}

void BfPSceneSector::DrawBodyMarker(
	const String& rSector, const String& rBody,
	Ogre::ManualObject* pMarker)
{
	assert(pMarker);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSectorType = GetSectorType();
	assert(pSectorType);
	const BodyInstance* pBodyInstance = pSectorType->GetBodyWithID(rBody);
	assert(pBodyInstance);

	std::stringstream sout;
	sout << "World/";

	// Determine whether it is selected or not - depends on whether the mode is ship or tactical.
	bool bSelected;
	const String* pSelectedSector;
	const String* pSelectedObject;
	if (!m_pPlayer->HasShips()) m_bFlyingFlagship = false;
	if (m_bFlyingFlagship)
	{
		const GroupController* pGroup = m_pPlayer->GetFlagShipGroup();
		// TODO: Group dest instead?
		pSelectedSector = &pGroup->GetLeader()->GetSector()->GetSectorType()->GetName();
		pSelectedObject = pGroup->GetDestBody();
	}
	else
	{
		pSelectedSector = &m_pPlayer->GetSelectedSector();
		pSelectedObject = m_pPlayer->GetSelectedObject();
	}

	bSelected = (pSelectedSector && pSelectedObject &&
		*pSelectedSector == rSector &&
		*pSelectedObject == rBody);
	if (bSelected) sout << "Selected";

	sout << "Body";

	String strMaterial = sout.str();

	// Don't draw the marker at all if set not to.
	if (!pScenarioConf->GetDrawUnselectedBodyMarkers() &&
		strMaterial == "World/Body")
	{
		pMarker->clear();
		return;
	}

	// Square marker for objects..
	// Now redraw the marker in the right position and with the right colour.
	const BodyType* pBodyType = pScenarioConf->GetBodyType(pBodyInstance->GetType());
	assert(pBodyType);
	Real fRadius = ( pBodyType->GetBoundingRadius() * 1.3 ) / pBodyType->GetScale();

	static Vector3 vecTopLeft;
	static Vector3 vecTopRight;
	static Vector3 vecBottomRight;
	static Vector3 vecBottomLeft;

	const Vector3& rCameraUp =
		OgreFramework::GetInstance().GetCamera()->getUp();
	const Vector3& rCameraRight =
		OgreFramework::GetInstance().GetCamera()->getRight();

	vecTopLeft = (rCameraUp-rCameraRight) * fRadius;
	vecTopRight = (rCameraUp+rCameraRight) * fRadius;
	vecBottomRight = (-rCameraUp+rCameraRight) * fRadius;
	vecBottomLeft = (-rCameraUp-rCameraRight) * fRadius;

	pMarker->clear();
	pMarker->begin(strMaterial, Ogre::RenderOperation::OT_LINE_STRIP);
		// Top front
		pMarker->position(vecTopLeft);
		pMarker->position(vecTopRight);
		pMarker->position(vecBottomRight);
		pMarker->position(vecBottomLeft);
		pMarker->position(vecTopLeft);
	pMarker->end();


}

void BfPSceneSector::DrawWarpPointMarker(
	const String& rSector, const String& rDestSector, Ogre::ManualObject* pMarker)
{
	assert(pMarker);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSectorType = GetSectorType();
	const Vector3& rPoint = pSectorType->GetWarpPointFromSector(rDestSector);

	const Vector3& rCameraUp =
		OgreFramework::GetInstance().GetCamera()->getUp();
	const Vector3& rCameraRight =
		OgreFramework::GetInstance().GetCamera()->getRight();
	Real fSize = pScenarioConf->GetMaxWarpDist()/2;

	Vector3 vecLeft = rPoint - rCameraRight*fSize;
	Vector3 vecRight = rPoint + rCameraRight*fSize;
	Vector3 vecBottom = rPoint - rCameraUp*fSize;
	Vector3 vecTop = rPoint + rCameraUp*fSize;

	pMarker->clear();

	// TODO: Come up with something better-looking than a big cross for warp points.
	bool bSelected = false;
	if (m_pPlayer->HasShips())
	{
		const String* pDestSector = m_pPlayer->GetFlagShipGroup()->GetDestSector();
		bSelected = (m_bFlyingFlagship && pDestSector && *pDestSector == rDestSector);
	}

	String strMaterial = bSelected ? "World/SelectedBody" : "World/Body";

	pMarker->begin(strMaterial, Ogre::RenderOperation::OT_LINE_LIST);
		pMarker->position(vecLeft);
		pMarker->position(vecRight);
		pMarker->position(vecTop);
		pMarker->position(vecBottom);
	pMarker->end();
}
