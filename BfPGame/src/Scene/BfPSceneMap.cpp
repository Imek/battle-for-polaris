#include <Scene/BfPSceneMap.h>

/**
 * BfPSceneMap public functions
 */

BfPSceneMap::BfPSceneMap(const FactionController* pPlayer)
: GameScene(GSCT_SECTOR, "MapRoot")
, m_pPlayer(pPlayer)
, m_bAbstract(false)
{
	assert(m_pPlayer);
}

BfPSceneMap::~BfPSceneMap()
{
	if (!m_mMapNodes.empty()) DestroyScene();
}

void BfPSceneMap::CreateScene()
{
	assert(m_mMapNodes.empty());

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const SectorType* pSectorType = pScenarioConf->GetSectorType(*it);
		assert(pSectorType);

		// Determine the material for the sphere based on whether it's visible
		String strMaterial = GetMapEntityMaterial(*it);

		// Draw the sector as a sphere
		std::stringstream sout;
		sout << "Map_" << *it;
		String strName = sout.str();
		Ogre::SceneNode* pSectorNode =
			m_pSceneMgr->createSceneNode(strName + "_Node");

		// Entity for the sphere
		Ogre::Entity* pSectorEntity =
			m_pSceneMgr->createEntity(strName + "_Entity", Ogre::SceneManager::PT_SPHERE);
		pSectorEntity->setMaterialName(strMaterial);
		pSectorNode->attachObject(pSectorEntity);
		Real fSize = pScenarioConf->GetMapNodeSize();
		pSectorNode->scale(fSize, fSize, fSize);
		pSectorNode->setPosition(pSectorType->GetMapRelPos());

		// Entity for the selection marker (separate node to be scaled thanks to PT_SPHERE)
		Ogre::SceneNode* pMarkerNode =
			m_pSceneMgr->createSceneNode(strName + "_MarkerNode");
		Ogre::Entity* pMarkerEntity =
			m_pSceneMgr->createEntity(strName + "_Marker", Ogre::SceneManager::PT_SPHERE);
		pMarkerEntity->setMaterialName("World/SelectedSector");
		pMarkerEntity->setVisible(false);
		pMarkerNode->attachObject(pMarkerEntity);
		pMarkerNode->scale(1.2, 1.2, 1.2);
		pSectorNode->addChild(pMarkerNode);

		sout << "_LinkTo_";
		// Draw lines to linked sectors
		const StringVector& rLinkedSectors = pSectorType->GetLinkedSectors();
		for (StringVector::const_iterator itLinked = rLinkedSectors.begin();
			 itLinked != rLinkedSectors.end(); ++itLinked)
		{
			const SectorType* pLinkedType = pScenarioConf->GetSectorType(*itLinked);
			assert(pLinkedType && pLinkedType != pSectorType);
			// The line is a child of this node, so the positions are relative.
			Vector3 vecRelPos = pLinkedType->GetMapRelPos() - pSectorType->GetMapRelPos();
			Ogre::ManualObject* pLinkLine = m_pSceneMgr->createManualObject(strName + *itLinked);
			pLinkLine->clear();
			pLinkLine->begin("Map/SectorLink", Ogre::RenderOperation::OT_LINE_LIST);
				pLinkLine->position(0,0,0);
				pLinkLine->position(vecRelPos*10);
			pLinkLine->end();

			pSectorNode->attachObject(pLinkLine);
		}
		sout.str(""); sout.clear();

		// Finally, do the text label.
		String strLabel;
		if (m_bAbstract)
			strLabel = GetAbstractSectorLabel(*it,
				AITestLogicEngine::GetInstance().GetState());
		else
			strLabel = GetMapSectorLabel(*it);


		Ogre::MovableText* pLabel = new Ogre::MovableText(strName + "_Text", strLabel);
		pLabel->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE); // Center horizontally and display above the node
		pLabel->setLocalTranslation( pSectorType->GetMapRelPos() + pSectorType->GetMapTextPos());
		pLabel->setCharacterHeight(pScenarioConf->GetMapTextSize());
		//msg->( 2.0f ); //msg->setAdditionalHeight( ei.getRadius() )
		m_pSceneNode->attachObject(pLabel);

		// Store the label and node for updates.
		m_mMapNodes[*it] = new MapNode(pSectorEntity, pMarkerEntity, pLabel);

		// Finally, add this node as a child to the map.
		m_pSceneNode->addChild(pSectorNode);
	}
}

void BfPSceneMap::UpdateScene(unsigned long iTime)
{
	assert(m_pSceneNode);
	assert(!m_mMapNodes.empty());

	//cout << "map scene update" << endl;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Look at every sector and update the node and sector.
	// Look at the label for every sector and update it if necessary.
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		MapNodeList::iterator itEntry = m_mMapNodes.find(*it);
		MapNode* pNode = itEntry->second;
		assert( itEntry != m_mMapNodes.end() );
		// Update the entity (sphere) colour
		Ogre::Entity* pEntity = pNode->m_pEntity;
		String strMaterial = GetMapEntityMaterial(*it);
		pEntity->setMaterialName(strMaterial);

		// Update the selection marker
		Ogre::Entity* pMarker = pNode->m_pMarker;
		pMarker->setVisible( m_pPlayer->GetSelectedSector() == *it );

		// Update the message
		Ogre::MovableText* pMsg = pNode->m_pLabel;
		assert(pMsg);
		const String& rOldLabel = pMsg->getCaption();

		String strLabel;
		if (m_bAbstract)
			strLabel = GetAbstractSectorLabel(*it,
				AITestLogicEngine::GetInstance().GetState());
		else
			strLabel = GetMapSectorLabel(*it);

		if (rOldLabel != strLabel)
		{
			pMsg->setCaption(strLabel);
		}
	}
}

void BfPSceneMap::DestroyScene()
{
	if (IsAttached()) Detach();

	// Destroy all the stuff attached to each sector's node
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		// Get the setor node and destroy all attached objects
		// (objects are attached to the marker nodes and the sector node itself)
		std::stringstream sout;
		sout << "Map_" << *it;
		String strName = sout.str();

		Ogre::SceneNode* pSectorNode = (Ogre::SceneNode*)
			m_pSceneNode->getChild(strName + "_Node");
		Ogre::SceneNode* pMarkerNode = (Ogre::SceneNode*)
			pSectorNode->getChild(strName + "_MarkerNode");
		DestroyAttachedObjects(pMarkerNode);

		DestroyAttachedObjects(pSectorNode);
		pSectorNode->removeAndDestroyAllChildren();
		m_pSceneMgr->destroySceneNode(pSectorNode);
	}

	m_pSceneNode->removeAndDestroyAllChildren();
	//m_pSceneNode->detachAllObjects();
	// Remove and destroy attached objects, as just calling detachAllObjects isn't enough.
	DestroyAttachedObjects(m_pSceneNode);

	for (MapNodeList::iterator it = m_mMapNodes.begin();
		 it != m_mMapNodes.end(); ++it)
	{
		delete it->second;
	}
	m_mMapNodes.clear();
}

void BfPSceneMap::Attach()
{
	GameScene::Attach();
	// Set up the skybox.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	m_pSceneMgr->setSkyBox( true, pScenarioConf->GetMapStarTexture() );
}


void BfPSceneMap::Detach()
{
	GameScene::Detach();
	// Disable the skybox
	m_pSceneMgr->setSkyBox(false, "default");
}


/**
 * BfPSceneMap private functions
 */

String BfPSceneMap::GetMapEntityMaterial(const String& rSector)
{
	const VisibleState* pState = m_pPlayer->GetObservedState();
	const BfPSectorState* pSectorState = pState->ObserveSector(rSector);
	String strMaterial;
	if (!pSectorState)
		strMaterial = "Map/UnseenSector";
	else if (!pSectorState->GetOwner())
		strMaterial = "Map/NeutralSector";
	else
		strMaterial = (*pSectorState->GetOwner()) + "/OwnedSector";

	return strMaterial;
}

String BfPSceneMap::GetMapSectorLabel(const String& rSector)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Make a text label with basic info on the sector
	const BfPSectorState* pSector = m_pPlayer->GetObservedState()->ObserveSector(rSector);
	const SectorType* pSectorType = pScenarioConf->GetSectorType(rSector);
	assert(pSectorType);

	std::stringstream sout;

	sout << "Sector: " << pSectorType->GetName() << endl;
	if (!pSector)
		sout << "Not visible.";
	else
	{
		const String* pOwner = pSector->GetOwner();
		sout << "Owner: " << (pOwner ? *pOwner : "None") << endl;
		// TODO: Summary if whose ships are here
		const String& rMyFaction = m_pPlayer->GetFactionName();
		const StringVector& rFactions = pScenarioConf->GetFactionNames();
		unsigned iNoShips = pSector->GetFactionShips(rMyFaction).size();
		sout << rMyFaction << " ships: " << iNoShips;
		for (StringVector::const_iterator it = rFactions.begin();
			 it != rFactions.end(); ++it)
		{
			if (*it != rMyFaction)
			{
				iNoShips = pSector->GetFactionShips(*it).size();
				if (iNoShips > 0)
					sout << endl << *it << " ships: " << iNoShips;
			}
		}
	}

	return sout.str();
}

String BfPSceneMap::GetAbstractSectorLabel(const String& rSector, const AbstractBfPState* pState)
{
	// TODO: Need to fix problem with it spitting out wrong letters when we use a newline
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	const String& rMyFaction = pState->GetMyFaction();

	const AbstractSectorState* pSector = pState->GetSector(rSector);
	assert(pSector);

	// Content on a label: Visible, ship count for each faction, ownership (definite or estimated)
	std::stringstream sout;
	sout << "Sector " << rSector;
	if (pSector->GetVisible())
	{
		unsigned iShipCount = pState->GetFactionVisibleUnitCount(rMyFaction, rSector);

		const String& rObservedOwner = pSector->GetObservedOwner();
		if (rObservedOwner == "NEUTRAL")
			sout << endl << "No owner";
		else
			sout << endl << "Owner: " << rObservedOwner;

		bool bEmpty = true;
		if (iShipCount > 0)
		{
			bEmpty = false;
			sout << endl << pState->GetMyFaction() << " ships: " << iShipCount;
		}
		for (StringVector::const_iterator it = rFactions.begin();
			 it != rFactions.end(); ++it)
		{
			if (*it == rMyFaction) continue;
			iShipCount = pState->GetFactionVisibleUnitCount(*it, rSector);
			if (iShipCount > 0)
			{
				bEmpty = false;
				sout << endl << *it << " ships: " << iShipCount;
			}
		}
		if (bEmpty)
			sout << endl << "No ships.";
	}
	else
	{
		// TODO: Would be a bit nicer to sort these from highest % to lowest.
		unsigned iOwnership = (unsigned)
			(pSector->GetOwnershipForFaction(rMyFaction) * 100.0);
		if (iOwnership > 0)
			sout << endl << rMyFaction << "? " << iOwnership << "%";

		for (StringVector::const_iterator it = rFactions.begin();
			 it != rFactions.end(); ++it)
		{
			if (*it == rMyFaction) continue;
			iOwnership = (unsigned)
				(pSector->GetOwnershipForFaction(*it) * 100.0);
			if (iOwnership > 0)
				sout << endl << *it << "? " << iOwnership << "%";
		}

		iOwnership = (unsigned)
			(pSector->GetNeutralOwnership() * 100.0);
		if (iOwnership > 0)
			sout << endl << "No owner? " << iOwnership << "%";

		// TODO: Estimated ships (known and non-hypothetical)
	}

	//cout << "string: " << sout.str() << endl;
	return sout.str();
}
