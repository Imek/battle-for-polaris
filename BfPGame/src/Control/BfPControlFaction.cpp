#include <Control/BfPControlFaction.h>

/**
 * FactionController code
 */

 /**
 * PUBLIC MEMBER FUNCTIONS
 */

FactionController::FactionController(BfPState* pBfPState, String strFaction)
: m_pMyFaction(NULL)
, m_pObservedState(new VisibleState(pBfPState, strFaction) )
, m_pEvaluator(NULL)
, m_pFlagShip(NULL)
, m_pSelectedGroup(NULL)
, m_pSelectedSector(NULL)
, m_pSelectedObject(NULL)
, m_pPurchaseTypes(NULL)
, m_pDeployTypes(NULL)
, m_bDead(false)
{
	m_pMyFaction = m_pObservedState->GetFactionState();
	assert(m_pMyFaction);
	// Get all of this faction's ships and make a controller for each
	// TODO: If we ever want to have factions become AI controlled part-way through a game,
	// we will need to uncomment the below & test MakeGroupControllers to ensure that it works fine.
	//MakeGroupControllers();
	//assert(m_pFlagShip);
}

FactionController::~FactionController()
{
	// Note that this doesn't delete the state contained within! (that's managed by the game itself)
	delete m_pObservedState;
	delete m_pSelectedGroup;
	delete m_pSelectedSector;
	delete m_pSelectedObject;
	delete m_pPurchaseTypes;
	delete m_pDeployTypes;

	// Only ShipController memory is managed here (FactionState is part of the external game state)
	// Note that m_pFlagShip is also contained within m_mMyGroups.
	for (GroupList::iterator it = m_mMyGroups.begin(); it != m_mMyGroups.end(); ++it)
	{
		GroupController* pShip = it->second;
		delete pShip;
	}
}

ShipController* FactionController::GetFlagShip()
{
	assert(m_pFlagShip && m_pFlagShip->GetLeader());
	return m_pFlagShip->GetLeader();
}

const ShipController* FactionController::GetFlagShip() const
{
	assert(m_pFlagShip && m_pFlagShip->GetLeader());
	return m_pFlagShip->GetLeader();
}

GroupController* FactionController::GetFlagShipGroup()
{
	assert(m_pFlagShip);
	return m_pFlagShip;
}

const GroupController* FactionController::GetFlagShipGroup() const
{
//	if (!m_pFlagShip)
//	{
//		cout << "Kaboom!" << endl;
//	}
	assert(m_pFlagShip);
	return m_pFlagShip;
}

bool FactionController::HasShips() const
{
	if (m_pFlagShip)
	{
		assert(!m_mMyGroups.empty());
		return true;
	}
	else
	{
		assert(m_mMyGroups.empty());
		return false;
	}
}

GroupController* FactionController::GetGroupWithID(unsigned iGroupID)
{
	GroupList::iterator it = m_mMyGroups.find(iGroupID);
	return (it == m_mMyGroups.end() ? NULL : it->second);
}

const GroupController* FactionController::GetGroupWithID(unsigned iGroupID) const
{
	GroupList::const_iterator it = m_mMyGroups.find(iGroupID);
	return (it == m_mMyGroups.end() ? NULL : it->second);
}

unsigned FactionController::GetNextGroupID() const
{
	unsigned iHighest = 0;
	for (GroupList::const_iterator it = m_mMyGroups.begin();
		 it != m_mMyGroups.end(); ++it)
	{
		if (it->first > iHighest)
			iHighest = it->first;
	}

	assert(iHighest >= 0); // I made a rather dumb mistake if this one fails
	return iHighest+1; // Group IDs start at 1 for user friendliness
}


bool FactionController::CanPurchaseGroup() const
{
	return (m_pPurchaseTypes &&
		m_pObservedState->CanPurchaseGroup(*m_pPurchaseTypes) && !m_bDead );
}

void FactionController::PurchaseGroup()
{
	// If tutorial is enabled and this is the player, throw a purchased event.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
	{
		const FactionType* pMyFaction = GetFactionState()->GetFactionType();
		if (pMyFaction->GetController() == "Player")
		{
			GlobalEvents::Get().PushEvent( new EventTrigger("PurchasedGroup") );
		}
	}

	// Purchase the currently-selected group type, blow up if nothing set.
	assert(m_pPurchaseTypes);
	assert( m_pObservedState->PurchaseGroup(*m_pPurchaseTypes) );
}


bool FactionController::CanDeployGroup() const
{
	if (!m_pDeployTypes || m_bDead) return false;

	const String& rSelectedSector = GetSelectedSector();
	return m_pObservedState->CanDeployGroup(
		*m_pDeployTypes, rSelectedSector,
		GetNextGroupID(), !m_pFlagShip);
}

void FactionController::DeployGroup()
{
	// If tutorial is enabled and this is the player, throw a deployed event.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
	{
		const FactionType* pMyFaction = GetFactionState()->GetFactionType();
		if (pMyFaction->GetController() == "Player")
		{
			GlobalEvents::Get().PushEvent( new EventTrigger("DeployedGroup") );
		}
	}

	// Must have checked that we can!
	assert(CanDeployGroup());
	const String& rSelectedSector = GetSelectedSector();
	bool bSuccess = DeployGroup(
		*m_pDeployTypes, rSelectedSector,
		GetNextGroupID(), !m_pFlagShip);
	assert(bSuccess);
}


bool FactionController::CanMoveGroup() const
{
	if (m_pSelectedGroup && (m_pSelectedSector || m_pSelectedObject) && !m_bDead ) return true;

	return false;
}

void FactionController::MoveGroup()
{
	// If tutorial is enabled and this is the player, throw a moved event.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
	{
		const FactionType* pMyFaction = GetFactionState()->GetFactionType();
		if (pMyFaction->GetController() == "Player")
		{
			GlobalEvents::Get().PushEvent( new EventTrigger("MovedGroup") );
		}
	}

	assert(CanMoveGroup());
	GroupController* pGroup = GetGroupWithID(*m_pSelectedGroup);
	if (!pGroup) return;
	assert(pGroup);
	const String& rSector = GetSelectedSector();
	if (m_pSelectedObject)
		pGroup->SetDestLocation(rSector, *m_pSelectedObject);
	else
		pGroup->SetDestLocation(rSector);
}

void FactionController::SetSelectedGroup(unsigned iGroupNo, bool bSelectSector)
{
	// If it doesn't exist, do nothing
	if (m_mMyGroups.find(iGroupNo) == m_mMyGroups.end()) return;

	if (bSelectSector && m_pSelectedGroup && *m_pSelectedGroup == iGroupNo)
	{
		// Selecting the same group twice selects that group's sector instead
		GroupController* pGroup = m_mMyGroups[iGroupNo];
		const String& rGroupSector = pGroup->GetSector()->GetSectorType()->GetName();
		SetSelectedSector(rGroupSector);
		return;
	}

	// Get here only if the group is changed
	if (!m_pSelectedGroup) m_pSelectedGroup = new unsigned();
	*m_pSelectedGroup = iGroupNo;
}

void FactionController::UnsetSelectedGroup()
{
	if (!m_pSelectedGroup) return;
	delete m_pSelectedGroup;
	m_pSelectedGroup = NULL;
}

const BfPSectorState* FactionController::GetSelectedSectorState() const
{
	if (!HasShips()) return NULL;
	// Returns NULL if unobserved; need to handle this (i.e. just show the sector with no entities) in OGRE
	if (m_pSelectedSector)
		return m_pObservedState->ObserveSector(*m_pSelectedSector);
	else
		return m_pFlagShip->GetSector();

}

const String& FactionController::GetSelectedSector() const
{
	// We always have a "selected" sector; if it's not specified, it's the location of the flagship.
	if (m_pSelectedSector)
		return *m_pSelectedSector;
	else if (HasShips())
		return m_pFlagShip->GetSector()->GetSectorType()->GetName();
	else
	{
		// No flag ship and no selected sector: choose our home sector.
		return m_pMyFaction->GetFactionType()->GetHomeSector();
	}
}

void FactionController::SetSelectedSector(const String& rSector)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert( pScenarioConf->GetSectorType(rSector) );

	if (m_pSelectedSector && rSector == *m_pSelectedSector)
		return;
	UnsetSelectedObject();
	if (!m_pSelectedSector)
		m_pSelectedSector = new String();
	*m_pSelectedSector = rSector;
}

void FactionController::UnsetSelectedSector()
{
	if (!m_pSelectedSector) return;
	delete m_pSelectedSector;
	m_pSelectedSector = NULL;
}

void FactionController::SetSelectedObject(const String& rObject)
{
	// Current sector is the one the flagship is in if no sector is selected.
	String strSector = (m_pSelectedSector ?
		*m_pSelectedSector :
		m_pFlagShip->GetSector()->GetSectorType()->GetName());

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSector = pScenarioConf->GetSectorType(strSector);
	assert(pSector && pSector->GetBodyWithID(rObject) );

	if (!m_pSelectedObject) m_pSelectedObject = new String();
	*m_pSelectedObject = rObject;
}

void FactionController::UnsetSelectedObject()
{
	if (!m_pSelectedObject) return;
	delete m_pSelectedObject;
	m_pSelectedObject = NULL;
}

void FactionController::SetPurchaseTypes(const StringVector& rTypes)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	assert( pShipsConf->ValidShipGroup(rTypes) );

	if (!m_pPurchaseTypes) m_pPurchaseTypes = new StringVector();
	*m_pPurchaseTypes = rTypes;
}

void FactionController::UnsetPurchaseTypes()
{
	if (!m_pPurchaseTypes) return;
	delete m_pPurchaseTypes;
	m_pPurchaseTypes = NULL;
}

void FactionController::SetDeployTypes(const StringVector& rTypes)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	assert( pShipsConf->ValidShipGroup(rTypes) );

	if (!m_pDeployTypes) m_pDeployTypes = new StringVector();
	*m_pDeployTypes = rTypes;
}

void FactionController::UnsetDeployTypes()
{
	if (!m_pDeployTypes) return;
	delete m_pDeployTypes;
	m_pDeployTypes = NULL;
}

void FactionController::KillFaction()
{
	// Destroy all of our ships
	for (GroupList::iterator it = m_mMyGroups.begin();
		 it != m_mMyGroups.end(); ++it)
	{
		GroupController* pGroup = it->second;
		pGroup->SelfDestruct();
	}

	// Set all of our sectors to neutral
	m_pObservedState->KillFaction( GetFactionName() );

	m_bDead = true;
}

void FactionController::DeployStartingGroups()
{
	// Should be in this state (called only once at the very start)
	assert(!m_pFlagShip && m_pMyFaction && m_pObservedState && m_pEvaluator);
	// This special function allows us to deploy
	m_pObservedState->DeployStartingGroups();
	// Create the group controllers
	MakeGroupControllers();
}

void FactionController::Update(unsigned long iTime)
{
	if (IsDead()) return;

	// Just update all of our groups; they handle low-level AI.
	for (GroupList::iterator it = m_mMyGroups.begin();
		 it != m_mMyGroups.end(); ++it)
	{
		GroupController* pGroup = it->second;
		assert(pGroup && !pGroup->IsDead()); // Should've been removed by CheckDestroyedGroups if it was!
		pGroup->Update(iTime);
	}

	// After checking, get rid of any destroyed groups and make sure we have a flag-group (if available)
	CheckDestroyedGroups();
	// Check the global event handler for faction death events pertaining to this faction
	CheckDeathEvents();

}

void FactionController::CheckDestroyedGroups()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	if (!m_pFlagShip)
	{
		assert(m_mMyGroups.empty());
		return;
	}
	assert(!m_mMyGroups.empty());

	// Check if our flag group is dead and make it NULL if so.
	bool bFlagShipDead = false;
	const String& rPrevSector = m_pFlagShip->GetSector()->GetSectorType()->GetName();
	if (m_pFlagShip->IsDead())
	{
		//cout << "FLAG GROUP DEAD" << endl;
		// We'll destroy it in the next for loop
		bFlagShipDead = true;
	}
	// Now check members and reassign/dispose/erase as necessary
	for (GroupList::iterator it = m_mMyGroups.begin();
		 it != m_mMyGroups.end();)
	{
		const unsigned& rID = it->first;
		GroupController* pGroup = it->second;
		if (pGroup->IsDead()) // Dead group: delete and remove
		{
			if (pGroup != m_pFlagShip)
				delete pGroup;
			// Increment the iterator then erase the entry we had selected.
			++it;
			m_mMyGroups.erase(rID);
		}
		else if (bFlagShipDead) // Found a new flag group
		{
			// This group isn't dead and we need a new flagship, so replace.
			//cout << "ASSIGN NEW FLAG GROUP" << endl;
			assert(pGroup != m_pFlagShip);
			delete m_pFlagShip;
			m_pFlagShip = pGroup;
			bFlagShipDead = false;
			m_pObservedState->SetMyFlagGroup(pGroup->GetGroupID());
			++it;
		}
		else
			++it;

	}

	if (bFlagShipDead) // No replacement found; must have no groups left
	{
		if (!m_pSelectedSector) SetSelectedSector(rPrevSector); // In case it's null

		// All ships lost? If we have no sectors left either, then we've been wiped out.
		if (pScenarioConf->GetWipeOutVictory())
		{
			//cout << GetFactionName() << " has no ships left. " << endl;
			if (m_pObservedState->FactionHasNoSectors(GetFactionName()))
			{
				//cout << "also has no sectors left!" << endl;
				// Create a death event
				EventFactionDeath* pEvent = new EventFactionDeath(
					FDT_WIPEDOUT, m_pFlagShip->GetKiller(), GetFactionName());
				GlobalEvents::Get().PushEvent(pEvent);
			}
			//cout << "but has sectors left.." << endl;
		}

		// Either way, dispose of it.
		delete m_pFlagShip;
		m_pFlagShip = NULL;
		assert(m_mMyGroups.empty());
		// Update the game state
		m_pObservedState->UnsetMyFlagShip();

	}
	else
	{
		// Otherwise, we must have a flag ship and at least one group in the list.
		assert(m_pFlagShip && !m_mMyGroups.empty());
	}



}

void FactionController::CheckDeathEvents()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Check the VisibleState for faction death (wiped our or home captured)
	m_pObservedState->CheckFactionDeath( GetFactionName() );

	EventHandler& rEvents = GlobalEvents::Get();

	// If WipeOut is enabled, catch any sector captured events that pertain to us, as we may have been wiped out.
	if (pScenarioConf->GetWipeOutVictory())
	{
		EventList lCaptureEvents = rEvents.PopEventsOfType("EV_CAPTURE");
		for (EventList::iterator it = lCaptureEvents.begin();
			 it != lCaptureEvents.end(); ++it)
		{
			EventSectorCaptured* pEvent = (EventSectorCaptured*)*it;
			assert(pEvent);

			// If that was our last sector and we have no ships left, we were wiped out
			// by whoever captured that last sector.
			if (m_pObservedState->FactionHasNoSectors(GetFactionName()) &&
				pEvent->GetCaptured() == GetFactionName() && !HasShips())
			{
				// Create a death event
				EventFactionDeath* pNewEvent = new EventFactionDeath(
					FDT_WIPEDOUT, pEvent->GetCapturer(), GetFactionName());
				rEvents.PushEvent(pNewEvent);
				delete pEvent;
			}
			else rEvents.PushEvent(pEvent); // put it back if it's not relevant

		}
	}

	// Finally, see if we've died.
	EventMessage* pDeathMessage = NULL;
	EventList lDeathEvents = rEvents.PopEventsOfType("EV_FACTIONDEATH");
	for (EventList::iterator it = lDeathEvents.begin(); it != lDeathEvents.end(); ++it)
	{
		EventFactionDeath* pDeathEvent = (EventFactionDeath*)*it;
		if (pDeathEvent->GetFactionKilled() == GetFactionName())
		{
			// A message event to show in the GUI
			pDeathMessage = new EventMessage(pDeathEvent->GetString());
			// Kill all our ships and set us as dead
			KillFaction();
		}
		// Either way put the event back; it needs to be handled again by the WorldController.
		rEvents.PushEvent(pDeathEvent);
	}

	if (pDeathMessage) rEvents.PushEvent(pDeathMessage);
}


/**
 * PRIVATE MEMBER FUNCTIONS
 */

GroupController* FactionController::DeployGroup(
	const StringVector& rTypes, const String& rSector, unsigned iGroupID, bool bFlagGroup)
{
	assert(m_pEvaluator);
	//cout << "rTypes size: " << rTypes.size() << endl;
	bool bSuccess = m_pObservedState->DeployGroup(rTypes, rSector, iGroupID, bFlagGroup);
	if (!bSuccess)
	{
		return NULL;
	}

	// Make the group controller for the new group
	const BfPSectorState* pSector = m_pObservedState->ObserveSector(rSector);
	vector<const ShipState*> vMembers;
	const ShipState* pLeaderState = pSector->GetShipGroup(GetFactionName(), iGroupID, vMembers);
	assert(pLeaderState);

	GroupController* pGroup = new GroupController(pSector, m_pObservedState, m_pEvaluator);
	ShipController* pLeader = new ShipController(pSector,
		pLeaderState, m_pObservedState->GetShipControls(pSector, pLeaderState));
	pGroup->SetLeader(pLeader);

	for (vector<const ShipState*>::const_iterator it = vMembers.begin();
		 it != vMembers.end(); ++it)
	{
		if (*it == pLeaderState) continue;
		ShipController* pFollower = new ShipController(pSector,
			*it, m_pObservedState->GetShipControls(pSector, *it));
		pGroup->AddFollower(pFollower);
	}

	// Assert that this group doesn't already exist (but that should've been checked already!) and add it to our map.
	assert( m_mMyGroups.find(iGroupID) == m_mMyGroups.end() );
	m_mMyGroups[iGroupID] = pGroup;

	// Finally, if this was the first group to be added then it's the flagship group.
	if (!m_pFlagShip)
	{
		if (IsPlayer()) pGroup->SetAutoPilot(false);
		m_pFlagShip = pGroup;

	}
	return pGroup;
}

void FactionController::MakeGroupControllers()
{
	assert(m_pEvaluator);
	// We only call this at the start, i.e. when there are no controllers..
	assert(m_mMyGroups.empty());
	assert(!m_pFlagShip);


	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rSectorNames = pScenarioConf->GetSectorTypes();

	for (StringVector::const_iterator itSector = rSectorNames.begin();
		 itSector != rSectorNames.end(); ++itSector)
	{
		// The sector has ships of ours in it if this doesn't return NULL.
		const BfPSectorState* pSector = m_pObservedState->ObserveSector(*itSector);
		if (pSector)// && m_pMyFaction->IsFlagShipIDSet())
		{
			vector<const ShipState*> vShips = pSector->GetFactionShips(GetFactionName());

			for (vector<const ShipState*>::iterator itShip = vShips.begin();
				 itShip != vShips.end(); ++itShip)
			{
				const ShipState* pShip = *itShip;

				ControlState* pShipControls =
					m_pObservedState->GetShipControls(pSector, pShip);
				ShipController* pController =
					new ShipController(pSector, pShip, pShipControls);

				GroupList::iterator it = m_mMyGroups.find(pShip->GetGroupID());
				GroupController* pGroup;
				if (it == m_mMyGroups.end())
				{
					pGroup = new GroupController(pSector, m_pObservedState, m_pEvaluator);
					m_mMyGroups[pShip->GetGroupID()] = pGroup;
				}
				else // Group exists
					pGroup = it->second;

				if (pShip->IsGroupLeader())
					pGroup->SetLeader(pController);
				else
					pGroup->AddFollower(pController);

				// If it's the flagship, add it.
				if (pShip->GetID() == m_pMyFaction->GetFlagShipID())
				{
					assert(!m_pFlagShip);
					m_pFlagShip = pGroup;
				}
			}
		}

	}

	// If we have groups, then we must have a flag ship.
	assert(m_mMyGroups.empty() || m_pFlagShip);
	// If we have a flag ship, then we must have groups.
	assert(!m_pFlagShip || !m_mMyGroups.empty());

	// (we could end this with no flag ship or groups, if it's the start of the game)
}

Vector3 FactionController::GetSectorColour(const String& rSector) const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	Vector3 vecColour;
	const BfPSectorState* pSectorState =
		GetObservedState()->ObserveSector(rSector);

	if (rSector != GetSelectedSector())
	{
		if (!pSectorState)
			vecColour = Vector3(0.3,0.3,0.3);
		else if (!pSectorState->GetOwner())
			vecColour = Vector3(0.5,0.5,0.5);
		else
		{
			const FactionType* pFaction =
				pScenarioConf->GetFactionType(*pSectorState->GetOwner());
			vecColour = pFaction->GetUnselectedColour();
		}
	}
	else // Selected
	{
		if (!pSectorState)
			vecColour = Vector3(0.7,0.7,0.7);
		else if (!pSectorState->GetOwner())
			vecColour = Vector3(0.9,0.9,0.9);
		else
		{
			const FactionType* pFaction =
				pScenarioConf->GetFactionType(*pSectorState->GetOwner());
			vecColour = pFaction->GetSelectedColour();
		}
	}
	return vecColour;
}
