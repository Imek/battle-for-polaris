#include <Control/BfPEvents.h>

/**
 * EventFactory code
 */

Event* EventFactory::CopyEvent(const Event* pEvent)
{
	const String& rType = pEvent->GetType();
	if (rType == "EV_MESSAGE")
		return new EventMessage( *(EventMessage*)pEvent );
	else if (rType == "EV_MESSAGEBOX")
		return new EventMessageBox( *(EventMessageBox*)pEvent );
	else if (rType == "EV_COL_SHIPOBJ")
		return new ShipObjectCollision( *(ShipObjectCollision*)pEvent );
	else if (rType == "EV_COL_SHIPSHIP")
		return new ShipShipCollision( *(ShipShipCollision*)pEvent );
	else if (rType == "EV_COL_SHIPPROJ")
		return new ShipProjectileCollision( *(ShipProjectileCollision*)pEvent );
	else if (rType == "EV_AI_SHIPKILLED")
		return new AIUnitKilledEvent( *(AIUnitKilledEvent*)pEvent );
	else if (rType == "EV_AI_SHIPWARPED")
		return new AIUnitWarpedEvent( *(AIUnitWarpedEvent*)pEvent );
	else if (rType == "EV_FACTIONDEATH")
		return new EventFactionDeath( *(EventFactionDeath*)pEvent );
	else if (rType == "EV_CAPTURE")
		return new EventSectorCaptured( *(EventSectorCaptured*)pEvent );
	else if (rType == "EV_TRIGGER")
		return new EventTrigger( *(EventTrigger*)pEvent );
	else if (rType == "EV_SETCONTROLSENABLED")
		return new EventSetControlsEnabled( *(EventSetControlsEnabled*)pEvent );
	else if (rType == "EV_SEQUENCE")
		return new EventSequence( *(EventSequence*)pEvent );
	else
		return NULL;

	// TODO: I may have missed some event types here.

}

EventHandler* EventFactory::CopyEventHandler(const EventHandler* pHandler)
{
	EventHandler* pCopy = new EventHandler();

	const EventList& rEvents = pHandler->GetEventList();
	for (EventList::const_iterator it = rEvents.begin();
	  it != rEvents.end(); ++it)
	{
		pCopy->PushEvent( CopyEvent(*it) );
	}

	return pCopy;
}


Event* EventFactory::MakeTriggerEvent(const EventConfig& rConf)
{
	const String& rPrefix = rConf.m_strPrefix;
	const BaseConfig* pConfFile = rConf.m_pConfig;

	Event* pToReturn = NULL;

	// TODO: Other event types
	String* pType = pConfFile->GetString(rPrefix + "Type");
	if (!pType)
		throw JFUtil::Exception("EventFactory", "MakeTriggerEvent",
			"Missing Type parameter in event", rPrefix);
	if (*pType == "Message")
	{
		// Can have multiple messages, in which case make a sequence
		StringVector vTitles = pConfFile->GetStrings(rPrefix + "Title");
		StringVector vContents = pConfFile->GetStrings(rPrefix + "Content");

		if (vTitles.size() != vContents.size())
			throw JFUtil::Exception("EventFactory", "MakeTriggerEvent",
				"Mismatched numbers of title/content parameters for a message event.", rPrefix);

		EventSequence* pSeqEv = new EventSequence("EVSEQ_MESSAGEBOXES");
		pToReturn = pSeqEv;
		for (size_t i = 0; i < vTitles.size(); ++i)
			pSeqEv->AddEvent( new EventMessageBox(vTitles[i], vContents[i], false) );

	}
	else if (*pType == "SetControlsEnabled")
	{
		StringVector vEnabled = pConfFile->GetStrings(rPrefix + "Enabled");
		StringVector vDisabled = pConfFile->GetStrings(rPrefix + "Disabled");

		pToReturn = new EventSetControlsEnabled(vEnabled, vDisabled);
	}
	// TODO: Lots more event types

	delete pType;
	if (!pToReturn)
		throw JFUtil::Exception("EventFactory", "MakeTriggerEvent",
			"Invalid or unimplemented event type given.", rPrefix);
	return pToReturn;
}
