#include <Control/BfPControlGroup.h>

/**
 * GroupController code
 */

Quaternion GroupController::m_qRot;

GroupController::GroupController(const BfPSectorState* pSectorState, VisibleState* pVisibleState,
	const AIEvaluator* pEvaluator)
: EntityController(pSectorState)
, m_pVisibleState(pVisibleState)
, m_pEvaluator(pEvaluator)
, m_bAutoPilot(true)
, m_pGroupID(NULL)
, m_pFaction(NULL)
, m_pKiller(NULL)
, m_iState(GST_STANDBY)
, m_pDestSector(NULL)
, m_pDestBody(NULL)
, m_iNextOrbitWP(0)
, m_pLeader(NULL)
, m_bDead(false)
, m_pFormation(NULL)
, m_pOrbitPaths(NULL)
, m_pOrbitDists(NULL)
, m_iThreatRecalcTimer(0)
{
	assert(m_pEvaluator);
	// Get the stance as the default one from the config
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	m_strStance = pAIConf->GetDefaultGroupStance();
	if (!GetAIStanceType())
		throw JFUtil::Exception("GroupController", "GroupController",
			"The default group stance in the AI config was invalid.");
}

GroupController::~GroupController()
{
	// Free all the memory we allocated for this controller.
	delete m_pGroupID;
	delete m_pFaction;
	delete m_pKiller;
	delete m_pDestSector;
	delete m_pDestBody;
	delete m_pLeader;
	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		delete *it;
	}
	m_vFollowers.clear();
	delete m_pFormation;
	delete m_pOrbitPaths;
	delete m_pOrbitDists;


}

void GroupController::SetAutoPilot(bool bOn)
{
	if (bOn == m_bAutoPilot) return;
	// If turning autopilot off, reset orbit waypoints and set the leader state to manual
	if (!bOn)
	{
		//cout << "turning autopilot off" << endl;
		m_iNextOrbitWP = 0;
		m_pLeader->UnsetDestDirection();
		m_pLeader->UnsetDestPoint();
		m_pLeader->SetAIState(SS_MANUAL);
	}
	// Reset ship controls in either case
	//cout << "resetting controls" << endl;
	m_pLeader->GetControlState()->ResetMovement();
	m_bAutoPilot = bOn;
}

bool GroupController::GetAutoPilot() const
{
	return m_bAutoPilot;
}

void GroupController::SetDestLocation(const String& rSector, const String& rBody)
{
	if (IsDead()) return;

	// Can't change destination if warping
	if (m_pLeader->GetState()->GetWarping()) return;
	// NOTE: Until we have map path-finding, don't let us set a sector that's not linked
	const SectorType* pMySector = m_pLeader->GetSector()->GetSectorType();
	const StringVector& rLinkedSectors = pMySector->GetLinkedSectors();
	if (rSector != pMySector->GetName() &&
		find(rLinkedSectors.begin(), rLinkedSectors.end(), rSector) == rLinkedSectors.end() )
		return;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSector = pScenarioConf->GetSectorType(rSector);
	assert(pSector);
	assert(pSector->GetBodyWithID(rBody));

	// If something is different, we need to reset the orbit path too.
	if ((!m_pDestSector || *m_pDestSector != rSector) ||
		(!m_pDestBody || *m_pDestBody != rBody))
	{
		m_iNextOrbitWP = 0;
		if (m_pLeader->GetAIState() == SS_GOTO)
			m_pLeader->UnsetDestPoint();

		delete m_pDestSector;
		m_pDestSector = new String(rSector);

		delete m_pDestBody;
		m_pDestBody = new String(rBody);
	}

}

void GroupController::SetDestLocation(const String& rSector)
{
	if (IsDead()) return;

	// Can't change destination if warping
	if (m_pLeader->GetState()->GetWarping()) return;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const SectorType* pSector = pScenarioConf->GetSectorType(rSector);
	assert(pSector);
	if (m_pDestSector && *m_pDestSector == rSector) return;

	// NOTE: Here we ignore destinations that aren't linked (remove this once we have pathfinding)
	assert(m_pLeader);
	assert(m_pLeader->GetSector());
	const SectorType* pMySector = m_pLeader->GetSector()->GetSectorType();
	const StringVector& rLinked = pMySector->GetLinkedSectors();
	if (rSector != pMySector->GetName() &&
		find(rLinked.begin(), rLinked.end(), rSector) == rLinked.end())
		return;

	// Same as the other: unset navigation stuff if we're changing the destination.
	m_iNextOrbitWP = 0;
	if (m_pLeader->GetAIState() == SS_GOTO)
		m_pLeader->UnsetDestPoint();

	delete m_pDestSector;
	m_pDestSector = new String(rSector);
	delete m_pDestBody;
	m_pDestBody = NULL;
}

void GroupController::UnsetDestLocation()
{
	// Can't change destination if warping
	if (m_pLeader->GetState()->GetWarping()) return;

	// Again, reset orbit waypoints and leader's destination.
	m_iNextOrbitWP = 0;
	if (m_pLeader->GetAIState() == SS_GOTO)
		m_pLeader->UnsetDestPoint();

	delete m_pDestBody;
	m_pDestBody = NULL;
	delete m_pDestSector;
	m_pDestSector = NULL;
}

void GroupController::SetLeader(ShipController* pLeader)
{
	assert(!m_pLeader && pLeader);
	const ShipState* pState = pLeader->GetState();
	// If this is the first ship to be added, set the group ID and faction name.
	if (!m_pGroupID)
	{
		assert(!m_pFaction && !m_pKiller);
		m_pGroupID = new int(pState->GetGroupID());
		m_pFaction = new String(pState->GetFaction());
		m_pKiller = new String(*m_pFaction); // If it's not set when we die, we killed ourselves.
	}
	else
	{
		assert(m_pFaction);
		assert((int)pState->GetGroupID() == *m_pGroupID);
		assert(pState->GetFaction() == *m_pFaction);
	}
	m_pLeader = pLeader;
	m_pLeader->SetGroupLeader();

	const ShipType* pType = pState->GetShipType();
	m_strStance = pType->GetInitialStance();

	// Leader can't already be a follower.
	assert(find(m_vFollowers.begin(), m_vFollowers.end(), pLeader) == m_vFollowers.end());
}

void GroupController::AddFollower(ShipController* pFollower)
{
	// Validate that this is an existing ship, in this group, of the correct faction, and in the correct sector.
	// NOTE: We can only add followers once we've set the leader. This shouldn't be a problem as we deploy groups discretely,
	// but will need changing if we ever want dynamically assignable groups.
	assert(m_pLeader);
	assert(pFollower && pFollower != m_pLeader);
	const ShipState* pState = pFollower->GetState();
	if (!m_pGroupID)
	{
		assert(!m_pFaction);
		m_pGroupID = new int(pState->GetGroupID());
		m_pFaction = new String(pState->GetFaction());
		m_pKiller = new String(*m_pFaction); // If it's not set when we die, we killed ourselves.
	}
	else
	{
		assert(m_pFaction);
		assert((int)pState->GetGroupID() == *m_pGroupID);
		assert(pState->GetFaction() == *m_pFaction);
	}

	pFollower->SetGroupLeader(m_pLeader->GetState()->GetID());

	// Must not already exist in our follower list!
	assert(find(m_vFollowers.begin(), m_vFollowers.end(), pFollower) == m_vFollowers.end());

	m_vFollowers.push_back(pFollower);
}

const ShipGroupStance* GroupController::GetAIStanceType() const
{
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	const ShipGroupStance* pStance = pAIConf->GetShipGroupStance(m_strStance);
	assert(pStance);
	return pStance;

}

void GroupController::SetAIStance(const String& rStance)
{
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");
	assert( pAIConf->GetShipGroupStance(rStance) );
	m_strStance = rStance;
}

void GroupController::SelfDestruct()
{
	// Destroy all ships in this group
	if (m_pLeader) m_pLeader->SelfDestruct();
	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		ShipController* pFollower = *it;
		pFollower->SelfDestruct();
	}
}

const String& GroupController::GetKiller() const
{
	assert(IsDead());
	assert(m_pKiller);
	return *m_pKiller;
}

void GroupController::Update(unsigned long iTime)
{


	m_iThreatRecalcTimer += iTime;

	// First, make sure there are no invalid ShipControllers of destroyed ships.
	if (m_bDead) return;
	CheckDestroyedMembers();
	if (m_bDead) return;

	switch (m_iState)
	{
	case GST_STANDBY:
		UpdateStandby(iTime);
		break;
	case GST_GOTO:
		UpdateGoto(iTime);
		break;
	case GST_FIGHT:
		UpdateFight(iTime);
		break;
	default:
		throw JFUtil::Exception("GroupController", "Update",
			"Unknown state in GroupController");
	}

	// Now update all of the member ships.
	m_pLeader->Update(iTime);
	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		(*it)->Update(iTime);
	}

	CheckWarping();
	CheckWarped();

}

void GroupController::CheckDestroyedMembers()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Check if the leader's dead; if so, we need a new one.
	assert(m_pLeader);
	bool bLeaderChanged = false;
	bool bFlagShip = false;
	bool bKillerSet = false;
	String strKiller; // Keep track of the killer, in case we need to make an event
	if (m_pLeader->GetState()->IsDead())
	{
		bFlagShip = m_pLeader->GetState()->IsInFlagGroup();
		strKiller = m_pLeader->GetState()->GetKillerFaction();
		delete m_pLeader;
		m_pLeader = NULL;
		bLeaderChanged = true;
		bKillerSet = true;
	}
	// Now check followers
	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end();)
	{
		ShipController* pFollower = *it;
		if (pFollower->GetState()->IsDead()) // follower dead, so erase it
		{
			delete pFollower;
			it = m_vFollowers.erase(it);
		}
		else if (!m_pLeader)
		{
			//follower alive but no leader: remove from follower list and make leader
			m_pLeader = pFollower;
			m_pLeader->UnsetDestDirection();
			m_pLeader->UnsetDestPoint();
			m_pLeader->UnsetFollowedShip();
			m_pLeader->SetAIState(SS_MANUAL);

			assert(m_pGroupID);
			unsigned iShipID = m_pLeader->GetState()->GetID();
			// Set the flags in the game state - flag for "I'm the group leader" and a flag for "I'm the flag ship".
			m_pVisibleState->SetGroupLeader(*m_pGroupID, iShipID);
			if (bFlagShip) m_pVisibleState->SetMyFlagShip(iShipID);

			it = m_vFollowers.erase(it);
		}
		else ++it; // follower alive and we have a leader, so do nothing
	}

	if (bLeaderChanged && m_pLeader)
	{
		// The leader has changed, so set each follower to the new followed ship.
		m_pLeader->UnsetFollowedShip();
		m_pLeader->SetGroupLeader();
		const ShipState* pLeaderState = m_pLeader->GetState();
		for (size_t i = 0; i < m_vFollowers.size(); ++i)
		{
			ShipController* pFollower = m_vFollowers[i];
			assert(pFollower != m_pLeader);
			pFollower->SetFollowedShip(pLeaderState, GetFormationPos(i));
			pFollower->SetGroupLeader(pLeaderState->GetID());
		}

	}
	else if (!m_pLeader) // No more ships left!
	{
		assert(m_vFollowers.empty()); // Disposed of them all
		m_bDead = true;

		// Here we trigger faction death if assassination is enabled
		assert(bKillerSet);
		assert(m_pFaction);
		assert(m_pKiller);
		if (bFlagShip && pScenarioConf->GetAssassinationVictory())
		{

			Event* pEvent = new EventFactionDeath(FDT_FLAGGROUPKILLED,
				strKiller, *m_pFaction);
			GlobalEvents::Get().PushEvent( pEvent );
		}
		// Make sure to remember who killed us, in case we were the final group.
		*m_pKiller = strKiller;
	}
}

void GroupController::CheckWarping()
{
	if (m_bDead)
	{
		return; // Group may have died in the update
	}
	// To warp a group, the leader must be in range of the warp point and the followers must be in formation with the leader
	if (!m_pDestSector ||
		*m_pDestSector == m_pSectorState->GetSectorType()->GetName())
	{
		return;
	}
	const Vector3& rLeaderPos = m_pLeader->GetState()->GetPosition();
	const SectorType* pSectorType = m_pLeader->GetSector()->GetSectorType();
	const String& rCurrentSector = pSectorType->GetName();
	const StringVector& rLinkedSectors = pSectorType->GetLinkedSectors();
	assert( find(rLinkedSectors.begin(), rLinkedSectors.end(), *m_pDestSector)
			!= rLinkedSectors.end() );
	const Vector3& rWarpPoint = pSectorType->GetWarpPointFromSector(*m_pDestSector);

	if ( !m_pLeader->CanWarp(*m_pDestSector, rWarpPoint) )
	{
		return;
	}

	assert(m_pDestSector);
	Vector3 vecFormationPos;
	for (size_t i = 0; i < m_vFollowers.size(); ++i)
	{
		ShipController* pFollower = m_vFollowers[i];
		const String& rFollowerSector =
			pFollower->GetSector()->GetSectorType()->GetName();
		assert(rFollowerSector == rCurrentSector);
		assert(pFollower->GetFormationPos());
		vecFormationPos = rLeaderPos +
			(m_pLeader->GetState()->GetOrientation()*(*pFollower->GetFormationPos()));

		if (!pFollower->CanWarp(*m_pDestSector, vecFormationPos))
		{
			return;
		}
	}

	// If we got here, then they can all warp - trigger the start of the warp effect.
	Vector3 vecWarpPointDir = rWarpPoint.normalisedCopy();
	// If the warp point is at (0,0,0), pick any arbitrary direction.
	if (vecWarpPointDir == Vector3::ZERO)
		vecWarpPointDir = Vector3::UNIT_Z;

	const ShipState* pState = m_pLeader->GetState();
	m_pVisibleState->BeginShipWarping(
		pState->GetID(), rCurrentSector, vecWarpPointDir);
	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		pState = (*it)->GetState();
		m_pVisibleState->BeginShipWarping(
			pState->GetID(), rCurrentSector, vecWarpPointDir);
	}
}

void GroupController::CheckWarped()
{
	if (m_bDead) return; // Group may have died in the update
	const String& rCurrentSector =
		m_pLeader->GetSector()->GetSectorType()->GetName();

	// We assume that, due to our checks, if the leader has warped then so has all followers.
	const ShipState* pState = m_pLeader->GetState();
	if (!pState->ReadyToWarp())
	{
		return;
	}
	//cout << "Entering warp!" << endl;

	// NOTE: We assume here the whole group will warp ok
	// Set the new values in the controller
	// TODO: This whole system needs tidying up - make the ShipState know the current sector!

	const String& rDestSector = *m_pDestSector;
	const ShipState* pNewState = m_pVisibleState->WarpShip(
		pState->GetID(), rCurrentSector, rDestSector);
	const BfPSectorState* pNewSector =
		m_pVisibleState->ObserveSector(rDestSector);
	assert(pNewState);
	assert(pNewSector);
	m_pLeader->SetNewState(pNewState, m_pVisibleState->GetShipControls(pNewSector, pNewState));
	m_pLeader->SetSector(pNewSector);

	m_pLeader->UnsetTargetShip();

	for (vector<ShipController*>::iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		ShipController* pController = *it;
		pState = pController->GetState();
		pNewState = m_pVisibleState->WarpShip(
			pState->GetID(), rCurrentSector, rDestSector);
		assert(pNewState);
		pController->SetNewState(pNewState, m_pVisibleState->GetShipControls(pNewSector, pNewState));
		pController->SetSector(pNewSector);
		pController->UnsetTargetShip();
	}
	//cout << "done checkwarped" << endl;
	m_pSectorState = pNewSector;
	delete m_pDestSector; m_pDestSector = NULL;
}


/**
 * GroupController private code
 */

void GroupController::UpdateStandby(unsigned long iTime)
{
	assert(m_pLeader);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	const ShipState* pLeaderState = m_pLeader->GetState();
	const BfPSectorState* pLeaderSector = m_pLeader->GetSector();
	const SectorType* pLeaderSectorType = pLeaderSector->GetSectorType();
	const Vector3& rLeaderPos = pLeaderState->GetPosition();
	const String& rMySector = pLeaderSectorType->GetName();

	/**
	 * CHECK TRANSITION CONDITIONS
	 */

	// Transition to FIGHT if there are nearby enemies (range depends on stance!)
	unsigned iTotalThreat;
	const ShipGroupStance* pStance = GetAIStanceType();
	const Real& rAggroDist = pStance->GetAggroRange();

	// If this is the threat update turn, Calculate threat and assign targets if enemies were found in range.
	if (m_iThreatRecalcTimer > pAIConf->GetThreatRecalcPeriod())
	{
		m_iThreatRecalcTimer = 0;

		m_mThreats = BfPUtil::GetThreatList(
			pLeaderState, pLeaderSector, rAggroDist, false, &iTotalThreat, m_pEvaluator);
		// WarpPastEnemies in the stance determines whether we ignore enemies when we have a move order for another sector
		bool bForceMove =
			pStance->GetWarpPastEnemies() && m_pDestSector && *m_pDestSector != rMySector;
		if (!m_mThreats.empty() && !bForceMove)
		{
			//cout << *m_pFaction << " group transitioning to FIGHT" << endl;
			m_iState = GST_FIGHT;
			return;
		}
	}

	// Transition to GOTO if we have been given a destination and are far enough away from it. (and we don't have any targets!)
	TransitionGoto(iTime);

	/**
	 * PERFORM STATE ACTIONS
	 */

	// In either case, we set the followers to follow the leader.
	for (size_t i = 0; i < m_vFollowers.size(); ++i)
	{
		ShipController* pShip = m_vFollowers[i];
		assert(pShip != m_pLeader);
		pShip->UnsetDestPoint();
		pShip->UnsetTargetShip();
		if (pShip->GetAIState() != SS_FOLLOW ||
			pShip->GetFollowedShip() != pLeaderState)
		{
			assert(pLeaderState);
			pShip->SetAIState(SS_FOLLOW);
			pShip->SetFollowedShip(pLeaderState, GetFormationPos(i));
		}
	}

	// If we're not on autopilot (i.e. the player is flying), do nothing with the leader.
	if (!GetAutoPilot()) return;

	// Otherwise, give our leader somewhere to go.
	m_pLeader->SetAIState(SS_GOTO);

	// Going to another sector
	if (m_pDestSector && *m_pDestSector !=
		m_pLeader->GetSector()->GetSectorType()->GetName()) // Dest sector set but not the same
	{
		const Vector3& rWarpPoint =
			pLeaderSectorType->GetWarpPointFromSector(*m_pDestSector);
		// TODO: Warp directions in config so we don't have to normalise every time
		Vector3 vecWarpDir = rWarpPoint.normalisedCopy();
		// TODO: Work out how to have it not re-check this every update
		if (!m_pLeader->GetDestPoint() || *m_pLeader->GetDestPoint() != rWarpPoint)
		{
			m_pLeader->SetDestPoint(rWarpPoint);
		}
		if (!m_pLeader->GetDestDirection() || *m_pLeader->GetDestDirection() != vecWarpDir)
		{
			m_pLeader->SetDestDirection(vecWarpDir);
		}

	}
	// Going to a body in this sector
	else if (m_pDestBody) // If dest body is set, maintain orbit destinations (formation)
	{
		//cout << "AI group moving to orbit" << endl;
		m_pLeader->SetAIState(SS_GOTO);

		const BodyInstance* pBody = pLeaderSectorType->GetBodyWithID(*m_pDestBody);
		assert(pBody);
		const Vector3& rBodyPos = pBody->GetLocation();
		const vector<Vector3>& rOrbitPath = GetOrbitPath(pBody->GetType());

		// If this is the first waypoint, clear the leader's orders first.
		if (m_iNextOrbitWP == 0)
		{
			m_pLeader->UnsetFollowedShip();
			m_pLeader->UnsetTargetShip();
			m_pLeader->UnsetDestPoint();

			// Give our leader the first (closest) waypoint)
			Vector3 vecRelPos = rLeaderPos - rBodyPos;
			size_t iClosestWP = GetClosestOrbitWP(pBody->GetType(), vecRelPos);
			m_iNextOrbitWP = iClosestWP + 1; // This can = the size of the list, at which point we look at the 0th
		}
		// Now just check if the leader has reached the waypoint and give it the next one if so
		assert(m_iNextOrbitWP > 0 && m_iNextOrbitWP <= rOrbitPath.size());
		if (!m_pLeader->GetDestPoint())
		{
			// Throw an "arrived" tutorial event if appropriate
			if (pScenarioConf->GetEventsEnabled("Tutorial") && pLeaderState->IsInFlagGroup())
			{
				const FactionType* pMyFaction = pScenarioConf->GetFactionType(pLeaderState->GetFaction());
				if (pMyFaction->GetController() == "Player")
				{
					GlobalEvents::Get().PushEvent( new EventTrigger("TacGroupArrived") );
				}
			}


			size_t iWP = m_iNextOrbitWP-1;
			const Vector3& rPoint = rOrbitPath[iWP];
			//cout << "Next waypoint: " << (rBodyPos + rPoint) << endl;
			m_pLeader->SetDestPoint(rBodyPos + rPoint);
			m_pLeader->UnsetDestDirection();
			++m_iNextOrbitWP;
			if (m_iNextOrbitWP > rOrbitPath.size())
				m_iNextOrbitWP = 1;
		}
	}
	else // Dest body isn't set; return to the middle of the sector and just sit there in formation.
	{
		// Followers are already in formation. Clear orders for the leader and tell it to go to zero.
		m_pLeader->SetAIState(SS_GOTO);
		m_pLeader->UnsetFollowedShip();
		m_pLeader->UnsetTargetShip();
		m_pLeader->SetDestPoint(Vector3::ZERO);
	}

}

void GroupController::UpdateGoto(unsigned long iTime)
{
	//cout << "group updating goto" << endl;
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	assert(m_pLeader);
	const ShipState* pLeaderState = m_pLeader->GetState();
	const BfPSectorState* pLeaderSector = m_pLeader->GetSector();
	const SectorType* pLeaderSectorType = pLeaderSector->GetSectorType();
	const Vector3& rLeaderPos = pLeaderState->GetPosition();
	const String& rMySector = pLeaderSectorType->GetName();


	/**
	 * CHECK TRANSITION CONDITIONS
	 */

	// First, check for enemies.
	if (m_iThreatRecalcTimer > pAIConf->GetThreatRecalcPeriod())
	{
		m_iThreatRecalcTimer = 0;

		unsigned iTotalThreat;
		const ShipGroupStance* pStance = GetAIStanceType();
		const Real& rAggroDist = pStance->GetAggroRange();

		m_mThreats = BfPUtil::GetThreatList(
			pLeaderState, pLeaderSector, rAggroDist, false, &iTotalThreat, m_pEvaluator);
		// WarpPastEnemies in the stance determines whether we ignore enemies when we have a move order for another sector
		bool bForceMove =
			pStance->GetWarpPastEnemies() && m_pDestSector && *m_pDestSector != rMySector;
		if (!m_mThreats.empty() && !bForceMove)
		{
			m_iState = GST_FIGHT;
			return;
		}
	}

	// Transition to STANDBY if we have no waypoints left (they're set when we transition to GOTO, so this is ok)
	if ((!m_pLeader->GetDestPoint() && !m_pLeader->GetDestDirection()) || !m_pDestBody)
	{
		//UnsetDestLocation();
		m_iState = GST_STANDBY;
		m_pLeader->SetAIState(SS_MANUAL);
		return;
	}

	// Transition to STANDBY if we're close enough to orbit (mainly copied from STANDBY same check)
	assert(m_pDestBody);

	// If our destination is in the current sector, check if we are already close to the target object.
	if (!m_pDestSector || *m_pDestSector ==  rMySector)
	{

		assert(m_pDestBody);
		// Need to get the type name
		const BodyInstance* pBody = pLeaderSectorType->GetBodyWithID(*m_pDestBody);
		assert(pBody);
		Real fMaxDist = GetOrbitDistance(pBody->GetType()) * 2;
		// We switch to STANDBY if we're orbiting a body - i.e. close enough to it.
		Real fDist = (pBody->GetLocation() - rLeaderPos).squaredLength();
		if (fDist <= fMaxDist)
		{
			m_iState = GST_STANDBY;
			return;
		}
	}


}

void GroupController::UpdateFight(unsigned long iTime)
{

	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	const ShipState* pLeaderState = m_pLeader->GetState();
	const BfPSectorState* pLeaderSector = m_pLeader->GetSector();
	const SectorType* pLeaderSectorType = pLeaderSector->GetSectorType();
	const String& rMySector = pLeaderSectorType->GetName();

	// In any case we will need to calculate threat this turn.
	unsigned iTotalThreat;
	const ShipGroupStance* pStance = GetAIStanceType();
	const Real& rAggroDist = pStance->GetAggroRange();
	assert(rAggroDist > 0); // If it was <= 0, then we shouldn't be in FIGHT state..
	// Calculate threat and assign targets if enemies were found in range.

	// Transition to GOTO if we have a destination that is in another sector and our stance
	// says to warp past enemies.
	bool bForceMove =
		pStance->GetWarpPastEnemies() && m_pDestSector && *m_pDestSector != rMySector;
	// (stay in combat if our destination is in the same sector!)
	if (bForceMove)
		TransitionGoto(iTime);


	// Unset all ship targets if we're ready

	if (m_iThreatRecalcTimer > pAIConf->GetThreatRecalcPeriod())
	{
		m_iThreatRecalcTimer = 0;

		/**
		 * CHECK TRANSITION CONDITIONS
		 */

		// Transition to STANDBY if no nearby enemies were detected
		// (reverse of STANDBY->FIGHT transition)
		m_mThreats = BfPUtil::GetThreatList(
			pLeaderState, pLeaderSector, rAggroDist, false, &iTotalThreat, m_pEvaluator);
		if (m_mThreats.empty())
		{
			m_iState = GST_STANDBY;
			return;
		}

		/**
		 * PERFORM STATE ACTIONS
		 */

		static BfPUtil::ThreatMap mAlteredThreats;
		mAlteredThreats = m_mThreats;

		// Choose a target and state (SS_ATTACK or SS_EVADE) for each ship (checks all enemies for each of our ships)
		if (GetAutoPilot())
			AssignTarget(m_pLeader, mAlteredThreats);
		for (vector<ShipController*>::iterator it = m_vFollowers.begin();
			 it != m_vFollowers.end(); ++it)
			AssignTarget(*it, mAlteredThreats);

	}



	// TODO: Choose between SS_EVADE and SS_ATTACK for each ship depending HP, threat, stance, etc
}

void GroupController::AssignTarget(ShipController* pMyShip, BfPUtil::ThreatMap& rThreats)
{
	assert(pMyShip);
	assert(!rThreats.empty());

	pMyShip->UpdateTargeting();

	const ShipState* pMyState = pMyShip->GetState();
	const ShipGroupStance* pStance = GetAIStanceType();
	const Real& rAggroDist = pStance->GetAggroRange();
	const Real& rEvadeRatio = pStance->GetEvadeThreshold();

	// These values are all assigned right away in the first iteration, as we already know
	// rThreats is not empty.
	// The best target is the one to which our ship poses the most threat
	unsigned iBestTargetID = 0;
	int iBestThreat = 0; // Will be reassigned, but we need to give it a value
	int iBestValue = -1; // Ship value not including distance modifier
	// The worst target is the one that poses the most threat to our ship.
	unsigned iWorstTargetID;
	int iWorstThreat = 0;
	bool bFirst = true;

	for (BfPUtil::ThreatMap::const_iterator it = rThreats.begin();
		 it != rThreats.end(); ++it)
	{
		const unsigned& rEnemyID = it->first;
		const int& rEnemyThreat = it->second;
		//cout << "enemy threat: " << rEnemyThreat << endl;
		const ShipState* pEnemy = (const ShipState*)
			m_pSectorState->GetObjState("SHIP", rEnemyID);
		assert(pEnemy);
		assert(pEnemy->GetFaction() != pMyState->GetFaction());
		int iMyThreat = BfPUtil::CalculateThreat(pMyState, rAggroDist, pEnemy, m_pEvaluator);

		// Leave enemies with negative threat (already chosen as target) until last
		if (rEnemyThreat < 0) iMyThreat = 0;

		// If it's the first one, make it the target no matter what.
		// Otherwise, make it the worst threat if their threat is highest
		// and make it the best threat if our threat is highest.
		if (bFirst || rEnemyThreat > iWorstThreat)
		{
			iWorstThreat = rEnemyThreat;
			iWorstTargetID = rEnemyID;
		}
		if (bFirst || iMyThreat > iBestThreat)
		{
			iBestThreat = iMyThreat;
			iBestTargetID = rEnemyID;
			iBestValue = BfPUtil::EvaluateShip(pMyState, pEnemy, m_pEvaluator);
		}
		bFirst = false;
	}
	//cout << endl;
	assert(!bFirst);

	// fThreatRatio is how much better our b
	assert(iBestThreat >= 0);
	// This could be negative, in which case we're overwhelming the enemy and won't want to evade anyway
	Real fThreatRatio;
	if (iBestThreat == 0)
		fThreatRatio = (Real)iWorstThreat;
	else fThreatRatio = (Real)iWorstThreat / (Real)iBestThreat;
//	cout << "threat ratio: " << fThreatRatio << endl;

	// TODO: We want to introduce a weighted probabilistic element to this behaviour, to make it
	// more interesting and less predictable.
	// If the threat ratio is too bad, te;; our ship to evade the most threatening target
	if (fThreatRatio > rEvadeRatio)
	{
		// TODO: This state transition stuff could be more encapsulated, taking appropriate
		// parameters validated or derive dinside the class.
		cout << "ship "  << pMyState->GetID() << " evading" << endl;
		pMyShip->SetAIState(SS_EVADE);
		pMyShip->SetTargetShip(iWorstTargetID);
	}
	else
	{
		// Set our ship to attack the best target and subtract our ship's value from the threat map
		// (this way, when the threat map is used to assign a target to another ship, they will pick a different one if appropriate)
//		cout << "ship "  << pMyState->GetID() << " attacking ship " << iBestTargetID << endl;
		int& rEnemyThreat = rThreats[iBestTargetID];
		assert(iBestValue >= 0);
		rEnemyThreat -= iBestValue;
		pMyShip->SetAIState(SS_ATTACK);
		pMyShip->SetTargetShip(iBestTargetID);
	}

}

void GroupController::TransitionGoto(unsigned long iTime)
{
	// No way we're going to do GOTO if we don't have a destination..
	if (!m_pDestSector && !m_pDestBody) return;
	//assert(m_pDestBody); // Must have at least a body if one of those are set

	// Check to see if we need to transition to GOTO (which overrides FIGHT and STANDBY)
	const ShipState* pLeaderState = m_pLeader->GetState();
	const BfPSectorState* pLeaderSector = m_pLeader->GetSector();
	const SectorType* pLeaderSectorType = pLeaderSector->GetSectorType();
	const Vector3& rLeaderPos = pLeaderState->GetPosition();

	const BodyInstance* pBody = m_pDestBody ? pLeaderSectorType->GetBodyWithID(*m_pDestBody) : NULL;

	bool bGoto = false;
	const String& rMySector = pLeaderSectorType->GetName();
	// If our destination is in the current sector, check if we are already close to the target object.
	if (pBody && (!m_pDestSector || *m_pDestSector ==  rMySector) )
	{
		// Need to get the type name

		Real fMaxDist = GetOrbitDistance(pBody->GetType()) * 2;
		// We only remain in STANDBY if we're orbiting a body - i.e. close enough to it.
		Real fDist = (pBody->GetLocation() - rLeaderPos).squaredLength();
		if (fDist > fMaxDist)
		{
			bGoto = true;
		}
	}
	else
	{
		bGoto = true;
	}

	if (bGoto)
	{
		for (size_t i = 0; i < m_vFollowers.size(); ++i)
		{
			ShipController* pShip = m_vFollowers[i];
			assert(pShip != m_pLeader);
			pShip->UnsetDestPoint();
			pShip->UnsetTargetShip();
			if (pShip->GetAIState() != SS_FOLLOW ||
				pShip->GetFollowedShip() != pLeaderState)
			{
				assert(pLeaderState);
				pShip->SetAIState(SS_FOLLOW);
				pShip->SetFollowedShip(pLeaderState, GetFormationPos(i));
			}
		}
		if (GetAutoPilot())
		{
//			m_pLeader->UnsetDestPoint();
			m_pLeader->UnsetFollowedShip();
			m_pLeader->UnsetTargetShip();
			//m_pLeader->SetDestPoint(pBody->GetLocation());
		}

		m_iState = GST_GOTO;
	}
}

void GroupController::CalculateFormation()
{
	vector<Vector3> vFormation;

	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	assert(m_pLeader);
	const ShipState* pLeaderState = m_pLeader->GetState();
	const ShipType* pLeaderType = pLeaderState->GetShipType();

	// Take the separation to be the sum of the largest two diameters.
	Real fMaxDiameter = 0;
	Real fLeaderRadius = pLeaderType->GetBoundingRadius();
	Real fDiameter;
	for (vector<ShipController*>::const_iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		ShipController* pFollower = *it;
		const ShipType* pType = pFollower->GetState()->GetShipType();
		fDiameter = fLeaderRadius + pType->GetBoundingRadius();
		if (fDiameter > fMaxDiameter) fMaxDiameter = fDiameter;
	}
	//cout << "fMaxDiameter: " << fMaxDiameter << endl;
	assert(fMaxDiameter > 0);

	// These values are orientated by the ship on use.
	// Forward: -X
	// Right: Z
	// Separation steps for a V formation.
	Vector3 vecBackLeft = Vector3::UNIT_X - Vector3::UNIT_Z;
	vecBackLeft.normalise();
	vecBackLeft *= fMaxDiameter * pAIConf->GetFormationSeparationMult();
	Vector3 vecBackRight = Vector3::UNIT_X + Vector3::UNIT_Z;
	vecBackRight.normalise();
	vecBackRight *= fMaxDiameter * pAIConf->GetFormationSeparationMult();

	//cout << "back left: " << vecBackLeft << endl;

	// Odd-numbered ships go on the left, even on the right.
	for (size_t i = 0; i < m_vFollowers.size(); ++i)
	{
		Vector3 vecPos = Vector3::ZERO;
		size_t iDist = (size_t)((Real)(i+1)/2 + 0.5);
		//cout << "number: " << iDist << endl;
		if (i % 2 == 0)
			vecPos += vecBackRight * (Real)iDist;
		else
			vecPos += vecBackLeft * (Real)iDist;
		assert(vecPos != Vector3::ZERO);

		//cout << "adding: " << vecPos << endl;
		vFormation.push_back(vecPos);
	}
	assert(vFormation.size() == m_vFollowers.size());

	if (m_pFormation)
		*m_pFormation = vFormation;
	else
		m_pFormation = new vector<Vector3>( vFormation );
}


void GroupController::CalculateOrbit()
{
	assert(m_pLeader);

	if (!m_pOrbitPaths)
		m_pOrbitPaths = new OrbitPaths();
	else
		m_pOrbitPaths->clear();

	if (!m_pOrbitDists)
		m_pOrbitDists = new OrbitDists();
	else
		m_pOrbitDists->clear();

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	// Base orbit distance = highest bounding radius + body's bounding radius
	Real fMaxRadius = m_pLeader->GetState()->GetShipType()->GetBoundingRadius();
	for (vector<ShipController*>::const_iterator it = m_vFollowers.begin();
		 it != m_vFollowers.end(); ++it)
	{
		const ShipState* pState = (*it)->GetState();
		Real fRadius = pState->GetShipType()->GetBoundingRadius();
		if (fRadius > fMaxRadius) fMaxRadius = fRadius;
	}

	// For each body type, get its bounding radius and calculate relative orbit coordinates.
	Real fDist;
	const StringVector& rBodyTypes = pScenarioConf->GetBodyTypes();
	for (StringVector::const_iterator it = rBodyTypes.begin();
		 it != rBodyTypes.end(); ++it)
	{
		const BodyType* pType = pScenarioConf->GetBodyType(*it);
		const String& rName = pType->GetName();
		Real fRadius = pType->GetBoundingRadius();
		fDist = (fRadius + fMaxRadius) * pAIConf->GetOrbitSeparationMult();
		assert(fDist > 0);

		vector<Vector3> vWaypoints;
		unsigned iNoWaypoints = pAIConf->GetOrbitWaypointCount();
		assert(iNoWaypoints > 0); // 1 or 2 will work, but that wouldn't be much of an "orbit"..
		Degree degAngle (0);
		Degree degAngleInc (360.0 / (Real)iNoWaypoints);
		// NOTE: Here we by default orbit around the x/z plane.
		const Vector3 vecStart = Vector3::UNIT_X * fDist;
		//cout << "orbit for body " << *it << ": " << endl;
		for (unsigned i = 0; i < iNoWaypoints; ++i)
		{
			m_qRot.FromAngleAxis(degAngle, Vector3::UNIT_Y);
			Vector3 vecWaypoint = m_qRot * vecStart;
			vWaypoints.push_back(vecWaypoint);
			degAngle += degAngleInc;
			//cout << "\t" << vecWaypoint << " " << endl;
		}
		//cout << endl;
		assert(vWaypoints.size() == iNoWaypoints);
		// No duplicate object types!
		assert(m_pOrbitPaths->find(rName) == m_pOrbitPaths->end());
		assert(m_pOrbitDists->find(rName) == m_pOrbitDists->end());
		m_pOrbitPaths->insert(
			pair< String, vector<Vector3> >(rName, vWaypoints) );
		m_pOrbitDists->insert(
			pair< String, Real >(rName, fDist) );
	}
	assert(m_pOrbitPaths->size() == rBodyTypes.size());
	assert(m_pOrbitDists->size() == rBodyTypes.size());

}

const Vector3& GroupController::GetFormationPos(size_t iShipNo)
{
	if (!m_pFormation) CalculateFormation();
	assert(m_pFormation);
	assert(iShipNo < m_pFormation->size());
	return m_pFormation->at(iShipNo);
}

Real GroupController::GetOrbitDistance(const String& rBodyType)
{
	if (!m_pOrbitDists)
	{
		assert(!m_pOrbitPaths);
		CalculateOrbit();
	}
	OrbitDists::const_iterator it = m_pOrbitDists->find(rBodyType);
	assert(it != m_pOrbitDists->end());
	return it->second;
}

const vector<Vector3>& GroupController::GetOrbitPath(const String& rBodyType)
{
	if (!m_pOrbitPaths)
	{
		assert(!m_pOrbitDists);
		CalculateOrbit();
	}
	OrbitPaths::const_iterator it = m_pOrbitPaths->find(rBodyType);
	assert(it != m_pOrbitPaths->end());
	return it->second;
}

size_t GroupController::GetClosestOrbitWP(const String& rBodyType, const Vector3& rRelativePos)
{
	//cout << "relative position: " << rRelativePos << endl;

	if (!m_pOrbitPaths)
	{
		assert(!m_pOrbitDists);
		CalculateOrbit();
	}

	OrbitPaths::const_iterator it = m_pOrbitPaths->find(rBodyType);
	assert(it != m_pOrbitPaths->end());
	const vector<Vector3>& rPath = it->second;
	assert(!rPath.empty());

	size_t iClosest = 0;
	const Vector3& rClosest = rPath[0];
	Real fClosestSqDist = rRelativePos.squaredDistance(rClosest);
	for (size_t i = 1; i < rPath.size(); ++i)
	{
		const Vector3& rThis = rPath[i];
		Real fThisSqDist = rRelativePos.squaredDistance(rThis);
		if (fThisSqDist < fClosestSqDist)
		{
			fClosestSqDist = fThisSqDist;
			iClosest = i;
		}
	}

	//cout << "closest: " << rPath[iClosest] << endl;

	return iClosest;
}
