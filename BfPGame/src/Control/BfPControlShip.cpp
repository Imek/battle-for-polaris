#include <Control/BfPControlShip.h>

/**
 * EntityController code
 */

EntityController::EntityController(const BfPSectorState* pSectorState)
: m_pSectorState(pSectorState)
{
	assert(m_pSectorState);
}

/**
 * ShipController code
 */

ShipController::ShipController(const BfPSectorState* pSectorState,
	const ShipState* pShipState, ControlState* pShipControls)
: EntityController(pSectorState)
, m_pShipState(pShipState)
, m_pShipControls(pShipControls)
, m_iState(SS_MANUAL)
, m_pDestPoint(NULL)
, m_pDestDirection(NULL)
, m_pAvoidCollDir(NULL)
, m_pFollowing(NULL)
, m_pFormationPos(NULL)
, m_iTargetShip(-1)
, m_iGroupLeader(-1) // TODO: Set leader
, m_bStrafingRun(true)
{
	assert(m_pShipState);
	// TODO: Manually set group leader when making ship controller groups!
	m_iGroupLeader = -1;
	assert(m_pShipControls);
	// Controls of the right ship! (returns const but we just check the address)
	assert(&m_pShipState->GetControls() == m_pShipControls);
}

ShipController::~ShipController()
{
	delete m_pDestPoint;
	delete m_pDestDirection;
	delete m_pAvoidCollDir;
	delete m_pFormationPos;
}

void ShipController::SetNewState(const ShipState* pNewState, ControlState* pNewControls)
{
	// NOTE: We don't check if m_pShipState is an invalid pointer here (it should be!)
	// TODO: Possible memory leak here!
	assert(pNewState);
	m_pShipState = pNewState;
	m_pShipControls = pNewControls;

	assert(&m_pShipState->GetControls() == m_pShipControls);
}

void ShipController::SetDestPoint(const Vector3& rPoint)
{
	delete m_pDestPoint;
	m_pDestPoint = new Vector3(rPoint);
}

void ShipController::UnsetDestPoint()
{
	if (!m_pDestPoint) return;
	delete m_pDestPoint;
	m_pDestPoint = NULL;
}

void ShipController::SetDestDirection(const Vector3& rDirection)
{
	//cout << "ship " << m_pShipState->GetID() << " set dest direction: " << rDirection << endl;
	delete m_pDestDirection;
	m_pDestDirection = new Vector3(rDirection);
}

void ShipController::UnsetDestDirection()
{
	//cout << "ship " << m_pShipState->GetID() << " unset dest direction" << endl;
	if (!m_pDestDirection) return;
	delete m_pDestDirection;
	m_pDestDirection = NULL;
}

void ShipController::SetFollowedShip(const ShipState* pFollowing, const Vector3& rRelPos)
{
	//cout << "set formation pos: " << rRelPos << endl;
	assert(pFollowing);
	m_pFollowing = pFollowing;
	if (!m_pFormationPos) m_pFormationPos = new Vector3();
	*m_pFormationPos = rRelPos;
}

void ShipController::UnsetFollowedShip()
{
	// Either both null or both not null
	if (!m_pFollowing)
	{
		assert(!m_pFormationPos);
		return;
	}

	assert(m_pFormationPos);
	m_pFollowing = NULL;
	delete m_pFormationPos;
	m_pFormationPos = NULL;
}

unsigned ShipController::GetTargetShip() const
{
	if (m_iTargetShip < 0)
		throw JFUtil::Exception("ShipController", "GetTargetShip",
			"Called when target ship was not set");
	return (unsigned)m_iTargetShip;
}

void ShipController::UnsetTargetShip()
{
	m_iTargetShip = -1;
	m_pShipControls->m_bFiringGroup = false;
	m_pShipControls->m_bFiringAll = false;
}

Vector3* ShipController::GetAvoidCollisionVector()
{
	// Work out if we're on a collision course; return NULL if not.
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");
	const ShipType* pShipType = m_pShipState->GetShipType();
	// Remember that the keys in this multimap are the squared distances!
	DistCollidableMap mCollidables = m_pSectorState->GetCollidablesWithin(
		m_pShipState->GetPosition(), pAIConf->GetMaxCollisionAvoidDist(), pShipType);


	if (BfPUtil::CollisionCourse(m_pShipState, mCollidables,
			pAIConf->GetMaxCollisionAvoidTime(), pAIConf->GetMaxCollisionAvoidDist(),
			pAIConf->GetPredictionTimeStep()))
	{
		return BfPUtil::GetAvoidVector( m_pShipState, mCollidables,
			pAIConf->GetMaxCollisionAvoidTime(), pAIConf->GetMaxCollisionAvoidDist(),
			pAIConf->GetPredictionTimeStep() );
	}
	else
	{
		return NULL;
	}

}

void ShipController::UpdateTargeting()
{
	// See whether our target is no longer valid.
	assert(m_pSectorState);
	if (HasTargetShip())
	{
		const ShipState* pTargetShip = (const ShipState*)
			m_pSectorState->GetObjState("SHIP", m_iTargetShip);
		if (!pTargetShip)
		{
			//cout << "unset target ship" << endl;
			UnsetTargetShip();
		}
		else // If it's valid, update the value stored in the control state
			m_pShipControls->SetTargetID(m_iTargetShip);
	}

}

bool ShipController::CanWarp(const String& rDestSector, const Vector3& rWarpPoint)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Begin warping the ship if the conditions are right
	// Only the leader needs to press the warp button
	if (GetWarpOn() && m_pShipState->FullEP())
	{
		//cout << "fullEP: " << m_pShipState->FullEP();

		// If we're the group leader, we must be close enough to the warp point.
		// If we're a follower, it is sufficient that we are in formation with the leader.
		const Vector3& rShipLoc = m_pShipState->GetPosition();
		Real fDistSq = rShipLoc.squaredDistance(rWarpPoint);
//			cout << "squared distance: " << fDistSq << endl;
//			cout << "max distance: " << pScenarioConf->GetMaxWarpDist()*pScenarioConf->GetMaxWarpDist() << endl;

		if (fDistSq <= pScenarioConf->GetMaxWarpDist()*pScenarioConf->GetMaxWarpDist())
		{
			//cout << "ship " << m_pShipState->GetID() << " can warp" << endl;
			return true;
		}
	}

	return false;
}

void ShipController::SelfDestruct()
{
	assert(m_pShipState);
	if (m_pShipState->IsDead()) return;

	// Set the self-destruct flag in the ship controls
	m_pShipControls->m_bSelfDestruct = true;
}

void ShipController::Update(unsigned long iTime)
{
	// First unset targets (enemies/groups) that are no longer valid.
	UpdateTargeting();
	// Do auto-aim update depending on our current target
	UpdateAutoAim();

	// Do low-level AI update
	switch (m_iState)
	{
	case SS_MANUAL:
		UpdateManual(iTime);
		break;
	case SS_GOTO:
		UpdateGoto(iTime);
		break;
	case SS_FOLLOW:
		UpdateFollow(iTime);
		break;
	case SS_ATTACK:
		UpdateAttack(iTime);
		break;
	case SS_EVADE:
		UpdateEvade(iTime);
		break;
	default:
		throw JFUtil::Exception("ShipController", "Update",
			"Unknown state in ShipController");
	}

}




/**
 * ShipController private code
 */

void ShipController::UpdateAutoAim()
{
	if (!m_pShipState->HasActiveWeapon())
	{
		return;
	}

	// Update the autoaim vectors for active weapons.
	const vector<unsigned>& rActiveWeaps = m_pShipControls->GetActiveWeapons();
	for (vector<unsigned>::const_iterator it = rActiveWeaps.begin();
		 it != rActiveWeaps.end(); ++it)
	{
		const unsigned& rWeapID = *it;
		const WeaponType* pWeap = m_pShipState->GetWeaponState(rWeapID).GetType();
		if (!pWeap->GetAutoTracking()) continue; // If this is false we don't even check, so it's fine to leave at Vector3::ZERO

		// The new firing direction is just facing direction if we have no target.
		Vector3 vecNewFiringDir = m_pShipState->GetFaceDir();
		if (HasTargetShip()) // TODO: When we have targetable projectiles, we will need to have a "target entity type" member that is checked here
		{
			// Only now do we calculate our aim direction.
			const EntityState* pTarget = (const EntityState*)
				m_pSectorState->GetObjState("SHIP", m_iTargetShip);
			assert(pTarget); // UpdateTargeting should've unset it

			bool bInRange;
			Vector3 vecPredictPos;
			vecNewFiringDir = UtilControl::LeadTarget(
			m_pShipState->GetPosition(), m_pShipState->GetVel(), pWeap->GetAccel(), pWeap->GetInitialSpeed(), pWeap->GetLifetime(),
			pTarget->GetPosition(), pTarget->GetVel(), &bInRange, &vecPredictPos);
			// If we're not in range, or facing too far away, put it back to facing direction
			Degree oMaxAngle ( pWeap->GetAutoTrackingMaxAngle() );
			if (!bInRange || vecNewFiringDir.angleBetween(m_pShipState->GetFaceDir()) > oMaxAngle)
				vecNewFiringDir = m_pShipState->GetFaceDir();
		}
		// Whatever the case, set the new firing direction.
		m_pShipControls->m_vAutoAim[rWeapID] = vecNewFiringDir;
	}
}


void ShipController::UpdateManual(unsigned long iTime)
{
	// Nothing really needed here
	//if (m_pShipState->GetID() == 0) cout << "player ship in MANUAL" << endl;
}

void ShipController::UpdateGoto(unsigned long iTime)
{
	// First check for collisions
	if (CheckAvoidCollisions()) return; // Do nothing else if we're busy turning to avoid something

	// Either navigate to the dest point or turn towards the dest direction. If we've arrived at both, unset them.
	bool bTurned = false;
	if (m_pDestPoint)
		bTurned = NavigateTo(*m_pDestPoint);

	if (!bTurned && m_pDestDirection)
	{
		//cout << "turning to face: " << *m_pDestDirection << endl;
		if (m_pDestDirection)
			bTurned = TurnToFace(*m_pDestDirection);
	}

	if (!bTurned)
	{
		// TODO: There's a turning bug in here somewhere (if facing the opposite direction of the destination direction TurnToFace doesn't turn, maybe?)
	   // cout << "UNSET" << endl;
		UnsetDestPoint();
		UnsetDestDirection();
	}
}

void ShipController::UpdateFollow(unsigned long iTime)
{
	//cout << "update follow" << endl;
	if (CheckAvoidCollisions()) return; // Do nothing else if we're busy turning to avoid something

	// move towards the position & leader orientation
	assert(m_pFollowing);
	assert(m_pFormationPos);

	// Navigate towards the target position (relative to the leader).
	const Quaternion& rLeaderOrient = m_pFollowing->GetOrientation();
	const Vector3& rLeaderPos = m_pFollowing->GetPosition();
	// TODO: Match speed so that it doesn't go off/on/off/on
	// If we've already arrived, match the facing direction of the leader.
	if (!NavigateTo((rLeaderOrient * (*m_pFormationPos)) + rLeaderPos ))
		TurnToFace(m_pFollowing->GetFaceDir());
	RollToMatch(m_pFollowing->GetFaceNorm());


}

void ShipController::UpdateAttack(unsigned long iTime)
{
	// Before we do anything, reset the relevant controls. They'll be turned on
	// below if appropriate.
	m_pShipControls->m_bFiringAll = false;
	m_pShipControls->m_bFiringGroup = false;
	m_pShipControls->m_bIncThrust = false;
	m_pShipControls->m_bRedThrust = false;
	m_pShipControls->m_bBrake = false;


	if (CheckAvoidCollisions()) return; // Do nothing else if we're busy turning to avoid something

	// Target may have been invalid and unset; if so, just change to SS_STANDBY and return.
	if (!HasTargetShip()) return;

	//cout << "ship " << m_pShipState->GetID();
	//cout << " attacking ship " << GetTargetShip() << endl;

	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");
	const ShipType* pMyShipType = m_pShipState->GetShipType();

	const ShipState* pEnemyShip = (const ShipState*)
		m_pSectorState->GetObjState("SHIP", GetTargetShip());
	assert(pEnemyShip);
	assert(pEnemyShip->GetFaction() != m_pShipState->GetFaction());
	if (pEnemyShip->IsDead())
	{
		UnsetTargetShip();
		return;
	}
	//NavigateTo(pEnemyShip->GetPosition());
	//pEnemyShip->GetPosition()

	const Vector3& rMyPos = m_pShipState->GetPosition();
	const Vector3& rMyVel = m_pShipState->GetVel();
	const Vector3& rMyFaceDir = m_pShipState->GetFaceDir();
	const Vector3& rEnemyPos = pEnemyShip->GetPosition();
	const Vector3& rEnemyVel = pEnemyShip->GetVel();

	static Vector3 vecMeToTarget;
	static Vector3 vecAim;
	static Vector3 vecHitPos;

	// TODO: We don't yet do choosing and switching of weapon groups in the ship AI.
	// When we implement it, we will want to call a weapon choosing & switching function around here somewhere.

	// For now, just get the type of the first active weapon (defaults to the first weapon group,
	// which is OK for most ship types we have so far).
	bool bInRange = false;
	bool bOnTarget = false;
	vecMeToTarget = rEnemyPos - rMyPos;
	vecAim = vecMeToTarget.normalisedCopy();
	if (m_pShipControls->HasActiveWeapon())
	{
		// NOTE: We also just do the target leading for the first weapon type found. As we're not generally grouping
		// weapons of different types (just yet at least), this isn't really a problem.
		const vector<unsigned>& rWeaps = m_pShipControls->GetActiveWeapons();
		assert(!rWeaps.empty());
		const WeaponType* pWeap = m_pShipControls->GetWeaponState(*rWeaps.begin()).GetType();
		vecAim = UtilControl::LeadTarget(
			rMyPos, rMyVel, pWeap->GetAccel(), pWeap->GetInitialSpeed(),
			pWeap->GetLifetime(),
			rEnemyPos, rEnemyVel, &bInRange, &vecHitPos);
		// Now that we know the aiming direction and whether we're in range, set our controls.
		bInRange = vecMeToTarget.squaredLength() < pWeap->GetRange()*pWeap->GetRange();
		Degree degFiringAngle (pAIConf->GetMaxFiringAngle());
		Degree degAutoAimAngle (pWeap->GetAutoTrackingMaxAngle());
		// If our weapon auto-tracks, be sure to aim if it's within the angle.
		if (pWeap->GetAutoTracking() && degAutoAimAngle > degFiringAngle)
			degFiringAngle = degAutoAimAngle;
		Degree degAngleFromFiring (vecAim.angleBetween(rMyFaceDir));
		bOnTarget = degAngleFromFiring <= degFiringAngle;
	}

	//cout << "angle from firing: " << degAngleFromFiring.valueDegrees() << endl;
	m_pShipControls->m_bFiringGroup = bInRange && bOnTarget;

	// Only don't turn if we're moving away and too close to the target
	// (If we turn too early we don't make enough distance to do a proper "strafing run"
	Real fDistSq = rMyPos.squaredDistance(rEnemyPos);
	Real fMinDistSq = pMyShipType->GetStrafeRunMinDist()*pMyShipType->GetStrafeRunMinDist();
	Real fMaxDistSq = pMyShipType->GetStrafeRunMaxDist()*pMyShipType->GetStrafeRunMaxDist();

	// TODO: Seems that there might be a glitch in here somewhere that occasionally
	// causes ships to constantly fly away until they leave aggro range?
	if (m_bStrafingRun)
	{
		//if (m_pShipState->GetID() == 0) cout << "strafing run" << endl;
		if (fDistSq < fMinDistSq) // Too close triggers end of strafing run
			m_bStrafingRun = false;
		else // On a strafing run and not too close? Good, we can keep trying to aim at the target
			TurnToFace(vecAim);
	}
	else // Not doing strafing run, so do evade stuff
	{
		//if (m_pShipState->GetID() == 0) cout << "away" << endl;
		// Basically the same as the evasion routine - get away from the enemy ship until we're far enough away.
		if (!m_pDestDirection) SetDestDirection( GetEvasionHeading() );
		if (! TurnToFace(*m_pDestDirection) ) SetDestDirection( GetEvasionHeading() );
		TurnToFace( GetEvasionHeading() );
		if (fDistSq > fMaxDistSq) m_bStrafingRun = true;
	}


	// For basic dogfighting, we always accelerate unless doing a strafing run and in range.
	bool bDrift = m_bStrafingRun && bInRange;
	//m_pShipControls->m_bZeroThrust = bDrift;
	m_pShipControls->m_bIncThrust = !bDrift;
	m_pShipControls->m_bRedThrust = bDrift;
	m_pShipControls->m_bBrake = false;


	// TODO: Transition to evade if too close to a collidable object
}

void ShipController::UpdateEvade(unsigned long iTime)
{
	// TODO: Get the combat behaviour code from the old project
	if (CheckAvoidCollisions()) return; // Do nothing else if we're busy turning to avoid something

	if (!m_pDestDirection)
	{
		SetDestDirection( GetEvasionHeading() );
	}

	// Now try to fly in the evasion direction we have set.
	if ( !TurnToFace(*m_pDestDirection) )
	{
		SetDestDirection( GetEvasionHeading() );
	}
	else
	{
		// Generally we want to be going fast when evading.. (simple version)
		m_pShipControls->m_bIncThrust = true;
		m_pShipControls->m_bRedThrust = false;
		m_pShipControls->m_bBrake = false;
	}

}

bool ShipController::CheckAvoidCollisions()
{
	if (!m_pAvoidCollDir)
	{
		Vector3* pAvoidVec = GetAvoidCollisionVector();
		if (!pAvoidVec) return false;
		m_pAvoidCollDir = pAvoidVec;
	}
	//cout << "COLLISION IMMINENT" << endl;

	// If we got here, we've got an avoid direction because we have a collision to avoid.
	// Turn and accelerate until we reach that direction, returning true if we're still turning.
	if ( !TurnToFace(*m_pAvoidCollDir) )
	{
		delete m_pAvoidCollDir;
		m_pAvoidCollDir = NULL;
		return false;
	}
	else
	{
		m_pShipControls->m_bIncThrust = true;
		m_pShipControls->m_bRedThrust = false;
		m_pShipControls->m_bBrake = false;
		return true;
	}
}


bool ShipController::TurnToFace(const Vector3& rTargetDir)
{
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	static Vector2 vecToTurn;
	vecToTurn = UtilControl::GetNeededTurn(
		m_pShipState->GetFaceDir(), m_pShipState->GetFaceNorm(), rTargetDir);

	// Do yaw and pitch
	bool bFinished = true;
	// TODO: Change minStep/toTurn stuff to degree angles
	// TODO: We could have smoother turning here if we somehow interpolate the yaw/pitch values (instead of just 1/-1/0)
	if (vecToTurn.x > pAIConf->GetMinYawStep()) // Yaw left
	{
		m_pShipControls->SetYaw(-1);
		bFinished = false;
	}
	else if (vecToTurn.x < -pAIConf->GetMinYawStep()) // Yaw right
	{
		m_pShipControls->SetYaw(1);
		bFinished = false;
	}
	else m_pShipControls->SetYaw(0);

	if (vecToTurn.y > pAIConf->GetMinPitchStep()) // Pitch down
	{
		m_pShipControls->SetPitch(1);
		bFinished = false;
	}
	else if (vecToTurn.y < -pAIConf->GetMinPitchStep()) // Pitch up
	{
		m_pShipControls->SetPitch(-1);
		bFinished = false;
	}
	else m_pShipControls->SetPitch(0);

	return !bFinished; // Returns false if finished turning
}

bool ShipController::RollToMatch(const Vector3& rTargetUp)
{
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	static Real fToRoll;
	fToRoll = UtilControl::GetNeededRoll(
		m_pShipState->GetFaceDir(), m_pShipState->GetFaceNorm(), rTargetUp);
	// Do roll
	Real fTurn;
	bool bRolling = GetSmoothTurnValue(fToRoll, pAIConf->GetMinRollStep(), pAIConf->GetMaxSlowRollStep(), fTurn);
	m_pShipControls->SetRoll(fTurn);
	return bRolling;
}

bool ShipController::GetSmoothTurnValue(const Real& rAngle, const Real& rMin, const Real& rDropOff, Real& rValue)
{
	// 0 if below minimum step, between 0 and (-)1 if between minimum and maximum, (-)1 otherwise.
	assert(rMin >= 0 && rMin <= rDropOff);
	Real fAbsAngle = Ogre::Math::Abs(rAngle);
	bool bPositive = rAngle > 0;

	if (fAbsAngle <= rMin)
	{
		rValue = 0; // In the dead zone
		return false;
	}


	if (fAbsAngle >= rDropOff)
		rValue = bPositive ? -1 : 1; // Outside of the smooth turn zone
	else // Otherwise interpolate between -1 and 1
	{
		if (rValue <= 0)
			rValue = bPositive ? -1 : 1;
		else
		{
			Real fMaxDiff = rDropOff - rMin;
			Real fDiff = fAbsAngle - rMin;
			rValue = fDiff / fMaxDiff;
//			cout << "amount: " << rValue << endl;
//			rValue = bPositive ? -1 : 1;

			rValue = (fAbsAngle - rMin) / rValue;
			rValue = bPositive ? -rValue : rValue;
		}
	}

	return true; // Either way we did turning.

}

bool ShipController::NavigateTo(const Vector3& rTargetPos)
{
	// Get the target direction and turn to face it.
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	// TODO: If we decide to give the AI ability to strafe, the stuff for it goes here

	Real fDistanceSq = m_pShipState->GetPosition().squaredDistance(rTargetPos);
	Real fMinDistSq = pAIConf->GetWaypointArrivedRadius();
	fMinDistSq *= fMinDistSq;
//	if (m_pShipState->GetID() == 0)
//		cout << fDistanceSq << " <= " << fMinDistSq << endl;

	if (fDistanceSq <= fMinDistSq)
	{
		//cout << "arrived" << endl;
		m_pShipControls->m_bIncThrust = false;
		m_pShipControls->m_bRedThrust = true;
		m_pShipControls->m_bBrake = true;
		m_pShipControls->SetYaw(0);
		m_pShipControls->SetPitch(0);
		return false;
	}
	//cout << "navigating" << endl;


	static Vector3 vecTargetDir;
	vecTargetDir = rTargetPos - m_pShipState->GetPosition();
	vecTargetDir.normalise();
	bool bTurning = TurnToFace(vecTargetDir);
//	if (!bTurning && m_pShipState->GetID() == 0)
//		cout << "TurnToFace " << vecTargetDir << endl;
	//if (!bTurning) return false;

	Radian oAngle = vecTargetDir.angleBetween(m_pShipState->GetFaceDir());

	// Accelerate if facing in the general direction of the target; decelerate otherwise.
	if (oAngle <= Radian(1.0f))
	{
		m_pShipControls->m_bIncThrust = true;
		m_pShipControls->m_bRedThrust = false;
		m_pShipControls->m_bBrake = false;
	}
	else
	{
		m_pShipControls->m_bIncThrust = false;
		m_pShipControls->m_bRedThrust = true;
		m_pShipControls->m_bBrake = true;
		if (!bTurning) return false;
	}

	return true;
}



Vector3 ShipController::GetEvasionHeading()
{
	// Evade our target ship (assigned by the group controller)
	// TODO: More advanced evasion that looks at multiple ships
	assert( HasTargetShip() );
	Vector3 vecTargetHeading (m_pShipState->GetFaceDir());

	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	// Work out the ideal aiming vector for the biggest threat, then avoid it.
	const ShipState* pThreat = (const ShipState*)
		m_pSectorState->GetObjState("SHIP", GetTargetShip());
	assert(pThreat);
	// TODO: Take into account multiple active weapons (currently we only look at the first one)
	bool bOnTarget = false;
	if (pThreat->HasActiveWeapon()) // They can't be "on target" with no weapons active
	{
		const vector<unsigned>& rWeaps = pThreat->GetActiveWeapons();
		assert(!rWeaps.empty());
		const WeaponType* pThreatWeap = pThreat->GetWeaponState(*rWeaps.begin()).GetType();
		bool bInRange;
		// The "threat aim" vector is the perceived ideal firing direction of the enemy.
		Vector3 vecHitPos; // We don't use this (the position we estimate the target will be on hit if applicable)
		Vector3 vecThreatAim = UtilControl::LeadTarget(
			pThreat->GetPosition(), pThreat->GetVel(), pThreatWeap->GetAccel(), pThreatWeap->GetInitialSpeed(), pThreatWeap->GetLifetime(),
			m_pShipState->GetPosition(), m_pShipState->GetVel(), &bInRange, &vecHitPos);
		// bOnTarget is our perception of whether the enemy should be firing
		bOnTarget = (bInRange &&
			(pThreat->GetFaceDir().angleBetween(vecThreatAim) <=
			 Degree(pAIConf->GetMaxFiringAngle())) );
		// If they're on target, try to evade by picking a randomised evasion heading
		if (bOnTarget)
		{

			return JFUtil::RandomVectorFromCentre(vecThreatAim,
				Degree(pAIConf->GetMinEvasionAngle()),
				Degree(pAIConf->GetMaxEvasionAngle()));
		}
	}
	// Not on target, so just accelerate away from the enemy.
	return (m_pShipState->GetPosition() - pThreat->GetPosition()).normalisedCopy();
}
