#include <Control/BfPControlEvents.h>

/**
 * @brief EventFactionDeath code
 */

EventFactionDeath::EventFactionDeath(FACTION_DEATH_TYPE iDeathType,
	const String& rFactionKilling, const String& rFactionKilled)
: Event("EV_FACTIONDEATH")
, m_iDeathType(iDeathType)
, m_strFactionKilling(rFactionKilling)
, m_strFactionKilled(rFactionKilled)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const FactionType* pKillingFaction = pScenarioConf->GetFactionType(rFactionKilling);
	const FactionType* pKilledFaction = pScenarioConf->GetFactionType(rFactionKilled);
	if (!pKillingFaction)
		throw JFUtil::Exception("EventFactionDeath", "EventFactionDeath",
						"Invalid killing faction provided");
	if (!pKilledFaction)
		throw JFUtil::Exception("EventFactionDeath", "EventFactionDeath",
						"Invalid killed faction provided");

	const String& rKillerName = pKillingFaction->GetReadableName();
	const String& rKilledName = pKilledFaction->GetReadableName();

	std::stringstream sout;

	switch (iDeathType)
	{
		case FDT_QUIT:
			if (rFactionKilled != rFactionKilling)
				throw JFUtil::Exception("EventFactionDeath", "EventFactionDeath",
								"Faction quit event but killed != killing");
			sout << rKilledName << " has surrendered!";
			break;
		case FDT_FLAGGROUPKILLED:
			if (rFactionKilled == rFactionKilling)
				sout << rKilledName << "'s leader has died in an accident! How embarrassing." ;
			else
				sout << rKilledName << "'s leader was assassinated by " << rKillerName << "!";
			break;
		case FDT_HOMECAPTURED:
			if (rFactionKilled == rFactionKilling)
				throw JFUtil::Exception("EventFactionDeath", "EventFactionDeath",
								"Faction flaggroup killed event but killed == killing");
			sout << pKilledFaction->GetReadableName() << "'s headquarters has been taken over by " << rKillerName << "!";
			break;
		case FDT_WIPEDOUT:
			if (rFactionKilled == rFactionKilling)
				sout << rKilledName << " has somehow managed to wipe itself out!";
			else
				sout << rKilledName << " has been wiped out by " << rKillerName << "!";
			break;
		default:
			throw JFUtil::Exception("EventFactionDeath", "EventFactionDeath",
							"Invalid faction death type provided.");
	}

	m_strMessage = sout.str();

}

EventFactionDeath::EventFactionDeath(const EventFactionDeath& rEvent)
: Event("EV_FACTIONDEATH")
, m_iDeathType(rEvent.m_iDeathType)
, m_strFactionKilling(rEvent.m_strFactionKilling)
, m_strFactionKilled(rEvent.m_strFactionKilled)
, m_strMessage(rEvent.m_strMessage)
{}

/**
 * EventSectorCaptured code
 */
EventSectorCaptured::EventSectorCaptured(const String& rSector,
	const String& rCapturer, const String& rCaptured)
: Event("EV_CAPTURE")
, m_strSector(rSector)
, m_strCapturer(rCapturer)
, m_strCaptured(rCaptured)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const FactionType* pCapturerFaction = pScenarioConf->GetFactionType(rCapturer);
	const FactionType* pCapturedFaction = pScenarioConf->GetFactionType(rCaptured);
	if (!pCapturerFaction)
		throw JFUtil::Exception("EventSectorCaptured", "EventSectorCaptured",
						"Invalid capturer faction provided");
	if (!pCapturedFaction)
		throw JFUtil::Exception("EventSectorCaptured", "EventSectorCaptured",
						"Invalid captured faction provided");

	std::stringstream sout;
	sout << pCapturerFaction->GetReadableName() << " has taken " << rSector;
	sout << " from " << pCapturedFaction->GetReadableName() << ".";
	m_strMessage = sout.str();
}

EventSectorCaptured::EventSectorCaptured(const String& rSector,
	const String& rCapturer)
: Event("EV_CAPTURE")
, m_strSector(rSector)
, m_strCapturer(rCapturer)
, m_strCaptured("")
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const FactionType* pCapturerFaction = pScenarioConf->GetFactionType(rCapturer);
	if (!pCapturerFaction)
		throw JFUtil::Exception("EventSectorCaptured", "EventSectorCaptured",
						"Invalid capturer faction provided");

	std::stringstream sout;
	sout << pCapturerFaction->GetReadableName() << " has taken " << rSector << ".";
	m_strMessage = sout.str();
}

EventSectorCaptured::EventSectorCaptured(const EventSectorCaptured& rObj)
: Event("EV_CAPTURE")
, m_strSector(rObj.m_strSector)
, m_strCapturer(rObj.m_strCapturer)
, m_strCaptured(rObj.m_strCaptured)
, m_strMessage(rObj.m_strMessage)
{

}

/**
 * EventMessageBox code
 */

EventMessageBox::EventMessageBox(string strTitle, string strMessage, bool bEndsGame)
: Event("EV_MESSAGEBOX")
, m_strTitle(strTitle)
, m_strMessage(strMessage)
, m_bEndsGame(bEndsGame)
{
}

EventMessageBox::EventMessageBox(const EventMessageBox& rObj)
: Event("EV_MESSAGEBOX")
, m_strTitle(rObj.m_strTitle)
, m_strMessage(rObj.m_strMessage)
, m_bEndsGame(rObj.m_bEndsGame)
{
}

/**
 * EventSetControlsEnabled code
 */

CONTROLS_MODE EventSetControlsEnabled::ReadControlsMode(const String& rMode)
{
	// Possible values are NONE, CHASECAM, TACCAM, MAPCAM, TURNING, MOVEMENT, BRAKE, SHIFT, TARGETING, WEAPONS,
	// SELECTBODY, SELECTGROUP, MOVEGROUP, SELECTSECTOR, PURCHASE, DEPLOY, ALL
	CONTROLS_MODE iMode;
	if ( JFUtil::StringEquals(rMode, "NONE") ) iMode = NONE;
	// Chase mode
	else if ( JFUtil::StringEquals(rMode, "TURNING") ) iMode = TURNING;
	else if ( JFUtil::StringEquals(rMode, "MOVEMENT") ) iMode = MOVEMENT;
	else if ( JFUtil::StringEquals(rMode, "BRAKE") ) iMode = BRAKE;
	else if ( JFUtil::StringEquals(rMode, "SHIFT") ) iMode = SHIFT;
	else if ( JFUtil::StringEquals(rMode, "TARGETING") ) iMode = TARGETING;
	else if ( JFUtil::StringEquals(rMode, "WEAPONS") ) iMode = WEAPONS;
	else if ( JFUtil::StringEquals(rMode, "SWITCHMODE") ) iMode = SWITCHMODE;
	// Tactical mode
	else if ( JFUtil::StringEquals(rMode, "SELECTBODY") ) iMode = SELECTBODY;
	else if ( JFUtil::StringEquals(rMode, "SELECTGROUP") ) iMode = SELECTGROUP;
	else if ( JFUtil::StringEquals(rMode, "MOVEGROUP") ) iMode = MOVEGROUP;
	else if ( JFUtil::StringEquals(rMode, "SELECTSECTOR") ) iMode = SELECTSECTOR;
	else if ( JFUtil::StringEquals(rMode, "PURCHASE") ) iMode = PURCHASE;
	else if ( JFUtil::StringEquals(rMode, "DEPLOY") ) iMode = DEPLOY;
	// All enabled (default)
	else if ( JFUtil::StringEquals(rMode, "ALL") ) iMode = ALL;
	else throw JFUtil::Exception("EventSetControlsEnabled", "ReadControlsMode",
			"Invalid control mode provided", rMode);
	return iMode;
}

EventSetControlsEnabled::EventSetControlsEnabled(const StringVector& vEnabled, const StringVector& vDisabled)
: Event("EV_SETCONTROLSENABLED")
{
	// Identify each mode in the lists.
	for (StringVector::const_iterator it = vEnabled.begin(); it != vEnabled.end(); ++it)
		m_vEnabled.push_back( ReadControlsMode(*it) );
	for (StringVector::const_iterator it = vDisabled.begin(); it != vDisabled.end(); ++it)
		m_vDisabled.push_back( ReadControlsMode(*it) );
}

EventSetControlsEnabled::EventSetControlsEnabled(const EventSetControlsEnabled& rObj)
: Event("EV_SETCONTROLSENABLED")
, m_vEnabled(rObj.m_vEnabled)
, m_vDisabled(rObj.m_vDisabled)
{

}

string EventSetControlsEnabled::GetString() const
{
	std::stringstream sout;
	sout << "EV_SETCONTROLSENABLED( Enabled[";
	for (vector<CONTROLS_MODE>::const_iterator it = m_vEnabled.begin(); it != m_vEnabled.end(); ++it)
		sout << *it << " ";
	sout << "] Disabled[";
	for (vector<CONTROLS_MODE>::const_iterator it = m_vDisabled.begin(); it != m_vDisabled.end(); ++it)
		sout << *it << " ";
	sout << "] )";
	return sout.str();
}

/**
 * EventTrigger code
 */

EventTrigger::EventTrigger(const String& rTrigger)
: Event("EV_TRIGGER")
, m_strTrigger(rTrigger)
{

}

EventTrigger::EventTrigger(const EventTrigger& rEvent)
: Event("EV_TRIGGER")
, m_strTrigger(rEvent.m_strTrigger)
{

}

string EventTrigger::GetString() const
{
	return "EV_TRIGGER(TRIG: \"" + m_strTrigger + "\")";
}

void EventTrigger::Resolve(unsigned long iTime)
{
	// We don't do this here any more.
}
