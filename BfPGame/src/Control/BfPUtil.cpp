#include <Control/BfPUtil.h>

unsigned BfPUtil::EvaluateShip(const ShipState* pShip, const ShipState* pAgainstShip,
		const AIEvaluator* pEvaluator)
{
	assert(pShip && pAgainstShip);
	assert(pShip->GetHP() > 0 && pAgainstShip->GetHP() > 0);

//	const ShipType* pMyType = pShip->GetShipType();
//	const ShipType* pAgainstType = pAgainstShip->GetShipType();

	// Overall condition (0-1) including energy
	Real fDiff =
		pShip->GetOverallCondition(true) /
		pAgainstShip->GetOverallCondition(true);

	const String& rOfType = pShip->GetSubType();
	const String& rAgainstType = pAgainstShip->GetSubType();
	return (unsigned)
		( fDiff * (Real)EvaluateShipType(rOfType, rAgainstType, pEvaluator) );
}

unsigned BfPUtil::EvaluateShipType(const String& rOfType, const String& rAgainstType,
		const AIEvaluator* pEvaluator)
{
	Real fValue = pEvaluator->GetShipValue(rOfType, rAgainstType);
	fValue += 1; // Note that the evaluator returns a value between -1 and 1; we want a value between 0 and 10.
	assert(fValue >= 0);

	return (unsigned)(fValue * 5.0);
}

unsigned BfPUtil::CalculateThreat(
	const ShipState* pMyShip, Real fRange, const ShipState* pTarget,
	const AIEvaluator* pEvaluator)
{
	const Vector3& rMyPos = pMyShip->GetPosition();
	unsigned iThisID = pTarget->GetID();
	assert(iThisID != pMyShip->GetID()); // Not the same ship!
	unsigned iThreat = BfPUtil::EvaluateShip(pMyShip, pTarget, pEvaluator); // Base threat is just the evaluation.

	// A ship may have multiple weapons. We consider the threat if we're within range
	// of at least one of their weapons.
	// NOTE: For now we don't consider if a weapon is active or not for general threat calculation.
	Real fWeapRange = 0;
	const vector<unsigned>& rWeaps = pTarget->GetAllWeapons();
	if (rWeaps.empty()) return 0; // No threat if no weapons at all!
	for (vector<unsigned>::const_iterator it = rWeaps.begin();
		 it != rWeaps.end(); ++it)
	{
		const WeaponState& rWState =  pTarget->GetWeaponState(*it);
		const WeaponType* pWType =  rWState.GetType();
		if (pWType->GetRange() > fWeapRange)
			fWeapRange = pWType->GetRange();
	}
	// This range is our aggro range or whatever (provided as a parameter)
	Real fRangeSq = fRange*fRange;
	Real fWeapRangeSq = fWeapRange * fWeapRange;

	// If <=, then we're always in their range if we're in their aggro
	// range, so we don't need to reduce threat by distance.
	Real fDistSq = pTarget->GetPosition().squaredDistance(rMyPos);
	if (fDistSq > fRangeSq) return 0;

	// They are within aggro range but we're outside their weapons range
	if (fRangeSq > fWeapRangeSq && fDistSq > fWeapRangeSq)
	{
		// Only now do we know we have to get proper distance.
		Real fDist = Ogre::Math::Sqrt(fDistSq);
		// .. The threat tends towards zero the further away the ship is.
		assert(fDist <= fRange);
		Real fDistOutside = fDist - fWeapRange;
		Real fMaxDist = fRange - fWeapRange;
		assert(fDistOutside >= 0 && fDistOutside < fMaxDist);
		fDistOutside = fMaxDist - fDistOutside;
		iThreat = (unsigned) ( (fDistOutside/fMaxDist)* (Real)iThreat );
	}

	return iThreat;
}

BfPUtil::ThreatMap BfPUtil::GetThreatList(
		const ShipState* pFrom, const BfPSectorState* pSector,
		Real fRange, bool bSameTeam, unsigned* pTotalThreat,
		const AIEvaluator* pEvaluator)
{
	ThreatMap mThreats;
	//const Vector3& rMyPos = pFrom->GetPosition();
	// GetEntitiesInRange doesn't get our own ship..
	vector<const EntityState*> vInRange =
		BfPUtil::GetEntitiesInRange("SHIP", bSameTeam, pFrom, pSector, fRange);
	// Sum up threat values for each nearby ship, to help decide whether to attack or retreat.
	assert(pTotalThreat);
	*pTotalThreat = 0;
	for (vector<const EntityState*>::iterator it = vInRange.begin();
		 it != vInRange.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		unsigned iThreat = CalculateThreat(pFrom, fRange, pShip, pEvaluator);
		//if (iThreat == 0) continue;
		unsigned iThisID = pShip->GetID();
		//cout << "ThisID for map: " << endl; //<< iThisID << endl;

		// Add it to the total.
		*pTotalThreat += iThreat;
		// Add the threat value to the map too.
		ThreatMap::iterator itEntry = mThreats.find(iThisID);
		if (itEntry != mThreats.end())
		{
			throw JFUtil::Exception("BfPUtil", "GetThreatList",
				"Duplicate ship IDs found when scanning for threats!");
		}
		//assert(mThreats.size() >= (iThisID-1));

		mThreats[iThisID] = iThreat;
		/*if(mThreats.size() > (iThisID-1))	{
			mThreats[iThisID] = iThreat;
		}*/
	}

	return mThreats;
}

//const ShipState* BfPUtil::GetBiggestThreat(
//		const ShipState* pFrom, Real fMyRange,
//		const ShipState* rSubject, Real fTheirRange,
//		const BfPSectorState* pSector, bool bSameTeam,
//		const AIEvaluator* pEvaluator)
//{
//	unsigned iHighest;
//	const ShipState* pHighestShip = NULL;
//	Real fMyRangeSq = fMyRange*fMyRange;
//	const Vector3& rMyPos = pFrom->GetPosition();
//
//	// GetEntitiesInRange doesn't get our own ship..
//	vector<const EntityState*> vInRange =
//		UtilControl::GetEntitiesInRange("SHIP", bSameTeam, pFrom, pSector, fMyRange);
//	// Iterate through each ship to find the one with the highest threat.
//	for (vector<const EntityState*>::iterator it = vInRange.begin();
//		 it != vInRange.end(); ++it)
//	{
//		const ShipState* pShip = (const ShipState*)*it;
//
//		// Check that pShip is within fMyRange of pFrom->
//		if ((pShip->GetPosition() - rMyPos).squaredLength() > fMyRangeSq)
//			continue;
//
//		// Calculate the threat value.
//		unsigned iThisID = pShip->GetID();
//		assert(iThisID != pFrom->GetID()); // Not the same ship!
//		unsigned iThreat = CalculateThreat(rSubject, fTheirRange, pShip, pEvaluator);
//
//		// See if it's higher.
//		if (!pHighestShip || iThreat > iHighest)
//		{
//			iHighest = iThreat;
//			pHighestShip = pShip;
//		}
//
//	}
//
//	// Return the highest ship, which could be NULL.
//	return pHighestShip;
//}


bool BfPUtil::CollisionCourse(const EntityState* pMe, const DistCollidableMap& rCollidables,
	unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep)
{
	assert(pMe);
	assert(rMaxDist > 0);
	assert(iTimeStep > 0);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const EntityType* pType = pMe->GetEntityType();

	const Vector3& rPosA = pMe->GetPosition();
	const Vector3& rVelA = pMe->GetVel();
	const Vector3& rDirA = pMe->GetFaceDir();
	// We assume here to take our acceleration (a) as maximum thrust(F) / mass(m).
	// TODO: In certain collision avoidance situations we won't want to go to allmax thrust immediately..
	// TODO: Incorporate strafing? (for now it's kind of a special player advantage as the AI doesn't strafe)
	Vector3 vecAccelA = rDirA.normalisedCopy();
	vecAccelA *= pType->GetMaxThrust() * pType->GetInvMass();
	const Real& rRadiusA = pMe->GetBoundingRadius();

	assert(rRadiusA >= 0);
	if (rRadiusA == 0) return false;

	static Vector3 vecPosB;
	static Vector3 vecVelB;
	static Vector3 vecAccelB;
	static Real fRadiusB;

	// Check collision course with every collidable (from closest to farthest)
	for (DistCollidableMap::const_iterator it = rCollidables.begin();
		 it != rCollidables.end(); ++it)
	{
//		const Real& rDist = it->first;
		const Collidable& rCollidable = it->second;

		//cout << "check ";
		if (rCollidable.IsEntity())
		{
			// Skip this one if:
			// It's my own ship..
			const EntityState* pEntity = rCollidable.GetEntity();
			if (pEntity->GetType() == pMe->GetType() &&
				pEntity->GetID() == pMe->GetID()) continue;
			// It's one of our own (produced) entities, e.g. projectiles..
			// TODO: If we decide to have ships collide with their own projectiles this needs removing
			if (pEntity->GetOwnerType() == pMe->GetType() &&
				pEntity->GetOwnerID() == pMe->GetID()) continue;
			// Or the thing isn't collidable in the first place.
			if (pEntity->GetBoundingRadius() == 0) continue;

			vecPosB = pEntity->GetPosition();
			vecVelB = pEntity->GetVel();
			vecAccelB = pEntity->GetAccel();
			fRadiusB = pEntity->GetBoundingRadius();
		}
		else
		{
			const BodyInstance* pBody = rCollidable.GetBody();
			//cout << "body " << pBody->GetID() << ": ";
			const BodyType* pBodyType =
				pScenarioConf->GetBodyType(pBody->GetType());
			vecPosB = pBody->GetLocation();
			vecVelB = Vector3::ZERO;
			vecAccelB = Vector3::ZERO;
			fRadiusB = pBodyType->GetBoundingRadius();
		}

		if (UtilControl::CollisionCourse(
			rPosA, rVelA, vecAccelA, rRadiusA,
			vecPosB, vecVelB, vecAccelB, fRadiusB,
			iMaxTime, rMaxDist, iTimeStep))
		{
			//if (pMe->GetID() == 0) cout << "hitting" << endl;
			return true;
		}

	}
	//if (pMe->GetID() == 0) cout << "not hitting" << endl;
	return false;
}

Vector3* BfPUtil::GetAvoidVector(const EntityState* pMe, const DistCollidableMap& rCollidables,
	unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep)
{
	static Quaternion qRot;
	static Vector3 vecAxis;
	static Vector3 vecNewFacing;

	assert(pMe);
	assert(rMaxDist > 0);
	assert(iTimeStep > 0);
	const AIConfig* pAIConf = (const AIConfig*)
		BfPConfig::GetInstance().GetConfig("AI");

	// NOTE: This isn't a reference because for some bizarre reason Ogre's angle-between function isn't const correct
	Vector3 vecOrigFacing = pMe->GetFaceDir();
	assert(vecOrigFacing != Vector3::ZERO);
	Degree degTurnInc ( pAIConf->GetCollisionAvoidAngleStep() );
	Degree degAngle (degTurnInc);
	Degree degDev;

	unsigned iNoIters = pAIConf->GetCollisionAvoidNumIterations();
	for (degAngle = degTurnInc; degAngle < Degree(360); degAngle += degTurnInc)
	{
		for (unsigned i = 0; i < iNoIters; ++i)
		{
			vecNewFacing = JFUtil::RandomVectorFromCentre(
				vecOrigFacing, degAngle, degAngle);
			vecAxis = vecOrigFacing.crossProduct(vecNewFacing);
			degDev = vecOrigFacing.angleBetween(vecNewFacing);
			qRot.FromAngleAxis(degDev, vecAxis);
			//cout << "deviation: " << degDev.valueDegrees() << endl;

			// TODO: See EntityState class declaration for details on this little hack.
			EntityState* pAlteredState = new EntityState( *pMe );
			pAlteredState->SetEntityType( pMe->GetEntityType() );
			pAlteredState->CombineOrientation(qRot);
			// Had to simplify this for now
//			pAlteredState->SetVel( vecNewFacing * pShipType->GetMaxSpeed() );
//			pAlteredState->SetAccel( Vector3::ZERO );
			// See if the ship, facing in the new altered direction, would still hit something
			if ( !CollisionCourse(pAlteredState, rCollidables, iMaxTime, rMaxDist, iTimeStep) )
			{
				// It didn't, return our new facing direction
				delete pAlteredState;
				return new Vector3(vecNewFacing);
			}
			else delete pAlteredState;
		}
	}

	return NULL;
}

vector<const EntityState*> BfPUtil::GetEntitiesInRange(
		const String& rType, bool bSameOwner,
		const EntityState* pFrom, const BfPSectorState* pSector, Real fRange)
{
	Real fRangeSq = fRange*fRange;
	vector<const EntityState*> vEntities;
	// Zero range (e.g. passive stance)
	if (fRange == 0) return vEntities;
	assert(fRange > 0);
	// This throws an exception if rType was invalid
	const ObjList& rObjList = pSector->GetObjectList(rType);

	for (ObjList::const_iterator it = rObjList.begin();
		 it != rObjList.end(); ++it)
	{
		const EntityState* pIt = (const EntityState*)*it;
		assert(pIt->GetType() == rType); // Wrong type for the ObjectList!
		if ( (bSameOwner
				? pIt->GetFaction() != pFrom->GetFaction()
				: pIt->GetFaction() == pFrom->GetFaction())
				|| pIt->GetID() == pFrom->GetID()
				|| pIt->IsDead())
		{
			continue; // Skip if pIt doesn't fpIt bSameTeam or dead or warping
		}

		// Use squared distances for efficiency
		Real fDistSq = pIt->GetPosition().squaredDistance(pFrom->GetPosition());
		assert(fDistSq >= 0);

		if (fDistSq < fRangeSq)
		{
			unsigned iID = pIt->GetID();
			const EntityState* pState =
				(const EntityState*)pSector->GetObjState(rType, iID);
			vEntities.push_back(pState);
		}
	}

	return vEntities;
}
