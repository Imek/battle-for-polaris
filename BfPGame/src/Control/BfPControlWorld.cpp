#include <Control/BfPControlWorld.h>

WorldController::WorldController(BfPState* pState)
: m_pState(pState)
{

}

WorldController::~WorldController()
{
	delete m_pState;
	for (map<String, FactionController*>::iterator it = m_mControllers.begin();
		 it != m_mControllers.end(); ++it)
	{
		FactionController* pController = it->second;
		delete pController;
	}
	m_mControllers.clear();
}

void WorldController::AddFactionController(const String& rName, FactionController* pController)
{
	assert(pController);
	assert(m_mControllers.find(rName) == m_mControllers.end());
	m_mControllers[rName] = pController;
}

void WorldController::Update(unsigned long iTime)
{
//	cout << "BEGIN WORLD UPDATE" << endl;

//	cout << "update controllers" << endl;
	UpdateControllers(iTime);
	// Updat the test logic engine (for display) too

	// EVENT: GAME START
	if (m_pState->GetAge() == 0)
		GlobalEvents::Get().PushEvent( new EventTrigger("GameStart") );
	// --

//	cout << "update state" << endl;
	m_pState->Update(iTime);
	//cout << "update triggers" << endl;
	UpdateTriggers(iTime);
//	cout << "update events" << endl;
	UpdateEvents(iTime);

//	cout << "END WORLD UPDATE" << endl;
}

void WorldController::UpdateControllers(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	// Update each controller.
	for (StringVector::const_iterator it = rFactions.begin(); it != rFactions.end(); ++it)
	{
		assert( m_mControllers.find(*it) != m_mControllers.end() );
		FactionController* pController = m_mControllers[*it];
		pController->Update(iTime);
		const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
		// Also update the test logic engine state if this was the player and we have a new tick.
		if (pFaction->GetController() == "Player")
		{
			const VisibleState* pVisibleState = pController->GetObservedState();
			const AbstractBfPState* pAbsState = AITestLogicEngine::GetInstance().GetState();
			if (pAbsState->GetAgeTicks() != pVisibleState->GetAgeInTicks())
				AITestLogicEngine::GetInstance().UpdateState(pVisibleState);
		}
	}

}

void WorldController::UpdateEvents(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Before we do events proper, first check for trigger events (as they may create and push events of their own)
	EventHandler& rEvents = GlobalEvents::Get();
	EventList lTriggers = rEvents.PopEventsOfType("EV_TRIGGER");
	for (EventList::iterator it = lTriggers.begin();
		 it != lTriggers.end(); ++it)
	{
		// Each one may add events to the global event handler.
		EventTrigger* pTriggerEvent = (EventTrigger*)*it;
		CheckEventTrigger(pTriggerEvent);
		delete pTriggerEvent;
	}

	// Also check for event sequences
	EventList lSequences = rEvents.PopEventsOfType("EV_SEQUENCE");
	for (EventList::iterator it = lSequences.begin();
		 it != lSequences.end(); ++it)
	{
		// Turn each event sequence into individual events
		EventSequence* pSeqEvent = (EventSequence*)*it;
		EventVector& rSeqEvs = pSeqEvent->GetEvents();
		for (EventVector::const_iterator it = rSeqEvs.begin(); it != rSeqEvs.end(); ++it)
		{
			Event* pEv = *it;
			rEvents.PushEvent(pEv);
		}
		// We need to remove the events from the list so they don't get destroyed
		// when we call delete pSeqEvent
		rSeqEvs.clear();
		delete pSeqEvent;
	}


	// Manage any control-related events
	size_t iSize = rEvents.GetSize();
	for (size_t i = 0; i < iSize; ++i)
	{
		Event* pEvent = rEvents.PopEvent();
		assert(pEvent);
		// Message and MessageBox events are kept in circulation until the GUI handles and removes them
		const String& rType = pEvent->GetType();
		if (rType != "EV_MESSAGE" && rType != "EV_MESSAGEBOX")
		{
			// Special cases for certain event types
			if (pEvent->GetType() == "EV_FACTIONDEATH")
			{
				// Handle faction death event: if it's the second-last or last faction, end the game.
				EventFactionDeath* pDeathEvent = (EventFactionDeath*)pEvent;
				CheckEventFactionDeath(pDeathEvent);
			}
			else if (pEvent->GetType() == "EV_SETCONTROLSENABLED")
			{
				// Controller events change the input flags in the state
				EventSetControlsEnabled* pInputEvent = (EventSetControlsEnabled*)pEvent;
				const vector<CONTROLS_MODE>& rEnabled = pInputEvent->GetModesEnabled();
				for (vector<CONTROLS_MODE>::const_iterator itMode = rEnabled.begin(); itMode != rEnabled.end(); ++itMode)
					m_pState->AddPlayerInputFlag( *itMode );
				const vector<CONTROLS_MODE>& rDisabled = pInputEvent->GetModesDisabled();
				for (vector<CONTROLS_MODE>::const_iterator itMode = rDisabled.begin(); itMode != rDisabled.end(); ++itMode)
					m_pState->RemovePlayerInputFlag( *itMode );
			}
			else
			{
				// Call Resolve on any other kind of event (e.g. tutorial events)
				pEvent->Resolve(iTime);
			}
			delete pEvent;
		}
		else // Put it back if it's a message
		{
			rEvents.PushEvent(pEvent);
		}
	}

	// Update events for sectors
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		// Make an event if the owner has changed
		BfPSectorState* pSector = m_pState->GetSectorState(*it);
		assert(pSector);
		if (!pSector->GetOwnerChanged() || !pSector->GetOwner()) continue;
		String strNewOwner = *pSector->GetOwner();
		const String* pPrevOwner = pSector->GetLastOwner();
		Event* pEvent;
		if (pPrevOwner)
			pEvent = new EventSectorCaptured(*it, strNewOwner, *pPrevOwner);
		else
			pEvent = new EventSectorCaptured(*it, strNewOwner);

		GlobalEvents::Get().PushEvent(pEvent);
		// Also make a message event
		GlobalEvents::Get().PushEvent( new EventMessage(pEvent->GetString()) );
	}
}

void WorldController::UpdateTriggers(unsigned long iTime)
{
	// Go through each mission that has an active trigger in the game state,
	// and increment it by iTime if it's below the required time.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (!pScenarioConf->HasMissionsConfig()) return; // No missions; nothing to do
	const MissionsConfig& rMissions = pScenarioConf->GetMissionsConfig();
	MissionList& rState = m_pState->GetMissions();

	for (MissionList::iterator itMission = rState.begin();
		 itMission != rState.end(); ++itMission)
	{
		const String& rMission = itMission->first;
		TriggerList& rTriggers = itMission->second;
		const MissionConfig& rMissionConf = rMissions.GetMission(rMission);

		for (TriggerList::iterator itTrigger = rTriggers.begin();
			 itTrigger != rTriggers.end(); ++itTrigger)
		{
			const String& rTriggerName = itTrigger->first;
			TriggerState& rTrigger = itTrigger->second;
			unsigned long& rTimer = rTrigger.m_iTimer;
			const TriggerConfig& rTrigConf = rMissionConf.GetTrigger(rTriggerName);
			// Do the timer and check completionif it's not done (need to check m_bDone in case of 0 timer)
			if (rTimer < rTrigConf.m_iTime || !rTrigger.m_bDone)
			{
				rTimer += iTime;
				if (rTimer > rTrigConf.m_iTime)
				{
					rTimer = rTrigConf.m_iTime;
					// If we got here, then it's time to execute the trigger if appropriate.
					if (!rTrigger.m_bDone)
					{
						ExecuteTrigger(rTrigConf);
						rTrigger.m_bDone = true;
					}
				}
			}
		}
	}

}

void WorldController::CheckEventFactionDeath(EventFactionDeath* pEvent)
{
	assert(pEvent);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	// See how many factions we have left alive.
	size_t iNoFactionsLeft = 0;
	for (StringVector::const_iterator it = rFactions.begin(); it != rFactions.end(); ++it)
	{
		assert( m_mControllers.find(*it) != m_mControllers.end() );
		FactionController* pController = m_mControllers[*it];
		if (!pController->IsDead()) ++iNoFactionsLeft;
	}

	// Game over message ends the game when the user clicks OK.
	if (iNoFactionsLeft < 2)
		GlobalEvents::Get().PushEvent( new EventMessageBox("Game Over", pEvent->GetString(), true) );
}

void WorldController::CheckEventTrigger(EventTrigger* pEvent)
{
	assert(pEvent);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if (!pScenarioConf->HasMissionsConfig()) return; // No missions; nothing to do
	const MissionsConfig& rMissions = pScenarioConf->GetMissionsConfig();

	// The mission list is a map of MissionID -> TriggerList
	// And a TriggerList is a map of TriggerID -> Timer
	// For triggers that aren't timed, they are just set as 0.
	MissionList& rState = m_pState->GetMissions();

	// Get the actual mission triggers that use this trigger type
	vector<const TriggerConfig*> vTriggers =
		rMissions.GetTriggersWithType(pEvent->GetTrigger());
	for (vector<const TriggerConfig*>::const_iterator itTrigger = vTriggers.begin();
		 itTrigger!= vTriggers.end(); ++itTrigger)
	{
		// For each one, see if it has been satisfied
		const TriggerConfig* pTrigger = *itTrigger;
		assert(pTrigger->m_strTriggerType == pEvent->GetTrigger());
		const MissionConfig& rMission = rMissions.GetMission(pTrigger->m_strMission);

		// Find the trigger in the state, if it is there (but the mission MUST exist!)
		assert(rState.find(pTrigger->m_strMission) != rState.end());
		MissionList::iterator itMissionState = rState.find(pTrigger->m_strMission);
		if (itMissionState == rState.end()) continue; // No entries for the mission
		TriggerList& rMissionState = itMissionState->second;
		TriggerList::const_iterator itTriggerState = rMissionState.find(pTrigger->m_strName);
		if (itTriggerState != rMissionState.end()) continue; // Trigger already activated or timing down

		// Next, check if the prerequisites have been met.
		bool bPrereqsSatisfied = true;
		const StringVector& rPrereqs = pTrigger->m_vPrereqs;
		for (StringVector::const_iterator itPrereq = rPrereqs.begin();
			 itPrereq != rPrereqs.end(); ++itPrereq)
		{
			TriggerList::const_iterator itPrereqTrigger =
				rMissionState.find(*itPrereq);
			if (itPrereqTrigger == rMissionState.end()) bPrereqsSatisfied = false;
			// If it's in there, check that its timer is OK.
			const TriggerConfig& rPrereqConf = rMission.GetTrigger(*itPrereq);
			const unsigned long& rPrereqTimer = itPrereqTrigger->second.m_iTimer;
			if (rPrereqTimer < rPrereqConf.m_iTime) bPrereqsSatisfied = false;
		}
		if (!bPrereqsSatisfied) continue;

		// If we got here, pEvent triggered an event whose prerequisites were all met - add it to the state.
		TriggerState oNewState;
		oNewState.m_bDone = false;
		oNewState.m_iTimer = 0;
		rMissionState[pTrigger->m_strName] = oNewState;
	}
}

void WorldController::ExecuteTrigger(const TriggerConfig& rTrigConf) const
{
	const vector<EventConfig>& vEvents = rTrigConf.m_vEvents;
	// Make each event and add it to the global event handler
	for (vector<EventConfig>::const_iterator it = vEvents.begin();
		 it != vEvents.end(); ++it)
	{
		Event* pEv = EventFactory::MakeTriggerEvent(*it);
		assert(pEv);
		GlobalEvents::Get().PushEvent(pEv);
	}
}
