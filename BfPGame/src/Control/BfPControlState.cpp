#include <Control/BfPControlState.h>

VisibleState::VisibleState(BfPState* pBfPState, String strFaction)
: m_pBfPState(pBfPState)
, m_pFactionState(NULL)
, m_bDeployedStartingGroups(false)
{
	assert(m_pBfPState);
	m_pFactionState = m_pBfPState->GetFactionState(strFaction);
	assert(m_pFactionState);
}

VisibleState::~VisibleState()
{
	// VisibleState doesn't manage any memory to which it points.
}

const FactionState* VisibleState::GetFactionState() const
{
	assert(m_pFactionState);
	return m_pFactionState;
}

unsigned long VisibleState::GetAgeInTicks() const
{
	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	unsigned long iAgeMs = m_pBfPState->GetAge();
	if (iAgeMs != 0)
	{
		unsigned long iTickLength = pAIConf->GetTickLength();
		iAgeMs -= iAgeMs % iTickLength;
		assert(iAgeMs % iTickLength == 0); // If this assertion fails, I've done something quite stupid
		iAgeMs = iAgeMs / iTickLength;
	}

	return iAgeMs;
}

String VisibleState::GetFactionName() const
{
	assert(m_pFactionState);
	return m_pFactionState->GetName();
}

const long& VisibleState::GetFactionScore(const String& rFaction) const
{
	const FactionState* pFaction = m_pBfPState->GetFactionState(rFaction);
	return pFaction->GetScore();
}


const BfPSectorState* VisibleState::ObserveSector(String strSectorID) const
{
	vector<const ShipState*> vShips;
	const BfPSectorState* pSector = m_pBfPState->GetSectorState(strSectorID);
	pSector->GetFactionShips(m_pFactionState->GetName(), vShips);

	// Basic observation: If our faction has a ship in a sector, we can see everything in it.
	// We can also see sectors that we still own.
	if (vShips.empty() &&
		(!pSector->GetOwner() || *pSector->GetOwner() != GetFactionName()))
		return NULL;
	else
		return pSector;
}

bool VisibleState::CanPurchaseGroup(const StringVector& rTypes) const
{
	return m_pFactionState->CanPurchaseGroup(rTypes);
}

bool VisibleState::PurchaseGroup(const StringVector& rTypes)
{
	return m_pFactionState->PurchaseGroup(rTypes);
}


bool VisibleState::CanDeployGroup(
	const StringVector& rTypes, const String& rSector,
	unsigned iGroupID, bool bFlagGroup) const
{
	// We must have available instances the right types to deploy, and we must own the sector.
	if (!m_pFactionState->HasGroupToDeploy(rTypes)) return false;

	BfPSectorState* pSector = m_pBfPState->GetSectorState(rSector);
	assert(pSector);

	// Check we actually own the sector.
	if (!pSector->GetOwner() || *(pSector->GetOwner()) != GetFactionName()) return false;

	// Now we can actually call CanSpawnGroup.
	return CanSpawnGroup(rTypes, rSector, iGroupID, bFlagGroup);
}

bool VisibleState::DeployGroup(
	const StringVector& rTypes, const String& rSector, unsigned iGroupID, bool bFlagGroup)
{
	// Check CanDeployGroup first
	if ( !CanDeployGroup(rTypes, rSector, iGroupID, bFlagGroup) ) return false;
	// Then just do the spawning
	if ( !SpawnGroup(rTypes, rSector, iGroupID, bFlagGroup) ) return false;
	// We know for sure that deployment was successful, so remove them from the deployment list
	assert ( m_pFactionState->RemoveDeployingGroup(rTypes) );
	return true;
}


void VisibleState::DeployStartingGroups()
{
	if (m_bDeployedStartingGroups)
		throw JFUtil::Exception("VisibleState", "DeployStartingGroups",
			"Tried to deploy starting groups when it had already been done", GetFactionName());
	if (m_pBfPState->GetAge() > 0)
		throw JFUtil::Exception("VisibleState", "DeployStartingGroups",
			"Tried to deploy starting groups when it wasn't the start of the game", GetFactionName());

	assert(m_pFactionState);
	const FactionType* pFaction = m_pFactionState->GetFactionType();
	assert(pFaction);
	const StringVector& rGroups = pFaction->GetStartingGroups();

	// i (plus one) can also be used as the group ID, as we don't yet have any groups.
	StringVector vTypes;
	for (size_t i = 0; i < rGroups.size(); ++i)
	{
		const StartingGroup& rGroup = pFaction->GetStartingGroup( rGroups[i] );
		const String& rSector = rGroup.GetSector();
		const String& rShipType = rGroup.GetShipType();
		unsigned iShipCount = rGroup.GetShipCount();
		assert(iShipCount > 0); // Should've been checked in the config constructor
		for (unsigned j=0;j<iShipCount;++j) vTypes.push_back(rShipType);
		//
		if (!SpawnGroup(vTypes, rSector, i+1, i==0))
			throw JFUtil::Exception("VisibleState", "DeployStartingGroups",
				"Failed to deploy all starting groups (invalid ID or not enough space?)", GetFactionName());
		vTypes.clear();
	}
}

ControlState* VisibleState::GetShipControls(String strSectorID, int iShipID)
{
	BfPSectorState* pSector = m_pBfPState->GetSectorState(strSectorID);
	if (!pSector) return NULL;
	ShipState* pShip = (ShipState*) pSector->GetObjState("SHIP", iShipID);
	if (!pShip || pShip->GetFaction() != GetFactionName()) return NULL;
	return &pShip->GetControls();
}

ControlState* VisibleState::GetShipControls(const BfPSectorState* pSector, const ShipState* pShip)
{
	return GetShipControls(pSector->GetSectorType()->GetName(), pShip->GetID());
}

ShipGroupList VisibleState::GetVisibleFactionGroups(const String& rFaction, const String& rSector) const
{
	// TODO: Change to just get groups for a sector
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	ShipGroupList mGroups;
	const BfPSectorState* pSector = ObserveSector(rSector);
	if (!pSector) return mGroups; // Move along, nothing to see here
	vector<const ShipState*> vShips = pSector->GetFactionShips(rFaction);

	for (vector<const ShipState*>::const_iterator itShip = vShips.begin();
		 itShip != vShips.end(); ++itShip)
	{
		const ShipState* pShip = *itShip;
		unsigned iGroupID = pShip->GetGroupID();
		ShipGroupList::iterator itGroup = mGroups.find(iGroupID);
		// May create a new group
		if (itGroup == mGroups.end())
			mGroups[iGroupID].first = NULL; // Null leader if newly made
		ShipGroup& rGroup = mGroups[iGroupID];
		if (pShip->IsGroupLeader())
		{
			assert(!rGroup.first);
			rGroup.first = pShip;
		}
		else // Follower
		{
			rGroup.second.push_back(pShip);
		}
	}

	return mGroups;
}

void VisibleState::BeginShipWarping(unsigned iID, const String& rShipSector, const Vector3& rWarpDirection)
{
	// Get a ship state and tell it to begin warping
	BfPSectorState* pSector = m_pBfPState->GetSectorState(rShipSector);
	assert(pSector);

	// Also set my ship to face the warp direction
	ShipState* pShip = (ShipState*)pSector->GetObjState("SHIP", iID);
	assert(pShip);
	pShip->SetFaceDir(rWarpDirection);
	pShip->SetVel(rWarpDirection * pShip->GetVel().length());
	pShip->SetAccel(Vector3::ZERO);


	pShip->BeginWarping();
	pShip->ZeroThrust();
}

const ShipState* VisibleState::WarpShip(unsigned iID, const String& rFromSector, const String& rDestSector)
{
	// Get a ship state and tell it to finish warping if it can or reset if not
	BfPSectorState* pSector = m_pBfPState->GetSectorState(rFromSector);
	//cout << "sector: " << rDestSector << endl;
	assert(pSector);
	ShipState* pShip = (ShipState*)
		pSector->GetObjState("SHIP", iID);
	// If it failed to warp, just reset
	const ShipState* pNewShip = m_pBfPState->WarpShip(iID, rDestSector);
	if (!pNewShip) pShip->ResetWarping();

	return pNewShip;
}

void VisibleState::UnsetMyFlagShip()
{
	// If we're unsetting the flag ship, then we must have no ships left!
	assert(m_pBfPState->GetFactionShips( GetFactionName() ).empty());
	m_pFactionState->UnsetFlagShipID();
}

void VisibleState::SetMyFlagShip(unsigned iShipID)
{
	BfPSectorState* pSector;
	assert( m_pBfPState->FindShipWithID(iShipID, &pSector) );
	m_pFactionState->SetFlagShipID(iShipID);
}

void VisibleState::SetMyFlagGroup(unsigned iGroupID)
{
	// This may not seem efficient, but we only do it occasionally (when a flag group dies).
	bool bFound = false;
	vector<ShipState*> vFactionShips =
		m_pBfPState->GetFactionShips(GetFactionName());
	for (vector<ShipState*>::iterator it = vFactionShips.begin();
		 it != vFactionShips.end(); ++it)
	{
		ShipState* pShip = *it;
		assert(pShip->GetFaction() == GetFactionName());
		assert(pShip->GetGroupID() >= 0);
		if ((unsigned)pShip->GetGroupID() == iGroupID)
		{
			bFound = true;
			pShip->SetInFlagGroup(true);
		}
	}
	assert(bFound);
}

void VisibleState::SetGroupLeader(unsigned iGroupID, unsigned iShipID)
{
	BfPSectorState* pSector;
	ShipState* pNewLeader = m_pBfPState->FindShipWithID(iShipID, &pSector);
	assert(!pNewLeader->IsGroupLeader());
	pNewLeader->SetGroupLeader(true);
}

void VisibleState::KillFaction(const String& rFaction)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		BfPSectorState* pSector = m_pBfPState->GetSectorState(*it);
		if (pSector->GetOwner() && *pSector->GetOwner() == rFaction)
			pSector->UnsetOwner();
	}
}

void VisibleState::CheckFactionDeath(const String& rFaction)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Check our home sector, if that kind of victory is enabled.
	if (pScenarioConf->GetHomeInvasionVictory())
	{
		const FactionType* pType = pScenarioConf->GetFactionType(rFaction);
		const String& rHome = pType->GetHomeSector();
		const BfPSectorState* pHomeSector = m_pBfPState->GetSectorState(rHome);
		// We only lose if it's captured by an enemy, not made neutral (if we change capturing to go mine->neutral->theirs)
		const String* pOwner = pHomeSector->GetOwner();
		if (pOwner && *pOwner != rFaction)
		{
			EventFactionDeath* pEvent = new EventFactionDeath(
				FDT_HOMECAPTURED, *pOwner, rFaction);
			GlobalEvents::Get().PushEvent(pEvent);
		}
	}

}

bool VisibleState::FactionHasNoSectors(const String& rFaction)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	// The main type of victory, wipe-out victory, occurs when we have no ships left and no sectors left.
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const BfPSectorState* pSector = m_pBfPState->GetSectorState(*it);
		if (pSector->GetOwner() && *pSector->GetOwner() == rFaction)
		{
			return false;
		}
	}
	// No sectors or ships left - wiped out.
	return true;
}


/**
 * VisibleGroup private code
 */

bool VisibleState::CanSpawnGroup(
	const StringVector& rTypes, const String& rSector,
	unsigned iGroupID, bool bFlagGroup) const
{
	assert(!rTypes.empty());

	BfPSectorState* pSector = m_pBfPState->GetSectorState(rSector);
	assert(pSector);

	// The group ID MUST not already be used (this is a check that should've been done!)
	assert(!pSector->ShipGroupExists(m_pFactionState->GetName(), iGroupID));

	// Get a single spawn point for the group (from the leader's type)
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pFirstType = pShipsConf->GetShipType(rTypes[0]);
	assert(pFirstType);

	BoundingVolume* pVol = BVFactory::Get().MakeCopy(pFirstType->GetBV());
	// TODO: Should probably use the same spawn point with the real action after trying it,
	// but shouldn't cause any major problems for now.
	Vector3 vecPoint = pSector->ChooseRandomSpawnPoint(pVol);
	delete pVol;

	for (StringVector::const_iterator it = rTypes.begin();
		 it != rTypes.end(); ++it)
	{
		// If we can't deploy any ship, we need to undo what we've done and return false.
		if (!pSector->CanAddShip(*it, m_pFactionState->GetName(), iGroupID, bFlagGroup, vecPoint))
		{
			return false;
		}
	}

	return true;
}


bool VisibleState::SpawnGroup(
	const StringVector& rTypes, const String& rSector,
	unsigned iGroupID, bool bFlagGroup)
{
	assert(!rTypes.empty());

//	cout << "getting here; sector: " << rSector << endl;
	BfPSectorState* pSector = m_pBfPState->GetSectorState(rSector);
	assert(pSector);

	// Make sure that the group ID isn't already used (the FactionController should keep track of group IDs)
	assert(!pSector->ShipGroupExists(m_pFactionState->GetName(), iGroupID));

	// Get a single spawn point for the group (from the leader's type)
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pFirstType = pShipsConf->GetShipType(rTypes[0]);
	assert(pFirstType);

	BoundingVolume* pVol = BVFactory::Get().MakeCopy(pFirstType->GetBV());
	Vector3 vecPoint = pSector->ChooseRandomSpawnPoint(pVol);
	delete pVol;

	list<int> lAdded;
	bool bFailed = false;
	bool bSetFlagShip = false;
	unsigned iAddedID;

	for (StringVector::const_iterator it = rTypes.begin();
		 it != rTypes.end(); ++it)
	{
		// If we can't deploy any ship, we need to undo what we've done and return false.
		const ShipState* pShip = pSector->AddShip(*it, m_pFactionState->GetName(), iGroupID, bFlagGroup, vecPoint);
		if (!pShip)
		{
			bFailed = true;
			break;
		}
		else
		{
			iAddedID = pShip->GetID();
			// First ship a faction deploys is the flag ship.
			if (!m_pFactionState->IsFlagShipIDSet())
			{
				bSetFlagShip = true;
				m_pFactionState->SetFlagShipID(iAddedID);
			}
			lAdded.push_back(iAddedID);
		}
	}
	// Undo and return false if we failed
	if (bFailed)
	{
		assert(!bSetFlagShip);
		for (list<int>::iterator it = lAdded.begin(); it != lAdded.end(); ++it)
		{
			assert( pSector->RemoveNewShip(*it) );
		}
		return false;
	}

	return true;
}
