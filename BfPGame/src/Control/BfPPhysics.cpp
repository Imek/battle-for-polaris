#include <Control/BfPPhysics.h>

#include <Config/BfPConfig.h>
#include <Physics/BfPPhysicsEvents.h>

using namespace JFPhysics;

SectorPhysics::SectorPhysics(BfPSectorState* pWorld)
: m_pWorld(pWorld)
{

}

SectorPhysics::~SectorPhysics()
{
	// Destructor destroys all of the EntityPhysics and Force objects
	for (std::vector<EntityPhysics*>::iterator it = m_vObjects.begin();
		 it != m_vObjects.end();)
	{
		delete *it;
		it = m_vObjects.erase(it);
	}
	for (std::vector<EntityForce*>::iterator it = m_vEntityForces.begin();
		 it != m_vEntityForces.end();)
	{
		delete *it;
		it = m_vEntityForces.erase(it);
	}
}

/**
 * SectorPhysics class code
 */

void SectorPhysics::UpdateShips(unsigned long iTime)
{
	const BfPConfig* pConf = (const BfPConfig*)GameConfig::GetInstancePtr();
	const PhysicsConfig* pPhysicsConf = (const PhysicsConfig*)pConf->GetConfig("Physics");

	// Add the flagged new ships.
	DynObjList vToAdd = m_pWorld->GetNewObjects("SHIP");
	for (std::vector<DynamicObjState*>::iterator it = vToAdd.begin();
		 it != vToAdd.end(); ++it)
	{
		ShipState* pShip = (ShipState*)*it;
		const ShipType* pType = pShip->GetShipType();

		//stringstream sout;
		//sout << "start vel: " << pShip->GetVel() << endl;
		//sout << "start accel: " << pShip->GetAccel();
		//OgreFramework::GetInstancePtr()->GetLog()->logMessage(sout.str());

		// Construct the object and assign rigid body attributes
		RigidBody* pShipBody = new RigidBody(
			pShip->GetPosition(), pShip->GetVel(), pShip->GetAccel(),
			pType->GetInvMass(), pType->GetDamping(), pType->GetMaxSpeed(),
			pType->GetInertiaTensor(), pType->GetAngularDamping(), pPhysicsConf->GetImmovableMass() );

		pShipBody->SetOrientation(pShip->GetOrientation());
		pShipBody->UpdateDerivedData();

		// Put it in a ShipPhysics object and add it to the system
		ShipPhysics* pObj = new ShipPhysics(m_pWorld, pShip->GetID(), pShipBody);
		m_vObjects.push_back(pObj);

		// Add Force objects for thrust and turning
		ShipThrustForce* pThrust = new ShipThrustForce(m_pWorld, pShip->GetID());
		m_oForces.Add(pShipBody, pThrust);
		m_vEntityForces.push_back(pThrust);
		ShipTurningForce* pTurning = new ShipTurningForce(m_pWorld, pShip->GetID());
		m_oForces.Add(pShipBody, pTurning);
		m_vEntityForces.push_back(pTurning);

		pShip->SetNew(false);

		//cout << "ADDED NEW SHIP " << endl;
	}

	// Delete the ships that have been flagged for removal.
	// (they're removed from the list when this is called, so they need to be deleted)
	DynObjList vToDelete = m_pWorld->GetRemovedObjects("SHIP");
	for (std::vector<DynamicObjState*>::iterator it = vToDelete.begin();
		 it != vToDelete.end(); ++it)
	{
		ShipState* pShip = (ShipState*)*it;
		//cout << "remove " << pShip->GetID() << " from " << GetSectorType()->GetName() << endl;
		ShipPhysics* pShipObj = NULL;

		// Remove it from the main list, and find the particle.
		for (std::vector<EntityPhysics*>::iterator itObj = m_vObjects.begin();
			 itObj != m_vObjects.end(); ++itObj)
		{
			EntityPhysics* pObj = *itObj;
			if (pObj->GetType() == "SHIP")
			{
				ShipPhysics* pPP = (ShipPhysics*)pObj;
				if (pPP->GetEntityID() == pShip->GetID())
				{
					m_vObjects.erase(itObj);
					pShipObj = pPP;
					break;
				}
			}
		}
		assert(pShipObj);
		// Remove its ship forces too.
		std::vector<EntityForce*> vMyForces;
		for (std::vector<EntityForce*>::iterator it = m_vEntityForces.begin();
			 it != m_vEntityForces.end(); )
		{
			EntityForce* pForce = *it;
			if (pForce->EntityEquals("SHIP", pShip->GetID()))
			{
				vMyForces.push_back(pForce);
				it = m_vEntityForces.erase(it);
			}
			else
			{
				++it;
			}
		}
		for (std::vector<EntityForce*>::iterator it = vMyForces.begin();
			 it != vMyForces.end(); ++it)
		{
			m_oForces.Remove(pShipObj->GetBody(), *it);
			delete *it;
		}

		// Dispose of the ship physics object and state object.
		delete pShipObj;
		delete pShip;

		//cout << "REMOVED OLD SHIP " << endl;
	}

}

void SectorPhysics::UpdateProjectiles(unsigned long iTime)
{
	// TODO: This function could perhaps be combined with UpdateShips
	const BfPConfig* pConf = (const BfPConfig*)GameConfig::GetInstancePtr();
	const PhysicsConfig* pPhysicsConf = (const PhysicsConfig*)pConf->GetConfig("Physics");

	// Add the flagged new projectiles.
	DynObjList vToAdd = m_pWorld->GetNewObjects("PROJECTILE");
	for (std::vector<DynamicObjState*>::iterator it = vToAdd.begin();
		 it != vToAdd.end(); ++it)
	{
		ProjectileState* pProj = (ProjectileState*)*it;
		const WeaponType* pWeap = pProj->GetWeaponType();

		JFPhysics::Particle* pParticle = new JFPhysics::Particle(
			pProj->GetPosition(), pProj->GetVel(), pProj->GetAccel(),
			pWeap->GetInvMass(), pWeap->GetDamping(), pWeap->GetMaxSpeed(), pPhysicsConf->GetImmovableMass());

		ProjectilePhysics* pObj =
			new ProjectilePhysics(m_pWorld, pProj->GetID(), pParticle, pWeap->GetBV());
		m_vObjects.push_back(pObj);

		// If the object has any thrust, add a thrust force.
		if (pWeap->GetMaxThrust() != 0)
		{
			ProjectileThrustForce* pThrust = new ProjectileThrustForce(m_pWorld, pProj->GetID());
			m_oForces.Add(pParticle, pThrust);
			m_vEntityForces.push_back(pThrust);
		}

		pProj->SetNew(false);
	}

	// Calling this removes these objects from the list!
	DynObjList vRemoved = m_pWorld->GetRemovedObjects("PROJECTILE");
	// Delete the ProjectilePhysics objects associated with the removed projectiles.
	for (std::vector<DynamicObjState*>::iterator it = vRemoved.begin();
		 it != vRemoved.end(); ++it)
	{
		ProjectileState* pProj = (ProjectileState*)*it;
		ProjectilePhysics* pProjObj = NULL;
		unsigned iID = pProj->GetID();

		// Find the projectilePhysics object with that ID and kill it
		for (std::vector<EntityPhysics*>::iterator itObj = m_vObjects.begin();
			 itObj != m_vObjects.end();)
		{
			EntityPhysics* pObj = *itObj;
			if (pObj->GetType() == "PROJECTILE")
			{
				ProjectilePhysics* pPP = (ProjectilePhysics*)pObj;
				if (pPP->GetID() == iID)
				{
					itObj = m_vObjects.erase(itObj);
					pProjObj = pPP;
				}
				else
					++itObj;
			}
			else
				++itObj;
		}
		assert(pProjObj);

		// TODO: Decompose to separate function
		// Delete any forces associated with this projectile.
		std::vector<EntityForce*> vMyForces;
		for (std::vector<EntityForce*>::iterator it = m_vEntityForces.begin();
			 it != m_vEntityForces.end(); )
		{
			EntityForce* pForce = *it;
			if (pForce->EntityEquals("PROJECTILE", pProj->GetID()))
			{
				vMyForces.push_back(pForce);
				it = m_vEntityForces.erase(it);
			}
			else
			{
				++it;
			}
		}
		for (std::vector<EntityForce*>::iterator it = vMyForces.begin();
			 it != vMyForces.end(); ++it)
		{
			m_oForces.Remove(pProjObj->GetObject(), *it);
			delete *it;
		}

		delete pProjObj;
		delete pProj;
	}

}

void SectorPhysics::UpdateForces(unsigned long iTime)
{
	// Then update all the forces.
	m_oForces.Update(iTime);
	// Now update the objects.
	for (std::vector<EntityPhysics*>::iterator it = m_vObjects.begin();
		 it != m_vObjects.end(); ++it)
	{
		EntityPhysics* pObj = *it;
		// Performs physics update then gives the player/projectile state the new values
		pObj->Update(iTime);
	}
}

void SectorPhysics::UpdateCollisions(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSector = m_pWorld->GetSectorType();

	// Check every object against every other object
	for (std::vector<EntityPhysics*>::iterator itObj1 = m_vObjects.begin();
		 itObj1 != m_vObjects.end(); ++itObj1)
	{
		EntityPhysics* pObj1 = *itObj1;
		JFPhysics::Particle* pPart1 = pObj1->GetObject();

		// Against world objects
		const BodyVector& rBodies = pSector->GetBodies();
		//std::vector<const BoundingVolume*> vObstacleBVs = pSector->GetObs.GetObstacleBVs();
		for (BodyVector::const_iterator it = rBodies.begin();
			 it != rBodies.end(); ++it)
		{
			const BodyInstance* pBody = *it;
			const BodyType* pBodyType = pScenarioConf->GetBodyType(pBody->GetType());
			BoundingVolume* pVol =
				new BoundingSphere(pBody->GetLocation(), pBodyType->GetBoundingRadius());
			assert(pBodyType);

			CollisionData* pData = pObj1->CollidingWith(pVol);
			if (pData)
			{
				if (pObj1->GetType() == "SHIP")
				{
					// Make a ship-object collision event
					ShipPhysics* pShipObj = (ShipPhysics*)pObj1;
					ParticleCollision oCol (pPart1, NULL, 1.0f, *pData);
					ShipObjectCollision* pEv =
						new ShipObjectCollision(oCol, pShipObj->GetShipState());
					m_oEvents.PushEvent(pEv);
				}
				else if (pObj1->GetType() == "PROJECTILE")
				{
					// Destroy projectiles that collide with static objects.
					ProjectilePhysics* pProjObj = (ProjectilePhysics*)pObj1;
					if (!pProjObj->GetProjectileState()->GetExplodeTrigger())
					{
						// TODO: Though not much is done, perhaps we should create the appropriate event for this too.
						pProjObj->GetProjectileState()->SetDestroyed();
						pProjObj->GetProjectileState()->SetVel(Vector3::ZERO);
					}
				}
			}

			delete pVol;
			delete pData;
		}

		// Against physics objects
		for (std::vector<EntityPhysics*>::iterator itObj2 = itObj1+1;
			 itObj2 != m_vObjects.end(); ++itObj2)
		{
			EntityPhysics* pObj2 = *itObj2;
			JFPhysics::Particle* pPart2 = pObj2->GetObject();
			CollisionData* pData = NULL;

			// Work out what type of event to make if the collision took place
			// (also which way around to do it)
			if (pObj1->GetType() == "SHIP")
			{
				ShipPhysics* pShipObj1 = (ShipPhysics*)pObj1;
				pData = pObj1->CollidingWith(pObj2);
				if (pData)
				{
					//cout << "COLLISION!!!!" << endl << endl;
					if (pObj2->GetType() == "SHIP")
					{
						// Ship-ship collision: ensure that they're not the same ship (!)
						ShipPhysics* pShipObj2 = (ShipPhysics*)pObj2;
						assert(pShipObj1->GetEntityID() != pShipObj2->GetEntityID());
						ParticleCollision oCol (pPart1, pPart2, 1.0f, *pData);
						ShipShipCollision* pEv =
							new ShipShipCollision(oCol,
								pShipObj1->GetShipState(), pShipObj2->GetShipState());
						m_oEvents.PushEvent(pEv);

					}
					if (pObj2->GetType() == "PROJECTILE")
					{
						// We don't collide with our own projectiles, or multiple times (when it's exploding)
						ProjectilePhysics* pProjObj2 = (ProjectilePhysics*)pObj2;
						if (!pProjObj2->GetProjectileState()->GetExplodeTrigger() &&
							pProjObj2->GetProjectileState()->GetOwnerID() != pShipObj1->GetEntityID())
						{
							// Assuming it's not the same ship, make the collision event.
							ParticleCollision oCol (pPart1, pPart2, 1.0f, *pData);
							ShipProjectileCollision* pEv =
								new ShipProjectileCollision(oCol, m_pWorld,
									pShipObj1->GetShipState(), pProjObj2->GetProjectileState());
							m_oEvents.PushEvent(pEv);
						}
					}
				}
			}
			else if (pObj2->GetType() == "SHIP")
			{
				ShipPhysics* pShipObj2 = (ShipPhysics*)pObj2;
				pData = pObj2->CollidingWith(pObj1);
				if (pData)
				{
					if (pObj1->GetType() == "SHIP")
					{
							// Ship-ship collision: ensure that they're not the same ship (!)
							ShipPhysics* pShipObj1 = (ShipPhysics*)pObj1;
							assert(pShipObj1->GetEntityID() != pShipObj2->GetEntityID());
							ParticleCollision oCol (pPart2, pPart1, 1.0f, *pData);
							ShipShipCollision* pEv =
								new ShipShipCollision(oCol,
									pShipObj2->GetShipState(), pShipObj1->GetShipState());
							m_oEvents.PushEvent(pEv);
					}
					if (pObj1->GetType() == "PROJECTILE")
					{
						// We don't collide with our own projectiles.
						ProjectilePhysics* pProjObj1 = (ProjectilePhysics*)pObj1;
						if (!pProjObj1->GetProjectileState()->GetExplodeTrigger() &&
							pProjObj1->GetProjectileState()->GetOwnerID() != pShipObj2->GetEntityID())
						{
							// Assuming it's not the same ship, make the collision event.
							ParticleCollision oCol (pPart2, pPart1, 1.0f, *pData);
							ShipProjectileCollision* pEv =
								new ShipProjectileCollision(oCol, m_pWorld,
									pShipObj2->GetShipState(), pProjObj1->GetProjectileState());
							m_oEvents.PushEvent(pEv);
						}
					}
				}

			}
			// falls through to here if neither object was a ship
			delete pData;
		}
	}
}


void SectorPhysics::UpdateEvents(unsigned long iTime)
{
	Event* pEvent = m_oEvents.PopEvent();
	while (pEvent)
	{
		pEvent->Resolve(iTime);
		//cout << "Event: " << pEvent->GetString() << endl;
		delete pEvent;
		pEvent = m_oEvents.PopEvent();
	}

}

void SectorPhysics::Update(unsigned long iTime)
{
	// Update the addition and removal of things managed by the physics engine.
	UpdateShips(iTime);
	UpdateProjectiles(iTime);

	// Then update the actual physics.
	UpdateCollisions(iTime);
	UpdateEvents(iTime);
	UpdateForces(iTime);

}


/**
 * GamePhysics code
 */

GamePhysics::GamePhysics(BfPState* pState)
: m_pState(pState)
{
	assert(pState);
	SectorList& rSectors = pState->GetSectorList();
	for (SectorList::iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		String strSector = it->first;
		BfPSectorState* pSector = it->second;
		m_mSectors[strSector] = new SectorPhysics(pSector);
	}
}

GamePhysics::~GamePhysics()
{
	for (PhysicsList::iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		delete it->second;
	}
	m_mSectors.clear();
}

void GamePhysics::Update(unsigned long iTime)
{
	for (PhysicsList::iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		SectorPhysics* pPhysics = it->second;
		assert(pPhysics);
		//UpdateWarpedShips(pPhysics);
		pPhysics->Update(iTime);
	}
}
