#include <Effects/BfPParticles.h>

/**
 * ParticleEffect public code
 */

ParticleEffect::ParticleEffect(String strName, PARTICLE_EFFECT_TYPE iType,
	String strParticles, unsigned long * pDuration)
: m_strName(strName)
, m_iType(iType)
, m_iAge(0)
, m_pDuration(pDuration)
, m_pNode(NULL)
, m_pParentNode(NULL)
, m_vecStoredVel(Vector3::ZERO)
{
	Ogre::SceneManager* pSceneMgr = OgreFramework::GetInstance().GetSceneManager();
	m_pNode = pSceneMgr->createSceneNode(strName + "Particles_Node");

	Ogre::ParticleSystem* pParticles =
		pSceneMgr->createParticleSystem( strName + "Particles", strParticles );
	m_pNode->attachObject(pParticles);

	assert(m_pNode);
}

ParticleEffect::~ParticleEffect()
{
	Ogre::SceneManager* pSceneMgr = OgreFramework::GetInstance().GetSceneManager();

// The scene nodes themselves is disposed of by the sector scene class's destructor
	//assert(m_pNode);
	m_pNode->removeAndDestroyAllChildren();
//	Ogre::ParticleSystem* pParticles = (Ogre::ParticleSystem*)
//		m_pNode->getAttachedObject(m_strName + "Particles");
//	pParticles->detatchFromParent();
	pSceneMgr->destroyParticleSystem(m_strName + "Particles");
	//cout << "destroy scene node: " << m_strName << endl;
	pSceneMgr->destroySceneNode(m_pNode->getName());
	delete m_pDuration;

	//cout << "destroyed particle effect: " << m_pNode->getName() << endl;
}

bool ParticleEffect::GetFinished() const
{
	return (m_pDuration && *m_pDuration <= m_iAge);
}

void ParticleEffect::SetOrientation(const Quaternion& rNew)
{
	assert(m_pNode);
	// TODO: Work out why this isn't working correctly (orient with ship)
	m_pNode->setOrientation(rNew);
}

const Vector3& ParticleEffect::GetPosition() const
{
	assert(m_pNode);
	return m_pNode->getPosition();
}

void ParticleEffect::SetPosition(const Vector3& rNew)
{
	assert(m_pNode);
	m_pNode->setPosition(rNew);
}

void ParticleEffect::TranslateAllParticles(const Vector3& rTranslate)
{
	assert(m_pNode);
	m_pNode->translate(rTranslate);
	Ogre::ParticleSystem* pParticles = GetParticles();
	size_t iNoParticles = pParticles->getNumParticles();
	for (size_t i = 0; i < iNoParticles; ++i)
		pParticles->getParticle(i)->position += rTranslate;
}

void ParticleEffect::SetVelocity(const Vector3& rVel)
{
	m_vecStoredVel = rVel;
}

void ParticleEffect::Update(unsigned long iTime)
{
	//Ogre::ParticleSystem* pParticles = GetParticles();
	//cout << "particles: " << pParticles->getNumParticles() << endl;


	if (m_pDuration)
		m_iAge += iTime;
}

void ParticleEffect::Attach(Ogre::SceneNode* pParent)
{
	//cout << "particles attached" << endl;
	assert(!m_pParentNode);
	assert(pParent);
	m_pParentNode = pParent;
	// Remove old effects if they have a name (TODO: They should remove themselves properly after timing out..)
	//const String& rName = m_pNode->getName();
	m_pParentNode->addChild(m_pNode);
}

void ParticleEffect::Detach()
{
	//cout << "particles detached" << endl;
	assert(m_pParentNode);
	m_pParentNode->removeChild(m_pNode);
}


/**
 * ParticleEffect private code
 */

Ogre::ParticleSystem* ParticleEffect::GetParticles()
{
	// The particle system is the only object attached to the node
	//cout << "num attached: " << m_pNode->numAttachedObjects() << endl;
	assert(m_pNode && m_pNode->numAttachedObjects() == 1);
	return (Ogre::ParticleSystem*)
		m_pNode->getAttachedObject(0);
}


/**
 * EngineParticles public code
 */

EngineParticles::EngineParticles(String strName, PARTICLE_EFFECT_TYPE iType, String strParticles,
	String strObjType, unsigned iObjID)
: ParticleEffect(strName, iType, strParticles, NULL)
, m_strObjType(strObjType)
, m_iObjID(iObjID)
{
	Ogre::ParticleSystem* pParticles = GetParticles();
	assert(pParticles);

	size_t iNo = pParticles->getNumEmitters();
	m_arrMaxRates = new Real[iNo];
	m_arrMaxVels = new Real[iNo];
	for (size_t i = 0; i < iNo; ++i)
	{
		Ogre::ParticleEmitter* pEmitter = pParticles->getEmitter(i);
		m_arrMaxRates[i] = pEmitter->getEmissionRate();
		m_arrMaxVels[i] = pEmitter->getParticleVelocity();
	}

	SetIntensity(0);
}

EngineParticles::~EngineParticles()
{
	delete [] m_arrMaxRates;
	delete [] m_arrMaxVels;
}

void EngineParticles::SetIntensity(const Real& rVal)
{
	assert(rVal >= 0.0 && rVal <= 1.0);
	// Particles look dodgy if the intensity is set too low (TODO: put in config)
	Real fVal = (rVal > 0.0 && rVal < 0.3) ? 0.3 : rVal;
	Ogre::ParticleSystem* pParticles = GetParticles();
	assert(pParticles);
	//cout << "new quota: " << (size_t)((Real)m_iMaxQuota * rVal) << endl;
	size_t iNo = pParticles->getNumEmitters();
	for (size_t i = 0; i < iNo; ++i)
	{
		Ogre::ParticleEmitter* pEmitter = pParticles->getEmitter(i);
		pEmitter->setEmissionRate(m_arrMaxRates[i] * fVal);
		pEmitter->setParticleVelocity(m_arrMaxVels[i] * fVal);
	}
}

const EntityState* EngineParticles::GetState(const BfPSectorState* pSector) const
{
	assert(pSector);
	const EntityState* pState = (const EntityState*)
		pSector->GetObjState(m_strObjType, m_iObjID);
	return pState; // Could be null
}
