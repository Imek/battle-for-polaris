#include <Physics/BfPPhysicsEvents.h>

#include <Config/BfPConfig.h>
#include <Config/BfPConfigPhysics.h>
#include <Config/BfPConfigGeneral.h>

using namespace JFPhysics;

/**
 * ShipObjectCollision code
 */

ShipObjectCollision::ShipObjectCollision(ParticleCollision oCollision, ShipState* pShip)
: Event("EV_COL_SHIPOBJ")
, m_oCollision(oCollision)
, m_pShip(pShip)
{
	assert(m_pShip);
}

ShipObjectCollision::ShipObjectCollision(const ShipObjectCollision& rObj)
: Event("EV_COL_SHIPOBJ")
, m_oCollision(rObj.m_oCollision)
, m_pShip(rObj.m_pShip)
{
	assert(m_pShip);
}

string ShipObjectCollision::GetString() const
{
	std::stringstream sout;
	// NOTE: Debug string stuff goes here
	/*sout << "ShipCollisionEvent {" << endl;
	sout << "BallPos: " << m_oBall.m_vecPos.GetString()
		 << ", BallRadius: " << m_oBall.m_fRadius << "{" << endl;*/
	return sout.str();
}

void ShipObjectCollision::Resolve(unsigned long iTime)
{
	const GameConfig* pGameConf = GameConfig::GetInstancePtr();
	const PhysicsConfig* pPhysicsConf = (const PhysicsConfig*)pGameConf->GetConfig("Physics");
	const GeneralConfig* pGenConf = (const GeneralConfig*)pGameConf->GetConfig("General");

	// Resolve the collision, and see what the velocity change was.
	Particle* pOne = m_oCollision.m_pOne;
	Particle* pTwo = m_oCollision.m_pTwo;

	Vector3 vecVelBefore = pOne->GetVel();
	m_oCollision.Resolve(iTime);
	Vector3 vecVelAfter = pOne->GetVel();
	// Apply hit-point damage to the ship, relative to velocity change and mass.
	Real fVelChange = (vecVelAfter - vecVelBefore).length();

	Real fP2Mass;
	if (pTwo == NULL)
		fP2Mass = pPhysicsConf->GetImmovableMass();
	else
		fP2Mass = pTwo->GetMass();

	Real fForce = fVelChange * fP2Mass*pOne->GetInvMass();
	fForce *= pGenConf->GetCollisionDmgMult();

	m_pShip->ApplyDamage((int)fForce);
}

/**
 * @brief ShipShipCollision code
 */

ShipShipCollision::ShipShipCollision(ParticleCollision oCollision, ShipState* pShip1, ShipState* pShip2)
: Event("EV_COL_SHIPSHIP")
, m_oCollision(oCollision)
, m_pShip1(pShip1)
, m_pShip2(pShip2)
{

}

ShipShipCollision::ShipShipCollision(const ShipShipCollision& rObj)
: Event("EV_COL_SHIPSHIP")
, m_oCollision(rObj.m_oCollision)
, m_pShip1(rObj.m_pShip1)
, m_pShip2(rObj.m_pShip2)
{

}

string ShipShipCollision::GetString() const
{
	std::stringstream sout;
	// NOTE: Debug string stuff goes here
	/*sout << "ShipCollisionEvent {" << endl;
	sout << "BallPos: " << m_oBall.m_vecPos.GetString()
		 << ", BallRadius: " << m_oBall.m_fRadius << "{" << endl;*/
	return sout.str();
}

void ShipShipCollision::Resolve(unsigned long iTime)
{
	const GeneralConfig* pConfig = (const GeneralConfig*)GameConfig::GetInstancePtr()->GetConfig("General");

	// Resolve the collision, and see what the velocity change was.
	Particle* pOne = m_oCollision.m_pOne;
	Particle* pTwo = m_oCollision.m_pTwo;
	assert(pOne && pTwo);

	Vector3 vecVel1Before = pOne->GetVel();
	Vector3 vecVel2Before = pTwo->GetVel();
	m_oCollision.Resolve(iTime);
	Vector3 vecVel1After = pOne->GetVel();
	Vector3 vecVel2After = pTwo->GetVel();
	// Apply hit-point damage to the ship, relative to velocity change and mass.
	Real fVel1Change = (vecVel1After - vecVel1Before).length();
	Real fVel2Change = (vecVel2After - vecVel2Before).length();

	Real fForce1 = fVel1Change * pTwo->GetMass()*pOne->GetInvMass();
	Real fForce2 = fVel2Change * pOne->GetMass()*pTwo->GetInvMass();
	fForce1 *= pConfig->GetCollisionDmgMult();
	fForce2 *= pConfig->GetCollisionDmgMult();

	m_pShip1->ApplyDamage((int)fForce1);
	m_pShip2->ApplyDamage((int)fForce2);
}


/**
 * ShipProjectileCollision code
 */

ShipProjectileCollision::ShipProjectileCollision(
	ParticleCollision oCollision,
	const BfPSectorState* pSectorState, ShipState* pShip, ProjectileState* pProjectile)
: Event("EV_COL_SHIPPROJ")
, m_oCollision(oCollision)
, m_pSectorState(pSectorState)
, m_pShip(pShip)
, m_pProjectile(pProjectile)
{
	assert(m_pSectorState);
}

ShipProjectileCollision::ShipProjectileCollision(const ShipProjectileCollision& rObj)
: Event("EV_COL_SHIPPROJ")
, m_oCollision(rObj.m_oCollision)
, m_pSectorState(rObj.m_pSectorState)
, m_pShip(rObj.m_pShip)
, m_pProjectile(rObj.m_pProjectile)
{

}

string ShipProjectileCollision::GetString() const
{
	std::stringstream sout;
	// NOTE: Debug string stuff goes here
	/*sout << "ShipCollisionEvent {" << endl;
	sout << "BallPos: " << m_oBall.m_vecPos.GetString()
		 << ", BallRadius: " << m_oBall.m_fRadius << "{" << endl;*/
	return sout.str();
}

void ShipProjectileCollision::Resolve(unsigned long iTime)
{
	const GeneralConfig* pConf = (const GeneralConfig*)
		BfPConfig::GetInstance().GetConfig("General");
	// Apply the damage and the force from the projectile's weapon config.
	const WeaponType* pWeap = m_pProjectile->GetWeaponType();
	int iDamage =  pWeap->GetDamage();

	Vector3 vecForce =
		m_oCollision.m_pTwo->GetVel().normalisedCopy() * pWeap->GetMomentumApplied();
	m_oCollision.m_pOne->AddForce(vecForce);

	int iHPBefore = m_pShip->GetHP();
	if (pConf->GetFriendlyFireEnabled() ||
		m_pProjectile->GetFaction() != m_pShip->GetFaction())
		m_pShip->ApplyDamage(iDamage);
	int iHPAfter = m_pShip->GetHP();

	// Check if this ship died; if so, we need to set its killer (for events that are produced elsewhere)
	if (iHPBefore > 0 && iHPAfter <= 0)
	{
		m_pShip->SetKillerFaction(m_pProjectile->GetFaction());
	}

	// Then destroy the projectile and set its velocity to that of what it impacted (for the explosion).
	m_pProjectile->SetDestroyed();
	m_pProjectile->SetVel(m_pShip->GetVel());
}
