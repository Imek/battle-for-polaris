#include <Physics/BfPPhysicsShip.h>

/**
 * ShipPhysics code
 */

ShipPhysics::ShipPhysics(BfPSectorState* pWorld, unsigned iShipID, RigidBody* pBody)
: EntityPhysics(pBody, pWorld, "SHIP", iShipID, NULL)
{
	const ShipState* pShip = GetShipState();
	const ShipType* pType = pShip->GetShipType();
	m_pBV = BVFactory::Get().MakeCopy(pType->GetBV());
	if (m_pBV) m_pBV->SetPosition(pShip->GetPosition());
	if (m_pBV) m_pBV->SetOrientation(pShip->GetOrientation());
}

void ShipPhysics::Update(unsigned long iTime)
{
	// TODO: Some of this could be moved up to a generic EntityPhysics update called at the start
	// of each subclass's update function.

//	cout << "Updating ShipPhysics in " << m_pWorld->GetSectorType()->GetName() << endl;
//	cout << "(ID: " << m_iShipID << ")" << endl;

	//cout << "shipphysics update" << endl;

	ShipState* pShip = GetShipState();
	RigidBody* pBody = (RigidBody*)m_pObject;

	// Also update the motion of the ship.
	//UpdateRespawning(iTime);

	// If it's exploding, we may have changed its velocity (for the death explosion), so update the body from that.
	if (pShip->GetExplodeTrigger())
	{
		pBody->ZeroForces();
		pBody->SetVel(pShip->GetVel());
		pBody->SetAccel(Vector3::ZERO);
		//pBody->SetMaxSpeed(0.0f);
		//pBody->SetAngularVelocity(Vector3::ZERO);
	}
	// If warping, do constant speed rather than proper physics.
	else if (pShip->GetWarping())
	{
		// TODO: Put warp speed in config
		Real fWarpSpeed = pShip->GetShipType()->GetMaxSpeed() * 10;
		pBody->SetVel( pShip->GetFaceDir() * fWarpSpeed);
		pBody->SetOrientation(pShip->GetOrientation());
		pBody->SetMaxSpeed(fWarpSpeed);
		pBody->SetAngularVelocity(Vector3::ZERO);
	}
	else
	{
		pBody->SetMaxSpeed( pShip->GetShipType()->GetMaxSpeed() );
	}

	pBody->Update(iTime);

	// Then update the relevant properties of the ShipState
	pShip->SetPosition(pBody->GetPos());
	pShip->SetVel(pBody->GetVel());
	pShip->SetAccel(pBody->GetAccel());

	pShip->SetOrientation(pBody->GetOrientation());

	// Update the properties of the bounding volume, if applicable.
	if (m_pBV)
	{
		m_pBV->SetPosition(pBody->GetPos());
		m_pBV->SetOrientation(pBody->GetOrientation());
	}

	//cout << "Position: " << pBody->m_vecPos << endl;
	//cout << "Velocity: " << pBody->GetVel() << endl;
	//cout << "Facing direction: " << pShip->m_vecFaceDir.GetString() << endl;

//	cout << "done updating ShipPhysics" << endl;
}

/**
 * ShipThrustForce code
 */

void ShipThrustForce::Update(JFPhysics::Particle* pParticle, unsigned long iTime)
{
	assert(pParticle);
	assert(iTime > 0);
	Real fTime = (Real)iTime;

	//cout << "updating ShipThrustForce in: " << GetSector()->GetSectorType()->GetName() << endl;

	// Provided parameter should be the ship's rigid body object
	// NOTE: Maybe better to have a type field so that we can validate this?
	//cout << "my ID is: " << GetID() << endl;
	ShipState* pState = GetShipState();

	// If it's warping or dead, don't do anything.
	if (pState->GetWarping() || pState->IsDead())
		return;

	RigidBody* pBody = (RigidBody*)pParticle;
	ControlState& rControls = pState->GetControls();
	const ShipType* pShipConf = pState->GetShipType();
	Vector3 vecThrust;
	// Apply force in opposite direction of velocity
	if (rControls.m_bBrake || pState->GetWarping())
	{
		rControls.m_bZeroThrust = true;
		Real fBrakeThrust = pShipConf->GetBrakeThrust();
		// Work out the maximum thrust that would halt the ship.
		Real fMaxThrust = pState->GetVel().length() / fTime;
		fMaxThrust *= pShipConf->GetMass();
		if (fBrakeThrust > fMaxThrust) fBrakeThrust = fMaxThrust;
		// The thrust vector is in the opposite direction of velocity.
		vecThrust = -(pState->GetVel().normalisedCopy());
		vecThrust *= fBrakeThrust;
	}
	else // Standard thrust force
	{
		vecThrust = pState->GetFaceDir();
		//assert(vecThrust.length() == 1);
		vecThrust *= pState->GetThrust();

		Real fStrafe = pShipConf->GetStrafeThrust();

		const Vector3& rRight = pState->GetFaceRight();
		const Vector3& rNorm = pState->GetFaceNorm();

		if (rControls.m_bStrafeL)
			vecThrust -= rRight * fStrafe;
		else if (rControls.m_bStrafeR)
			vecThrust += rRight * fStrafe;
		else if (pState->GetThrust() > 0.f)
		{
			// Correct our existing strafe velocity to zero
			Vector3 vecStrafeBrake = rRight * rRight.dotProduct(pState->GetVel());
			vecStrafeBrake.normalise();
			vecStrafeBrake *= pShipConf->GetBrakeThrust();

			//cout << "correct strafe" << endl;
			vecThrust -= vecStrafeBrake;

		}

		//cout << "StrafeU: " << rControls.m_bStrafeU << endl;

		if (rControls.m_bStrafeU)
			vecThrust += rNorm * fStrafe;
		else if (rControls.m_bStrafeD)
			vecThrust -= rNorm * fStrafe;
		else if (pState->GetThrust() > 0.f)
		{
			// Correct our existing strafe velocity to zero
			Vector3 vecStrafeBrake = rNorm * rNorm.dotProduct(pState->GetVel());
			vecStrafeBrake.normalise();
			vecStrafeBrake *= pShipConf->GetBrakeThrust();

			//cout << "correct strafe" << endl;
			vecThrust -= vecStrafeBrake;
		}
	}

	pBody->AddForce(vecThrust);

	//cout << "done updating ShipThrustForce" << endl;
}

/**
 * ShipTurningForce code
 */

void ShipTurningForce::Update(JFPhysics::Particle* pParticle, unsigned long iTime)
{
	// Turn the ship based on the control state
	ShipState* pShip = GetShipState();
	assert(pShip);

	// If it's warping or dead, don't do anything.
	if (pShip->GetWarping() || pShip->IsDead())
		return;

	const ShipType* pShipType = pShip->GetShipType();
	RigidBody* pBody = (RigidBody*)pParticle;
	ControlState& rControls = pShip->GetControls();

	Vector3 vecTurnVel = Vector3::ZERO;

	// Interpolate yaw
	static Real fRate;
	static Real fAmount;
	static Real fMax;

	fRate = pShipType->GetYawRate() * iTime;
	fAmount = fRate * rControls.GetYaw();
	//cout << "yaw amount: " << fAmount << endl;
	fMax = pShipType->GetMaxYaw() * Ogre::Math::Abs( rControls.GetYaw() );
	if (fAmount == 0)
	{
		if (Math::Abs(m_fYaw) < Math::Abs(fRate))
			m_fYaw = 0;
		else
		{
			if (m_fYaw > 0)
				m_fYaw -= fRate;
			else
				m_fYaw += fRate;
		}
	}
	else
	{
		m_fYaw += fAmount;
		if (m_fYaw > fMax) m_fYaw = fMax;
		if (m_fYaw < -fMax) m_fYaw = -fMax;
	}

	// Do the same for pitch
	fRate = pShipType->GetPitchRate() * iTime;
	fAmount = fRate * rControls.GetPitch();
	//cout << "pitch amount: " << fAmount << endl;
	fMax = pShipType->GetMaxPitch() * Ogre::Math::Abs( rControls.GetPitch() );
	if (fAmount == 0)
	{
		if (Math::Abs(m_fPitch) < Math::Abs(fRate))
			m_fPitch = 0;
		else
		{
			if (m_fPitch > 0)
				m_fPitch -= fRate;
			else
				m_fPitch += fRate;
		}
	}
	else
	{
		m_fPitch += fAmount;
		if (m_fPitch > fMax) m_fPitch = fMax;
		if (m_fPitch < -fMax) m_fPitch = -fMax;
	}

	// Do the same for roll
	fRate = pShipType->GetRollRate() * iTime;
	fAmount = fRate * rControls.GetRoll();
	fMax = pShipType->GetMaxRoll();
	if (fAmount == 0)
	{
		if (Math::Abs(m_fRoll) < Math::Abs(fRate))
			m_fRoll = 0;
		else
		{
			if (m_fRoll > 0)
				m_fRoll -= fRate;
			else
				m_fRoll += fRate;
		}
	}
	else
	{
		m_fRoll += fAmount;
		if (m_fRoll > fMax) m_fRoll = fMax;
		if (m_fRoll < -fMax) m_fRoll = -fMax;
	}

	// Apply our resultant yaw/pitch/roll values to the turn velocity to give to the body.
	Vector3 vecFaceNorm = pShip->GetFaceNorm();
	Vector3 vecFaceDir = pShip->GetFaceDir();
	Vector3 vecRight = pShip->GetFaceRight();

	vecTurnVel += (vecFaceNorm * m_fYaw);
	vecTurnVel += (vecRight * -m_fPitch);
	vecTurnVel += (vecFaceDir * m_fRoll);

	// NOTE: Maybe incorporate this better rather than just setting the velocity?
	pBody->SetAngularVelocity( vecTurnVel );

	//pBody->AddForceAtPoint

}
