#include <Physics/BfPPhysicsProjectile.h>

#include <Util/Control.h>

#include <Config/BfPConfig.h>

/**
 * ProjectilePhysics code
 */

ProjectilePhysics::ProjectilePhysics(
	BfPSectorState* pWorld, unsigned iProjID, JFPhysics::Particle* pParticle, const BoundingVolume* pBV)
: EntityPhysics(pParticle, pWorld, "PROJECTILE", iProjID, pBV), m_pWorld(pWorld), m_iProjID(iProjID)
{
	assert(pWorld);
	assert(m_pWorld->GetObjState("PROJECTILE", iProjID));
	const WeaponType* pType = GetProjectileState()->GetWeaponType();
	m_pBV = BVFactory::Get().MakeCopy(pType->GetBV());
	if (m_pBV) m_pBV->SetPosition(GetState()->GetPosition());
	if (m_pBV) m_pBV->SetOrientation(GetState()->GetOrientation());
}

void ProjectilePhysics::Update(unsigned long iTime)
{
	//cout << "proj vel: " << m_pObject->GetVel() << endl;

	ProjectileState* pState = GetProjectileState();

	// Check if we're dead first, so we can update the velocity set from the collision event.
	if (pState->GetExplodeTrigger())
	{
		m_pObject->ZeroForces();
		m_pObject->SetVel(pState->GetVel());
		m_pObject->SetAccel(Vector3::ZERO);
	}


	// Update (integrate) the motion of the particle
	m_pObject->Update(iTime);

	// Then update the relevant properties of the state.
	pState->SetPosition(m_pObject->GetPos());
	pState->SetVel(m_pObject->GetVel());
	pState->SetAccel(m_pObject->GetAccel());

	//cout << "Projectile position: " << m_pObject->GetPos() << endl;

	// NOTE: Projectile as particle means no orientation in bounding volume here
	// Update the properties of the bounding volumes
	if (m_pBV)
	{
		m_pBV->SetPosition(m_pObject->GetPos());
		//m_pComplexBV->SetOrientation(m_pObject->m_qOrientation);
	}
}

/**
 * ProjectileThrustForce code
 */

void ProjectileThrustForce::Update(JFPhysics::Particle* pParticle, unsigned long iTime)
{
	static bool bInRange;
	static Vector3 vecPredictPos;
	static Vector3 vecAxis;
	static Vector3 vecMyDir;
	static Radian oMaxTurn;
	static Radian oToTurn;
	static Quaternion qRot;

	assert(pParticle);

	// Provided parameter should be the ship's rigid body object
	// NOTE: Maybe better to have a type field so that we can validate this?
	//cout << "my ID is: " << GetID() << endl;
	ProjectileState* pState = GetProjectileState();
	vecMyDir = pState->GetFaceDir();
	const WeaponType* pWeapType = pState->GetWeaponType();

	int iTarget = pState->GetOwnerTarget();
	if (pWeapType->GetHoming() && iTarget >= 0)
	{
		// Get the position of the target and turn towards it.
		ShipState* pTarget = (ShipState*)m_pWorld->GetObjState("SHIP", iTarget);
		if (pTarget)
		{
			// Lead the target to get the point towards which to face
			vecMyDir = UtilControl::LeadTarget(
				pState->GetPosition(), pState->GetVel(), pWeapType->GetAccel(), pState->GetVel().length(), pState->GetTimeLeft(),
				pTarget->GetPosition(), pTarget->GetVel(), &bInRange, &vecPredictPos);
			pState->SetFaceDir(vecMyDir);
		}
	}

	// In any case, do the thrust like normal
	// vecMyDir at this point is the same as the face dir  whether we did the above code or not
	vecMyDir *= pWeapType->GetMaxThrust();
	pParticle->AddForce(vecMyDir);
}
