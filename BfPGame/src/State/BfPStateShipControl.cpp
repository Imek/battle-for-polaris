#include <State/BfPStateShipControl.h>

#include <Config/BfPConfig.h>
#include <Config/BfPConfigScenario.h>

/**
 * WeaponState code
 */

WeaponState::WeaponState(String strType)
: m_strType(strType)
, m_iCooldown(0)
, m_iNewProjectiles(0)
{
	Reset();
	// An assertion will fail if it is invalid.
	const WeaponType* pType = GetType();
	m_iAmmo = pType->GetMaxAmmo();

}

const WeaponType* WeaponState::GetType() const
{
	const WeaponsConfig* pWeapsConf = (WeaponsConfig*)
		GameConfig::GetInstance().GetConfig("Weapons");
	const WeaponType* pType = pWeapsConf->GetWeaponType(m_strType);
	assert(pType);
	return pType;
}

void WeaponState::Reset()
{
	const WeaponType* pWeap = GetType();
	m_iCooldown = 0;
	m_iAmmo = pWeap->GetMaxAmmo();
}

bool WeaponState::Fire()
{
	if (!ReadyToFire()) return false;
	const WeaponType* pType = GetType();
	assert(pType);
	m_iCooldown = pType->GetCooldownTime();
	if (!pType->GetInfiniteAmmo()) --m_iAmmo;
	assert(m_iAmmo >= 0 && m_iCooldown > 0);
	return true;
}

void WeaponState::Update(unsigned long iTime)
{
	m_iCooldown -= iTime;
	if (m_iCooldown < 0) m_iCooldown = 0;
}


/**
 * WeaponStateList code
 */

void WeaponStateList::AddWeapon(String strType)
{
	m_vList.push_back( WeaponState(strType) );
}

const WeaponState& WeaponStateList::GetWeaponState(size_t iNo) const
{
	assert(iNo < m_vList.size());
	return m_vList[iNo];
}

WeaponState& WeaponStateList::GetWeaponState(size_t iNo)
{
	assert(iNo < m_vList.size());
	return m_vList[iNo];
}

void WeaponStateList::GetNextGroup(bool bNext, String& rGroup, vector<unsigned>& rIDs) const
{
	// If we have none available, clear them and return
	if (m_vList.empty())
	{
		rGroup = "";
		rIDs.clear();
		return;
	}

	if (rIDs.empty()) // If none selected, select the first
	{
		// if bNext is true, pick the first. else, pick the last.
		const WeaponState& rWeap = bNext ?
			*m_vList.begin() : m_vList.back();
		rGroup = rWeap.GetTypeName();
		SetWeaponIDs(rGroup, rIDs);
		return;
	}

	// Otherwise, get the ID of the final/first weapon in the current group to work out the next/previous
	unsigned iWeapID;
	size_t iSize = m_vList.size();

	if (bNext) // Next: or first if we have the last selected.
	{
		iWeapID = rIDs.back();
		if (iWeapID >= iSize-1) iWeapID = 0;
		else ++iWeapID;
	}
	else // Previous: or last if we have the first selected
	{
		iWeapID = rIDs.front();
		if (iWeapID == 0) iWeapID = iSize-1;
		else --iWeapID;
	}
	// Finally set rGroup and rIDs to the new values.
	const WeaponState& rWeap = m_vList[iWeapID];
	rGroup = rWeap.GetTypeName();
	SetWeaponIDs(rGroup, rIDs);
}

void WeaponStateList::SetWeaponIDs(const String& rGroup, vector<unsigned>& rIDs) const
{
	assert(rGroup != "");
	assert(!m_vList.empty());
	rIDs.clear();
	for (size_t i = 0; i < m_vList.size(); ++i)
	{
		const WeaponState& rWeap = m_vList[i];
		if (rWeap.GetTypeName() == rGroup) rIDs.push_back(i);
	}
}

void WeaponStateList::Update(unsigned long iTime)
{
	for (vector<WeaponState>::iterator it = m_vList.begin();
		 it != m_vList.end(); ++it)
	{
		it->Update(iTime);
	}
}



/**
 * ControlState code
 */

ControlState::ControlState(String strShipType)
: m_bFiringGroup(false)
, m_bFiringAll(false)
, m_bSelfDestruct(false)
, m_strShipType(strShipType)
, m_strActiveWeapGroup("")
, m_iTargetID(-1)
, m_pTargetSector(NULL)
, m_fYaw(0)
, m_fPitch(0)
, m_fRoll(0)
{
	// An exception is thrown here if the type was invalid.
	const ShipType* pShipType = GetShipType();

	// Get the weapons load-out from the ship type
	// (TODO: custom load-outs separate of the ship config, and custom weapon groups)
	const StringVector& rWeaps = pShipType->GetWeapons();
	for (size_t i = 0; i < rWeaps.size(); ++i)
	{
		const String& rWeap = rWeaps[i];
		// First active weapon group is the first type in the config
		if (m_strActiveWeapGroup == "") m_strActiveWeapGroup = rWeap;
		if (rWeap == m_strActiveWeapGroup) m_vActiveWeaps.push_back(i); // Add the ID to active
		m_vAllWeaps.push_back(i); // Allweaps always takes the ID
		// Add a weapon of that type to the list
		m_oWeapons.AddWeapon( rWeap );
		// Also put in a blank autoaim value for each weapon
		m_vAutoAim.push_back( Vector3::ZERO );
	}

	Reset();
}

ControlState::ControlState(const ControlState& rToCopy)
: m_bIncThrust(rToCopy.m_bIncThrust)
, m_bRedThrust(rToCopy.m_bRedThrust)
, m_bZeroThrust(rToCopy.m_bZeroThrust)
, m_bStrafeL(rToCopy.m_bStrafeL)
, m_bStrafeR(rToCopy.m_bStrafeR)
, m_bStrafeU(rToCopy.m_bStrafeU)
, m_bStrafeD(rToCopy.m_bStrafeD)
, m_bBrake(rToCopy.m_bBrake)
//, m_bWarp(rToCopy.m_bWarp)
, m_bFiringGroup(rToCopy.m_bFiringGroup)
, m_bFiringAll(rToCopy.m_bFiringAll)
, m_bSelfDestruct(rToCopy.m_bSelfDestruct)
, m_vAutoAim(rToCopy.m_vAutoAim)
, m_strShipType(rToCopy.m_strShipType)
, m_strActiveWeapGroup(rToCopy.m_strActiveWeapGroup)
, m_vActiveWeaps(rToCopy.m_vActiveWeaps)
, m_vAllWeaps(rToCopy.m_vAllWeaps)
, m_oWeapons(rToCopy.m_oWeapons)
, m_iTargetID(rToCopy.m_iTargetID)
, m_pTargetSector(rToCopy.m_pTargetSector ?
	new String(*rToCopy.m_pTargetSector) : NULL)
, m_fYaw(rToCopy.m_fYaw)
, m_fPitch(rToCopy.m_fPitch)
, m_fRoll(rToCopy.m_fRoll)
//, m_fTargetSpeed(rToCopy.m_fTargetSpeed)
{

}

const ShipType* ControlState::GetShipType() const
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const ShipType* pShipType = pShipsConf->GetShipType(m_strShipType);
	if (!pShipType)
		throw JFUtil::Exception("ControlState", "GetShipType",
			"Had an invalid ship type", m_strShipType);

	return pShipType;
}

bool ControlState::HasActiveWeapon() const
{
	// When firing all, all weapons are "active". Otherwise, just the selected group.
	if (m_bFiringAll) return m_oWeapons.GetSize() > 0;
	else return (m_strActiveWeapGroup != "");
}

const WeaponState& ControlState::GetWeaponState(unsigned iID) const
{
	return m_oWeapons.GetWeaponState(iID);
}

WeaponState& ControlState::GetWeaponState(unsigned iID)
{
	return m_oWeapons.GetWeaponState(iID);
}

const vector<unsigned>& ControlState::GetActiveWeapons() const
{
	// When firing all is on, all weapons are "active".
	if (m_bFiringAll) return m_vAllWeaps;
	else return m_vActiveWeaps;
}

void ControlState::ToggleWeapons(bool bNext)
{
	// This passes them in as references and puts the values into m_strActiveWeapGroup & m_vActiveWeaps
	m_oWeapons.GetNextGroup(bNext, m_strActiveWeapGroup, m_vActiveWeaps);
}

// NOTE: Old code for setting target speed (if we decide this is needed, it's probably best put in ShipController)
//void ControlState::SetTargetSpeed(const Real& rVal)
//{
//	// Get the max speed from the config
//	const ShipType* pShip = GetShipType();
//	// Validate input
//	if (rVal < 0 || rVal > pShip->GetMaxSpeed())
//	{
//		JFUtil::Exception e ("ControlState", "SetTargetSpeed",
//			"Out-of-bounds value provided", rVal);
//		e.Append("for ship type:");
//		e.Append(m_strShipType);
//		throw e;
//	}
//	// Set the value
//	m_fTargetSpeed = rVal;
//}
//
//void ControlState::SetTargetSpeedOfMax(const Real& rProportion)
//{
//	// Validate the input
//	if (rProportion < 0 || rProportion > 1)
//		throw JFUtil::Exception("ControlState", "SetTargetSpeedOfMax",
//			"Out-of-bounds value provided", rProportion);
//
//	// Get the max speed from the config
//	const ShipType* pShip = GetShipType();
//	const Real& rMaxSpeed = pShip->GetMaxSpeed();
//
//	// Multiply rProportion by the max.
//	m_fTargetSpeed = rProportion * rMaxSpeed;
//}
//
//Real ControlState::GetTargetSpeedOfMax()
//{
//	// Get the max speed from the config
//	const ShipType* pShip = GetShipType();
//	const Real& rMaxSpeed = pShip->GetMaxSpeed();
//
//	if ( Ogre::Math::Abs(m_fTargetSpeed) > rMaxSpeed )
//	{
//		JFUtil::Exception e("ControlState", "GetTargetSpeedOfMax",
//			"m_fTargetSpeed was invalid", m_fTargetSpeed);
//		e.Append("Maximum:");
//		e.Append(rMaxSpeed);
//	}
//	return m_fTargetSpeed / rMaxSpeed;
//}

void ControlState::UnsetTargetSector()
{
	if (m_pTargetSector)
	{
		delete m_pTargetSector;
		m_pTargetSector = NULL;
	}
}

void ControlState::SetTargetSector(const String& rTarget)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert( pScenarioConf->GetSectorType(rTarget) );
	if (!m_pTargetSector)
		m_pTargetSector = new String(rTarget);
	else
		*m_pTargetSector = rTarget;
}

void ControlState::Update(unsigned long iTime)
{
//	const GeneralConfig* pConf = (const GeneralConfig*)
//		GameConfig::GetInstance().GetConfig("General");

	m_oWeapons.Update(iTime);

	// NOTE: old code for updating the target speed stuff.
//	Real fTargetSpeedOfMax;
//	if (m_bIncThrust && !m_bRedThrust)
//	{
//		fTargetSpeedOfMax = GetTargetSpeedOfMax();
//		fTargetSpeedOfMax += pConf->GetChaseCamAccelSensitivity() * (Real)iTime;
//	}
//	else if (m_bRedThrust && !m_bIncThrust)
//	{
//		fTargetSpeedOfMax = GetTargetSpeedOfMax();
//		fTargetSpeedOfMax -= pConf->GetChaseCamDecelSensitivity() * (Real)iTime;
//	}
//	if (fTargetSpeedOfMax > 1) fTargetSpeedOfMax = 1;
//	else if (fTargetSpeedOfMax < 0) fTargetSpeedOfMax = 0;
//	SetTargetSpeedOfMax(fTargetSpeedOfMax);

}


void ControlState::Reset()
{
	// Reset selected weapon by clearing them and then calling GetNextGroup
	m_strActiveWeapGroup = "";
	m_vActiveWeaps.clear();
	ToggleWeapons(true);

	m_iTargetID = -1; // No target
	m_pTargetSector = NULL;

	ResetMovement();
}

void ControlState::ResetMovement()
{
	m_bFiringGroup = false;
	m_bFiringAll = false;
	m_bIncThrust = false;
	m_bRedThrust = false;
	m_bZeroThrust = true;
	m_bStrafeL = false;
	m_bStrafeR = false;
	m_bStrafeU = false;
	m_bStrafeD = false;
	m_bBrake = false;
//	m_bWarp = false;

	m_fYaw = 0;
	m_fPitch = 0;
	m_fRoll = 0;

	//m_fTargetSpeed = 0;

}
