#include <State/BfPStateFaction.h>

#include <Config/BfPConfig.h>
#include <Config/BfPConfigScenario.h>

/**
 * FactionState code
 */

FactionState::FactionState(const FactionType* pConf)
: m_strName(pConf->GetName())
, m_iResourcePoints(pConf->GetStartingRPs())
, m_iScore(0)
, m_iFlagShipID(-1)
{
}

FactionState::~FactionState()
{

}

const FactionType* FactionState::GetFactionType() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const FactionType* pType = pScenarioConf->GetFactionType(m_strName);
	assert(pType);
	return pType;
}

void FactionState::AlterRPs(long iAmount)
{
	//if (m_bDead) return;
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	m_iResourcePoints += iAmount;
	if (iAmount > 0) m_iScore += iAmount;
	// Can't let RPs go over a maximum (partly game balance, partly space limitations in GUI)
	if (m_iResourcePoints > (long)pScenarioConf->GetRPMaxAmount())
		m_iResourcePoints = (long)pScenarioConf->GetRPMaxAmount();
}

bool FactionState::HasShipToDeploy(const String& rType) const
{
	const ShipsConfig* pShipsConf =
		(const ShipsConfig*)BfPConfig::GetInstance().GetConfig("Ships");
	assert(pShipsConf->GetShipType(rType));
	return find(m_vShipsToDeploy.begin(), m_vShipsToDeploy.end(), rType) != m_vShipsToDeploy.end();
}

bool FactionState::HasGroupToDeploy(const StringVector& rTypes) const
{
	StringVector vShips (m_vShipsToDeploy);
	for (StringVector::const_iterator it = rTypes.begin();
		 it != rTypes.end(); ++it)
	{
		// Check our list, removing ships as we go.
		StringVector::iterator itAvailShip =
			find(vShips.begin(), vShips.end(), *it);
		if (itAvailShip == vShips.end()) return false;
		else vShips.erase(itAvailShip);
	}
	return true;
}

bool FactionState::RemoveDeployingShip(const String& rType)
{
	const ShipsConfig* pShipsConf =
		(const ShipsConfig*)BfPConfig::GetInstance().GetConfig("Ships");
	//cout << "RemoveDeployingShip " << rType << endl;
	assert(pShipsConf->GetShipType(rType));

	StringVector::iterator it =
		find(m_vShipsToDeploy.begin(), m_vShipsToDeploy.end(), rType);
	if (it == m_vShipsToDeploy.end()) return false;

	m_vShipsToDeploy.erase(it);
	return true;
}

bool FactionState::RemoveDeployingGroup(const StringVector& rTypes)
{
	if (!HasGroupToDeploy(rTypes)) return false;
	//cout << "rTypes size: " << rTypes.size() << endl;
	size_t iSize = rTypes.size();
	for (size_t i = 0; i < iSize; ++i)
	{
		assert(RemoveDeployingShip( rTypes[i] )); // We checked HasGroup so it must work
	}
//	for (StringVector::const_iterator it = rTypes.begin();
//		 it != rTypes.end(); ++it)
//	{
//		assert(RemoveDeployingShip(*it)); // We checked HasGroup so it must work
//	}
	//cout << "done deploying group" << endl;
	return true;
}

unsigned FactionState::GetFlagShipID() const
{
	assert(m_iFlagShipID >= 0);
	return (unsigned)m_iFlagShipID;
}

bool FactionState::CanPurchaseGroup(const StringVector& rTypes) const
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	const ShipsConfig* pShipsConf =
		(const ShipsConfig*) BfPConfig::GetInstance().GetConfig("Ships");

	// First check the size is below or equal to max
	if (rTypes.size() > pGameConf->GetMaxGroupSize()) return false;

	unsigned iCost = 0;

	for (StringVector::const_iterator it = rTypes.begin(); it != rTypes.end(); ++it)
	{
		const ShipType* pType = pShipsConf->GetShipType(*it);
		assert(pType);

		iCost += pType->GetRPCost();

	}

	return (m_iResourcePoints >= (long)iCost);

}

bool FactionState::PurchaseGroup(const StringVector& rTypes)
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	const ShipsConfig* pShipsConf =
		(const ShipsConfig*) BfPConfig::GetInstance().GetConfig("Ships");

	// This should've been checked in earlier validation
	assert(!rTypes.empty() && rTypes.size() <= pGameConf->GetMaxGroupSize());

	unsigned iCost = 0;

	for (StringVector::const_iterator it = rTypes.begin(); it != rTypes.end(); ++it)
	{
		const ShipType* pType = pShipsConf->GetShipType(*it);
		assert(pType);

		iCost += pType->GetRPCost();

	}

	if (m_iResourcePoints < (long)iCost)
		return false;

	m_iResourcePoints -= iCost;

	for (StringVector::const_iterator it = rTypes.begin(); it != rTypes.end(); ++it)
	{
		m_vShipsToDeploy.push_back(*it);
	}

	return true;
}
