#include <State/BfPStateProjectile.h>

/**
 * ProjectileState code
 */

ProjectileState::ProjectileState(
	unsigned iOwnerID, String strOwnerFaction, int iOwnerTarget,
	String strType, Vector3 vecPos, Quaternion qOrientation, String strSectorID)
: BfPEntityState("PROJECTILE", strType, strOwnerFaction, vecPos, qOrientation, strSectorID)
, m_iOwnerID(iOwnerID)
, m_iOwnerTarget(iOwnerTarget)
{
	const WeaponType* pWeapType = GetWeaponType();
	if (!pWeapType->GetInvincible()) m_iHP = pWeapType->GetMaxHP();
	m_iLifetime = pWeapType->GetLifetime();
}

const EntityType* ProjectileState::GetEntityType() const
{
	const WeaponsConfig* pWeapsConf = (WeaponsConfig*)GameConfig::GetInstance().GetConfig("Weapons");
	const EntityType* pType = (EntityType*)pWeapsConf->GetWeaponType(GetSubType());
	assert(pType);
	return pType;
}

const WeaponType* ProjectileState::GetWeaponType() const
{
	const WeaponsConfig* pWeapsConf = (WeaponsConfig*)GameConfig::GetInstance().GetConfig("Weapons");
	const WeaponType* pType = pWeapsConf->GetWeaponType(GetSubType());
	assert(pType);
	return pType;
}

void ProjectileState::ApplyDamage(int iAmount)
{
	if (m_iHP <= 0) return;
	EntityState::ApplyDamage(iAmount);
	if (m_iHP <= 0) SetDestroyed();
}

void ProjectileState::Update(unsigned long iTime)
{
	EntityState::Update(iTime);

	m_iLifetime -= iTime;
	// Destroy the projectile if its lifetime has run out.
	if (m_iLifetime < 0) m_iLifetime = 0;
	if (m_iLifetime == 0) SetDestroyed();
}

