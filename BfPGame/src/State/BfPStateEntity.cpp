#include <State/BfPStateEntity.h>

#include <Config/BfPConfig.h>

/**
 * BfPEntityState code
 */
BfPEntityState::BfPEntityState(String strType, string strSubType, String strFactionID,
	Vector3 vecPos, Quaternion qOrientation, String strSectorID)
: EntityState(strType, strSubType, strFactionID, vecPos, qOrientation)
, m_strSectorID(strSectorID)
{
	// TODO: Call superclass constructor, and set sector ID
}

BfPEntityState::BfPEntityState(const BfPEntityState& rState)
: EntityState(rState)
, m_strSectorID(rState.m_strSectorID)
{
}


void BfPEntityState::SetSectorID(const String& rNewSector)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	if ( !pScenarioConf->GetSectorType(rNewSector) )
		throw JFUtil::Exception("BfPEntityState", "SetSectorID",
			"Invalid sector given", rNewSector);
	m_strSectorID = rNewSector;
}
