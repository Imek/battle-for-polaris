#include <State/BfPState.h>

#include <Config/BfPConfig.h>

/**
 * TriggerState code
 */


/**
 * BfPState code
 */

BfPState::BfPState()
//: m_pActiveSector(NULL)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// All controls are enabled by default
	m_oInputState.insert(ALL);

	// We need to initialise a trigger list for each mission in the config.
	if (pScenarioConf->HasMissionsConfig())
	{
		const MissionsConfig& rMissions = pScenarioConf->GetMissionsConfig();
		const StringVector& rMissNames = rMissions.GetMissionNames();
		for (StringVector::const_iterator it = rMissNames.begin();
			 it != rMissNames.end(); ++it)
		{
			m_mMissions[*it] = TriggerList();
		}
	}

	InitFromConfig();
}

BfPState::BfPState(const BfPState& rState)
: m_iAge(rState.m_iAge)
, m_iTimeSincePrevRPIncome(rState.m_iTimeSincePrevRPIncome)
, m_mMissions(rState.m_mMissions)
, m_oInputState(rState.m_oInputState)
{
	for (SectorList::const_iterator it = rState.m_mSectors.begin();
		 it != rState.m_mSectors.end(); ++it)
	{
		m_mSectors[it->first] = new BfPSectorState(*it->second);
	}

	for (FactionList::const_iterator it = rState.m_mFactions.begin();
		 it != rState.m_mFactions.end(); ++it)
	{
		m_mFactions[it->first] = new FactionState(*it->second);
	}
}

BfPState::~BfPState()
{
	for (SectorList::iterator it = m_mSectors.begin(); it != m_mSectors.end(); ++it)
	{
		delete it->second;
	}
	m_mSectors.clear();
	for (FactionList::iterator it = m_mFactions.begin(); it != m_mFactions.end(); ++it)
	{
		delete it->second;
	}
	m_mFactions.clear();
}

vector<const ShipState*> BfPState::GetFactionShips(String strFaction) const
{
	vector<const ShipState*> vShips;
	FactionList::const_iterator itFaction = m_mFactions.find(strFaction);
	assert(itFaction != m_mFactions.end());
	const FactionState* pFaction = itFaction->second;
	assert(pFaction);
	for (SectorList::const_iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		const BfPSectorState* pSector = it->second;
		pSector->GetFactionShips(strFaction, vShips);
	}
	return vShips;
}

vector<ShipState*> BfPState::GetFactionShips(String strFaction)
{
	vector<ShipState*> vShips;
	FactionList::const_iterator itFaction = m_mFactions.find(strFaction);
	assert(itFaction != m_mFactions.end());
	const FactionState* pFaction = itFaction->second;
	assert(pFaction);
	for (SectorList::iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		BfPSectorState* pSector = it->second;
		pSector->GetFactionShips(strFaction, vShips);
	}
	return vShips;
}

FactionState* BfPState::GetPlayerFaction()
{
	FactionList::iterator it = m_mFactions.find("Player");
	assert(it != m_mFactions.end());
	return it->second;
}

ShipState* BfPState::GetPlayerShip()
{
	FactionState* pPlayerFaction = GetPlayerFaction();
	assert(pPlayerFaction);
	BfPSectorState* pSector;
	ShipState* pShip = FindShipWithID(pPlayerFaction->GetFlagShipID(), &pSector);
	// TODO: Check here that it's in the active sector
	assert(pShip);
	return pShip;
}

bool BfPState::ObjectExists(String strType, unsigned iID) const
{
	for (SectorList::const_iterator it = m_mSectors.begin();
		 it != m_mSectors.end(); ++it)
	{
		const BfPSectorState* pSector = it->second;
		if (pSector->ObjectExists(strType, iID)) return true;
	}

	return false;
}

const ShipState* BfPState::WarpShip(unsigned iID, const String& rDestSector)
{
	// Find the origin and destination sectors as well as the ship state.
	SectorList::iterator itDestSector = m_mSectors.find(rDestSector);
	assert(itDestSector != m_mSectors.end());
	BfPSectorState* pDestSector = itDestSector->second;
	BfPSectorState* pFromSector = NULL;
	ShipState* pFromShip = FindShipWithID(iID, &pFromSector);

	// Get the types and make sure the sectors are actually linked.
	assert(pFromShip && pFromSector);
	const SectorType* pFromType = pFromSector->GetSectorType();
	const SectorType* pDestType = pDestSector->GetSectorType();
	assert(pFromType->IsLinkedTo(rDestSector));

	// Return false if the ship is not close enough to the warp point
	//Real fMaxWarpDist = pScenarioConf->GetMaxWarpDist();
//	const Vector3& rWarpPoint =
//		pFromType->GetWarpPointFromSector(pDestType->GetName());
	// We no longer check for distance (it's enough that we were close enough when we started warping)
//	if (pFromShip->GetPosition().squaredDistance(rWarpPoint) > fMaxWarpDist*fMaxWarpDist)
//		return NULL;

	//cout << "warp ship" << endl;
	// Otherwise, we can try to warp the ship. We do this by making a copy and marking the old one for removal.
	ShipState* pToShip = new ShipState(*pFromShip);
	//cout << "done copying" << endl;
	pToShip->GetControls().Reset(); // Resets everything including target sector.
	pToShip->SetNew(true); // Mark as new so that it will be added properly.
	pToShip->SetSectorID(rDestSector);
	pToShip->SetVel(Vector3::ZERO);

	Vector3 vecDestPos = pDestType->GetWarpPointFromSector(pFromType->GetName());
	if (!pDestSector->AddWarpedShip(pToShip, vecDestPos))
	{
		delete pToShip;
		return NULL;
	}

	//cout << "done adding" << endl;

	// TODO: If it's the group leader, log an event for the AI system to see.
//	if (pToShip->IsGroupLeader())
//	{
//		Event* pWarpedEvent = new AIUnitWarpedEvent(
//			pToShip->GetFaction(), pToShip->GetGroupID(),
//			pFromType->GetName(), pDestType->GetName());
//		m_oAIEvents.PushEvent(pWarpedEvent);
//
//	}

	//cout << "done event" << endl;

	// Warping was successful, so remove our state from the origin sector.
	pFromShip->SetRemoved(true);
	//pFromSector->RemoveWarpedShip(pFromShip);

	//cout << "done removing" << endl;
	pToShip->ResetWarping();
	pToShip->ZeroEP();
	return pToShip;

}

BfPSectorState* BfPState::GetSectorState(String strName)
{
	SectorList::iterator it = m_mSectors.find(strName);
	if (it == m_mSectors.end()) return NULL;
	else return it->second;
}

FactionState* BfPState::GetFactionState(String strName)
{
	FactionList::iterator it = m_mFactions.find(strName);
	if (it == m_mFactions.end()) return NULL;
	else return it->second;
}

void BfPState::Update(unsigned long iTime)
{
	m_iAge += iTime;

	assert(iTime >= 0);
	for (SectorList::iterator it = m_mSectors.begin(); it != m_mSectors.end(); ++it)
	{
		BfPSectorState* pSector = it->second;
		assert(pSector);
		pSector->Update(iTime);
	}

	UpdateResourceIncome(iTime);
}

ShipState* BfPState::FindShipWithID(unsigned iID, BfPSectorState** pSector)
{
	*pSector = NULL;
	ShipState* pToReturn = NULL;
	for (SectorList::iterator it = m_mSectors.begin(); it != m_mSectors.end(); ++it)
	{
		BfPSectorState* pThisSector = it->second;
		ShipState* pShip = (ShipState*)pThisSector->GetObjState("SHIP", iID);
		if (pShip)
		{
			//assert(!pToReturn);
			*pSector = pThisSector;
			pToReturn = pShip;
		}
	}
	return pToReturn;
}

void BfPState::AddPlayerInputFlag(CONTROLS_MODE iFlag)
{
	// Nothing to do if the flag is already enabled.
	InputState::const_iterator itFlagFound = m_oInputState.find(iFlag);
	if ( CheckPlayerInputFlag(iFlag) ) return; // Nothing to do.
	// If it's ALL or NONE, we need to always make sure that flag will be the only one there.
	if (iFlag == ALL || iFlag == NONE)
	{
		m_oInputState.clear();
	}
	else
	{
		// If we're setting some other flag, ALL and NONE must not be present.
		InputState::iterator itRemove;
		itRemove = m_oInputState.find(ALL);
		if (itRemove != m_oInputState.end()) m_oInputState.erase(itRemove);
		itRemove = m_oInputState.find(NONE);
		if (itRemove != m_oInputState.end()) m_oInputState.erase(itRemove);
	}
	// Add the flag to the input state
	m_oInputState.insert(iFlag);
}

void BfPState::RemovePlayerInputFlag(CONTROLS_MODE iFlag)
{
	if (iFlag == ALL || iFlag == NONE)
	{
		throw JFUtil::Exception("BfPState", "RemovePlayerInputFlag",
			"Illegal flag provided (ALL or NONE)");
	}
	InputState::iterator itRemove;
	itRemove = m_oInputState.find(iFlag);
	if (itRemove != m_oInputState.end()) m_oInputState.erase(itRemove);
	// Otherwise it may be enabled if m_oInputState just contains "all".
	else if (m_oInputState.find(ALL) != m_oInputState.end())
	{
		m_oInputState.clear();
		// Insert everything but iFlag.
		for (CONTROLS_MODE i = (CONTROLS_MODE)0; i < CMODE_LAST;
			 i = (CONTROLS_MODE)((int)i+1))
		{
			if (i != iFlag && i != ALL && i != NONE) m_oInputState.insert(i);
		}
	}
}

bool BfPState::CheckPlayerInputFlag(CONTROLS_MODE iFlag) const
{
	InputState::const_iterator itFlagFound = m_oInputState.find(iFlag);
	bool bFlagFound = itFlagFound != m_oInputState.end();
	bool bEmpty = m_oInputState.empty();
	// If we're checking for NONE, it should either be empty or just contain the NONE flag.
	if (iFlag == NONE)
	{
		if (bFlagFound)
		{
			// If NONE is in the state, it must be the only one!
			assert(m_oInputState.size() == 1);
			return true;
		}
		else return bEmpty;
	}
	// Otherwise, if ALL is in the state it's always true.
	else if (m_oInputState.find(ALL) != m_oInputState.end())
		return true;
	// Otherwise just see if the flag is in there.
	else
		return bFlagFound;

}

/**
 * BfPState private code
 */

void BfPState::InitFromConfig()
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// TODO: Lots of validation is being done through assertions around here when it should be in the config constructors (or at least with proper exceptions)
	m_iAge = 0;
	m_iTimeSincePrevRPIncome = (long)pScenarioConf->GetRPIncomeDelay();

	// First add sector states
	const StringVector& rvSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rvSectors.begin();
		 it != rvSectors.end(); ++it)
	{
		const SectorType* pSectorConf = pScenarioConf->GetSectorType(*it);
		assert(pSectorConf);
		BfPSectorState* pSector = new BfPSectorState(pSectorConf);

		String strSector = pSectorConf->GetName();
		assert(m_mSectors.find(strSector) == m_mSectors.end());
		m_mSectors[strSector] = pSector;
	}

	// Add faction states now
	const StringVector& rvFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rvFactions.begin();
		 it != rvFactions.end(); ++it)
	{
		const FactionType* pFactionConf = pScenarioConf->GetFactionType(*it);
		assert(pFactionConf);
		FactionState* pFaction = new FactionState(pFactionConf);

		String strFaction = pFactionConf->GetName();
		assert(m_mFactions.find(strFaction) == m_mFactions.end());
		m_mFactions[strFaction] = pFaction;

		// Set ownership of sectors
		const StringVector& rMySectors = pFactionConf->GetStartingSectors();
		for (StringVector::const_iterator itSector = rMySectors.begin();
			 itSector != rMySectors.end(); ++itSector)
		{
			SectorList::iterator itOwned = m_mSectors.find(*itSector);
			assert(itOwned != m_mSectors.end());
			BfPSectorState* pOwned = itOwned->second;
			// Can't have multiple factions starting with the same sector!
			assert(!pOwned->GetOwner());
			pOwned->SetOwner(*it);
		}
	}

}

void BfPState::UpdateResourceIncome(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Count down the timer and inject some RPs to each faction if ticks have passed.
	m_iTimeSincePrevRPIncome -= (long)iTime;
	//cout << "m_iTimeSincePrevRPIncome: " << m_iTimeSincePrevRPIncome << endl;
	if (m_iTimeSincePrevRPIncome > 0) return;

	unsigned long iIncome = (unsigned long)(0 - m_iTimeSincePrevRPIncome);
	iIncome = iIncome / pScenarioConf->GetRPIncomeDelay();
	++iIncome;

	//cout << "RP Income per sector: " << iPassedTicks << endl;
	// iIncome is number of ticks passed
	m_iTimeSincePrevRPIncome += iIncome * pScenarioConf->GetRPIncomeDelay();

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const BfPSectorState* pSector = m_mSectors[*it];
		const SectorType* pType = pSector->GetSectorType();
		const String* pOwner = pSector->GetOwner();
		if (!pOwner) continue;
		if (*pOwner == "NEUTRAL") continue;
		// Must be a valid faction if not visible or neutral
		FactionList::iterator itFaction = m_mFactions.find(*pOwner);
		assert(itFaction != m_mFactions.end());
		FactionState* pFaction = itFaction->second;
		pFaction->AlterRPs(iIncome * pType->GetRPIncome());
	}

}
