#include <State/BfPStateShip.h>

/**
 * ShipState code
 */

ShipState::ShipState(unsigned iGroupID, bool bFlagGroup, bool bGroupLeader,
	String strType, String strFaction, Vector3 vecPos, Quaternion qOrientation, String strSectorID)
: BfPEntityState("SHIP", strType, strFaction, vecPos, qOrientation, strSectorID)
, m_iGroupID(iGroupID)
, m_bFlagGroup(bFlagGroup)
, m_bGroupLeader(bGroupLeader)
, m_strKillerFaction(strFaction)
, m_fSPRegen(0)
, m_fEPRegen(0)
, m_fThrust(0)
, m_iWarpTimer(0)
, m_bTriggerWarp(false)
, m_bTriggeredWarp(false)
, m_bWarping(false)
, m_bWarped(false)
, m_oControls(strType)
, m_vecFiringDir(Vector3::NEGATIVE_UNIT_X)
{
	const ShipType* pType = GetShipType();
	m_iHP = pType->GetMaxHP();
	m_iSP = pType->GetMaxSP();
	m_iEP = pType->GetMaxEP();
}


bool ShipState::GetRemoved() const
{
	if (EntityState::GetRemoved()) return true; // Exploded
	if (m_bWarped) return true; // Manually removed by a controller (e.g. warped)
	return false;
}


void ShipState::SetRemoved(bool bRemoved)
{
	// SetRemoved for a ShipState "warps" it rather than destroying it
	const ShipType* pType = GetShipType();
	if (bRemoved == GetRemoved()) return;
	if (bRemoved)
	{
		m_bWarped = true; // Remove in this case is set because we warped
		m_iWarpTimer = 0;
	}
	else // !bRemoved
	{
		if (m_iHP <= 0)
		{
			m_iHP = pType->GetMaxHP();
			m_iExplodeTimer = 0;
			m_bTriggerExplode = false;
		}
		if (m_bWarping)
		{
			m_bWarping = false;
			m_iWarpTimer = 0;
		}
	}
	assert(bRemoved == GetRemoved());
}

const EntityType* ShipState::GetEntityType() const
{
	const ShipsConfig* pShipsConf = (ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const EntityType* pType = (EntityType*)pShipsConf->GetShipType(GetSubType());
	assert(pType);
	return pType;
}

const ShipType* ShipState::GetShipType() const
{
	const ShipsConfig* pShipsConf = (ShipsConfig*)
		GameConfig::GetInstance().GetConfig("Ships");
	const ShipType* pType = pShipsConf->GetShipType(GetSubType());
	assert(pType);
	return pType;
}

void ShipState::ApplyDamage(int iAmount)
{
	assert(iAmount >= 0);
	assert(m_iHP >= 0);
	if (IsDead() || m_bWarping)
	{
		m_fThrust = 0;
		return;
	}
	const ShipType* pType = GetShipType();
	if (pType->GetInvincible()) return;

	// Take off the shield first and then the hull
	m_iSP -= iAmount;
	if (m_iSP < 0)
	{
		m_iHP += m_iSP; // Subtract remainder of damage
		m_iSP = 0;
	}

	// The ship was destroyed, so trigger the explosion effect.
	if (m_iHP <= 0)
	{
		// This sets off the explosion effect too
		SetDestroyed();

		// Stop warping if we're dead!
		m_bTriggerWarp = false;
		m_bTriggeredWarp = false;
		m_bWarping = false;
		m_iWarpTimer = 0;
	}
}

void ShipState::SetKillerFaction(const String& rFaction)
{
	assert(IsDead());
	m_strKillerFaction = rFaction;
}

const String& ShipState::GetKillerFaction() const
{
	assert(IsDead());
	return m_strKillerFaction;
}

bool ShipState::FullEP() const
{
	return (m_iEP == (int)GetShipType()->GetMaxEP());
}

Real ShipState::GetOverallCondition(bool bInclEP) const
{
	// Overall condition is (hp+sp+ep)/(maxhp+maxsp+maxep)
	const ShipType* pType = GetShipType();

	Real fMyTotal = m_iHP+m_iSP;
	if (bInclEP) fMyTotal += m_iEP;

	Real fMaxTotal = pType->GetMaxHP() + pType->GetMaxSP();
	if (bInclEP) fMaxTotal += pType->GetMaxEP();

	assert(fMyTotal >= 0 && fMaxTotal > 0);
	assert(fMaxTotal >= fMyTotal);
	return fMyTotal/fMaxTotal;
}

Real ShipState::GetEPOfMax() const
{
	// ep/maxep
	const ShipType* pType = GetShipType();
	assert(m_iEP >= 0 && m_iEP <= (int)pType->GetMaxEP());
	return (Real)m_iEP/(Real)pType->GetMaxEP();
}

bool ShipState::ReadyToFire() const
{
	// GetCostToFire returns -1 if we can't fire, but we also need to check for EPs.
	int iCost = GetCostToFire();
	return (iCost >= 0 && iCost <= m_iEP);
}

bool ShipState::Fire()
{
	int iEPCost = GetCostToFire();

	if (iEPCost < 0 || m_iEP < iEPCost)
		return false;


	// Take off the EP cost and fire all the weapons
	m_iEP -= iEPCost;
	// at least one firing flag must be on
	const vector<unsigned>& rGroup = m_oControls.GetActiveWeapons();
	for (vector<unsigned>::const_iterator it = rGroup.begin();
		 it != rGroup.end(); ++it)
	{
		WeaponState& rWeap = m_oControls.GetWeaponState(*it);
		assert(rWeap.Fire());
	}

	return true;
}


Vector3 ShipState::GetFiringDir(unsigned iWeapID) const
{
	const WeaponState& rWeap = GetWeaponState(iWeapID);
	if (!HasActiveWeapon() || !rWeap.GetType()->GetAutoTracking())
		return GetFaceDir();
	else
		return m_oControls.m_vAutoAim[iWeapID];
}

void ShipState::BeginWarping()
{
	// Begin the effect that leads to the ship being warped.
	assert(m_iHP > 0);
	assert(FullEP());

	if (m_bWarping) return;
	assert(m_iWarpTimer == 0 && !m_bTriggerWarp);

	const ShipType* pType = GetShipType();

	m_iWarpTimer = pType->GetWarpDelay();
	m_bTriggerWarp = true;
	m_bWarping = true;
}

void ShipState::ResetWarping()
{
	m_bWarping = false;
	m_bTriggerWarp = false;
	m_bTriggeredWarp = false;
	m_iWarpTimer = 0;
}

void ShipState::Update(unsigned long iTime)
{
	assert(iTime >= 0);

	// First, check the self-destruct flag and set our HP to 0 if it's set
	if (m_oControls.m_bSelfDestruct)
		ApplyDamage(m_iSP + m_iHP);

	// Call EntityState update to handle destruction
	EntityState::Update(iTime);
	if (IsDead()) return;

	const ShipType* pShipType = GetShipType();

	// Update (cooldown etc) for each weapon through the control state.
	m_oControls.Update(iTime);

	if (m_bWarping)
	{
		// TODO: The whole particle trigger system (for both ships & projectiles) could be a bit cleaner. Maybe use an EventHandler?
		// If the trigger is still on and we've already done one update, turn the trigger off.
		if (m_bTriggerWarp && m_bTriggeredWarp)
		{
			m_bTriggerWarp = false;
			m_bTriggeredWarp = false;
		}
		else if (m_bTriggerWarp) // And not checked yet
			m_bTriggeredWarp = true;

		m_iWarpTimer -= iTime;
		if (m_iWarpTimer < 0) m_iWarpTimer = 0;
	}
	else
	{
		assert(m_iWarpTimer == 0);
		assert(!m_bTriggerWarp);
	}


	// Regenerate shields and energy
	// Need to keep track of regenerated amounts below zero between updates.
	Real fTime = (Real)iTime;
	m_fSPRegen += fTime * pShipType->GetSPRate();
	m_fEPRegen += fTime * pShipType->GetEPRate();

	int iSPGained = (int)m_fSPRegen;
	int iEPGained = (int)m_fEPRegen;
	assert(iSPGained >= 0 && iEPGained >= 0);

	m_fSPRegen -= iSPGained;
	m_fEPRegen -= iEPGained;

	m_iSP += iSPGained;
	if (m_iSP > (int)pShipType->GetMaxSP()) m_iSP = (int)pShipType->GetMaxSP();
	m_iEP += iEPGained;
	if (m_iEP > (int)pShipType->GetMaxEP()) m_iEP = (int)pShipType->GetMaxEP();


	// Ship controls (thrust)
//	// Brake if we're pressing the reduce thrust button and have no thrust
//	if (m_oControls.m_bRedThrust &&
//		m_fThrust <= 0.f && !m_oControls.m_bIncThrust && !m_oControls.m_bStrafeD &&
//		!m_oControls.m_bStrafeL && !m_oControls.m_bStrafeR && !m_oControls.m_bStrafeU)
//		m_oControls.m_bBrake = true;

	if (!m_bWarping && m_oControls.m_bBrake) // Braking (stopping) is managed by the physics engine; just zero m_fThrust here
	{
		m_fThrust = 0;
	}
	else if (m_oControls.m_bZeroThrust)
	{
		m_fThrust = 0;
		m_oControls.m_bZeroThrust = false;
	}
	else
	{
		const Real& rMaxThrust = pShipType->GetMaxThrust();

		assert(rMaxThrust >= 0); // Should've been validated in the config
		if (m_oControls.m_bIncThrust && !m_oControls.m_bRedThrust)
		{
			m_fThrust += pShipType->GetThrustRate() * fTime;
			if (m_fThrust > rMaxThrust) m_fThrust = rMaxThrust;
		}
		if (m_oControls.m_bRedThrust && !m_oControls.m_bIncThrust)
		{
			m_fThrust -= pShipType->GetThrustRate() * fTime;
			if (m_fThrust < 0) m_fThrust = 0;
		}
	}

	// Part-finished code for moving towards target speed rather than setting thrust
//	else
//	{
//		// Increase thrust until we reach the target speed, then set thrust to zero.
//		// NOTE: If we ever decide to have damping in space, we need to calculate the thrust
//		// required to counter damping rather than set thrust to 0 once we're the right speed.
//		Vector3 vecFacing = GetFaceDir();
//		const Vector3& rVel = GetVel();
//		Real fVelInFacing = vecFacing.dotProduct(rVel);
//		// Moving too slow: set thrust to maximum
//		if (fVelInFacing < m_oControls.GetTargetSpeed())
//			m_fThrust = pShipType->GetMaxThrust();
//		// Otherwise set thrust to zero.
//		else// if (fVelInFacing > m_oControls.GetTargetSpeed())
//			m_fThrust = 0; // TODO: Brake if appropriate here
//
//	}




	assert(m_iEP >= 0);

}



int ShipState::GetCostToFire() const
{
	// Return -1 if for any reason we can't fire (firing key not pressed, no weapon selected, cooldown not 0)
	// Caller still needs to check for EPs though!
	if (!HasActiveWeapon()) return -1;
	if (!m_oControls.m_bFiringGroup && !m_oControls.m_bFiringAll) return -1;

	int iEPCost = 0;
	// This is a list of them all if m_bFiringAll is true
	const vector<unsigned>& rGroup = m_oControls.GetActiveWeapons();
	for (vector<unsigned>::const_iterator it = rGroup.begin();
		 it != rGroup.end(); ++it)
	{
		const WeaponState& rWeap = m_oControls.GetWeaponState(*it);
		if (!rWeap.ReadyToFire()) return -1;
		const WeaponType* pWeapType = rWeap.GetType();
		iEPCost += (int)pWeapType->GetEnergyCost();
	}

	assert(iEPCost >= 0); // Don't see how this could fail..
	return iEPCost;

}
