#include <State/BfPStateSector.h>

#include <Config/BfPConfig.h>
#include <Config/BfPConfigScenario.h>


/**
 * Collidable code
 */
Collidable::Collidable(const EntityState* pEntity)
: m_bIsEntity(true)
, m_pEntity(pEntity)
{
	assert(pEntity);
}

Collidable::Collidable(const String& rSector, const String& rBody)
: m_bIsEntity(false)
, m_strSector(rSector)
, m_strBody(rBody)
{
	assert( GetBody() );
}

const EntityState* Collidable::GetEntity() const
{
	assert(m_bIsEntity);
	assert(m_pEntity);
	return m_pEntity;
}

const BodyInstance* Collidable::GetBody() const
{
	assert(!m_bIsEntity);
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const SectorType* pSector = pScenarioConf->GetSectorType(m_strSector);
	assert(pSector);
	const BodyInstance* pBody = pSector->GetBodyWithID(m_strBody);
	assert(pBody);
	return pBody;
}

/**
 * BfPSectorState code
 */
BfPSectorState::BfPSectorState(const SectorType* pSectorConf)
: SectorState(pSectorConf->GetName(), pSectorConf->GetObjTypes())
, m_pOwner(NULL)
, m_pLastOwner(NULL)
, m_bOwnerChanged(false)
, m_iTimeHeldByOccupier(0)
, m_strLastOccupier("")
{
}

BfPSectorState::BfPSectorState(const BfPSectorState& rState)
: SectorState(rState)
, m_pOwner(new String(*rState.m_pOwner))
, m_pLastOwner(new String(*rState.m_pLastOwner))
, m_bOwnerChanged(rState.m_bOwnerChanged)
, m_iTimeHeldByOccupier(rState.m_iTimeHeldByOccupier)
, m_strLastOccupier(rState.m_strLastOccupier)
{
	assert(!m_strName.empty());
	// Copy ship list
	m_mObjects["SHIP"] = ObjList();
	ObjList& rMyShips = m_mObjects["SHIP"];
	ObjListMap::const_iterator itShipList = rState.m_mObjects.find("SHIP");
	assert(itShipList != rState.m_mObjects.end());
	const ObjList& rShips = itShipList->second;
	for (ObjList::const_iterator it = rShips.begin(); it != rShips.end(); ++it)
	{
		ShipState* pShip = (ShipState*)*it;
		//cout << "copy ship" << endl;
		rMyShips.push_back( new ShipState(*pShip) );
	}
	// Copy projectile list
	m_mObjects["PROJECTILE"] = ObjList();
	ObjList& rMyProjs = m_mObjects["PROJECTILE"];
	ObjListMap::const_iterator itProjList = rState.m_mObjects.find("PROJECTILE");
	assert(itProjList != rState.m_mObjects.end());
	const ObjList& rProjectiles = itProjList->second;
	for (ObjList::const_iterator it = rProjectiles.begin(); it != rProjectiles.end(); ++it)
	{
		ProjectileState* pProj = (ProjectileState*)*it;
		rMyProjs.push_back( new ProjectileState(*pProj) );
	}


}

BfPSectorState::~BfPSectorState()
{
	delete m_pOwner;
	delete m_pLastOwner;
}

const SectorType* BfPSectorState::GetSectorType() const
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pType = pScenarioConf->GetSectorType(m_strName);
	assert(pType);
	return pType;
}

void BfPSectorState::Update(unsigned long iTime)
{
	assert(iTime >= 0);

	// Update the various behaviours and values in all the game objects
	// TODO: Change the GetNextObjectID function to be more efficient (just remember the next ID now)
	UpdateProjectiles(iTime);
	UpdateShips(iTime);
	UpdateCapturing(iTime);

	//UpdatePickups(iTime);

}

void BfPSectorState::UpdateShips(unsigned long iTime)
{
	assert(iTime >= 0);
	ObjList& rShips = GetObjectList("SHIP");
	ObjList& rProjectiles = GetObjectList("PROJECTILE");

	//cout << "Number of projectiles: " << rProjectiles.size() << endl;

	// Go through each player state, updating each (cooldown, firing, thrust, turning...)
	for (ObjList::iterator it = rShips.begin();
		 it != rShips.end(); ++it)
	{
		ShipState* pShip = (ShipState*)*it;
		const ControlState& rControls = pShip->GetControls();
		// The state's update function decrements cooldown, regenerates shields/energy
		// and updates thrust and orientation from turning.
		pShip->Update(iTime);


		const ShipType* pShipType = pShip->GetShipType();
		assert(pShipType);

		// Fire() returns true and reduces ammo/ep/etc if the ship is ready and willing to fire.
		if (pShip->Fire() && !pShip->GetWarping())
		{
			// For each active weapon, make a new projectile.
			const vector<unsigned>& rWeapIDs = pShip->GetActiveWeapons();
			for (vector<unsigned>::const_iterator it = rWeapIDs.begin();
				 it != rWeapIDs.end(); ++it)
			{
				const unsigned& rWeapID = *it;
				const WeaponState& rWeapState = pShip->GetWeaponState(rWeapID);
				const WeaponType* pWeapType = rWeapState.GetType();
				assert(pWeapType);

				// Work out the absolute position from the offset and the ship position/orientation.
				Vector3 vecPos = pShipType->GetWeapOffset( rWeapID );
				vecPos = pShip->GetOrientation() * vecPos;
				vecPos += pShip->GetPosition();

				ProjectileState* pProj =
					new ProjectileState( pShip->GetID(), pShip->GetFaction(),
						rControls.GetTargetID(), pWeapType->GetName(),
						vecPos, pShip->GetOrientation(), m_strName);

				Vector3 vecFiringDir = pShip->GetFiringDir(rWeapID);
				pProj->SetFaceDir(vecFiringDir);

				// Set up the bounding volume data for the state.
				if (pWeapType->GetBV())
					pProj->SetBoundingVolume(pWeapType->GetBV());
				pProj->SetBoundingRadius(pWeapType->GetBoundingRadius());

				Vector3 vecRelVel =
					vecFiringDir.normalisedCopy() * pWeapType->GetInitialSpeed();
				assert(vecRelVel != Vector3::ZERO);
				//cout << "rel vel: " << vecRelVel;
				pProj->SetVel(pShip->GetVel() + vecRelVel);

				//cout << "actual vel: " << pShip->GetVel() + vecRelVel << endl;

				rProjectiles.push_back(pProj);
			}
		}

	}
}

void BfPSectorState::UpdateProjectiles(unsigned long iTime)
{
	assert(iTime >= 0);
	ObjList& rProjectiles = GetObjectList("PROJECTILE");
	// Update every projectile
	for (ObjList::iterator it = rProjectiles.begin();
		 it != rProjectiles.end(); ++it)
	{
		ProjectileState* pProj = (ProjectileState*)*it;
		pProj->Update(iTime);
	}
}

void BfPSectorState::UpdateCapturing(unsigned long iTime)
{
	const ObjList& rShips = GetObjectList("SHIP");
	bool bDominated = true;
	if (rShips.empty())
	{
		m_iTimeHeldByOccupier = 0;
		return;
	}

	ObjList::const_iterator it = rShips.begin();
	const ShipState* pShip = (const ShipState*)*it;
	String strOccupier = pShip->GetFaction();
	++it;
	for (; it != rShips.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		if (pShip->GetFaction() != strOccupier)
		{
			bDominated = false;
			break;
		}
	}

	// If it's still true here, all ships in the sector are of the same faction
	// Otherwise reset the timer.
	if (bDominated) AddCaptureTime(iTime, strOccupier);
	else m_iTimeHeldByOccupier = 0;
}

Vector3 BfPSectorState::ChooseRandomSpawnPoint(const BoundingVolume* pVol) const
{
	const SectorType* pSectorType = GetSectorType();
	const vector<Vector3>& rPoints = pSectorType->GetSpawnPoints();
	assert(!rPoints.empty());

	size_t iPos = rand() % rPoints.size();
	for (size_t i = 0; i < rPoints.size(); ++i)
	{
		BoundingVolume* pTestVol = BVFactory::Get().MakeCopy(pVol);
		pTestVol->Translate(rPoints[iPos]);

		if (CanSpawnVolume(pTestVol)) return rPoints[iPos];
		++iPos;
		if (iPos == rPoints.size()) iPos = 0;
	}

	// Just return the last one in the random sequence if all were occupied
	return rPoints[iPos-1];
}

bool BfPSectorState::CanAddShip(String strType, String strFaction,
   unsigned iGroupID, bool bFlagGroup, const Vector3& rPoint) const
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pType = pShipsConf->GetShipType(strType);

	// TODO: We don't really use the spawn point system as originally intende any more; tidy this up.
//	const vector<Vector3>& rPoints = GetSectorType()->GetSpawnPoints();
//	assert( find(rPoints.begin(), rPoints.end(), rPoint) != rPoints.end() );

	BoundingVolume* pVol = BVFactory::Get().MakeCopy(pType->GetBV());
	pVol->Translate(rPoint);
	if (!GetClosestPassablePoint(pVol))
	{
		delete pVol;
		return false;
	}

	delete pVol;
	return true;
}

const ShipState* BfPSectorState::AddShip(String strType, String strFaction,
   unsigned iGroupID, bool bFlagGroup, const Vector3& rPoint)
{
	//assert( !ObjectExists("SHIP") );
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");
	const ShipType* pType = pShipsConf->GetShipType(strType);

	// TODO: We don't really use the spawn point system as originally intende any more; tidy this up.
//	const vector<Vector3>& rPoints = GetSectorType()->GetSpawnPoints();
//	assert( find(rPoints.begin(), rPoints.end(), rPoint) != rPoints.end() );


	BoundingVolume* pVol = BVFactory::Get().MakeCopy(pType->GetBV());
	pVol->Translate(rPoint);
	if (!GetClosestPassablePoint(pVol))
	{
		delete pVol; // TODO: Surely we can reuse this.
		return NULL;
	}

	Quaternion qOrientation; // Default orientation should be fine for now

	// First to be added of a group is made the leader.
	bool bLeader = !ShipGroupExists(strFaction, iGroupID);
	//cout << "add ship (leader: " << bLeader << ")" << endl;
	ShipState* pShip =
		new ShipState(iGroupID, bFlagGroup, bLeader,
		strType, strFaction, pVol->GetCentre(), qOrientation, m_strName);
	// Bounding volumes are created by the config system and copied when they
	// need to be translated/oriented and checked
	if (pType->GetBV())
		pShip->SetBoundingVolume(pType->GetBV());
	assert(pType->GetBoundingRadius() > 0);
	pShip->SetBoundingRadius(pType->GetBoundingRadius());

	ObjList& rShips = m_mObjects["SHIP"];
	rShips.push_back(pShip);

	delete pVol;
	return pShip;
}

bool BfPSectorState::AddWarpedShip(ShipState* pState, const Vector3& rPoint)
{
	//cout << "AddWarpedShip to " << m_strName << endl;

	// make sure pState is no null and does not already exist in the ship list.
	assert(pState);
	ObjList& rShips = m_mObjects["SHIP"];
	assert( find(rShips.begin(), rShips.end(), pState) == rShips.end() );
	// Make sure the requested point is a valid spawn or warp point.
	const ShipType* pType = pState->GetShipType();
	const vector<Vector3>& rPoints = GetSectorType()->GetWarpPoints();
	assert( find(rPoints.begin(), rPoints.end(), rPoint) != rPoints.end() );

	// Get an unoccupied space near the warp point.
	BoundingVolume* pVol = BVFactory::Get().MakeCopy(pType->GetBV());
	pVol->Translate(rPoint);
	if (!GetClosestPassablePoint(pVol))
	{
		delete pVol;
		return false;
	}

	// Move this ship to that point and place it in the sector.
	pState->SetPosition(pVol->GetCentre());

	rShips.push_back(pState);

	//cout << "AddWarpedShip done" << endl;

	delete pVol;
	return true;
}

const ShipState* BfPSectorState::GetShipGroup(const String& rFaction, unsigned iGroupID,
	vector<const ShipState*>& rMembers) const
{
	//cout << "getshipgroup ID: " << iGroupID << endl;
	assert(rMembers.empty());

	// We only want this to return as NULL if there was no group.
	const ShipState* pLeader = NULL;

	// Make sure it's a valid faction given.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	ObjListMap::const_iterator itAllShips = m_mObjects.find("SHIP");
	assert(itAllShips != m_mObjects.end());
	const ObjList& rAllShips = itAllShips->second;
	for (ObjList::const_iterator it = rAllShips.begin();
		 it != rAllShips.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		if (pShip->GetFaction() == rFaction && pShip->GetGroupID() == iGroupID)
		{
			rMembers.push_back(pShip);
			if (pShip->IsGroupLeader() && !pShip->IsDead())
			{
				assert(!pLeader);
				pLeader = pShip;
			}
		}
	}

	// We may return NULL here; the caller should check if it existed.
	return pLeader;
}

bool BfPSectorState::ShipGroupExists(const String& rFaction, int iGroupID) const
{
	vector<const ShipState*> vShips;
	return GetShipGroup(rFaction, iGroupID, vShips);
}

bool BfPSectorState::RemoveNewShip(int iID)
{
	ObjState* pObj = GetObjState("SHIP", iID);
	if (!pObj) return false;
	ShipState* pShip = (ShipState*)pObj;
	// If we tried to remove a non-new ship like this directly, it'd still be left in the physics/etc systems!
	if (!pShip->GetNew()) return false;

	ObjList& rShips = m_mObjects["SHIP"];
	ObjList::iterator it = find(rShips.begin(), rShips.end(), pObj);
	assert(it != rShips.end());
	rShips.erase(it);

	return true;
}

void BfPSectorState::SetOwner(String strFaction)
{
	// Check the faction exists
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	//cout << "set owner: " << strFaction << endl;
	const FactionType* pFaction = pScenarioConf->GetFactionType(strFaction);
	assert(pFaction);

	// If we have an owner, make that the previous one. Otherwise, make a new blank string.
	if (m_pOwner)
	{
		if (strFaction == *m_pOwner)
			return; // No change
		else
			SetLastOwner(*m_pOwner);
	}
	else
		m_pOwner = new String();

	m_bOwnerChanged = true;
	*m_pOwner = strFaction;
	m_iTimeHeldByOccupier = 0;
}

void BfPSectorState::GetFactionShips(String strFaction, vector<const ShipState*>& rShips) const
{
	// Make sure it's a valid faction given.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(strFaction));

	ObjListMap::const_iterator itAllShips = m_mObjects.find("SHIP");
	assert(itAllShips != m_mObjects.end());
	const ObjList& rAllShips = itAllShips->second;
	for (ObjList::const_iterator it = rAllShips.begin();
		 it != rAllShips.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		if (!pShip->IsDead() && pShip->GetFaction() == strFaction)
			rShips.push_back(pShip);
	}
}

void BfPSectorState::GetFactionShips(String strFaction, vector<ShipState*>& rShips)
{
	// Make sure it's a valid faction given.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert(pScenarioConf->GetFactionType(strFaction));

	ObjListMap::iterator itAllShips = m_mObjects.find("SHIP");
	assert(itAllShips != m_mObjects.end());
	ObjList& rAllShips = itAllShips->second;
	for (ObjList::iterator it = rAllShips.begin();
		 it != rAllShips.end(); ++it)
	{
		ShipState* pShip = (ShipState*)*it;
		if (!pShip->IsDead() && pShip->GetFaction() == strFaction)
			rShips.push_back(pShip);
	}
}

vector<const ShipState*> BfPSectorState::GetFactionShips(String strFaction) const
{
	vector<const ShipState*> vShips;
	GetFactionShips(strFaction, vShips);
	return vShips;
}


bool BfPSectorState::CanSpawnVolume(const BoundingVolume* pVol) const
{
//	cout << "can spawn at " << pVol->GetCentre() << "?" << endl;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSectorType = GetSectorType();
	const BodyVector& rBodies = pSectorType->GetBodies();

	// First check against all bodies
	for (BodyVector::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		const BodyType* pBodyType = pScenarioConf->GetBodyType(pBody->GetType());
		assert(pBodyType);
		BoundingVolume* pBodyVol =
			new BoundingSphere(pBody->GetLocation(), pBodyType->GetBoundingRadius());
		if (pBodyVol->CheckBoundingVolume(pVol))
		{
//			cout << "..no" << endl;
			return false;
		}

		delete pBodyVol;
	}

	// Then check against ships
	const ObjList& rShips = GetObjectList("SHIP");
	for (ObjList::const_iterator it = rShips.begin();
		 it != rShips.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		const ShipType* pType = pShip->GetShipType();
		// NOTE: This isn't so efficient, but spawning isn't done that often..
		BoundingVolume* pShipVol = BVFactory::Get().MakeCopy( pType->GetBV() );
		pShipVol->Translate(pShip->GetPosition());
		if (pShipVol->CheckBoundingVolume(pVol))
		{
//			cout << "..no" << endl;
			return false;
		}
		delete pShipVol;
	}

//	cout << "..yes" << endl;
	return true;
}

bool BfPSectorState::GetClosestPassablePoint(BoundingVolume* pVol) const
{
	static Quaternion qRot;

	// If it can spawn in the first place, nothing needs to be done.
	if (CanSpawnVolume(pVol)) return true;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	//const SectorType* pSectorType = GetSectorType();

	Real fStep = pScenarioConf->GetSpawnDeviationStep();
	Real fMax = pScenarioConf->GetMaxSpawnDeviation();
//	cout << "step: " << fStep << ", max: " << fMax << endl;
	Real fDeviation = 0;
	assert(fDeviation <= fMax);

	// Randomly deviate the position from the centre until a passable point is found.
	Vector3 vecNewPos;
	while (fDeviation <= fMax)
	{
		fDeviation += fStep;
		vecNewPos = pVol->GetCentre();
		for (unsigned i=0; i<6; ++i)
		{
			vecNewPos = Vector3::UNIT_X * fDeviation;
//			cout << "x * deviation: " << vecNewPos << endl;
//			std::stringstream sout;
			unsigned iAngle = rand() % 360;
//			sout << "Rand: " << iAngle << endl;

			qRot.FromAngleAxis(Degree(iAngle), Vector3::UNIT_Y);
			vecNewPos = qRot * vecNewPos;

			//vecNewPos = vecNewPos.randomDeviant( oAngle, Vector3::UNIT_Y  );
//			GlobalEvents::Get().PushEvent(new EventMessage(sout.str()));
//			cout << "angle: " << iAngle << endl;
//			cout << "x * deviation rotated: " << vecNewPos << endl;


			pVol->Translate(vecNewPos);
			if
				(CanSpawnVolume(pVol)) return true;
			else
				pVol->Translate(-vecNewPos);
		}

	}

	return false;
}

void BfPSectorState::AddCaptureTime(unsigned long iTime, const String& rFaction)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	assert(pScenarioConf->GetFactionType(rFaction));

	if (GetOwner() && rFaction == *GetOwner())
	{
		m_iTimeHeldByOccupier = 0;
		return;
	}

	m_iTimeHeldByOccupier += iTime;
	if (m_strLastOccupier != rFaction)
		m_iTimeHeldByOccupier = 0;
	m_strLastOccupier = rFaction;

//	if (m_strName == "Beta") cout << "time held: " << m_iTimeHeldByOccupier << endl;
	if (m_iTimeHeldByOccupier >= pScenarioConf->GetSectorCaptureDelay())
	{
		SetOwner(rFaction);
		m_iTimeHeldByOccupier = 0;
	}
	//cout << m_iTimeHeldByOccupier << endl;
}


vector<const EntityState*> BfPSectorState::GetCollidables() const
{
	// Ships and Projectiles are entities
	ObjListMap::const_iterator itShips = m_mObjects.find("SHIP");
	ObjListMap::const_iterator itProjs = m_mObjects.find("PROJECTILE");
	assert(itShips != m_mObjects.end());
	assert(itProjs != m_mObjects.end());
	const ObjList& rShips = itShips->second;
	const ObjList& rProjs = itProjs->second;

	vector<const EntityState*> vCollidables;
	for (ObjList::const_iterator it = rShips.begin(); it != rShips.end(); ++it)
	{
		const EntityState* pEntity = (const EntityState*)*it;
		if (pEntity->IsCollidable()) vCollidables.push_back(pEntity);
	}
	for (ObjList::const_iterator it = rProjs.begin(); it != rProjs.end(); ++it)
	{
		const EntityState* pEntity = (const EntityState*)*it;
		if (pEntity->IsCollidable()) vCollidables.push_back(pEntity);
	}

	return vCollidables;
}

DistCollidableMap BfPSectorState::GetCollidablesWithin(
	const Vector3& rPoint, const Real& rDist, const ShipType* pShipType) const
{
	// Ships and Projectiles are entities
	ObjListMap::const_iterator itShips = m_mObjects.find("SHIP");
	ObjListMap::const_iterator itProjs = m_mObjects.find("PROJECTILE");
	assert(itShips != m_mObjects.end());
	assert(itProjs != m_mObjects.end());
	const ObjList& rShips = itShips->second;
	const ObjList& rProjs = itProjs->second;

	Real fMaxDistSq = rDist*rDist;
	Real fDistSq;

	const SectorType* pSectorType = GetSectorType();

	DistCollidableMap mCollidables;
	const BodyVector& rBodies = pSectorType->GetBodies();
	for (BodyVector::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		fDistSq = rPoint.squaredDistance( pBody->GetLocation() );
		if (fDistSq <= fMaxDistSq)
		{
			pair<Real, Collidable> oPair (fDistSq, Collidable(m_strName, pBody->GetID()) );
			mCollidables.insert(oPair);
		}
	}
	for (ObjList::const_iterator it = rShips.begin(); it != rShips.end(); ++it)
	{
		const EntityState* pEntity = (const EntityState*)*it;
		if (!pEntity->IsCollidable()) continue;

		fDistSq = rPoint.squaredDistance( pEntity->GetPosition() );
		if (fDistSq <= fMaxDistSq)
		{
			pair<Real, Collidable> oPair (fDistSq, Collidable(pEntity) );
			mCollidables.insert(oPair);
		}
	}
	for (ObjList::const_iterator it = rProjs.begin(); it != rProjs.end(); ++it)
	{
		const ProjectileState* pProj = (const ProjectileState*)*it;
		if (!pProj->IsCollidable()) continue;
		// If we have a ship config, exclude this projectile if it's not set to dodge.
		if ( pShipType && !pShipType->GetDodgeWeaponType(pProj->GetSubType()) )
			continue;

		fDistSq = rPoint.squaredDistance( pProj->GetPosition() );
		if (fDistSq <= fMaxDistSq)
		{
			pair<Real, Collidable> oPair (fDistSq, Collidable(pProj) );
			mCollidables.insert(oPair);
		}
	}

	return mCollidables;
}

void BfPSectorState::SetLastOwner(const String& rOwner)
{
	if (!m_pLastOwner) m_pLastOwner = new String(rOwner);
	else *m_pLastOwner = rOwner;
}
