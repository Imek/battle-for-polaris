#include <UI/BfPInput.h>

BfPInput::BfPInput(OgreFramework* pOgre)
: m_pInputMgr(NULL)
, m_pKeyboard(NULL)
, m_pMouse(NULL)
, m_pOgre(pOgre)
, m_pGUI(NULL)
, m_pPlayer(NULL)
, m_iCamMode(CM_CHASE)
, m_iGameMode(GVM_SECTOR)
, m_bShift(false)
, m_bShutDown(false)
, m_bQuitToMenu(false)
, m_bJustSwitched(true)
, m_vecKeyMotion(Vector4(0,0,0,0))
, m_vecMousePanning(Vector2::ZERO)
, m_vecMouseRotation(Vector3::ZERO)
, m_iWheelZoom(0)
, m_iZoom(0)
, m_vecCamRelativePos((Vector3::UNIT_X + Vector3::UNIT_Y/6).normalisedCopy())
, m_fCamCurrentZoom(0)
, m_fCamSetZoom(0)
, m_fCamZoomVel(0)
, m_vecChaseCamIdealPos((Vector3::UNIT_X + Vector3::UNIT_Y/6).normalisedCopy())
, m_fChaseCamXFromIdeal(0)
, m_fChaseCamYFromIdeal(0)
, m_fChaseCamXVelocity(0)
, m_fChaseCamYVelocity(0)
, m_iChaseCamDeathTimer(0)
, m_oShiftTurned(0)
{
	m_pOgre->GetLog()->logMessage("Initialising input system...");

	static Quaternion qRot;

	Ogre::Camera* pCamera = m_pOgre->GetCamera();
	qRot.FromAngleAxis(Degree(90), pCamera->getUp());
	m_qCamOffset = m_qCamOffset * qRot;

	m_vecCamRelativePos.normalise();
	m_vecChaseCamIdealPos.normalise();

	// Multiply pos and ideal pos by the initial distance
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	m_vecCamRelativePos *= pGameConf->GetChaseCamInitialDist();
	m_vecChaseCamIdealPos *= pGameConf->GetChaseCamInitialDist();
	m_fCamCurrentZoom = pGameConf->GetChaseCamInitialDist();
	m_fCamSetZoom = pGameConf->GetChaseCamInitialDist();


	// Set up the InputManager, Keyboard and Mouse objects using the Ogre framework
	unsigned long hWnd = 0;
	OIS::ParamList paramList;
	pOgre->GetRenderWindow()->getCustomAttribute("WINDOW", &hWnd);
	paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

	m_pInputMgr = OIS::InputManager::createInputSystem(paramList);

	m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputMgr->createInputObject(OIS::OISKeyboard, true));
	m_pMouse = static_cast<OIS::Mouse*>(m_pInputMgr->createInputObject(OIS::OISMouse, true));

	m_pMouse->getMouseState().height = m_pOgre->GetRenderWindow()->getHeight();
	m_pMouse->getMouseState().width	 = m_pOgre->GetRenderWindow()->getWidth();

	m_pKeyboard->setEventCallback(this);
	m_pMouse->setEventCallback(this);

	// Set up the GUI
	m_pGUI = new BfPGUI(m_iCamMode);
	m_pGUI->Init();

	m_pOgre->GetLog()->logMessage("... Input system initialised.");

	if (m_iCamMode == CM_LOGICENGINE && !pGameConf->GetShowAIDebugInterface())
		ToggleMode();


}

BfPInput::~BfPInput()
{
	delete m_pGUI;
	delete m_pKeyboard;
	delete m_pMouse;

	OIS::InputManager::destroyInputSystem(m_pInputMgr);
}

bool BfPInput::keyPressed(const OIS::KeyEvent &rEvent)
{
	// Keybindings are in the GameConfig
	const GeneralConfig* pGameConf = (GeneralConfig*)GameConfig::GetInstance().GetConfig("General");

	String strKey = m_pKeyboard->getAsString(rEvent.key);
	//m_pOgre->GetLog()->logMessage("BfPInput::keyPressed: " + strKey);

	// System keyboard input
	if (rEvent.key ==  OIS::KC_ESCAPE)
	{
		// Escape shows a confirm dialogue to quit to the menu (does nothing if already in the menu)
		m_pGUI->QuitToMenu();
		return true;
	}

	if (rEvent.key == OIS::KC_RETURN)
	{
		m_pGUI->ShowPauseDialogue();
	}


	// Even if we're in the GUI we need to update they KeyMotion values
	// (In case a key is being held when the mode changes)

	if (JFUtil::StringEquals(strKey, pGameConf->GetZoomInKey()))
	{
		m_iZoom -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetZoomOutKey()))
	{
		m_iZoom += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeLKey()))
	{
		m_vecKeyMotion.x -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeRKey()))
	{
		m_vecKeyMotion.x += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeUKey()))
	{
		m_vecKeyMotion.y += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeDKey()))
	{
		m_vecKeyMotion.y -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetAccelKey()))
	{
		m_vecKeyMotion.z -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetDecelKey()))
	{
		m_vecKeyMotion.z += 1;
	}

	// Rolling controls
	if (JFUtil::StringEquals(strKey, pGameConf->GetRollLKey()))
	{
		m_vecKeyMotion.w += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetRollRKey()))
	{
		m_vecKeyMotion.w -= 1;
	}


	// Anything after this is disabled in menu mode
	if (!ShowingGame()) return true;


	if(rEvent.key == OIS::KC_SYSRQ)
	{
		return m_pOgre->TakeScreenShot();
	}

//	if(rEvent.key == OIS::KC_M)
//	{
//		return m_pOgre->TogglePolyMode();
//		return true;
//	}
//
//	if(rEvent.key == OIS::KC_O)
//		return m_pOgre->ToggleOverlayMode();

	// See if we pressed a number key, and if our faction has that group select it
	std::stringstream sout;
	sout << strKey;
	unsigned iKeyNo;
	bool bSelGrpEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTGROUP);
	if (bSelGrpEnabled && !(sout >> iKeyNo).fail() && m_iCamMode != CM_LOGICENGINE)
	{
		m_pGUI->SetTargetMode(TM_GROUPS);
		if (m_pPlayer->GetGroupWithID(iKeyNo))
			m_pPlayer->SetSelectedGroup(iKeyNo, m_iCamMode != CM_CHASE);
		else
			m_pPlayer->UnsetSelectedGroup();

		return true;
	}
	sout.str(""); sout.clear();

	// Camera-related keyboard input
	bool bShiftEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SHIFT);
	if(bShiftEnabled && JFUtil::StringEquals(strKey, pGameConf->GetCamShiftKey()))
	{
		m_bShift = true;
		m_pGUI->SetShift(true);
		return true;
	}

	bool bSwitchModeEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SWITCHMODE);
	if(bSwitchModeEnabled && JFUtil::StringEquals(strKey, pGameConf->GetSwitchModeKey()))
	{
		ToggleMode(!m_bShift);
		return true;
	}

	//cout << "key pressed: " << strKey << endl;

	// GUI keys
	bool bTargetingEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(TARGETING);
	if (bTargetingEnabled)
	{
		if (JFUtil::StringEquals(strKey, pGameConf->GetToggleTargetModeKey()))
		{
			m_pGUI->ToggleTargetMode(!m_bShift);
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetNextTargetKey()))
		{
			m_pGUI->SwitchTarget(!m_bShift);
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetClosestEnemyKey()))
		{
			m_pGUI->TargetClosestEnemy();
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetNextEnemyKey()))
		{
			m_pGUI->TargetNextEnemy(!m_bShift);
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetUnselectTargetKey()))
		{
			m_pGUI->UnselectTarget();
			return true;
		}

	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetToggleGroupStanceKey()))
	{
		m_pGUI->ToggleGroupStance();
		return true;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetPauseKey()))
	{
		m_pGUI->ShowPauseDialogue();
		return true;
	}



	// Ship-related keyboard input, if appropriate.
	if (!m_pPlayer->HasShips()) return true;

	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	// Set all to false for now.
	pControls->m_bIncThrust = false;
	pControls->m_bRedThrust = false;
//	pControls->m_bStrafeL = false;
//	pControls->m_bStrafeR = false;
//	pControls->m_bStrafeU = false;
//	pControls->m_bStrafeD = false;
	pControls->m_bBrake = false;

	// Only do these controls if the camera and input state are in the appropriate mode.
	bool bMovementEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(MOVEMENT);
	bool bBrakeEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(BRAKE);
	bool bWeaponsEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(WEAPONS);
	if (m_iCamMode == CM_CHASE && !m_bShift)
	{
		if (bMovementEnabled)
		{
			if (JFUtil::StringEquals(strKey, pGameConf->GetAccelKey()))
			{
				pControls->m_bIncThrust = true;
				return true;
			}

			if (JFUtil::StringEquals(strKey, pGameConf->GetDecelKey()))
			{
				pControls->m_bRedThrust = true;
				return true;
			}

			if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeLKey()))
			{
				pControls->m_bStrafeL = true;
				return true;
			}

			if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeRKey()))
			{
				pControls->m_bStrafeR = true;
				return true;
			}

			if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeUKey()))
			{
				pControls->m_bStrafeU = true;
				return true;
			}

			if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeDKey()))
			{
				pControls->m_bStrafeD = true;
				return true;
			}
		}


		if (bBrakeEnabled && JFUtil::StringEquals(strKey, pGameConf->GetBrakeKey()))
		{
			pControls->m_bBrake = true;
			return true;
		}

		// Weapons
		if (bWeaponsEnabled && JFUtil::StringEquals(strKey, pGameConf->GetNextWeapGroupKey()))
		{
			pControls->ToggleWeapons(m_bShift);
			return true;
		}


//		if (JFUtil::StringEquals(strKey, pGameConf->GetWarpKey())
//			pControls->m_bWarp = true;
	}



	return true;

}


bool BfPInput::keyReleased(const OIS::KeyEvent &rEvent)
{
	// Keybindings are in the GameConfig
	const GeneralConfig* pGameConf = (GeneralConfig*)GameConfig::GetInstance().GetConfig("General");

	String strKey = m_pKeyboard->getAsString(rEvent.key);

	if(JFUtil::StringEquals(strKey, pGameConf->GetCamShiftKey()))
	{
		m_pGUI->SetShift(false);
		m_bShift = false;
		return true;
	}

	// Key motion is used in various ways depending on the camera mode.
	if (JFUtil::StringEquals(strKey, pGameConf->GetZoomInKey()))
	{
		m_iZoom += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetZoomOutKey()))
	{
		m_iZoom -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeLKey()))
	{
		m_vecKeyMotion.x += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeRKey()))
	{
		m_vecKeyMotion.x -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeUKey()))
	{
		m_vecKeyMotion.y -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeDKey()))
	{
		m_vecKeyMotion.y += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetAccelKey()))
	{
		m_vecKeyMotion.z += 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetDecelKey()))
	{
		m_vecKeyMotion.z -= 1;
	}

	// Rolling controls for various cameras
	if (JFUtil::StringEquals(strKey, pGameConf->GetRollLKey()))
	{
		m_vecKeyMotion.w -= 1;
	}

	if (JFUtil::StringEquals(strKey, pGameConf->GetRollRKey()))
	{
		m_vecKeyMotion.w += 1;
	}

	// After this point only do anything if we're controlling the game and have a ship
	if (!ShowingGame() || !m_pPlayer->HasShips()) return true;

	// TODO: Move stuff like this to separate functions.
	// Ship-related keyboard input
	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	// Only do these controls if the camera is in the appropriate mode.
	if (m_iCamMode == CM_CHASE && !m_bShift)
	{
		if (JFUtil::StringEquals(strKey, pGameConf->GetAccelKey()))
		{
			pControls->m_bIncThrust = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetDecelKey()))
		{
			pControls->m_bRedThrust = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeLKey()))
		{
			pControls->m_bStrafeL = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeRKey()))
		{
			pControls->m_bStrafeR = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeUKey()))
		{
			pControls->m_bStrafeU = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetStrafeDKey()))
		{
			pControls->m_bStrafeD = false;
			return true;
		}

		if (JFUtil::StringEquals(strKey, pGameConf->GetBrakeKey()))
		{
			pControls->m_bBrake = false;
			return true;
		}

//		if (JFUtil::StringEquals(strKey, pGameConf->GetWarpKey()))
//			pControls->m_bWarp = false;
	}

	return true;
}

bool BfPInput::mouseMoved(const OIS::MouseEvent &evt)
{
	// X/Y mouse movement
	m_vecMouseRotation.x += -0.1 * evt.state.X.rel;
	m_vecMouseRotation.y += -0.1 * evt.state.Y.rel;

	// Anything after this isn't done if we're not in game mode
	if (!ShowingGame()) return true;
	if (evt.state.Z.rel != 0)
		m_iWheelZoom = (evt.state.Z.rel > 0) ? -1 : 1;
//	else
//		m_iWheelZoom = 0;

	return true;
}

bool BfPInput::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	// Keybindings are in the GameConfig
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	// Inject this event into the GUI system
	m_pGUI->MousePressed(id);

	// Anything after this isn't done if we're not in game mode
	if (!ShowingGame()) return true;

	// Shift can only become true if it's enabled
	bool bShiftEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SHIFT);
	if (!bShiftEnabled)
		m_bShift = false;
	else if (id == pGameConf->GetCamShiftMButton())
		m_bShift = true;
	bool bSwitchModeEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SWITCHMODE);
	if (id == pGameConf->GetSwitchModeMButton() && bSwitchModeEnabled)
		ToggleMode();

	// Selection controls
	if (!DoTacticalMouseSelection(evt, id)) return false;
	if (!DoMapMouseSelection(evt, id)) return false;

	// Ship-related controls
	if (!m_pPlayer->HasShips()) return true;

	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	pControls->m_bFiringGroup = false;
	pControls->m_bFiringAll = false;
	bool bWeaponsEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(WEAPONS);
	if (m_iCamMode == CM_CHASE && !m_bShift && bWeaponsEnabled)
	{
		if (id == pGameConf->GetFireAllMButton())
			pControls->m_bFiringAll = true;
		else if (id == pGameConf->GetFireGroupMButton())
			pControls->m_bFiringGroup = true;
	}

	return true;
}

bool BfPInput::DoTacticalMouseSelection(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (m_iCamMode != CM_TACTICAL) return true;

	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	Real fX = m_pGUI->GetMouseX() / m_pOgre->GetRenderWindow()->getWidth();
	Real fY = m_pGUI->GetMouseY() / m_pOgre->GetRenderWindow()->getHeight();
	if (fX < pGameConf->GetTacticalSelectMinX() ||
		fY > pGameConf->GetTacticalSelectMaxY())
	{
		// Clicking outside the view area doesn't do any selection
		return true;
	}

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const SectorType* pSectorType = pScenarioConf->GetSectorType(
		m_pPlayer->GetSelectedSector());
	assert(pSectorType);

	// Get a ray from the mouse position through the camera and see if it hits anything
	Ogre::Ray oRay = m_pOgre->GetCamera()->getCameraToViewportRay(fX, fY);
	Ogre::Sphere oSphere;
	// First check bodies
	const BodyVector& rBodies = pSectorType->GetBodies();
	bool bHitObject = false;
	// Don't bother if the SELECTBODY control is disabled.
	bool bMoveEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(MOVEGROUP);
	bool bObjEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTBODY);
	if (bObjEnabled)
	{
		for (BodyVector::const_iterator it = rBodies.begin();
			 it != rBodies.end(); ++it)
		{
			const BodyInstance* pBody = *it;
			const BodyType* pBodyType = pScenarioConf->GetBodyType(pBody->GetType());
			oSphere = Ogre::Sphere(pBody->GetLocation(), pBodyType->GetBoundingRadius());
			pair<bool, Real> oResults = oRay.intersects(oSphere);
			if (oResults.first)
			{
				bHitObject = true;
				// Left-click selects; right-click selects and does a move if appropriate.
				if (id == OIS::MB_Left || id == OIS::MB_Right)
					m_pPlayer->SetSelectedObject(pBody->GetID());
				if (id == OIS::MB_Right && m_pPlayer->CanMoveGroup() && bMoveEnabled)
					m_pPlayer->MoveGroup();
			}
		}
	}
	// Next check warp points
	// Clicking on a warp point goes to the sector to which it leads.
	// But first, check if it's even enabled.
	bool bWarpEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTSECTOR);
	if (bWarpEnabled)
	{
		const Real& rWarpRadius = pScenarioConf->GetMaxWarpDist();
		const StringVector& rLinks = pSectorType->GetLinkedSectors();
		for (StringVector::const_iterator it = rLinks.begin();
			 it != rLinks.end(); ++it)
		{
			const String& rDestination = *it;
			const Vector3& rWarpPos = pSectorType->GetWarpPointFromSector(rDestination);
			oSphere = Ogre::Sphere(rWarpPos, rWarpRadius);
			pair<bool, Real> oResults = oRay.intersects(oSphere);
			if (oResults.first)
			{
				// Left-click selects; right-click selects and does a move if appropriate.
				if (id == OIS::MB_Left || id == OIS::MB_Right)
					m_pPlayer->SetSelectedSector(rDestination);
				if (id == OIS::MB_Right && m_pPlayer->CanMoveGroup() && bMoveEnabled)
					m_pPlayer->MoveGroup();
			}
		}
	}

	// Check click-selection of groups, if the sector is visible.
	const BfPSectorState* pSector =
		m_pPlayer->GetObservedState()->ObserveSector( pSectorType->GetName() );
	bool bGrpEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTGROUP);
	if (pSector && !bHitObject && bGrpEnabled)
	{
		// See if we hit a ship; we can safely assume that we won't have problems
		// with constantly hitting multiple (unless the select radius multiplier is too high)
		const ObjList& rShips = pSector->GetObjectList("SHIP");
		for (ObjList::const_iterator it = rShips.begin(); it != rShips.end(); ++it)
		{
			// Discard the ship if it's a different faction
			const ShipState* pState = (const ShipState*)*it;
			if (pState->GetFaction() != m_pPlayer->GetFactionName()) continue;
			oSphere = Ogre::Sphere(pState->GetPosition(),
				pState->GetBoundingRadius() * pGameConf->GetTacticalSelectRadiusMult());
			pair<bool, Real> oResults = oRay.intersects(oSphere);
			if (oResults.first)
			{
				bHitObject = true;
				// Left/right click both select
				if (id == OIS::MB_Left || id == OIS::MB_Right)
					m_pPlayer->SetSelectedGroup(pState->GetGroupID(), false);
			}
		}

	}

	// If it was enabled but we didn't hit any object, unselect.
	// We don't bother with deselection of groups.
	if (!bHitObject && bObjEnabled) m_pPlayer->UnsetSelectedObject();

	return true;
}

bool BfPInput::DoMapMouseSelection(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (m_iCamMode != CM_MAP && m_iCamMode != CM_LOGICENGINE) return true;

	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	Real fX = m_pGUI->GetMouseX() / m_pOgre->GetRenderWindow()->getWidth();
	Real fY = m_pGUI->GetMouseY() / m_pOgre->GetRenderWindow()->getHeight();
	if (fX < pGameConf->GetTacticalSelectMinX() ||
		fY > pGameConf->GetTacticalSelectMaxY())
	{
		// Clicking outside the view area doesn't do any selection
		return true;
	}

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Get a ray from the mouse position through the camera and see if it hits anything
	Ogre::Ray oRay = m_pOgre->GetCamera()->getCameraToViewportRay(fX, fY);
	// Check each sector's position against the ray
	Ogre::Sphere oSphere (Vector3::ZERO, pScenarioConf->GetMapNodeSize() * 50); // Size of ogre pre-set sphere..
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const String& rSector = *it;
		const SectorType* pType = pScenarioConf->GetSectorType(rSector);
		assert(pType);

		oSphere.setCenter(pType->GetMapRelPos());
		pair<bool, Real> oResults = oRay.intersects(oSphere);
		if (oResults.first)
		{
			// Left-click selects; right-click selects and does a move if appropriate.
			if (id == OIS::MB_Left || id == OIS::MB_Right)
				m_pPlayer->SetSelectedSector(rSector);
			if (id == OIS::MB_Right && m_pPlayer->CanMoveGroup())
				m_pPlayer->MoveGroup();
		}
	}

	return true;
}

bool BfPInput::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	// Keybindings are in the GameConfig
	const GeneralConfig* pGameConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	// Inject this event into the GUI system
	m_pGUI->MouseReleased(id);

	if (!ShowingGame()) return true;

	if (id == pGameConf->GetCamShiftMButton())
		m_bShift = false;

	// Ship-related controls
	if (!m_pPlayer->HasShips()) return true;

	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	if (m_iCamMode == CM_CHASE && !m_bShift)
	{
		if (id == pGameConf->GetFireAllMButton())
			pControls->m_bFiringAll = false;
		if (id == pGameConf->GetFireGroupMButton())
			pControls->m_bFiringGroup = false;
	}

	return true;
}

bool BfPInput::SetPlayer(FactionController* pPlayer)
{
	assert(!m_pPlayer);
	if (!pPlayer) return false;
	m_pPlayer = pPlayer;
	m_pGUI->SetPlayer(pPlayer);

	return true;
}

void BfPInput::Reset()
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	// set out player to NULL (was already deleted!)
	m_pPlayer = NULL;

	// Reset other values to the constructor values (except for managed & ogre stuff)
	m_iCamMode = CM_CHASE;
	m_iGameMode = GVM_SECTOR;
	m_bShift = false;
	m_bShutDown = false;
	m_bQuitToMenu = false;
	m_bJustSwitched = true;
	m_vecMousePanning = Vector2::ZERO;
	m_vecMouseRotation = Vector3::ZERO;
	//m_iWheelZoom = 0;
	//m_iZoom = 0;
	m_vecCamRelativePos = (Vector3::UNIT_X + Vector3::UNIT_Y/6).normalisedCopy();
	m_fCamCurrentZoom = 0;
	m_fCamSetZoom = 0;
	m_fCamZoomVel = 0;
	m_vecChaseCamIdealPos = (Vector3::UNIT_X + Vector3::UNIT_Y/6).normalisedCopy();
	m_fChaseCamXFromIdeal = 0;
	m_fChaseCamYFromIdeal = 0;
	m_fChaseCamXVelocity = 0;
	m_fChaseCamYVelocity = 0;
	m_iChaseCamDeathTimer = 0;
	m_oShiftTurned = 0;

	// reset the GUI system
	m_pGUI->Reset(m_iCamMode);

	m_pOgre->GetLog()->logMessage("... Input system initialised.");

	if (m_iCamMode == CM_LOGICENGINE && !pGameConf->GetShowAIDebugInterface())
		ToggleMode();

}

void BfPInput::Update(unsigned long iTime)
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	// TODO: These are probably unnecessary, so remove
	m_pKeyboard->capture();
	m_pMouse->capture();

	if (m_pGUI->ShowingGame())
	{
		if (m_pPlayer->HasShips())
		{
			// Set the flagship group to autopilot if the player isn't in direct control.
			m_pPlayer->GetFlagShipGroup()->SetAutoPilot(
				m_iCamMode != CM_CHASE ); // Give control of the flagship if in chase cam mode.
		}
		else if (m_iCamMode == CM_CHASE) // No ships and camera mode is chase
		{
			m_bJustSwitched = true;
			m_iCamMode = CM_TACTICAL;
			m_pGUI->SetMode(CM_TACTICAL);
		}

		switch (m_iCamMode)
		{
		case CM_FREELOOK:
			UpdateFreeCamera(iTime);
			break;
		case CM_CHASE:
			UpdateChaseMode(iTime);
			break;
		case CM_TACTICAL:
			UpdateTacticalMode(iTime);
			break;
		case CM_LOGICENGINE:
			// Same as map camera
		case CM_MAP:
			UpdateMapMode(iTime);
			break;
		default:
			throw JFUtil::Exception("BfPInput", "Update", "Invalid camera mode found??");
		}

		// Some stuff needs to be reset to 0 every update
		m_bJustSwitched = false;
		m_iWheelZoom = 0;

	}
	else// if (m_pGUI->GetGUIMode() == GM_MENU)
	{
		// Move the mouse for the menu
		m_pGUI->MoveMouse(-m_vecMouseRotation.x * pGameConf->GetMenuPointerSensitivity() * (Real)iTime,
						  -m_vecMouseRotation.y * pGameConf->GetMenuPointerSensitivity() * (Real)iTime);
	}

	// Reset the stuff that we want to check for per update
	m_vecMouseRotation = Vector3::ZERO;

	// Do GUI update in any case
	m_pGUI->Update(iTime);
	// Check for requests to either shut down the game or quit to the menu.
	if (m_pGUI->GetQuit()) m_bShutDown = true;
	else if (m_pGUI->GetEndGame()) m_bQuitToMenu = true;

}

void BfPInput::ToggleMode(bool bNext)
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	m_bJustSwitched = true;
	if (!bNext && m_iCamMode == 0)
		m_iCamMode = (CAMERA_MODE)(CM_LAST-1);
	else
		// Wraps around to the last mode if -1, or first if CM_LAST
		m_iCamMode = (CAMERA_MODE) ((m_iCamMode + (bNext ? 1 : -1)) % CM_LAST);


	assert(m_iCamMode != CM_LAST);
	// Decide what game mode to have (the scene drawn)
	if (m_iCamMode == CM_MAP || m_iCamMode == CM_LOGICENGINE)
		m_iGameMode = GVM_MAP;
	else
		m_iGameMode = GVM_SECTOR;

	// Reset the player's selected sector if we're in the home sector.
	if (m_iCamMode == CM_CHASE || m_iCamMode == CM_FREELOOK)
		m_pPlayer->UnsetSelectedSector();

	// Need to do some things to the flag ship itself, if we have one.
	if (m_pPlayer->HasShips())
	{
		// Set the flagship group to autopilot if the player isn't in direct control.
		m_pPlayer->GetFlagShipGroup()->SetAutoPilot(
			m_iCamMode != CM_CHASE );
		// Reset movement, if e.g. another key is pressed.
		ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
		assert(pPlayerShip);
		ControlState* pControls = pPlayerShip->GetControlState();
		assert(pControls);
		pControls->ResetMovement();
	}

	m_pGUI->SetMode(m_iCamMode);

	// Check if any modes need to be skipped (NOTE: shouldn't infinite loop as long as the tactical camera is never disabled..)
	if (!pGameConf->GetEnableFreeCam() && m_iCamMode == CM_FREELOOK) ToggleMode(bNext);
	if (!m_pPlayer->HasShips() && m_iCamMode == CM_CHASE) ToggleMode(bNext);
	if (!pGameConf->GetShowAIDebugInterface() && m_iCamMode == CM_LOGICENGINE) ToggleMode(bNext);

}


void BfPInput::UpdateFreeCamera(unsigned long iTime)
{
	const GeneralConfig* pConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	static Quaternion qRot;
	Real rTime = (Real)iTime;
	Ogre::Camera* pCamera = m_pOgre->GetCamera();

	Vector3 vecTranslate (m_vecKeyMotion.x, m_vecKeyMotion.y, m_vecKeyMotion.z);
	vecTranslate.normalise();
	vecTranslate = pCamera->getOrientation() * vecTranslate;

	Vector3 vecRotate = m_vecMouseRotation;

	qRot.FromAngleAxis(Degree(vecRotate.x * pConf->GetCamRotation() * rTime),
		pCamera->getUp());

	pCamera->rotate(qRot);
	//pCamera->yaw();
	pCamera->pitch(Degree(vecRotate.y * pConf->GetCamRotation() * rTime));
	pCamera->roll(Degree(m_vecKeyMotion.w * pConf->GetCamRotation()/4 * rTime));
	pCamera->move(vecTranslate * pConf->GetCamSpeed() * rTime);
}

void BfPInput::UpdateChaseMode(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	// Do the tutorial trigger update if applicable
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
		UpdateChaseTutorial(iTime);
	// Do the camera update
	UpdateChaseCamera(iTime);
}

void BfPInput::UpdateChaseTutorial(unsigned long iTime)
{
	// Get the stuff we need
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert( pScenarioConf->GetEventsEnabled("Tutorial") );

	assert(m_pPlayer->HasShips());
	const ShipState* pPlayer = m_pPlayer->GetFlagShip()->GetState();
	const ShipType* pPlayerType = pPlayer->GetShipType();
	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	// Check for trigger events
	EventHandler& rEvents = GlobalEvents::Get();
	// Throw the ChaseCam tutorial trigger
	rEvents.PushEvent( new EventTrigger("ChaseCam") );
	// Check for player ship triggers
	Real fSpeedSq = pPlayer->GetVel().squaredLength();
	Real fMaxSpeedSq = pPlayerType->GetMaxSpeed() * pPlayerType->GetMaxSpeed();
	if (fSpeedSq >= fMaxSpeedSq &&
		!pControls->m_bStrafeD && !pControls->m_bStrafeL &&
		!pControls->m_bStrafeR && !pControls->m_bStrafeU)
		rEvents.PushEvent( new EventTrigger("ChaseMaxSpeed") );
	if (fSpeedSq <= 0.00000001f && pControls->m_bBrake)
		rEvents.PushEvent( new EventTrigger("ChaseStopped") );
	// Events for the targeting tutorial
	if (m_pGUI->GetTargetMode() == TM_BODIES)
	{
		rEvents.PushEvent( new EventTrigger("ChaseObjMode") );
		const String* pObj = m_pPlayer->GetFlagShipGroup()->GetDestBody();
		if (pObj)
		{
			rEvents.PushEvent( new EventTrigger("ChaseObjSelected") );
			// Check if we're facing towards it
			const Vector3& rFaceDir = pPlayer->GetFaceDir();
			const SectorType* pSector = pScenarioConf->GetSectorType(pPlayer->GetSectorID());
			const BodyInstance* pBody = pSector->GetBodyWithID(*pObj);
			Vector3 vecTargDir = (pBody->GetLocation() - pPlayer->GetPosition()).normalisedCopy();
			Degree degAngle = vecTargDir.angleBetween(rFaceDir);
			if (degAngle.valueDegrees() <= 25)
				rEvents.PushEvent( new EventTrigger("ChaseObjFacing") );
		}
		else rEvents.PushEvent( new EventTrigger("ChaseNoObjSelected") );
	}
	else if (m_pGUI->GetTargetMode() == TM_SECTORS)
	{
		const String* pSelected = m_pPlayer->GetFlagShipGroup()->GetDestSector();
		if (pSelected && *pSelected != pPlayer->GetSectorID())
		{
			rEvents.PushEvent( new EventTrigger("ChaseWarpSelected") );
		}
	}
	else if (m_pGUI->GetTargetMode() == TM_ENEMIES)
	{
		if (pPlayerShip->HasTargetShip())
		{
			//unsigned iSelected = m_pPlayer->GetFlagShip()->GetTargetShip();
			rEvents.PushEvent( new EventTrigger("ChaseEnemySelected") );
		}
	}
	// Events related to the sector state
	const BfPSectorState* pSector = pPlayerShip->GetSector();
	assert(pSector);
	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	bool bNoEnemies = true;
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		const String& rFaction = *it;
		if (rFaction == m_pPlayer->GetFactionName()) continue;
		std::vector<const ShipState*> vShips = pSector->GetFactionShips(rFaction);
		if (!vShips.empty()) bNoEnemies = false;
	}
	if (bNoEnemies)
		rEvents.PushEvent( new EventTrigger("ChaseNoEnemies") );
	else
		rEvents.PushEvent( new EventTrigger("ChaseEnemies") );
}

void BfPInput::UpdateChaseCamera(unsigned long iTime)
{
	// TODO: Optimise by putting this static stuff in the class rather than the function and using it elsewhere
	static Degree rAngle;
	static Degree rMaxAngle;
	static Real rDist;
	static Real rMaxDist;
	static Vector3 vecAxis;
	static Quaternion qRot;

	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	Ogre::Camera* pCamera = m_pOgre->GetCamera();
	assert(m_pPlayer->HasShips());
	const ShipState* pPlayer = m_pPlayer->GetFlagShip()->GetState();
	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	if (m_bJustSwitched)
	{
		// Move mouse to centre
		m_pGUI->ResetMouse();

		//cout << "SWITCH" << endl;
		m_vecChaseCamIdealPos = (Vector3::UNIT_X + Vector3::UNIT_Y/6).normalisedCopy();
		m_vecChaseCamIdealPos *= pGameConf->GetChaseCamInitialDist();
		m_vecCamRelativePos = m_vecChaseCamIdealPos;
		m_qCamRelativeOrientation = Quaternion();
		m_fChaseCamXFromIdeal = 0;
		m_fChaseCamYFromIdeal = 0;
		m_fChaseCamXVelocity = 0;
		m_fChaseCamYVelocity = 0;
		m_fCamSetZoom = pGameConf->GetChaseCamInitialDist();
		m_fCamCurrentZoom = m_fCamSetZoom;
		m_fCamZoomVel = 0;
		m_iChaseCamDeathTimer = 0;

		pCamera->setPosition(pPlayer->GetPosition() + m_vecCamRelativePos);
		pCamera->setDirection(pPlayer->GetFaceDir());
		pCamera->setOrientation(m_qCamOffset * pPlayer->GetOrientation());
	}

	// If our ship has died, set off the death camera and don't pass this point until it has reached 0.
	if (pPlayer->GetExplodeTrigger())
		m_iChaseCamDeathTimer = pGameConf->GetDeathCamDelay();
	if (m_iChaseCamDeathTimer > 0)
	{
		m_iChaseCamDeathTimer -= iTime;
		if (m_iChaseCamDeathTimer < 0) m_iChaseCamDeathTimer = 0;
		return;
	}

	if (m_bShift)
	{
		pControls->SetYaw(0);
		pControls->SetPitch(0);
		pControls->SetRoll(0);
		UpdateMouseRotation(iTime);

	}
	else
	{
		// TODO: Decompose this stuff into separate functions later
		// CAMERA ZOOM (mouse wheel takes precedence over keys)
		Real fZoom = (m_iWheelZoom != 0) ? (Real)m_iWheelZoom : (Real)m_iZoom;
		m_fCamSetZoom += fZoom *pGameConf->GetChaseCamZoomSpeed();
		rDist = pGameConf->GetChaseCamMinDist();
		rMaxDist = pGameConf->GetChaseCamMaxDist();
		assert(rDist < rMaxDist);
		if (m_fCamSetZoom < rDist) m_fCamSetZoom = rDist;
		if (m_fCamSetZoom > rMaxDist) m_fCamSetZoom = rMaxDist;

		// SHIP TURNING
		// Apply mouse motion to our yaw/pitch controls, when controls are appropriate.
		// We do this by turning the ship towards where the mouse pointer is.
		m_pGUI->MoveMouse(-m_vecMouseRotation.x * pGameConf->GetChaseCamSensitivity() * (Real)iTime,
						  -m_vecMouseRotation.y * pGameConf->GetChaseCamSensitivity() * (Real)iTime);

		// Only do these controls if the camera is in the appropriate mode, and turning is enabled.
		bool bTurningEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(TURNING);
		if (m_iCamMode == CM_CHASE && !m_bShift && bTurningEnabled)
		{
			// TODO: Move these values to a config or something
			Real fXCentre = 631.0/1280.0;
			Real fYCentre = 521.0/1024.0;
			Real fXDeadZone = 14.0/1280.0;
			Real fYDeadZone = 14.0/1024.0;

			const Ogre::RenderWindow* pWindow =
				OgreFramework::GetInstance().GetRenderWindow();

			Real fXProp = m_pGUI->GetMouseX() / pWindow->getWidth();
			fXProp -= fXCentre;
			if ( Ogre::Math::Abs(fXProp) <= fXDeadZone ) fXProp = 0.0;
			fXProp *= 5; // TODO: Another thing to put in a config
			if (fXProp > 1) fXProp = 1;
			if (fXProp < -1) fXProp = -1;

			Real fYProp = m_pGUI->GetMouseY() / pWindow->getHeight();
			fYProp -= fYCentre;
			if ( Ogre::Math::Abs(fYProp) <= fYDeadZone ) fYProp = 0.0;
			fYProp *= 5;
			if (fYProp > 1) fYProp = 1;
			if (fYProp < -1) fYProp = -1;

			pControls->SetYaw(-fXProp);
			pControls->SetPitch(fYProp);
			pControls->SetRoll( -m_vecKeyMotion.w );
		}
		else
		{
			pControls->SetYaw(0);
			pControls->SetPitch(0);
			pControls->SetRoll(0);
		}

		// CAMERA MOTION
		// The following code "bounces" the camera back to its default position.
		// Work out the angles for X (yaw) springing
		rAngle = m_fChaseCamXVelocity * (Degree)iTime;
		rMaxAngle = m_fChaseCamXFromIdeal;
		// Apply the motion
		vecAxis = Vector3::UNIT_Y;
		qRot.FromAngleAxis((Math::Abs(rAngle) > Math::Abs(rMaxAngle) ? rMaxAngle : rAngle), vecAxis);
		m_vecCamRelativePos = qRot * m_vecCamRelativePos;
		//pCamera->rotate(qRot);
		m_qCamRelativeOrientation = m_qCamRelativeOrientation * qRot;
		m_fChaseCamXFromIdeal -= rAngle;
		// Works kind of like a spring: velocity is proportional to distance from ideal.
		m_fChaseCamXVelocity = pGameConf->GetChaseCamStiffness() * m_fChaseCamXFromIdeal;

		// Do the same around Y (pitch)
//		rAngle = m_fChaseCamYVelocity * (Degree)iTime;
//		rMaxAngle = m_fChaseCamYFromIdeal;
//		// Apply the motion
//		vecAxis = -Vector3::UNIT_Z;
//		qRot.FromAngleAxis((Math::Abs(rAngle) > Math::Abs(rMaxAngle) ? rMaxAngle : rAngle), vecAxis);
//		m_vecCamRelativePos = qRot * m_vecCamRelativePos;
//		//pCamera->rotate(qRot);
//		m_qCamRelativeOrientation = m_qCamRelativeOrientation * qRot;
//		m_fChaseCamYFromIdeal -= rAngle;
//		// Works kind of like a spring: velocity is proportional to distance from ideal.
//		m_fChaseCamYVelocity = pGameConf->GetChaseCamStiffness() * m_fChaseCamYFromIdeal;

	}

	// Do the zoom spring effect
	rDist = m_fCamZoomVel * iTime;
	rMaxDist = m_fCamSetZoom - m_fCamCurrentZoom;
	// Apply the zooming motion
	rDist = (Math::Abs(rDist) > Math::Abs(rMaxDist) ? rMaxDist : rDist);
	m_vecCamRelativePos += m_vecCamRelativePos * rDist;
	m_fCamCurrentZoom += rDist;
	// Works kind of like a spring: velocity is proportional to distance from ideal.
	m_fCamZoomVel = pGameConf->GetChaseCamStiffness() * rMaxDist;

	UpdateCameraPosition(iTime);
}




void BfPInput::UpdateTacticalMode(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	// Do the tutorial trigger update if applicable
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
		UpdateTacticalTutorial(iTime);
	// Update the camera
	UpdateTacticalCamera(iTime);
}

void BfPInput::UpdateTacticalTutorial(unsigned long iTime)
{
	// Get the stuff we need
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert( pScenarioConf->GetEventsEnabled("Tutorial") );

	if (!m_pPlayer->HasShips()) return;
	const ShipState* pPlayer = m_pPlayer->GetFlagShip()->GetState();
	ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	assert(pPlayerShip);
	ControlState* pControls = pPlayerShip->GetControlState();
	assert(pControls);

	// Check for trigger events
	EventHandler& rEvents = GlobalEvents::Get();
	// Throw the ChaseCam tutorial trigger
	rEvents.PushEvent( new EventTrigger("TacCam") );
	// Events for the tutorial
	// Body selected
	const String* pObj = m_pPlayer->GetSelectedObject();
	if (pObj)
	{
		rEvents.PushEvent( new EventTrigger("TacObjSelected") );
	}
	else rEvents.PushEvent( new EventTrigger("TacNoObjSelected") );
	// Sector selected
	const String& rSect = m_pPlayer->GetSelectedSector();
	if (rSect != pPlayer->GetSectorID())
	{
		rEvents.PushEvent( new EventTrigger("TacDiffSector") );
	}
	// Group selected
	const unsigned* pGrpID = m_pPlayer->GetSelectedGroup();
	if (pGrpID)
	{
		rEvents.PushEvent( new EventTrigger("TacGroupSelected") );
	}

	// Events for the state of the player's sector
	const BfPSectorState* pSector = pPlayerShip->GetSector();
	const String* pOwner = pSector->GetOwner();
	if (pOwner)
	{
		if (*pOwner == m_pPlayer->GetFactionName())
			rEvents.PushEvent( new EventTrigger("TacPlayerSectorFriendly") );
		else
			rEvents.PushEvent( new EventTrigger("TacPlayerSectorHostile") );
	}
	else rEvents.PushEvent( new EventTrigger("TacPlayerSectorNeutral") );

}

void BfPInput::UpdateTacticalCamera(unsigned long iTime)
{
	static Quaternion qRot;
	static Real rDist;
	static Real rMaxDist;

	const GeneralConfig* pGameConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	Ogre::Camera* pCamera = m_pOgre->GetCamera();
	//const ShipState* pPlayer = m_pPlayer->GetFlagShip()->GetState();

	// First check for panning with the mouse.
	UpdateCameraScrolling(iTime);

	if (m_bShift)
	{
		// NOTE: Put stuff here if we decide to have a shift function for the tactical screen
	}
	else
	{
		// Do mouse pointer movement.
		m_pGUI->MoveMouse(-m_vecMouseRotation.x * pGameConf->GetTacCamSensitivity() * (Real)iTime,
						  -m_vecMouseRotation.y * pGameConf->GetTacCamSensitivity() * (Real)iTime);
	}

	if (m_bJustSwitched)
	{
		//cout << "just switched" << endl;
		m_vecCamRelativePos = Vector3::UNIT_Y * pGameConf->GetTacCamInitialDist();
		m_fCamSetZoom = pGameConf->GetTacCamInitialDist();
		m_fCamCurrentZoom = pGameConf->GetTacCamInitialDist();
		m_fCamZoomVel = 0;

		pCamera->setPosition(m_vecCamRelativePos);
		//pCamera->setDirection(Vector3::NEGATIVE_UNIT_Y);
		qRot.FromAngleAxis(Degree(-90), Vector3::UNIT_X);
		pCamera->setOrientation(qRot);
	}

	// CAMERA ZOOM (mouse wheel takes precedence over keys)
	Real fZoom = (m_iWheelZoom != 0) ? (Real)m_iWheelZoom : (Real)m_iZoom;
	m_fCamSetZoom += fZoom * pGameConf->GetTacCamZoomSpeed();
	rDist = pGameConf->GetTacCamMinDist();
	rMaxDist = pGameConf->GetTacCamMaxDist();
	assert(rDist < rMaxDist);
	if (m_fCamSetZoom < rDist) m_fCamSetZoom = rDist;
	if (m_fCamSetZoom > rMaxDist) m_fCamSetZoom = rMaxDist;


	// Do the zoom spring effect
	rDist = m_fCamZoomVel * iTime;
	rMaxDist = m_fCamSetZoom - m_fCamCurrentZoom;
	// Apply the zooming motion
	rDist = (Math::Abs(rDist) > Math::Abs(rMaxDist) ? rMaxDist : rDist);
	m_vecCamRelativePos += Vector3::UNIT_Y * rDist;
	m_fCamCurrentZoom += rDist;
	// Works kind of like a spring: velocity is proportional to distance from ideal.
	m_fCamZoomVel = pGameConf->GetTacCamZoomStiffness() * rMaxDist;

	// Pan the top-down tactical camera. Key input overrides mouse input.
	Real rTime = (Real)iTime;
	Vector3 vecTranslate;
	if (m_vecKeyMotion.x != 0 || m_vecKeyMotion.z != 0)
		vecTranslate = Vector3(m_vecKeyMotion.x, -m_vecKeyMotion.z, 0);
	else
		vecTranslate = Vector3(m_vecMousePanning.x, -m_vecMousePanning.y, 0);
	vecTranslate = pCamera->getOrientation() * vecTranslate;
	m_vecCamRelativePos += vecTranslate * pGameConf->GetMapCamPanSpeed() * rTime;

	pCamera->setPosition(m_vecCamRelativePos);
}


void BfPInput::UpdateMapMode(unsigned long iTime)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	// Do the tutorial trigger update if applicable
	if (pScenarioConf->GetEventsEnabled("Tutorial"))
		UpdateMapTutorial(iTime);
	// Update the camera
	UpdateMapCamera(iTime);
}

void BfPInput::UpdateMapTutorial(unsigned long iTime)
{
	// Get the stuff we need
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	assert( pScenarioConf->GetEventsEnabled("Tutorial") );

	// Check for trigger events
	EventHandler& rEvents = GlobalEvents::Get();
	// Throw the ChaseCam tutorial trigger
	rEvents.PushEvent( new EventTrigger("MapCam") );

	// Events that based on the ownership of visible sectors
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const BfPSectorState* pSector = m_pPlayer->GetObservedState()->ObserveSector(*it);
		if (!pSector) continue;
		const String* pOwner = pSector->GetOwner();
		if (pOwner)
		{
			if (*pOwner == m_pPlayer->GetFactionName())
				rEvents.PushEvent( new EventTrigger("MapFriendlySector") );
			else
				rEvents.PushEvent( new EventTrigger("MapHostileSector") );
		}
		else rEvents.PushEvent( new EventTrigger("MapNeutralSector") );
	}
}

void BfPInput::UpdateMapCamera(unsigned long iTime)
{
	static Quaternion qRot;

	const GeneralConfig* pGameConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	Ogre::Camera* pCamera = m_pOgre->GetCamera();

	// First check for panning with the mouse.
	UpdateCameraScrolling(iTime);

	if (m_bShift)
	{
		// TODO: Possibly rotate the map here when shift is held
	}
	else
	{
		// Do mouse pointer movement.
		m_pGUI->MoveMouse(-m_vecMouseRotation.x * pGameConf->GetMapCamSensitivity() * (Real)iTime,
						  -m_vecMouseRotation.y * pGameConf->GetMapCamSensitivity() * (Real)iTime);
	}

	if (m_bJustSwitched)
	{
		m_vecCamRelativePos = Vector3::UNIT_Y * pGameConf->GetMapCamInitialDist();
		m_fCamSetZoom = pGameConf->GetMapCamInitialDist();
		m_fCamCurrentZoom = pGameConf->GetMapCamInitialDist();
		m_fCamZoomVel = 0;

		pCamera->setPosition(m_vecCamRelativePos);
		//pCamera->setDirection(Vector3::NEGATIVE_UNIT_Y);
		qRot.FromAngleAxis(Degree(-90), Vector3::UNIT_X);
		pCamera->setOrientation(qRot);
	}

	static Real rDist;
	static Real rMaxDist;

	// CAMERA ZOOM (mouse wheel takes precedence over keys)
	Real fZoom = (m_iWheelZoom != 0) ? (Real)m_iWheelZoom : (Real)m_iZoom;
	m_fCamSetZoom += fZoom * pGameConf->GetMapCamZoomSpeed();
	rDist = pGameConf->GetMapCamMinDist();
	rMaxDist = pGameConf->GetMapCamMaxDist();
	assert(rDist < rMaxDist);
	if (m_fCamSetZoom < rDist) m_fCamSetZoom = rDist;
	if (m_fCamSetZoom > rMaxDist) m_fCamSetZoom = rMaxDist;

	// TODO: Combine tactical and map code somehow (e.g. generic "pan camera"?) to avoid code duplication

	// Do the zoom spring effect
	rDist = m_fCamZoomVel * iTime;
	rMaxDist = m_fCamSetZoom - m_fCamCurrentZoom;
	// Apply the zooming motion
	rDist = (Math::Abs(rDist) > Math::Abs(rMaxDist) ? rMaxDist : rDist);
	m_vecCamRelativePos += Vector3::UNIT_Y * rDist;
	m_fCamCurrentZoom += rDist;
	// Works kind of like a spring: velocity is proportional to distance from ideal.
	m_fCamZoomVel = pGameConf->GetMapCamZoomStiffness() * rMaxDist;

	// Pan the top-down tactical camera. Key input overrides mouse input.
	Real rTime = (Real)iTime;
	Vector3 vecTranslate;
	if (m_vecKeyMotion.x != 0 || m_vecKeyMotion.z != 0)
		vecTranslate = Vector3(m_vecKeyMotion.x, -m_vecKeyMotion.z, 0);
	else
		vecTranslate = Vector3(m_vecMousePanning.x, -m_vecMousePanning.y, 0);
	vecTranslate = pCamera->getOrientation() * vecTranslate;
	m_vecCamRelativePos += vecTranslate * pGameConf->GetMapCamPanSpeed() * rTime;

	pCamera->setPosition(m_vecCamRelativePos);
}


void BfPInput::UpdateCameraScrolling(unsigned long iTime)
{
	const GeneralConfig* pGameConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	assert(m_iCamMode == CM_TACTICAL ||
		   m_iCamMode == CM_MAP ||
		   m_iCamMode == CM_LOGICENGINE);

	// Alter m_vecKeyMotion.x and m_vecKeyMotion.z based on whether the mouse is at the edge of the screen
	const RenderWindow* pWindow = m_pOgre->GetRenderWindow();

	const Real& fMouseX = m_pGUI->GetMouseX();
	Real fWidth = (Real)pWindow->getWidth();
	const Real& fMouseY = m_pGUI->GetMouseY();
	Real fHeight = (Real)pWindow->getHeight();

	if (fMouseX <= pGameConf->GetEdgePanDistance())
		m_vecMousePanning.x = -1;
	else if (fMouseX >= fWidth - pGameConf->GetEdgePanDistance())
		m_vecMousePanning.x = 1;
	else
		m_vecMousePanning.x = 0;

	if (fMouseY <= pGameConf->GetEdgePanDistance())
		m_vecMousePanning.y = -1;
	else if (fMouseY >= fHeight - pGameConf->GetEdgePanDistance())
		m_vecMousePanning.y = 1;
	else
		m_vecMousePanning.y = 0;


}

void BfPInput::UpdateMouseRotation(unsigned long iTime)
{
	const GeneralConfig* pGameConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	static Degree rAngle;
	static Quaternion qRot;

	Real fTime = (Real)iTime;

	// X mouse movement: use the right vector of our camera and the relative position to get the axis
	rAngle = Degree(m_vecMouseRotation.x * pGameConf->GetCamRotation() * fTime);
	m_fChaseCamXFromIdeal -= rAngle;
	while (m_fChaseCamXFromIdeal > Degree(360)) m_fChaseCamXFromIdeal -= Degree(360);
	while (m_fChaseCamXFromIdeal < Degree(-360)) m_fChaseCamXFromIdeal += Degree(360);
	qRot.FromAngleAxis(rAngle, Vector3::UNIT_Y);
	m_vecCamRelativePos = qRot * m_vecCamRelativePos;
	m_qCamRelativeOrientation = qRot * m_qCamRelativeOrientation;

	if (m_oShiftTurned.valueDegrees() < 720)
	{
		m_oShiftTurned += Degree( Ogre::Math::Abs( rAngle.valueDegrees() ) );
		if (m_oShiftTurned.valueDegrees() >= 720)
		{
			//cout << "DONE" << endl;
			EventHandler& rEvents = GlobalEvents::Get();
			rEvents.PushEvent( new EventTrigger("ChaseShiftTurned") );
		}
	}

	// Y mouse movement: use the normal of our camera and the relative position to get the axis
//	rAngle = Degree(m_vecMouseRotation.y * pGameConf->GetCamRotation() * rTime);
//	m_fChaseCamYFromIdeal -= rAngle;
//	while (m_fChaseCamYFromIdeal > Degree(360)) m_fChaseCamYFromIdeal -= Degree(360);
//	while (m_fChaseCamYFromIdeal < Degree(-360)) m_fChaseCamYFromIdeal += Degree(360);
//	qRot.FromAngleAxis(rAngle,  -Vector3::UNIT_Z);
//	m_vecCamRelativePos = qRot * m_vecCamRelativePos;
//	m_qCamRelativeOrientation =  qRot * m_qCamRelativeOrientation;

//	std::stringstream sout;
//	sout << "xfromideal: " << m_fChaseCamXFromIdeal.valueDegrees();
//	m_pOgre->GetLog()->logMessage(sout.str());

}

void BfPInput::UpdateCameraPosition(unsigned long iTime)
{
	static Vector3 vecAbsPos;

	// Set the camera to the new position relative to the player ship
	Ogre::Camera* pCamera = m_pOgre->GetCamera();
	assert(m_pPlayer->HasShips());
	const ShipState* pState = m_pPlayer->GetFlagShip()->GetState();
	// Translate our chase cam position to absolute coordinates
	vecAbsPos = pState->GetOrientation() *  m_vecCamRelativePos;
	vecAbsPos += pState->GetPosition();
	pCamera->setPosition(vecAbsPos);

	pCamera->setOrientation(pState->GetOrientation() * m_qCamRelativeOrientation * m_qCamOffset);
}
