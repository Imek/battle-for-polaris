#include <UI/BfPGUI.h>

template<> BfPGUI* JFUtil::Singleton<BfPGUI>::s_instance = NULL;

BfPGUI::BfPGUI(CAMERA_MODE iCamMode)
: m_iGUIMode(GM_MENU)
, m_iCamMode(iCamMode)
, m_iTargMode(TM_GROUPS)
, m_bShift(false)
, m_bPause(false)
, m_bJustUnpaused(false)
, m_fTimeFactorBeforePause( Ogre::ControllerManager::getSingleton().getTimeFactor() )
, m_bQuit(false)
, m_bBeginGame(false)
, m_bEndGame(false)
, m_fMouseX(OgreFramework::GetInstance().GetRenderWindow()->getWidth() / 2)
, m_fMouseY(OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 2)
, m_pPlayer(NULL)
, m_strSelectedShipType("")
, m_iNoToBuy(1)
, m_bPlusMinusClicked(false)
, m_strSelectedFaction("")
, m_iSelectedTick(0)
, m_iLastNoTicks(0)
// Initialise CEGUI-specific fields
#ifndef BFP_NO_CEGUI
, m_pMenuWindow(NULL)
, m_pHUDWindow(NULL)
, m_pTacticalWindow(NULL)
, m_pLogicEngineWindow(NULL)
#endif // BFP_NO_CEGUI
{
	// Initialise the data for the abstract state interface
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rFactions = pScenarioConf->GetFactionNames();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		m_mSelectedUnits[*it] = NULL;
		m_mFactionUnits[*it] = std::vector<unsigned>();
	}
}

BfPGUI::~BfPGUI()
{
	// We only destroy the GUI when quitting, so CEGUI should handle the windows and such.
	for (std::map<String, unsigned*>::iterator it = m_mSelectedUnits.begin();
		 it != m_mSelectedUnits.end(); ++it)
	{
		delete it->second;
	}
	m_mSelectedUnits.clear();
}

void BfPGUI::SetPlayer(FactionController* pPlayer)
{
	assert(!m_pPlayer);
	m_pPlayer = pPlayer;
	m_strSelectedFaction = pPlayer->GetFactionName();
}

void BfPGUI::UnsetPlayer()
{
	assert(m_pPlayer);
	m_pPlayer = NULL;
}

void BfPGUI::Init()
{
#ifndef BFP_NO_CEGUI
	// Set up the CEGUI interface with OGRE.
	CEGUI::OgreRenderer::bootstrapSystem();
	CEGUI::System& rSystem = CEGUI::System::getSingleton();
	CEGUI::GUIContext& rGUIContext = rSystem.getDefaultGUIContext();
	CEGUI::MouseCursor& rCursor = rGUIContext.getMouseCursor();
	CEGUI::String cursorImage = CEGUI::String(
		m_iCamMode == CM_CHASE ? "TaharezLook/Cursor" : "TaharezLook/MouseArrow");

	CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");

	// XXX: InitMode already seems to handle this updating & showing of the image, so why do it redundantly here?
	rCursor.setDefaultImage(cursorImage);
	rCursor.show();

#else

	// No CEGUI, so auto-start with a default event.
	const GeneralConfig* pConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	const String& rDefaultScenario = pConf->GetDefaultScenario();
	((BfPConfig*)(GameConfig::GetInstancePtr()))->SetScenario(rDefaultScenario);
	m_bBeginGame = true;

#endif // BFP_NO_CEGUI
	// Set up the new mode & subscribe the events
	InitMode();
};



void BfPGUI::SetMode(CAMERA_MODE iMode)
{
	if (!ShowingGame()) return;

	// Destroy the window
	DestroyMode();

	m_iCamMode = iMode;

	// Set up the new mode & subscribe events
	InitMode();
}

void BfPGUI::ResetMouse()
{
	const Ogre::RenderWindow* pWindow =
		OgreFramework::GetInstance().GetRenderWindow();
	Real fWidth = pWindow->getWidth();
	Real fHeight = pWindow->getHeight();

	m_fMouseX = fWidth/2;
	m_fMouseY = fHeight/2;
}

void BfPGUI::MoveMouse(Real fX, Real fY)
{
	const Ogre::RenderWindow* pWindow =
		OgreFramework::GetInstance().GetRenderWindow();
	m_fMouseX += fX; m_fMouseY += fY;
	Real fWidth = pWindow->getWidth();
	Real fHeight = pWindow->getHeight();

	if (m_fMouseX > fWidth) m_fMouseX = fWidth;
	else if (m_fMouseX < 0) m_fMouseX = 0;

	if (m_fMouseY > fHeight) m_fMouseY = fHeight;
	else if (m_fMouseY < 0) m_fMouseY = 0;
}

void BfPGUI::InitMode()
{

#ifndef BFP_NO_CEGUI

	CEGUI::WindowManager& rManager = CEGUI::WindowManager::getSingleton();
	CEGUI::System& rSystem = CEGUI::System::getSingleton();
	CEGUI::GUIContext& rGUIContext = rSystem.getDefaultGUIContext();
	CEGUI::MouseCursor& rCursor =  rGUIContext.getMouseCursor();

	if (m_iGUIMode == GM_MENU || m_bPause)
	{
		// Make our menu window the root.
		m_pMenuWindow = rManager.loadLayoutFromFile("Main_menu.layout");
		rGUIContext.setRootWindow(m_pMenuWindow);
		rCursor.setDefaultImage("TaharezLook/MouseArrow");
		rCursor.show();

		// Stupid CEGUI editor won't let me set it to visible, so I have to do it here
		// TODO: It's a bit shit now that we can't look up elements directly by ID -
		// this code will break if the window layout is changed. Needs a refactor.
		m_pMenuWindow->getChild("TitleBar/SkirmishMenu")->setVisible(false);

		// Register triggers for buttons
		CEGUI::ButtonBase* pSkirmishButton = (CEGUI::ButtonBase*)m_pMenuWindow->getChild("TitleBar/SkirmishButton");
		pSkirmishButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
			CEGUI::Event::Subscriber(&CEGUISkirmishButtonClicked));

		CEGUI::ButtonBase* pQuitButton = (CEGUI::ButtonBase*)m_pMenuWindow->getChild("TitleBar/QuitButton");
		pQuitButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
			CEGUI::Event::Subscriber(&CEGUIQuitButtonClicked));
	}
	else if (ShowingGame())
	{
		switch (m_iCamMode)
		{
			case CM_FREELOOK:
			{
				rCursor.hide();
				break;
			}
			case CM_CHASE:
			{
				m_pHUDWindow = rManager.loadLayoutFromFile("hud.layout");
				// Update the selected tab to match the current target mode.
				CEGUI::TabControl* pMenu = (CEGUI::TabControl*)m_pHUDWindow->getChild("TargetMenu");
				//if (m_iTargMode == TM_SECTORS) m_iTargMode = TM_GROUPS;
				pMenu->setSelectedTab( GetActiveTargetTabName() );
				rGUIContext.setRootWindow(m_pHUDWindow);
				// Mouse pointer is a cursor for the crosshair
				rCursor.setDefaultImage("TaharezLook/Cursor");
				rCursor.show();
				break;
			}
			case CM_LOGICENGINE:
			{
				// Load the logic engine debugger window.
				m_pLogicEngineWindow = rManager.loadLayoutFromFile("logicengine.layout");
				rGUIContext.setRootWindow(m_pLogicEngineWindow);
				rCursor.show();
				// TODO: Update selected tab for logic engine interface

				// TODO: Subscribe any mouse events for logic engine interface here
				CEGUI::Listbox* pFactionMenu = (CEGUI::Listbox*)m_pLogicEngineWindow->getChild("UnitSelectMenu/Factions/List");
				pFactionMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIFactionMenuClicked));

				CEGUI::Listbox* pUnitMenu = (CEGUI::Listbox*)m_pLogicEngineWindow->getChild("UnitSelectMenu/Units/List");
				pUnitMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIUnitMenuClicked));

				// Mouse pointer is an arrow
				rCursor.setDefaultImage("TaharezLook/MouseArrow");

				break;
			}
			case CM_MAP:
				// Same as tactical
			case CM_TACTICAL:
			{
				m_pTacticalWindow = rManager.loadLayoutFromFile("tactical.layout");
				// Update the selected tab to match the current target mode.
				CEGUI::TabControl* pMenu = (CEGUI::TabControl*)m_pTacticalWindow->getChild("TargetMenu");
				pMenu->setSelectedTab( GetActiveTargetTabName() );
				rGUIContext.setRootWindow(m_pTacticalWindow);
				rCursor.show();
				// CEGUI likes to segfault for no reason, which is why we have to subscribe events here in this hacky fashion
				CEGUI::Listbox* pPurchaseMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("BuildMenu/PurchaseTab/List");
				pPurchaseMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIPurchaseMenuClicked));

				CEGUI::Listbox* pDeployMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("BuildMenu/DeployTab/List");
				pDeployMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIDeployMenuClicked));

				CEGUI::Listbox* pSectorMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("TargetMenu/Sectors/List");
				pSectorMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUISectorMenuClicked));
				CEGUI::Listbox* pGroupMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("TargetMenu/Groups/List");

				pGroupMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIGroupMenuClicked));

				CEGUI::Listbox* pEnemyMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("TargetMenu/Enemies/List");
				pEnemyMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIEnemyMenuClicked));

				CEGUI::Listbox* pBodyMenu = (CEGUI::Listbox*)m_pTacticalWindow->getChild("TargetMenu/Bodies/List");
				pBodyMenu->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIBodyMenuClicked));

				CEGUI::ButtonBase* pClearButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("ClearButton");
				pClearButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIClearButtonClicked));

				CEGUI::ButtonBase* pStanceButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("StanceButton");
				pStanceButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIStanceButtonClicked));

				CEGUI::ButtonBase* pMoveButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("MoveButton");
				pMoveButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIMoveButtonClicked));

				CEGUI::ButtonBase* pExecuteButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("ExecuteButton");
				pExecuteButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIExecuteButtonClicked));

				CEGUI::ButtonBase* pIncButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("borderTL/IncButton");
				pIncButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIPlusButtonClicked));

				CEGUI::ButtonBase* pDecButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("borderTL/DecButton");
				pDecButton->subscribeEvent(CEGUI::Listbox::EventMouseClick,
					CEGUI::Event::Subscriber(&CEGUIMinusButtonClicked));

				// Mouse pointer is an arrow
				rCursor.setDefaultImage("TaharezLook/MouseArrow");

				break;
			}
			default:
				break;
		}
	}

#endif // BFP_NO_CEGUI
}

void BfPGUI::DestroyMode()
{
#ifndef BFP_NO_CEGUI
	CEGUI::WindowManager& rManager = CEGUI::WindowManager::getSingleton();

	switch (m_iCamMode)
	{
		case CM_CHASE:
			rManager.destroyWindow(m_pHUDWindow);
			m_pHUDWindow = NULL;
			break;
		case CM_LOGICENGINE:
			rManager.destroyWindow(m_pLogicEngineWindow);
			m_pLogicEngineWindow = NULL;
			break;
		case CM_MAP:
			// Same as tactical
		case CM_TACTICAL:
			rManager.destroyWindow(m_pTacticalWindow);
			m_pTacticalWindow = NULL;
			break;
		default:
			break;
	}
#endif // BFP_NO_CEGUI
}

void BfPGUI::ShowPauseDialogue()
{
	// Pause key gets rid of the current message box if visible, otherwise pauses.
	if (DialogueVisible())
		HideDialogue();
	else
		ShowMessageBox("Paused", "Game Paused", false);
}

void BfPGUI::QuitToMenu()
{
	if (m_iGUIMode == GM_MENU) return;
	if (DialogueVisible()) HideDialogue();
	ShowConfirmBox("Quit the game and return to the menu?", "Confirm", true);
}

void BfPGUI::ToggleTargetMode(bool bPrev)
{
	if (m_iGUIMode == GM_MENU) return;
	if (m_iCamMode == CM_FREELOOK) return;

	assert(m_iCamMode != CM_LAST);
	if (!bPrev && m_iTargMode == 0)
		m_iTargMode = (TARGET_MODE) (TM_LAST-1);
	else
		m_iTargMode = (TARGET_MODE)((m_iTargMode + (bPrev ? 1 : -1)) % TM_LAST);

	assert(m_iTargMode != TM_LAST);

	String strNewTab = GetActiveTargetTabName();

#ifndef BFP_NO_CEGUI
	CEGUI::TabControl* pMenu = (CEGUI::TabControl*)m_pHUDWindow->getChild("TargetMenu");
	pMenu->setSelectedTab(strNewTab);
#endif

}

void BfPGUI::SetTargetMode(TARGET_MODE iMode)
{
	if (m_iGUIMode == GM_MENU) return;
	if (m_iCamMode == CM_FREELOOK) return;

	assert(m_iCamMode != CM_LAST);
	m_iTargMode = iMode;
	assert(m_iTargMode != TM_LAST);

	String strNewTab = GetActiveTargetTabName();

#ifndef BFP_NO_CEGUI
	CEGUI::TabControl* pMenu = (CEGUI::TabControl*)m_pHUDWindow->getChild("TargetMenu");
	pMenu->setSelectedTab(strNewTab);
#endif
}

void BfPGUI::SwitchTarget(bool bPrev)
{
	if (m_iGUIMode == GM_MENU) return;
	if (m_iCamMode == CM_FREELOOK) return;
	assert(m_iCamMode != CM_LAST);
	assert(m_iTargMode != TM_LAST);

	switch(m_iTargMode)
	{
	case TM_SECTORS:
	{
		if (m_iCamMode == CM_TACTICAL || m_iCamMode == CM_MAP)
			SwitchTargetSector(bPrev);
		else if (m_iCamMode == CM_CHASE)
			SwitchWarpTarget(bPrev);
		break;
	}
	case TM_GROUPS:
		SwitchTargetGroup(bPrev);
		break;
	case TM_ENEMIES:
		SwitchTargetEnemy(bPrev);
		break;
	case TM_BODIES:
		SwitchTargetBody(bPrev);
		break;
	default:
		break;
	}

}

void BfPGUI::TargetClosestEnemy()
{
	if (m_mEnemies.empty()) return;
	SetTargetMode(TM_ENEMIES);

	// Find the enemy that is our current target
	ShipController* pPlayer = m_pPlayer->GetFlagShip();

	const ShipState* pShip = m_mEnemies.begin()->second;
	pPlayer->SetTargetShip( pShip->GetID() );
}

void BfPGUI::TargetNextEnemy(bool bPrev)
{
	// Only do it if we actually have enemies
	if (m_mEnemies.empty()) return;
	SetTargetMode(TM_ENEMIES);
	SwitchTargetEnemy(bPrev);
}

void BfPGUI::Reset(CAMERA_MODE iNewMode)
{
	// Destroy whatever the currently-active window is.
	DestroyMode();

	// Reset various values (except the CEGUI stuff managed) to false
	m_iGUIMode = GM_MENU;
	m_iCamMode = iNewMode;
	m_iTargMode = TM_GROUPS;
	m_bShift = false;
	SetPaused(false);
	m_fTimeFactorBeforePause = Ogre::ControllerManager::getSingleton().getTimeFactor();
	m_bQuit = false;
	m_bBeginGame = false;
	m_bEndGame = false;
	m_fMouseX = OgreFramework::GetInstance().GetRenderWindow()->getWidth() / 2;
	m_fMouseY = OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 2;
	m_pPlayer = NULL; // Assume that the caller deals with destroying it or whatever.
	m_strSelectedShipType = "";
	m_iNoToBuy = 1;
	m_bPlusMinusClicked = false;
	m_strSelectedFaction = "";
	m_iSelectedTick = 0;
	m_iLastNoTicks = 0;
	m_lMessages.clear();

	InitMode(); // Initialises the menu
}

void BfPGUI::Update(unsigned long iTime)
{
	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();

	if (ShowingGame())
	{
		if (m_iCamMode == CM_CHASE)
			UpdateChaseHUD();
		else if (m_iCamMode == CM_TACTICAL || m_iCamMode == CM_MAP)
			UpdateTacticalHUD();
		else if (m_iCamMode == CM_LOGICENGINE)
			UpdateLogicEngineHUD();

		UpdateMessages();
		UpdateMessageBoxes();
	}
	else
	{
#ifndef BFP_NO_CEGUI
		rGUIContext.injectMousePosition(m_fMouseX, m_fMouseY);
		// Inject time into CEGUI - don't think we need this for our purposes.
		CEGUI::System::getSingleton().injectTimePulse(iTime);
#endif // BFP_NO_CEGUI
	}

	// See if we switched to game mode (from the menu)
	if (m_bBeginGame)
	{
#ifndef BFP_NO_CEGUI
		CEGUI::WindowManager& rMgr = CEGUI::WindowManager::getSingleton();
		rMgr.destroyWindow(m_pMenuWindow);
		m_pMenuWindow = NULL;
#endif // BFP_NO_CEGUI

		m_iGUIMode = GM_GAME;
		InitMode();
		m_bBeginGame = false;
	}
	// Update cursor if just unpaused
	// XXX: This is just a stopgap solution to a crash when messing with the cursor in a callback (!)
	// TODO: Refactor to centralise mouse cursor/visibility logic behaviour in one place.
	if (m_bJustUnpaused)
	{
		if (m_iCamMode == CM_CHASE && m_iGUIMode != GM_MENU && !DialogueVisible())
			rGUIContext.getMouseCursor().setDefaultImage("TaharezLook/Cursor");
		else
			rGUIContext.getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

		if (m_iCamMode == CM_FREELOOK)
			rGUIContext.getMouseCursor().hide();
		else
			rGUIContext.getMouseCursor().show();
		m_bJustUnpaused = false;
	}

}

void BfPGUI::UnselectTarget(TARGET_MODE iMode)
{
	bool bChaseCam = m_iCamMode == CM_CHASE;
	if (bChaseCam) assert(m_pPlayer->HasShips());
	assert(m_iTargMode != TM_LAST);
	switch (m_iTargMode)
	{
	case TM_BODIES:
		if (bChaseCam)
			m_pPlayer->GetFlagShipGroup()->UnsetDestLocation();
		else
			m_pPlayer->UnsetSelectedObject();
		break;
	case TM_ENEMIES:
		if (bChaseCam)
			m_pPlayer->GetFlagShip()->UnsetTargetShip();
		break;
	case TM_GROUPS:
		m_pPlayer->UnsetSelectedGroup();
		break;
	case TM_SECTORS:
	{
		if (bChaseCam)
			// NOTE: Need to unset dest point too here?
			m_pPlayer->GetFlagShipGroup()->UnsetDestLocation();
		else
			m_pPlayer->UnsetSelectedSector();
		break;
	}
	default:
		assert(false); // This can't happen
	}
}

void BfPGUI::UnselectTarget()
{
	UnselectTarget(m_iTargMode);
}

void BfPGUI::ClearTargets()
{
	// TODO: Check chasecam here
	m_pPlayer->UnsetSelectedSector();
	m_pPlayer->UnsetSelectedGroup();
	m_pPlayer->UnsetSelectedObject();

	if (m_pPlayer->HasShips())
	{
		m_pPlayer->GetFlagShip()->UnsetTargetShip();
		m_pPlayer->GetFlagShipGroup()->UnsetDestLocation();
	}

}

bool BfPGUI::HasTargets()
{
	const String& rSector = m_pPlayer->GetSelectedSector();
	const BfPSectorState* pPlayerSector = m_pPlayer->HasShips() ?
		m_pPlayer->GetFlagShip()->GetSector() : NULL;
	if (pPlayerSector && rSector != pPlayerSector->GetSectorType()->GetName()) return true;

	// Check other target types
	return (m_pPlayer->GetSelectedGroup() ||
			(m_pPlayer->HasShips() && m_pPlayer->GetFlagShip()->HasTargetShip()) ||
			m_pPlayer->GetSelectedObject());
}

void BfPGUI::ToggleGroupStance(bool bPrev)
{
	if (!m_pPlayer->HasShips() || !m_pPlayer->GetSelectedGroup()) return;

	const AIConfig* pAIConf = (const AIConfig*)
		GameConfig::GetInstance().GetConfig("AI");

	// Set the group's stance to the next in the config.
	GroupController* pGroup = m_pPlayer->GetGroupWithID(*m_pPlayer->GetSelectedGroup());
	const StringVector& rStances = pAIConf->GetShipGroupStances();
	assert(!rStances.empty());
	StringVector::const_iterator it = find(
		rStances.begin(), rStances.end(), pGroup->GetAIStance());
	assert(it != rStances.end());

	// Toggle the stance depending on bPrev
	if (bPrev) ++it;
	else if (it == rStances.begin()) it = rStances.end() - 1;
	else --it;

	if (it == rStances.end()) it = rStances.begin();
	//cout << "new stance: " << *it << endl;
	pGroup->SetAIStance(*it);
}

bool BfPGUI::MousePressed(OIS::MouseButtonID id)
{

#ifndef BFP_NO_CEGUI
	CEGUI::MouseButton cid = GetCEGUIMouseButton(id);
	return CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(cid);
#else
	return false;
#endif // BFP_NO_CEGUI

}

bool BfPGUI::MouseReleased(OIS::MouseButtonID id)
{

#ifndef BFP_NO_CEGUI
	CEGUI::MouseButton cid = GetCEGUIMouseButton(id);
	return CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(cid);
#else
	return false;
#endif

}

// CEGUI-specific function implementations

#ifndef BFP_NO_CEGUI

CEGUI::MouseButton BfPGUI::GetCEGUIMouseButton(OIS::MouseButtonID id)
{
	// TODO: More general? E.g. thumb buttons support would be nice.
	switch(id)
	{
		case OIS::MB_Left:
		{
			return CEGUI::LeftButton;
		}
		case OIS::MB_Right:
		{
			return CEGUI::RightButton;
		}
		case OIS::MB_Middle:
		{
			return CEGUI::MiddleButton;
		}
		default:
		{
			return CEGUI::LeftButton;
		}
	}
}

size_t* BfPGUI::GetItemClicked(CEGUI::Listbox* pList, const CEGUI::Vector2f& rRelativeTo)
{
	size_t iNoItems = pList->getItemCount();
	if (iNoItems < 1) return NULL;

	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::Vector2f vecPos = rGUIContext.getMouseCursor().getPosition();
	// NOTE: Got this working with a really ugly hack for the time being
	vecPos -= rRelativeTo;
	// We know, as the list was clicked, it's within the x boundary. See what item it clicked by Y.
	Real fItemHeight = pList->getTotalItemsHeight();
	Real fClickedHeight = vecPos.d_y;
	if (fClickedHeight > fItemHeight) return NULL;
	fItemHeight /= (Real)iNoItems;

	size_t* pItem = new size_t ( (size_t)(fClickedHeight / fItemHeight) );
	//cout << "item: " << *pItem << endl;
	return pItem;
}

bool BfPGUI::CEGUIPurchaseMenuClicked(const CEGUI::EventArgs& e)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);

	// Nothing to choose (but we need to return as they may have clicked the "no money" label)
	if (rGUI.m_mPurchaseList.empty()) return true;

	//cout << "clicky purchase" << endl;
	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.m_pTacticalWindow->getChild("BuildMenu/PurchaseTab/List");

	// Hackish stuff here thanks to CEGUI being stupid; relies on my GUI layout not changing
	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 66.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem)
	{
		rGUI.m_strSelectedShipType = "";
		return true;
	}
	size_t iItem = *pItem;
	delete pItem;

	size_t i = 0;
	bool bFound = false;
	for (PurchaseMap::const_iterator it = rGUI.m_mPurchaseList.begin();
		 it != rGUI.m_mPurchaseList.end(); ++it)
	{
		if (iItem == i)
		{
			assert(!bFound);
			bFound = true;
			const String& rItem = it->second;
			// Highlight the selected ship type only if we can afford it!
			const ShipType* pType = pShipsConf->GetShipType(rItem);
			assert(pType);
			unsigned iMyRPs = rGUI.m_pPlayer->GetFactionState()->GetRPs();
			unsigned iCost = pType->GetRPCost();
			// See what number we have selected in the "number to buy" box.
			if (rGUI.m_iNoToBuy > 0) iCost *= rGUI.m_iNoToBuy;
			if (iCost <= iMyRPs)
			{
				rGUI.m_strSelectedShipType = rItem;
			}
			break;
		}
		++i;
	}
	//assert(bFound);

	return true;
}

bool BfPGUI::CEGUIDeployMenuClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);

	// Nothing to choose (but we need to return as they may have clicked the label)
	if (rGUI.m_vDeployList.empty()) return true;

//	const ShipsConfig* pShipsConf = (const ShipsConfig*)
//		BfPConfig::GetInstance().GetConfig("Ships");

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.m_pTacticalWindow->getChild("BuildMenu/DeployTab/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 66.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem)
	{
		rGUI.m_strSelectedShipType = "";
		return true;
	}

	assert(*pItem < rGUI.m_vDeployList.size());
	const String& rItem = rGUI.m_vDeployList.at(*pItem);
	rGUI.m_strSelectedShipType = rItem;

	delete pItem;
	return true;
}

bool BfPGUI::CEGUISectorMenuClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);
	// Check both emptiness and disabled. ness.
	bool bEnabled = rGUI.m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTSECTOR);
	if (!bEnabled || rGUI.m_vSectors.empty()) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.GetTargetMenu()->getChild("Sectors/List");

	// XXX: This UI scaling stuff seems dodgy. Need a better system?
	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 450.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem)
	{
		return true;
	}

	assert(*pItem < rGUI.m_vSectors.size());
	const String& rItem = rGUI.m_vSectors[*pItem];
	rGUI.m_pPlayer->SetSelectedSector( rItem );

	delete pItem;
	return true;
}

bool BfPGUI::CEGUIGroupMenuClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);
	// Check both emptiness and disabled. ness.
	bool bEnabled = rGUI.m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTGROUP);
	if (!bEnabled || rGUI.m_mGroups.empty() || !rGUI.m_pPlayer->HasShips()) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.GetTargetMenu()->getChild("Groups/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 450.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem) rGUI.m_pPlayer->UnsetSelectedGroup();
	else
	{
		// We need to look at the group and then at the specific ship (as pItem refers to a ship index)
		GroupMap::const_iterator itGroup = rGUI.m_mGroups.begin();
		if (itGroup == rGUI.m_mGroups.end())
		{
			delete pItem;
			return true;
		}
		for (size_t i = 0; i < *pItem; ++i)
			++itGroup;
		assert(itGroup != rGUI.m_mGroups.end());
		rGUI.m_pPlayer->SetSelectedGroup( itGroup->first, true );

		delete pItem;
	}

	return true;
}

bool BfPGUI::CEGUIEnemyMenuClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);
	if (rGUI.m_mEnemies.empty()) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.GetTargetMenu()->getChild("Enemies/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 450.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem) rGUI.m_pPlayer->GetFlagShip()->UnsetTargetShip();
	else
	{
		assert(*pItem < rGUI.m_mEnemies.size());
		ShipMap::const_iterator it = rGUI.m_mEnemies.begin();
		for (size_t i = 0; i < *pItem; ++i) ++it;
		rGUI.m_pPlayer->GetFlagShip()->SetTargetShip( it->second->GetID() );

		delete pItem;
	}

	return true;
}

bool BfPGUI::CEGUIBodyMenuClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_MAP || rGUI.m_iCamMode == CM_TACTICAL);
	// Check both emptiness and disabled. ness.
	bool bEnabled = rGUI.m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTBODY);
	if (rGUI.m_mBodies.empty() || !bEnabled) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)rGUI.GetTargetMenu()->getChild("Bodies/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 450.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem) rGUI.m_pPlayer->UnsetSelectedObject();
	else
	{
		assert(*pItem < rGUI.m_mBodies.size());
		BodyMap::const_iterator it = rGUI.m_mBodies.begin();
		for (size_t i = 0; i < *pItem; ++i) ++it;
		rGUI.m_pPlayer->SetSelectedObject( it->second->GetID() );

		delete pItem;
	}


	return true;
}

bool BfPGUI::CEGUIFactionMenuClicked(const CEGUI::EventArgs& r)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_LOGICENGINE);
	if (rGUI.m_mBodies.empty()) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)
		rGUI.m_pTacticalWindow->getChild("UnitSelectMenu/Factions/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 430.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	if (!pItem) return true;

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const StringVector& rFactions = pScenarioConf->GetFactionNames();

	assert(*pItem < rFactions.size());
	const String& rNewFaction = rFactions[*pItem];
	rGUI.m_strSelectedFaction = rNewFaction;

	delete pItem;
	return true;
}

bool BfPGUI::CEGUIUnitMenuClicked(const CEGUI::EventArgs& r)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	assert(rGUI.m_iCamMode == CM_LOGICENGINE);
	if (rGUI.m_mBodies.empty()) return true;

	// See where the mouse is, and determine if we clicked an item in the list.
	CEGUI::Listbox* pList = (CEGUI::Listbox*)
		rGUI.m_pLogicEngineWindow->getChild("UnitSelectMenu/Units/List");

	Real fYScale = (Real)OgreFramework::GetInstance().GetRenderWindow()->getHeight() / 768.0;
	CEGUI::Vector2f vecWindowPos = CEGUI::Vector2f(0.0, 430.0*fYScale);
	size_t* pItem = GetItemClicked(pList, vecWindowPos);
	//if (!pItem) return true;

	//cout << "doing this: " << *pItem << endl;
	unsigned* pSelected = rGUI.m_mSelectedUnits[rGUI.m_strSelectedFaction];
	const std::vector<unsigned>& vUnits = rGUI.m_mFactionUnits[rGUI.m_strSelectedFaction];
	assert(!pItem || *pItem < vUnits.size());
	if (!pItem)
	{
		delete rGUI.m_mSelectedUnits[rGUI.m_strSelectedFaction];
		rGUI.m_mSelectedUnits[rGUI.m_strSelectedFaction] = NULL;
	}
	else if (!pSelected) rGUI.m_mSelectedUnits[rGUI.m_strSelectedFaction] = new size_t(vUnits[*pItem]);
	else *pSelected = vUnits[*pItem];

	delete pItem;
	return true;
}

bool BfPGUI::CEGUIClearButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	rGUI.ClearTargets();
	return true;
}

bool BfPGUI::CEGUIStanceButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	rGUI.ToggleGroupStance();
	return true;
}

bool BfPGUI::CEGUIMoveButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	FactionController* pPlayer = rGUI.m_pPlayer;
	// Don't do it if disabled
	bool bEnabled =  pPlayer->GetObservedState()->CheckPlayerInputFlag(MOVEGROUP);
	if (bEnabled) pPlayer->MoveGroup();
	return true;
}

bool BfPGUI::CEGUIExecuteButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	FactionController* pPlayer = rGUI.m_pPlayer;
	// Make up the ship group, which we will be using either way
	StringVector vGroup;
	unsigned iCount = (rGUI.m_iNoToBuy == 0 ? 1 : rGUI.m_iNoToBuy);
	assert(rGUI.m_strSelectedShipType != "");
	for (unsigned i=0;i<iCount;++i)
		vGroup.push_back(rGUI.m_strSelectedShipType);
	assert(iCount == vGroup.size()); // I may be crazy...
	// Which order type we execute depends on what tab is visible
	String strActiveTab = rGUI.GetActiveBuildTabName();
	// Of course we should only actually do it if enabled
	bool bPurchaseEnabled = pPlayer->GetObservedState()->CheckPlayerInputFlag(PURCHASE);
	bool bDeployEnabled = pPlayer->GetObservedState()->CheckPlayerInputFlag(DEPLOY);
	// Although the button should always be disabled if the proper action is impossible, it seems that if you click
	// at the exact moment it's disabled, we could get here and not be able to execute.
	if (strActiveTab == "Purchase" && bPurchaseEnabled)
	{
		pPlayer->SetPurchaseTypes(vGroup);
		//assert(pPlayer->CanPurchaseGroup());
		if (pPlayer->CanPurchaseGroup()) pPlayer->PurchaseGroup();
	}
	else if (strActiveTab == "Deploy" && bDeployEnabled)
	{
		cout << "Deploy clicked; count: " << iCount << endl;

		pPlayer->SetDeployTypes(vGroup);
		//assert(pPlayer->CanDeployGroup());
		if (pPlayer->CanDeployGroup()) pPlayer->DeployGroup();
	}
	return true;
}

bool BfPGUI::CEGUIPlusButtonClicked(const CEGUI::EventArgs& e)
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");

	BfPGUI& rGUI = BfPGUI::GetInstance();

	assert(!rGUI.m_bPlusMinusClicked);
	rGUI.m_bPlusMinusClicked = true;
	// Increment the number of ships to buy/deploy
	++rGUI.m_iNoToBuy;
	unsigned iMax = pGameConf->GetMaxGroupSize();
	if (rGUI.m_iNoToBuy > iMax) rGUI.m_iNoToBuy = iMax;
	assert(rGUI.m_iNoToBuy > 0);
	return true;
}

bool BfPGUI::CEGUIMinusButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();

	assert(!rGUI.m_bPlusMinusClicked);
	rGUI.m_bPlusMinusClicked = true;
	// Decrement the number of ships to buy/deploy, 1 being minimum
	assert(rGUI.m_iNoToBuy > 0);
	--rGUI.m_iNoToBuy;
	if (rGUI.m_iNoToBuy == 0) rGUI.m_iNoToBuy = 1;
	return true;
}

bool BfPGUI::CEGUISkirmishButtonClicked(const CEGUI::EventArgs& e)
{
	// Set skirmish menu to visible
	BfPGUI& rGUI = BfPGUI::GetInstance();
	CEGUI::WindowManager& rManager = CEGUI::WindowManager::getSingleton();
	CEGUI::Window* pMenu = rGUI.m_pMenuWindow->getChild("TitleBar/SkirmishMenu");
	assert(pMenu);

	// Setting the menu to visible for the first time; add our contents (each possible map config)
	const GeneralConfig* pConf = (const GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	if (pMenu->isVisible()) return true;
	pMenu->setVisible(true);
	pMenu->setAlwaysOnTop(true);
	CEGUI::ItemListbox* pMapList = (CEGUI::ItemListbox*)pMenu->getChild("MapList");
	assert(pMapList->getItemCount() == 0);
	assert(pMapList);

	// Add the items for the possible world configs
	const StringVector& rMaps = pConf->GetScenarioConfigs();
	for (StringVector::const_iterator it = rMaps.begin();
		 it != rMaps.end(); ++it)
	{
		CEGUI::ItemEntry* pItem = (CEGUI::ItemEntry*)rManager.createWindow("TaharezLook/MenuItem");
		pItem->setText(*it);
		pItem->subscribeEvent("Clicked",
			CEGUI::Event::Subscriber(&CEGUISkirmishScenarioConfigClicked));
		pMapList->addItem(pItem);
	}

	return true;
}

bool BfPGUI::CEGUISkirmishScenarioConfigClicked(const CEGUI::EventArgs& e)
{
	BfPGUI& rGUI = BfPGUI::GetInstance();
	CEGUI::Window* pMenu = rGUI.m_pMenuWindow->getChild("TitleBar/SkirmishMenu");
	CEGUI::ItemListbox* pMapList = (CEGUI::ItemListbox*)pMenu->getChild("MapList");
	CEGUI::ItemEntry* pActiveItem = (CEGUI::ItemEntry*)pMapList->getActiveChild();
	// This might happen in windowed mode if we don't have focus.
	if (!pActiveItem) return true;
	String strWorldConf (pActiveItem->getText().c_str());
	((BfPConfig*)(GameConfig::GetInstancePtr()))->SetScenario(strWorldConf);
	rGUI.m_bBeginGame = true;

	return true;
}

bool BfPGUI::CEGUIQuitButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI::GetInstance().m_bQuit = true;
	return true;
}

bool BfPGUI::CEGUIMessageBoxOKButtonClicked(const CEGUI::EventArgs& e)
{
	//assert(BfPGUI::GetInstance().DialogueVisible());
	BfPGUI::GetInstance().HideDialogue();
	return true;
}

bool BfPGUI::CEGUIEndGameButtonClicked(const CEGUI::EventArgs& e)
{
	BfPGUI::GetInstance().m_bEndGame = true;
	return true;
}

#endif // BFP_NO_CEGUI


String BfPGUI::GetActiveTargetTabName()
{
	String strNewTab = "";
	if (m_iTargMode == TM_SECTORS)
		strNewTab = "Sectors";
	else if (m_iTargMode == TM_GROUPS)
		strNewTab = "Groups";
	else if (m_iTargMode == TM_ENEMIES)
		strNewTab = "Enemies";
	else if (m_iTargMode == TM_BODIES)
		strNewTab = "Bodies";
	assert(!strNewTab.empty());
	return strNewTab;
}

String BfPGUI::GetActiveBuildTabName()
{
#ifndef BFP_NO_CEGUI
	CEGUI::Window* tacticalWindow = BfPGUI::GetInstance().m_pTacticalWindow;
	CEGUI::Window* pPurchaseTab = tacticalWindow->getChild("BuildMenu/PurchaseTab");
	CEGUI::Window* pDeployTab = tacticalWindow->getChild("BuildMenu/DeployTab");

	if (pPurchaseTab->isVisible())
		return "Purchase";
	else if (pDeployTab->isVisible())
		return "Deploy";
#endif
	// Just in case we add something..
	return "???";
}

void BfPGUI::SetTargetMode(const String& rMode)
{
	if (rMode == "Sectors")
		m_iTargMode = TM_SECTORS;
	else if (rMode == "Groups")
		m_iTargMode = TM_GROUPS;
	else if (rMode == "Enemies")
		m_iTargMode = TM_ENEMIES;
	else if (rMode == "Bodies")
		m_iTargMode = TM_BODIES;
	else
		assert(false); // Please blow up
}

void BfPGUI::SwitchTargetSector(bool bPrev)
{
	// Find the currently selected sector and switch to the one after that in the list.
	String strOldSelection = m_pPlayer->GetSelectedSector();
	StringVector::iterator itNewSelection =
		find(m_vSectors.begin(), m_vSectors.end(), strOldSelection);
	assert(itNewSelection != m_vSectors.end());
	++itNewSelection;
	if (itNewSelection == m_vSectors.end()) itNewSelection = m_vSectors.begin();
	// If it's our player's sector, just unset (so that it'll follow the player ship if it warps)
	if (m_pPlayer->HasShips() && *itNewSelection ==
		m_pPlayer->GetFlagShip()->GetSector()->GetSectorType()->GetName())
		m_pPlayer->UnsetSelectedSector();
	else
		m_pPlayer->SetSelectedSector(*itNewSelection);

}

void BfPGUI::SwitchWarpTarget(bool bPrev)
{
	// First check if group selection is enabled.
	bool bEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTSECTOR);
	if (!bEnabled) return;
	// Find the currently selected sector and switch to the one after that in the list.
	// If there is no currently selected sector, set it to the first one.
	GroupController* pPlayer = m_pPlayer->GetFlagShipGroup();
	const String* pOldSelection = pPlayer->GetDestSector();
	// We don't have a selection if the "destination sector" is the current one
	bool bNoSelection = !pOldSelection ||
		*pOldSelection == pPlayer->GetSector()->GetSectorType()->GetName();
	StringVector::const_iterator itNewSelection = bNoSelection ?
		m_vLinks.begin() : find(m_vLinks.begin(), m_vLinks.end(), *pOldSelection);
	// Had an invalid selection (because we just warped) or the list was empty
	if (m_vLinks.empty() || itNewSelection == m_vLinks.end())
	{
		pPlayer->UnsetDestLocation();
		return;
	}

	++itNewSelection;
	if (itNewSelection == m_vLinks.end()) itNewSelection = m_vLinks.begin();

	assert(itNewSelection != m_vLinks.end());
	// Finally, set the player ship's destination sector to be the new selection.
	pPlayer->SetDestLocation(*itNewSelection);
}

void BfPGUI::SwitchTargetGroup(bool bPrev)
{
	// First check if group selection is enabled.
	bool bEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTGROUP);
	if (!bEnabled) return;
	// Find the body that is our current target
	if (!m_pPlayer->GetSelectedGroup())
	{
		if (m_mGroups.empty()) return;
		m_pPlayer->SetSelectedGroup( m_mGroups.begin()->first, m_iCamMode != CM_CHASE );
	}
	else
	{
		// Find the currently selected group and switch to the one after that in the list.
		GroupMap::const_iterator itNewSelection =
			m_mGroups.end();
		unsigned iOldSelectedGroup = *m_pPlayer->GetSelectedGroup();
		unsigned iNewSelectedGroup = iOldSelectedGroup;
		for (GroupMap::const_iterator it = m_mGroups.begin();
			 it != m_mGroups.end(); ++it)
		{
			if (it->first == iOldSelectedGroup)
			{
				// Next one or the first one if this was the last
				assert(itNewSelection == m_mGroups.end()); // Must be the only one!
				itNewSelection = it;
				if (bPrev)
				{
					++itNewSelection;
					if (itNewSelection == m_mGroups.end())
						itNewSelection = m_mGroups.begin();
				}
				else if (itNewSelection == m_mGroups.begin())
					itNewSelection = --(m_mGroups.end());
				else
					--itNewSelection;
				// We use a separate variable here so that we can make sure there are no duplicates
				iNewSelectedGroup = itNewSelection->first;
			}
		}
		assert(itNewSelection != m_mGroups.end());
		m_pPlayer->SetSelectedGroup(iNewSelectedGroup, m_iCamMode != CM_CHASE);
	}
}

void BfPGUI::SwitchTargetEnemy(bool bPrev)
{
	// Find the body that is our current target
	ShipController* pPlayer = m_pPlayer->GetFlagShip();
	if (!pPlayer->HasTargetShip())
	{
		if (m_mEnemies.empty()) return;
		const ShipState* pShip = m_mEnemies.begin()->second;
		pPlayer->SetTargetShip( pShip->GetID() );
	}
	else
	{
		// Find the currently selected body and switch to the one after that in the list.
		ShipMap::const_iterator itNewSelection =
			m_mEnemies.end();
		unsigned iOldSelectedShip = pPlayer->GetTargetShip();
		unsigned iNewSelectedShip = iOldSelectedShip;
		for (ShipMap::const_iterator it = m_mEnemies.begin();
			 it != m_mEnemies.end(); ++it)
		{
			const ShipState* pShip = it->second;
			if (pShip->GetID() == iOldSelectedShip)
			{
				// Next one or the first one if this was the last
				assert(itNewSelection == m_mEnemies.end()); // Must be the only one!
				itNewSelection = it;
				if (bPrev)
				{
					++itNewSelection;
					if (itNewSelection == m_mEnemies.end())
						itNewSelection = m_mEnemies.begin();
				}
				else if (itNewSelection == m_mEnemies.begin())
					itNewSelection = --(m_mEnemies.end());
				else
					--itNewSelection;
				const ShipState* pNewShip = itNewSelection->second;
				// We use a separate variable here so that we can make sure there are no duplicates
				iNewSelectedShip = pNewShip->GetID();
			}
		}
		assert(itNewSelection != m_mEnemies.end());
		pPlayer->SetTargetShip(iNewSelectedShip);
	}
}

void BfPGUI::SwitchTargetBody(bool bPrev)
{
	// First, just return if the control is disabled.
	bool bEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(SELECTBODY);
	if (!bEnabled) return;

	// Find the body that is our current target -
	// faction's selected object if it's the tactical view
	// or group's selected object if it's the ship view
	bool bChaseCam = m_iCamMode == CM_CHASE;
	const String* pSelectedBody = bChaseCam ?
		m_pPlayer->GetFlagShipGroup()->GetDestBody() :
		m_pPlayer->GetSelectedObject();
	String strNewSelection;
	if (!pSelectedBody)
	{
		if (m_mBodies.empty()) return;
		const BodyInstance* pBody = m_mBodies.begin()->second;
		strNewSelection = pBody->GetID();
	}
	else
	{
		// Find the currently selected body and switch to the one after that in the list.
		BodyMap::const_iterator itNewSelection =
			m_mBodies.end();
		for (BodyMap::const_iterator it = m_mBodies.begin();
			 it != m_mBodies.end(); ++it)
		{
			const BodyInstance* pBody = it->second;
			if (pBody->GetID() == *pSelectedBody)
			{
				// Next one or the first one if this was the last
				assert(itNewSelection == m_mBodies.end()); // Must be the only one!
				itNewSelection = it;
				if (bPrev)
				{
					++itNewSelection;
					if (itNewSelection == m_mBodies.end())
						itNewSelection = m_mBodies.begin();
				}
				else if (itNewSelection == m_mBodies.begin())
					itNewSelection = --(m_mBodies.end());
				else
					--itNewSelection;
				const BodyInstance* pNewBody = itNewSelection->second;
				// We use a separate variable here so that we can make sure there are no duplicates
				strNewSelection = pNewBody->GetID();
			}
		}
		assert(itNewSelection != m_mBodies.end());
	}
	// TODO: Tidy up the whole selected/active sector system - depends on camera mode, et cetera
	const String& rSector = bChaseCam ?
		m_pPlayer->GetFlagShip()->GetSector()->GetSectorType()->GetName() :
		m_pPlayer->GetSelectedSector();
	if (bChaseCam)
		m_pPlayer->GetFlagShipGroup()->SetDestLocation(rSector, strNewSelection);
	else
		m_pPlayer->SetSelectedObject(strNewSelection);

}

void BfPGUI::ShowMessageBox(const String& rMessage, const String& rTitle, bool bGameOver)
{

#ifndef BFP_NO_CEGUI

	CEGUI::FrameWindow* pFrame = GetMessageBox();
	// Make the mouse a pointer and visible here (e.g. for chase cam mode)
	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::MouseCursor& rCursor = rGUIContext.getMouseCursor();
	rCursor.setDefaultImage("TaharezLook/MouseArrow");
	rCursor.show();

	if (!pFrame) return;
	assert(!pFrame->isVisible());
	pFrame->setText(rTitle);
	CEGUI::Window* pText = pFrame->getChild("Message");
	assert(pText);
	pText->setText(rMessage);

	pFrame->setVisible(true);
	SetPaused(true); // Pause the game while the box is showing

	// Reset movement controls, in case the player is holding down a key
	if (m_pPlayer->HasShips())
	{
		ControlState* pPlayerControls = m_pPlayer->GetFlagShip()->GetControlState();
		bool bZeroThrust = pPlayerControls->m_bZeroThrust; // We want to keep this the same
		pPlayerControls->ResetMovement();
		pPlayerControls->m_bZeroThrust = bZeroThrust;
	}

	// Event callback for button clicked; depends on whether it's a "game over" event.
	CEGUI::ButtonBase* pOKButton = (CEGUI::ButtonBase*)pFrame->getChild("OKButton");
	CEGUI::Event::Subscriber oHandler = CEGUI::Event::Subscriber(
		(bGameOver ? &CEGUIEndGameButtonClicked : &CEGUIMessageBoxOKButtonClicked) );
	pOKButton->subscribeEvent(CEGUI::Listbox::EventMouseClick, oHandler);

#endif // BFP_NO_CEGUI

}

void BfPGUI::ShowConfirmBox(const String& rMessage, const String& rTitle, bool bYesEndsGame)
{

#ifndef BFP_NO_CEGUI

	CEGUI::FrameWindow* pFrame = GetConfirmBox();
	// Make the mouse a pointer and visible here (e.g. for chase cam mode)
	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::MouseCursor& rCursor = rGUIContext.getMouseCursor();
	rCursor.setDefaultImage("TaharezLook/MouseArrow");
	rCursor.show();

	if (!pFrame) return;
	assert(!pFrame->isVisible());
	pFrame->setText(rTitle);
	CEGUI::Window* pText = pFrame->getChild("Message");
	assert(pText);
	pText->setText(rMessage);

	pFrame->setVisible(true);
	SetPaused(true);
	// Reset movement controls, in case the player is holding down a key
	if (m_pPlayer->HasShips())
	{
		ControlState* pPlayerControls = m_pPlayer->GetFlagShip()->GetControlState();
		bool bZeroThrust = pPlayerControls->m_bZeroThrust; // We want to keep this the same
		pPlayerControls->ResetMovement();
		pPlayerControls->m_bZeroThrust = bZeroThrust;
	}

	// Event callback for the yes & no buttons
	CEGUI::ButtonBase* pButton = (CEGUI::ButtonBase*)pFrame->getChild("YesButton");
	CEGUI::Event::Subscriber oHandler = CEGUI::Event::Subscriber(
		(bYesEndsGame ? &CEGUIEndGameButtonClicked : &CEGUIMessageBoxOKButtonClicked) );
	pButton->subscribeEvent(CEGUI::Listbox::EventMouseClick, oHandler);

	pButton = (CEGUI::ButtonBase*)pFrame->getChild("NoButton");
	oHandler = CEGUI::Event::Subscriber(
		&CEGUIMessageBoxOKButtonClicked);
	pButton->subscribeEvent(CEGUI::Listbox::EventMouseClick, oHandler);

#endif // BFP_NO_CEGUI

}

void BfPGUI::HideDialogue()
{

#ifndef BFP_NO_CEGUI
	// Get the dialogue that is visible
	if (!DialogueVisible()) return; // This also validates that only one is visible at a time
	CEGUI::FrameWindow* pFrame = GetMessageBox();
	if (!pFrame || !pFrame->isVisible()) pFrame = GetConfirmBox();
	if (!pFrame || !pFrame->isVisible()) return;

	// Hide the window, unpause the game, and set the mouse pointer back to the way it was
	pFrame->setVisible(false);
	SetPaused(false);
#endif // BFP_NO_CEGUI

}

bool BfPGUI::DialogueVisible()
{

#ifndef BFP_NO_CEGUI

	CEGUI::FrameWindow* pMsgBox = GetMessageBox();
	CEGUI::FrameWindow* pCfmBox = GetConfirmBox();
	if (pMsgBox && pMsgBox->isVisible() && pCfmBox && pCfmBox->isVisible())
		throw JFUtil::Exception("BfPGUI", "DialogueVisible", "Both dialogues were visible at once!");
	if (pMsgBox && pMsgBox->isVisible()) return true;
	else return (pCfmBox && pCfmBox->isVisible());

#else

	return false;

#endif // BFP_NO_CEGUI

}

void BfPGUI::SetPaused(bool bPause)
{
	if (bPause == m_bPause) return;
	m_bPause = bPause;

	// When pausing, we also need to pause the stuff controlled within OGRE like particles.
	Ogre::ControllerManager& rManager = Ogre::ControllerManager::getSingleton();
	Real fTF = rManager.getTimeFactor();

	if (fTF == 0)
	{
		assert(!bPause);
		rManager.setTimeFactor(m_fTimeFactorBeforePause);
	}
	else
	{
		assert(bPause);
		rManager.setTimeFactor(0);
	}

	m_fTimeFactorBeforePause = fTF;
	if (!bPause)
	{
		m_bJustUnpaused = true;
	}
}


void BfPGUI::UpdateMessageBoxes()
{
	// Get the first available message box event. As we can only show one at a time, put it back if we've got more.
	EventHandler& rEvents = GlobalEvents::Get();
	EventList lEvents = rEvents.PopEventsOfType("EV_MESSAGEBOX");
	for (EventList::iterator it = lEvents.begin(); it != lEvents.end(); ++it)
	{
		EventMessageBox* pEvent = (EventMessageBox*)*it;
		assert(pEvent);

		if (!DialogueVisible())
		{
			ShowMessageBox(pEvent->GetString(), pEvent->GetTitle(), pEvent->GetEndsGame());
			delete pEvent;
		}
		else
			rEvents.PushEvent(pEvent);

	}
}

void BfPGUI::UpdateChaseHUD()
{
#ifndef BFP_NO_CEGUI

	CEGUI::System& rSystem = CEGUI::System::getSingleton();
	CEGUI::GUIContext& rGUIContext = rSystem.getDefaultGUIContext();
	rGUIContext.injectMousePosition(m_fMouseX, m_fMouseY);
	assert(m_iCamMode == CM_CHASE);
	assert(m_pHUDWindow);

	std::stringstream sout;
	assert(m_pPlayer);
	assert(m_pPlayer->GetFlagShip());
	const ShipState* pPlayer = m_pPlayer->GetFlagShip()->GetState();
	const FactionState* pFaction = m_pPlayer->GetFactionState();
	assert(pPlayer && pFaction);

	sout << pPlayer->GetSP();
	m_pHUDWindow->getChild("SPValue")->setText(sout.str());
	sout.str(""); sout.clear();

	sout << pPlayer->GetHP();
	m_pHUDWindow->getChild("HPValue")->setText(sout.str());
	sout.str(""); sout.clear();

	sout << pPlayer->GetEP();
	m_pHUDWindow->getChild("EPValue")->setText(sout.str());
	sout.str(""); sout.clear();

	sout << pFaction->GetRPs();
	m_pHUDWindow->getChild("RPValue")->setText(sout.str());
	sout.str(""); sout.clear();

	// Display the type of the first weapon in the ship's currently-active weapon group
	// TODO: When we have custom groups we'll want to replace this with either a group name
	// or a list of all active types
	if (pPlayer->HasActiveWeapon())
	{
		const std::vector<unsigned>& rWeaps = pPlayer->GetActiveWeapons();
		assert(!rWeaps.empty());
		sout << pPlayer->GetWeaponState(*rWeaps.begin()).GetTypeName();
	}
	else sout << "N/A";
	m_pHUDWindow->getChild("WeapValue")->setText(sout.str());
	sout.str(""); sout.clear();

	if (pPlayer->HasActiveWeapon())
	{
		const std::vector<unsigned>& rWeaps = pPlayer->GetActiveWeapons();
		assert(!rWeaps.empty());
		// Display it as "Amm1/Amm2/etc"
		size_t iSize = rWeaps.size();
		for (size_t i = 0; i < iSize; ++i)
		{
			sout << pPlayer->GetWeaponState( rWeaps[i] ).GetAmmo();
			if (i+1<iSize) sout << "/";
		}
	}
	else
		sout << "N/A";
	m_pHUDWindow->getChild("AmmoValue")->setText(sout.str());
	sout.str(""); sout.clear();

	// Show the crosshair and cursor only if we're not doing free-rotate.
	if (m_bShift)
	{
		m_pHUDWindow->getChild("Crosshair")->setVisible(false);
		rGUIContext.getMouseCursor().hide();
	}
	else
	{
		m_pHUDWindow->getChild("Crosshair")->setVisible(true);
		rGUIContext.getMouseCursor().show();
	}

	// Update the list items.
	CEGUI::TabControl* pTargetMenu =
		(CEGUI::TabControl*)m_pHUDWindow->getChild("TargetMenu");
	UpdateChaseTargetMenu(pTargetMenu);

#endif // BFP_NO_CEGUI

	// Update the target pointers
	UpdateTargetPointers();
}

void BfPGUI::UpdateTargetPointers()
{

#ifndef BFP_NO_CEGUI

	assert(m_iCamMode == CM_CHASE);
	assert(m_pHUDWindow);
	CEGUI::Window* pPointer = m_pHUDWindow->getChild("TargetPointer");
	pPointer->setVisible(false);
	// Get a target position; if it returns NULL, there's nothing to show.
	const Vector3* pTargetPos = GetCurrentTargetPos();
	if (!pTargetPos || m_bShift) return;

	const ShipState* pPlayerState = m_pPlayer->GetFlagShip()->GetState();
	assert(pPlayerState);
	const Vector3& rMyPos = pPlayerState->GetPosition();
	const Vector3& rMyFacing = pPlayerState->GetFaceDir();
	const Vector3& rMyNormal = pPlayerState->GetFaceNorm();
	const Vector3& rMyRight = pPlayerState->GetFaceRight();

	// TODO: Optimise
	static Vector3 vecAxis;
	static Real fPropUp;
	static Real fPropLeft;
	vecAxis = *pTargetPos - rMyPos; // Target facing
	bool bInFront = rMyFacing.dotProduct(vecAxis) > 1;
	vecAxis.normalise();
	vecAxis = rMyFacing.crossProduct(vecAxis); // Axis to rotate from my facing to target facing
	vecAxis = vecAxis.crossProduct(rMyFacing); // Up to axis from our point of view
	fPropUp = vecAxis.dotProduct(rMyNormal);
	fPropLeft = vecAxis.dotProduct(Vector3::ZERO - rMyRight);

	// TEST CODE BEGIN
//	Degree degYaw;
//	Degree degPitch;
//	UtilControl::GetTurnAngle(rMyFacing, rMyNormal, (*pTargetPos - rMyPos).normalisedCopy(), degYaw, degPitch);
//	cout << "to yaw: " << degYaw.valueDegrees() << endl;
//	cout << "to pitch " << degPitch.valueDegrees() << endl;
	// TEST CODE END

	// Don't show the marker when it's on the crosshair (TODO: put these vals in config too)
	if ( bInFront && Ogre::Math::Abs(fPropLeft) < 0.1 &&
		 Ogre::Math::Abs(fPropUp) < 0.1 ) return;

	// The marker is positioned on a circle around the crosshair, pointing to the target.
	// TODO: Put this in a config file (crosshair position and radius of targeting circle)
	// TODO: Possible feature - adjustable/dynamic crosshair position?
	static Real fRadius;
	fRadius = 1.0/15.0;

	Vector2 vecMarkerPos (-fPropLeft, -fPropUp);
	vecMarkerPos.normalise();
	vecMarkerPos *= fRadius;
	vecMarkerPos += Vector2(634.0/1280.0, 524.0/1024.0); // Crosshair coordinates (TODO: from config!)

	CEGUI::UVector2 uvecMarkerPos(
		CEGUI::UDim(vecMarkerPos.x, 0.0),
		CEGUI::UDim(vecMarkerPos.y, 0.0) );

	pPointer->setVisible(true);
	pPointer->setPosition(uvecMarkerPos);

#endif // BFP_NO_CEGUI

}

const Vector3* BfPGUI::GetCurrentTargetPos()
{
	// Get the position of the current target, depending on the current target mode.
	const GroupController* pPlayerGroup = m_pPlayer->GetFlagShipGroup();
	const ShipController* pPlayer = pPlayerGroup->GetLeader();
	assert(pPlayer);
	const ShipState* pPlayerState = pPlayer->GetState();
	assert(pPlayerState);
	const BfPSectorState* pSectorState = pPlayer->GetSector();
	assert(pSectorState);
	const SectorType* pSectorType = pSectorState->GetSectorType();

	m_pPlayer->GetFlagShip()->UpdateTargeting();

	switch (m_iTargMode)
	{
		case TM_SECTORS:
		{
			const String* pTargetSector = pPlayerGroup->GetDestSector();
			if (!pTargetSector || *pTargetSector == pSectorType->GetName()) return NULL;
			return &pSectorType->GetWarpPointFromSector(*pTargetSector);
			break;
		}
		case TM_GROUPS:
		{
			const unsigned* pGroupID = m_pPlayer->GetSelectedGroup();
			if (!pGroupID || *pGroupID == pPlayerState->GetGroupID()) return NULL;
			std::vector<const ShipState*> vTargetMembers;
			const ShipState* pTargetLeader = pSectorState->GetShipGroup(
				m_pPlayer->GetFactionName(), *pGroupID, vTargetMembers);
			if (!pTargetLeader) return NULL;
			return &pTargetLeader->GetPosition();
			break;
		}
		case TM_ENEMIES:
		{
			if (!pPlayer->HasTargetShip()) return NULL;
			unsigned iTargetID = pPlayer->GetTargetShip();
			const ShipState* pTarget = (const ShipState*)
				pSectorState->GetObjState("SHIP", iTargetID);
			if (!pTarget) return NULL;
			return &pTarget->GetPosition();
			break;
		}
		case TM_BODIES:
		{
			if (m_pPlayer->GetSelectedSector() != pSectorType->GetName()) return NULL;
			const String* pBody = m_pPlayer->GetFlagShipGroup()->GetDestBody();
			if (!pBody) return NULL;
			// Could be a body in another sector?
			const BodyInstance* pBodyInstance = pSectorType->GetBodyWithID(*pBody);
			if (!pBodyInstance) return NULL;
			return &pBodyInstance->GetLocation();
			break;
		}
		default:
			throw JFUtil::Exception("BfPGUI", "UpdateTargetPointers",
				"Invalid target mode found");
	}
}


void BfPGUI::UpdateTacticalHUD()
{

#ifndef BFP_NO_CEGUI

	CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(m_fMouseX, m_fMouseY);

	assert(m_iCamMode == CM_TACTICAL || m_iCamMode == CM_MAP);
	assert(m_pTacticalWindow);

	std::stringstream sout;
	const FactionState* pFaction = m_pPlayer->GetFactionState();
	assert(pFaction);

	sout << pFaction->GetRPs();
	m_pTacticalWindow->getChild("RPValue")->setText(sout.str());
	sout.str(""); sout.clear();

	sout << pFaction->GetScore();
	m_pTacticalWindow->getChild("ScoreValue")->setText(sout.str());
	sout.str(""); sout.clear();

	// Update the target list items.
	CEGUI::TabControl* pTargetMenu =
		(CEGUI::TabControl*)m_pTacticalWindow->getChild("TargetMenu");
	UpdateTacticalTargetMenu(pTargetMenu);

	// Update the build menu too.
	CEGUI::TabControl* pBuildMenu =
		(CEGUI::TabControl*)m_pTacticalWindow->getChild("BuildMenu");
	UpdateBuildMenu(pBuildMenu);


	// Disable the move button if we don't have a group AND a sector/body/both selected, or if move is disabled
	bool bMoveEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(MOVEGROUP);
	CEGUI::ButtonBase* pMoveButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("MoveButton");
	pMoveButton->setEnabled(bMoveEnabled && m_pPlayer->CanMoveGroup());

	// Disable the clear button if we have no targets.
	CEGUI::ButtonBase* pClearButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("ClearButton");
	pClearButton->setEnabled( HasTargets() );

	// Disable the stance button if we have no selected group.
	CEGUI::ButtonBase* pStanceButton = (CEGUI::ButtonBase*)m_pTacticalWindow->getChild("StanceButton");
	if (!m_pPlayer->HasShips() || !m_pPlayer->GetSelectedGroup()) pStanceButton->disable();
	else // If enabled, set the next to the stance.
	{
		pStanceButton->enable();
		const GroupController* pGroup =
			m_pPlayer->GetGroupWithID(*m_pPlayer->GetSelectedGroup());
		if (!pGroup)
			pStanceButton->disable();
		else if (pGroup->IsDead())
			pStanceButton->disable();
		else
			pStanceButton->setText(pGroup->GetAIStanceType()->GetReadableName());

	}

#endif // BFP_NO_CEGUI

}

void BfPGUI::UpdateMessages()
{

#ifndef BFP_NO_CEGUI

	const GeneralConfig* pConf = (const GeneralConfig*)
		BfPConfig::GetInstance().GetConfig("General");
	unsigned iMaxMessages = pConf->GetMaxMessages();
	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();

	// Get message events and make sure the list is no bigger than the maximum.
	EventHandler& rEvents = GlobalEvents::Get();
	EventList lEvents = rEvents.PopEventsOfType("EV_MESSAGE");
	for (EventList::iterator it = lEvents.begin(); it != lEvents.end(); ++it)
	{
		EventMessage* pEvent = (EventMessage*)*it;
		assert(pEvent);
		m_lMessages.push_back(pEvent->GetString());
		delete pEvent;
	}

	while (m_lMessages.size() > iMaxMessages)
		m_lMessages.pop_front();

	if (m_iCamMode == CM_FREELOOK) return;
	assert(m_iCamMode != CM_LAST);

	// XXX: These messages should be global and independent of the window, rather than duplicated like this.
	CEGUI::MultiLineEditbox* pMessageBox = (CEGUI::MultiLineEditbox*)
		rGUIContext.getRootWindow()->getChild("MessagePanel");
	assert(pMessageBox);

	// Do it backwards so messages are bottom to top.
	std::stringstream sout;
	for (std::list<String>::reverse_iterator it = m_lMessages.rbegin();
		 it != m_lMessages.rend(); ++it)
	{
		sout << *it << endl;
	}
	pMessageBox->setText(sout.str());

#endif // BFP_NO_CEGUI

}

/**
 * Code for the logic engine HUD
 */

void BfPGUI::UpdateLogicEngineHUD()
{

#ifndef BFP_NO_CEGUI

	// TODO: Update all of the outputs etc for logic engine HUD
	CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(m_fMouseX, m_fMouseY);

	CEGUI::TabControl* pMenu = (CEGUI::TabControl*)
		m_pLogicEngineWindow->getChild("InfoMenu");
	UpdateInfoMenu(pMenu);

	pMenu = (CEGUI::TabControl*)m_pLogicEngineWindow->getChild("UnitSelectMenu");
	UpdateUnitSelectMenu(pMenu);

	CEGUI::Window* pWindow = m_pLogicEngineWindow->getChild("borderBL/TickMenu");
	UpdateTickMenu(pWindow);

	// Update the debug message box
	UpdateMessages();

#endif // BFP_NO_CEGUI

}


// Begin CEGUI-specific function implementations

#ifndef BFP_NO_CEGUI

CEGUI::FrameWindow* BfPGUI::GetMessageBox()
{
	// NOTE: No GUI in free look (it's really just for debugging/testing..)
	if (m_iCamMode == CM_FREELOOK) return NULL;

	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::FrameWindow* pFrame = (CEGUI::FrameWindow*)rGUIContext.getRootWindow()->getChild("MessageBox");
	assert(pFrame);
	return pFrame;
}

CEGUI::FrameWindow* BfPGUI::GetConfirmBox()
{
	// NOTE: No GUI in free look (it's really just for debugging/testing..)
	if (m_iCamMode == CM_FREELOOK) return NULL;

	CEGUI::GUIContext& rGUIContext = CEGUI::System::getSingleton().getDefaultGUIContext();
	CEGUI::FrameWindow* pFrame = (CEGUI::FrameWindow*)rGUIContext.getRootWindow()->getChild("ConfirmBox");
	assert(pFrame);
	return pFrame;
}

CEGUI::TabControl* BfPGUI::GetTargetMenu()
{
	switch (m_iCamMode)
	{
		case CM_CHASE:
		{
			return static_cast<CEGUI::TabControl*>(m_pHUDWindow->getChild("TargetMenu"));
		}
		case CM_TACTICAL:
		case CM_MAP:
		{
			return static_cast<CEGUI::TabControl*>(m_pTacticalWindow->getChild("TargetMenu"));
		}
		default:
		{
			return NULL;
		}
	}
}

void BfPGUI::UpdateChaseTargetMenu(CEGUI::TabControl* pTargetMenu)
{
	CEGUI::Window* pTabPane = pTargetMenu->getChildAtIdx(0);

	CEGUI::Listbox* pList;
	CEGUI::Window* pTab;
	pTab = pTabPane->getChild("Groups");
	pList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateGroupList(pList);

	pTab = pTabPane->getChild("Enemies");
	pList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateEnemyList(pList);

	pTab = pTabPane->getChild("Bodies");
	pList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateBodyList(pList);

	pTab = pTabPane->getChild("Sectors");
	pList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateWarpList(pList);
}

void BfPGUI::UpdateTacticalTargetMenu(CEGUI::TabControl* pTargetMenu)
{
	CEGUI::Window* pTabPane = pTargetMenu->getChildAtIdx(0);

	// We need to update m_iTargMode when the tab is clicked on
	// It's slightly easier to do this just by seeing if it changed than by braving CEGUI's event system.
	String strMode = GetActiveTargetTabName();
	unsigned iNewMode = pTargetMenu->getSelectedTabIndex();
	CEGUI::Window* pSelectedTab = pTargetMenu->getTabContentsAtIndex(iNewMode);
	// Oh, you need your own implementation of String, do you CEGUI? Well that's just peachy.
	String strNewMode (pSelectedTab->getName().c_str());
	SetTargetMode(strNewMode);

	CEGUI::Window* pTab = pTabPane->getChild("Sectors");

	CEGUI::Listbox* pSectorsList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateSectorList(pSectorsList);

	pTab = pTabPane->getChild("Groups");
	CEGUI::Listbox* pGroupList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateGroupList(pGroupList);

	pTab = pTabPane->getChild("Enemies");
	CEGUI::Listbox* pEnemyList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateEnemyList(pEnemyList);

	pTab = pTabPane->getChild("Bodies");
	CEGUI::Listbox* pBodyList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateBodyList(pBodyList);


}

void BfPGUI::UpdateBuildMenu(CEGUI::TabControl* pBuildMenu)
{
	const GeneralConfig* pGameConf = (GeneralConfig*)
		GameConfig::GetInstance().GetConfig("General");
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	assert(m_iCamMode == CM_TACTICAL || m_iCamMode == CM_MAP);

	CEGUI::Window* pTabPane = pBuildMenu->getChildAtIdx(0);
	CEGUI::Window* pPurchaseTab = pTabPane->getChild("PurchaseTab");
	CEGUI::Listbox* pPurchaseList = (CEGUI::Listbox*)pPurchaseTab->getChild("List");
	UpdatePurchaseList(pPurchaseList);

	CEGUI::Window* pDeployTab = pTabPane->getChild("DeployTab");
	CEGUI::Listbox* pDeployList = (CEGUI::Listbox*)pDeployTab->getChild("List");
	UpdateDeployList(pDeployList);

	CEGUI::ButtonBase* pExecuteButton = (CEGUI::ButtonBase*)
		m_pTacticalWindow->getChild("ExecuteButton");

	// Enabled state of the Execute button depends on the selected tab
	bool bPurchaseEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(PURCHASE);
	bool bDeployEnabled = m_pPlayer->GetObservedState()->CheckPlayerInputFlag(DEPLOY);
	if (pPurchaseTab->isVisible())
	{
		pExecuteButton->setText("Build");
		if (!bPurchaseEnabled || m_strSelectedShipType == "") pExecuteButton->disable();
		else
		{
			const ShipType* pSelectedType = pShipsConf->GetShipType(m_strSelectedShipType);
			assert(pSelectedType);
			// Could have a selected item in the list that we can't afford; unselect it if so
			unsigned iGroupCost = 0;
			unsigned iShipCost = pSelectedType->GetRPCost();
			unsigned iNoToDeploy = m_iNoToBuy == 0 ? 1 : m_iNoToBuy;
			for (unsigned i=0;i<iNoToDeploy;++i) iGroupCost += iShipCost;
			pExecuteButton->setEnabled(m_pPlayer->GetFactionState()->GetRPs() >= (long)iGroupCost);
		}
	}
	else if (pDeployTab->isVisible())
	{
		pExecuteButton->setText("Deploy");
		if (!bDeployEnabled || m_strSelectedShipType == "") pExecuteButton->disable();
		else
		{
			StringVector vGroup;
			unsigned iNoToDeploy = m_iNoToBuy == 0 ? 1 : m_iNoToBuy;
			for (unsigned i=0;i<iNoToDeploy;++i) vGroup.push_back(m_strSelectedShipType);
			bool bPossible = m_pPlayer->GetObservedState()->CanDeployGroup(
				vGroup, m_pPlayer->GetSelectedSector(),
				m_pPlayer->GetNextGroupID(), !m_pPlayer->HasShips());
			pExecuteButton->setEnabled(bPossible);

		}

	}
	else
	{
		pExecuteButton->setText("Execute");
		pExecuteButton->disable();
	}

	// Disable the dec button if the number to deploy is 1
	CEGUI::ButtonBase* pDecButton = (CEGUI::ButtonBase*)
		m_pTacticalWindow->getChild("borderTL/DecButton");
	pDecButton->setEnabled(m_iNoToBuy > 1);
	// Disable the inc button if number to deploy is max
	CEGUI::ButtonBase* pIncButton = (CEGUI::ButtonBase*)
		m_pTacticalWindow->getChild("borderTL/IncButton");
	pIncButton->setEnabled(m_iNoToBuy < pGameConf->GetMaxGroupSize());


	// Make sure that the contents of BuyCount match m_iNoToBuy
	CEGUI::Editbox* pBuyCount = (CEGUI::Editbox*)
		m_pTacticalWindow->getChild("borderTL/BuyCountLabel/BuyCount");
	std::stringstream ss;
	// If plus/minus was clicked inc/decrement m_iNoToBuy and set the text to that
	if (m_bPlusMinusClicked)
	{
		ss << m_iNoToBuy;
		pBuyCount->setText(ss.str());
		m_bPlusMinusClicked = false;
	}
	else // Otherwise set m_iNoToBuy to the text if changed
	{
		String strNoToBuy ( pBuyCount->getText().c_str() );
		ss << strNoToBuy;
		unsigned iNewNo;
		bool bFail = (ss >> iNewNo).fail();
		if (iNewNo == 0 || iNewNo > pGameConf->GetMaxGroupSize()) bFail = true;
		if (bFail) // If it's not a valid number..
		{
			ss.str(""); ss.clear();
			ss << m_iNoToBuy;
			pBuyCount->setText(ss.str());
		}
		else
		{
			m_iNoToBuy = iNewNo;
		}
	}

}

void BfPGUI::UpdatePurchaseList(CEGUI::Listbox* pList)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	unsigned iMyRPs = m_pPlayer->GetFactionState()->GetRPs();

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	unsigned iNoToBuy = m_iNoToBuy;
	if (iNoToBuy == 0) iNoToBuy = 1;
	m_mPurchaseList.clear();
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const ShipType* pType = pShipsConf->GetShipType(*it);
		assert(pType);
		unsigned iCost = pType->GetRPCost();
		m_mPurchaseList.insert( pair<unsigned, String>(iCost, pType->GetName()) );
	}

	if (pList->getItemCount() > 0) pList->resetList();
	for (PurchaseMap::const_iterator it = m_mPurchaseList.begin();
		 it != m_mPurchaseList.end(); ++it)
	{
		unsigned iCost = it->first;
		iCost *= iNoToBuy;
		const String& rName = it->second;
		const ShipType* pThisShip = pShipsConf->GetShipType(rName);

		std::stringstream sout;
		sout << iNoToBuy << "x" << (pThisShip ? pThisShip->GetReadableName() : "OHSHITITBROKE?!") << " (" << iCost << " RP)";
		CEGUI::ListboxTextItem* pItem =
			new CEGUI::ListboxTextItem(sout.str());

		// Highlight if selected and affordable (the execute button should also be greyed out if not affordable!)
		if (iMyRPs >= iCost)
		{
			if (rName == m_strSelectedShipType)
				pItem->setTextColours(CEGUI::Colour(1,1,1));
			else // Unselected but can afford
				pItem->setTextColours(CEGUI::Colour(0.65,0.65,0.65));
		}
		else // Unselected and can't afford
			pItem->setTextColours(CEGUI::Colour(0.3,0.3,0.3));

		pList->addItem(pItem);
	}
	if (pList->getItemCount() == 0)
		pList->addItem(new CEGUI::ListboxTextItem("(Empty?)"));

}

void BfPGUI::UpdateDeployList(CEGUI::Listbox* pList)
{
	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	const StringVector& rShipTypes = pShipsConf->GetShipTypes();
	// TODO: Create a system that does the GUI update only if it has been changed
	if (pList->getItemCount() > 0) pList->resetList();
	m_vDeployList.clear();

	const FactionState* pFaction = m_pPlayer->GetFactionState();
	// Either this while look will end or the assertion of pGroup being valid will fail
	// List each ship type of which we have some available. Highlight the selected one, if any.
	unsigned iNoToBuy = m_iNoToBuy == 0 ? 1 : m_iNoToBuy;
	for (StringVector::const_iterator it = rShipTypes.begin();
		 it != rShipTypes.end(); ++it)
	{
		const String& rShip = *it;
		const ShipType* pShip = pShipsConf->GetShipType(*it);
		assert(pShip);

		if (!pFaction->HasShipToDeploy(rShip))
			continue;

		// See how many ships we could deploy. I know this is a bit of a weird way to do it..
		// TODO: Better way: change the FactionState to have a shiptype->number map rather than a stringvector of types
		unsigned iNoAvail = 0;
		FactionState* pFactionCopy = new FactionState(*pFaction);
		while (pFactionCopy)
		{
			if (pFactionCopy->HasShipToDeploy(rShip))
			{
				pFactionCopy->RemoveDeployingShip(rShip);
				iNoAvail++;
			}
			else
			{
				delete pFactionCopy;
				pFactionCopy = NULL;
			}
		}
		assert(iNoAvail > 0);
		std::stringstream sout;
		sout << pShip->GetReadableName() << " (Have " << iNoAvail << ")";

		CEGUI::ListboxTextItem* pItem =
			new CEGUI::ListboxTextItem(sout.str());

		if (rShip == m_strSelectedShipType && iNoToBuy <= iNoAvail)
			pItem->setTextColours( CEGUI::Colour(1,1,1) );
		else
			pItem->setTextColours( CEGUI::Colour(0.3,0.3,0.3) );

		pList->addItem(pItem);

		m_vDeployList.push_back(rShip);

	}

	if (pList->getItemCount() == 0)
	{
		// TODO: More efficient (no allocating new stuff every update)
		if (pList->getItemCount() > 0) pList->resetList();
		pList->addItem(new CEGUI::ListboxTextItem("(Nothing to deploy)"));
	}

}

void BfPGUI::UpdateSectorList(CEGUI::Listbox* pList)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// We show the current sector at the top, followed by adjacent sectors, followed by all others
	//const ShipState* pPlayerShip = m_pPlayer->GetFlagShip()->GetState();

	// Note that this could return null if the sector isn't visible
	const BfPSectorState* pCurrentSectorState = m_pPlayer->GetSelectedSectorState();

	const SectorType* pCurrentSector = (pCurrentSectorState ?
		m_pPlayer->GetSelectedSectorState()->GetSectorType() :
		pScenarioConf->GetSectorType(m_pPlayer->GetSelectedSector()));
	assert(pCurrentSector);

	const String& rCurrentSector = pCurrentSector->GetName();

	std::stringstream sout;
	sout << "*" << rCurrentSector << "*";
	String strCurrentStarred = sout.str();
	sout.str(""); sout.clear();

	// If we're in the same sector as last update, nothing needs to be done.
	CEGUI::ListboxItem* pFirstItem = (CEGUI::ListboxItem*)pList->getChildAtIdx(0);
	if (pFirstItem->getText() == strCurrentStarred)
		return;

	// Otherwise, clear and refill the whole thing.
	const StringVector& rAdjacentSectors = pCurrentSector->GetLinkedSectors();
	const StringVector& rAllSectors = pScenarioConf->GetSectorTypes();

	m_vSectors.clear();
	m_vSectors.push_back(rCurrentSector);

	if (pList->getItemCount() > 0) pList->resetList();
	CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(strCurrentStarred);
	Vector3 vecColour = m_pPlayer->GetSectorColour(rCurrentSector);
	pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );
	pList->addItem(pItem);


	for (StringVector::const_iterator it = rAdjacentSectors.begin();
		 it != rAdjacentSectors.end(); ++it)
	{
		assert(*it != rCurrentSector); // Sector adjacent to itself??
		Vector3 vecColour = m_pPlayer->GetSectorColour(*it);
		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(*it);
		pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );

		pList->addItem(pItem);
		m_vSectors.push_back(*it);
	}

	// Now do the rest.
	for (StringVector::const_iterator it = rAllSectors.begin();
		 it != rAllSectors.end(); ++it)
	{
		if ( *it != rCurrentSector &&
			 find(rAdjacentSectors.begin(), rAdjacentSectors.end(), *it) == rAdjacentSectors.end() )
		{
			sout << "(" << *it << ")";
			CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());
			// Work out the colour for the sector
			Vector3 vecColour = m_pPlayer->GetSectorColour(*it);
			pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );

			pList->addItem(pItem);
			m_vSectors.push_back(*it);
			sout.str(""); sout.clear();
		}
	}

}

void BfPGUI::UpdateWarpList(CEGUI::Listbox* pList)
{
	// We only list the sectors that are linked to the current one here
	const GroupController* pPlayer = m_pPlayer->GetFlagShipGroup();
	const ShipController* pPlayerShip = m_pPlayer->GetFlagShip();
	const SectorType* pCurrentSector = pPlayer->GetSector()->GetSectorType();
	assert(pCurrentSector);

	const String* pSelectedSector = pPlayer->GetDestSector();
	m_vLinks.clear();


	pList->resetList();

	Vector3 vecColour;
	std::stringstream sout;
	String strSelectedStarred;
	if (pSelectedSector)
	{
		sout << "*" << *pSelectedSector << "*";
		strSelectedStarred = sout.str();
		sout.str(""); sout.clear();
	}

	// Otherwise, clear and refill the whole thing.
	const StringVector& rLinks = pCurrentSector->GetLinkedSectors();
	m_vLinks = rLinks;
	Real fDist;
	Vector3 vecPoint;

	for (StringVector::const_iterator it = rLinks.begin();
		 it != rLinks.end(); ++it)
	{
		vecColour = m_pPlayer->GetSectorColour(*it);

		// Show the distance on the label too
		String strText = (pSelectedSector && *it == *pSelectedSector) ?
			strSelectedStarred : *it;
		vecPoint = pCurrentSector->GetWarpPointFromSector(*it);
		fDist = pPlayerShip->GetState()->GetPosition().distance(vecPoint);
		sout << "Warp to " << strText << " (Dist: " << (int)fDist << ")";
		strText = sout.str();
		sout.str(""); sout.clear();

		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(strText);
		pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );

		pList->addItem(pItem);
		m_vLinks.push_back(*it);
	}

}

void BfPGUI::UpdateGroupList(CEGUI::Listbox* pList)
{
	assert(pList);

	// If we have no ships, clear and return (or else we may have lingering items that cause a crash on clicking)
	if (!m_pPlayer->HasShips())
	{
		m_mGroups.clear();
		pList->resetList();
		return;
	}

	const ShipController* pPlayer = m_pPlayer->GetFlagShip();
	const ShipState* pPlayerShip = pPlayer->GetState();
	const Vector3& rMyPos = pPlayerShip->GetPosition();

	//  multimap<unsigned, map<Real, const ShipState*> >
	m_mGroups.clear();

	const unsigned* pSelectedGroup = m_pPlayer->GetSelectedGroup();

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	// Get ALL groups owned by the faction!
	const StringVector& rSectors = pScenarioConf->GetSectorTypes();
	for (StringVector::const_iterator it = rSectors.begin();
		 it != rSectors.end(); ++it)
	{
		const String& rSector = *it;
		const BfPSectorState* pCurrentSector =
			m_pPlayer->GetObservedState()->ObserveSector(rSector);
		if (!pCurrentSector) continue;
		const ObjList& rShips = pCurrentSector->GetObjectList("SHIP");
		for (ObjList::const_iterator it = rShips.begin();
			 it != rShips.end(); ++it)
		{
			const ShipState* pShip = (const ShipState*)*it;
			if (pShip->GetFaction() != m_pPlayer->GetFactionName()) continue;
			unsigned iGroup = pShip->GetGroupID();
			// -1 distance for ships in another sector or tactical view
			bool bShowDist =
				(m_iCamMode == CM_CHASE && rSector == pPlayer->GetSector()->GetSectorType()->GetName());
			Real fDist = bShowDist ?
				(pShip->GetPosition() - rMyPos).length() : -1;

			// Get the distance sub-map (i.e. the list for iGroup) in the map; create if doesn't exist
			GroupMap::iterator itGroup = m_mGroups.find(iGroup);
			if (itGroup == m_mGroups.end())
				itGroup = m_mGroups.insert( pair<unsigned, ShipMap >(iGroup, ShipMap()) );

			ShipMap& rGroup = itGroup->second;
			rGroup.insert( pair<Real, const ShipState*>(fDist, pShip) );
		}
	}


	const FactionType* pFaction = m_pPlayer->GetFactionState()->GetFactionType();

	if (pList->getItemCount() > 0)
		pList->resetList();
	for (GroupMap::const_iterator it = m_mGroups.begin();
		 it != m_mGroups.end(); ++it)
	{
		unsigned iGroupNo = it->first;
		const ShipMap& rGroup = it->second;
		assert(!rGroup.empty());

		bool bGotLeader = false;
		String strLeaderType;
		Real fLeaderDist = -1; // Will be assigned or the following assertion will fail
		Real fCondition = 0;
		Real fEPOfMax = 0;
		unsigned iNoShips = rGroup.size();
		assert(iNoShips > 0);
		for (ShipMap::const_iterator itShip = rGroup.begin();
			 itShip != rGroup.end(); ++itShip)
		{
			Real fDist = itShip->first;
			const ShipState* pShip = itShip->second;
			if (pShip->IsGroupLeader())
			{
				strLeaderType = pShip->GetShipType()->GetReadableName();
				fLeaderDist = fDist;
				bGotLeader = true;
			}
			// Condition excluding energy
			fCondition += pShip->GetOverallCondition(false);
			fEPOfMax += pShip->GetEPOfMax();

		}
		assert(bGotLeader); // Must have leader!
		fCondition /= rGroup.size();
		fEPOfMax /= rGroup.size();

		std::stringstream sout;
		sout << iGroupNo << "-" << strLeaderType << "x" << iNoShips;
		sout << " H:" << (unsigned)(fCondition*100) << "%";
		sout << " E: " << (unsigned)(fEPOfMax*100) << "%";
		if (fLeaderDist >= 0) sout << " (Dist: " << (int)fLeaderDist << ")";
		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());

		Vector3 vecColour;
		if (pSelectedGroup && *pSelectedGroup == iGroupNo)
			vecColour = pFaction->GetSelectedColour();
		else
			vecColour = pFaction->GetUnselectedColour();

		pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );
		pList->addItem(pItem);

	}
}

void BfPGUI::UpdateEnemyList(CEGUI::Listbox* pList)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	// Consult what our faction knows about the current sector.
	const BfPSectorState* pCurrentSector = m_pPlayer->GetSelectedSectorState();
	// Non-visible sector
	if (!pCurrentSector)
	{
		CEGUI::ListboxItem* pFirstItem = (CEGUI::ListboxItem*)pList->getChildAtIdx(0);
		if (pList->getItemCount() != 1 ||
			pFirstItem->getText() != "(Unknown)")
		{
			if (pList->getItemCount() > 0) pList->resetList();
			pList->addItem(new CEGUI::ListboxTextItem("(Unknown)"));
		}
		return;
	}

	// Instead, handle ship list for visible sector.
	const ShipController* pPlayer = m_pPlayer->GetFlagShip();
	const ShipState* pPlayerShip = pPlayer->GetState();
	const Vector3& rMyPos = pPlayerShip->GetPosition();

	m_mEnemies.clear();

	const ObjList& rShips = pCurrentSector->GetObjectList("SHIP");
	for (ObjList::const_iterator it = rShips.begin();
		 it != rShips.end(); ++it)
	{
		const ShipState* pShip = (const ShipState*)*it;
		if (pShip->GetFaction() == m_pPlayer->GetFactionName()) continue;
		Real fDist = (pShip->GetPosition() - rMyPos).length();
		m_mEnemies.insert( pair<Real, const ShipState*>(fDist, pShip) );
	}

	if (pList->getItemCount() > 0) pList->resetList();
	for (ShipMap::const_iterator it = m_mEnemies.begin();
		 it != m_mEnemies.end(); ++it)
	{
		Real fDist = it->first;
		const ShipState* pShip = it->second;

		std::stringstream sout;
		sout << pShip->GetFaction() << " ";
		sout << pShip->GetShipType()->GetReadableName() << " " << pShip->GetID();

		bool bFlagGroup = pShip->IsInFlagGroup();
		bool bLeader = pShip->IsGroupLeader();
		// Concise (as we've limited space..) markers of whether the ship is a flag-member/leader
		sout << " (Dist: " << (int)fDist << ")";
		if (bFlagGroup || bLeader)
		{
			sout << " (";
			if (bFlagGroup) sout << "F";
			if (bLeader) sout << "L";
			sout << ")";
		}



		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());
		Vector3 vecColour;

		const FactionType* pFaction = pScenarioConf->GetFactionType(pShip->GetFaction());
		if (pPlayer->HasTargetShip() && pPlayer->GetTargetShip() == pShip->GetID())
			vecColour = pFaction->GetSelectedColour();
		else
			vecColour = pFaction->GetUnselectedColour();

		pItem->setTextColours( CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );
		pList->addItem(pItem);
	}

}

void BfPGUI::UpdateBodyList(CEGUI::Listbox* pList)
{
	// Get info on the current sector.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

//	const BfPSectorState* pCurrentSector = m_pPlayer->GetSelectedSectorState();
	// Player ship may be null
	const ShipState* pPlayerShip = m_pPlayer->HasShips() ?
		m_pPlayer->GetFlagShip()->GetState() : NULL;

	// If it's the ship view, the body list lists bodies in the ship's current  sector.
	// if it's the tactical view, it's bodies in the selected sector.
	bool bChaseCam = m_iCamMode == CM_CHASE && pPlayerShip;
	const SectorType* pSectorType = bChaseCam ?
		m_pPlayer->GetFlagShip()->GetSector()->GetSectorType() :
		pScenarioConf->GetSectorType(m_pPlayer->GetSelectedSector());
	assert(pSectorType);
	const BodyVector& rBodies = pSectorType->GetBodies();

	// TODO: Do these things more efficiently (insert when necessary rather than clear each update)
	m_mBodies.clear();

	// We need to highlight the item that's currently selected by the faction.
	const String* pSelectedBody = bChaseCam ?
		m_pPlayer->GetFlagShipGroup()->GetDestBody() :
		m_pPlayer->GetSelectedObject();

	for (std::vector<BodyInstance*>::const_iterator it = rBodies.begin();
		 it != rBodies.end(); ++it)
	{
		const BodyInstance* pBody = *it;
		Real fDist = (bChaseCam ?
			(pBody->GetLocation() - pPlayerShip->GetPosition()).length() :
			-1); // Given -1 if unobserved
		const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
		if (!pType->IsBackground() && pType->IsMajor())
			m_mBodies.insert( pair<Real, const BodyInstance*>(fDist, pBody) );
	}

	// Finally, do the body list.
	if (pList->getItemCount() > 0) pList->resetList();
	for (BodyMap::iterator it = m_mBodies.begin();
		 it != m_mBodies.end(); ++it)
	{
		Real fDist = it->first;
		const BodyInstance* pBody = it->second;

		std::stringstream sout;
		const BodyType* pType = pScenarioConf->GetBodyType(pBody->GetType());
		assert(pType);
		sout << pType->GetReadableName() << " " << pBody->GetID();
		if (fDist >= 0) sout << " (Dist: " << (int)fDist << ")";
		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());
		// Set the text colour based on whether it is selected or not.

		if (pSelectedBody && *pSelectedBody == pBody->GetID())
			pItem->setTextColours( CEGUI::Colour(1, 1, 1) );
		else
			pItem->setTextColours( CEGUI::Colour(0.6,0.6,0.6) );

		pList->addItem(pItem);
	}
}

void BfPGUI::UpdateInfoMenu(CEGUI::TabControl* pInfoMenu)
{
	assert(m_iCamMode == CM_LOGICENGINE);

	CEGUI::Window* pTabPane = pInfoMenu->getChildAtIdx(0);
	CEGUI::Window* pTab = pTabPane->getChild("FactionTab");
	CEGUI::MultiLineEditbox* pInfoBox  = (CEGUI::MultiLineEditbox*)pTab->getChild("Info");
	UpdateFactionInfo(pInfoBox);

	pTab = pTabPane->getChild("UnitTab");
	pInfoBox = (CEGUI::MultiLineEditbox*)pTab->getChild("Info");
	UpdateUnitInfo(pInfoBox);

	pTab = pTabPane->getChild("PlayerModelTab");
	pInfoBox = (CEGUI::MultiLineEditbox*)pTab->getChild("Info");
	UpdateModelInfo(pInfoBox);

	pTab = pTabPane->getChild("LearningTab");
	pInfoBox = (CEGUI::MultiLineEditbox*)pTab->getChild("Info");
	UpdateLearningInfo(pInfoBox);
}

void BfPGUI::UpdateFactionInfo(CEGUI::MultiLineEditbox* pInfoBox)
{
	// Get the faction info from the logic engine and display it.
	const AbstractBfPState* pBfPState = AITestLogicEngine::GetInstance().GetState();
	const AbstractFactionState* pFaction = pBfPState->GetFaction(m_strSelectedFaction);
	bool bIsPlayer = m_strSelectedFaction == pBfPState->GetMyFaction();

	std::stringstream sout;

	sout << "Name: " << m_strSelectedFaction << endl;
	sout << "Score: " << pFaction->GetScore() << endl;
	if (bIsPlayer)
	{
		// TODO: These assertions should pass, but occasionally they don't. Check the abs state update function.
		//assert(pFaction->GetMinRPs() == pFaction->GetEstimatedRPs() &&
		//	   pFaction->GetMaxRPs() == pFaction->GetEstimatedRPs());
		sout << "RPs: " << pFaction->GetEstimatedRPs() << endl;
		//assert(pBfPState->GetFactionEstimatedUnitCount(m_strSelectedFaction) == 0);
		sout << "Units: " <<
			pBfPState->GetFactionVisibleUnitCount(m_strSelectedFaction) << endl;
		//assert(pBfPState->GetFactionEstimatedSectorCount(m_strSelectedFaction) == 0);
		sout << "Sectors: " <<
			pBfPState->GetFactionVisibleSectorCount(m_strSelectedFaction) << endl;
	}
	else
	{
		sout << "Estimated RPs: " << pFaction->GetEstimatedRPs() << endl;
		sout << "Min RPs: " << pFaction->GetMinRPs();
		sout << " Max RPs: " << pFaction->GetMaxRPs() << endl;
		sout << "Units - Known: " << pBfPState->GetFactionVisibleSectorCount(m_strSelectedFaction);
		sout << " Estimated: " << pBfPState->GetFactionEstimatedUnitCount(m_strSelectedFaction) << endl;
		sout << "Sectors - Known: " << pBfPState->GetFactionVisibleSectorCount(m_strSelectedFaction);
		sout << " Estimated: " << pBfPState->GetFactionEstimatedSectorCount(m_strSelectedFaction) << endl;
	}

	if (pInfoBox->getText() != sout.str())
		pInfoBox->setText(sout.str());
}

void BfPGUI::UpdateUnitInfo(CEGUI::MultiLineEditbox* pInfoBox)
{
	// Get the faction/unit info from the test logic engine.
	const AbstractBfPState* pBfPState = AITestLogicEngine::GetInstance().GetState();
	unsigned* pSelectedUnit = m_mSelectedUnits[m_strSelectedFaction];
	const AbstractUnitState* pUnit = pSelectedUnit ?
		pBfPState->GetUnit(m_strSelectedFaction, *pSelectedUnit) : NULL;

	// Only get the unit info if it's selected.
	String strInfo = pUnit ?
		GetUnitInfo(pUnit) : "Nothing selected.";

	if (pInfoBox->getText() != strInfo)
		pInfoBox->setText(strInfo);
}

String BfPGUI::GetUnitInfo(const AbstractUnitState* pUnit)
{
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();

	const ShipsConfig* pShipsConf = (const ShipsConfig*)
		BfPConfig::GetInstance().GetConfig("Ships");

	// If the unit doesn't exist in the current tick,
	bool bFuture = m_iSelectedTick < pUnit->GetEarliestTick();
	bool bPast = m_iSelectedTick > pUnit->GetLatestTick();
	std::stringstream sout;

	// Stuff we display no matter what
	sout << "Unit ID: " << pUnit->GetUnitID();
	sout << " Group ID: ";
	if (pUnit->GetHypothetical())
		sout << "N/A";
	else
		sout << pUnit->GetGroupID();
		sout << endl << "Tick - Earliest: " << pUnit->GetEarliestTick() << " Latest: " << pUnit->GetLatestTick();

	if (bFuture)
	{
		assert(!bPast);
		sout << "Doesn't exist yet!";
	}
	else if (bPast)
	{
		assert(!bFuture);
		sout << "No longer exists!";

	}
	else // Exists now
	{
		const UnitHistoryEntry* pEntry = pUnit->GetEntry(m_iSelectedTick);
		bool bIsObserved = pEntry->GetObserved();

		// TODO: Do history of unit health in the unit at some point
		sout << endl << "Health: " <<
			(unsigned)(pUnit->GetHealth()*100.0) << "%";

		if (bIsObserved)
		{
			sout << endl << "Type: " << pUnit->GetKnownType();
			sout << endl << "Count: " << pUnit->GetKnownCount();
			sout << endl << "Sector: " << pEntry->GetObservedLoc();
		}
		else
		{
			sout << endl << "Exists? " << (unsigned)
				(pEntry->GetExistence()*100.0) << "%";

			sout << endl << "Purchased? " <<
				(pEntry->GetPurchased() ? "Yes" : "No");

			const StringVector& rShipTypes = pShipsConf->GetShipTypes();
			const StringVector& rSectors = pScenarioConf->GetSectorTypes();
			sout << endl << "Type:";
			// TODO: Could sort descending by probability
			// For each ship type, give a probability.
			for (StringVector::const_iterator it = rShipTypes.begin();
				 it != rShipTypes.end(); ++it)
			{
				const Real& rProb = pUnit->GetTypeProb(*it);
				if (rProb > 0)
				{
					sout << endl << "  " << *it << "? ";
					sout << (unsigned)(rProb*100) << "%";
				}

			}

			// TODO: Put count probabilities in here (this whole GUI isn't really needed until we start with advanced AI)

			// TODO: For each location with a probability, give percentage
			sout << endl << "Sector:";
			for (StringVector::const_iterator it = rSectors.begin();
				 it != rSectors.end(); ++it)
			{
				const Real& rProb = pEntry->GetLocProb(*it);
				if (rProb > 0)
				{
					sout << endl << "  " << *it << "? ";
					sout << (unsigned)(rProb*100) << "%";
				}
			}

		}
	}

	return sout.str();
}

void BfPGUI::UpdateModelInfo(CEGUI::MultiLineEditbox* pInfoBox)
{
	const AILogicEngine* pLogicEngine =
		AITestLogicEngine::GetInstance().GetEngine();
	const AIPlayerModel* pModel = pLogicEngine->GetPlayerModel();

	String strInfo = GetPlayerModelInfo(pModel);

	if (pInfoBox->getText() != strInfo)
		pInfoBox->setText(strInfo);
}

void BfPGUI::UpdateLearningInfo(CEGUI::MultiLineEditbox* pInfoBox)
{
	const AILogicEngine* pLogicEngine =
		AITestLogicEngine::GetInstance().GetEngine();
	const AIPlayerModel* pObserved = pLogicEngine->GetObservedModel();

	String strInfo = GetPlayerModelInfo(pObserved);

	if (pInfoBox->getText() != strInfo)
		pInfoBox->setText(strInfo);
}

String BfPGUI::GetPlayerModelInfo(const AIPlayerModel* pModel)
{
	assert(pModel);

	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	std::stringstream sout;

	sout << "Type: " << pModel->GetType();
	sout << endl << "Purchase Probabilities: ";
	const StringRealMap& rPurchaseProbs = pModel->GetPurchaseProbs();
	for (StringRealMap::const_iterator it = rPurchaseProbs.begin();
		 it != rPurchaseProbs.end(); ++it)
	{
		sout << endl << "  " << it->first << ": "
			 << (unsigned)(it->second*100.0) << "%";
	}
	sout << endl << "  (None): "
		 << (unsigned)(pModel->GetSaveProb()*100.0) << "%";
	sout << endl << "Purchase Learning Rate: "
		 << (unsigned)(pModel->GetPurchaseLRate()*100.0) << "%";

	const StringVector& rSectors = pScenarioConf->GetSectorTypes();

	for (StringVector::const_iterator itFrom = rSectors.begin();
		 itFrom != rSectors.end(); ++itFrom)
	{
		const SectorType* pSector = pScenarioConf->GetSectorType(*itFrom);
		const StringVector& rLinked = pSector->GetLinkedSectors();
		sout << endl << "Move Probabilities: ";
		for (StringVector::const_iterator itTo = rLinked.begin();
			 itTo != rLinked.end(); ++itTo)
		{
			const Real& rProb = pModel->GetSectorLinkProb(*itFrom, *itTo);
			sout << endl << "  " << *itFrom << "-" << *itTo << ": "
				 << (unsigned)(rProb*100.0) << "%";
		}

	}

	sout << endl << "Move Learning Rate: "
		 << (unsigned)(pModel->GetMoveLRate()*100.0) << "%";

	return sout.str();
}


void BfPGUI::UpdateUnitSelectMenu(CEGUI::TabControl* pUnitSelectMenu)
{
	assert(m_iCamMode == CM_LOGICENGINE);

	CEGUI::Window* pTabPane = pUnitSelectMenu->getChildAtIdx(0);
	CEGUI::Window* pTab = pTabPane->getChild("UnitSelectMenu/Factions");

	CEGUI::Listbox* pList  = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateFactionList(pList);

	pTab = pTabPane->getChild("UnitSelectMenu/Units");
	pList = (CEGUI::Listbox*)pTab->getChild("List");
	UpdateUnitList(pList);
}

void BfPGUI::UpdateFactionList(CEGUI::Listbox* pList)
{
	// Get info on the current sector.
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();


	const StringVector& rFactions = pScenarioConf->GetFactionNames();

	// We need to highlight the faction that's currently selected.
	Vector3 vecColour;
	if (pList->getItemCount() > 0) pList->resetList();
	for (StringVector::const_iterator it = rFactions.begin();
		 it != rFactions.end(); ++it)
	{
		bool bSelected = (*it == m_strSelectedFaction);
		std::stringstream sout;
		if (bSelected)
			sout << "*" << *it << "*";
		else
			sout << *it;

		// Set the text colour based on whether it is selected or not.
		const FactionType* pFaction = pScenarioConf->GetFactionType(*it);
		vecColour = bSelected ?
			pFaction->GetSelectedColour() :
			pFaction->GetUnselectedColour();

		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());
		pItem->setTextColours(
			CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );
		pList->addItem(pItem);
	}
}

void BfPGUI::UpdateUnitList(CEGUI::Listbox* pList)
{
	// TODO: List of all known units for m_strSelectedFaction that exist in m_iSelectedTick
	const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
	const AbstractBfPState* pBfPState = AITestLogicEngine::GetInstance().GetState();
	std::vector<const AbstractUnitState*> vUnits =
		pBfPState->GetFactionUnits(m_strSelectedFaction, m_iSelectedTick);
	std::vector<unsigned>& rUnitList = m_mFactionUnits[m_strSelectedFaction];
	unsigned* pSelectedUnit = m_mSelectedUnits[m_strSelectedFaction];
	rUnitList.clear();

	Vector3 vecColour;
	if (pList->getItemCount() > 0) pList->resetList();
	for (std::vector<const AbstractUnitState*>::const_iterator it = vUnits.begin();
		 it != vUnits.end(); ++it)
	{
		std::stringstream sout;
		const AbstractUnitState* pUnit = *it;
		rUnitList.push_back(pUnit->GetUnitID());

		bool bSelected = (pSelectedUnit &&
			pUnit->GetUnitID() == *pSelectedUnit);
		bool bKnown = !pUnit->GetHypothetical();

		if (bSelected) sout << "*";
		sout << "Unit " << pUnit->GetUnitID();
		if (bKnown) sout << ": " << pUnit->GetKnownType();
		if (bSelected) sout << "*";

		// Determine the colour (use faction colours or greys for hypothetical units)

		if (bKnown)
		{
			const FactionType* pFaction = pScenarioConf->GetFactionType(pUnit->GetFaction());
			vecColour = bSelected ?
				pFaction->GetSelectedColour() :
				pFaction->GetUnselectedColour();
		}
		else
		{
			vecColour = bSelected ?
				Vector3(0.9,0.9,0.9) :
				Vector3(0.5,0.5,0.5);
		}

		CEGUI::ListboxTextItem* pItem = new CEGUI::ListboxTextItem(sout.str());
		pItem->setTextColours(
			CEGUI::Colour(vecColour.x, vecColour.y, vecColour.z) );
		pList->addItem(pItem);
	}
}

void BfPGUI::UpdateTickMenu(CEGUI::Window* pWindow)
{
	// The tick menu shows the latest tick, the selected tick, and lets the user pick a tick to observe.
	const AbstractBfPState* pBfPState = AITestLogicEngine::GetInstance().GetState();
	unsigned long iLatestTick = pBfPState->GetAgeTicks();

	std::stringstream sout;
	sout << m_iSelectedTick;
	String strSelectedTick = sout.str();
	sout.str(""); sout.clear();
	sout << iLatestTick;
	String strLatestTick = sout.str();

	// Update the labels for current tick and latest tick
	CEGUI::Window* pText = pWindow->getChild("borderBL/TickMenu/CurrentTickValue");
	if ( pText->getText() != strSelectedTick )
		pText->setText(strSelectedTick);

	pText = pWindow->getChild("borderBL/TickMenu/LatestTickValue");
	if ( pText->getText() != strLatestTick )
		pText->setText(strLatestTick);

	// Update the tick slider
	CEGUI::Scrollbar* pTickSlider = (CEGUI::Scrollbar*)
		pWindow->getChild("borderBL/TickMenu/TickSlider");
	unsigned iNewNoTicks = pBfPState->GetAgeTicks() + 1;
	if (iNewNoTicks != m_iLastNoTicks) // Tick changed
	{
		m_iLastNoTicks = iNewNoTicks;
		++m_iSelectedTick;
		pTickSlider->setDocumentSize( (Real)iNewNoTicks );
		Real fPageSize = (Real)iNewNoTicks/10.0;
		if (fPageSize < 1) fPageSize = 1;
		pTickSlider->setPageSize(fPageSize);
		pTickSlider->setStepSize(1);
		pTickSlider->setScrollPosition(m_iSelectedTick);
	}
	else // Moved the slider
		m_iSelectedTick = (unsigned)pTickSlider->getScrollPosition();

}

#endif // BFP_NO_CEGUI
