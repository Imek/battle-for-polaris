#ifndef AILOGICENGINE_H_INCLUDED
#define AILOGICENGINE_H_INCLUDED

// Engine imports
#include <Util/Singleton.h>
#include <AI/BayesianNetwork.h>

// Game-specific imports
#include <Control/BfPControlState.h>
#include <CommandAI/AIAbstractState.h>
#include <CommandAI/AIPlayerModel.h>
#include <CommandAI/SectorBayesianNet.h>

/**
 * @brief Does the updates of probability values whenever the state is changed.
 *
 * The logic engine is the class that handles all logical calculation required to make estimations
 * on the abstract state. Without the logic engine, the abstract state is just a simplified
 * version of the concrete game state. Probabilistic estimations are made based on the actions
 * of other players (movemement, deployment and purchase of ships) over time using a player model.
 *
 * @author Joe Forster
 */
class AILogicEngine
{
public:

	/**
	 * @brief A constructor for the logic engine (of a type given in the config)
	 */
	AILogicEngine(String strType, String strInitialModel);

	/**
	 * @brief Destructor for the logic engine
	 */
	virtual ~AILogicEngine();

	/**
	 * @brief Get the player model currently being used for estimations.
	 */
	const AIPlayerModel* GetPlayerModel() const;

	/**
	 * @brief Get the observed model being constructed to update our player model.
	 */
	const AIPlayerModel* GetObservedModel() const;

	/**
	 * @brief Update an abstract state using information from a new state.
	 */
	void UpdateAbstractState(
		const AbstractBfPState* pOldState, AbstractBfPState* pCurrentState);

protected:
	typedef std::map<unsigned long, bool>		SpendOrSaveTally;
	typedef std::map<String, SpendOrSaveTally>   FactionSSTallies;

	// This is the player model being used for estimations for this update.
	AIPlayerModel*		  m_pPlayerModel;
	// We put observed information into this "model" for when we do the learning update at the end.
	AIPlayerModel*		  m_pObservedModel;
	// TODO: We also need to keep a tally of when each faction purchased or saved their money in each tick to apply to the player model.
	FactionSSTallies		m_pFactionSSTallies;

	// Provided to UpdateAbstractState
	const AbstractBfPState* m_pOldState;
	AbstractBfPState*	   m_pCurrentState;

private:
	String		  m_strType;

	Real			m_fInjectionsPerTick;

	/**
	 * @brief Perform estimations of faction RPs by income.
	 */
	virtual void EstimateIncome(const String& rFaction);

	/**
	 * @brief Perform estimations regarding use of resources (purchasing of ships)
	 */
	virtual void EstimateResources(const String& rFaction);

	/**
	 * @brief Add a new hypothetical ship into the system.
	 */
	virtual void AddHypotheticalShip(const String& rFaction, const Real& rExistence,
		const StringRealMap& rType, const UIntRealMap& rCount, const StringRealMap& rLocation);

	/**
	 * @brief alter min/est/max RP values based on a possibly purchased ship
	 */
	virtual void AccountForRPCost(const String& rFaction,
		const StringRealMap& rType);

	/**
	 * @brief Perform estimations regarding abstract unit states that aren't hypothetical
	 */
	virtual void EstimateKnownShips(const String& rFaction);

	/**
	 * @brief Calculates probabilities, based on the player model, for the past movement and origin point of a newly-spotted ship.
	 */
	virtual void InferShipPastMovement(const String& rFaction, unsigned iUnitID);

	/**
	 *  @brief This function changes estimations for subsequent events for a newly-spotted ship whose past movement has been estimated.
	 */
	virtual void PropagateInferredHistory(const String& rFaction, unsigned iUnitID);

	/**
	 * @brief Perform estimations regarding hypothetical abstract unit states.
	 */
	virtual void EstimateUnknownShips(const String& rFaction);

	/**
	 * @brief Implement this to estimate movement of a no-longer-seen ship based on the player model.
	 */
	virtual void EstimateShipMovement(const String& rFaction, unsigned iUnitID);
};

/**
 * @brief A factory class for logic engines - a type and initial player model from the config are required.
 *
 * @author Joe Forster
 */
struct AILogicEngineFactory
{
	/**
	 * @brief Make a logic engine of a given type and initial model
	 */
	static AILogicEngine* MakeLogicEngine(String strType, String strInitialModel);

};

/**
 * @brief A singleton class to hold an instance of AILogicEngine for testing
 *
 * @author Joe Forster
 */
class AITestLogicEngine : public JFUtil::Singleton<AITestLogicEngine>
{
public:
	/**
	 * @brief Constructor for AITestLogicEngine, which creates the abstract state from a visible state.
	 */
	AITestLogicEngine(const VisibleState* pState);

	/**
	 * @brief We've implemented this copy constructor so we can throw an exception if you try to copy one.
	 */
	AITestLogicEngine(const AITestLogicEngine& rObj);

	/**
	 * @brief Destructor destroys our abstract state and logic engine.
	 */
	~AITestLogicEngine();

	// Get functions for our members
	const AILogicEngine* GetEngine() const { return m_pEngine; }
	AILogicEngine* GetEngine() { return m_pEngine; }
	const AbstractBfPState* GetState() const { return m_pState; }

	/**
	 * @brief Update our abstract state with a new visible state (for the player)
	 */
	void UpdateState(const VisibleState* pVisibleState);

private:
	AbstractBfPState*   m_pState;
	AILogicEngine*	  m_pEngine;
};

#endif // AILOGICENGINE_H_INCLUDED
