#ifndef AIPLAYERMODEL_H_INCLUDED
#define AIPLAYERMODEL_H_INCLUDED

#include <Config/BfPConfig.h>



/**
 * @brief A class representing a model of an opposing player's behaviour, for use by the logic engine.
 *
 * @author Joe Forster
 */
class AIPlayerModel : public BaseConfig
{
public:

	/**
	 * @brief When we provide an initial model, the constructor gets common values from ai.cfg.
	 */
	AIPlayerModel(String strType, String strInitialModel);

	/**
	 * @brief When no initial model is provided, all probabilities are initialised to 0.
	 */
	AIPlayerModel(String strType);

	const String& GetType() const { return m_strType; }

	/**
	 * @brief Returns true if the currently-set values are valid.
	 */
	virtual bool Validate() const;

	/**
	 * @brief Updates the probabilities based on those in an observed model.
	 */
	virtual void DoLearningUpdate(const AIPlayerModel* pObserved);

	/**
	 * @brief Reset all the probabilities to zero - generally used for the observed model.
	 */
	virtual void Reset();

	// Get functions
	const StringRealMap& GetPurchaseProbs() const { return m_mPurchaseProbs; }
	const Real& GetPurchaseProb(const String& rShipType) const;
	const Real& GetSaveProb() const;
	const Real& GetPurchaseLRate() const;

	unsigned GetMinGroupSize(const String& rShipType) const;
	unsigned GetMaxGroupSize(const String& rShipType) const;
	const Real& GetIdealGroupSize(const String& rShipType) const;
	const Real& GetGroupSizeLRate() const;

	/**
	 * @brief Estimate a group purchase probability based on minimum/maximum/ideal sizes
	 *
	 * Returns 0 if less than minimum or greater than maximum. Otherwise does a linear
	 * estimation based on distance from ideal.
	 */
	Real EstimatePurchaseProb(const String& rShipType, unsigned iGroupSize);

	const Real& GetMoveLRate() const;

	/**
	 * @brief Get the probability of moving from rFrom to rTo.
	 *
	 * For every sector rFrom, there should be a probability for each of its linked sectors rTo.
	 * the probability of not moving from rFrom should be the remainder left over after summing
	 * the move probabilities.
	 */
	const Real& GetSectorLinkProb(const String& rFrom, const String& rTo) const;

private:
	String			  m_strType;

	// Resource management values
	// Map of ship type to relative preference
	StringRealMap	   m_mPurchaseProbs;

	// Relative preference for saving rather than purchasing
	Real				m_fSaveProb;
	// Learning rate for purchase probabilities
	Real				m_fPurchaseLRate;

	// Map of ship type to min/max/ideal group size
	StringUIntMap	   m_mMinGroupSizes;
	StringUIntMap	   m_mMaxGroupSizes;
	StringRealMap	   m_mIdealGroupSizes;
	// Learning rate for group sizes
	Real				m_fGroupSizeLRate;

	// Ship movement values
	Real				m_fMoveLRate;
	typedef std::map< std::pair<String, String>, Real > SectorLinkProbs;
	SectorLinkProbs	 m_mMoveProbs;

};



#endif // AIPLAYERMODEL_H_INCLUDED
