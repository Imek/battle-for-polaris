#ifndef AIPLAYERBASIC_H_INCLUDED
#define AIPLAYERBASIC_H_INCLUDED

#include <CommandAI/AIPlayerBase.h>

/**
 * @brief An AIPlayer class that makes basic use of action selection using a distribution over objectives.
 *
 * @author Joe Forster
 */
class AIPlayerBasic : public AIPlayer
{
public:
	/**
	 * @brief Construct a basic AI player with a specific type and controller
	 */
	AIPlayerBasic(const String& rType, FactionController* pControls);

	/**
	 * @brief Destructor for the basic player.
	 */
	virtual ~AIPlayerBasic();

	// Implementations of AIPlayer update functions.
	virtual void Update(unsigned long iTime);
	virtual void DoTickUpdate();

private:
	/**
	 * @brief Update the objective values based on the new abstract state.
	 */
	void UpdateObjectives();

	/**
	 * @brief Validate m_mObjectives, throwing an exception if invalid.
	 */
	void ValidateObjectives() const;

	// A map of objective to proportion (which add to 1)
	StringRealMap   m_mObjectives;

};



#endif // AIPLAYERBASIC_H_INCLUDED
