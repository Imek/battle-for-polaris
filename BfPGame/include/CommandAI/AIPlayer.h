#ifndef AIPLAYER_H_INCLUDED
#define AIPLAYER_H_INCLUDED

#include <CommandAI/AIPlayerBasic.h>

/**
 * @brief A factory class for AIPlayers of all subtypes defined in the configs.
 *
 * @author Joe Forster
 */
struct AIPlayerFactory
{
	static AIPlayer* MakePlayer(String strType, FactionController* pControls)
	{
		const AIConfig* pAIConf = (const AIConfig*)
			GameConfig::GetInstance().GetConfig("AI");
		const AIPlayerType* pType = pAIConf->GetAIPlayerType(strType);
		String strClass = pType->GetClass();

		AIPlayer* pPlayer = NULL;

		if (strClass == "Basic")
			pPlayer = new AIPlayerBasic(strType, pControls);
		else
		{
			std::stringstream sout;
			sout << "Invalid player class given: " << strClass << " for type ";
			sout << strType << "; bad config entry?";
			throw JFUtil::Exception("AIPlayerFactory", "MakePlayer", sout.str());
		}

		assert(pPlayer);
		return pPlayer;
	}
};

#endif // AIPLAYER_H_INCLUDED
