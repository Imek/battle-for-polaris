#ifndef AIMANAGERBASE_H_INCLUDED
#define AIMANAGERBASE_H_INCLUDED

#include <CommandAI/AIAction.h>
#include <CommandAI/AIAbstractState.h>
#include <CommandAI/AIEvaluator.h>

/**
 * @brief A further abstraction of the abstract game state, for use by a sub-player.
 *
 * @author Joe Forster
 */
struct AIManagerState
{
	AIManagerState() {}

	virtual ~AIManagerState() {}

	/**
	 * @brief Set this manager's contents to match an abstract state.
	 */
	virtual void FromState(const AbstractBfPState* pAbsState) = 0;

	/**
	 * @brief Get possible actions for this state and relevant to its manager.
	 *
	 * @param pType The type of action, or NULL to get all possible actions.
	 */
	virtual std::vector<AIAction*> GetPossibleActions(AA_TYPE* pType = NULL) const = 0;
};


/**
 * @brief Represents a sub-player within an AIPlayer, which handles a particular subsection of the abstracted game.
 *
 * @author Joe Forster
 */
class AIManager
{
public:

	/**
	 * @brief Constructor for the manager base class.
	 *
	 * @param strType	   The type of the subclass being used.
	 * @param pEvaluator	The AIEvaluator to use for decision making.
	 */
	AIManager(String strType, const AIEvaluator* pEvaluator)
	: m_pEvaluator(pEvaluator)
	, m_pManagerState(NULL)
	, m_strType(strType)
	{
		assert(m_pEvaluator);

		// TODO: Construct policy based on strPolicyType (Factory for AIManagerPolicy and AIManagerState)
	}

	/**
	 * @brief Destructor for AIManager destroys the state being used.
	 */
	virtual ~AIManager()
	{
		delete m_pManagerState;
	}

	/**
	 * @brief The update function looks at the latest state and decides what abstract actions to perform.
	 */
	virtual void ChooseActions(const AbstractBfPState* pNewState, ActionQueue& rActionQueue) = 0;

	/**
	 * @brief Get the type of manager.
	 */
	const String& GetType() const { return m_strType; }

protected:
	const AIEvaluator*  m_pEvaluator;

	AIManagerState*	 m_pManagerState;

	String			  m_strType;
};


#endif // AIMANAGERBASE_H_INCLUDED
