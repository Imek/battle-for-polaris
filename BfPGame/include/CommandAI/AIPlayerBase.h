#ifndef AIPLAYERBASE_H_INCLUDED
#define AIPLAYERBASE_H_INCLUDED

#include <BfPGameDefs.h>
#include <State/BfPState.h>
#include <CommandAI/AIManager.h>
#include <CommandAI/AILogicEngine.h>
#include <CommandAI/AIEvaluator.h>

/**
 * @brief Represents a single high-level AI player, and converts abstract actions to real ones.
 *
 * This is a base class, defining what a player should be able to do at minimum. It can be used,
 * but a subclass needs to actually do the required updating and action choice.
 *
 * @author Joe Forster
 */
class AIPlayer
{
public:
	/**
	 * @brief Constructor for AIPlayer with a type and a controller.
	 *
	 * The given controller provides the interface through which the player observes and
	 * performs actions with the same capability as a human player does through the GUI.
	 */
	AIPlayer(const String& rType, FactionController* pControls);

	/**
	 * @brief Destructor needs to free up all of the managed memory for this class instance.
	 */
	virtual ~AIPlayer();

	/**
	 * @brief The update function for an AIPlayer to be called in the main control update cycle.
	 *
	 * Any derived function should call this base function before doing its own stuff.
	 */
	virtual void Update(unsigned long iTime);

	/**
	 * @brief The update function called at the start of every AI tick.
	 *
	 * Any derived function should call this base function before doing its own stuff.
	 */
	virtual void DoTickUpdate();

	/**
	 * @brief Convert the queue of abstract actions to real actions, changing the game state.
	 *
	 * As the actions are already sorted by value, the highest ones are executed and any that conflict
	 * with higher-valued actions are discarded.
	 */
	virtual void ExecuteActions();

	/**
	 * @brief Clear the action queue, generally done at the end of each tick.
	 */
	void ClearActionQueue();

	/**
	 * @brief Get the name of the faction that this player controls
	 */
	const String& GetFactionName() const { return m_pControls->GetFactionName(); }

	/**
	 * @brief Get the name of the AI player type.
	 */
	const String& GetTypeName() const { return m_strType; }

	/**
	 * @brief Get the AI player type.
	 */
	const AIPlayerType* GetType() const;

	/**
	 * @brief Get the FactionController object used as an interface by this AI player.
	 */
	FactionController* GetController() { return m_pControls; }

protected:
	String					  m_strType;

	// Our interface with the main game via the control system.
	FactionController*		  m_pControls;

	AbstractBfPState*		   m_pPrevState;
	AILogicEngine*			  m_pLogicEngine;
	AIEvaluator*				m_pEvaluator;
	std::vector<AIManager*>	 m_vManagers;

	// An ActionQueue is an ordered map of action value to action, from highest value to lowest.
	ActionQueue				 m_mActionQueue;



};

#endif // AIPLAYERBASE_H_INCLUDED
