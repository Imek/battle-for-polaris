#ifndef AIEVALUATORPRESET_H_INCLUDED
#define AIEVALUATORPRESET_H_INCLUDED

#include <CommandAI/AIEvaluatorBase.h>
#include <Config/BfPConfig.h>

/**
 * @brief A type of evaluator that uses pre-set values from a table in a configuration file.
 *
 * @author Joe Forster
 */
class AIPresetEvaluator : public AIEvaluator, public BaseConfig
{
public:
	AIPresetEvaluator(const String& rType, const String& rInitialConfig);

	/**
	 * @brief GetShipValue's implementation for the preset evaluator just looks up the value in the table.
	 */
	Real GetShipValue(const String& rOfType, const String& rAgainstType) const;

	// Our implementations of min/max/ideal group sizes also look up the values from the config.
	unsigned GetMinGroupSize(const String& rShipType) const;
	unsigned GetMaxGroupSize(const String& rShipType) const;
	const Real& GetIdealGroupSize(const String& rShipType) const;

	/**
	 * @brief Implementation of the Validate function from BaseConfig
	 */
	bool Validate() const;

private:
	// Map of < ofShip, againstShip > -> value for ofShip
	std::map< pair<String, String>, Real >	m_mShipValues;

	// Map of ship type to min/max/ideal group size
	StringUIntMap	   m_mMinGroupSizes;
	StringUIntMap	   m_mMaxGroupSizes;
	StringRealMap	   m_mIdealGroupSizes;

};

#endif // AIEVALUATORPRESET_H_INCLUDED
