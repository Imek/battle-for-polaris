#ifndef AIMANAGERPRESET_H_INCLUDED
#define AIMANAGERPRESET_H_INCLUDED

#include <CommandAI/AIManagerBase.h>


/**
 * @brief A Manager state that just contains a full abstract state.
 *
 * This is for use in cases where it isn't a problem to use and store the full thing,
 * for example for the pre-set/static AI player that doesn't do any high-level learning.
 *
 * @author Joe Forster
 */
class AIFullManagerState : public AIManagerState
{
public:
	AIFullManagerState(const AbstractBfPState* pAbsState);

	virtual ~AIFullManagerState() { delete m_pAbsState; }

	/**
	 * @brief Set this manager's contents to match an abstract state.
	 */
	void FromState(const AbstractBfPState* pAbsState);

	/**
	 * @brief Get possible actions for this state and relevant to its manager.
	 */
	std::vector<AIAction*> GetPossibleActions(AA_TYPE* pType = NULL) const;

	/**
	 * @brief Access the AbstractBfPState contained within.
	 */
	const AbstractBfPState* GetState() const { return m_pAbsState; }

private:
	void GetPossibleMoveActions(std::vector<AIAction*>& rActions) const;
	void GetPossiblePurchaseActions(std::vector<AIAction*>& rActions) const;
	void GetPossibleDeployActions(std::vector<AIAction*>& rActions) const;


	AbstractBfPState* m_pAbsState;

};

/**
 * @brief A manager (AI sub-player) for the basic preset AI player.
 *
 * This manager makes use of the Objectives system, whereby action values are weighted
 * depending on the current strategic focus of the player. For the preset player,
 * these values are calculated from simple observations of the game state.
 *
 * @author Joe Forster
 */
class AIPresetManager : public AIManager
{
public:
	AIPresetManager(String strType, const AIEvaluator* pEvaluator)
	: AIManager(strType, pEvaluator)
	{
		assert(pEvaluator);
	}

	void SetObjectives(const StringRealMap& rObjs)
	{
		m_mObjectives = rObjs;
	}

	const AIFullManagerState* GetManagerState() const
	{
		return (const AIFullManagerState*)m_pManagerState;
	}

protected:
	StringRealMap   m_mObjectives;
};


/**
 * @brief The preset resource manager uses pre-set values to evaluate and choose purchase and deploy actions.
 *
 * @author Joe Forster
 */
class AIPresetResourceManager : public AIPresetManager
{
public:
	AIPresetResourceManager(const AIEvaluator* pEvaluator)
	: AIPresetManager("PresetResourceManager", pEvaluator)
	{
	}

	void ChooseActions(const AbstractBfPState* pNewState, ActionQueue& rActionQueue);

private:
	/**
	 * @brief Choose purchase actions by evaluating them and buying what can be afforded
	 */
	void ChoosePurchaseActions(ActionQueue& rActionQueue);

	/**
	 * @brief Choose deploy actions by evaluating them and buying what can be afforded
	 */
	void ChooseDeployActions(ActionQueue& rActionQueue);

	/**
	 * @brief Produce a value for a purchase action based on the abstract state and our ship values.
	 */
	int EvaluatePurchaseAction(AIPurchaseAction* pAction) const;

	// Get a purchase action value for each objective type
	int EvaluatePurchaseActionForAttack(AIPurchaseAction* pAction) const;
	int EvaluatePurchaseActionForDefend(AIPurchaseAction* pAction) const;
	int EvaluatePurchaseActionForExplore(AIPurchaseAction* pAction) const;

	/**
	 * @brief Produce a value for a deploy action based on the abstract state and our ship values.
	 */
	int EvaluateDeployAction(AIDeployAction* pAction) const;

	// Get a deploy action value for each objective type
	int EvaluateDeployActionForAttack(AIDeployAction* pAction) const;
	int EvaluateDeployActionForDefend(AIDeployAction* pAction) const;
	int EvaluateDeployActionForExplore(AIDeployAction* pAction) const;

};

/**
 * @brief The preset resource manager uses pre-set values to evaluate and choose unit movement actions.
 *
 * @author Joe Forster
 */
class AIPresetShipManager : public AIPresetManager
{
public:
	/**
	 * @brief Constructor for AIPresetShipManager
	 */
	AIPresetShipManager(const AIEvaluator* pEvaluator)
	: AIPresetManager("PresetShipManager", pEvaluator)
	{
	}

	/**
	 * @brief Choose actions for a tick (the latest in pNewState) and add them to rActionQueue.
	 */
	void ChooseActions(const AbstractBfPState* pNewState, ActionQueue& rActionQueue);

private:
	/**
	 * @brief Produce a value for a move action based on the abstract state and our ship values.
	 */
	int EvaluateMoveAction(AIMoveAction* pAction) const;

	// Get the value for each objective type
	int EvaluateMoveActionForAttack(AIMoveAction* pAction) const;
	int EvaluateMoveActionForDefend(AIMoveAction* pAction) const;
	int EvaluateMoveActionForExplore(AIMoveAction* pAction) const;

};


#endif // AIMANAGERPRESET_H_INCLUDED
