#ifndef AIEVALUATOR_H_INCLUDED
#define AIEVALUATOR_H_INCLUDED

#include <CommandAI/AIEvaluatorPreset.h>

/**
 * @brief Factory class for constructing an evaluator of a given type from the AI config.
 *
 * @author Joe Forster
 */
struct AIEvaluatorFactory
{
	/**
	 * @brief Make an AIEvaluator object of a given type and initial state.
	 */
	static AIEvaluator* MakeEvaluator(const String& rType, const String& rInitialConfig)
	{
		AIEvaluator* pEvaluator = NULL;

		if (rType == "Preset")
			pEvaluator = new AIPresetEvaluator(rType, rInitialConfig);
		else
		{
			std::stringstream sout;
			sout << "Invalid evaluator type given: " << rType << "; bad config entry?";
			throw JFUtil::Exception("AIEvaluatorFactory", "MakeEvaluator", sout.str());
		}

		assert(pEvaluator);
		return pEvaluator;
	}
};

#endif // AIEVALUATOR_H_INCLUDED
