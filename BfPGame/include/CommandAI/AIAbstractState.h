#ifndef AIABSTRACTSTATE_H_INCLUDED
#define AIABSTRACTSTATE_H_INCLUDED

#include <BfPGameDefs.h>
#include <State/BfPState.h>
#include <Control/BfPControlFaction.h>
#include <Control/BfPEvents.h>

#include <CommandAI/AIAbstractStateUnit.h>

using namespace std;

/**
 * @brief This class represents the saved state of a faction at a given tick in history.
 *
 * The intention is for the logic engine to be able to check and update these values.
 * It is also possible to view these historical values in the AI debug GUI (though said
 * GUI is at best experimental by this point).
 *
 * @author Joe Forster
 */
class FactionHistoryEntry
{
public:
	FactionHistoryEntry(unsigned long iTick, long iScore,
		long iMinRPs, long iMaxRPs, long iEstRPs);

	// Get the tick for this entry
	const unsigned long& GetTick() const { return m_iTick; }
	// Get the various pieces of data stored for an entry
	const long& GetScore() const { return m_iScore; }
	const long& GetMinRPs() const { return m_iMinRPs; }
	const long& GetMaxRPs() const { return m_iMaxRPs; }
	const long& GetEstRPs() const { return m_iEstRPs; }

	/**
	 * @brief Using the minimum, maximum and estimated RP values, make an estimation of how likely the faction is to be able to afford a given amount at a this tick.
	 */
	Real GetProbabilityOfHaving(const long& iAmount) const;

	/**
	 * @brief Update the values in this entry with new estimations.
	 */
	void Update(const long& iScore,
		const long& rMinRPs, const long& rMaxRPs, const long& rEstRPs);

	/**
	 * @brief Update the values in this entry with new values for our own faction.
	 */
	void Update(const long& iScore, const long& rKnownRPs);

	/**
	 * @brief Validate this entry, throwing an exception if invalid.
	 */
	void Validate() const;

private:
	unsigned long m_iTick;
	long m_iScore;
	long m_iMinRPs;
	long m_iMaxRPs;
	long m_iEstRPs;
};

typedef std::map<unsigned long, FactionHistoryEntry*> FactionHistory;

/**
 * @brief The minimum abstraction of the game's faction state.
 *
 * This is pretty much the same as FactionState, except that we make estimations of an
 * opposing faction's RPs with a simple function approximation.
 *
 * @author Joe Forster
 */
class AbstractFactionState
{
public:
	/**
	 * @brief Initial (tick 0!) state)
	 */
	AbstractFactionState(String strFaction);

	/**
	 * @brief Copy constructor.
	 */
	AbstractFactionState(const AbstractFactionState& rFaction);

	/**
	 * @brief Destructor
	 */
	~AbstractFactionState()
	{
		for (FactionHistory::iterator it = m_mHistory.begin();
			 it != m_mHistory.end(); ++it)
		{
			delete it->second;
		}
	}

	/**
	 * @brief A function to update this abstract factions state from a real one (i.e. the player's state)
	 */
	void UpdateFromFactionState(const FactionState* pState, unsigned long iTick);

	// The following functions access information from within the FactionHistoryEntry for a given tick.

	const long& GetMinRPs(unsigned long iTick) const;
	const long& GetMinRPs() const;

	const long& GetMaxRPs(unsigned long iTick) const;
	const long& GetMaxRPs() const;

	const long& GetEstimatedRPs(unsigned long iTick) const;
	const long& GetEstimatedRPs() const;

	const long& GetScore(unsigned long iTick) const;
	const long& GetScore() const;

	Real GetProbabilityOfHaving(unsigned long iTick, const long& rRPs) const;
	Real GetProbabilityOfHaving(const long& rRPs) const;

	/**
	 * @brief Add or update a FactionHistoryEntry for an opposing (non-visible) faction state.
	 *
	 * @param iTick	 The tick for this entry (could exist)
	 * @param rScore	The known score of the faction
	 * @param rMinRPs   The minimum possible RPs this faction could have, given all possibilities
	 * @param rMaxRPs   The maximum possible RPs this faction could have, given all possibilities
	 * @param rEstRPs   An estimation of how many RPs this faction is likely to have
	 */
	void EstimateValues(unsigned long iTick, const long& rScore,
		const long& rMinRPs, const long& rMaxRPs, const long& rEstRPs);


	/**
	 * @brief Add or update a FactionHistoryEntry for a friendly (visible) faction state.
	 *
	 * @param iTick	 The tick for this entry (could exist)
	 * @param rScore	The known score of the faction
	 * @param rKnownRPs The known RP count for the faction
	 */
	void ObserveValues(unsigned long iTick, const long& rScore, const long& rKnownRPs);

	/**
	 * @brief Get the number of the latest tick we have a FactionHistoryEntry for.
	 */
	const unsigned long& GetLatestTick() const { return m_iLatestTick; }

	/**
	 * @brief Get the number of available ships to deploy, assuming that this is our faction.
	 */
	const unsigned& GetNumToDeploy(const String& rShipType) const;

	/**
	 * @brief Add a group to deploy.
	 */
	void AddGroupsToDeploy(const String& rType, unsigned iCount = 1);

	/**
	 * @brief Remove a deployed group (assumes we have it!)
	 */
	void RemoveDeployingGroups(const String& rType, unsigned iCount = 1);

	/**
	 * @brief Validate all values and entries, throwing an exception if invalid.
	 */
	void Validate() const;

private:
	String					  m_strName;

	unsigned long			   m_iLatestTick;
	FactionHistory			  m_mHistory;

	// How many times we could deploy each group type (like in the GUI)
	std::map<String, unsigned>	   m_mPossibleDeploys;

//	/**
//	 * @brief Round RPs to the closest significant value (from AIConfig).
//	 */
//	 void RoundRPs();
};

/**
 * @brief This abstract state class represents a sector, including who we know or think owns it.
 *
 * Note that actual contents of sectors, in terms of ships, are kept as AbstractUnitStates contained within
 * the main AbstractBfPState.
 *
 * @author Joe Forster
 */
class AbstractSectorState
{
public:
	/**
	 * @brief Standard constructor from an initial state (possibly NULL).
	 */
	AbstractSectorState(const BfPSectorState* pSector, const String& rName);

	const String& GetName() const { return m_strName; }
	const SectorType* GetSectorType() const;

	/**
	 * @brief Get whether this sector is currently visible by our faction
	 */
	bool GetVisible() const { return m_bVisible; }

	/**
	 * @brief Set the ownership values of this sector from another abstract state
	 */
	void SetOwnership(const AbstractSectorState* pToCopy);

	/**
	 * @brief Set the sector to non-visible, and set the probability distribution over owning factions
	 */
	void EstimateOwnership(const StringRealMap& rOwnership);

	/**
	 * @brief Set the sector to visible, and set the known owner
	 */
	void ObserveOwnership(const String& rOwner);

	/**
	 * @brief Set the sector to visible, and unset the owner.
	 */
	void ObserveNeutralOwnership();

	/**
	 * Set this sector as non-visible, but keep the current observations.
	 */
	void SetNotVisible();

	/**
	 * @brief Get the probability of the given faction owning this sector.
	 */
	const Real& GetOwnershipForFaction(const String& rFaction) const;

	/**
	 * @brief Get the probability of no faction owning this sector.
	 */
	const Real& GetNeutralOwnership() const;

	/**
	 * @brief Assuming that this sector is observed (visible), get the owner.
	 */
	const String& GetObservedOwner() const;

	/**
	 * @brief Assuming that this sector is observed (visible), get whether the sector has no owner.
	 */
	bool GetIsObservedNeutral() const;

	/**
	 * @brief Validate the values in this data structure, throwing an exception if invalid.
	 */
	void Validate() const;

private:
	String			  m_strName;
	bool				m_bVisible;

	// Map of faction name (and NEUTRAL) to probability (0 - 1.0)
	StringRealMap	   m_mOwnership;
	String			  m_strObservedOwner;
};


typedef std::map<String, AbstractFactionState*> AbsFactionList;
typedef std::map< pair<String, unsigned>, AbstractUnitState*> AbsUnitList;
typedef std::map<String, AbstractSectorState*> AbsSectorList;

/**
 * @brief This is the main full state of the abstracted game, derived from the proper BfPState.
 *
 * An abstract state is defined as being the known and estimated details of the game state, from
 * the point of view of a specific faction at a specific tick (time period) in the game. This class
 * consists of abstract unit, sector and faction states as appropriate.
 *
 * @author Joe Forster
 */
class AbstractBfPState
{
public:
	/**
	 * @brief Construct an abstract state consisting of the known information from a VisibleState
	 */
	AbstractBfPState(const VisibleState* pState);

	/**
	 * @brief Copy constructor for AbstractBfPState
	 */
	AbstractBfPState(const AbstractBfPState& rState);

	/**
	 * @brief Destructor just calls Clear.
	 */
	~AbstractBfPState() { Clear(); }

	/**
	 * @brief Get the age of this state in ticks, which is also the latest tick number.
	 */
	const unsigned long& GetAgeTicks() const { return m_iAgeTicks; }


	/**
	 * @brief Get a const pointer to a given abstract faction state.
	 */
	const AbstractFactionState* GetFaction(const String& rName) const;

	/**
	 * @brief Get a given abstract faction state.
	 */
	AbstractFactionState* GetFaction(const String& rName);

	/**
	 * @brief Get a const pointer to our abstract faction state.
	 */
	const AbstractFactionState* GetMyFactionState() const;

	/**
	 * @brief Get our abstract faction state.
	 */
	AbstractFactionState* GetMyFactionState();


	/**
	 * @brief Get the number of visible units owned by a given faction.
	 */
	unsigned GetFactionVisibleUnitCount(const String& rFaction) const;

	/**
	 * @brief Get the number of visible units owned by a given faction in a given sector.
	 */
	unsigned GetFactionVisibleUnitCount(const String& rFaction, const String& rSector) const;

	/**
	 * @brief Get the number of known units owned by a given faction of a given type
	 */
	unsigned GetFactionKnownUnitTypeCount(const String& rFaction, const String& rType) const;

	/**
	 * @brief Get the number of visible sectors owned by a given faction.
	 */
	unsigned GetFactionVisibleSectorCount(const String& rFaction) const;

	/**
	 * @brief Get the number of possible units  owned by a given faction.
	 */
	unsigned GetFactionEstimatedUnitCount(const String& rFaction) const;

	/**
	 * @brief Get the number of possible units owned by a given faction in a given sector.
	 */
	unsigned GetFactionEstimatedUnitCount(const String& rFaction, const String& rSector) const;

	/**
	 * @brief Get the number of possible sectors owned by a given faction.
	 */
	unsigned GetFactionEstimatedSectorCount(const String& rFaction) const;

	/**
	 * @brief Get a reference to the faction list itself.
	 */
	const AbsFactionList& GetFactions() const { return m_mFactions; }

	/**
	 * @brief Get a reference to the unit list itself.
	 */
	const AbsUnitList& GetUnits() const { return m_mUnits; }

	/**
	 * @brief Get a reference to the sector list itself..
	 */
	const AbsSectorList& GetSectors() const { return m_mSectors; }

	/**
	 * @brief Get a const pointer to a unit with the given faction and ID.
	 */
	const AbstractUnitState* GetUnit(const String& rFaction, unsigned iUnitID) const;

	/**
	 * @brief Get a pointer to a unit with the given faction and ID.
	 */
	AbstractUnitState* GetUnit(const String& rFaction, unsigned iUnitID);

	/**
	 * @brief Get a const pointer to a unit with the given faction and group ID.
	 */
	const AbstractUnitState* GetUnitByGroupID(const String& rFaction, unsigned iGroupID) const;

	/**
	 * @brief Get a pointer to a unit with the given faction and group ID.
	 */
	AbstractUnitState* GetUnitByGroupID(const String& rFaction, unsigned iGroupID);

	/**
	 * @brief Generate a list of const AbstractUnitState pointers owned by the given faction at the given tick.
	 */
	std::vector<const AbstractUnitState*> GetFactionUnits(const String& rFaction, unsigned iTick) const;

	/**
	 * @brief Generate a list of all const AbstractUnitState pointers owned by the given faction
	 */
	std::vector<const AbstractUnitState*> GetCurrentFactionUnits(const String& rFaction) const;

	/**
	 * @brief Generate a list of all const AbstractUnitState pointers owned by the observing faction
	 */
	std::vector<const AbstractUnitState*> GetMyCurrentFactionUnits() const;

	/**
	 * @brief Get the total existence value of enemies and friends in a sector
	 *
	 * @param rFriends a map of sector to amount for friendly ships
	 * @param rEnemies a map of sector to amount for enemy ships
	 * @param rSectors the list of sectors to check
	 * @param iExclude the ID of a group of ours to exclude, or -1 to exclude none.
	 */
	void GetEnemyAndFriendAmounts(
		StringRealMap& rFriends, StringRealMap& rEnemies,
		const StringVector& rSectors, int iExcludeGroupID = -1) const;

	/**
	 * @brief See if there's a non-hypothetical enemy that could be in the given sector.
	 */
	bool KnownEnemyInSector(const String& rSector);

	/**
	 * @brief Get a unit state with the given type probabilities, or NULL if none found.
	 */
	AbstractUnitState* GetHypotheticalUnitOfType(const String& rFaction, const StringRealMap& rTypeProbs);

	/**
	 * @brief Get a const pointer to an abstract sector state.
	 */
	const AbstractSectorState* GetSector(const String& rName) const;

	/**
	 * @brief Get a pointer to an abstract sector state.
	 */
	AbstractSectorState* GetSector(const String& rName);

	/**
	 * @brief Generate a list of const AbstractSectorState pointers (possibly) owned by a given faction.
	 */
	std::vector<const AbstractSectorState*> GetOwnedSectors(const String& rFaction) const;

	/**
	 * @brief Get the observing faction for this abstract state.
	 */
	const String& GetMyFaction() const { return m_strMyFaction; }

	/**
	 * @brief Add a new abstract unit state to the abstract state.
	 */
	void AddUnit(AbstractUnitState* pNewUnit);

	/**
	 * @brief Dispose of and remove every abstract unit/sector/faction state contained within this state.
	 */
	void Clear();

	/**
	 * @brief Set the values in this state using visible data from a VisibleState for our faction.
	 */
	void FromState(const VisibleState* pBfPState);

	/**
	 * @brief Validate the values in this data structure, throwing an exception if invalid.
	 */
	void Validate() const;

	/**
	 * @brief Get the next available Unit ID for a new unit.
	 *
	 * Note that Unit ID is not the same as Group ID! An abstract unit, which is estimated
	 * to exist and not observed, can be created with a unique ID and no group ID set.
	 */
	const unsigned& GetNextUnitID() const { return m_iNextUnitID; }

	/**
	 * @brief Update the information in this state given new information in a state for the next tick.
	 */
	void Update(const VisibleState* pNewState);

private:
	// Number of ticks since the game began.
	unsigned long	   m_iAgeTicks;
	// Next unassigned unit ID.
	unsigned			m_iNextUnitID;

	// All the contents of an AbstractBfPState consist of abstract faction, unit and sector states.
	AbsFactionList	  m_mFactions;
	AbsUnitList		 m_mUnits;
	AbsSectorList	   m_mSectors;

	// The faction from whose point of view this state is
	String			  m_strMyFaction;

	// AI Events (ship destruction, ship warping) that occurred last tick
	// TODO: This may need removing
	//EventHandler*	   m_pEventsLastTick;

};

#endif // AIABSTRACTSTATE_H_INCLUDED
