#ifndef AISHIPEVALUATORBASE_H_INCLUDED
#define AISHIPEVALUATORBASE_H_INCLUDED

#include <BfPGameDefs.h>

#include <Config/BfPConfig.h>
#include <Util/Exception.h>

/**
 * @brief An evaluator produces numerical values of ships and ship groups, for use by the AI.
 *
 * @author Joe Forster
 */
class AIEvaluator
{
public:
	/**
	 * @brief Construct an evaluator from a type in the configuration.
	 */
	AIEvaluator(const String& rAIPlayerType);

	/**
	 * @brief Destroy the evaluator
	 */
	virtual ~AIEvaluator() {}

	/**
	 * @brief Get the AI player type
	 */
	const String& GetPlayerType() const { return m_strPlayerType; }

	/**
	 * @brief Evaluate the effectiveness of a single ship type against another ship type.
	 */
	virtual Real GetShipValue(const String& rOfType, const String& rAgainstType) const = 0;

	// The evaluator also keeps values for min/max/ideal group sizes for each type
	virtual unsigned GetMinGroupSize(const String& rShipType) const = 0;
	virtual unsigned GetMaxGroupSize(const String& rShipType) const = 0;
	virtual const Real& GetIdealGroupSize(const String& rShipType) const = 0;

	/**
	 * @brief Produce a group purchase preference value between 0 and 1
	 *
	 * Should return 0 if the player would never choose a group of that type and size,
	 * 1 if that is an ideal group size for that ship type, or a value in between as
	 * appropriate. By default we do a simple linear function from our Min/Max/Ideal values.
	 */
	virtual Real GetGroupSizePreference(const String& rShipType, unsigned iGroupSize) const;

	/**
	 * @brief Evaluate a group type against another group type by using GetShipValue
	 *		and the health values.
	 *
	 * Our base implementation should usually suffice. It makes use of GetShipValue and
	 * alters the effectiveness of each group by its health.
	 */
	virtual Real GetGroupValue(
		const String& rOfType, unsigned iOfCount, const Real& rOfHealth,
		const String& rAgainstType, unsigned iAgainstCount, const Real& rAgainstHealth) const;



private:
	String m_strPlayerType;

};

#endif // AISHIPEVALUATORBASE_H_INCLUDED
