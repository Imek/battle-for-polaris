#ifndef AIACTION_H_INCLUDED
#define AIACTION_H_INCLUDED

#include <Control/BfPControlFaction.h>

enum AA_TYPE { AAT_MOVE, AAT_PURCHASE, AAT_DEPLOY};

/**
 * @brief An action in the context of a sub-player's abstraction of the game state.
 *
 * This may better be described as an "abstract action", but in terms of the possible
 * user actions in the high-level strategic game there isn't (yet) much difference to what
 * the user can actually do with the GUI.
 *
 * @author Joe Forster
 */
class AIAction
{
public:

	/**
	 * @brief Constructor for the action base class.
	 *
	 * @param iType		 The type of action from AA_TYPE.
	 * @param strParameter1 The first action parameter (could be blank depending on the type)
	 * @param strParameter2 The second action parameter (could be blank depending on the type)
	 * @param strParameter2 The third action parameter (could be blank depending on the type)
	 */
	AIAction(AA_TYPE iType, String strParameter1 = "", String strParameter2 = "", String strParameter3 = "")
	: m_iType(iType)
	, m_strParameter1(strParameter1)
	, m_strParameter2(strParameter2)
	, m_strParameter3(strParameter3)
	{}

	/**
	 * @brief Virtual destructor for the action base class
	 */
	virtual ~AIAction() {}

	/**
	 * @brief Get the action type.
	 */
	AA_TYPE GetType() const { return m_iType; }

	/**
	 * @brief This function sees whether the abstract action can be executed.
	 */
	virtual bool CanExecute(const FactionController* pController) const = 0;

	/**
	 * @brief This function affects the abstract action on the game using the faction's controller.
	 *
	 * The implementation can assume that CanExecute is true (and this the action must execute successfully!),
	 * giving an error otherwise.
	 */
	virtual void Execute(FactionController* pController) const = 0;

	/**
	 * @brief Serialises this action into string form for storage.
	 *
	 * TODO: When we actually need serialisation, we will want the GetString function to be this one
	 * (TypeID-str1-str2-str3) for all actions, and a Factory class to put them back together.)
	 * Maybe better to use a String instead of an enum for the Type member?
	 */
	virtual String GetString() const = 0;

protected:
	// Getter methods for parameters (Note that implementing classes
	// should provide more appropriate public getters for parameters)
	const String& GetParameter1() const { return m_strParameter1; }
	const String& GetParameter2() const { return m_strParameter2; }
	const String& GetParameter3() const { return m_strParameter3; }

private:
	// Members are just the type and the three parameters.
	AA_TYPE	 m_iType;
	String	  m_strParameter1;
	String	  m_strParameter2;
	String	  m_strParameter3;

};

// For use by managers and the AI player to order queues in descending order of priority
typedef std::multimap< int, AIAction*, greater<int> > ActionQueue;

/**
 * @brief An action type for moving an existing ship group.
 *
 * This is achieved in the GUI by selecting a ship group and then right-clicking
 * a destination, or selecting a destination and then clicking "Execute". It may
 * be disabled if no group/destination is selected or it is invalid, in which case
 * CanExecute should return false.
 *
 * Format: M_strGroupID_strDestSector_strDestLocation
 *
 * @author Joe Forster
 */
class AIMoveAction : public AIAction
{
public:
	/**
	 * @brief Construct a move action given a group ID, a sector name, and a location (body).
	 */
	AIMoveAction(String strGroupID, String strDestSector, String strDestLocation);

	/**
	 * @brief Destroy a move action.
	 */
	virtual ~AIMoveAction() {}

	// Get functions for the parameters.
	unsigned GetGroupID() const { return m_iGroupID; }
	String GetDestSector() const { return GetParameter2(); }
	String GetDestLocation() const { return GetParameter3(); }

	// Implemented functions from the base class.
	bool CanExecute(const FactionController* pController) const;
	void Execute(FactionController* pController) const;
	String GetString() const;


private:
	unsigned m_iGroupID;
};

/**
 * @brief An action type for purchasing/constructing a ship to deploy later.
 *
 * This is achieved in the GUI by selecting a group type in the build menu and then
 * clicking Purchase. It may be disabled if the faction cannot afford that unit, in which
 * case CanExecute returns false.
 *
 * Format: P_strType_strCount
 *
 * @author Joe Forster
 */
class AIPurchaseAction : public AIAction
{
public:
	/**
	 * @brief Construct a purchase action given a type and count
	 */
	AIPurchaseAction(String strType, String strCount);

	/**
	 * @brief Destroy a purchase action.
	 */
	virtual ~AIPurchaseAction() {}

	// Get functions for the parameters
	const String& GetShipType() const { return GetParameter1(); }
	unsigned GetShipCount() const { return m_iShipCount; }
	const StringVector& GetGroup() const { return m_vGroup; }

	// Implemented functions from the base class.
	bool CanExecute(const FactionController* pController) const;
	void Execute(FactionController* pController) const;
	String GetString() const;

private:
	unsigned m_iShipCount;
	StringVector m_vGroup;
};

/**
 * @brief An action type for deploying a purchased ship.
 *
 * This is achieved in the GUI by selecting a group in the deploy menu and then
 * clicking Deploy. It may be disabled if that group isn't available, in which
 * case CanExecute returns false.
 *
 * Format: D_strType_strCount_strSector
 *
 * @author Joe Forster
 */
class AIDeployAction : public AIAction
{
public:
	/**
	 * @brief Construct a deploy action given a type, count and sector.
	 */
	AIDeployAction(String strType, String strCount, String strSector);

	/**
	 * @brief Destroy a deploy action.
	 */
	virtual ~AIDeployAction() {}

	// Get functions for the parameters.
	String GetShipType() const { return GetParameter1(); }
	unsigned GetShipCount() const { return m_iShipCount; }
	const StringVector& GetGroup() const { return m_vGroup; }
	String GetSector() const { return GetParameter3(); }

	// Implemented functions from the base class.
	bool CanExecute(const FactionController* pController) const;
	void Execute(FactionController* pController) const;
	String GetString() const;

private:
	unsigned m_iShipCount;
	StringVector m_vGroup;
};

/**
 * @brief A factory class for constructing AIAction objects from strings.
 *
 * @author Joe Forster
 */
class AIActionFactory
{
public:
	/**
	 * @brief Make an AIAction object from a string by determining which Make function to call.
	 */
	static AIAction* MakeAction(String strAction);

private:

	/**
	 * @brief Function for reading in a purchase action from a string produced by GetString()
	 */
	static AIMoveAction* MakeMoveAction(String strAction);

	/**
	 * @brief Function for reading in a purchase action from a string produced by GetString()
	 */
	static AIPurchaseAction* MakePurchaseAction(String strAction);

	/**
	 * @brief Function for reading in a deploy action from a string produced by GetString()
	 */
	static AIDeployAction* MakeDeployAction(String strAction);

};

#endif // AIACTION_H_INCLUDED
