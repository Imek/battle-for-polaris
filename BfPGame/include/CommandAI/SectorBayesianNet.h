#ifndef SECTORBAYESIANNET_H_INCLUDED
#define SECTORBAYESIANNET_H_INCLUDED

#include <AI/BayesianNetwork.h>

#include <CommandAI/AIAbstractState.h>

class SectorLabel : public NodeLabel
{
public:
	SectorLabel(const AbstractSectorState* sector);
	~SectorLabel() {};

	const String& GetString() { return m_sectorName; }

private:
	String m_sectorName;
};

#endif // SECTORBAYESIANNET_H_INCLUDED
