#ifndef AISYSTEM_H_INCLUDED
#define AISYSTEM_H_INCLUDED

#include <iostream>
#include <list>

#include <BfPGameDefs.h>
#include <State/BfPState.h>
#include <CommandAI/AIPlayer.h>

using namespace std;

typedef std::map<String, AIPlayer*> AIPlayerList;

/**
 * @brief GameAI is the main access point of the AI system, where we set everything up and update it.
 *
 * @author Joe Forster
 */
class GameAI
{
public:
	/**
	 * @brief The GameAI constructor sets up all of the AI players specified in the configuration.
	 */
	GameAI(BfPState* pGameState);

	/**
	 * @brief GameAI destructor manages the players
	 */
	virtual ~GameAI();

	/**
	 * @brief Get the list of AI players.
	 */
	AIPlayerList& GetPlayers() { return m_mPlayers; }

	/**
	 * @brief Main AI update function, called during the control update cycle.
	 */
	void Update(unsigned long iTime);

private:
	// A pointer to the previous state, managed here.
	const BfPState*	 m_pCurrentState;

	// The list of players, mapped from player IDs.
	AIPlayerList		m_mPlayers;

};




#endif // AISYSTEM_H_INCLUDED
