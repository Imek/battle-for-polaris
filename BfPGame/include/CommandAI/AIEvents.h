#ifndef AIEVENTS_H_INCLUDED
#define AIEVENTS_H_INCLUDED

#include <Util/Events.h>
#include <BfPGameDefs.h>
#include <Config/BfPConfig.h>

/*
 * TODO: AI Events are currently only part-implemented. We still need to put them into the
 * BfPState and make proper use of them in the logic engine.
 */

/**
 * @brief An AI event indicating that a ship was killed, where, and by whom.
 *
 * @author Joe Forster
 */
class AIUnitKilledEvent : public Event
{
public:
	/**
	 * @brief Construct a unit death event.
	 *
	 * @param rSector		   The location of the unit
	 * @param rKillerFaction	Owner of the unit that killed this unit
	 * @param rKillerGroupID	The group ID of the unit that killed the unit
	 * @param rKilledFaction	Owner of the unit that was killed
	 * @param rKilledGroupID	The group ID of the unit that was killed
	 */
	AIUnitKilledEvent(const String& rSector,
		const String& rKillerFaction, const unsigned& rKillerGroupID,
		const String& rKilledFaction, const unsigned& rKilledGroupID)
	: Event("EV_AI_SHIPKILLED")
	, m_strSector(rSector)
	, m_strKillerFaction(rKillerFaction), m_iKillerGroupID(rKillerGroupID)
	, m_strKilledFaction(rKilledFaction), m_iKilledGroupID(rKilledGroupID)
	{
		const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
		assert(pScenarioConf->GetSectorType(rSector));
		assert(pScenarioConf->GetFactionType(rKillerFaction));
		assert(pScenarioConf->GetFactionType(rKilledFaction));
	}

	/**
	 * @brief Copy constructor for a unit death event.
	 */
	AIUnitKilledEvent(const AIUnitKilledEvent& rObj)
	: Event("EV_AI_SHIPKILLED")
	, m_strSector(rObj.m_strSector)
	, m_strKillerFaction(rObj.m_strKillerFaction)
	, m_strKilledFaction(rObj.m_strKilledFaction) {}

	// Inherited functions
	String GetString() const
	{
		// If we need to serialise a BfPState,
		// we'll need to fill this in and make a fromstring constructor.
		return "";
	}
	void Resolve(unsigned long iTime) {}

	// Get functions for members
	const String& GetSector() const { return m_strSector; }

	const String& GetKillerFaction() const { return m_strKillerFaction; }
	const unsigned& GetKillerGroupID() const { return m_iKillerGroupID; }

	const String& GetKilledFaction() const { return m_strKilledFaction; }
	const unsigned& GetKilledGroupID() const { return m_iKilledGroupID; }

private:
	String	  m_strSector;

	String	  m_strKillerFaction;
	unsigned	m_iKillerGroupID;

	String	  m_strKilledFaction;
	unsigned	m_iKilledGroupID;
};

/**
 * @brief An AI event indicating that a ship has been observed warping from one sector to another.
 *
 * @author Joe Forster
 */
class AIUnitWarpedEvent : public Event
{
public:
	/**
	 * @brief Construct a unit warping event.
	 *
	 * @param rFaction	  The location of the unit
	 * @param rGroupID	  Owner of the unit that killed this unit
	 * @param rFromSector   The group ID of the unit that killed the unit
	 * @param rToSector	 Owner of the unit that was killed
	 */
	AIUnitWarpedEvent(const String& rFaction, const unsigned& rGroupID,
		const String& rFromSector, const String& rToSector)
	: Event("EV_AI_SHIPWARPED")
	, m_strFaction(rFaction)
	, m_iGroupID(rGroupID)
	, m_strFromSector(rFromSector)
	, m_strToSector(rToSector)
	{
		const ScenarioConfig* pScenarioConf = ((const BfPConfig*)BfPConfig::GetInstancePtr())->GetActiveScenario();
		assert(pScenarioConf->GetSectorType(rFromSector));
		assert(pScenarioConf->GetSectorType(rToSector));
	}

	/**
	 * @brief Copy constructor for a warping event
	 */
	AIUnitWarpedEvent(const AIUnitWarpedEvent& rObj)
	: Event("EV_AI_SHIPWARPED")
	, m_strFaction(rObj.m_strFaction)
	, m_iGroupID(rObj.m_iGroupID)
	, m_strFromSector(rObj.m_strFromSector)
	, m_strToSector(rObj.m_strToSector)
	{}

	// Inherited functions
	String GetString() const
	{
		// If we need to serialise a BfPState,
		// we'll need to fill this in and make a fromstring constructor.
		return "";
	}
	void Resolve(unsigned long iTime) {}

	// Get functions
	const String& GetFaction() const { return m_strFaction; }
	const unsigned& GetGroupID() const { return m_iGroupID; }
	const String& GetFromSector() const { return m_strFromSector; }
	const String& GetToSector() const { return m_strToSector; }

private:

	String	  m_strFaction;
	unsigned	m_iGroupID;

	String	  m_strFromSector;
	String	  m_strToSector;
};

#endif // AIEVENTS_H_INCLUDED
