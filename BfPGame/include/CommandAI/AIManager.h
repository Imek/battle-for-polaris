#ifndef AIMANAGER_H_INCLUDED
#define AIMANAGER_H_INCLUDED

#include <CommandAI/AIManagerPreset.h>

/**
 * @brief Manager class for AIManager objects (from types in the configuration)
 *
 * @author Joe Forster
 */
struct AIManagerFactory
{
	/**
	 * @brief Make an AIManager object from a type and with an evaluator
	 */
	static AIManager* MakeManager(String strType, const AIEvaluator* pEvaluator)
	{
		assert(pEvaluator);
		AIManager* pManager = NULL;

		if (strType == "PresetShipManager")
			pManager = new AIPresetShipManager(pEvaluator);
		else if (strType == "PresetResourceManager")
			pManager = new AIPresetResourceManager(pEvaluator);
		else
		{
			std::stringstream sout;
			sout << "Invalid manager type given: " << strType << "; bad config entry?";
			throw JFUtil::Exception("AIManagerFactory", "MakeManager", sout.str());
		}

		assert(pManager);
		return pManager;
	}
};

#endif // AIMANAGER_H_INCLUDED
