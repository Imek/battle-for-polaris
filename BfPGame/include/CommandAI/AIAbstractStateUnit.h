#ifndef AIABSTRACTSTATEUNIT_H_INCLUDED
#define AIABSTRACTSTATEUNIT_H_INCLUDED

#include <BfPGameDefs.h>
#include <State/BfPState.h>

/**
 * @brief Represents an entry in a unit state's history for a given tick.
 *
 * Contains information on observation, existence, and the possible locations
 * the unit could have for this tick.
 *
 * @author Joe Forster
 */
class UnitHistoryEntry
{
public:
	/**
	 * @brief Constructor for an observed entry
	 */
	UnitHistoryEntry(unsigned long iTick, bool bPurchased,
		const String& rLocation);

	/**
	 * @brief Constructor for an estimated/hypothetical entry
	 */
	UnitHistoryEntry(unsigned long iTick, bool bPurchased,
		const Real& rExistence, const StringRealMap& rLocation);

	// Getter functions for the members.
	const unsigned long& GetTick() const { return m_iTick; }
	const bool& GetPurchased() const { return m_bPurchased; }
	const bool& GetObserved() const { return m_bObserved; }
	const Real& GetExistence() const { return m_fExistence; }
	const StringRealMap& GetLocProbs() const { return m_mLocation; }

	/**
	 * @brief Get the probability (between 0 and 1) of the unit being at a given location.
	 */
	const Real& GetLocProb(const String& rSector) const;

// TODO: Delete once we're sure we won't be using the merging stuff
//	/**
//	 * @brief Merge a new set of location probabilities with this one.
//	 */
//	void MergeNewShip(const StringRealMap& rNewLocProbs);

	/**
	 * @brief Set the unit to observed and set its known location.
	 *
	 * @param bPurchased	Whether this unit may have been created at this turn
	 * @param rSector	   The sector that we have observed the unit at.
	 *
	 */
	void SetObservedLoc(bool bPurchased, const String& rSector);

	/**
	 * @brief Assuming that the unit is observed, get its location.
	 */
	String GetObservedLoc() const;

	/**
	 * @brief Set the unit to unobserved, and make an estimation of its location.
	 *
	 * @param bPurchased	Whether this unit may have been created at this turn.
	 * @param fExistence	The overall probability of this unit actually existing.
	 * @param rLocation	 A probability distribution representing of location to probability.
	 */
	void SetEstimatedLoc(bool bPurchased, const Real& fExistence, const StringRealMap& rLocation);

	/**
	 * @brief Validate the values in this data structure, throwing an exception if invalid.
	 */
	void Validate() const;

private:

	unsigned long   m_iTick;
	bool			m_bPurchased;
	bool			m_bObserved;
	// Probability of existing
	Real			m_fExistence;
	// Probability distribution of sector -> probability
	StringRealMap   m_mLocation;
	// Location (if m_bObserved is true!)
	String		  m_strObservedLoc;
};

typedef std::map<unsigned long, UnitHistoryEntry*> UnitHistory;


/**
 * @brief An abstract representation of a group of ships; could just be one ship.
 *
 * A "Unit" may be a ship group that has been observed, or it may be an estimated
 * or hypothetical unit produced by the logic engine. Note that, for this reason,
 * Unit IDs are not the same as Group IDs.
 *
 * @author Joe Forster
 */
class AbstractUnitState
{
public:
	/**
	 * @brief Add a spotted ship.
	 */
	AbstractUnitState(unsigned long iTick, const String& rFaction,
		const BfPSectorState* pSector, unsigned iGroupID, unsigned iUnitID);

	/**
	 * @brief Add a hypothetical ship.
	 */
	AbstractUnitState(unsigned long iTick, const String& rFaction, const Real& rExistence,
		const StringRealMap& rType, const UIntRealMap& rCount, const StringRealMap& rLocation, unsigned iUnitID);

	/**
	 * @brief Copy constructor for AbstractUnitState (as the history entries need copying)
	 */
	AbstractUnitState(const AbstractUnitState& rUnit)
	: m_pGroupID(NULL)
	, m_iUnitID(rUnit.m_iUnitID)
	, m_strFaction(rUnit.m_strFaction)
	, m_fHealth(rUnit.m_fHealth)
	, m_iEarliestTick(rUnit.m_iEarliestTick)
	, m_iLatestTick(rUnit.m_iLatestTick)
	, m_mType(rUnit.m_mType)
	, m_mCount(rUnit.m_mCount)
	{
		// If it's not hypothetical, get the group ID and the known count
		if (rUnit.m_pGroupID)
		{
			m_pGroupID = new unsigned(*rUnit.m_pGroupID);
			m_iKnownCount = rUnit.m_iKnownCount;
		}
		for (UnitHistory::const_iterator it = rUnit.m_mHistory.begin();
			 it != rUnit.m_mHistory.end(); ++it)
		{
			m_mHistory[it->first] = new UnitHistoryEntry(*it->second);
		}

	}

	/**
	 * @brief The destructor for AbstractUnitState disposes of the managed memory used for units.
	 */
	~AbstractUnitState()
	{
		delete m_pGroupID;
		for (UnitHistory::iterator it = m_mHistory.begin();
			 it != m_mHistory.end(); ++it)
		{
			delete it->second;
		}
		m_mHistory.clear();
	}

	// Getter functions for members.
	const unsigned& GetUnitID() const { return m_iUnitID; }
	const String& GetFaction() const { return m_strFaction; }
	const Real& GetHealth() const { return m_fHealth; }
	const unsigned& GetGroupID() const
	{
		assert(m_pGroupID); // Should've checked GetHypothetical!
		return *m_pGroupID;
	}

	/**
	 * @brief Get whether this unit is hypothetical.
	 *
	 * A unit is hypothetical (estimated) if it hasn't been given a
	 * Group ID from the concrete state.
	 */
	bool GetHypothetical() const { return !m_pGroupID; }

// TODO: Delete once we're sure we won't be using the merging stuff
//	/**
//	 * @brief Merge a newly-created hyothetical unit with the same type by adding the probabilities.
//	 */
//	void MergeNewShip(unsigned long iTick, const StringRealMap& rNewLocProbs, const UIntRealMap& rNewCountProbs);


	/**
	 * @brief Set the type/count to 1.0 for the given values and 0.0 for everything (i.e. observed)
	 */
	void SetType(const String& rType, unsigned iCount);

	/**
	 * @brief Set the type probabilities to a new set of values.
	 */
	void SetType(const StringRealMap& rType, const UIntRealMap& rCount);


	/**
	 * @brief Get a reference to the type probability map.
	 */
	const StringRealMap& GetTypeProbs() const { return m_mType; }

	/**
	 * @brief Get the probability of this unit being a given group type.
	 */
	const Real& GetTypeProb(const String& rType) const;

	/**
	 * @brief Get the type of this unit, if it is known (i.e. not hypothetical).
	 */
	const String& GetKnownType() const;


	/**
	 * @brief Get a reference to the ship count probability map.
	 */
	const UIntRealMap& GetCountProbs() const { return m_mCount; }

	/**
	 * @brief Get the probability of this unit being a given size.
	 */
	Real GetCountProb(unsigned iCount) const;

	/**
	 * @brief Get the ship count for this unit, if it is known (i.e. not hypothetical).
	 */
	const unsigned& GetKnownCount() const;


	/**
	 * @brief Get the earliest tick this ship could have existed.
	 */
	unsigned long GetEarliestTick() const;

	/**
	 * @brief Get the tick of the (chronologically) latest update.
	 */
	unsigned long GetLatestTick() const;

	/**
	 * @brief Get the probability that the ship was at a given location at a given (assumed valid!) tick.
	 */
	const UnitHistoryEntry* GetEntry(unsigned long iTick) const;

	/**
	 * @brief Get the history entry for the latest tick.
	 */
	const UnitHistoryEntry* GetEntry() const;

	/**
	 * @brief Set the location & count to 1.0 for the given value and 0.0 for everything (i.e. observed), recording it in the history.
	 */
	void ObserveState(unsigned long iTick, bool bPurchased,
		const String& rSector);

	/**
	 * @brief Set the location probabilities to a new set of values, recording it in the history.
	 */
	void EstimateState(unsigned long iTick, bool bPurchased,
		const Real& rExistence, const StringRealMap& rNewProbs);

	/**
	 * @brief Flag this unit as no longer observed; the next logic engine update should do an estimation for it.
	 */
	void SetNoLongerObserved();

	/**
	 * @brief Validate the values in this data structure, throwing an exception if invalid.
	 */
	void Validate() const;

private:
	// ID number of the actual ship group (NULL if this ship is hypothetical)
	unsigned*	   m_pGroupID;
	// ID number of the unit in the abstract state
	unsigned		m_iUnitID;
	String		  m_strFaction;
	// Current health as a proportion of the original.
	Real			m_fHealth;

	unsigned long   m_iEarliestTick;
	unsigned long   m_iLatestTick;

	// Probability distribution of ship type -> probability
	StringRealMap   m_mType;
	// Probability distribution of num ships -> probability
	UIntRealMap	 m_mCount;
	unsigned		m_iKnownCount; // Must only be accessed if known!

	// History of location & existence
	UnitHistory	 m_mHistory;
};

#endif // AIABSTRACTSTATEUNIT_H_INCLUDED
