#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

#include <unistd.h>
#include <map>
#include <vector>
#include <algorithm>

#include <Util/Singleton.h>

// View
#include <Ogre.h>
#include <BfPGameDefs.h>
#include <Ogre/OgreFramework.h>
#include <Ogre/OgreTimerManager.h>
#include <Ogre/OgreMovableText.h>
#include <Scene/BfPSceneSector.h>
#include <Scene/BfPSceneMap.h>
#include <UI/BfPInput.h>

// Model (state)
#include <State/BfPState.h>

// Controller
#include <Control/BfPControlWorld.h>
#include <Control/BfPPhysics.h>
#include <CommandAI/AISystem.h>

/**
 * @brief Core view class for interfacing between the OGRE system and everything else.
 *
 * @author Joe Forster
 */
class BfPGame : public JFUtil::Singleton<BfPGame>
{
public:
	BfPGame();
	~BfPGame();

	void Go();

	/**
	 * @brief Update the state of the game
	 */
	bool Update(int i);


private:

	typedef std::map<String, BfPSceneSector*> SectorScenes;

	// Members relating to the game state
	BfPState*			   m_pState;
	BfPConfig*			  m_pConfigs;

	// Members relating to game control
	BfPInput*			   m_pInput; // Handles player input and the GUI
	WorldController*		m_pWorld;
	FactionController*	  m_pPlayer; // Controller for player controlled by our input
	AIEvaluator*			m_pPlayerEvaluator;
	AITestLogicEngine*	  m_pPlayerLogicEngine; // Test logic engine for debugging & demonstration
	GameAI*				 m_pAI; // Contains controllers for all other (AI-controlled) players
	GamePhysics*			m_pPhysics; // The physics engine for game entities

	bool					m_bShutDown; // Shut down the game and load up the menu next update

	// Members relating to display of the game in OGRE
	OgreFramework*		  m_pOgre;
	TimerManager<BfPGame>   m_oTimerManager;
	SectorScenes			m_mSectorScenes;
	BfPSceneMap*			m_pMapScene;
	BfPSceneSector*		 m_pPrevSectorScene;

	/**
	 * @brief Destroy the current game session (state)
	 */
	void DestroyGame();

	/**
	 * @brief Run game initialisation
	 */
	void InitGame();

	/**
	 * @brief Begin the game loop.
	 */
	void Run();

	CAMERA_MODE GetCamMode() { return m_pInput->GetCameraMode(); }

	GAME_VIEW_MODE GetMode() { return m_pInput->GetGameMode(); }

	/**
	 * @brief The active sector is the sector to display.
	 *
	 * It is just the player's selected sector in most modes, except the chase camera mode,
	 * in which it is always the player's flag ship sector.
	 */
	const String& GetActiveSector();

	/**
	 * @brief Set up the scenes for sectors and map.
	 */
	void CreateScenes();

	/**
	 * @brief dispose of all the scenes.
	 */
	void DestroyScenes();


	/**
	 * @brief Call the scene's update function, which manages addition and removal of objects.
	 */
	void UpdateScenes(unsigned long iTime);

	/**
	 * @brief Updates the input manager and handles toggling of game view mode.
	 */
	void UpdateInput(unsigned long iTime);
};

#endif // CORE_H_INCLUDED
