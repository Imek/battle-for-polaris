#ifndef BFPPARTICLES_H_INCLUDED
#define BFPPARTICLES_H_INCLUDED

#include <Game/GameScene.h>

#include <Config/BfPConfig.h>
#include <State/BfPState.h>
#include <Util/Events.h>

enum PARTICLE_EFFECT_TYPE { PET_TRAIL, PET_WARP, PET_EXPLOSION };

/**
 * @brief A class to handle the various triggered and timed particle events in the game
 *
 * If the parameter pDuration is provided as a null pointer, then this particle effect lasts
 * until disposed of.
 *
 * @author Joe Forster
 */
class ParticleEffect
{
public:
	/**
	 * @brief Construct a ParticleEffect with a given name, type, particle template, and optional duration.
	 */
	ParticleEffect(String strName, PARTICLE_EFFECT_TYPE iType,
		String strParticles, unsigned long * pDuration);

	/**
	 * @brief Dispose of this particle effect's particle system and scene node.
	 */
	virtual ~ParticleEffect();

	/**
	 * @brief Get the enum value used as the type.
	 */
	PARTICLE_EFFECT_TYPE GetType() const { return m_iType; }

	/**
	 * @brief Get whether this particle effect's life counter has finished counting down.
	 */
	bool GetFinished() const;

	/**
	 * @brief Alter the orientation of these particles.
	 */
	void SetOrientation(const Quaternion& rNew);

	/**
	 * @brief Get a const reference to the position of this node
	 */
	const Vector3& GetPosition() const;

	/**
	 * @brief Set the position of the emitter.
	 */
	void SetPosition(const Vector3& rNew);

	/**
	 * @brief Translate all the particles by the given vector.
	 */
	void TranslateAllParticles(const Vector3& rTranslate);

	/**
	 * @brief Get the stored velocity for these particles' node.
	 */
	const Vector3& GetVelocity() const { return m_vecStoredVel; }

	/**
	 * @brief Set a stored velocity value for these projectiles, used by the caller of SetPosition.
	 */
	void SetVelocity(const Vector3& rVel);

	/**
	 * @brief Alter the duration of the particle effect.
	 */
	void SetDuration(unsigned long iNew)
	{
		if (!m_pDuration) m_pDuration = new unsigned long();
		*m_pDuration = iNew;
	}

	// Attach/detach our scene from its parent.
	void Attach(Ogre::SceneNode* pParent);
	void Detach();

	/**
	 * @brief Update the effect, counting down its lifetime.
	 */
	void Update(unsigned long iTime);

protected:
	Ogre::ParticleSystem* GetParticles();

private:
	String				  m_strName;
	PARTICLE_EFFECT_TYPE	m_iType;

	unsigned long		   m_iAge;
	unsigned long *		 m_pDuration;

	Ogre::SceneNode*		m_pNode;
	Ogre::SceneNode*		m_pParentNode;

	Vector3				 m_vecStoredVel;
};

/**
 * @brief A type of particle effect for entity engines with varying intensity.
 *
 * @author Joe Forster
 */
class EngineParticles : public ParticleEffect
{
public:
	/**
	 * @brief Construct the particle effect for ship engines.
	 *
	 * @param strName	   The name of the particle effect.
	 * @param iType		 The particle effect type
	 * @param strParticles  The OGRE particle script to use.
	 * @param strObjType	The type of entity the particles should follow.
	 * @param iObjID		The ID of the entity.
	 */
	EngineParticles(String strName, PARTICLE_EFFECT_TYPE iType, String strParticles,
		String strObjType, unsigned iObjID);

	/**
	 * @brief Destroy the effect.
	 */
	virtual ~EngineParticles();

	/**
	 * @brief Set the intensity of the engine effect, with respect to thrust.
	 *
	 * @param rVal  A value between 0 and 1.
	 */
	void SetIntensity(const Real& rVal);

	String GetObjType() const { return m_strObjType; }
	unsigned GetObjID() const { return m_iObjID; }

	const EntityState* GetState(const BfPSectorState* pSector) const;

private:
	// For each emitter (engine)
	Real*	   m_arrMaxRates;
	Real*	   m_arrMaxVels;

	String	  m_strObjType;
	unsigned	m_iObjID;

};

#endif // BFPPARTICLES_H_INCLUDED
