#ifndef BFPMAPSCENE_H_INCLUDED
#define BFPMAPSCENE_H_INCLUDED

#include <Ogre/OgreMovableText.h>
#include <Game/GameScene.h>

#include <Config/BfPConfig.h>
#include <Control/BfPControlFaction.h>
#include <CommandAI/AILogicEngine.h>

/**
 * @brief A struct containing the stuff needed for updating and managing a map node in the scene.
 *
 * TODO: Destroy these properly (not high priority unless we start dynamically
 * adding/destroying map scenes or map nodes)
 *
 * @author Joe Forster
 */
struct MapNode
{
	/**
	 * @brief Construct a map node instance with a given sphere entity, selection marker and label
	 */
	MapNode(Ogre::Entity* pEntity, Ogre::Entity* pMarker, Ogre::MovableText* pLabel)
	: m_pEntity(pEntity)
	, m_pMarker(pMarker)
	, m_pLabel(pLabel)
	{
		assert(m_pEntity);
		assert(m_pMarker);
		assert(m_pLabel);
	}

	~MapNode()
	{
		// NOTE: The BfPSceneMap destructor handles actually destroying this stuff
//		OgreFramework::GetInstance().GetSceneManager()->destroyEntity(m_pEntity);
//		OgreFramework::GetInstance().GetSceneManager()->destroyEntity(m_pMarker);
		//delete m_pLabel;
	}

	Ogre::Entity*	   m_pEntity;
	Ogre::Entity*	   m_pMarker; // When it's selected
	Ogre::MovableText*  m_pLabel;

};

typedef std::map<String, MapNode*> MapNodeList;

/**
 * @brief A class representing the OGRE scene of the game map, shown in the map camera mode.
 *
 * @author Joe Forster
 */
class BfPSceneMap : public GameScene
{
public:
	BfPSceneMap(const FactionController* pPlayer);
	~BfPSceneMap();

	// Implementations of all the GameScene virtual functions
	void CreateScene();
	void UpdateScene(unsigned long iTime);
	void DestroyScene();
	void Attach();
	void Detach();

	/**
	 * @brief Set whether we're viewing the visible state or the abstract one.
	 */
	void SetAbstract(bool bAbstract) { m_bAbstract = bAbstract; }

private:
	const FactionController*	m_pPlayer;

	MapNodeList				 m_mMapNodes;

	bool						m_bAbstract;

	/**
	 * @brief Get the OGRE material for the colour of the map node for a given sector
	 *
	 * Colour coding depends on a sector's visibility and ownership.
	 */
	String GetMapEntityMaterial(const String& rSector);

	/**
	 * @brief Put together the text label for a sector on the game map.
	 */
	String GetMapSectorLabel(const String& rSector);

	/**
	 * @brief Put together the text label for a sector on the logic engine (AI debugger) map.
	 */
	String GetAbstractSectorLabel(const String& rSector, const AbstractBfPState* pState);

};

#endif // BFPMAPSCENE_H_INCLUDED
