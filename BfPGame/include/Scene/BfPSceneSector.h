#ifndef BFPSECTORSCENE_H_INCLUDED
#define BFPSECTORSCENE_H_INCLUDED

#include <OgreLight.h>
#include <Ogre/OgreMovableText.h>

#include <Game/GameScene.h>

#include <Config/BfPConfig.h>
#include <Control/BfPControlFaction.h>
#include <Effects/BfPParticles.h>

typedef std::pair<String, unsigned> EntityID;
typedef std::map< EntityID, Ogre::SceneNode*> EntityList;
typedef std::map<String, Ogre::SceneNode*> BodyList;
typedef std::map<String, Ogre::ManualObject*> MarkerList;
typedef std::vector<Ogre::SceneNode*> NodeList;
typedef std::vector<Ogre::Light*> LightList;


/**
 * @brief A GameScene implementation for the sector view used by the free/chase/tactical cameras.
 *
 * @author Joe Forster
 */
class BfPSceneSector : public GameScene
{
public:
	BfPSceneSector(const String& rSector, const FactionController* pPlayer);
	~BfPSceneSector();

	// Implementations of all the GameScene virtual functions
	void CreateScene();
	void UpdateScene(unsigned long iTime);
	void DestroyScene();
	void Attach();
	void Detach();

	void SetFlyingFlagship(bool bVal) { m_bFlyingFlagship = bVal; }

	const SectorType* GetSectorType() const;

private:
	const FactionController*	m_pPlayer;
	bool						m_bFlyingFlagship;

	// Stuff to do with entities (ships etc)
	EntityList				  m_mEntitiesLastFrame;
	std::list<ParticleEffect*>	   m_lParticleEffects;

	// Static stuff in the sector
	BodyList					m_mMajorBodies;
	MarkerList				  m_mWarpPoints; // String key is the dest sector
	Ogre::SceneNode*			m_pDustParticles;

	// Background objects (that stay at the same position relative to the camera
	NodeList					m_vBGNodes;
	std::vector<Ogre::Light*>   m_vBGLights;
	std::vector<Vector3>		m_vBGObjLocs;
	std::vector<Ogre::Light*>   m_vAllLights;

	Ogre::SceneNode* GetNodeForEntity(String strType, unsigned iID);

	/**
	 * @brief Update the dynamic objects in the scene (objects that move and arrive/leave)
	 */
	void UpdateEntities(unsigned long iTime);

	/**
	 * @brief Update the particle effects associated with an entity
	 */
	void UpdateEntityParticles(unsigned long iTime, const EntityState* pState);

	/**
	 * @brief Add the various particle effects associated with a particular ship in the sector.
	 */
	void UpdateShipParticles(unsigned long iTime, const ShipState* pShip);

	/**
	 * @brief Update the labels attached to a particular ship.
	 */
	void UpdateShipLabel(unsigned long iTime, const ShipState* pShip);

	/**
	 * @brief Update particle effects that already have been added.
	 */
	void UpdateActiveParticles(unsigned long iTime);

	/**
	 * @brief Update bacgkround stuff that follows the camera
	 */
	void UpdateBackgroundObjects(unsigned long iTime);

	/**
	 * @brief Update the tactical markers on ships and objects.
	 */
	void UpdateMarkers(unsigned long iTime);

	// Draw the various markers used for ships/entities in the game GUI
	void DrawShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const;
	void DrawFactionShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const;

	void DrawEnemyShipMarker(const ShipState* pShip, Ogre::ManualObject* pMarker) const;
	void AddAimPredictMarker(const ShipState* pTarget, Ogre::ManualObject* pMarker) const;

	void DrawBodyMarker(const String& rSector, const String& rBody, Ogre::ManualObject* pMarker);
	void DrawWarpPointMarker(const String& rSector, const String& rDestSector, Ogre::ManualObject* pMarker);

};

#endif // BFPSECTORSCENE_H_INCLUDED
