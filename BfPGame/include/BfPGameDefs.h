#ifndef BFPGAMEDEFS_H_INCLUDED
#define BFPGAMEDEFS_H_INCLUDED

#include <map>

#include <BfPEngineDefs.h>

typedef std::map<String, String> StringStringMap;
typedef std::map<String, Real> StringRealMap;
typedef std::map<String, unsigned> StringUIntMap;
typedef std::map<unsigned, Real> UIntRealMap;

#endif // BFPGAMEDEFS_H_INCLUDED
