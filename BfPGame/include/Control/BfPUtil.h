#ifndef BFPUTIL_H_INCLUDED
#define BFPUTIL_H_INCLUDED

#include <Util/Maths.h>
#include <Util/Control.h>

#include <State/BfPState.h>
#include <CommandAI/AIEvaluator.h>

/**
 * @brief A namespace containing some utility code specific to Battle for Polaris
 *
 * This is mostly for ship evaluation and threat calculation for the AI and control code.
 *
 * @author Joe Forster
 */
namespace BfPUtil
{
	typedef std::map<unsigned, int> ThreatMap; // Map Ship ID to threat

	/**
	 * Come up with a number representing a ship's threat or danger value, derived from things
	 * like its type, remaining hitpoints, weapons, and so on.
	 *
	 * @param	pOf			a pointer to the ship being evaluated.
	 * @param   pAgainst	a pointer to the ship we're evaluating ours against
	 * @param   pEvaluator  a pointer to the AI evaluator to use.
	 *
	 * @return	a positive or negative integer value
	 */
	unsigned EvaluateShip(const ShipState* pOf, const ShipState* pAgainst,
		const AIEvaluator* pEvaluator);

	/**
	 * @brief Retrieve the base value of one ship against another ship, using some
	 *		kind of evaluator from the evaluator system.
	 *
	 * @param   pEvaluator  a pointer to the AI evaluator to use.
	 */
	unsigned EvaluateShipType(const String& rOfType, const String& rAgainstType,
		const AIEvaluator* pEvaluator);

	/**
	 * Calculate the threat against my ship posed by the target ship.
	 *
	 * @param	rMyShip		my ship - the ship against which threat is posed.
	 * @param	fRange		the aggro range of my ship.
	 * @param	rTarget		the target ship posing the threat.
	 * @param   pEvaluator  a pointer to the AI evaluator to use.
	 *
	 * @return	a threat value.
	 */
	unsigned CalculateThreat(const ShipState* pMyShip, Real fRange, const ShipState* pTarget,
		const AIEvaluator* pEvaluator);


	/**
	 * GetThreatList: Get a map of Ship ID to threat value given the parameters, putting the total threat
	 * into pTotalThreat.
	 *
	 * @param	rFrom			the ship we're looking for ships in range of.
	 * @param	pSector			the world containing all the ships.
	 * @param	fRange			the maximum range from the ship to search.
	 * @param	bSameTeam		true means friendly, false means hostile.
	 * @param	pTotalThreat	An integer value to set containing the total threat added up.
	 * @param   pEvaluator	  a pointer to the AI evaluator to use.
	 *
	 * @return	a (possibly empty) map of ship ID to threat value.
	 */
	ThreatMap GetThreatList(
		const ShipState* pFrom, const BfPSectorState* pSector,
		Real fRange, bool bSameTeam, unsigned* pTotalThreat,
		const AIEvaluator* pEvaluator);

//	/**
//	 * Get the ship that poses the most threat towards rSubject that is in fMyRange of rFrom.
//	 * (rFrom and rSubject will only be different if rFrom is assisting and rTarget is being attacked)
//	 *
//	 * @param	rFrom			the observing ship, which we need the returned ship to be in range of.
//	 * @param	fMyRange		the aggro range of the observing ship.
//	 * @param	rSubject		the ship against which to calculate threat values.
//	 * @param	fTheirRange		the aggro range of the target ship (generally the same as fMyRange)
//	 * @param	pSector			the world containing all the ships.
//	 * @param	bSameTeam		true means friendly, false means hostile.
//	 * @param   pEvaluator	  a pointer to the AI evaluator to use.
//	 *
//	 * @return	a (possibly NULL) pointer to a threatening ship.
//	 */
//	const ShipState* GetBiggestThreat(
//		const ShipState* pFrom, Real fMyRange,
//		const ShipState* pSubject, Real fTheirRange,
//		const BfPSectorState* pSector, bool bSameTeam,
//		const AIEvaluator* pEvaluator);


	/* TODO: These are here because DistCollidableMap needs to be part of BfP rather than Game. Once we have Bodies
	 * as objects/entity instances in the game world rather than fetched from the configs, we will be able to move the
	 * following two functions to Control (which is more appropriate).
	 */
	/**
	 * @brief Use bounding spheres to predict whether one entity will collide with one of many collidable objects.
	 *
	 * @param pMe		   Our entity. Note that we take the MaxThrust value as constant thrust.
	 * @param rCollidables  The list of collidable objects
	 * @param iMaxTime	  The maximum amount of time forward to predict
	 * @param rMaxDist	  The maximum distance away an object can initially be to be checked
	 * @param iTimeStep	 The time increment to use in prediction iterations
	 *
	 * @return A bool value indicating whether we are on a collision course
	 */
	bool CollisionCourse(const EntityState* pMe, const DistCollidableMap& rCollidables,
		unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep);

	/**
	 * @brief Using the CollisionCourse function, find a direction to turn to avoid the collision
	 *
	 * @param pMe		   Our entity. Note that we take the MaxThrust value as constant thrust.
	 * @param rCollidables  The list of collidable objects
	 * @param iMaxTime	  The maximum amount of time forward to predict
	 * @param rMaxDist	  The maximum distance away an object can initially be to be checked
	 * @param iTimeStep	 The time increment to use in prediction iterations
	 *
	 * @return A pointer to the direction towards which to turn, or NULL if we weren't going to collide in the first place.
	 */
	Vector3* GetAvoidVector(const EntityState* pMe, const DistCollidableMap& rCollidables,
		unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep);
	// END TODO

	/* TODO: Similar issue here. At some point we will make a "WorldState" class that belongs in the Game
	 * code rather than BfP code, so that the following function can be put into Control.
	 */
	/**
	 * Get a list of pointers to EntityStates in the given range of rFrom,
	 * based on an owning faction parameter.
	 *
	 * @param   rType	   the entity type for which to search.
	 * @param	bSameOwner	true means the owner must be the same. false means it must be different.
	 * @param	rFrom		the entity we're looking for entity in range of.
	 * @param	pSector		the world containing all the entities.
	 * @param	fRange		the maximum range from the entity to search.

	 *
	 * @return	a (possibly empty) list of pointers to PlayerState objects in pSector.
	 */
	std::vector<const EntityState*> GetEntitiesInRange(
		const String& rType, bool bSameOwner,
		const EntityState* pFrom, const BfPSectorState* pSector, Real fRange);
	// END TODO
}

#endif // BFPUTIL_H_INCLUDED
