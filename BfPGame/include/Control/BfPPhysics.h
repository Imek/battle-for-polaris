#ifndef GAMEPHYSICS_H_INCLUDED
#define GAMEPHYSICS_H_INCLUDED

#include <BfPGameDefs.h>

#include <Physics/PhysicsSystem.h>
#include <Physics/CollisionResolve.h>

#include <Physics/BfPPhysicsEvents.h>
#include <Physics/BfPPhysicsShip.h>
#include <Physics/BfPPhysicsProjectile.h>


using JFPhysics::Particle;
using JFPhysics::RigidBody;
using JFPhysics::EntityPhysics;
using JFPhysics::ParticleForceManager;
using JFPhysics::EntityForce;
using JFPhysics::ParticleCollision;


// TODO: This collision stuff should not be under JFPhysics namespace

using JFPhysics::ShipObjectCollision;
using JFPhysics::ShipShipCollision;
using JFPhysics::ShipProjectileCollision;

// TODO: Move this to a more generic class in JFPhysics (after making a generic BfPSectorState e.g. WorldState in JFGame)
/**
 * @brief An instance of the physics engine for a particular sector (world) instance.
 *
 * @author Joe Forster
 */
class SectorPhysics
{
public:
	/**
	 * @brief The constructor for Physics, which sets up all of the things to be simulated.
	 *
	 * This constructor needs to set up all the particles and rigid bodies to be simulated
	 * in the game, as well as the forces acting upon them.
	 *
	 * @param	pWorld	A pointer to the sector state being affected by the physics engine.
	 */
	SectorPhysics(BfPSectorState* pWorld);

	virtual ~SectorPhysics();

	const SectorType* GetSectorType() const { return m_pWorld->GetSectorType(); }

	/**
	 * @brief The update function for the physics engine after a certain amount of time has passed.
	 *
	 * Call update function (with the number of milliseconds since it was last called).
	 * Firstly, looks at the control state of each player to determine whether they are turning,
	 * accelerating, and so on. Then uses the physics engine to update the state of the rigid bodies
	 * and update the player states from those objects.
	 *
	 * @param	iTime	Number of milliseconds since previous update.
	 */
	void Update(unsigned long iTime);

private:
	// Pointer to the state of the game world
	BfPSectorState*			  m_pWorld;
	// TODO: Reference to the AI event handler for when ships are destroyed (should be moved to control system)
//	EventHandler&		   m_rAIEvents;

	// The manager for all the forces in the game.
	ParticleForceManager		 m_oForces;
	// All particles/bodies linked to external state objects.
	std::vector<EntityPhysics*>  m_vObjects;
	std::vector<EntityForce*>	m_vEntityForces; // Pointers to ship forces in the force manager

	EventHandler				 m_oEvents; // For handling collisions

	/**
	 * @brief Update function for associating ships with the physics engine
	 *
	 * This function creates and destroys the ShipPhysics objects associated with ships, according
	 * to whether they have been flagged for addition or removal from the system.
	 *
	 * @param	iTime	The amount of time (milliseconds) since the last update
	 */
	void UpdateShips(unsigned long iTime);

	/**
	 * @brief Update function for weapon projectiles fired by ships.
	 *
	 * This function creates and destroys the projectiles fired by player ships.
	 *
	 * @param	iTime	The amount of time (milliseconds) since the last update
	 */
	void UpdateProjectiles(unsigned long iTime);

	/**
	 * @brief Update function for forces and EntityPhysics objects.
	 *
	 * To be called at the end of the update loop for the physics engine, this function updates
	 * the force manager and then updates each simulated particle and body. It also updates the
	 * values held in each state object referenced from the BfPSectorState given in the constructor.
	 *
	 * @param	iTime	The amount of time (milliseconds) since the last update
	 */
	void UpdateForces(unsigned long iTime);

	/**
	 * @brief Update function for collision detection
	 *
	 * This update function checks whether a collision event has occurred, and if so logs it with the
	 * event system to be handled later on in the update. This function should always be called once
	 * every update loop.
	 *
	 * @param	iTime	The amount of time (milliseconds) since the last update
	 */
	void UpdateCollisions(unsigned long iTime);

	/**
	 * @brief Update function for events
	 *
	 * This update function, which should be called at the end of the update cycle, handles events that
	 * have been added to the event handler. For the moment, events are just collision events.
	 *
	 * @param	iTime	The amount of time (milliseconds) since the last update
	 */
	void UpdateEvents(unsigned long iTime);

};

/**
 * @brief Class for all physics engines in the game; one for each sector, generally.
 *
 * TODO: An LOD system so that "inactive" sectors update less often.
 *
 * @author Joe Forster
 */
class GamePhysics
{
public:
	typedef std::map<String, SectorPhysics*> PhysicsList;

	/**
	 * @brief Construct a GamePhysics instance from a game state.
	 */
	GamePhysics(BfPState* pState);

	~GamePhysics();

	/**
	 * @brief Update each sector physics engine.
	 */
	void Update(unsigned long iTime);


private:
	 PhysicsList	m_mSectors;
	 BfPState*	  m_pState;

};

#endif // GAMEPHYSICS_H_INCLUDED
