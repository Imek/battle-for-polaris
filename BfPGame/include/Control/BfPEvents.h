#ifndef BFPEVENTS_H_INCLUDED
#define BFPEVENTS_H_INCLUDED

#include <Control/BfPControlEvents.h>
#include <Physics/BfPPhysicsEvents.h>
#include <CommandAI/AIEvents.h>

using JFPhysics::ShipObjectCollision;
using JFPhysics::ShipProjectileCollision;
using JFPhysics::ShipShipCollision;

/**
 * @brief A factory class for copying events and event handlers.
 *
 * @author Joe Forster
 */
struct EventFactory
{
	/**
	 * @brief Copy an event.
	 */
	static Event* CopyEvent(const Event* pEvent);

	/**
	 * @brief Copy an event handler.
	 */
	static EventHandler* CopyEventHandler(const EventHandler* pHandler);

	/**
	 * @brief Construct a trigger event from a type and a set of parameters given in the configuration.
	 */
	static Event* MakeTriggerEvent(const EventConfig& rConf);

};

#endif // BFPEVENTS_H_INCLUDED
