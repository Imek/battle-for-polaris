#ifndef BFPCONTROLEVENTS_H_INCLUDED
#define BFPCONTROLEVENTS_H_INCLUDED

#include <BfPGameDefs.h>

#include <Util/Events.h>
#include <Config/BfPConfig.h>
#include <State/BfPState.h>

enum FACTION_DEATH_TYPE { FDT_QUIT, FDT_FLAGGROUPKILLED, FDT_HOMECAPTURED, FDT_WIPEDOUT };

/**
 * @brief An event that is triggered when a faction loses the game; what triggers this depends on the game config.
 *
 * Note that these events should be caught by the gameplay system, which should end the game when
 * a death event is received and 1 or 0 factions are left. In the future, with the inclusion of
 * quit death, this will be the main way to quit the game itself.
 *
 * @author Joe Forster
 *
 */
class EventFactionDeath : public Event
{
public:
	/**
	 * @brief Create a faction death event.
	 *
	 * iDeathType	   The type of the faction death.
	 * rFactionKilling  The ID of the faction doing the killing.
	 * rFactionKilled   The ID of the faction killed.
	 */
	EventFactionDeath(FACTION_DEATH_TYPE iDeathType,
		const String& rFactionKilling, const String& rFactionKilled);

	/**
	 * @brief Copy a faction death event.
	 */
	EventFactionDeath(const EventFactionDeath& rEvent);

	/**
	 * @brief Get the faction death type for this event.
	 */
	FACTION_DEATH_TYPE GetFactionDeathType() const { return m_iDeathType; }

	/**
	 * @brief Get the ID of the faction doing the killing.
	 */
	const String& GetFactionKilling() const { return m_strFactionKilling; }

	/**
	 * @brief Get the ID of the faction being killed.
	 */
	const String& GetFactionKilled() const { return m_strFactionKilled; }

	/**
	 * @brief GetString just gets the message.
	 */
	string GetString() const { return m_strMessage; }

	/**
	 * @brief Resolve a victory event by just printing it to stdout for now.
	 */
	void Resolve(unsigned long iTime) { cout << m_strMessage << endl; }

private:
	FACTION_DEATH_TYPE  m_iDeathType;
	String			  m_strFactionKilling;
	String			  m_strFactionKilled;

	// A victory message compiled for message output.
	String			  m_strMessage;

};

/**
 * @brief An event for when a sector is captured.
 *
 * @author Joe Forster
 */
class EventSectorCaptured : public Event
{
public:
	/**
	 * @brief Sector rSector owned by rCaptured taken by rCapturer
	 */
	EventSectorCaptured(const String& rSector,
		const String& rCapturer, const String& rCaptured);

	/**
	 * @brief Neutral sector rSector captured by rCapturer
	 */
	EventSectorCaptured(const String& rSector,
		const String& rCapturer);

	/**
	 / @brief Copy constructor for this event type
	 */
	EventSectorCaptured(const EventSectorCaptured& rObj);

	// Member getters
	const String& GetSector() const { return m_strSector; }
	const String& GetCapturer() const { return m_strCapturer; }
	const String& GetCaptured() const { return m_strCaptured; }

	/**
	 * @brief GetString just gets the message.
	 */
	string GetString() const { return m_strMessage; }

	/**
	 * @brief Resolve a message by just printing it to stdout.
	 */
	void Resolve(unsigned long iTime) { cout << m_strMessage << endl; }

private:
	String m_strSector;
	String m_strCapturer;
	String m_strCaptured;

	String m_strMessage;

};

/**
 * @brief An event that pops up a message box in the GUI, optionally ending/restarting the game.
 *
 * @author Joe Forster
 */
class EventMessageBox : public Event
{
public:
	/**
	 * @brief Display a message box with the given title and message
	 *
	 * @param strTitle	  The title of the dialogue window
	 * @param strMessage	The text content of the dialogue
	 * @param bEndsGame	 True if clicking OK on the dialogue ends the game.
	 */
	EventMessageBox(string strTitle = "Message", string strMessage = "Message", bool bEndsGame = false);


	/**
	 * @brief Copy constructor for a message box event
	 */
	EventMessageBox(const EventMessageBox& rObj);

	// Member getters
	bool GetEndsGame() const { return m_bEndsGame; }
	string GetTitle() const { return m_strTitle; }

	/**
	 * @brief GetString just gets the message.
	 */
	string GetString() const { return m_strMessage; }

	/**
	 * @brief We don't use resolve; just cout the message.
	 */
	void Resolve(unsigned long iTime) { cout << m_strMessage << endl; }

private:
	string  m_strTitle;
	string  m_strMessage;
	bool	m_bEndsGame;
};

/**
 * @brief An event that changes what player controls are enabled.
 *
 * Possible values are NONE, TURNING, MOVEMENT, BRAKE, SHIFT, TARGETING, WEAPONS, SWITCHMODE,
 * SELECTBODY, SELECTGROUP, MOVE, SELECTSECTOR, PURCHASE, DEPLOY, ALL
 *
 * Enabled parameters add those to the list, or set the mode if ALL or NONE.
 * Disabled paramaters are removed from the list.
 *
 * @author Joe Forster
 */
class EventSetControlsEnabled : public Event
{
public:
	static CONTROLS_MODE ReadControlsMode(const String& rMode);

	/**
	 * @brief Set the enabled state of player controls by adding/removing flags
	 */
	EventSetControlsEnabled(const StringVector& vEnabled, const StringVector& vDisabled);

	/**
	 * @brief Copy constructor for a controls enabled event
	 */
	EventSetControlsEnabled(const EventSetControlsEnabled& rObj);

	// Member getters
	const std::vector<CONTROLS_MODE>& GetModesEnabled() const { return m_vEnabled; }
	const std::vector<CONTROLS_MODE>& GetModesDisabled() const { return m_vDisabled; }

	/**
	 * @brief GetString just gets the message.
	 */
	string GetString() const;

	/**
	 * @brief Once again we don't Resolve for this kind of event.
	 */
	void Resolve(unsigned long iTime) { }

private:
	std::vector<CONTROLS_MODE> m_vEnabled;
	std::vector<CONTROLS_MODE> m_vDisabled;
};

/**
 * @brief An event for trigger events to be checked by the mission system.
 *
 * EventTriggers of various types are hard-coded in the game
 *
 * @author Joe Forster
 */
class EventTrigger : public Event
{
public:
	/**
	 * @brief Construct a trigger event from a trigger name.
	 */
	EventTrigger(const String& rTriggerType);

	/**
	 * @brief Copy a trigger event
	 */
	EventTrigger(const EventTrigger& rEvent);

	/**
	 * @brief Get a string for debug purposes.
	 */
	virtual string GetString() const;

	/**
	 * @brief Get the type of the trigger that created this event.
	 */
	const String& GetTrigger() const { return m_strTrigger; }

	/**
	 * @brief Resolve doesn't do anything for a trigger event.
	 */
	virtual void Resolve(unsigned long iTime);

private:
	String m_strTrigger;
};

#endif // BFPCONTROLEVENTS_H_INCLUDED
