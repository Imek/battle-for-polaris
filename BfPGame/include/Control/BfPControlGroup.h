#ifndef BFPCONTROLGROUP_H_INCLUDED
#define BFPCONTROLGROUP_H_INCLUDED

#include <Control/BfPControlShip.h>
#include <Control/BfPUtil.h>
#include <Control/BfPEvents.h>
#include <CommandAI/AIEvaluator.h>

// States for a group controller. This is a state machine that transitions between them.
// STANDBY: Orbit the target object if it's set, otherwise don't move.
//		  Transition to GOTO if given any waypoints.
//		  Transition to FIGHT if enemies are nearby and we have no waypoints.
// GOTO:	Set and manage waypoints for navigating towards the set object/location.
//		  If destination reached, transition to STANDBY.
// FIGHT:   Enter into combat with nearby enemies, behaving depending on the stance, by giving group members targets to fight.
//		  Also manage individual ships switching between ATTACK/EVADE depending on threat.
//		  Transition to STANDBY if no more nearby enemies found.
//		  Transition to GOTO if given a waypoint by the faction controller (which supersedes combat).
enum GROUP_STATE { GST_STANDBY, GST_GOTO, GST_FIGHT };


/**
 * @brief A class for co-ordinating the actions of a ship group (unit used by the AI and user interface)
 *
 * @author Joe Forster
 */
class GroupController : public EntityController
{
public:
	GroupController(const BfPSectorState* pSectorState, VisibleState* pVisibleState,
		const AIEvaluator* pEvaluator);

	virtual ~GroupController();

	/**
	 * @brief Set AutoPilot on or off. Generally it's on, unless this is the player.
	 */
	void SetAutoPilot(bool bOn);
	/**
	 * @brief Get whether AutoPilot is on or off.
	 */
	bool GetAutoPilot() const;

	/**
	 * @brief Set the group leader (to be called at the start when we don't yet have one)
	 */
	void SetLeader(ShipController* pLeader);

	/**
	 * @brief Add a following ship to the group.
	 */
	void AddFollower(ShipController* pFollower);

	/**
	 * @brief Assuming it's been set, get the group ID.
	 */
	unsigned GetGroupID() const
	{
		assert(m_pGroupID);
		return *m_pGroupID;
	}

	/**
	 * @brief Get the destination sector, if we have one.
	 */
	const String* GetDestSector() const { return m_pDestSector; }

	/**
	 * @brief Get the destination location (object), if we have one.
	 */
	const String* GetDestBody() const { return m_pDestBody; }

	/**
	 * @brief Set the destination location as a body in a sector
	 */
	void SetDestLocation(const String& rSector, const String& rBody);

	/**
	 * @brief Set the destination location as just a sector
	 */
	void SetDestLocation(const String& rSector);

	/**
	 * @brief Unset the destination.
	 */
	void UnsetDestLocation();

	// Getters for the ship controllers contained within this group
	ShipController* GetLeader() { return m_pLeader; }
	const ShipController* GetLeader() const { return m_pLeader; }
	std::vector<ShipController*>& GetFollowers() { return m_vFollowers; }
	/**
	 * @brief Get whether this group has been wiped out (in which case it should be removed)
	 */
	bool IsDead() const { return m_bDead; }

	// Getters and setters for Ai state (group) and set stance
	GROUP_STATE GetAIState() const { return m_iState; }
	void SetAIState(GROUP_STATE iState) { m_iState = iState; }
	const String& GetAIStance() const { return m_strStance; }
	const ShipGroupStance* GetAIStanceType() const;
	void SetAIStance(const String& rStance);

	/**
	 * @brief Destroy all ships in this group
	 */
	void SelfDestruct();

	/**
	 * @brief Get the name of the faction that killed this group, assuming this group is dead.
	 */
	const String& GetKiller() const;

	/**
	 * @brief Main update function for a GroupController, to be called in the main control update cycle.
	 */
	void Update(unsigned long iTime);

	typedef std::map<String, std::vector<Vector3> > OrbitPaths;
	typedef StringRealMap OrbitDists;

private:
	// Re-used thingies for calculations (to avoid remaking local variables)
	static Quaternion		   m_qRot;


	VisibleState*			   m_pVisibleState;

	// The ship evaluator class for our AI player
	const AIEvaluator*		  m_pEvaluator;

	// Whether this GroupController is AI controlled or player controlled
	bool						m_bAutoPilot;

	// Remember these to make sure newly-added ships are valid
	int*						m_pGroupID;
	String*					 m_pFaction;
	String*					 m_pKiller;

	// AI state and stance for the group.
	GROUP_STATE				 m_iState;
	String					  m_strStance;

	// Navigation for a group: dealing with bodies as locations rather than points.
	String*					 m_pDestSector;
	String*					 m_pDestBody;
	// The next waypoint in the sequence to orbit an object; starts at 0 when not orbiting.
	unsigned					m_iNextOrbitWP;

	ShipController*			 m_pLeader;
	std::vector<ShipController*>	 m_vFollowers;
	bool						m_bDead; // Our group is dead and needs to be disposed of!
	// Relative position of each follower in the formation.
	std::vector<Vector3>*			m_pFormation;
	// Pre-calculated coordinates for orbiting all the object types in the world.
	OrbitPaths*				 m_pOrbitPaths;
	OrbitDists*				 m_pOrbitDists;

	// Our threat list, from the leader, which is recalculated every recalculation period (in the config)
	BfPUtil::ThreatMap		  m_mThreats;
	unsigned long			   m_iThreatRecalcTimer;


	// Private copy constructor (we don't want to copy a controller)
	GroupController(const GroupController& rCont): EntityController(NULL) {}

	// (functions here part of our Update function)
	/**
	 * @brief Check to see if we've lost any member ships, disposing of the lost ones
	 *
	 * Also re-assigns the group leader if the leader is dead.
	 */
	void CheckDestroyedMembers();

	// (Externally called parts of our update that need the full game state)
	/**
	 * @brief Check to see if we should and can begin the warping effect
	 */
	void CheckWarping();
	/**
	 * @brief Check to see if we should finish warping
	 *
	 * By finish warping here we mean that the ship has been doing the warp effect
	 * for the duration set in the config and needs to be removed from this sector
	 * and put in the destination one.
	 */
	void CheckWarped();

	// Update functions for each group state called by Update
	void UpdateStandby(unsigned long iTime);
	void UpdateGoto(unsigned long iTime);
	void UpdateFight(unsigned long iTime);

	/**
	 * @brief Gives pMyShip a target from rThreats, assigning attack/evade as appropriate.
	 *
	 * This function also alters rThreats by subtracting pMyShip's value from the threat
	 * selected as the target. This is done so that, given a number of smaller/weaker threats,
	 * a group of ships will select separate targets rather than all the same.
	 */
	void AssignTarget(ShipController* pMyShip, BfPUtil::ThreatMap& rThreats);

	/**
	 * @brief Check if we need to transition to GOTO (used in both STANDBY and FIGHT updates)
	 */
	void TransitionGoto(unsigned long iTime);

	/**
	 * @brief Calculate the relative formation positions, using our ship bounding radii
	 *
	 * This should be called when we have first finished putting the group together,
	 * and then every time the group changes afterwards.
	 */
	void CalculateFormation();

	/**
	 * @brief Calculate the relative orbit patrol positions, using our ship bounding radii
	 *
	 * Like CalculateFormation, this should be called when we have first finished putting
	 * the group together, and then every time the group changes afterwards.
	 */
	void CalculateOrbit();

	/**
	 * @brief Calculate the formation if necessary and return a relative position.
	 */
	const Vector3& GetFormationPos(size_t iShipNo);

	/**
	 * @brief Calculate (if necessary) and get the orbit distance for a particular body type
	 */
	Real GetOrbitDistance(const String& rBodyType);

	/**
	 * @brief Calculate (if necessary) and get the particular orbit path for a given body type.
	 */
	const std::vector<Vector3>& GetOrbitPath(const String& rBodyType);

	/**
	 * @brief Get the index of the orbit waypoint for the given body relative to the given position
	 */
	size_t GetClosestOrbitWP(const String& rBodyType, const Vector3& rRelativePos);


};

#endif // BFPCONTROLGROUP_H_INCLUDED
