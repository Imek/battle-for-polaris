#ifndef BFPCONTROLLERFACTION_H_INCLUDED
#define BFPCONTROLLERFACTION_H_INCLUDED

#include <Control/BfPControlState.h>
#include <Control/BfPControlGroup.h>
#include <Control/BfPEvents.h>

// A map of group ID to controller
typedef std::map<unsigned, GroupController*> GroupList;

/**
 * @brief The main interface for a player (AI or human) to control a faction.
 *
 * This includes all necessary methods for affecting desired player control, and the means with
 * which to observe the state from that faction's point of view.
 *
 * @author Joe Forster
 */
class FactionController
{
public:
	/**
	 * @brief Construct a faction controller, which requires its name and the state.
	 */
	FactionController(BfPState* pBfPState, String strFaction);

	virtual ~FactionController();

	/**
	 * @brief Set the AIEvaluator used by this faction's groups & ships for combat AI.
	 */
	void SetEvaluator(const AIEvaluator* pEvaluator) { m_pEvaluator = pEvaluator; }

	/**
	 * @brief Get a const pointer to the state of our faction.
	 */
	const FactionState* GetFactionState() const { return m_pMyFaction; }

	/**
	 * @brief Get the name of our faction.
	 */
	const String& GetFactionName() const { return m_pMyFaction->GetName(); }

	/**
	 * @brief Get whether this faction is the player faction.
	 */
	bool IsPlayer() const { return m_pMyFaction->GetFactionType()->GetController() == "Player"; }

	/**
	 * @brief Get the VisibleState: the interface to the game's main state for this faction.
	 */
	const VisibleState* GetObservedState() const { return m_pObservedState; }

	/**
	 * @brief Get a pointer to the flag-ship of our faction. Could be NULL if we have no ships!
	 */
	ShipController* GetFlagShip();

	/**
	 * @brief Get a const pointer to the flag-ship of our faction. Could be NULL if we have no ships!
	 */
	const ShipController* GetFlagShip() const;

	/**
	 * @brief Get a pointer to the flag-ship's group of our faction. Could be NULL if we have no ships!
	 */
	GroupController* GetFlagShipGroup();

	/**
	 * @brief Get a const pointer to the flag-ship's group of our faction. Could be NULL if we have no ships!
	 */
	const GroupController* GetFlagShipGroup() const;

	/**
	 * @brief See whether the faction has any ships left at all.
	 */
	bool HasShips() const;

	/**
	 * @brief Get a pointer to the group with the given ID number.
	 */
	GroupController* GetGroupWithID(unsigned iGroupID);

	/**
	 * @brief Get a const pointer to the group with the given ID number.
	 */
	const GroupController* GetGroupWithID(unsigned iGroupID) const;

	/**
	 * @brief Get the next available group ID.
	 */
	unsigned GetNextGroupID() const;

	/**
	 * @brief Check if the faction can purchase a group with the current selections and states.
	 */
	bool CanPurchaseGroup() const;

	/**
	 * @brief Purchase a ship group with some resource points, if we have enough.
	 */
	void PurchaseGroup();

	/**
	 * @brief Check if the faction can deplot a group with the current selections and states.
	 */
	bool CanDeployGroup() const;

	/**
	 * @brief Deploy the currently-selected deployent group at the currently-selected sector.
	 */
	void DeployGroup();

	/**
	 * @brief See if it is possible to execute a move order.
	 */
	bool CanMoveGroup() const;

	/**
	 * @brief Move the currently-selected ship group to the currently selected sector and location.
	 */
	void MoveGroup();

	/**
	 * @brief Get the ID of the selected owned group, or NULL if not set.
	 */
	const unsigned* GetSelectedGroup() const { return m_pSelectedGroup; }

	/**
	 * @brief Set this faction's selected owned group ID.
	 *
	 * @param iGroupNo		  The ID of the new group selected.
	 * @param bSelectSector	 Whether to select that group's sector if it's selected a second time.
	 */
	void SetSelectedGroup(unsigned iGroupNo, bool bSelectSector);

	/**
	 * @brief Unset this faction's owned group selection.
	 */
	void UnsetSelectedGroup();

	/**
	 * @brief Get the state of the currently-selected sector, if any.
	 *
	 * (NULL means that we may have a selected sector, but if so it's not visible)
	 */
	const BfPSectorState* GetSelectedSectorState() const;

	/**
	 * @brief Get the name of the currently-selected sector.
	 */
	const String& GetSelectedSector() const;

	/**
	 * @brief Set the GUI selected sector to rSector
	 */
	void SetSelectedSector(const String& rSector);
	/**
	 * @brief Unset the GUI sector selection
	 */
	void UnsetSelectedSector();

	/**
	 * @brief Get the GUI selection of an object (major body), or NULL if none.
	 */
	const String* GetSelectedObject() const { return m_pSelectedObject; }
	/**
	 * @brief Set the selected object ID (must correspond to a body in the selected sector)
	 */
	void SetSelectedObject(const String& rObject);
	/**
	 * @brief Unset the selected object.
	 */
	void UnsetSelectedObject();


	/**
	 * @brief Get the selected ship types for purchase.
	 */
	const StringVector* GetPurchaseTypes() const { return m_pPurchaseTypes; }

	/**
	 * @brief Set the selected ship types for purchase.
	 */
	void SetPurchaseTypes(const StringVector& rTypes);

	/**
	 * @brief Unset the selected purchase types
	 */
	void UnsetPurchaseTypes();


	/**
	 * @brief Get the selected deploy types for purchase.
	 */
	const StringVector* GetDeployTypes() const { return m_pDeployTypes; }
	/**
	 * @brief Set the selected deploy types for purchase.
	 */
	void SetDeployTypes(const StringVector& rTypes);
	/**
	 * @brief Unset the selected deploy types
	 */
	void UnsetDeployTypes();

//	EventHandler& GetAIEvents() { return m_pObservedState->GetAIEvents(); }

	/**
	 * @brief Work out the colour a sector should be in the GUI.
	 *
	 * Given what this faction knows and has selected.
	 */
	Vector3 GetSectorColour(const String& rSector) const;

	/**
	 * @brief Kill the faction by destroying all of its ships and setting m_bDead to true.
	 */
	void KillFaction();

	/**
	 * @brief See if the faction is dead.
	 */
	bool IsDead() const { return m_bDead; }

	/**
	 * @brief Call this at the start to deploy the starting groups from the configuration.
	 *
	 * It then makes the GroupController objects to control the new groups.
	 */
	void DeployStartingGroups();

	/**
	 * @brief The purpose of a FactionController's update function is to handle low-level (group) AI.*
	 *
	 * This function is called as part of the main control update cycle of the game.
	 */
	void Update(unsigned long iTime);


private:
	const FactionState*	 m_pMyFaction;
	VisibleState*		   m_pObservedState;
	const AIEvaluator*	  m_pEvaluator;

	GroupController*		m_pFlagShip; // Could be NULL if we have no groups!
	GroupList			   m_mMyGroups;

	// Info for the GUI.
	unsigned*			   m_pSelectedGroup;
	String*				 m_pSelectedSector;
	String*				 m_pSelectedObject;
	StringVector*		   m_pPurchaseTypes;
	StringVector*		   m_pDeployTypes;

	// Whether this faction is marked as "dead"
	bool					m_bDead;
	//String				  m_strKiller;

	// Private copy constructor (we don't want to copy a controller)
	FactionController(const FactionController& rCont) {}

	/**
	 * @brief A function to check if any groups have been wiped out and dispose of them.
	 *
	 * This function also reassigns the flag group to a new one, or to NULL if impossible,
	 * when our flag group is wiped out.
	 */
	void CheckDestroyedGroups();

	/**
	 * @brief A function to check the global event handler for faction death events with us as the object.
	 */
	void CheckDeathEvents();

	/**
	 * @brief Deploy a ship group that has already been purchased.
	 *
	 * @return a group controller if successful, NULL if not.
	 */
	GroupController* DeployGroup(
		const StringVector& rTypes, const String& rSector, unsigned iGroupID, bool bFlagGroup = false);

	/**
	* @brief Makes a controller for every existing ship group in the game; done once at the start.
	*
	* This sets m_pFlagShip to the flagship's group controller, and adds that plus the rest to the
	* controller list.
	*/
	void MakeGroupControllers();



};

#endif // BFPCONTROLLERFACTION_H_INCLUDED
