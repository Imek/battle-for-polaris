#ifndef BFPCONTROLWORLD_H_INCLUDED
#define BFPCONTROLWORLD_H_INCLUDED

#include <State/BfPState.h>
#include <Control/BfPEvents.h>
#include <Control/BfPControlFaction.h>

// For the test engine
#include <CommandAI/AILogicEngine.h>

/**
 * @brief The WorldController handles dynamic control of elements in the game world that
 *		do not fall under the FactionControllers.
 *
 * @author Joe Forster
 */
class WorldController
{
public:
	/**
	 * @brief The constructor for WorldController takes in the state that will be controlled.
	 */
	WorldController(BfPState* pState);

	/**
	 * @brief Destructor for WorldController.
	 */
	~WorldController();

	/**
	 * @brief Add the controller for a given faction
	 */
	void AddFactionController(const String& rName, FactionController* pController);

	/**
	 * @brief Update the game world.
	 */
	void Update(unsigned long iTime);

private:

	// Private copy constructor (we don't copy control stuff)
	WorldController(const WorldController& rCont) {}

	/**
	 * @brief Update the controller for each faction.
	 */
	void UpdateControllers(unsigned long iTime);

	/**
	 * @brief Handle control events from GlobalEvents.
	 */
	void UpdateEvents(unsigned long iTime);

	/**
	 * @brief Update the timers on active triggers.
	 */
	void UpdateTriggers(unsigned long iTime);

	/**
	 * @brief Check to see if a faction's death ends the game. Push a game over message box event if so.
	 */
	void CheckEventFactionDeath(EventFactionDeath* pEvent);

	/**
	 * @brief Check to see whether a trigger event fulfills a mission trigger, and send off the appropriate events if so.
	 */
	void CheckEventTrigger(EventTrigger* pEvent);

	/**
	 * @brief Execute a trigger given a trigger config.
	 */
	void ExecuteTrigger(const TriggerConfig& rTrigConf) const;

	// The state itself
	BfPState*								m_pState;
	// Faction controllers
	std::map<String, FactionController*>	 m_mControllers;

};

#endif // BFPCONTROLWORLD_H_INCLUDED
