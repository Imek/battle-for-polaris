#ifndef BFPCONTROLLERSHIP_H_INCLUDED
#define BFPCONTROLLERSHIP_H_INCLUDED

#include <Control/BfPControlState.h>
#include <Control/BfPUtil.h>

// States for an individual ship controller, generally set by its group controller.
// MANUAL: Manual control by the player (doesn't do anything automatic)
// GOTO: Follow any waypoint(s) set by the group controller (generally only if leader)
// FOLLOW: Follow the group leader.
// ATTACK: Seek, engage and fire upon any set targets.
// EVADE: Try to avoid being hit by nearby enemies or enemy missiles.
enum SHIP_STATE { SS_MANUAL, SS_GOTO, SS_FOLLOW, SS_ATTACK, SS_EVADE };

/**
 * @brief An entity controller generally controls some object or unit in the game, e.g. a ship.
 *
 * @author Joe Forster
 */
class EntityController
{
public:
	/**
	 * Constructor for EntityController, which just takes the sector state.
	 */
	EntityController(const BfPSectorState* pSectorState);

	virtual ~EntityController() {}

	/**
	 * @brief Get the sector state in which this entity is contained.
	 */
	const BfPSectorState* GetSector() const { return m_pSectorState; }

	/**
	 * @brief Set the sector state for this entity.
	 */
	void SetSector(const BfPSectorState* pNewSector)
	{
		assert(pNewSector);
		m_pSectorState = pNewSector;
	}

	/**
	 * @brief Generally an entity controller will do some form of control within this function it implements.
	 */
	virtual void Update(unsigned long iTime) = 0;

protected:
	const BfPSectorState*	  m_pSectorState;

};

/**
 * @brief a class to handle low-level ship AI and player controls of a ship
 *
 * @author Joe Forster
 */
class ShipController : public EntityController
{
public:
	/**
	 * @brief Construct a ship controller from a sector state, a ship state, and its controls.
	 *
	 * We give a const pointer to the ShipState and a non-const pointer to the controls; this
	 * way, a ship controller can only affect the state indirectly in allowed ways via the
	 * physics/control systems updates.
	 */
	ShipController(const BfPSectorState* pSectorState,
		const ShipState* pShipState, ControlState* pShipControls);
	/**
	 * @brief ShipController destructor
	 */
	virtual ~ShipController();

	/**
	 * @brief Get the const pointer to our ship's state.
	 */
	const ShipState* GetState() const { return m_pShipState; }

	/**
	 * @brief Set the state to a new one (for when warped to a new sector as it's been copied)
	 */
	void SetNewState(const ShipState* pNewState, ControlState* pNewControls);

	/**
	 * @brief Get the controls for our ship.
	 */
	ControlState* GetControlState() { return m_pShipControls; }

	/**
	 * @brief Get the destination waypoint, or NULL if not set.
	 */
	const Vector3* GetDestPoint() const { return m_pDestPoint; }

	/**
	 * @brief Set the destination waypoint to the given vector.
	 */
	void SetDestPoint(const Vector3& rPoint);

	/**
	 * @brief Unset the waypoint.
	 */
	void UnsetDestPoint();


	/**
	 * @brief Get the destination facing direction (e.g. for formation) or NULL if not set.
	 */
	const Vector3* GetDestDirection() const { return m_pDestDirection; }

	/**
	 * @brief Set the destination direction to the given unit vector.
	 */
	void SetDestDirection(const Vector3& rDirection);

	/**
	 * @brief Unset the destination direction.
	 */
	void UnsetDestDirection();


	/**
	 * @brief Get the state of the ship we are following.
	 */
	const ShipState* GetFollowedShip() const { return m_pFollowing; }

	/**
	 * @brief Get the relative position of our ship in the formation (NULL if unset).
	 */
	const Vector3* GetFormationPos() const { return m_pFormationPos; }

	/**
	 * @brief Set the state of the ship we should be following, and our relative formation position.
	 */
	void SetFollowedShip(const ShipState* pFollowing, const Vector3& rRelPos);

	/**
	 * @brief Unset the followed ship.
	 */
	void UnsetFollowedShip();


	/**
	 * @brief Get our target ship's ID (for combat). Throws an exception if not set!
	 */
	unsigned GetTargetShip() const;

	/**
	 * @brief See whether or not our target ship is set.
	 */
	bool HasTargetShip() const { return m_iTargetShip >= 0; }

	/**
	 * @brief Set the our target ship by ID.
	 */
	void SetTargetShip(unsigned iShipID) { m_iTargetShip = (int)iShipID; }

	/**
	 * @brief Unset our target ship.
	 */
	void UnsetTargetShip();

	/**
	 * @brief Get the AI state for this ship.
	 */
	SHIP_STATE GetAIState() const { return m_iState; }

	/**
	 * @brief Set the AI state for this ship.
	 */
	void SetAIState(SHIP_STATE iState) { m_iState = iState; }

	/**
	 * @brief Generate a collision avoidance vector if on a collision course, or return NULL if not.
	 */
	Vector3* GetAvoidCollisionVector();

	/**
	 * @brief Whether this ship is the group leader
	 */
	bool IsLeader() const { return m_iGroupLeader == -1; }

	/**
	 * @brief Get whether the ship is activating warp.
	 *
	 * NOTE: For now we don't use the warp trigger key any more, so this doesn't do much.
	 */
	bool GetWarpOn() { return true; }

	/**
	 * @brief Set this ship as the group leader (caller should handle removing/unsetting the previous leader!)
	 */
	void SetGroupLeader() { m_iGroupLeader = -1; }

	/**
	 * @brief Tell this ship that the ship with the given ID is the group leader.
	 */
	void SetGroupLeader(unsigned iID) { m_iGroupLeader = iID; }

	/**
	 * @brief See whether the conditions are right for this ship to begin warping.
	 */
	bool CanWarp(const String& rDestSector, const Vector3& rWarpPoint);

	/**
	 * @brief Destroy this ship.
	 */
	void SelfDestruct();


	/**
	 * @brief Unsets our target ship if it is no longer valid.
	 */
	void UpdateTargeting();

	/**
	 * @brief Main update function for a ShipController, which calls the appropriate state update.
	 */
	void Update(unsigned long iTime);

private:
	// Private copy constructor (we don't want to copy controllers)
	ShipController(const ShipController& rShip): EntityController(NULL) {}

	const ShipState*		m_pShipState;
	ControlState*		   m_pShipControls;

	// AI state for this ship (set by the group controller)
	SHIP_STATE			  m_iState;

	// These are targets of the various autopilot functions. They default to NULL/-1, i.e. no target.
	Vector3*				m_pDestPoint;
	Vector3*				m_pDestDirection;
	Vector3*				m_pAvoidCollDir; // Direction to turn to to avoid a collision
	const ShipState*		m_pFollowing;
	Vector3*				m_pFormationPos;
	// Could be -1 (meaning not set)
	int					 m_iTargetShip;
	// If this is -1, this ship is the group leader.
	int					 m_iGroupLeader;

	// Stuff for attack
	bool					m_bStrafingRun;

	/**
	 * @brief Update the facing vectors for our auto-tracking weapon(s) if any.
	 */
	void UpdateAutoAim();


	// Update functions for the states.
	void UpdateManual(unsigned long iTime);
	void UpdateGoto(unsigned long iTime);
	void UpdateFollow(unsigned long iTime);
	void UpdateAttack(unsigned long iTime);
	void UpdateEvade(unsigned long iTime);


	// Utility functions for ship AI (navigation mainly)

	/**
	 * @brief To be called at the start of state updates, this checks for and performs collision avoidance.
	 *
	 * Will take into account our ship type for types of projectile to dodge.
	 *
	 * @return true if avoiding collisions.
	 */
	bool CheckAvoidCollisions();

	/**
	 * @brief TurnToFace alters the controls for this ship so that it will turn to face a given direction.
	 *
	 * @return true if turning, false if finished turning.
	 */
	bool TurnToFace(const Vector3& rTargetDir);

	/**
	 * @brief Alter the ship controls so that the ship rolls to match the given up vector.
	 *
	 * @return true if rolling, false if finished roling.
	 */
	bool RollToMatch(const Vector3& rTargetUp);

	/**
	 * @brief Work out the Yaw/Pitch/Roll value to assign based on smooth turning configs.
	 *
	 * Get a value between -1 and 1 to turn based on an angle, a minimum angle to turn at all,
	 * and a maximum angle for smooth interpolation of turn amount between 0 and 1.
	 *
	 * @return True if we were above the minimum.
	 */
	static bool GetSmoothTurnValue(const Real& rAngle, const Real& rMin, const Real& rDropOff, Real& rValue);

	/**
	 * @brief Navigate to a position, doing the appropriate turning and thrust controls.
	 *
	 * @return false if we're already at the target position.
	 */
	bool NavigateTo(const Vector3& rTargetPos);

	/**
	 * @brief Generate a target heading for evasion in combat.
	 */
	Vector3 GetEvasionHeading();

};

#endif // BFPCONTROLLERSHIP_H_INCLUDED
