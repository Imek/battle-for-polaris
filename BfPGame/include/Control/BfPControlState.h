#ifndef BFPCONTROLSTATE_H_INCLUDED
#define BFPCONTROLSTATE_H_INCLUDED

#include <State/BfPState.h>
#include <Control/BfPEvents.h>

// ShipGroup: A group leader and a list of group followers.
typedef std::pair<const ShipState*, std::vector<const ShipState*> > ShipGroup;
typedef std::map<unsigned, ShipGroup > ShipGroupList;

/**
 * @brief A interface for the AI and Controller classes to use a BfPState in a restricted fashion.
 *
 * A faction controller is essentially a player of the game. This means that it can directly
 * "see" the state of the game, but not directly alter it. The only way a player can alter the
 * game is indirectly through ShipControllers, which control the individual ships owned by that
 * faction. The other use for this class is by the AI, in observing what a faction can observe.
 *
 * Faction actions, such as deploying a ship, are also done through the observed state.
 */
class VisibleState
{
public:
	VisibleState(BfPState* pBfPState, String strFaction);
	virtual ~VisibleState();

	const FactionState* GetFactionState() const;
	String GetFactionName() const;

	/**
	 * @brief See the score of a faction (anyone can see anyone's score)
	 */
	const long& GetFactionScore(const String& rFaction) const;

	/**
	 * @brief Get the number of ticks (period from the config) that have passed since the start of the game.
	 */
	unsigned long GetAgeInTicks() const;

	/**
	 * @brief Attempt to observe a sector, i.e. return the sector state. Returns NULL if impossible for my faction.
	 */
	const BfPSectorState* ObserveSector(String strSectorID) const;

	/**
	 * @brief Pretend to perform a purchase action, returning whether it was successful.
	 */
	bool CanPurchaseGroup(const StringVector& rTypes) const;

	/**
	 * @brief Perform a purchase action for real, returning whether it was successful.
	 */
	bool PurchaseGroup(const StringVector& rTypes);

	/**
	 * @brief Pretend to perform a deploy action, returning whether it was successful.
	 */
	bool CanDeployGroup(
		const StringVector& rTypes, const String& rSector,
		unsigned iGroupID, bool bFlagGroup) const;

	/**
	 * @brief Perform a deploy action for real, returning whether it was successful.
	 */
	bool DeployGroup(
		const StringVector& rTypes, const String& rSector,
		unsigned iGroupID, bool bFlagGroup);

	/**
	 * @brief Deploy the starting groups for our faction.
	 *
	 * This differs from regular deploy actions in that it is allowed to deploy groups
	 * the faction doesn't have to deploy, and also in non-visible sectors. However, this
	 * can only be done once at the start of the game.
	 */
	void DeployStartingGroups();

	/**
	 * @brief Get the flight controls of a particular ship.
	 *
	 * Through ControlStates, the FactionController can directly alter the controls of its ships,
	 * as with a player pressing keys/buttons or moving the mouse/joystick.
	 */
	ControlState* GetShipControls(String strSectorID, int iShipID);

	ControlState* GetShipControls(const BfPSectorState* pSector, const ShipState* pShip);

	/**
	 * @brief Get a list of all known groups owned by a faction in a given sector.
	 */
	ShipGroupList GetVisibleFactionGroups(const String& rFaction, const String& rSector) const;

	// TODO: AI Events for logic engine
//	const EventHandler& GetAIEvents() const { return m_pBfPState->GetAIEvents(); }

//	EventHandler& GetAIEvents() { return m_pBfPState->GetAIEvents(); }

	/**
	 * @brief Set a ship to begin the warping process once conditions have been met.
	 */
	void BeginShipWarping(unsigned iID, const String& rShipSector, const Vector3& rWarpDirection);

	/**
	 * @brief Finish the process of warping a ship to another sector.
	 *
	 * @return Returns the new state if the ship warped, otherwise null
	 */
	const ShipState* WarpShip(unsigned iID, const String& rFromSector, const String& rDestSector);

	/**
	 * @brief Unset my faction's flag ship, for when we no longer have any ships at all.
	 */
	void UnsetMyFlagShip();

	/**
	 * @brief Set my faction's flag ship to a new one by ship ID.
	 *
	 * It is the job of the FactionController to keep track of when its group leader dies.
	 */
	void SetMyFlagShip(unsigned iShipID);

	/**
	 * @brief Go through each ship in the given group and set its "I'm the flag group" flag to true.
	 *
	 * It is the job of the FactionController to do this once the old flag group is gone!
	 */
	void SetMyFlagGroup(unsigned iGroupID);

	/**
	 * @brief Set the leader flag of a group; note that this assumes the existing leader is removed!
	 */
	void SetGroupLeader(unsigned iGroupID, unsigned iShipID);

	/**
	 * @brief Kill a faction - set all of its sectors to neutral.
	 */
	void KillFaction(const String& rFaction);

	/**
	 * @brief Check a faction if it has been defeated by capture, creating events as appropriate.
	 */
	void CheckFactionDeath(const String& rFaction);

	/**
	 * @brief Check if a faction has no sectors left
	 */
	bool FactionHasNoSectors(const String& rFaction);

	// The following functions just call their corresponding functions in BfPState.
	/**
	 * @brief Add an enabling/disabling flag to the input state, generally from a trigger event.
	 */
	void AddPlayerInputFlag(CONTROLS_MODE iFlag) { m_pBfPState->AddPlayerInputFlag(iFlag); }

	/**
	 * @brief Find and remove a flag from the input state.
	 */
	void RemovePlayerInputFlag(CONTROLS_MODE iFlag) { m_pBfPState->RemovePlayerInputFlag(iFlag); }

	/**
	 * @brief Check to see if a certain type of control is enabled.
	 */
	bool CheckPlayerInputFlag(CONTROLS_MODE iFlag) const { return m_pBfPState->CheckPlayerInputFlag(iFlag); }


private:
	BfPState*	   m_pBfPState;
	FactionState*   m_pFactionState;
	// Whether we have deployed our starting group already (no cheating!)
	bool			m_bDeployedStartingGroups;

	// Private copy constructor (we don't copy control stuff)
	VisibleState(const VisibleState& rState) {}

	/**
	 * @brief Pretend to spawn a new group, returning whether it was successful.
	 */
	bool CanSpawnGroup(
		const StringVector& rTypes, const String& rSector,
		unsigned iGroupID, bool bFlagGroup) const;

	/**
	 * @brief Spawn a new group in the game world - assumes we have a controller and free ID for it.
	 */
	bool SpawnGroup(
		const StringVector& rTypes, const String& rSector,
		unsigned iGroupID, bool bFlagGroup);
};

#endif // BFPCONTROLSTATE_H_INCLUDED
