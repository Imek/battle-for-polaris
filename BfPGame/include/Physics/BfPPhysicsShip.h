#ifndef BFPPHYSICSSHIP_H_INCLUDED
#define BFPPHYSICSSHIP_H_INCLUDED

#include <Physics/PhysicsSystem.h>

#include <State/BfPStateSector.h>
#include <State/BfPStateShip.h>

using JFPhysics::EntityPhysics;
using JFPhysics::EntityForce;
using JFPhysics::RigidBody;

/**
 * @brief A class to link the ShipState class with the Physics module.
 *
 * This class keeps the state of a player up to date with regards to the physics model.
 * Note that this should generally only alter the basic motion-related attributes, i.e.
 * position and velocity. We also keep data for collision detection here.
 *
 * @author Joe Forster
 */
class ShipPhysics : public EntityPhysics
{
public:
	/**
	 * @brief ShipPhysics constructor from a ship state reference and a corresponding rigid body
	 */
	ShipPhysics(BfPSectorState* pWorld, unsigned iShipID, RigidBody* pBody);

	virtual ~ShipPhysics() {}

	/**
	 * @brief We need access to m_pBody here for collision detection and resolution.
	 */
	RigidBody* GetBody() { return (RigidBody*)m_pObject; }

	/**
	 * @brief Update function for a pairing between a ship and its rigid body.
	 *
	 * The update function for ShipPhysics performs the physics update for the rigid body,
	 * then updates the player state
	 *
	 */
	void Update(unsigned long iTime);

	/**
	 * @brief Get a pointer to the state of the ship.
	 */
	ShipState* GetShipState() { return (ShipState*)GetState(); }

};

/**
 * @brief Thrust force applied whenever a ship is firing its thrusters.
 *
 * Represents a force exerted by the ship's thrusters on itself, creating motion.
 * The force applied is proportional to the ship type and the percentage of thrust.
 * Also handles braking, which applies a force in the opposite direction of the
 * ship's current motion.
 *
 * @author Joe Forster
 */
class ShipThrustForce : public EntityForce
{
public:
	ShipThrustForce(BfPSectorState* pWorld, unsigned iShipID)
	: EntityForce(pWorld, "SHIP", iShipID) {}

	virtual ~ShipThrustForce() {}

	void Update(JFPhysics::Particle* pParticle, unsigned long iTime);

private:
	ShipState* GetShipState() { return (ShipState*)GetObjState(); }
};

/**
 * @brief A force similar to ShipThrustForce, except that it deals with turning of the ship.
 *
 * @author Joe Forster
 */
class ShipTurningForce : public EntityForce
{
public:
	ShipTurningForce(BfPSectorState* pWorld, unsigned iShipID)
	: EntityForce(pWorld, "SHIP", iShipID)
	, m_fYaw(0)
	, m_fPitch(0)
	, m_fRoll(0)
	{
	}

	virtual ~ShipTurningForce() {}

	void Update(JFPhysics::Particle* pParticle, unsigned long iTime);

private:
	// Current accumulated turn rates
	Real m_fYaw;
	Real m_fPitch;
	Real m_fRoll;

	ShipState* GetShipState() { return (ShipState*)GetObjState(); }
};


#endif // BFPPHYSICSSHIP_H_INCLUDED
