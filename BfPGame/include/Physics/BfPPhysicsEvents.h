#ifndef BFPPHYSICSEVENTS_H_INCLUDED
#define BFPPHYSICSEVENTS_H_INCLUDED

#include <BfPGameDefs.h>

#include <Util/Events.h>
#include <Physics/CollisionResolve.h>

#include <State/BfPState.h>

using JFPhysics::Particle;
using JFPhysics::ParticleCollision;

namespace JFPhysics
{

	/**
	 * @brief Collision between the ship and an invulnerable object.
	 *
	 * This kind of collision applies kinetic damage to the ship according to the velocities
	 * and masses of the involved objects. It also resolves the collision and applies the
	 * resultant impulse forces. For now, we just treat all collisions as particle collisions.
	 *
	 * @author Joe Forster
	 */
	class ShipObjectCollision : public Event
	{
	public:
		/**
		 * @brief Constructor from a collision and a ship state.
		 *
		 * @param	oCollision	The collision to be resolved.
		 * @param	rShip		The ship state to be changed (damage)
		 */
		ShipObjectCollision(ParticleCollision oCollision, ShipState* pShip);

		ShipObjectCollision(const ShipObjectCollision& rObj);

		virtual ~ShipObjectCollision() {}

		/**
		 * @brief Get a string for debug purposes.
		 */
		string GetString() const;

		/**
		 * @brief Resolve the collision, and apply mass-based collision damage to the ship.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void Resolve(unsigned long iTime);

	protected:
		ParticleCollision   m_oCollision;
		ShipState*		  m_pShip;
	};

	/**
	 * @brief Collision between a ship and another ship.
	 *
	 * This kind of collision applies kinetic damage to both ships according to the velocities
	 * and masses of the involved objects. It also resolves the collision and applies the
	 * resultant impulse forces. For now, we just treat all collisions as particle collisions.
	 *
	 * @author Joe Forster
	 */
	class ShipShipCollision : public Event
	{
	protected:
		ParticleCollision m_oCollision;
		ShipState* m_pShip1;
		ShipState* m_pShip2;

	public:
		/**
		 * @brief Constructor from a collision and ship states.
		 *
		 * @param	oCollision	The collision to be resolved.
		 * @param	rShip1		The ship state corresponding to the first particle.
		 * @param	rShip2		The ship state corresponding to the second particle.
		 */
		ShipShipCollision(ParticleCollision oCollision, ShipState* pShip1, ShipState* pShip2);

		ShipShipCollision(const ShipShipCollision& rObj);

		virtual ~ShipShipCollision() { }

		/**
		 * @brief Get a string for debug purposes.
		 */
		string GetString() const;

		/**
		 * @brief Resolve the collision, and apply mass-based collision damage to the ships.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void Resolve(unsigned long iTime);

	};

	/**
	 * @brief Collision between a projectile and a target ship.
	 *
	 * This kind of collision applies damage to the ship hit, and disregards the actual resolution of
	 * the collision. Instead, we apply an impulse set in the weapon's configuration, then destroy the
	 * projectile. Note that the first object in the ParticleCollision object should be the ship.
	 *
	 * @author Joe Forster
	 */
	class ShipProjectileCollision : public Event
	{
	public:
		/**
		 * @brief Constructor from a collision, a ship state and a projectile state.
		 *
		 * TODO: Make things consistent by turning the referencers into pointers (low priority)
		 *
		 * @param	oCollision	The collision to be resolved.
		 * @param   pSectorState  A pointer to the state of the game
		 * @param	rShip		The ship state to be changed (damage)
		 * @param	rProjectile	The state of the involved projectile.
		 */
		ShipProjectileCollision(
			ParticleCollision oCollision,
			const BfPSectorState* pSectorState, ShipState* pShip, ProjectileState* pProjectile);

		/**
		 * @brief Copy a collision event.
		 */
		ShipProjectileCollision(const ShipProjectileCollision& rObj);

		virtual ~ShipProjectileCollision() {}

		/**
		 * @brief Get a string for debug purposes.
		 */
		string GetString() const;

		/**
		 * @brief Resolve the collision, and apply mass-based collision damage to the ships.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void Resolve(unsigned long iTime);

	protected:
		ParticleCollision   m_oCollision;
		const BfPSectorState*  m_pSectorState;
		ShipState*		  m_pShip;
		ProjectileState*	m_pProjectile;

	};
}

#endif // BFPPHYSICSEVENTS_H_INCLUDED
