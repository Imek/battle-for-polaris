#ifndef BFPPHYSICSPROJECTILE_H_INCLUDED
#define BFPPHYSICSPROJECTILE_H_INCLUDED

#include <Physics/PhysicsSystem.h>

#include <State/BfPStateSector.h>
#include <State/BfPStateProjectile.h>

using JFPhysics::EntityPhysics;
using JFPhysics::EntityForce;

/**
 * @brief A class to link the projectile state module with the Physics module.
 *
 * This class keeps the state of a player up to date with regards to the physics model.
 * Note that this should generally only alter the basic motion-related attributes, i.e.
 * position and velocity. We also keep data for collision detection here.
 *
 * @author Joe Forster
 */
class ProjectilePhysics : public EntityPhysics
{
private:
	BfPSectorState* m_pWorld;
	unsigned m_iProjID;

public:
	/**
	 * @brief Constructor for a ProjectilePhysics object from a state and its particle.
	 */
	ProjectilePhysics(BfPSectorState* pWorld, unsigned iProjID, JFPhysics::Particle* pParticle, const BoundingVolume* pBV = NULL);

	virtual ~ProjectilePhysics() {}

	/**
	 * @brief Update function for a pairing between a projectile and the particle.
	 *
	 * The update function for ProjectilePhysics performs the physics update for the particle,
	 * then updates the particle state.
	 *
	 */
	void Update(unsigned long iTime);

	/**
	 * @brief Get a pointer to the state of the ship.
	 */
	ProjectileState* GetProjectileState() { return (ProjectileState*)GetState(); }

	unsigned GetID() const { return m_iProjID; }
};

/**
 * @brief Thrust force for a projectile. If the projectile is set as Homing in
 *		its configuration, this force also controls the homing.
 *
 * @author Joe Forster
 */
class ProjectileThrustForce : public EntityForce
{
public:
	ProjectileThrustForce(BfPSectorState* pWorld, unsigned iProjID)
	: EntityForce(pWorld, "PROJECTILE", iProjID) {}

	virtual ~ProjectileThrustForce() {}

	void Update(JFPhysics::Particle* pParticle, unsigned long iTime);

private:
	ProjectileState* GetProjectileState() { return (ProjectileState*)GetObjState(); }
};


#endif // BFPPHYSICSPROJECTILE_H_INCLUDED
