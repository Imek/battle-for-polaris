#ifndef PLAYERINPUT_H_INCLUDED
#define PLAYERINPUT_H_INCLUDED

#include <OgreCamera.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <BfPGameDefs.h>
#include <OgreRay.h>
#include <Ogre/OgreFramework.h>
#include <State/BfPState.h>
#include <Config/BfPConfig.h>
#include <Control/BfPControlFaction.h>
#include <UI/BfPGUI.h>
#include <Util/Events.h>

// Affects what is drawn on the scene rather than what GUI is drawn.
enum GAME_VIEW_MODE { GVM_SECTOR=0, GVM_MAP, GVM_LAST };

/**
 * @brief Class to handle player input and control of the camera.
 *
 * @author Joe Forster
 */
class BfPInput : public OIS::KeyListener, OIS::MouseListener
{
public:
	/**
	 * @brief Constructor for the BfP input system.
	 *
	 * @param The game's ogre system for which we process input.
	 */
	BfPInput(OgreFramework* pOgre);

	~BfPInput();

	// Get methods for the OIS things
	OIS::InputManager* GetInputManager() { return m_pInputMgr; }
	OIS::Keyboard* GetKeyboard() { return m_pKeyboard; }
	OIS::Mouse*	GetMouse() { return m_pMouse; }

	// Implemented callback methods for input events
	bool keyPressed(const OIS::KeyEvent &rEvent);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	/**
	 * @brief Handle a click event in the tactical menu, checking a ray to see if the user clicked an object.
	 */
	bool DoTacticalMouseSelection(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	/**
	 * @brief Handle a click event in the map menu, checking a ray to see if the user clicked an sector.
	 */
	bool DoMapMouseSelection(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	/**
	 * @brief Should we shut down the game?
	 */
	bool ShuttingDown() const { return m_bShutDown; }

	/**
	 * @brief Should we clear the game state and return to the menu?
	 */
	bool QuittingToMenu() const { return m_bQuitToMenu; }

	/**
	 * @brief Should we suspend updating the game?
	 */
	bool Paused() const { return m_pGUI->GetPause(); }

	/**
	 * @brief Is the game itself being updated?
	 */
	bool ShowingGame() const { return m_pGUI->ShowingGame(); }

	bool SetPlayer(FactionController* pPlayer);
	void SetJustSwitched() { m_bJustSwitched = true; }

	GUI_MODE GetGUIMode() const { return m_pGUI->GetGUIMode(); }
	CAMERA_MODE GetCameraMode() const { return m_pGUI->GetCamMode(); }
	GAME_VIEW_MODE GetGameMode() const { return m_iGameMode; }

	/**
	 * @brief Reset the input system to the state it is in right after being constructed.
	 *
	 * This includes destroying and setting to NULL m_pPlayer.
	 */
	void Reset();

	/**
	 * @brief  Timed update function for the input
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void Update(unsigned long iTime);

private:

	OIS::InputManager*		m_pInputMgr;
	OIS::Keyboard*			m_pKeyboard;
	OIS::Mouse*				m_pMouse;

	OgreFramework*		  m_pOgre;

	BfPGUI*				 m_pGUI;

	FactionController*	  m_pPlayer;
	CAMERA_MODE			 m_iCamMode;
	GAME_VIEW_MODE		  m_iGameMode;
	bool					m_bShift;
	bool					m_bShutDown;
	bool					m_bQuitToMenu;
	bool					m_bJustSwitched;

	Quaternion			  m_qCamOffset;
//	Quaternion			  m_qShipPrevOrientation;

	// Members for basic camera motion
	// (l/r, u/d, f/b, rollL/rollR)
	Vector4					m_vecKeyMotion; // Records the status of keyboard input for all modes.
	Vector2				 m_vecMousePanning; // Records whether the mouse is at +/- X/Y edges.
	Vector3				 m_vecMouseRotation;
	int					 m_iWheelZoom; // Zoom from the mouse wheel.
	int					 m_iZoom; // Zoom from keys.

	// Members for the chase & tactical cameras
	Vector3				 m_vecCamRelativePos; // Relative to the ship (chase or tac)
	Quaternion			  m_qCamRelativeOrientation;
	Real					m_fCamCurrentZoom;
	Real					m_fCamSetZoom;
	Real					m_fCamZoomVel;

	Vector3				 m_vecChaseCamIdealPos;
	Degree				  m_fChaseCamXFromIdeal;
	Degree				  m_fChaseCamYFromIdeal;
	Degree				  m_fChaseCamXVelocity;
	Degree				  m_fChaseCamYVelocity;

	// Timer used for the lingering death camera
	int					 m_iChaseCamDeathTimer;

	// Counter used for the shift-turn tutorial
	Degree				  m_oShiftTurned;

	/**
	 * @brief Toggle the camera mode
	 *
	 * @param bPrev Toggle to prev (next if false)
	 */
	void ToggleMode(bool bPrev = true);

	/**
	 * @brief Get our player's faction controller.
	 */
	FactionController* GetPlayer() { return m_pPlayer; }

	/**
	 * @brief Update function for the free camera movement
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateFreeCamera(unsigned long iTime);

	/**
	 * @brief Update function for the chase view mode.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateChaseMode(unsigned long iTime);

	/**
	 * @brief Check for input trigger events for the chase mode.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateChaseTutorial(unsigned long iTime);

	/**
	 * @brief Update the chase camera.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateChaseCamera(unsigned long iTime);


	/**
	 * @brief Update function for the tactical mode.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateTacticalMode(unsigned long iTime);

	/**
	 * @brief Check for input trigger events for the tactical mode.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateTacticalTutorial(unsigned long iTime);

	/**
	 * @brief Update the tactical camera.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateTacticalCamera(unsigned long iTime);


	/**
	 * @brief Update function for the map mode
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateMapMode(unsigned long iTime);

	/**
	 * @brief Check for input trigger events for the map mode.
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateMapTutorial(unsigned long iTime);

	/**
	 * @brief Update function for the map camera's movement
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateMapCamera(unsigned long iTime);




	/**
	 * @brief Check to see if the mouse is at the edge of the screen and scroll it if so
	 */
	void UpdateCameraScrolling(unsigned long iTime);

	/**
	 * @brief Update function to rotate the chase camera position with the mouse
	 *
	 * @param   iTime   Number of milliseconds since the previous frame.
	 */
	void UpdateMouseRotation(unsigned long iTime);

	/**
	 * @brief Update the position of the actual OGRE camera from its relative position
	 */
	void UpdateCameraPosition(unsigned long iTime);

};

#endif // PLAYERINPUT_H_INCLUDED
