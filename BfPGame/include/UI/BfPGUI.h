#ifndef BFPGUI_H_INCLUDED
#define BFPGUI_H_INCLUDED

// Currently GameGUI just contains common stuff needed even if the GUI system
// is turned off; ideally only game-specific code should be in this file.
#include <UI/GameGUI.h>

//#define BFP_NO_CEGUI // TEMPORARY FOR UPGRADE!


#ifndef BFP_NO_CEGUI

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

#endif // BFP_NO_CEGUI

#include <OgreRenderOperation.h>

#include <Ogre/OgreFramework.h>

#include <Util/Events.h>
#include <Control/BfPControlFaction.h>
#include <CommandAI/AIAbstractState.h>
#include <CommandAI/AIPlayerModel.h>
#include <CommandAI/AILogicEngine.h>


enum GUI_MODE { GM_MENU, GM_GAME };

enum CAMERA_MODE { CM_CHASE = 0, CM_TACTICAL, CM_MAP, CM_LOGICENGINE, CM_FREELOOK, CM_LAST };

enum TARGET_MODE { TM_SECTORS = 0, TM_GROUPS, TM_ENEMIES, TM_BODIES, TM_LAST };

// TODO: This GUI code could possibly use some tidying up, e.g. by splitting it into separate classes.
/**
 * @brief A class to handle the graphical user interface part of the game.
 *
 * @author Joe Forster
 */
class BfPGUI : public JFUtil::Singleton<BfPGUI>
{
public:
	BfPGUI(CAMERA_MODE iMode);

	~BfPGUI();

	void SetPlayer(FactionController* pPlayer);
	void UnsetPlayer();

	void Init();

	void SetMode(CAMERA_MODE iMode);
	CAMERA_MODE GetCamMode() const { return m_iCamMode; }
	GUI_MODE GetGUIMode() const { return m_iGUIMode; }
	bool ShowingGame() const { return m_iGUIMode == GM_GAME && !m_bPause; }

	void SetShift(bool bVal) { m_bShift = bVal; }

	/**
	 * @brief Reset the mouse position to the centre of the screen.
	 */
	void ResetMouse();

	/**
	 * @brief Move the mouse by the given offset.
	 */
	void MoveMouse(Real fX, Real fY);

	const Real& GetMouseX() const { return m_fMouseX; }
	const Real& GetMouseY() const { return m_fMouseY; }

	bool GetShift() const { return m_bShift; }
	bool GetPause() const { return m_bPause; }
	bool GetQuit() const { return m_bQuit; }
	bool GetEndGame() const { return m_bEndGame; }

	/**
	 * @brief Pause the game by showing a message box.
	 */
	void ShowPauseDialogue();

	/**
	 * @brief Request that we quit to the menu (by showing a confirm box)
	 */
	void QuitToMenu();

	/**
	 * @brief Toggle the type of entity being targetted
	 *
	 * @param bNext is true if next, false if previous
	 */
	void ToggleTargetMode(bool bNext = true);

	/**
	 * @brief Set the target mode
	 */
	void SetTargetMode(TARGET_MODE iMode);

	/**
	 * @brief Get the target mode
	 */
	TARGET_MODE GetTargetMode() const { return m_iTargMode; }

	/**
	 * @brief Switch to the next/previous target for this mode (in the player's controller)
	 *
	 * @param bNext prev target if true, previous if false.
	 */
	void SwitchTarget(bool bNext = true);

	/**
	 * @brief If there are enemies, switch to ships target mode and target the closest enemy.
	 */
	void TargetClosestEnemy();

	/**
	 * @brief If there are enemies, switch to enemies mode and switch to the next/previous enemy
	 *
	 * @param bNext prev enemy if true, next if false.
	 */
	void TargetNextEnemy(bool bPrev = true);

	/**
	 * @brief Unselect the target for the current target mode.
	 */
	void UnselectTarget(TARGET_MODE iMode);
	void UnselectTarget();
	void ClearTargets();
	bool HasTargets();

	/**
	 * @brief Toggle the stance for the selected group, if we have one.
	 */
	void ToggleGroupStance(bool bNext = true);

	// Mouse callbacks

	/**
	 * @brief Inject a mouse button down event into the CEGUI system
	 */
	bool MousePressed(OIS::MouseButtonID id);

	/**
	 * @brief Inject a mouse button up event into the CEGUI system
	 */
	bool MouseReleased(OIS::MouseButtonID id);

	// CEGUI Event callbacks!


#ifndef BFP_NO_CEGUI

	// TODO: Clean up this horribly messy way of selecting stuff (replace Listbox with Itemlistbox?)
	static size_t* GetItemClicked(CEGUI::Listbox* pList, const CEGUI::Vector2f& rRelativeTo);

	// Selection events for tactical/map screen
	static bool CEGUIPurchaseMenuClicked(const CEGUI::EventArgs& e);
	static bool CEGUIDeployMenuClicked(const CEGUI::EventArgs& e);
	static bool CEGUISectorMenuClicked(const CEGUI::EventArgs& e);
	static bool CEGUIGroupMenuClicked(const CEGUI::EventArgs& e);
	static bool CEGUIEnemyMenuClicked(const CEGUI::EventArgs& e);
	static bool CEGUIBodyMenuClicked(const CEGUI::EventArgs& e);
	// Button events for tactical/map screen
	static bool CEGUIClearButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIStanceButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIMoveButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIExecuteButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIPlusButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIMinusButtonClicked(const CEGUI::EventArgs& e);
	// Selection events for abstract state screen
	static bool CEGUIFactionMenuClicked(const CEGUI::EventArgs& r);
	static bool CEGUIUnitMenuClicked(const CEGUI::EventArgs& r);
	// Button events for menu screen
	static bool CEGUISkirmishButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUISkirmishScenarioConfigClicked(const CEGUI::EventArgs& e);
	static bool CEGUIQuitButtonClicked(const CEGUI::EventArgs& e);
	// Button events for message boxes
	static bool CEGUIMessageBoxOKButtonClicked(const CEGUI::EventArgs& e);
	static bool CEGUIEndGameButtonClicked(const CEGUI::EventArgs& e);

#endif // BFP_NO_CEGUI


	/**
	 * @brief Reset the GUI system to the state it is in right after being constructed.
	 *
	 * This sets m_pPlayer to NULL but doesn't destroy it, so the caller should make sure it
	 * does destroy it!
	 */
	void Reset(CAMERA_MODE iMode);

	/**
	 * @brief The main update function of the GUI, which changes what is displayed if appropriate.
	 */
	void Update(unsigned long iTime);

private:
	GUI_MODE					m_iGUIMode;
	CAMERA_MODE				 m_iCamMode;
	TARGET_MODE				 m_iTargMode;
	bool						m_bShift;
	bool						m_bPause;
	bool						m_bJustUnpaused;
	Real						m_fTimeFactorBeforePause;
	bool						m_bQuit;
	bool						m_bBeginGame;
	bool						m_bEndGame;

	Real						m_fMouseX;
	Real						m_fMouseY;

	FactionController*		  m_pPlayer;

	// Lists of the data displayed in CEGUI
	typedef std::multimap<unsigned, String> PurchaseMap;
	typedef std::multimap<Real, const ShipState*> ShipMap;
	typedef std::multimap<unsigned, ShipMap > GroupMap;
	typedef std::multimap<Real, const BodyInstance*> BodyMap;

	PurchaseMap			 m_mPurchaseList;
	StringVector			m_vDeployList;
	String				  m_strSelectedShipType;
	unsigned				m_iNoToBuy;
	bool					m_bPlusMinusClicked;

	StringVector			m_vSectors;
	StringVector			m_vLinks;
	GroupMap				m_mGroups;
	ShipMap				 m_mEnemies;
	BodyMap				 m_mBodies;

	std::list<String>			m_lMessages;

	// Stuff for the logic engine GUI
	String									m_strSelectedFaction;
	std::map<String, unsigned*>			   m_mSelectedUnits;
	std::map<String, std::vector<unsigned> >  m_mFactionUnits;
	unsigned								  m_iSelectedTick;
	unsigned								  m_iLastNoTicks;

	/**
	 * @brief Initialise the new camera mode, setting up the CEGUI window and registering events
	 */
	void InitMode();

	/**
	 * @brief Destroy the window for the current mode (you must InitMode your new mode afterwards!)
	 */
	void DestroyMode();

	String GetActiveTargetTabName();
	String GetActiveBuildTabName();

	/**
	 * @brief Set the target mode from a string (DOESN'T set the proper tab like toggle!)
	 */
	void SetTargetMode(const String& rMode);
	void SwitchTargetSector(bool bPrev);
	void SwitchWarpTarget(bool bPrev);
	void SwitchTargetGroup(bool bPrev);
	void SwitchTargetEnemy(bool bPrev);
	void SwitchTargetBody(bool bPrev);

	/**
	 * @brief Assuming a message box isn't already visible, show a message box with the given message and title.
	 */
	void ShowMessageBox(const String& rMessage, const String& rTitle, bool bGameOver);

	/**
	 * @brief Assuming a message box isn't already visible, show a confirmation box.
	 *
	 * @param rMessage  The message content for the box
	 * @param rTitle		The title for the dialogue
	 * @param bYesEndsGame  Whether clicking Yes ends the game.
	 */
	void ShowConfirmBox(const String& rMessage, const String& rTitle, bool bYesEndsGame);

	/**
	 * @brief Hide the message box, unpausing the game and changing the mouse pointer back.
	 *
	 * Assumes the message box is already visible!
	 */
	void HideDialogue();

	/**
	 * @brief See if a message box is visible; you should check this before calling ShowMessageBox!
	 */
	bool DialogueVisible();

	/**
	 * @brief Set the paused state of the game.
	 */
	void SetPaused(bool bPause);

	// Update message box events
	void UpdateMessageBoxes();
	// Update functions for the chase and tactical interfaces.
	void UpdateChaseHUD();
	// Update the pointer on the chase HUD that shows where the current target is.
	void UpdateTargetPointers();
	/**
	 * @brief Return the position of the currently-selected target for this mode, or NULL if none.
	 */
	const Vector3* GetCurrentTargetPos();

	/**
	 * @brief Perform the updates for the tactical/map GUI and its components.
	 */
	void UpdateTacticalHUD();

	/**
	 * @brief Messaging system (pop-ups) update function.
	 */
	void UpdateMessages();

	/**
	 * @brief Perform the updates for the logic engine debugging / info GUI mode.
	 */
	void UpdateLogicEngineHUD();

	// CEGUI-specific private members

#ifndef BFP_NO_CEGUI

	// CEGUI objects
	CEGUI::Window*			  m_pMenuWindow;
	CEGUI::Window*			  m_pHUDWindow;
	CEGUI::Window*			  m_pTacticalWindow;
	CEGUI::Window*			  m_pLogicEngineWindow;

	/**
	 * @brief Convert an OIS mouse button ID into a CEGUI one (returns NULL if not recognised)
	 */
	CEGUI::MouseButton GetCEGUIMouseButton(OIS::MouseButtonID id);

	/**
	 * @brief Get the message box (depends on what mode we're in; could be NULL)
	 */
	CEGUI::FrameWindow* GetMessageBox();

	/**
	 * @brief Get the confirm box (like we get the message box)
	 */
	CEGUI::FrameWindow* GetConfirmBox();

	/**
	 * @brief Grab the pointer for the target menu in the active window.
	 */
	CEGUI::TabControl* GetTargetMenu();

	// Chase & tactical update functions
	// TODO: De-couple from CEGUI by not passing around the menus?

	void UpdateChaseTargetMenu(CEGUI::TabControl* pTargetMenu);
	void UpdateTacticalTargetMenu(CEGUI::TabControl* pTargetMenu);
	void UpdateBuildMenu(CEGUI::TabControl* pBuildMenu);

	void UpdatePurchaseList(CEGUI::Listbox* pList);
	void UpdateDeployList(CEGUI::Listbox* pList);

	void UpdateSectorList(CEGUI::Listbox* pList);
	void UpdateWarpList(CEGUI::Listbox* pList);

	void UpdateGroupList(CEGUI::Listbox* pList);
	void UpdateEnemyList(CEGUI::Listbox* pList);
	void UpdateBodyList(CEGUI::Listbox* pList);

	// Logic engine HUD updates

	void UpdateInfoMenu(CEGUI::TabControl* pInfoMenu);
	void UpdateFactionInfo(CEGUI::MultiLineEditbox* pInfoBox);
	void UpdateUnitInfo(CEGUI::MultiLineEditbox* pInfoBox);
	String GetUnitInfo(const AbstractUnitState* pUnit);
	void UpdateModelInfo(CEGUI::MultiLineEditbox* pInfoBox);
	void UpdateLearningInfo(CEGUI::MultiLineEditbox* pInfoBox);
	String GetPlayerModelInfo(const AIPlayerModel* pModel);

	void UpdateUnitSelectMenu(CEGUI::TabControl* pUnitSelectMenu);
	void UpdateFactionList(CEGUI::Listbox* pList);
	void UpdateUnitList(CEGUI::Listbox* pList);

	void UpdateTickMenu(CEGUI::Window* pWindow);

#endif // BFP_NO_CEGUI

};

#endif // BFPGUI_H_INCLUDED
