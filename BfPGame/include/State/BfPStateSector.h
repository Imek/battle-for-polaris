#ifndef BFPSTATESECTOR_H_INCLUDED
#define BFPSTATESECTOR_H_INCLUDED

// Engine imports
#include <State/GameState.h>
#include <State/SectorState.h>

// Game-specific imports
#include <State/BfPStateProjectile.h>
#include <State/BfPStateShip.h>

#include <Config/BfPConfigMap.h>
#include <Config/BfPConfigShips.h>

/**
 * @brief A collidable can either be an entity state or a body ID.
 *
 * TODO: We could tidy up the collidable system by creating bodies as entities/objects
 * in the game world and using those as collidables. This class could then be moved
 * to the generic GameState code (or probably just removed entirely and a Collidable
 * flag added to ObjState).
 *
 * @author Joe Forster
 */
class Collidable
{
public:
	/**
	 * @brief Construct an entity collidable.
	 */
	Collidable(const EntityState* pEntity);

	/**
	 * @brief Construct a body collidable from a sector ID and a body ID.
	 */
	Collidable(const String& rSector, const String& rBody);
	bool IsEntity() const { return m_bIsEntity; }

	/**
	 * @brief Assuming that this is an entity, get the entity state.
	 */
	const EntityState* GetEntity() const;

	/**
	 * @brief Assuming that this is not an entity, get the body instance from the config.
	 */
	const BodyInstance* GetBody() const;

	const String& GetSectorName() const { return m_strSector; }
	const String& GetBodyName() const { return m_strBody; }

private:
	bool				m_bIsEntity;
	const EntityState*  m_pEntity;
	String			  m_strSector;
	String			  m_strBody;
};

typedef std::multimap<Real, Collidable>	DistCollidableMap;

/**
 * @brief Contains the state of a given sector: ship groups, projectiles, and any other dynamic objects.
 *
 * Also deals with matters of ownership and capturing.
 *
 * @author Joe Forster
 */
class BfPSectorState : public SectorState
{
public:
	/**
	 * @brief Construct a BfPSectorState from a type in the configuration.
	 */
	BfPSectorState(const SectorType* pSectorConf);

	/**
	 * @brief Copy constructor for BfPSectorState copies the objects
	 */
	BfPSectorState(const BfPSectorState& rState);
	~BfPSectorState();

	/**
	 * @brief Get the configuration associated with this sector.
	 */
	const SectorType* GetSectorType() const;

	/**
	 * @brief Update oooldowns, shield/capacitor regeneration, and fired projectiles.
	 *
	 * When a player is firing and has reached its weapon's cooldown time, new projectiles
	 * are created. Existing projectiles have their lifetimes updated. Note that the physics
	 * update should be called before this is called, or an assertion will fail. We also need
	 * to regenerate shields and capacitor at the set rate.
	 *
	 * We also check to see if a player is occupying this sector - i.e. they're the only
	 * faction with ships in it - and if so we
	 *
	 * TODO: It makes more design sense to move this to a Control class of some kind
	 *
	 * @param	iTime	The number of milliseconds since the last update
	 */
	void Update(unsigned long iTime);

	/**
	 * @brief Choose a spawn point for a bounding volume, finding a passable one if possible.
	 */
	Vector3 ChooseRandomSpawnPoint(const BoundingVolume* pVol) const;

	/**
	 * @brief Pretend to add a new ship and see if it was successful.
	 */
	bool CanAddShip(String strType, String strFaction,
		unsigned iGroupID, bool bFlagGroup, const Vector3& rPoint) const;

	/**
	 * @brief Allocate and add a new ship to this sector, given a type. Return a pointer
	 * to the newly-created ShipState if successful, otherwise NULL.
	 */
	const ShipState* AddShip(String strType, String strFaction,
		unsigned iGroupID, bool bFlagGroup, const Vector3& rPoint);

	/**
	 * @brief Add an existing ship state to this sector at a given warp point.
	 *
	 * Note that it is the job of the caller to ensire that pState no longer exists in any
	 * other sector!
	 */
	bool AddWarpedShip(ShipState* pState, const Vector3& rPoint);

	/**
	 * @brief Get all the ships with a given group ID.
	 *
	 * @return A pointer to the leader.
	 */
	const ShipState* GetShipGroup(const String& rFaction, unsigned iGroupID,
		std::vector<const ShipState*>& rMembers) const;

	bool ShipGroupExists(const String& rFaction, int iGroupID) const;

	/**
	 * @brief Remove a ship that was just added using AddShip (before Update is called!)
	 *
	 * @return Success?
	 */
	bool RemoveNewShip(int iID);

	/**
	 * @brief Get all of the ships in this sector owned by a given faction, adding them to the provided vector.
	 *
	 * @param   strFaction  the ID of the faction.
	 * @param   rShips	  a reference to the vector to be added to
	 *
	 */
	void GetFactionShips(String strFaction, std::vector<const ShipState*>& rShips) const;

	void GetFactionShips(String strFaction, std::vector<ShipState*>& rShips);

	/**
	 * @brief A function like GetFactionShips that returns the vector instead.
	 */
	std::vector<const ShipState*> GetFactionShips(String strFaction) const;

	/**
	 * @brief Get the faction that currently "owns" this sector, or NULL if neutral.
	 */
	const String* GetOwner() const { return m_pOwner; }

	/**
	 * @brief Set the owning faction of this sector to some value (faction ID).
	 */
	void SetOwner(String strFaction);

	/**
	 * @brief Unset the owner - i.e. set to neutral.
	 */
	void UnsetOwner() { delete m_pOwner; m_pOwner = NULL; }


	/**
	 * @brief Get a list of all collidable entities in this sector.
	 */
	std::vector<const EntityState*> GetCollidables() const;


	/**
	 * @brief Get a sorted (squared)distance-entity map of collidables within a range of a point.
	 *
	 * rPoint	   The point from which to search.
	 * rDist		The maximum distance.
	 * pShipType	If given, the ship type specifying the types of projectile to include.
	 */
	DistCollidableMap GetCollidablesWithin(
		const Vector3& rPoint, const Real& rDist, const ShipType* pShipType = NULL) const;


	/**
	 * @brief Get whether the owner has changed since the last time this function was called.
	 */
	bool GetOwnerChanged()
	{
		if (m_bOwnerChanged)
		{
			m_bOwnerChanged = false;
			return true;
		}
		else return false;
	}

	const String* GetLastOwner() const { return m_pLastOwner; }

private:
	String*		 m_pOwner;
	String*		 m_pLastOwner;
	bool			m_bOwnerChanged;

	// How long a faction has dominated this sector
	unsigned long   m_iTimeHeldByOccupier;
	// The last faction to be the only one with ships in this sector
	String		  m_strLastOccupier;

	void UpdateShips(unsigned long iTime);
	void UpdateProjectiles(unsigned long iTime);
	void UpdateCapturing(unsigned long iTime);

	/**
	 * @brief See if a bounding volume can spawn in this sector without an obstacle.
	 */
	bool CanSpawnVolume(const BoundingVolume* pVol) const;

	/**
	 * @brief Get a random closest passable point to a spawn point given a bounding volume.
	 *
	 * @param   pVol	Volume to move
	 */
	bool GetClosestPassablePoint(BoundingVolume* pVol) const;

	/**
	 * @brief Add time to the capture timer for a faction
	 */
	void AddCaptureTime(unsigned long iTime, const String& rFaction);

	/**
	 * @brief Set the previous owner value used for the capturing system.
	 */
	void SetLastOwner(const String& rOwner);
};

#endif // BFPSTATESECTOR_H_INCLUDED
