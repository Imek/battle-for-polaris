#ifndef BFPSTATEPROJECTILE_H_INCLUDED
#define BFPSTATEPROJECTILE_H_INCLUDED

#include <State/BfPStateEntity.h>

#include <Config/BfPConfigWeapons.h>

/**
 * @brief Holds and manages the state of a weapon projectile in the game world.
 *
 * @author Joe Forster
 */
class ProjectileState : public BfPEntityState
{
public:
	/**
	 * @brief Construct a projectile state from data.
	 *
	 * @param iID			   The unique Entity ID number for the projectile.
	 * @param iOwnerID		  The ID of the ship that fired this projectile.
	 * @param strOwnerFaction   The faction that owned the ship that fired this projectile.
	 * @param iOwnerTarget	  The ID of the owning ship's target ship, or -1 if not set.
	 * @param strType		   The weapon type for this projectile.
	 * @param qOrientation	  The initial orientation of this projectile.
	 * @param strSectorID	   The sector in which this entity is located.
	 */
	ProjectileState(unsigned iOwnerID, String strOwnerFaction, int iOwnerTarget,
					String strType, Vector3 vecPos, Quaternion qOrientation, String strSectorID);

	/**
	 * @brief Our implementation of EntityState's GetEntityType gets it from the appropriate config.
	 */
	virtual const EntityType* GetEntityType() const;

	/**
	 * @brief Get the EntityType cast as a WeaponType for convenience.
	 */
	const WeaponType* GetWeaponType() const;

	/**
	 * @brief Get the time this projectile has remaining.
	 */
	const long& GetTimeLeft() const { return m_iLifetime; }

	/**
	 * @brief Call the generic ApplyDamage function, then trigger an explosion if was destroyed.
	 */
	virtual void ApplyDamage(int iAmount);

	/**
	 * @brief Override GetOwnerType returning SHIP. Projectile owners are always ships, at least for now.
	 */
	virtual const String& GetOwnerType() const { static String s = "SHIP"; return s; }

	/**
	 * @brief Override GetOwnerID to return the ID of the ship that fired us.
	 */
	virtual unsigned GetOwnerID() const { return m_iOwnerID; }

	/**
	 * @brief Get the ship ID of this projectile's owner's target.
	 *
	 * @return A ship ID, or -1 if not set.
	 */
	int GetOwnerTarget() const { return m_iOwnerTarget; }

	/**
	 * @brief Alter the ship ID of this projectile's owner's target.
	 */
	void SetOwnerTarget(int iNewVal) { assert(iNewVal >= 0); m_iOwnerTarget = iNewVal; }

	/**
	 * @brief Unset (to -1) the ship ID of this projectile's owner's target.
	 */
	void UnsetOwnerTarget() { m_iOwnerTarget = -1; }

	/**
	 * @brief The control update for a projectile handles things like the explode trigger.
	 */
	void Update(unsigned long iTime);

private:
	long		m_iLifetime;
	unsigned	m_iOwnerID;
	int		 m_iOwnerTarget;
};

#endif // BFPSTATEPROJECTILE_H_INCLUDED
