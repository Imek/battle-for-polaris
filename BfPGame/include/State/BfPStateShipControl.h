#ifndef BFPSTATESHIPCONTROL_H_INCLUDED
#define BFPSTATESHIPCONTROL_H_INCLUDED

#include <State/GameState.h>

#include <Config/BfPConfigShips.h>
#include <Config/BfPConfigWeapons.h>


/**
 * @brief The state of a weapon equipped on a ship.
 *
 * @author Joe Forster
 */
class WeaponState
{
public:
	WeaponState(String strType);

	const WeaponType* GetType() const;
	const String& GetTypeName() const { return m_strType; }

	/**
	 * @brief Reset the values of this weapon state to its initial values.
	 */
	void Reset();

	int GetCooldown() const { return m_iCooldown; }
	int GetAmmo() const { return m_iAmmo; }

	/**
	 * @brief See if this weapon is ready to be fired.
	 */
	bool ReadyToFire() const { return (m_iCooldown <= 0 && m_iAmmo > 0); }

	/**
	 * @brief Attempt to fire this weapon, returning true if successful.
	 *
	 * The sector state should then create a new projectile.
	 */
	bool Fire();

	/**
	 * @brief The control update for a weapon handles the cooldown.
	 */
	void Update(unsigned long iTime);

private:
	String	  m_strType;
	int		 m_iCooldown;
	int		 m_iAmmo;

	// How many new projectiles (generally 0 or 1) created in the last update
	unsigned	m_iNewProjectiles;
};

/**
 * @brief A class to represent a list of WeaponState objects associated with a ship.
 *
 * @author Joe Forster
 */
class WeaponStateList
{
public:
	/**
	 * @brief Add a weapon of a given type to the list.
	 */
	void AddWeapon(String strType);

	/**
	 * @brief Get a const reference to a weapon state at a given index.
	 */
	const WeaponState& GetWeaponState(size_t iNo) const;

	/**
	 * @brief Get a reference to a weapon state at a given index.
	 */
	WeaponState& GetWeaponState(size_t iNo);

	/**
	 * @brief Update the provided group name and weapon ID values to the next/previous group config.
	 *
	 * @param bNext	 true if next, false if previous
	 * @param rGroup	reference to currently-selected group name (is altered)
	 * @param rIDs	  reference to the list of active weapon IDs (is altered)
	 */
	void GetNextGroup(bool bNext, String& rGroup, std::vector<unsigned>& rIDs) const;

	/**
	 * @brief Set the provided list of weapon IDs to those of the given weapon group.
	 *
	 * @param rGroup	the group name (i.e. the weapon type)
	 * @param rIDs	  the ID list to alter
	 */
	void SetWeaponIDs(const String& rGroup, std::vector<unsigned>& rIDs) const;

	/**
	 * @brief Get the number of items in the list.
	 */
	unsigned GetSize() const { return m_vList.size(); }

	/**
	 * @brief The control update for a weapon state list just updates all the weapon states.
	 */
	void Update(unsigned long iTime);

private:
	std::vector<WeaponState> m_vList;

};


/**
 * @brief The state of a player's control of its ship.
 *
 * This class contains all of the flags and values for the state of a ship's controls.
 * For example whether and in what direction the ship is turning. This is essentially the
 * "user interface" for a faction controller (AI/player) to control their ships.
 *
 * @author Joe Forster
 */
class ControlState
{
public:
	bool	m_bIncThrust; // Increase engine thrust (by increasing target speed)
	bool	m_bRedThrust; // Reduce engine thrust (by decreasing target speed)
	bool	m_bZeroThrust; // Zero thrust
	// Strafing thrusters (simple: on (accel) or off (decel) )
	bool	m_bStrafeL;
	bool	m_bStrafeR;
	bool	m_bStrafeU;
	bool	m_bStrafeD;
	bool	m_bBrake; // Zero thrust and reduce velocity
	//bool m_bWarp; // Warp to another sector once other conditions have been met.
	bool	m_bFiringGroup;
	bool	m_bFiringAll;
	bool	m_bSelfDestruct; // Destroy the ship if this flag is set
	std::vector<Vector3> m_vAutoAim; // The direction to face our weapons, if other than the facing direction

	/**
	 * @brief Create a ControlState for a ship of a given type
	 */
	ControlState(String strShipType);

	/**
	 * @brief Copy constructor
	 */
	ControlState(const ControlState& rToCopy);

	~ControlState() { delete m_pTargetSector; }

	/**
	 * @brief Get the ShipType object for this controller.
	 */
	const ShipType* GetShipType() const;

	int GetTargetID() const { return m_iTargetID; }

	void SetTargetID(int iID) { m_iTargetID = iID; }

	/**
	 * @brief Get whether this ship has an active weapon group selected.
	 */
	bool HasActiveWeapon() const;

	/**
	 * @brief Get a const reference to the state of the weapon with the given ID.
	 */
	const WeaponState& GetWeaponState(unsigned iID) const;

	/**
	 * @brief Get a reference to the state of the weapon with the given ID.
	 */
	WeaponState& GetWeaponState(unsigned iID);

	/**
	 * @brief Get the number of weapons for iteration (e.g. if we're firing all weapons)
	 */
	unsigned GetNoWeapons() const { return m_oWeapons.GetSize(); }

	/**
	 * @brief Get the ID numbers of the active weapons (which is all of them if m_bFiringAll is true)
	 */
	const std::vector<unsigned>& GetActiveWeapons() const;

	/**
	 * @brief Get the ID numbers of all weapons
	 */
	const std::vector<unsigned>& GetAllWeapons() const { return m_vAllWeaps; }

	/**
	 * @brief Toggle the active weapon group.
	 *
	 * @param bNext true if next, false if previous
	 */
	void ToggleWeapons(bool bNext);

	const Real& GetYaw() const { return m_fYaw; }
	const Real& GetPitch() const { return m_fPitch; }
	const Real& GetRoll() const { return m_fRoll; }

	void SetYaw(const Real& rVal) { m_fYaw = rVal; }
	void SetPitch(const Real& rVal) { m_fPitch = rVal; }
	void SetRoll(const Real& rVal) { m_fRoll = rVal; }

// NOTE: Old code for setting target speed (if we decide this is needed, it's probably best put in ShipController)
//	/**
//	 * @brief Set the target speed, which the controller will reach using thrust.
//	 *
//	 * The speed here uses the same units as in the config (units per millisecond)
//	 * We throw an exception if rVal is < 0 or > 1.
//	 */
//	void SetTargetSpeed(const Real& rVal);
//
//	/**
//	 * @brief Set the target speed as a proportion of the maximum in our configuration.
//	 *
//	 * @param rProportion   Value between 0 and 1
//	 *
//	 * NOTE: If we decide to add reversing to ships, we'll need to change this function.
//	 */
//	void SetTargetSpeedOfMax(const Real& rProportion);
//
//	/**
//	 * @brief Get the currently-set target speed.
//	 */
//	const Real& GetTargetSpeed() const { return m_fTargetSpeed; }
//
//	/**
//	 * @brief Get the currently-set target speed as a proportion of the maximum.
//	 */
//	Real GetTargetSpeedOfMax();


	const String* GetTargetSector() { return m_pTargetSector; }

	void UnsetTargetSector();

	void SetTargetSector(const String& rTarget);

	/**
	 * @brief Update function for the control state only needs to call update on our weapons.
	 */
	void Update(unsigned long iTime);

	/**
	 * @brief Reset all the fields in this control state to their initial values.
	 */
	void Reset();

	/**
	 * @brief Reset just the movement-related controls
	 */
	void ResetMovement();

private:
	// The type of ship this controller is linked to.
	String				  m_strShipType;

	// Active weapon by group name (could be empty if none)
	String				  m_strActiveWeapGroup; // Name of the group (currently just the name of the weapon type)
	std::vector<unsigned>	m_vActiveWeaps; // IDs of each active weapon in that group
	std::vector<unsigned>	m_vAllWeaps; // We keep a vector of all weapons to return in place of the above and m_bFiringAll is true
	WeaponStateList		 m_oWeapons;

	int					 m_iTargetID; // Selected target for this ship
	// TODO: Using pointers for unsettable things like these was a dumb idea if we want to actually copy stuff; fix this by using bools instead
	String*				 m_pTargetSector; // Target sector for warp, if any.

	Real					m_fYaw;
	Real					m_fPitch;
	Real					m_fRoll;

};

#endif // BFPSTATESHIPCONTROL_H_INCLUDED
