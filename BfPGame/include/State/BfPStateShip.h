#ifndef BFPSTATESHIP_H_INCLUDED
#define BFPSTATESHIP_H_INCLUDED

#include <State/BfPStateEntity.h>
#include <State/BfPStateShipControl.h>

/**
 * @brief A data structure for holding all the information on the state of a Ship entity.
 *
 * @author Joe Forster
 */
class ShipState : public BfPEntityState
{
public:
	/**
	 * @brief Construct a ship state from the pertinent data.
	 *
	 * @param iGroupID	  The ship group ID.
	 * @param bFlagGroup	Whether this ship's group is the flag group.
	 * @param bGroupLeader  Whether this is the group leader.
	 * @param strType	   The entity sub-type (i.e. ship type)
	 * @param strFaction	The faction ID of this ship.
	 * @param strSectorID   The sector in which this ship is located.
	 * @param vecPos		The position of this ship in the sector.
	 * @param qOrientation  The initial orientation of the ship.
	 */
	ShipState(unsigned iGroupID, bool bFlagGroup, bool bGroupLeader,
		String strType, String strFaction, Vector3 vecPos, Quaternion qOrientation, String strSectorID);

	// Implementations of our removal management stuff.
	bool GetRemoved() const;
	void SetRemoved(bool bRemoved);

	/**
	 * @brief See if this ship is in the flag group.
	 */
	bool IsInFlagGroup() const { return m_bFlagGroup; }

	/**
	 * @brief Set this ship's flag group status.
	 */
	void SetInFlagGroup(bool bVal) { m_bFlagGroup = bVal; }

	/**
	 * @brief See if this ship is the group leader.
	 */
	bool IsGroupLeader() const { return m_bGroupLeader; }

	/**
	 * @brief Set this ship's group leader status.
	 */
	void SetGroupLeader(bool bVal) { m_bGroupLeader = bVal; }

	/**
	 * @brief Our implementation of EntityState's GetEntityType gets it from the appropriate config
	 */
	virtual const EntityType* GetEntityType() const;

	/**
	 * @brief Get the EntityType cast as a ShipType for convenience.
	 */
	const ShipType* GetShipType() const;

	/**
	 * @brief Apply damage to this ship - first to the shields, then to the hit points.
	 */
	virtual void ApplyDamage(int iAmount);

	/**
	 * @brief Set the faction ID of who destroyed this ship.
	 */
	void SetKillerFaction(const String& rFaction);

	/**
	 * @brief Get the faction ID of this ship's destroyer, or just its own faction if not set.
	 */
	const String& GetKillerFaction() const;

	unsigned GetGroupID() const { assert(m_iGroupID >= 0); return m_iGroupID; }

	int GetSP() const { return m_iSP; }
	int GetEP() const { return m_iEP; }
	bool FullEP() const;
	void ZeroEP() { m_iEP = 0; }

	Real GetOverallCondition(bool bInclEP) const;

	/**
	 * @brief Get an EPs value for this ship as a proportion of the maximum.
	 */
	Real GetEPOfMax() const;

	// Cumulated amounts to regenerate EPs/SPs between updates
	Real GetSPRegen() const { return m_fSPRegen; }
	Real GetEPRegen() const { return m_fEPRegen; }

	// Alter the above regen values
	void IncSPRegen(Real fAmount) { m_fSPRegen += fAmount; }
	void DecSPRegen(Real fAmount) { m_fSPRegen -= fAmount; }
	void IncEPRegen(Real fAmount) { m_fEPRegen += fAmount; }
	void DecEPRegen(Real fAmount) { m_fEPRegen -= fAmount; }

	// Thrust (from the main thrusters) as a force
	Real GetThrust() const { return m_fThrust; }
	void ZeroThrust() { m_fThrust = 0; }

	/**
	 * @brief Get a reference to our ControlState for this ship.
	 */
	ControlState& GetControls() { return m_oControls; }

	/**
	 * @brief Get a const reference to our ControlState for this ship.
	 */
	const ControlState& GetControls() const { return m_oControls; }

	/**
	 * @brief Whether the ship has a currently-active (selected) weapon.
	 */
	bool HasActiveWeapon() const { return m_oControls.HasActiveWeapon(); }

	/**
	 * @brief Get a reference to the WeaponState with the given ID.
	 */
	const WeaponState& GetWeaponState(unsigned iID) const { return m_oControls.GetWeaponState(iID); }

	/**
	 * @brief Get a reference to a vector of all currently-active weapon state IDs.
	 */
	const std::vector<unsigned>& GetActiveWeapons() const { return m_oControls.GetActiveWeapons(); }

	/**
	 * @brief Get a reference to a vector of all weapon state IDs this ship has.
	 */
	const std::vector<unsigned>& GetAllWeapons() const { return m_oControls.GetAllWeapons(); }


	/**
	 * @brief Get the direction a weapon goes when we fire the weapon with the given ID.
	 *
	 * This value only differs from the facing direction if our weapon has the auto-tracking feature.
	 */
	Vector3 GetFiringDir(unsigned iWeapID) const;

	/**
	 * @brief Get whether the ship is ready to fire a projectile.
	 */
	bool ReadyToFire() const;

	/**
	 * @brief Attempt to fire a projectile, returning true if one was fired and needs to be created.
	 */
	bool Fire();

	bool GetWarpTrigger() const { return m_bTriggerWarp; }

	bool ReadyToWarp() const { return m_bWarping && m_iWarpTimer <= 0; }

	bool GetWarping() const { return m_bWarping; }

	/**
	 * @brief Begin the warping sequence
	 *
	 * This is done by the controller, which should check that the conditions are correct.
	 */
	void BeginWarping();

	/**
	 * @brief Reset the warping sequence once it is complete (success or failure).
	 */
	void ResetWarping();

	/**
	 * @brief Main control update function for a ShipState.
	 *
	 * This function manages things like triggers and regeneration of shields and energy.
	 */
	void Update(unsigned long iTime);

private:
	int			 m_iGroupID;
	bool			m_bFlagGroup;
	bool			m_bGroupLeader;
	// Which faction killed this ship if it's dead (set to my faction by default e.g. collision death)
	String		  m_strKillerFaction;

	int			 m_iSP;
	int			 m_iEP;
	Real			m_fSPRegen;
	Real			m_fEPRegen;
	Real			m_fThrust;

	// Timers for visual effects (count down to finish)
	long			m_iWarpTimer;
	bool			m_bTriggerWarp;
	bool			m_bTriggeredWarp;

	bool			m_bWarping; // In process of warping
	bool			m_bWarped; // Warped, so needs to be removed from the sector

	ControlState	m_oControls;

	Vector3		 m_vecFiringDir;

	/**
	 * @brief Return the EP cost of firing if we are ready to fire; otherwise return -1.
	 */
	int GetCostToFire() const;

};

#endif // BFPSTATESHIP_H_INCLUDED
