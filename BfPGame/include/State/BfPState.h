#ifndef BFPSTATE_H_INCLUDED
#define BFPSTATE_H_INCLUDED

#include <State/GameState.h>

#include <State/BfPStateSector.h>
#include <State/BfPStateFaction.h>

// Various typedefs used by the state
typedef std::map<String, BfPSectorState*>	SectorList;
typedef std::map<String, FactionState*>	  FactionList;

// The states of all triggers are considred part of the game state.
typedef struct {
	unsigned long m_iTimer;
	bool m_bDone; }						 TriggerState;
typedef std::map<String,TriggerState>	   TriggerList;
typedef std::map<String,TriggerList>		MissionList;

// For now, InputState just contains a set of flags.
enum CONTROLS_MODE {
	NONE=0, TURNING, MOVEMENT, BRAKE, SHIFT, TARGETING, WEAPONS, SWITCHMODE,
	SELECTBODY, SELECTGROUP, MOVEGROUP, SELECTSECTOR, PURCHASE, DEPLOY,
	ALL, CMODE_LAST };
typedef std::set<CONTROLS_MODE>			  InputState;

/**
 * @brief The full state of the game world overall.
 *
 * This class represents the state of the entire game, as lists of sub state objects.
 *
 * @author Joe Forster
 */
class BfPState
{
public:
	/**
	 * @brief This constructs an initial state from the configuration.
	 */
	BfPState();

	/**
	 * @brief Copy constructor for the full game state
	 */
	BfPState(const BfPState& rState);

	~BfPState();

	SectorList& GetSectorList() { return m_mSectors; }
	const SectorList& GetSectorList() const { return m_mSectors; }

	FactionList& GetFactionList() { return m_mFactions; }
	const FactionList& GetFactionList() const { return m_mFactions; }

	std::vector<const ShipState*> GetFactionShips(String strFaction) const;
	std::vector<ShipState*> GetFactionShips(String strFaction);

	BfPSectorState* GetSectorState(String strName);

	FactionState* GetFactionState(String strName);

	FactionState* GetPlayerFaction();
	ShipState* GetPlayerShip();

	unsigned long GetAge() const { return m_iAge; }

	/**
	 * Determine if a given ID number is that of an existing object.
	 */
	bool ObjectExists(String strType, unsigned iID) const;

	/**
	 * @brief Get the next available ID number for an object of a given type..
	 */
	unsigned GetNextObjectID(String strType) const;

	/**
	 * @brief Transfer a ship from its sector to a destination, assuming it's finished warping.
	 */
	const ShipState* WarpShip(unsigned iID, const String& rDestSector);

	/**
	 * @brief Control update function for the game state.
	 */
	void Update(unsigned long iTime);



//	const EventHandler& GetAIEvents() const { return m_oAIEvents; }
//	EventHandler& GetAIEvents() { return m_oAIEvents; }

	/**
	 * @brief Find a ship state with the given ID number.
	 *
	 * @param iID	   The ID of the ship
	 * @param pSector   A pointer to a pointer that'll be set to the ship's sector if we find it.
	 *
	 * @return A pointer to a ship state if found, of NULL if not.
	 */
	ShipState* FindShipWithID(unsigned iID, BfPSectorState** pSector);

	/**
	 * @brief Get the stored states of our triggers, managed by the event control system.
	 */
	MissionList& GetMissions() { return m_mMissions; }

	/**
	 * @brief Add an enabling/disabling flag to the input state, generally from a trigger event.
	 */
	void AddPlayerInputFlag(CONTROLS_MODE iFlag);

	/**
	 * @brief Remove a flag to the input state (anything but ALL/NONE), generally from a trigger event.
	 */
	void RemovePlayerInputFlag(CONTROLS_MODE iFlag);

	/**
	 * @brief Check to see if a certain type of control is enabled.
	 */
	bool CheckPlayerInputFlag(CONTROLS_MODE iFlag) const;

private:
	// How long this game has been going (assuming no game will last longer than ~1200 hours...
	unsigned long   m_iAge;
	long			m_iTimeSincePrevRPIncome;

	SectorList	  m_mSectors;
	FactionList	 m_mFactions;

	// A handler for AI events.
//	EventHandler m_oAIEvents;

	// We keep track of what triggers (from missions/tutorials) have been activated thus far.
	// The integer value here is used for timed triggers - the trigger is only considered passed
	// if the time has passed. Otherwise, merely being in m_mTriggers means a trigger has passed.
	MissionList	 m_mMissions;

	// The player's control enabled state needs to be remembered here too.
	InputState	  m_oInputState;

	/**
	 * @brief Initialise the values in this state from the currently-set scenario configuration.
	 */
	void InitFromConfig();

	/**
	 * @brief Update sub-function for resource income.
	 */
	void UpdateResourceIncome(unsigned long iTime);

};

#endif // BFPSTATE_H_INCLUDED
