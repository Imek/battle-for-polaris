#ifndef BFPSTATEENTITY_H_INCLUDED
#define BFPSTATEENTITY_H_INCLUDED

#include <State/GameState.h>

/**
 * @brief Base state for an entity in the BfP world, contained within a sector.
 *
 * @author Joe Forster
 */
class BfPEntityState : public EntityState
{
public:
	/**
	 * @brief Constructor for a BfP entity state
	 *
	 * @param strType	   The type of entity (e.g. ship or projectile)
	 * @param iID		   An ID number
	 * @param strSubType	The sub-type of entity (e.g. ship type or weapon type)
	 * @param strFactionID  The ID of the faction that owns this entity
	 * @param vecPos		The starting position of the entity.
	 * @param qOrientation  The initial orientation of the entity.
	 * @param strSectorID   The sector (world) ID the entity is located in
	 */
	BfPEntityState(
		String strType, string strSubType, String strFactionID,
		Vector3 vecPos, Quaternion qOrientation, String strSectorID);

	/**
	 * @brief Copy constructor.
	 */
	BfPEntityState(const BfPEntityState& rState);

	/**
	 * @brief Get the ID of the sector/world in which this entity is located.
	 */
	const String& GetSectorID() const { return m_strSectorID; }

	/**
	 * @brief Change the sector stored in m_strSectorID.
	 */
	void SetSectorID(const String& rNewSector);

private:
	// ID of the sector this entity is located within
	String				  m_strSectorID;
};

#endif // BFPSTATEENTITY_H_INCLUDED
