#ifndef BFPSTATEFACTION_H_INCLUDED
#define BFPSTATEFACTION_H_INCLUDED

#include <State/GameState.h>

#include <Config/BfPConfigScenario.h>

/**
 * @brief Manages and stores the current state of a faction in the game, excluding its ships.
 *
 * (All ship states and ownership are managed in sector states)
 *
 * @author Joe Forster
 */
class FactionState
{
public:
	/**
	 * @brief Main constructor creates a blank faction state.
	 */
	FactionState(const FactionType* pConf);

	/**
	 * @brief Destructor for FactionState.
	 */
	~FactionState();

	/**
	 * @brief Get the name (ID from config) of this faction.
	 */
	const String& GetName() const { return m_strName; }

	/**
	 * @brief Get the faction type configuration for this faction.
	 */
	const FactionType* GetFactionType() const;

	/**
	 * @brief Get how many resource points this faction has.
	 */
	long GetRPs() const { return m_iResourcePoints; }

	/**
	 * @brief Alter this faction's resource points.
	 */
	void AlterRPs(long iAmount);

	/**
	 * @brief Get this faction's score (points).
	 */
	const long& GetScore() const { return m_iScore; }

	/**
	 * @brief Alter this faction's score (points).
	 */
	void AlterScore(int iChange) { m_iScore += iChange; }

	/**
	 * @brief Get the list of ship types we have to deploy.
	 */
	const StringVector& GetShipsToDeploy() const { return m_vShipsToDeploy; }

	/**
	 * @brief See if we have a given ship type available for deployment.
	 */
	bool HasShipToDeploy(const String& rType) const;

	/**
	 * @brief See if we have a given group available for deployment.
	 */
	bool HasGroupToDeploy(const StringVector& rTypes) const;

	/**
	 * @brief Assuming we have it, remove a ship type from our deployment list.
	 */
	bool RemoveDeployingShip(const String& rType);

	/**
	 * @brief Assuming we have them, remove a list of ship types from our deployment list.
	 */
	bool RemoveDeployingGroup(const StringVector& rTypes);


	/**
	 * @brief Set the ID of our flag ship
	 */
	void SetFlagShipID(unsigned iID) { m_iFlagShipID = iID; }

	/**
	 * @brief Unset the ID of our flag ship
	 */
	void UnsetFlagShipID() { m_iFlagShipID = -1; }

	/**
	 * @brief See if our flag ship ID is set.
	 */
	bool IsFlagShipIDSet() const { return (m_iFlagShipID >= 0); }

	/**
	 * @brief Get the ID of our flag ship.
	 */
	unsigned GetFlagShipID() const;

	/**
	 * @brief Give this faction a ship to deploy.
	 */
	void GiveShip(String strType);

	/**
	 * @brief See if we can afford to buy the given ship group.
	 */
	bool CanPurchaseGroup(const StringVector& rTypes) const;

	/**
	 * @brief Purchase a ship group.
	 */
	bool PurchaseGroup(const StringVector& rTypes);


private:
	String		  m_strName;

	long			m_iResourcePoints;
	long			m_iScore;
	StringVector	m_vShipsToDeploy;
	int			 m_iFlagShipID;

};

#endif // BFPSTATEFACTION_H_INCLUDED
