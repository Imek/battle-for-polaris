#ifndef BFPCONFIGMISSIONS_H_INCLUDED
#define BFPCONFIGMISSIONS_H_INCLUDED

#include <Game/GameConfig.h>

/**
 * @brief A struct to contain the needed information to look up an event's parameters in the config.
 *
 * @author Joe Forster
 */
struct EventConfig
{
	// The prefix in the config for looking up each parameter:
	// e.g. to find the Type look at <Prefix>Type in m_pParams
	// And to find a parameter do the same with the name of that parameter
	String			  m_strPrefix;
	const BaseConfig*   m_pConfig;

};

/**
 * @brief This struct represents the configuration of a trigger that is part of a mission.
 *
 * A trigger can be activated by a hard-coded part of the gameplay code or a custom trigger
 * with appropriate parameters. A trigger may also have prerequisites in the form
 * of other triggers.
 *
 * @author Joe Forster
 */
struct TriggerConfig
{
	// The name and type of trigger
	String				  m_strName;
	String				  m_strMission;
	String				  m_strTriggerType;
	// Prerequisite triggers
	StringVector			m_vPrereqs;
	// Amount of time (ms) between activation and completion
	unsigned long		   m_iTime;
	// Number of times to repeat, or -1 if indefinitely
	//long					m_iRepeat;
	// Events to occur when this is triggered.
	std::vector<EventConfig>	 m_vEvents;

};


/**
 * @brief This class represents a Mission - a set of triggers with event responses.
 *
 * @author Joe Forster
 */
class MissionConfig : public ObjectType
{
public:
	MissionConfig(String strName, const BaseConfig* pConfig);

	/**
	 * @brief Add triggers with type rType to the list rList.
	 */
	void GetTriggersWithType(const String& rType, std::vector<const TriggerConfig*>& rList) const;

	/**
	 * @brief Get the trigger config with the given name.
	 */
	const TriggerConfig& GetTrigger(const String& rName) const;

	virtual ~MissionConfig();

private:

	StringVector				m_vTriggers;
	std::map<String,TriggerConfig*>  m_mTriggers;
};


/**
 * @brief Configuration class for a set of missions in a scenario.
 *
 * A ScenarioConfig can (but doesn't need to) point to a MissionsConfig object constructed
 * from a missions configuration file.
 *
 * See the missions_tutorial.cfg file for examples and documentation on how missions are made.
 *
 * @author Joe Forster
 */
class MissionsConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file and sets up the values.
	 */
	MissionsConfig(const String& rPath);

	virtual ~MissionsConfig();

	/**
	 * @brief See whether a particular category of hard-coded event should be enabled.
	 */
	bool GetEventsEnabled(const String& rCategory) const;


	const StringVector& GetMissionNames() const { return m_vMissions; }
	const MissionConfig& GetMission(const String& rName) const;

	/**
	 * @brief Get a list of TriggerConfigs using the given trigger type.
	 */
	std::vector<const TriggerConfig*> GetTriggersWithType(const String& rType) const;

private:
	StringVector				m_vEventsEnabled;
	StringVector				m_vMissions;
	std::map<String,MissionConfig*>  m_mMissions;
};

#endif // BFPCONFIGMISSIONS_H_INCLUDED
