#ifndef BFPCONFIGAI_H_INCLUDED
#define BFPCONFIGAI_H_INCLUDED

#include <BfPGameDefs.h>

#include <Game/GameConfig.h>

/**
 * @brief A class representing a type of stance for ship group control used by the low-level AI.
 *
 * @author Joe Forster
 */
class ShipGroupStance : public ObjectType
{
public:
	ShipGroupStance(String strName, const BaseConfig* pConfig);

	const Real& GetAggroRange() const { return m_fAggroRange; }
	const Real& GetAvoidThreshold() const { return m_fAvoidThreshold; }
	const Real& GetEvadeThreshold() const { return m_fEvadeThreshold; }
	bool GetWarpPastEnemies() const { return m_bWarpPastEnemies; }

private:
	Real	m_fAggroRange;
	Real	m_fAvoidThreshold;
	Real	m_fEvadeThreshold;
	bool	m_bWarpPastEnemies;
};

/**
 * @brief A class representing a specific configuration for the AI player.
 *
 * @author Joe Forster
 */
class AIPlayerType : public ObjectType
{
public:
	AIPlayerType(String strName, const BaseConfig* pConfig);

	const String& GetClass() const { return m_strClass; }
	const String& GetLogicEngineType() const { return m_strLogicEngine; }
	const String& GetInitialModel() const { return m_strInitialModel; }
	const String& GetEvaluator() const { return m_strEvaluator; }
	const String& GetInitialValues() const { return m_strInitialValues; }

	const StringVector& GetManagers() const { return m_vManagers; }

	const StringVector& GetObjectiveTypes() const { return m_vObjectives; }
	const Real& GetObjectiveProp(const String& rObj) const;

private:
	String						  m_strClass;
	String						  m_strLogicEngine;
	String						  m_strInitialModel;
	String						  m_strEvaluator;
	String						  m_strInitialValues;

	StringVector					m_vManagers;

	StringVector					m_vObjectives;
	StringRealMap				   m_mInitObjs;

};


/**
 * @brief The configuration file for settings pertaining to the AI system (both high and low level).
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class AIConfig : public BaseConfig
{
public:
	/**
	* @brief Constructor looks in the given path for a config file, then reads in the AI config values.
	*/
	AIConfig(String strPath);

	~AIConfig();

	const String& GetTestLogicEngine() const { return m_strTestLogicEngine; }
	const String& GetTestInitialModel() const { return m_strTestInitialModel; }
	const String& GetTestEvaluator() const { return m_strTestEvaluator; }
	const String& GetTestInitialValues() const { return m_strTestInitialValues; }

	unsigned long GetTickLength() const { return m_iTickLength; }
	long GetAbstractRPUnit() const { return m_iAbstractRPUnit; }
	int GetAbstractPercentUnit() const { return m_iAbstractPercentUnit; }
	const Real& GetMinRPsConfidence() const { return m_fMinRPsConfidence; }
	const Real& GetMaxRPsConfidence() const { return m_fMaxRPsConfidence; }
	const Real& GetEstRPsConfidence() const { return m_fEstRPsConfidence; }
	const Real& GetSaveRPsUpThreshold() const { return m_fSaveRPsUpThreshold; }
	const Real& GetGroupSizeMinProbability() const { return m_fGroupSizeMinProbability; }
	const Real& GetGroupSizeMaxProbability() const { return m_fGroupSizeMaxProbability; }

	const Real& GetFormationSeparationMult() const { return m_fFormationSeparationMult; }
	const Real& GetOrbitSeparationMult() const { return m_fOrbitSeparationMult; }
	unsigned GetOrbitWaypointCount() const { return m_iOrbitWaypointCount; }
	const Real& GetWaypointArrivedRadius() const { return m_fWaypointArrivedRadius; }
	unsigned GetPredictionTimeStep() const { return m_iPredictionTimeStep; }

	const Real& GetMinYawStep() const { return m_fMinYawStep; }
	const Real& GetMaxSlowYawStep() const { return m_fMaxSlowYawStep; }
	const Real& GetMinPitchStep() const { return m_fMinPitchStep; }
	const Real& GetMaxSlowPitchStep() const { return m_fMaxSlowPitchStep; }
	const Real& GetMinRollStep() const { return m_fMinRollStep; }
	const Real& GetMaxSlowRollStep() const { return m_fMaxSlowRollStep; }

	const Real& GetMaxFiringAngle() const { return m_fMaxFiringAngle; }
	const Real& GetMinEvasionAngle() const { return m_fMinEvasionAngle; }
	const Real& GetMaxEvasionAngle() const { return m_fMaxEvasionAngle; }
	const Real& GetDogfightStrafeRunMult() const { return m_fDogfightStrafeRunMult; }

	const Real& GetCollisionAvoidAngleStep() const { return m_fCollisionAvoidAngleStep; }
	unsigned GetCollisionAvoidNumIterations() const { return m_iCollisionAvoidNumIterations; }
	const Real& GetMaxCollisionAvoidDist() const { return m_fMaxCollisionAvoidDist; }
	unsigned GetMaxCollisionAvoidTime() const { return m_iMaxCollisionAvoidTime; }

	const String& GetDefaultGroupStance() const { return m_strDefaultGroupStance; }

	unsigned GetThreatRecalcPeriod() const { return m_iThreatRecalcPeriod; }

	const AIPlayerType* GetAIPlayerType(const String& rName) const;

	const StringVector& GetShipGroupStances() const { return m_vStanceNames; }
	const ShipGroupStance* GetShipGroupStance(const String& rName) const;

private:
	// What type of logic engine to use as the player's test engine.
	String		  m_strTestLogicEngine;
	String		  m_strTestInitialModel;
	String		  m_strTestEvaluator;
	String		  m_strTestInitialValues;

	// Accuracy settings for the abstract state.
	unsigned long   m_iTickLength; // Number of milliseconds in an abstract game "tick" (turn)
	long			m_iAbstractRPUnit; // The closest multiple to which to round RP values.
	int			 m_iAbstractPercentUnit; // The closest multiple to which to round percentages.
	// Set probabilities used in the logic engine
	Real			m_fMinRPsConfidence;
	Real			m_fMaxRPsConfidence;
	Real			m_fEstRPsConfidence;
	Real			m_fSaveRPsUpThreshold;
	Real			m_fGroupSizeMinProbability;
	Real			m_fGroupSizeMaxProbability;

	Real			m_fFormationSeparationMult; // Distance between ships (multiplied by bounding diameter)
	Real			m_fOrbitSeparationMult;
	unsigned		m_iOrbitWaypointCount;
	Real			m_fWaypointArrivedRadius;
	unsigned		m_iPredictionTimeStep;
	Real			m_fMinYawStep;
	Real			m_fMaxSlowYawStep;
	Real			m_fMinPitchStep;
	Real			m_fMaxSlowPitchStep;
	Real			m_fMinRollStep;
	Real			m_fMaxSlowRollStep;
	Real			m_fMaxFiringAngle;
	Real			m_fMinEvasionAngle;
	Real			m_fMaxEvasionAngle;
	Real			m_fDogfightStrafeRunMult;

	Real			m_fCollisionAvoidAngleStep;
	unsigned		m_iCollisionAvoidNumIterations;
	Real			m_fMaxCollisionAvoidDist;
	unsigned		m_iMaxCollisionAvoidTime;

	String		  m_strDefaultGroupStance;

	unsigned		m_iThreatRecalcPeriod;

	// Every possible AI player configuration.
	std::map<String, AIPlayerType*>	  m_mPlayers;
	// Ship group stance types
	StringVector					m_vStanceNames;
	std::map<String, ShipGroupStance*>   m_mStances;

};

#endif // BFPCONFIGAI_H_INCLUDED
