#ifndef BFPCONFIGMAP_H_INCLUDED
#define BFPCONFIGMAP_H_INCLUDED

#include <Config/BfPConfigBodies.h>


/**
 * @brief Represents a physical instance of a BodyType located in a SectorType.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class BodyInstance
{
public:
	BodyInstance(String strID, String strType, String strLoc);

	const String& GetID() const { return m_strID; }
	const String& GetType() const { return m_strType; }
	const Vector3& GetLocation() const { return m_vecLoc; }

private:
	String			  m_strID;
	String			  m_strType;
	Vector3			 m_vecLoc;
};

typedef std::vector<BodyInstance*> BodyVector;

/**
 * @brief Represents a "sector type", generally the config for a particular sector in the game world.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class SectorType : public ObjectType
{
public:
	SectorType(String strName, const BaseConfig* pConfig);

	virtual ~SectorType();

	const Vector3& GetAmbientColour() const { return m_vecAmbientColour; }
	String GetStarTexture() const { return m_strStarTexture; }
	String GetDustParticlesConf() const { return m_strDustParticlesConf; }
	String GetDustParticlesMat() const { return m_strDustParticlesMat; }

	// Settings for the world map.
	const StringVector& GetLinkedSectors() const { return m_vLinkedSectors; }
	const std::vector<Vector3>& GetWarpPoints() const { return m_vWarpPoints; }
	const Vector3& GetWarpPointFromSector(const String& rSector) const;
	const std::vector<Vector3>& GetSpawnPoints() const { return m_vSpawnPoints; }
	bool IsLinkedTo(const String& rSector) const;
	// Position of this sector on the map overview (relative to centre)
	const Vector3& GetMapRelPos() const { return m_vecMapRelPos; }
	const Vector3& GetMapTextPos() const { return m_vecMapTextPos; }

	// Object types in the sector.
	const StringVector& GetObjTypes() const { return m_vecObjTypes; }

	const BodyVector& GetBodies() const { return m_vBodies; }
	const BodyInstance* GetBodyWithID(String strID) const;

	// RP income for this sector.
	unsigned GetRPIncome() const { return m_iRPIncome; }

private:
	Vector3				 m_vecAmbientColour;
	String				  m_strStarTexture;
	String				  m_strDustParticlesConf;
	String				  m_strDustParticlesMat;
	StringVector			m_vLinkedSectors;

	std::vector<Vector3>	m_vWarpPoints;
	std::vector<Vector3>	m_vSpawnPoints;

	Vector3				 m_vecMapRelPos;
	Vector3				 m_vecMapTextPos;

	StringVector			m_vecObjTypes;

	BodyVector			  m_vBodies;

	unsigned				m_iRPIncome;

};

/**
 * @brief Configurations for a set of sectors that make up the game world.
 *
 * A map is a list of SectorTypes; each SectorType determines its contents,
 * characteristics, and links. It also contains some configuration settings.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 */
class MapConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads each sector config.
	 */
	MapConfig(const String& rPath);

	~MapConfig();

	Real GetInitialMapScale() const { return m_fInitialMapScale; }
	unsigned long GetRPIncomeDelay() const { return m_iRPIncomeDelay; }
	unsigned long GetRPMaxAmount() const { return m_iRPMaxAmount; }
	unsigned long GetSectorCaptureDelay() const { return m_iSectorCaptureDelay; }
	const String& GetMapStarTexture() const { return m_strMapStarTexture; }
	Real GetMapTextSize() const { return m_fMapTextSize; }
	Real GetMapNodeSize() const { return m_fMapNodeSize; }
	bool GetDrawUnselectedBodyMarkers() const { return m_bDrawUnselectedBodyMarkers; }
	Real GetSpawnDeviationStep() const { return m_fSpawnDeviationStep; }
	Real GetMaxSpawnDeviation() const { return m_fMaxSpawnDeviation; }
	Real GetMaxWarpDist() const { return m_fMaxWarpDist; }

	const StringVector& GetSectorTypes() const { return m_vSectors; }
	const SectorType* GetSectorType(const String& rName) const;

	const StringVector& GetBodyTypes() const { return m_pBodiesConf->GetBodyTypes(); }
	const BodyType* GetBodyType(const String& rName) const { return m_pBodiesConf->GetBodyType(rName); }

private:
	Real						m_fInitialMapScale;
	unsigned long			   m_iRPIncomeDelay;
	unsigned long			   m_iRPMaxAmount;
	unsigned long			   m_iSectorCaptureDelay;
	String					  m_strMapStarTexture;
	Real						m_fMapTextSize;
	Real						m_fMapNodeSize;
	bool						m_bDrawUnselectedBodyMarkers;
	Real						m_fSpawnDeviationStep;
	Real						m_fMaxSpawnDeviation;
	Real						m_fMaxWarpDist;

	BodiesConfig*			   m_pBodiesConf;

	StringVector				m_vSectors;
	std::map<String, SectorType*>	m_mSectors;


};

#endif // BFPCONFIGMAP_H_INCLUDED
