#ifndef BFPGENERALCONFIG_H_INCLUDED
#define BFPGENERALCONFIG_H_INCLUDED

#include <BfPGameDefs.h>

#include <Game/GameConfig.h>

/**
 * @brief Configuration class for the general game (mostly gameplay, UI and control-related values)
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class GeneralConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file and sets up the values.
	 */
	GeneralConfig(String strPath);

	// Gameplay options
	bool GetFriendlyFireEnabled() const { return m_bFriendlyFire; }
	Real GetCollisionDmgMult() const { return m_fCollisionDmgMult; }
	unsigned GetMaxGroupSize() const { return m_iMaxGroupSize; }
	unsigned GetDeathCamDelay() const { return m_iDeathCamDelay; }

	const String& GetDefaultWorld() const
	{
		// Should've been checked when the config was read.
		assert(!m_vScenarioConfigs.empty());
		return *m_vScenarioConfigs.begin();
	}

	const StringVector& GetScenarioConfigs() const { return m_vScenarioConfigs; }
	const String& GetDefaultScenario() const { return m_strDefaultScenario; }
	const String& GetScenarioConfig(const String& rName) const;
	unsigned GetControlUpdatePeriod() const { return m_iControlUpdatePeriod; }
	bool GetShowAIDebugInterface() const { return m_bShowAIDebugInterface; }

	// GUI
	Real GetMenuPointerSensitivity() const { return m_fMenuPointerSensitivity; }
	Real GetTacticalSelectMinX() const { return m_fTacticalSelectMinX; }
	Real GetTacticalSelectMaxY() const { return m_fTacticalSelectMaxY; }
	Real GetTacticalSelectRadiusMult() const { return m_fTacticalSelectRadiusMult; }
	Real GetEdgePanDistance() const { return m_fEdgePanDistance; }
	unsigned GetMaxMessages() const { return m_iMaxMessages; }

	// Camera
	bool GetEnableFreeCam() const { return m_bEnableFreeCam; }
	Real GetCamSpeed() const { return m_fCamSpeed; }
	Real GetCamRotation() const { return m_fCamRotation; }
	Real GetChaseCamInitialDist() const { return m_fChaseCamInitialDist; }
	Real GetChaseCamMinDist() const { return m_fChaseCamMinDist; }
	Real GetChaseCamMaxDist() const { return m_fChaseCamMaxDist; }
	Real GetChaseCamStiffness() const { return m_fChaseCamStiffness; }
	Real GetChaseCamDamping() const { return m_fChaseCamDamping; }
	Real GetChaseCamZoomStiffness() const { return m_fChaseCamZoomStiffness; }
	Real GetChaseCamZoomSpeed() const { return m_fChaseCamZoomSpeed; }
	Real GetChaseCamSensitivity() const { return m_fChaseCamSensitivity; }
	Real GetChaseCamAccelSensitivity() const { return m_fChaseCamAccelSensitivity; }
	Real GetChaseCamDecelSensitivity() const { return m_fChaseCamDecelSensitivity; }

	Real GetTacCamInitialDist() const { return m_fTacCamInitialDist; }
	Real GetTacCamMinDist() const { return m_fTacCamMinDist; }
	Real GetTacCamMaxDist() const { return m_fTacCamMaxDist; }
	Real GetTacCamZoomStiffness() const { return m_fTacCamZoomStiffness; }
	Real GetTacCamZoomSpeed() const { return m_fTacCamZoomSpeed; }
	Real GetTacCamPanSpeed() const { return m_fTacCamPanSpeed; }
	Real GetTacCamSensitivity() const { return m_fTacCamSensitivity; }

	Real GetMapCamInitialDist() const { return m_fMapCamInitialDist; }
	Real GetMapCamMinDist() const { return m_fMapCamMinDist; }
	Real GetMapCamMaxDist() const { return m_fMapCamMaxDist; }
	Real GetMapCamZoomStiffness() const { return m_fMapCamZoomStiffness; }
	Real GetMapCamZoomSpeed() const { return m_fMapCamZoomSpeed; }
	Real GetMapCamPanSpeed() const { return m_fMapCamPanSpeed; }
	Real GetMapCamSensitivity() const { return m_fMapCamSensitivity; }

	// Key bindings
	String GetZoomInKey() const { return m_strZoomInKey; }
	String GetZoomOutKey() const { return m_strZoomOutKey; }
	String GetAccelKey() const { return m_strAccelKey; }
	String GetDecelKey() const { return m_strDecelKey; }
	String GetStrafeLKey() const { return m_strStrafeLKey; }
	String GetStrafeRKey() const { return m_strStrafeRKey; }
	String GetStrafeUKey() const { return m_strStrafeUKey; }
	String GetStrafeDKey() const { return m_strStrafeDKey; }
	String GetRollLKey() const { return m_strRollLKey; }
	String GetRollRKey() const { return m_strRollRKey; }
	String GetBrakeKey() const { return m_strBrakeKey; }
	String GetWarpKey() const { return m_strWarpKey; }
	String GetCamShiftKey() const { return m_strCamShiftKey; }
	String GetSwitchModeKey() const { return m_strSwitchModeKey; }

	String GetToggleTargetModeKey() const { return m_strToggleTargetModeKey; }
	String GetNextTargetKey() const { return m_strNextTargetKey; }
	String GetUnselectTargetKey() const { return m_strUnselectTargetKey; }
	String GetClosestEnemyKey() const { return m_strClosestEnemyKey; }
	String GetNextEnemyKey() const { return m_strNextEnemyKey; }
	String GetNextWeapGroupKey() const { return m_strNextWeapGroupKey; }

	String GetToggleGroupStanceKey() const { return m_strToggleGroupStanceKey; }
	String GetPauseKey() const { return m_strPauseKey; }

	// Mouse button bindings
	OIS::MouseButtonID GetFireGroupMButton() const { return m_iFireGroupMButton; }
	OIS::MouseButtonID GetFireAllMButton() const { return m_iFireAllMButton; }
	OIS::MouseButtonID GetCamShiftMButton() const { return m_iCamShiftMButton; }
	OIS::MouseButtonID GetSwitchModeMButton() const { return m_iSwitchModeMButton; }

private:

	// Game options
	bool				m_bFriendlyFire;
	Real				m_fCollisionDmgMult;
	unsigned			m_iMaxGroupSize;
	unsigned			m_iDeathCamDelay;

	StringVector		m_vScenarioConfigs;
	StringStringMap	 m_mScenarioConfigs;
	String			  m_strDefaultScenario;
	unsigned			m_iControlUpdatePeriod;
	bool				m_bShowAIDebugInterface;

	// GUI settings
	Real				m_fMenuPointerSensitivity;
	Real				m_fTacticalSelectMinX;
	Real				m_fTacticalSelectMaxY;
	Real				m_fTacticalSelectRadiusMult;
	Real				m_fEdgePanDistance;
	unsigned			m_iMaxMessages;

	// Camera settings
	bool				m_bEnableFreeCam;
	Real				m_fCamSpeed;
	Real				m_fCamRotation;
	Real				m_fChaseCamInitialDist;
	Real				m_fChaseCamMinDist;
	Real				m_fChaseCamMaxDist;
	Real				m_fChaseCamStiffness;
	Real				m_fChaseCamDamping;
	Real				m_fChaseCamZoomStiffness;
	Real				m_fChaseCamZoomSpeed;
	Real				m_fChaseCamSensitivity;
	Real				m_fChaseCamAccelSensitivity;
	Real				m_fChaseCamDecelSensitivity;

	Real				m_fTacCamInitialDist;
	Real				m_fTacCamMinDist;
	Real				m_fTacCamMaxDist;
	Real				m_fTacCamZoomStiffness;
	Real				m_fTacCamZoomSpeed;
	Real				m_fTacCamPanSpeed;
	Real				m_fTacCamSensitivity;
	Real				m_fTacCamShipLabelDist;

	Real				m_fMapCamInitialDist;
	Real				m_fMapCamMinDist;
	Real				m_fMapCamMaxDist;
	Real				m_fMapCamZoomStiffness;
	Real				m_fMapCamZoomSpeed;
	Real				m_fMapCamPanSpeed;
	Real				m_fMapCamSensitivity;

	// Key bindings
	String			  m_strZoomInKey;
	String			  m_strZoomOutKey;
	String			  m_strAccelKey;
	String			  m_strDecelKey;
	String			  m_strStrafeLKey;
	String			  m_strStrafeRKey;
	String			  m_strStrafeUKey;
	String			  m_strStrafeDKey;
	String			  m_strRollLKey;
	String			  m_strRollRKey;
	String			  m_strBrakeKey;
	String			  m_strWarpKey;
	String			  m_strCamShiftKey;
	String			  m_strSwitchModeKey;

	String			  m_strToggleTargetModeKey;
	String			  m_strNextTargetKey;
	String			  m_strUnselectTargetKey;
	String			  m_strClosestEnemyKey;
	String			  m_strNextEnemyKey;
	String			  m_strNextWeapGroupKey;

	String			  m_strToggleGroupStanceKey;
	String			  m_strPauseKey;
	// Mouse button bindings
	OIS::MouseButtonID  m_iFireGroupMButton;
	OIS::MouseButtonID  m_iFireAllMButton;
	OIS::MouseButtonID  m_iCamShiftMButton;
	OIS::MouseButtonID  m_iSwitchModeMButton;
};

#endif // BFPGENERALCONFIG_H_INCLUDED
