#ifndef BFPCONFIGPHYSICS_H_INCLUDED
#define BFPCONFIGPHYSICS_H_INCLUDED

#include <Game/GameConfig.h>

/**
 * @brief Configuration file containing values used by the physics system.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class PhysicsConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads in the physics config.
	 */
	PhysicsConfig(String strPath);

	~PhysicsConfig();

	Real GetImmovableMass() const { return m_fImmovableMass; }

private:
	Real m_fImmovableMass; // Mass value to use for objects with "infinite" mass

};

#endif // BFPCONFIGPHYSICS_H_INCLUDED
