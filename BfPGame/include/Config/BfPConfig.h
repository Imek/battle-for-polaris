#ifndef BFPCONFIG_H_INCLUDED
#define BFPCONFIG_H_INCLUDED

#include <algorithm>

#include <Game/GameConfig.h>
#include <Config/BfPConfigGeneral.h>
#include <Config/BfPConfigScenario.h>
#include <Config/BfPConfigShips.h>
#include <Config/BfPConfigWeapons.h>
#include <Config/BfPConfigPhysics.h>
#include <Config/BfPConfigAI.h>

/**
 * @brief A struct to contain the few static settings and all configuration entities used by Battle for Polaris.
 *
 * This class also contains the capability of taking a selection of world configuration, generally
 * at the start of the game where the player makes a choice of scenario.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class BfPConfig : public GameConfig
{
public:
	static const String GENERALCFG_FILE;
	static const String PHYSICSCFG_FILE;
	static const String AICFG_FILE;
	static const String SHIPSCFG_FILE;
	static const String WEAPONSCFG_FILE;

	/**
	 * @brief Construct a BfP config from config files in the given relative path.
	 */
	BfPConfig(String strPath = "");

	/**
	 * @brief Destructor for BfP config.
	 */
	virtual ~BfPConfig() { }

	/**
	 * @brief Set the selected world configuration to the parameter.
	 */
	void SetScenario(const String& rConf);

	/**
	 * @brief Get the name of the selected world configuration.
	 */
	const String& GetActiveScenarioName() const { return m_strScenario; }

	/**
	 * @brief Get a const pointer to the currently-selected world configuration
	 */
	const ScenarioConfig* GetActiveScenario() const;

private:
	String m_strScenario;
};

#endif // BFPCONFIG_H_INCLUDED
