#ifndef BFPCONFIGBODIES_H_INCLUDED
#define BFPCONFIGBODIES_H_INCLUDED

#include <Game/GameConfig.h>

/**
 * @brief Contains the settings for a particular type of body (object or location) in the game world
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class BodyType : public ObjectType
{
public:
	BodyType(String strName, const BaseConfig* pConfig);

	virtual ~BodyType() {}

	String GetModel() const { return m_strModel; }
	Real GetBoundingRadius() const { return m_fBoundingRadius; }
	Real GetScale() const { return m_fScale; }
	bool IsMajor() const { return m_bMajor; }
	bool IsBackground() const { return m_bBackground; }
	const Vector3& GetLightDiffuse() const { return m_vecLightDiffuse; }
	const Vector3& GetLightSpecular() const { return m_vecLightSpecular; }

private:
	String  m_strModel;
	Real	m_fBoundingRadius;
	Real	m_fScale;
	bool	m_bMajor;
	bool	m_bBackground; // Moves relative to the camera; mutually exclusive to m_bMajor.
	// Optional; both (0,0,0) means no light
	Vector3 m_vecLightDiffuse;
	Vector3 m_vecLightSpecular;

};

/**
 * @brief Configurations for all types of body (static object) in the game.
 *
 * A body config us used by a MapConfig, which specifies what types of bodies are in
 * which sector.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 *
 */
class BodiesConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads each body config.
	 */
	BodiesConfig(const String& rPath);

	~BodiesConfig();

	const StringVector& GetBodyTypes() const { return m_vBodyTypes; }
	const BodyType* GetBodyType(String strName) const;

private:
	StringVector			m_vBodyTypes;
	std::map<String, BodyType*>  m_mBodyTypes;

};

#endif // BFPCONFIGBODIES_H_INCLUDED
