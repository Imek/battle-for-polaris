#ifndef BFPCONFIGWEAPONS_H_INCLUDED
#define BFPCONFIGWEAPONS_H_INCLUDED

#include <Game/GameConfig.h>

/**
 * @brief A class representing a specific type of weapon
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class WeaponType : public EntityType
{
public:
	WeaponType(String strName, const BaseConfig* pConfig);

	String GetModel() const { return m_strModel; }
	Real GetScale() const { return m_fScale; }

	int GetDamage() const { return m_iDamage; }
	Real GetMomentumApplied() const { return m_fMomentumApplied; }
	unsigned GetMaxAmmo() const { return m_iMaxAmmo; }
	unsigned GetCooldownTime() const { return m_iCooldownTime; }
	unsigned GetEnergyCost() const { return m_iEnergyCost; }

	// The "Thrust" value we use is the MaxThrust value from EntityType
	Real GetAccel() const { return m_fAccel; }
	Real GetInitialSpeed() const { return m_fInitialSpeed; }
	Real GetRange() const { return m_fRange; }
	unsigned long GetLifetime() const { return m_iLifetime; }

	// Aspects of the weapon's control go here (e.g. auto-tracking, homing)
	bool GetAutoTracking() const { return m_bAutoTracking; }
	Real GetAutoTrackingMaxAngle() const { return m_fAutoTrackingMaxAngle; }
	bool GetHoming() const { return m_bHoming; }
	bool GetInfiniteAmmo() const { return m_bInfiniteAmmo; }
	bool GetNoModel() const { return m_bNoModel; }
	bool GetTargetable() const { return m_bTargetable; }

private:
	// Config values
	String		  m_strModel;
	Real			m_fScale;

	unsigned		m_iDamage;
	Real			m_fMomentumApplied;
	unsigned		m_iMaxAmmo;
	unsigned		m_iCooldownTime;
	unsigned		m_iEnergyCost;

	Real			m_fAccel;
	Real			m_fInitialSpeed;
	Real			m_fRange;
	unsigned long   m_iLifetime;

	bool			m_bAutoTracking;
	Real			m_fAutoTrackingMaxAngle;
	bool			m_bHoming;
	bool			m_bInfiniteAmmo;
	bool			m_bNoModel;
	bool			m_bTargetable;
};

/**
 * @brief Configurations for all weapons in the game
 *
 * @author Joe Forster
 */
class WeaponsConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads each weapon type.
	 */
	WeaponsConfig(String strPath);

	~WeaponsConfig();

	const WeaponType* GetWeaponType(String strName) const;

private:
	std::map<String, WeaponType*> m_mWeapons;

};

#endif // BFPCONFIGWEAPONS_H_INCLUDED
