#ifndef BFPCONFIGSHIPS_H_INCLUDED
#define BFPCONFIGSHIPS_H_INCLUDED

#include <Game/GameConfig.h>

/**
 * @brief A class representing a specific type of ship
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class ShipType : public EntityType
{
public:
	ShipType(String strName, const BaseConfig* pConfig);

	unsigned GetRPCost() const { return m_iRPCost; }

	String GetModel() const { return m_strModel; }
	Real GetScale() const { return m_fScale; }
	String GetWarpParticles() const { return m_strWarpParticles; }

	unsigned GetMaxSP() const { return m_iMaxSP; }
	unsigned GetMaxEP() const { return m_iMaxEP; }

	Real GetSPRate() const { return m_fSPRate; }
	Real GetEPRate() const { return m_fEPRate; }

	// (MaxThrust used is in EntityType)
	Real GetBrakeThrust() const { return m_fBrakeThrust; }
	Real GetStrafeThrust() const { return m_fStrafeThrust; }
	Real GetThrustRate() const { return m_fThrustRate; }

	Real GetYawRate() const { return m_fYawRate; }
	Real GetPitchRate() const { return m_fPitchRate; }
	Real GetRollRate() const { return m_fRollRate; }

	Real GetMaxYaw() const { return m_fMaxYaw; }
	Real GetMaxPitch() const { return m_fMaxPitch; }
	Real GetMaxRoll() const { return m_fMaxRoll; }

	const StringVector& GetWeapons() const { return m_vWeapons; }
	const Vector3& GetWeapOffset(unsigned iNo) const;

	unsigned long GetWarpDuration() const { return m_iWarpDuration; }
	unsigned long GetWarpDelay() const { return m_iWarpDelay; }

	const String& GetInitialStance() const { return m_strInitialStance; }

	const Real& GetStrafeRunMinDist() const { return m_fStrafeRunMinDist; }
	const Real& GetStrafeRunMaxDist() const { return m_fStrafeRunMaxDist; }

	bool GetDodgeWeaponType(const String& rWeapType) const;

private:
	// Config values
	unsigned						m_iRPCost;

	String						  m_strModel;
	Real							m_fScale;
	String						  m_strWarpParticles;

	unsigned						m_iMaxSP;
	unsigned						m_iMaxEP;

	Real							m_fSPRate;
	Real							m_fEPRate;

	// ShipThrustForce config vals
	Real							m_fMaxThrust;
	Real							m_fMaxReverseThrust;
	Real							m_fBrakeThrust;
	Real							m_fStrafeThrust;
	Real							m_fThrustRate;

	// ShipTurningForce config vals
	Real							m_fYawRate;
	Real							m_fPitchRate;
	Real							m_fRollRate;
	Real							m_fMaxYaw;
	Real							m_fMaxPitch;
	Real							m_fMaxRoll;

	// Weapon types (TODO: Make weapon load-outs separate later)
	StringVector					m_vWeapons;
	// Relative position of each weapon (could have less than the above, in which case the highest possible one is used)
	std::vector<Vector3>			m_vWeapOffsets;

	unsigned long				   m_iWarpDuration; // Duration of particle effect when warping
	unsigned long				   m_iWarpDelay; // Delay before ship disappears when warping

	// Settings & presets for low-level AI control
	// TODO: If this keeps getting bigger it may be better to have a dedicated class for it in AI config?
	String					  m_strInitialStance;
	Real						m_fStrafeRunMinDist;
	Real						m_fStrafeRunMaxDist;
	std::set<String>			m_sDodgeWeaponTypes;

};

/**
 * @brief Configurations for all ships in the game
 */
class ShipsConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads each ship config.
	 */
	ShipsConfig(String strPath);

	~ShipsConfig();

	const StringVector& GetShipTypes() const { return m_vShips; }
	const ShipType* GetShipType(String strName) const;

	bool ValidShipGroup(const StringVector& rTypes) const;

private:
	StringVector			m_vShips;
	std::map<String, ShipType*>  m_mShips;

};

#endif // BFPCONFIGSHIPS_H_INCLUDED
