#ifndef BFPCONFIGSCENARIO_H_INCLUDED
#define BFPCONFIGSCENARIO_H_INCLUDED

#include <Game/GameConfig.h>
#include <Config/BfPConfigMap.h>
#include <Config/BfPConfigMissions.h>


/**
 * @brief Contains the settings for a particular starting group in a faction config.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class StartingGroup : public ObjectType
{
public:
	StartingGroup(String strName, const BaseConfig* pConfig);

	virtual ~StartingGroup() {}

	const String& GetSector() const { return m_strSector; }
	const String& GetShipType() const { return m_strShipType; }
	unsigned GetShipCount() const { return m_iShipCount; }

private:
	String m_strSector;
	String m_strShipType;
	unsigned m_iShipCount;

};

/**
 * @brief Contains the settings for a particular faction in a scenario.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class FactionType : public ObjectType
{
public:
	FactionType(String strName, const BaseConfig* pConfig);

	virtual ~FactionType() {}

	const Vector3& GetUnselectedColour() const { return m_vecUnselectedColour; }
	const Vector3& GetSelectedColour() const { return m_vecSelectedColour; }
	const String& GetController() const { return m_strController; }
	const StringVector& GetStartingSectors() const { return m_vStartingSectors; }
	const String& GetHomeSector() const
	{
		assert(!m_vStartingSectors.empty());
		return *m_vStartingSectors.begin();
	}
	long GetStartingRPs() const { return m_iStartingRPs; }

	const StringVector& GetStartingGroups() const { return m_vGroupNames; }
	const StartingGroup& GetStartingGroup(const String& rName) const;

private:
	Vector3		 m_vecUnselectedColour;
	Vector3		 m_vecSelectedColour;
	String		  m_strController;
	StringVector	m_vStartingSectors;
	long			m_iStartingRPs;

	StringVector	m_vGroupNames;
	std::map<String, StartingGroup*> m_mGroups;

};

/**
 * @brief The configuration for the game scenario, representing all static aspects of the world.
 *
 * A scenario consists of a game map, a mission config, a set of faction configs, and possibly various
 * configuration values. The intention is for players to be able to create custom scenarios
 * in the future.
 *
 * Note that the purposes of specific configuration values are documented in the .cfg files themselves.
 *
 * @author Joe Forster
 */
class ScenarioConfig : public BaseConfig
{
public:
	/**
	 * @brief Constructor looks in the given path for a config file, then reads in the config.
	 */
	ScenarioConfig(String strPath);

	~ScenarioConfig();

	// Victory event configs
	bool GetWipeOutVictory() const { return m_bWipeOutVictory; }
	bool GetAssassinationVictory() const { return m_bAssassinationVictory; }
	bool GetHomeInvasionVictory() const { return m_bHomeInvasionVictory; }

	// Functions to get values from the configs within for convenience

	bool GetEventsEnabled(const String& rCategory) const { return (m_pMissionsConfig && m_pMissionsConfig->GetEventsEnabled(rCategory)); }

	Real GetInitialMapScale() const { return m_pMapConfig->GetInitialMapScale(); }
	unsigned long GetRPIncomeDelay() const { return m_pMapConfig->GetRPIncomeDelay(); }
	unsigned long GetRPMaxAmount() const { return m_pMapConfig->GetRPMaxAmount(); }
	unsigned long GetSectorCaptureDelay() const { return m_pMapConfig->GetSectorCaptureDelay(); }
	const String& GetMapStarTexture() const { return m_pMapConfig->GetMapStarTexture(); }
	Real GetMapTextSize() const { return m_pMapConfig->GetMapTextSize(); }
	Real GetMapNodeSize() const { return m_pMapConfig->GetMapNodeSize(); }
	bool GetDrawUnselectedBodyMarkers() const { return m_pMapConfig->GetDrawUnselectedBodyMarkers(); }
	Real GetSpawnDeviationStep() const { return m_pMapConfig->GetSpawnDeviationStep(); }
	Real GetMaxSpawnDeviation() const { return m_pMapConfig->GetMaxSpawnDeviation(); }
	Real GetMaxWarpDist() const { return m_pMapConfig->GetMaxWarpDist(); }

	const StringVector& GetSectorTypes() const { return m_pMapConfig->GetSectorTypes(); }
	const SectorType* GetSectorType(const String& rName) const { return m_pMapConfig->GetSectorType(rName); }

	const StringVector& GetBodyTypes() const { return m_pMapConfig->GetBodyTypes(); }
	const BodyType* GetBodyType(const String& rName) const { return m_pMapConfig->GetBodyType(rName); }

	/**
	 * @brief Get the configuration for missions/triggers in this scenario
	 *
	 * Note that this will throw an exception if we have no missions -
	 * so check with HasMissionsConfig first!
	 */
	const MissionsConfig& GetMissionsConfig() const
	{
		if (!m_pMissionsConfig)
			throw JFUtil::Exception("ScenarioConfig", "GetMissionsConfig",
				"Tried to get this config when there was none given for this scenario");
		return *m_pMissionsConfig;
	}

	/**
	 * @brief See whether there is a missions config file for this scenario
	 */
	bool HasMissionsConfig() const { return m_pMissionsConfig; }

	const StringVector& GetFactionNames() const { return m_vFactionNames; }
	const FactionType* GetFactionType(String strName) const;


private:
	// Basic victories outside of missions
	bool				m_bWipeOutVictory;
	bool				m_bAssassinationVictory;
	bool				m_bHomeInvasionVictory;

	MapConfig*		  m_pMapConfig;

	MissionsConfig*	 m_pMissionsConfig;

	StringVector		m_vFactionNames;
	std::map<String, FactionType*> m_mFactions;

};


#endif // BFPCONFIGSCENARIO_H_INCLUDED
