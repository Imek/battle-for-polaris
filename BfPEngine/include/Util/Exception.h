#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <sstream>
#include <string>

using namespace std;

// JFUtil code for exceptions. Right now our Exception class is fairly simple..
namespace JFUtil
{

	/**
	 * @brief A simple exception class for error handling.
	 *
	 * The intention is for this object to be thrown as an exception after the pertinent information
	 * has been inserted into its string. We use templates and streams here so any arbitrary type
	 * with the proper extraction/insertion functions can be put in there.
	 *
	 * @author Joe Forster
	 */
	class Exception
	{
	private:
		string m_strText;

	public:
		/**
		 * @brief Create an exception with a class, function, and optional message.
		 */
		template <typename T>
		Exception(string strClass, string strFunction, string strMessage = "", const T& rObj = NULL)
		{
			std::stringstream ss;
			ss << "Exception in " << strClass << "::" << strFunction << ": ";
			ss << endl << strMessage << " " << rObj << endl;
			m_strText = ss.str();
		}

		Exception(string strClass, string strFunction, string strMessage = "")
		{
			std::stringstream ss;
			ss << "Exception in " << strClass << "::" << strFunction << ": ";
			ss << endl << strMessage << endl;
			m_strText = ss.str();
		}

		/**
		 * @brief Append an object (using a stream) to the string.
		 */
		template <typename T>
		void Append(const T& rObj)
		{
			std::stringstream ss;
			ss << m_strText << " " << rObj << endl;
		}

		string GetString() const
		{
			return m_strText;
		}

	};
}

#endif /* EXCEPTION_H */
