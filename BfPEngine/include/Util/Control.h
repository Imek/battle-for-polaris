#ifndef UTILCONTROL_H_INCLUDED
#define UTILCONTROL_H_INCLUDED

#include <map>

#include <State/GameState.h>

/**
 * @brief a namespace containing some utility code for AI and control.
 *
 * @author Joe Forster
 */
namespace UtilControl
{
	/**
	 * Work out in what direction (given a vector) an entity needs to aim in order to hit
	 * a moving target with a projectile weapon.
	 *
	 * @param	rOrigin		  the current position of the ship or gun
	 * @param   rMyVel		the current motion of the entity firing the projectile
	 * @param   fAccel		  the acceleration for the projectile being fired
	 * @param   fVel		  the initial velocity of the projectile being fired
	 * @param   iLifetime	  the duration the fired projectile lasts.
	 * @param	rTarget		  the current position of the target
	 * @param	rTargVel	  the current velocity of the target
	 * @param   pInRange	  a boolean value, indicating whether we're in firing range, is put here.
	 * @param   pHitPos	   a Vector, indicating the position of the target if bInRange is true, is put here.
	 *						otherwise, it will be set to Vector3::ZERO.
	 *
	 * @return	a pointer to a vector showing the new facing direction that will be required
				to hit the target, or best estimate.
	 */
	Vector3 LeadTarget(
		const Vector3& rOrigin, const Vector3& rMyVel, Real fAccel, Real fVel, unsigned iLifetime,
		const Vector3& rTarget, const Vector3& rTargVel, bool* pInRange, Vector3* pHitPos);

	/**
	 * @brief Use bounding spheres to predict whether two objects will collide within a given period.
	 *
	 * @param rPositionA	the initial position of the first object
	 * @param rVelA		 the initial velocity of the first object
	 * @param rAccelA	   the (overall, average) acceleration of the first object
	 * @param rRadiusA	  the bounding radius of the first object
	 * @param rPositionB	the initial position of the second object
	 * @param rVelB		 the initial velocity of the second object
	 * @param rAccelB	   the (overall, average) acceleration of the second object
	 * @param rRadiusB	  the bounding radius of the second object
	 * @param iMaxTime	  the maximum amount of time forward to predict
	 * @param rMaxDist	  the maximum distance
	 * @param iTimeStep	 the amount to increment time every prediction step
	 *
	 * @return whether they will collide given our prediction
	 */
	bool CollisionCourse(
		const Vector3& rPositionA, const Vector3& rVelA, const Vector3& rAccelA, const Real& rRadiusA,
		const Vector3& rPositionB, const Vector3& rVelB, const Vector3& rAccelB, const Real& rRadiusB,
		unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep);

	/**
	 * Work out the amount of yaw and pitch that is required to turn our ship to
	 * face a given target direction, using distances from planes.
	 *
	 * @param	rMyFacing	the facing direction of the thing being turned
	 * @param	rMyNormal	the normal of the thing being turned
	 * @param	rTargetDir	the direction towards which to turn
	 *
	 * @return	A vector whose x/y values are values between -2 and 2 representing distances to turn over yaw/pitch respectively.
	 */
	Vector2 GetNeededTurn(
		const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetDir);

	/**
	 * @brief An equivalent of GetNeededTurn for rolling towards a normal
	 *
	 * @param	rMyFacing	the facing direction of the thing being turned
	 * @param	rMyNormal	the normal of the thing being turned
	 * @param	rTargetDir	the direction towards which to turn
	 *
	 * @return A value between -2 and 2 representing the amount of roll needed.
	 */
	Real GetNeededRoll(
		const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetUp);


	/**
	 * Work out the amount of yaw/pitch (as angles between -180 and 180) needed to turn to face a direction.
	 *
	 * @param	rMyFacing	the facing direction of the thing being turned
	 * @param	rMyNormal	the normal of the thing being turned
	 * @param	rTargetDir	the direction towards which to turn
	 * @param   rToYaw	  assigned - the angle to yaw.
	 * @param   rToPitch	assigned - the angle to pitch.
	 *
	 */
	void GetTurnAngle(
		const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetDir,
		Degree& rToYaw, Degree& rToPitch);

	/**
	 * Work out the amount of roll (as an angle between -180 and 180) needed to match our normal with a given normal.
	 *
	 * NOTE: This doesn't seem to be used currently?
	 *
	 * @param	rMyFacing	the facing direction of the thing being turned
	 * @param	rMyNormal	the normal of the thing being turned
	 * @param	rTargetUp	the direction towards which to turn
	 */
	Degree GetRollAngle(
		const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetUp);

};

#endif // UTILCONTROL_H_INCLUDED
