#ifndef UTILMATHS_H_INCLUDED
#define UTILMATHS_H_INCLUDED

#include <BfPEngineDefs.h>

// Some mathematic utility code (for now just for random generation of vectors/numbers)
namespace JFUtil
{
	/**
	 * @brief Generate a random real number.
	 *
	 * @param rOne  The minimum
	 * @param rTwo  The maximum
	 *
	 * @return A random number.
	 */
	Real RandomBetween(const Real& rOne, const Real& rTwo);

	/**
	 * @brief	Return a random vector rotated from the given centre vector.
	 *
	 * This vector is rotated a random amount (between the minimum and maximum), within a cone
	 * of that size from the centre vector.
	 *
	 * @param	rCentre		the centre vector to rotate from
	 * @param	degMinAngle	the minimum angle to vary from the centre
	 * @param	degMaxAngle	the maximum angle to vary from the centre
	 *
	 * @return	the randomised vector.
	 */
	Vector3 RandomVectorFromCentre(
		const Vector3& rCentre, Degree degMinAngle, Degree degMaxAngle);
}

#endif // UTILMATHS_H_INCLUDED
