#ifndef UTILSTRING_H_INCLUDED
#define UTILSTRING_H_INCLUDED

#include <BfPEngineDefs.h>
#include <Util/Exception.h>

#include <sstream>

// UtilString contains some functions in the JFUtil namespace related to strings.
// We use the String definition from OGRE everywhere in the interests of consistency.
namespace JFUtil
{
	/**
	 * @brief String equality (case-insensitive) check.
	 */
	bool StringEquals(const String& rOne, const String& rTwo);

	/**
	 * @brief Read in a Vector3 from a string using the format "(x,y,z)"
	 */
	Vector3 VectorFromString(const String& rString);

	/**
	 * @brief Get a string representation of a Vector3 in the format "(x,y,z)"
	 */
	String StringFromVector(const Vector3& rVector);

}


#endif // UTILSTRING_H_INCLUDED
