#ifndef SINGLETON_H_INCLUDED
#define SINGLETON_H_INCLUDED

#include <assert.h>

#include <BfPEngineDefs.h>

namespace JFUtil
{
	/**
	 * @brief Simple template class for singletons.
	 */
	template<typename T> class Singleton
	{
	public:
		Singleton()
		{
			assert(!s_instance);
			s_instance = static_cast<T*>(this);
		}
		~Singleton()
		{
			assert(s_instance);
			s_instance = NULL;
		}
		static T& GetInstance()
		{
			assert(s_instance);
			return *s_instance;
		}
		static T* GetInstancePtr()
		{
			return s_instance;
		}

	protected:
		static T* s_instance;

	private:
		// Copy and assignment obviously shouldn't be accessible for singletons.
		Singleton(const Singleton<T> &);
		Singleton& operator=(const Singleton<T> &);
	};
}

#endif // SINGLETON_H_INCLUDED
