#ifndef EVENTS_H
#define EVENTS_H

#include <list>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>

#include <Util/Exception.h>

using namespace std;

/**
 * @brief A generic event class for the event handler.
 *
 * This class represents a game event, generally collisions. The EventHandler class
 * manages and responds to Event objects.
 *
 * @author Joe Forster
 */
class Event
{
public:

	/**
	 * @brief Constructor for an event, given the type.
	 *
	 * @param	iType	the type from the String enum.
	 */
	Event(const string& rType) : m_strType(rType) {}
	virtual ~Event() {}

	/**
	 * @brief Copy constructor shouldn't be used; use EventFactory instead!
	 */
	Event(const Event& rEvent)
	{
		throw JFUtil::Exception("Event", "Event",
			"Whoops! Attempted to use copy constructor (use factory instead)");
	}

	/**
	 * @brief Get the String value given to this event.
	 */
	const string& GetType() const { return m_strType; }

	/**
	 * @brief Get a string for debug purposes.
	 */
	virtual string GetString() const = 0;

	/**
	 * @brief Perform whatever resolution operations needed by the event.
	 */
	virtual void Resolve(unsigned long iTime) = 0;

private:
	string m_strType;

};

// Event typedefs
typedef std::vector<Event*> EventVector;
typedef std::list<Event*> EventList;

/**
 * @brief An event that holds a sequence of sub-events to be executed/retrieved a given order.
 *
 * @author Joe Forster
 */
class EventSequence : public Event
{
public:
	/**
	 * @brief Constructor just makes an empty sequence
	 */
	EventSequence(const string& rType);

	EventSequence(const EventSequence& rEvent);

	/**
	 * @brief Destructor needs to call the destructor of each message.
	 */
	virtual ~EventSequence();

	/**
	 * @brief Get the type of sequence given to the constructor.
	 */
	const string& GetSeqType() const { return m_strSeqType; }

	/**
	 * @brief Add an event to the sequence.
	 */
	void AddEvent(Event* pEvent);

	// Member getters
	const EventVector& GetEvents() const { return m_vEvents; }

	EventVector& GetEvents() { return m_vEvents; }

	/**
	 * @brief GetString gets the string for each.
	 */
	string GetString() const;

	/**
	 * @brief Resolve each event.
	 */
	void Resolve(unsigned long iTime);

private:
	string m_strSeqType;
	EventVector m_vEvents;
};

/**
 * @brief A class for generic message events.
 *
 * @author Joe Forster
 */
class EventMessage : public Event
{
public:
	EventMessage(string strMessage = "")
	: Event("EV_MESSAGE"), m_strMessage(strMessage) {}

	EventMessage(const EventMessage& rObj)
	: Event("EV_MESSAGE"), m_strMessage(rObj.m_strMessage) {}

	void Append(string strAdd)
	{
		m_strMessage.append(strAdd);
	}

	/**
	 * @brief GetString just gets the message.
	 */
	string GetString() const { return m_strMessage; }

	/**
	 * @brief Resolve a message by just printing it to stdout.
	 */
	void Resolve(unsigned long iTime) { cout << m_strMessage << endl; }

private:
	string m_strMessage;

};

/**
 * @brief A basic event handler class.
 *
 * This class keeps a list of Event objects, which can be pushed, popped and cleared.
 *
 * @author Joe Forster
 */
class EventHandler
{
public:
	/**
	 * @brief Default constructor.
	 */
	EventHandler() {}

	/**
	 * @brief Copy constructor shouldn't be used; use EventFactory instead!
	 */
	EventHandler(const EventHandler& rHandler);

	/**
	 * @brief Destructor destroys all the events in the queue.
	 */
	virtual ~EventHandler();

	const EventList& GetEventList() const { return m_lEvents; }

	size_t GetSize() const { return m_lEvents.size(); }

	// Shove a new event onto the top (back) of the stack.
	void PushEvent(Event* pEvent);

	/**
	 * @brief Take the next event off the front of the queue.
	 *
	 * @return a pointer to the event. Note that you need to delete it when finished.
	 */
	Event* PopEvent();

	/**
	 * @brief Pop all events of a given type, while leaving others in the handler.
	 *
	 * The caller now has responsibility for managing the memory of any pointers returned.
	 *
	 * @param rType The type of event for which to search
	 *
	 * @return A new list of pointers to those events
	 */
	EventList PopEventsOfType(const string& rType);

	/**
	 * @brief Clear the entire event queue, disposing of the events.
	 */
	void Clear();

	/**
	 * @brief Check if the queue is empty.
	 */
	bool Empty() const;

protected:
	EventList m_lEvents;

};

/**
 * @brief Singleton wrapper class for a global event handler.
 *
 * @author Joe Forster
 */
class GlobalEvents
{
public:
	static EventHandler& Get()
	{
		if (!m_pInstance)
			m_pInstance = new EventHandler();
		return *m_pInstance;
	}

private:
	static EventHandler* m_pInstance;

};

#endif /* EVENTS_H */
