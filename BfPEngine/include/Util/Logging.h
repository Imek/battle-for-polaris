#ifndef UTILDEBUGPRINT_H_INCLUDED
#define UTILDEBUGPRINT_H_INCLUDED

// TODO: This file needs renaming

class Logging
{
public:
	enum LOG_LEVEL { debug, info, warn, error, critical };

	/**
	 * @brief Log some event, given a severity level, category and message.
	 *
	 * Currently just functions as a cross-platform debug output function.
	 *
	 * @author Joe Forster
	 */
	static void Log(LOG_LEVEL logLevel, const char* category, const char* message);

};

#endif // UTILDEBUGPRINT_H_INCLUDED
