#ifndef AIBAYESIANNETWORK_H_INCLUDED
#define AIBAYESIANNETWORK_H_INCLUDED

#include <map>
#include <set>
#include <list>
#include <vector>

#include <BfPEngineDefs.h>

/*
 * NOTE: This is all currently just a work in progress. Needs proper finished implementation and
 * integration into the game-specific high-level AI system.
 */

// TODO: could re-use a lot of the below code in a more generic graph class - if/when needed.

/**
 * @brief Represents an independent element of world state with an associated probability.
 */
class NodeLabel
{
public:
	virtual ~NodeLabel() {};

	virtual const String& GetString() const = 0;

	inline bool operator< (const NodeLabel& other) const { return GetString() < other.GetString(); }
	inline bool operator== (const NodeLabel& other) const { return GetString() == other.GetString(); }
	inline bool operator!= (const NodeLabel& other) const { return GetString() != other.GetString(); }
};

/**
 * @brief Represents an independent element of the probabilistic world state.
 */
class StateEntry
{
public:
	virtual ~StateEntry() {};

	virtual Real GetProbability() const = 0;
	virtual void SetProbability(Real newValue) const = 0;
	virtual void AlterProbability(Real amount) const = 0;
};

class BayesianNetworkNode;

typedef std::map<unsigned, StateEntry*> EntryMap;
typedef std::map<String, BayesianNetworkNode*> NodePtrMap;
typedef std::vector<BayesianNetworkNode*> NodePtrVector;
typedef std::list<BayesianNetworkNode*> NodePtrList;
typedef std::set<BayesianNetworkNode*> NodePtrSet;

/**
 * @brief A node in a Bayesian network contains a label (event) and some probabilistic state entei.
 *
 * A node also contains a list of connected nodes (events that depend upon this event).
 */
class BayesianNetworkNode
{
public:
	BayesianNetworkNode(NodeLabel* label);
	~BayesianNetworkNode();

	inline bool operator== (const BayesianNetworkNode& other) const { return *GetLabel() == *(other.GetLabel()); }
	inline bool operator!= (const BayesianNetworkNode& other) const { return *GetLabel() != *(other.GetLabel()); }
	inline bool operator< (const BayesianNetworkNode& other) const { return *GetLabel() < *(other.GetLabel()); }

	bool AddStateEntry(unsigned stateID, StateEntry* newEntry);

	bool AttachNode(BayesianNetworkNode& child);
	void OnAttachedToNode(const BayesianNetworkNode& parent);

	const NodeLabel* GetLabel() const { return m_label; }
	size_t GetNumChildren() const { return m_childNodes.size(); }
	bool HasChild(const BayesianNetworkNode& child) const;

	EntryMap* GetStateEntries() { return &m_stateEntries; }
	NodePtrMap* GetChildNodes() { return &m_childNodes; }

private:
	NodeLabel* m_label;
	// Flag that keeps track of whether this is an "origin" node, i.e. one without any vertices
	// pointing at it.
	bool m_isOrigin;

	// NOTE: Could use unordered_map if we move to C++11
	EntryMap m_stateEntries;
	NodePtrMap m_childNodes;
};

/**
 * @brief A generic Bayesian network is a graph, with nodes as events and edges as causal relations between said events.
 */
class BayesianNetwork
{
public:
	BayesianNetwork() {};
	~BayesianNetwork();

	BayesianNetworkNode& GetNode(const String& labelStr);

	// Attach a new node - note that it will be managed by this network (destroyed in destructor).
	bool AddNode(BayesianNetworkNode* newNode);

	// Construct a list of origin nodes for the current structure of the network, returning a
	// pointer to it. The caller should make sure it's deleted when appropriate.
	NodePtrList* MakeOriginNodeList() const;

private:
	// Main managed place to keep all nodes, mapped by label for easy lookup.
	NodePtrMap m_nodesByLabel;
};

/**
 * @brief An abstract class representing an algorithm for propagating inferrences across a Bayesian network.
 */
class BayesianNetworkUpdater
{
public:
	/**
	 * @brief Traverse the network once, updating probabilities as if one AI tick has passed.
	 *
	 * Base implementation of this is to perform a breadth-first traversal of the network,
     * Using the implementation of PropagateToChildren to propagate probabilities.
	 */
	virtual void UpdateNetwork(BayesianNetwork& network);

private:
	/**
	 * @brief Update probabilities from one node to its children.
	 */
	virtual void PropagateToChildren(BayesianNetworkNode& parent) = 0;
};

/**
 * @brief A simple BayesianNetworkUpdater implementation that just propagates probabilities evenly.
 */
class EvenSplitUpdater : public BayesianNetworkUpdater
{
private:
	/**
	 * @brief Our implementation to propagate by splitting evenly between the node and children.
	 */
	virtual void PropagateToChildren(BayesianNetworkNode& parent);
};

// TODO: Factory base class goes here. BfP implementation of the factory constructs
// nodes/network based on an abstract state.

#endif // AIBAYESIANNETWORK_H_INCLUDED
