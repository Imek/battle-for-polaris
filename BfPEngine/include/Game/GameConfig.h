#ifndef GAMECONFIG_H_INCLUDED
#define GAMECONFIG_H_INCLUDED

#include <map>
#include <sstream>

#include <OgreConfigFile.h>
#include <OISMouse.h>

#include <BfPEngineDefs.h>

#include <Util/String.h>
#include <Util/Singleton.h>

#include <Physics/Intersection.h>

using JFPhysics::BoundingVolume;
using JFPhysics::BoundingSphere;

/**
 * @brief A simple wrapper for Ogre's Ogre::ConfigFile class to use as a base class for our game configs.
 *
 * @author Joe Forster
 */
class BaseConfig
{
public:
	BaseConfig(const String& rPath);

	virtual ~BaseConfig() {}

	const String& GetFilePath() const { return m_strPath; }

	/**
	 * @brief Attempt to read a value of a given type from the configuration.
	 *
	 * @return Either a pointer to a newly allocated value, or NULL if not found/valid.
	 */
	template <typename T>
	T* GetValue(String strKey, String strSection = "") const
	{
		assert(!strKey.empty());
		T val;
		std::stringstream ss;
		// Could return blank (not found)
		if (strSection.empty())
			ss << m_oConfig.getSetting(strKey);
		else
			ss << m_oConfig.getSetting(strKey, strSection);

		if (!(ss >> val))
			return NULL;
		else
			return new T(val);
	}

	/**
	 * @brief Attempt to read a string (including whitespace) from the configuration.
	 *
	 * @return Either a pointer to a newly-allocated string, or NULL if not found.
	 */
	String* GetString(String strKey, String strSection = "") const;

	/**
	 * @brief Attempt to read a list of values of a given type from the configuration.
	 *
	 * @return A possibly empty list of T values
	 */
	template <typename T>
	std::vector<T> GetValues(String strKey, String strSection = "") const
	{
		assert(!strKey.empty());
		std::vector<T> vReturn;
		std::stringstream ss;

		StringVector vVals;
		if (strSection.empty())
			vVals = m_oConfig.getMultiSetting(strKey);
		else
			vVals = m_oConfig.getMultiSetting(strKey, strSection);

		for (StringVector::iterator it = vVals.begin(); it != vVals.end(); ++it)
		{
			T oVal;
			ss << *it;
			if (!(ss >> oVal).fail())
				vReturn.push_back(oVal);
			ss.str(""); ss.clear();
		}
		return vReturn;
	}

	/**
	 * @brief Attempt to read a list of strings from the configuration.
	 *
	 * @return A possibly empty string vector
	 */
	StringVector GetStrings(String strKey, String strSection = "") const
	{
		assert(!strKey.empty());

		StringVector vVals;
		if (strSection.empty())
			vVals = m_oConfig.getMultiSetting(strKey);
		else
			vVals = m_oConfig.getMultiSetting(strKey, strSection);

		return vVals;
	}

protected:
	Ogre::ConfigFile m_oConfig;

private:
	// Remember the file path for error messages
	String m_strPath;

	// I don't think these will be needed
	BaseConfig(const BaseConfig& rFile);
	BaseConfig& operator=(const BaseConfig& rFile);
};


/**
 * BaseConfig typedefs
 */
typedef std::map<String, BaseConfig*> ConfigMap;
typedef ConfigMap::const_iterator ConfigMapIt;


/**
 * @brief A class for a type of object contained within a configuration.
 *
 * @author Joe Forster
 */
class ObjectType
{
public:
	ObjectType(String strName, const BaseConfig* pConfig);

	virtual ~ObjectType() {}

	const String& GetName() const { return m_strName; }
	const String& GetReadableName() const { return m_strReadableName; }

private:
	String m_strName;
	String m_strReadableName;
};

/**
 * @brief An abstract subclass for a configuration of a physical object (ship or weapon projectile)
 *
 * @author Joe Forster
 */
class EntityType : public ObjectType
{
public:
	EntityType(String strName, const BaseConfig* pConfig);

	virtual ~EntityType() { delete m_pBV; }

	Real GetBoundingRadius() const { return m_fBoundingRadius; }
	const BoundingVolume* GetBV() const { return m_pBV; }

	Real GetMass() const
	{
		assert(m_fMass > 0);
		return m_fMass;
	}

	Real GetInvMass() const
	{
		assert(m_fMass > 0);
		return 1/m_fMass;
	}
	Real GetDamping() const { return m_fDamping; }
	Real GetMaxSpeed() const { return m_fMaxSpeed; }
	Real GetMaxThrust() const { return m_fMaxThrust; }
	Real GetAngularDamping() const { return m_fAngularDamping; }
	const Matrix3& GetInertiaTensor() const { return m_mxInertiaTensor; }

	// Properties relating to destruction
	bool GetInvincible() const { return m_bInvincible; }
	unsigned GetMaxHP() const { return m_iMaxHP; }
	// Particles used for engines/trails
	String GetTrailParticles() const { return m_strTrailParticles; }
	String GetExplosionParticles() const { return m_strExplosionParticles; }
	unsigned long GetExplodeDuration() const { return m_iExplodeDuration; }
	unsigned long GetExplodeDelay() const { return m_iExplodeDelay; }


private:
	String			  m_strName;
	String			  m_strReadableName;

	Real				m_fBoundingRadius;
	BoundingVolume*	 m_pBV;

	Real				m_fMass;
	Real				m_fDamping;
	Real				m_fMaxSpeed;
	Real				m_fMaxThrust;
	Real				m_fAngularDamping;
	Matrix3			 m_mxInertiaTensor;

	bool				m_bInvincible;
	unsigned			m_iMaxHP;
	String			  m_strTrailParticles; // Particle effect for trail/engine
	String			  m_strExplosionParticles; // Particle effect to use when exploding
	unsigned long	   m_iExplodeDuration; // Duration of particle effect when exploding
	unsigned long	   m_iExplodeDelay; // Delay before entity disappears when exploding


};

/**
 * @brief a singleton for keeping all the BaseConfig classes for our game in one place.
 *
 * @author Joe Forster
 */
class GameConfig : public JFUtil::Singleton<GameConfig>
{
public:
	static const String CFG_PATH;

	GameConfig() {}

	virtual ~GameConfig();

	const BaseConfig* GetConfig(const String& rType) const;

protected:
   ConfigMap m_mConfigs;
};

#endif // GAMECONFIG_H_INCLUDED
