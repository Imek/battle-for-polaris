#ifndef GAMESCENE_H_INCLUDED
#define GAMESCENE_H_INCLUDED

#include <Ogre.h>

#include <BfPEngineDefs.h>
#include <Util/Exception.h>
#include <Ogre/OgreFramework.h>

enum GAME_SCENE_TYPE { GSCT_SECTOR, GSCT_MAP };

/**
 * @brief A class representing an OGRE scene used as some view or mode of the game.
 *
 * @author Joe Forster
 */
class GameScene
{
public:
	/**
	 * @brief Remove and destroy all attached objects for a scene node
	 */
	static void DestroyAttachedObjects(Ogre::SceneNode* pNode);


	GameScene(GAME_SCENE_TYPE iType, String strName);

	virtual ~GameScene();

	/**
	 * @brief Implement this with code to set up the scene.
	 */
	virtual void CreateScene() = 0;

	/**
	 * @brief Implement this with any code required to update the scene.
	 */
	virtual void UpdateScene(unsigned long iTime) = 0;

	/**
	 * @brief Implement this with code to destroy/dispose of the scene.
	 */
	virtual void DestroyScene() = 0;

	/**
	 * @brief Attach the scene to the main root.
	 */
	virtual void Attach();

	/**
	 * @brief Detach the scene from the main root.
	 */
	virtual void Detach();

	const String& GetName() const { return m_strName; }
	bool IsAttached() const { return m_bAttached; }


protected:
	// The node that encompasses this whole scene.
	Ogre::SceneNode*	m_pSceneNode;

	// Convenience access to singletons
	OgreFramework*	  m_pOgre;
	Ogre::SceneManager* m_pSceneMgr;

private:

	GAME_SCENE_TYPE	 m_iType;
	String			  m_strName;
	bool				m_bAttached;
};

#endif // GAMESCENE_H_INCLUDED
