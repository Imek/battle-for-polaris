#ifndef SECTORSTATE_H_INCLUDED
#define SECTORSTATE_H_INCLUDED

#include <State/GameState.h>

// TODO: Should use an enum for object type, defined in the Obj
/**
 * @brief Generic class for a sector in the game world: a zone in the world defined as a single
 *		scene, to which objects and entities can be added and removed
 *
 * @author Joe Forster
 */
class SectorState
{
public:
	/**
	 * @brief Construct a blank SectorState given a name.
	 */
	SectorState(const String& rName, const StringVector& rObjTypes);

	/**
	 * @brief Copy constructor for SectorState copies the objects
	 */
	SectorState(const SectorState& rState);
	virtual ~SectorState();


	/**
	 * @brief Get a reference to the vector of a particular object type
	 */
	const ObjList& GetObjectList(String strType) const;

	/**
	 * @brief Get a non-const reference to the vector of players
	 */
	ObjList& GetObjectList(String strType);

	/**
	 * @brief Get a reference to a specific ObjState, returning NULL if not found
	 */
	const ObjState* GetObjState(String strType, unsigned iID) const;

	/**
	 * @brief Non-const version of GetObjState
	 */
	ObjState* GetObjState(String strType, unsigned iID);

	/**
	 * @brief Get all the objects of a given type in the world that are freshly added.
	 *
	 * @param	iType	The type of object to look for
	 *
	 * @return	A list of pointers to freshly-created objects that have yet to be
	 *			added to the Physics engine. Note that this needs to be done every update
	 *			cycle.
	 */
	DynObjList GetNewObjects(String strType);

	/**
	 * @brief Get all the objects of a given type that are flagged for removal from the physics system.
	 *
	 * Also removes those objects from the list, so you need to dispose of them!!
	 *
	 * @param	iType	The type of object to look for
	 *
	 * @return	A list of copies of objects with their remove flags set to true.
	 *			Note that this needs to be done every Physics update cycle.
	 */
	DynObjList GetRemovedObjects(String strType);

	/**
	 * Determine if a given ID number is that of an existing object.
	 */
	bool ObjectExists(String strType, unsigned iID) const;


protected:

	String			  m_strName;

	// TODO: We should have a generic config type here from which we can get stuff like this.
	// Then we can probably move more stuff from BfPSectorState into SectorState.
	const StringVector& m_rObjTypes;

	/**
	 * @brief Map of object type of lists of all objects in this sector of that type; this is where the memory is managed.
	 */
	ObjListMap  m_mObjects;
};

#endif // SECTORSTATE_H_INCLUDED
