#ifndef GameState_H_INCLUDED
#define GameState_H_INCLUDED

#include <iostream>
#include <vector>

#include <OgreNode.h>

#include <Physics/Intersection.h>

#include <Game/GameConfig.h>


using namespace std;

using JFPhysics::BVFactory;
using JFPhysics::BoundingVolume;
using JFPhysics::BoundingSphere;

/* TODO: We don't really use anything above EntityState yet in this inheritance tree.
 * The intention is to eventually have Bodies added from the config to the world state
 * as ObjStates, rather than read from the config as it is now. This hasn't been done yet
 * because it's a fairly big undertaking.
 */
/**
 * @brief An abstract base class for static objects that won't be added or removed dynamically.
 *
 * @author Joe Forster
 */
class ObjState
{
private:
	String	  m_strType;
	unsigned	m_iID;

	/**
	 * @brief Get the next available unique object ID; assumes it will work (we increment on this call)
	 */
	static int GetNextID()
	{
		static int iNextID = 0;
		return iNextID++;
	}

public:
	/**
	 * @brief Create the object state with a given type.
	 */
	ObjState(String strType);

	virtual ~ObjState() {  }

	virtual bool IsDynamic() const { return false; }

	/**
	 * @brief Get the type of this object.
	 */
	const String& GetType() const { return m_strType; }

	/**
	 * @brief Get the ID number of this object.
	 */
	virtual unsigned GetID() const;

};

typedef std::vector<ObjState*> ObjList;
typedef std::map< String, ObjList > ObjListMap;

// TODO: Can DynamicObjState and EntityState be combined? Can we make this system more modular?


/**
 * @brief An abstract class for objects to be added or removed from the game state.
 *
 * A DynamicObjState, an extension of ObjState, is any object that could be dynamically
 * added or removed from the game. We need to keep track of this addition and removal,
 * for example for the graphics and physics systems.
 *
 * @author Joe Forster
 */
class DynamicObjState : public ObjState
{
public:
	DynamicObjState(String strType, unsigned iID);

	DynamicObjState(String strType);

	virtual ~DynamicObjState() {}


	virtual bool IsDynamic() const { return true; }

	/**
	 * @brief Virtual function for whether this object is fresh to be added.
	 */
	virtual bool GetNew() const { return m_bNew; }

	/**
	 * @brief Abstract virtual function for setting this object as new or not.
	 */
	virtual void SetNew(bool bNew) { m_bNew = bNew; }

	/**
	 * @brief Abstract virtual function for getting whether this object is to be removed.
	 */
	virtual bool GetRemoved() const { return m_bRemove; }

	/**
	 * @brief Abstract virtual function for setting this object as removed or not.
	 */
	virtual void SetRemoved(bool bRemoved) { m_bRemove = bRemoved; }


private:
	bool m_bNew;
	bool m_bRemove;
};

typedef std::vector<DynamicObjState*> DynObjList;


/**
 * @brief Contains the state of a physical entity in the game world.
 *
 * An entity is a dynamic object that may be updated by controller(s) in the control system,
 * for example a physics controller. Entities may also be damaged and destroyed, or set
 * as invulnerable. Generally an entity will also be "owned" in some capacity by some faction.
 *
 * @author Joe Forster
 */
class EntityState : public DynamicObjState
{
public:
	/**
	 * @brief Constructor for an entity state
	 *
	 * @param strType	   The type of entity (e.g. ship or projectile)
	 * @param strSubType	The sub-type of entity (e.g. ship type or weapon type)
	 * @param strFactionID  The ID of the faction that owns this entity
	 * @param vecPos		The starting position of the entity.
	 * @param qOrientation  The initial orientation of the entity.
	 */
	EntityState(
		String strType, string strSubType, String strFactionID,
		Vector3 vecPos, Quaternion qOrientation);

	/**
	 * @brief The copy constructor for an entity state.
	 */
	EntityState(const EntityState& rState);

	virtual ~EntityState() {}

	/*
	 * TODO: This whole EntityType thing is a bit of a hack. It would be better to alter the config system
	 * so that the EntityState superclass can handle getting the EntityType regardless of the type.
	 * (i.e. ShipsConfig and WeaponsConfig become subclasses of EntitiesConfig, and we can fetch an
	 * EntitiesConfig from GameConfig without referring to any BfP stuff)
	 */

	/**
	 * @brief Implement this to get the generic type configuration of this entity.
	 *
	 * This function isn't pure virtual because there are cases where we may want to copy/create
	 * generic EntityStates (e.g. for physical prediction). In those cases, we need to call
	 * SetEntityType after constructing so that we can call this.
	 */
	virtual const EntityType* GetEntityType() const;

	/**
	 * @brief Give this generic EntityState an EntityType to use.
	 *
	 * Throws an exception if this isn't a generic EntityState, or already has a type.
	 */
	void SetEntityType(const EntityType* pType);

	const String& GetSubType() const { return m_strSubType; }

	/**
	 * @brief Get the type of this entity's owner, if produced by another entity. Our own type if not.
	 *
	 * This is used for things like projectiles, in case we want to do collision
	 * detection while ignoring our own entities. Otherwise, just return our own type.
	 */
	virtual const String& GetOwnerType() const { return GetType(); }

	/**
	 * @brief Get the ID of this entity's owner, if produced by another entity. Our own ID if not.
	 *
	 * This is used for things like projectiles, in case we want to do collision
	 * detection while ignoring our own entities. Otherwise, just return our own ID.
	 */
	virtual unsigned GetOwnerID() const { return GetID(); }

	/**
	 * @brief Get the faction with which this entity is associated.
	 */
	virtual const String& GetFaction() const { return m_strFactionID; }

	/**
	 * @brief Get the number of hit points this entity has.
	 */
	virtual int GetHP() const { return m_iHP; }

	/**
	 * @brief Get whether this entity is "dead" - generally has 0 HP left.
	 */
	virtual bool IsDead() const { return !GetEntityType()->GetInvincible() && GetHP() <= 0; }

	/**
	 * @brief Get the orientation of this entity as a quaternion.
	 */
	const Quaternion& GetOrientation() const
	{
		return m_qOrientation;
	}

	void SetOrientation(const Quaternion& rQuat)
	{
	   m_qOrientation = rQuat;
	}

	void CombineOrientation(const Quaternion& rRot)
	{
		m_qOrientation = m_qOrientation * rRot;
	}

	Vector3& GetPosition()
	{
		return m_vecPos;
	}

	const Vector3& GetPosition() const
	{
		return m_vecPos;
	}

	void SetPosition(const Vector3& rVec)
	{
		m_vecPos = rVec;
	}

	Vector3 GetFaceDir() const
	{
		return GetOrientation() * Vector3::NEGATIVE_UNIT_X;
	}

	Vector3 GetFaceNorm() const
	{
		return GetOrientation() * Vector3::UNIT_Y;
	}

	Vector3 GetFaceRight() const
	{
		return GetOrientation() * Vector3::NEGATIVE_UNIT_Z;
	}

	const Vector3& GetVel() const { return m_vecVel; }
	const Vector3& GetAccel() const { return m_vecAccel; }


	void SetVel(const Vector3& rVec) { m_vecVel = rVec; }
	void SetAccel(const Vector3& rVec) { m_vecAccel = rVec; }

	/**
	 * @brief SetFaceDir uses a quaternion to rotate our orientation to the facing direction.
	 */
	void SetFaceDir(const Vector3& rVec);

	/**
	 * @brief Apply physical damage or healing to this entity.
	 *
	 * Note that, for things such as ships that have shields, this function should be overridden
	 * to handle that damage.
	 *
	 * @param iAmount   The amount of damage, in hit points, to apply (can be negative to heal)
	 */
	virtual void ApplyDamage(int iAmount);

	// Generic entity removal is through destruction
	virtual bool GetRemoved() const { return m_iHP <= 0 && m_iExplodeTimer <= 0; }
	virtual void SetRemoved(bool bRemoved) { SetDestroyed(); }

	/**
	 * @brief Activate the trigger for the explosion effect (and sets it to removed after).
	 */
	void SetDestroyed();

	// Collision stuff
	const Real& GetBoundingRadius() const { return m_fBoundingRadius; }
	const BoundingVolume* GetBoundingVolume() const { return m_pBV; }
	bool IsCollidable() const { return m_pBV || m_fBoundingRadius > 0; }

	bool CollidingWith(const EntityState* pEntity) const;
	bool CollidingWith(const BoundingVolume* pBV) const;
	bool CollidingWith(const Vector3& rPos, const Real& rRadius) const;

	void SetBoundingRadius(const Real& rRadius);
	/**
	 * @brief Set the bounding volume to a const pointer managed externally.
	 *
	 * Note that the BV needn't be in our position/orientation,
	 * and its memory should be managed by the caller.
	 */
	void SetBoundingVolume(const BoundingVolume* pBV);
	void UnsetBoundingVolume() { m_pBV = NULL; }

	/**
	 * @brief See whether the death explosion for this entity has been triggered.
	 */
	bool GetExplodeTrigger() const { return m_bTriggerExplode; }

	virtual void Update(unsigned long iTime);

protected:
	// We provide access to entity destruction-related stuff for subclasses
	// (e.g. ships that make use of shields for damage and warping for removal)
	long					m_iHP;
	long					m_iExplodeTimer;
	bool					m_bTriggerExplode;

private:
	// This is kept NULL unless we instantiated a generic EntityState and want it to have a type
	const EntityType*	   m_pGenericType;

	String				  m_strSubType;
	String				  m_strFactionID;

	Vector3				 m_vecPos;
	Quaternion			  m_qOrientation;

	// Snapshot of our motion state (from the physics engine)
	Vector3				 m_vecVel;
	Vector3				 m_vecAccel;

	// We keep a bounding volume in the state code for things like AI,
	// which need to check collisions without knowing the physics system.
	Real					m_fBoundingRadius;
	const BoundingVolume*   m_pBV;
};

#endif // GameState_H_INCLUDED
