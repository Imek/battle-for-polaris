#ifndef INTERSECTION_H
#define INTERSECTION_H

#include <vector>

#include <BfPEngineDefs.h>
#include <Util/Exception.h>

namespace JFPhysics
{

	/**
	 * @brief Simple Primitive class for a (bounding) quad.
	 *
	 * A quad primitive represented by four corners and a normal. We can translate or rotate it,
	 * and directly access the properties for intersection calculation.
	 *
	 * @author Joe Forster
	 */
	class Quad
	{
	public:
		Vector3 TL, TR, BR, BL, N; // Corners and normal

		/**
		 * @brief Blank constructor for a Quad
		 */
		Quad();

		/**
		 * @brief Construct a quad from its corners and normal
		 *
		 * @param vecTL Top left
		 * @param vecTR Top right
		 * @param vecBR Bottom right
		 * @param vecBL Bottom left
		 * @param vecN  Normal
		 *
		 */
		Quad(Vector3 vecTL, Vector3 vecTR, Vector3 vecBR, Vector3 vecBL, Vector3 vecN);

		/**
		 * @brief Translate all the vectors in this quad by a given vector.
		 */
		void Translate(const Vector3& rVec);

		/**
		 * @brief Rotate this quad by a given quaternion.
		 */
		void Rotate(const Quaternion& rQuat);

		/**
		 * @brief Rotate this quad around a given axis by a given angle.
		 */
		void Rotate(const Vector3& rAxis, Degree fAngle);

		/**
		 * @brief Get a copy of this quad rotated by the given quaternion
		 */
		Quad GetRotated(const Quaternion& rQuat) const;

		/**
		 * @brief Get a rotated copy of this quad by the given axis & angle.
		 */
		Quad GetRotated(const Vector3& rAxis, Degree fAngle) const;

	private:
		static Quaternion m_qRot; // Quaternion used for rotation
	};

	/**
	 * @brief Class containing data associated with the results of a detected collision.
	 *
	 * Contains information on a collision for collision resolution. This includes data for the
	 * resolution of velocity and penetration. The penetration is a vector representing how far
	 * the first object (parameter) provided has moved "inside" the second provided object.
	 *
	 * @author Joe Forster
	 */
	struct CollisionData
	{
		Vector3 m_vecPoint;
		Vector3 m_vecNormal;
		Vector3 m_vecPenetration;
	};


	enum BV_TYPE { BV_HIERARCHICAL, BV_AABB, BV_OBB, BV_SPHERE };

	/**
	 * @brief Abstract class to represent a bounding volume for collision detection.
	 *
	 * An abstract class for a bounding volume. Here we specify the basic operations a bounding
	 * volume of any kind needs to be able to perform.
	 *
	 * @author Joe Forster
	 */
	class BoundingVolume
	{
	private:
		BV_TYPE m_iType;

	public:
		BoundingVolume(BV_TYPE iType)
		: m_iType(iType) {}

		virtual ~BoundingVolume() {}

		/**
		 * @brief Copy constructor.
		 */
		BoundingVolume(const BoundingVolume& rVol)
		: m_iType(rVol.m_iType) {}

		BV_TYPE GetType() const { return m_iType; }

		/**
		 * @brief Get a vector representing the centre of this volume
		 */
		virtual const Vector3& GetCentre() const = 0;

		/**
		 * @brief Translate this bounding volume by a given vector.
		 */
		virtual void Translate(const Vector3& rVec) = 0;

		/**
		 * @brief Set the position of this bounding volume with a point vector.
		 */
		virtual void SetPosition(const Vector3& rPoint) = 0;

		// Rotate the bounding volume by a given vector (direction is axis, length is amount)
		//virtual void Rotate(const Vector3& rAmount) = 0;
		virtual void SetOrientation(const Quaternion& rQuat, const Vector3* pCentre = NULL) = 0;

		// A bounding volume should be able to check intersection with a point.
		virtual bool CheckPoint(const Vector3& rPoint) const = 0;

		// We should also be able to check any other type of bounding volume against this one,
		// returning a pointer to a CollisionData object.
		virtual CollisionData* CheckBoundingVolume(const BoundingVolume* pVolume) const = 0;
	};

	typedef std::vector<BoundingVolume*> BVVector;

	/**
	 * @brief A primitive representing a sphere.
	 *
	 * This type of primitive just contains a centre and a radius. Its general use is to represent
	 * bounding spheres of various world objects for collision detection.
	 *
	 * @author Joe Forster
	 */
	struct Sphere
	{
		Vector3		m_vecPos;
		Real		m_fRadius;

		// Blank constructor
		Sphere() : m_fRadius(0.0f) {}
		// Constructor from values
		Sphere(Vector3 vecPos, Real fRadius)
		: m_vecPos(vecPos), m_fRadius(fRadius) {}

	};

	/**
	 * @brief A sphere primitive used as a bounding volume
	 *
	 * A simple wrapper for our Sphere primitive class to represent it as a
	 * bounding volume to be checked against.
	 *
	 * @author Joe Forster
	 */
	class BoundingSphere : public BoundingVolume
	{
	private:
		Sphere m_oVol;

	public:
		/**
		 * @brief Construct a bounding sphere with a position and a radius.
		 */
		BoundingSphere(Vector3 vecPos, Real fRadius);

		/**
		 * @brief Copy a bounding sphere.
		 */
		BoundingSphere(const BoundingSphere& rBV);

		virtual ~BoundingSphere() {}

		const Vector3& GetCentre() const;

		/**
		 * @brief Provide access to the sphere volume object.
		 */
		const Sphere& GetSphere() const;

		/**
		 * @brief Translate the position of the sphere by a vector.
		 */
		void Translate(const Vector3& rVec);

		/**
		 * @brief Set the position of the sphere to a vector.
		 */
		void SetPosition(const Vector3& rPoint);

		/**
		 * @brief Set the orientation of the sphere around a vector.
		 */
		void SetOrientation(const Quaternion& rQuat, const Vector3* pCentre = NULL);

		/**
		 * @brief A bounding volume should be able to check intersection with a point.
		 */
		bool CheckPoint(const Vector3& rVec) const;

		/**
		 * @brief Check this sphere against any given bounding volume.
		 */
		CollisionData* CheckBoundingVolume(const BoundingVolume* pVolume) const;
	};

	/**
	 * @brief A subtype of BoundingVolume for the box types.
	 *
	 * This class represents both oriented and axis-aligned bounding boxes, as there are some common
	 * features of the two. In particular, we keep track of the corners of both types of bounding
	 * box.
	 *
	 * @author Joe Forster
	 */
	class BoundingBox : public BoundingVolume
	{
	public:
		// An enumeration of each possible side, which corresponds to an index in the array.
		enum BOX_SIDE { TOP=0, BOTTOM, LEFT, RIGHT, FRONT, BACK, LAST };

		/**
		 * @brief Called by a subclass to make a blank bounding box with the given type.
		 */
		BoundingBox(BV_TYPE iType) : BoundingVolume(iType) {}

		/**
		 * @brief Copy constructor.
		 */
		BoundingBox(const BoundingBox& rBV);

		virtual ~BoundingBox() {}

		/**
		 * @brief Get the centre vector for the box.
		 */
		const Vector3& GetCentre() const { return m_vecCentre; }

		// Get the vectors for each corner
		const Vector3& GetFBL() const { return m_vecFBL; }
		const Vector3& GetFTL() const { return m_vecFTL; }
		const Vector3& GetFTR() const { return m_vecFTR; }
		const Vector3& GetFBR() const { return m_vecFBR; }
		const Vector3& GetBBL() const { return m_vecBBL; }
		const Vector3& GetBTL() const { return m_vecBTL; }
		const Vector3& GetBTR() const { return m_vecBTR; }
		const Vector3& GetBBR() const { return m_vecBBR; }

		virtual Plane* GetPlaneWithNormal(Vector3 vecNormal) const = 0;
		virtual Plane GetPlane(BOX_SIDE iSide) const = 0;

	protected:
		// The centre and all the corners of the bounding box, for convenience.
		Vector3 m_vecCentre;

		Vector3 m_vecFBL;
		Vector3 m_vecFTL;
		Vector3 m_vecFTR;
		Vector3 m_vecFBR;
		Vector3 m_vecBBL;
		Vector3 m_vecBTL;
		Vector3 m_vecBTR;
		Vector3 m_vecBBR;
	};

	/**
	 * @brief An orientable box used as a bounding volume
	 *
	 * Represents a bounding box of arbitrary orientation. Detection is done by checking
	 * against six planes. Should use an AABB where possible (see below), as they are more efficient.
	 * Note that we could add a constructor to make the bounding box from eight points as well, but
	 * this isn't needed (yet).
	 * Note also that we can make a box from min/max and orientate it any way we want by rotating.
	 *
	 * @author Joe Forster
	 */
	class OrientedBoundingBox : public BoundingBox
	{
	public:
		/**
		 * @brief Create a bounding box from minimum and maximum points (i.e. an orientable AABB)
		 */
		OrientedBoundingBox(Vector3 vecMin, Vector3 vecMax);

		/**
		 * @brief Copy constructor
		 */
		OrientedBoundingBox(const OrientedBoundingBox& rBV);

		virtual ~OrientedBoundingBox();

		/**
		 * @brief Translate the bounding box by a vector.
		 *
		 * Translating the bounding box is done by translating the centre and the point of each plane.
		 */
		void Translate(const Vector3& rVec);

		/**
		 * @brief Set the centre position of this box, translating the planes as well.
		 */
		void SetPosition(const Vector3& rPoint);

		/**
		 * @brief Set the orientation of the box to a new quaternion.
		 */
		void SetOrientation(const Quaternion& rQuat, const Vector3* pCentre);

		/**
		 * @brief Check whether a point rVec is within this box.
		 *
		 * To check a point against a bounding box, check it's on the negative
		 * (i.e. opposite the normal) side of each plane.
		 */
		bool CheckPoint(const Vector3& rVec) const;

		/**
		 * @brief Check this box against some other bounding volume
		 *
		 * @return Pointer to a CollisionData object if we collided, otherwise NULL. We should also be able to check any other type of bounding volume against this one,
		 */
		CollisionData* CheckBoundingVolume(const BoundingVolume* pVolume) const;

		/**
		 * @brief Get the plane with the given normal, if any.
		 */
		Plane* GetPlaneWithNormal(Vector3 vecNormal) const;

		/**
		 * @brief Get the plane of the given side.
		 */
		Plane GetPlane(BOX_SIDE iSide) const;

	private:
		Plane*		m_arrPlanes; // Array of six planes

		Quaternion m_qOrientation;

		// Rotate the bounding box by rotating each point and normal about the centre.
		void Rotate(const Quaternion& rQuat);
	};

	/**
	 * @brief A non-rotatable bounding box aligned with the X/Y/Z axes.
	 *
	 * This class represents a box aligned by the X, Y and Z axes, so that it can be specified
	 * by simply giving a minimum and a maximum point. These are useful for simple collision
	 * detection, as they are simple to implement and more efficient to check against than
	 * arbitrarily-aligned bounding boxes.
	 */
	class AxisAlignedBoundingBox : public BoundingBox
	{
	public:
		/**
		 * @brief Construct an AABB from minimum and maximum points.
		 */
		AxisAlignedBoundingBox(Vector3 vecMin, Vector3 vecMax);

		/**
		 * @brief Copy an AABB.
		 */
		AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rBV);

		virtual ~AxisAlignedBoundingBox() {}

		/**
		 * @brief Translate this bounding box by a given vector.
		 */
		void Translate(const Vector3& rVec);

		/**
		 * @brief Set the centre position of this box.
		 */
		void SetPosition(const Vector3& rPoint);

		// Rotate does nothing for AABB.
		//void Rotate(const Vector3& rAmount) {}

		/**
		 * @brief Set the orientation of this AABB, around the given centre point.
		 */
		void SetOrientation(const Quaternion& rQuat, const Vector3* pCentre);

		/**
		 * @brief Check if the given point lies within this box.
		 */
		bool CheckPoint(const Vector3& rVec) const;

		/**
		 * @brief Check this AABB against any given bounding volume.
		 */
		CollisionData* CheckBoundingVolume(const BoundingVolume* pVolume) const;

		// Inherited methods for getting planes
		Plane* GetPlaneWithNormal(Vector3 vecNormal) const;
		Plane GetPlane(BOX_SIDE iSide) const;

		const Vector3& GetCentre() const { return m_vecCentre; }
		const Vector3& GetMaxFromCentre() const { return m_vecMaxFromCentre; }
		const Vector3& GetMax() const { return m_vecMax; }
		const Vector3& GetMin() const { return m_vecMin; }

	protected:
		// Minimum and maximum points for this box.
		Vector3 m_vecMin;
		Vector3 m_vecMax;
		// Calculated maximum amount from centre of the box.
		Vector3 m_vecMaxFromCentre;

		/**
		 * @brief Check if a given AABB is overlapping this box.
		 */
		CollisionData* CheckAABB(const AxisAlignedBoundingBox& rBox) const;

	};


	/**
	 * @brief A hierarchical bounding volume class.
	 *
	 * A hierarchical bounding volume. This represents a tree structure of bounding
	 * volumes that we can check against as a composite object, allowing us to represent a complex
	 * object with numerous simple shapes.
	 *
	 * @author Joe Forster
	 */
	class HierarchicalBV : public BoundingVolume
	{
	private:
		BVVector m_vChildren;
		BoundingVolume* m_pVolume;

	public:
		/**
		 * @brief Create a node in an HBV tree with the given non-hierarchical volume attached
		 */
		HierarchicalBV(BoundingVolume* pVolume);

		/**
		 * @brief Copy constructor recursively copies the subtree.
		 */
		HierarchicalBV(const HierarchicalBV& rBV);

		/**
		 * @brief Destructor recurses down the tree, destroying all children.
		 */
		virtual ~HierarchicalBV();

		/**
		 * @brief Get the volume attached to this node.
		 */
		const BoundingVolume* GetNodeVolume() const;

		/**
		 * @brief Get all children of this node.
		 */
		const BVVector& GetChildren() const;

		/**
		 * @brief Get the centre position of this volume.
		 */
		const Vector3& GetCentre() const;

		/**
		 * @brief Add a child (either hierarchical or regular BV)
		 */
		void AddChild(BoundingVolume* pVol);

		// All of these are done by passing down the command recursively.
		void Translate(const Vector3& rVec);
		void SetPosition(const Vector3& rPoint);
		//void Rotate(const Vector3& rAmount);
		void SetOrientation(const Quaternion& rAmount, const Vector3* pCentre);
		bool CheckPoint(const Vector3& rPoint) const;
		CollisionData* CheckBoundingVolume(const BoundingVolume* pVolume) const;
	};

	/**
	 * @brief A factory class for constructing new bounding volumes of any type.
	 *
	 * @author Joe Forster
	 */
	class BVFactory
	{
	private:
		static BVFactory* m_pInstance;

		BVFactory() {}

	public:
		static const BVFactory& Get();

		BoundingVolume* MakeCopy(const BoundingVolume* pVol) const;

	};


	/**
	 * @brief A struct containing some static functions with code for checking intersections.
	 *
	 * Static code for fine-grain collision detection between various basic primitives.
	 * We return either a pointer to a ContactData object if a collision was detected, or a null
	 * pointer if not.
	 *
	 * @author Joe Forster
	 */
	struct Intersection
	{
		/**
		 * @brief Detect collisions between two spheres.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* SphereAndSphere(const Sphere& rOne, const Sphere& rTwo);

		/**
		 * @brief Detect collisions between a sphere and a plane.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* SphereAndPlane(const Sphere& rSphere, const Plane& rPlane);

		/**
		 * @brief Detect collisions between a sphere and a quad.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* SphereAndQuad(const Sphere& rSphere, const Quad& rQuad);

		/**
		 * @brief Detect collisions between a sphere and a box.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* SphereAndBox(const Sphere& rSphere, const BoundingBox* pBox);

		/**
		 * @brief Detect collisions between any box and an oriented box.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* BoxAndOBB(const BoundingBox* pBox, const OrientedBoundingBox& rOBB);

		/**
		 * @brief	Check a line against an OBB - used by the above BoxAndOBB function.
		 *
		 * Given a line (two vectors) and an oriented bounding box, determine if the line is
		 * intersecting with the box.
		 *
		 * @param	rLS			The point at the start of the line.
		 * @param	rLE			The point at the end of the line.
		 * @param	rOBB		The oriented bounding box to check against.
		 * @param	pHitFace	A pointer to which we assign the index of the face on the box that the
		 *						line intersects with, in the case that an intersection is found.
		 *
		 * @return	Either the point of contact (mid-point inside the box) or NULL.
		 */
		static Vector3* LineAndOBB(const Vector3& rLS, const Vector3& rLE,
			const OrientedBoundingBox& rOBB, int* pHitFace);

		/**
		 * @brief Detect collisions between an AABB and a Quad.
		 *
		 * @return a CollisionData object if a collision was detected; NULL otherwise.
		 */
		static CollisionData* AABBAndQuad(const AxisAlignedBoundingBox& rBox, const Quad& rQuad);

		/**
		 * @brief Invert a CollisionData object ("A collided with B" becomes "B collided with A")
		 *
		 * @param pData a pointer to the collision data to flip
		 */
		static void FlipCollision(CollisionData* pData);
	};
}

#endif /* INTERSECTION_H */
