#ifndef COLLISIONRESOLVE_H
#define COLLISIONRESOLVE_H

#include <BfPEngineDefs.h>
#include <Physics/Particle.h>
#include <Physics/Intersection.h>

using namespace std;

namespace JFPhysics
{
	/**
	 * @brief Collision resolution class between two particles.
	 *
	 * A collision involving one particle and possibly another. Calculates the resulting velocity
	 * of each particle; also resolves penetration.
	 *
	 * @author Joe Forster
	 */
	class ParticleCollision
	{
	public:
		Particle*		m_pOne;
		Particle*		m_pTwo;
		Real			m_fRestitution;
		Vector3			m_vecContNormal;
		Vector3			m_vecPenetration;

		ParticleCollision(Particle* pOne, Particle* pTwo, Real fRestitution, const CollisionData& rData);

		/**
		 * @brief Resolve the collision, altering the attributes of the particle(s) accordingly.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void Resolve(unsigned long iTime);

	protected:

		/**
		 * @brief Calculate the separating velocity for this collision.
		 */
		Real CalcSeparatingVelocity() const;

		/**
		 * @brief Resolve the velocity of the collision, affecting particle attributes.
		 *
		 * Calculate a new velocity for the particle(s) given the attributes of this collision.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void ResolveVelocity(unsigned long iTime);

		/**
		 * @brief Resolve any penetration given in the collision data.
		 *
		 * Resolve penetration: the CollisionData object contains a vector describing how far the
		 * first object is penetrating into the second; we need to alter its position to avoid this.
		 * For now, it should be sufficient just to move the first particle all the way.
		 *
		 * @param	iTime	The amount of time since the last update.
		 */
		void ResolvePenetration(unsigned long iTime);

	};

	/**
	 * @brief Collision resolution class between rigid bodies.
	 *
	 * This class handles and resolves collisions between two rigid bodies, applying the appropriate
	 * forces that result. This works in much the same way as a particle collision for linear
	 * values, but needs to handle torque as well.
	 *
	 * @author Joe Forster
	 */
	//class RigidBodyCollision : public ParticleCollision
	//{
	//	// TODO: Implement proper rigid body collisions (low priority, use ParticleCollision for now)
	//};

}


#endif /* COLLISIONRESOLVE_H */
