#ifndef PARTICLE_H
#define PARTICLE_H

#include <vector>

#include <BfPEngineDefs.h>

using namespace std;

// TODO: Finish the namespace system and move all generalisable Physics stuff from BfPGame

namespace JFPhysics
{

	/**
	 * @brief A physics class for simulating the motion of a point mass.
	 *
	 * Particle class: this class represents a single point mass being physically simulated in the
	 * game world. It is kept separate from the game itself or any notion of rendering anything on the
	 * screen; the intention is for any code that uses this class to handle all that stuff. This code
	 * is largely inspired by the class of the same name in Cyclone, though some extra behaviour has
	 * been made to handle problems with collisions.
	 *
	 * @author Joe Forster
	 */
	class Particle
	{
	public:
		/**
		 * @brief Constructor from provided parameters.
		 */
		Particle(Vector3 vecPos, Vector3 vecVel, Vector3 vecAccel,
				 Real fInvMass, Real fDamping, Real fMaxSpeed,
				 Real fImmovableMass);

		/**
		 * @brief Blank destructor: no memory managed.
		 */
		virtual ~Particle() {}



		/**
		 * @brief Add a force to the accumulator for this update cycle.
		 *
		 * @param	rForce	the force to add.
		 */
		void AddForce(const Vector3& rForce);

		/**
		 * @brief Zero the force accumulator for this update cycle.
		 */
		void ZeroForces();

		/**
		 * @brief Add a contact normal to the set of surfaces the object is stuck to (landed on).
		 *
		 * @param	rNormal		The surface normal being contacted with.
		 */
		void AddContactNormal(const Vector3& rNormal);

		Real GetInvMass() const;

		/**
		 * @brief Get the mass of the particle.
		 *
		 * @return	Either a Real value for mass, or -1.0f if mass is infinite.
		 */
		Real GetMass() const;

		void SetMass(Real fVal);

		// Getters and setters for the attributes
		const Vector3& GetPos() const { return m_vecPos; }
		const Vector3& GetVel() const { return m_vecVel; }
		const Vector3& GetAccel() const { return m_vecAccel; }
		const Real&	GetDamping() const { return m_fDamping; }
		const Real&	GetMaxSpeed() const { return m_fMaxSpeed; }

		void SetPos(const Vector3& rVec) { m_vecPos = rVec; }
		void SetVel(const Vector3& rVec) { m_vecVel = rVec; }
		void SetAccel(const Vector3& rVec) { m_vecAccel = rVec; }
		void SetDamping(const Real& rVal) { m_fDamping = rVal; }
		void SetMaxSpeed(const Real& rVal) { m_fMaxSpeed = rVal; }

		/**
		  * @brief The Update function integrates particle motion.
		  *
		  * Updates the new position and motion values of the particle, given iTime milliseconds
		  * have passed.
		  *
		  * @param	iTime	Number of times passed since the last update.
		  */
		void Update(unsigned long iTime);

	protected:
		// Attributes of this particle.
		Vector3				 m_vecPos;
		Vector3					m_vecVel;
		Vector3					m_vecAccel;
		Real					m_fInvMass;
		Real					m_fDamping;
		Real					m_fMaxSpeed;
		// List used for handling repeated collisions
		std::vector<Vector3>			m_vContactNormals;

		// Accumulated force since the last update.
		Vector3				 m_vecForceAccum;

		// Settings
		Real					m_fImmovableMass;


	};

}

#endif /* PARTICLE_H */
