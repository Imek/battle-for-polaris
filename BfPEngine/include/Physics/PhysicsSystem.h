/**
 * PhysicsSystem.h - the physics simulation system for the game engine.
 *
 * Author: Joe Forster
 */

#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <algorithm>

#include <BfPEngineDefs.h>
#include <Ogre/OgreFramework.h>
#include <Physics/Intersection.h>
#include <Physics/Particle.h>
#include <Physics/RigidBody.h>


#include <State/SectorState.h>

using namespace std;

namespace JFPhysics
{

	/**
	 * @brief A class to represent an external entity with a particle/body in the physics module.
	 *
	 * This abstract class describes something that will keep an EntityState in the game engine
	 * synchronised with its simulated particle/body in the physics system.
	 *
	 * @author Joe Forster
	 */
	class EntityPhysics
	{
	public:
		/**
		 * @brief Construct an EntityPhysics object given a particle and an entity.
		 *
		 * @author pObject	  The particle/body to which to connect our state.
		 * @author pWorld	   The world (sector) state with our entity.
		 * @author rEntityType  The type of entity we're linking to.
		 * @author iEntityID	The ID of our entity.
		 * @author pBV		  A bounding volume to use.
		 */
		EntityPhysics(Particle* pObject,
			SectorState* pWorld, const String& rEntityType, unsigned iEntityID,
			const BoundingVolume* pBV = NULL);

		virtual ~EntityPhysics();

		Particle* GetObject() { return m_pObject; }

		String GetType() const { return m_strEntityType; }

		/**
		 * @brief Get a pointer to the state of the entity here.
		 */
		EntityState* GetState();

		unsigned GetEntityID() const { return m_iEntityID; }

		// Update function needs to be implemented (integrates m_pObject then updates external data)
		virtual void Update(unsigned long iTime) = 0;

		/**
		 * @brief Check collisions against this object with another object.
		 *
		 * @param	pBV		The other object's BoundingVolume to check against.
		 *
		 * @return	Either the collision data object of the resultant collision, or NULL if none.
		 */
		CollisionData* CollidingWith(const BoundingVolume* pBV);

		/**
		 * @brief Check collisions with another physics object.
		 */
		CollisionData* CollidingWith(const EntityPhysics* pObj);

	protected:
		// The object being controlled/updated (memory is also managed here!)
		Particle*		   m_pObject;
		// We get the state being controlled through the sector state using a type and ID.
		SectorState*		m_pWorld;
		String			  m_strEntityType;
		unsigned			m_iEntityID;
		// A bounding volume of any kind, or NULL if collisions are disabled for this object.
		BoundingVolume*	 m_pBV;

	};

	/**
	 * @brief An abstract class for any force that exerts a force on particles.
	 *
	 * Any class that extends this one represents some force in the world, possibly acting on
	 * one or more particles. These are added to a force manager class, where they become part
	 * of the main update cycle.
	 *
	 * @author Joe Forster
	 */
	struct Force
	{
		virtual void Update(Particle *pParticle, unsigned long iTime) = 0;

		virtual ~Force() {}
	};


	/**
	 * @brief A class for managing all the forces to be simulated on particles in the game world.
	 *
	 * Holds all the forces currently acting on some particle in the game world.
	 * Note that we don't manage any memory here; we just hold the pointers.
	 *
	 * @author Joe Forster
	 */
	class ParticleForceManager
	{
	protected:
		/**
		 * @brief A struct for a pairing of a particle with a force being applied to it.
		 *
		 * This class is used for entries in the particle force manager's m_vParticleForces
		 * member vector.
		 */
		struct ParticleForce
		{
			Particle* m_pParticle;
			Force* m_pForce;

			ParticleForce() : m_pParticle(NULL), m_pForce(NULL) {}

			ParticleForce(Particle* pParticle, Force* pForce)
			: m_pParticle(pParticle), m_pForce(pForce) {}
			virtual ~ParticleForce() { }
		};
		std::vector<ParticleForce> m_vParticleForces;

	public:
		/**
		 * @brief Add a particle/force pairing to this force manager.
		 *
		 * The force will be applied when Update is called. Note that this memory isn't managed here.
		 */
		void Add(Particle* pParticle, Force *pForce);

		/**
		 * @brief Remove a particle/force pairing from the manager.
		 */
		void Remove(Particle* pParticle, Force *pForce);

		/**
		 * @brief Remove every particle/force pairing in this instance.
		 */
		void Clear() { m_vParticleForces.clear(); }

		/**
		 * @brief Update all the forces managed on their corresponding particles.
		 */
		void Update(unsigned long iTime);

		/**
		 * @brief Get a string representation of the force/particle pairing set for debugging.
		 */
		string GetString() const;

	};


	/**
	 * @brief A force type associated with a particular entity (ship/projectile) in the game.
	 *
	 * @author Joe Forster
	 */
	class EntityForce : public Force
	{
	public:
		EntityForce(SectorState* pWorld, String strType, unsigned iID)
		: m_pWorld(pWorld), m_strType(strType), m_iID(iID) {}

		virtual ~EntityForce() {}

		bool EntityEquals(String strType, unsigned iID) const
		{
			return (m_strType == strType && m_iID == iID);
		}


		const SectorState* GetSector() const { return m_pWorld; }
		String GetType() const { return m_strType; }
		unsigned GetID() const { return m_iID; }
		void SetSector(SectorState* pNewState)
		{
			assert(pNewState);
			m_pWorld = pNewState;
		}

		virtual void Update(Particle* pParticle, unsigned long iTime) = 0;

	protected:
		SectorState*	m_pWorld;
		String		  m_strType;
		unsigned		m_iID;

		ObjState* GetObjState()
		{
			assert( m_pWorld->GetObjState(m_strType, m_iID) );
			return (ObjState*)m_pWorld->GetObjState(m_strType, m_iID);
		}

	};

}


#endif /* PHYSICS_H */
