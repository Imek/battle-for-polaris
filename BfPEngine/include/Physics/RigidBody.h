#ifndef RIGIDBODY_H_INCLUDED
#define RIGIDBODY_H_INCLUDED

#include <Physics/Particle.h>

namespace JFPhysics
{
	/**
	 * @brief A physics class for simulating a rigid body, including angular motion.
	 *
	 * The rigid body adds on top of the particle class by simulating an object that is
	 * more complex than a particle. Rigid bodies can handle angular motion brought on
	 * by torque on top of linear motion.
	 *
	 * @author Joe Forster
	 */
	class RigidBody : public Particle
	{
	public:
		/**
		 * @brief Constructor for a RigidBody from particle attributes, an inertia tensor and an
		 * angular damping value.
		 */
		RigidBody(Vector3 vecPos, Vector3 vecVel, Vector3 vecAccel,
				 Real fInvMass, Real fDamping, Real fMaxSpeed,
				 Matrix3 mxInertia, Real fAngularDamping, Real fImmovableMass);

		/**
		 * @brief Get a reference to the orientation quaternion.
		 */
		Quaternion& GetOrientation() { return m_qOrientation; }

		/**
		 * @brief Get a reference to the angular velocity value.
		 */
		Vector3& GetAngularVelocity() { return m_vecAngularVel; }

		/**
		 * @brief Set the angular velocity to a new value.
		 */
		void SetAngularVelocity(const Vector3& rVec) { m_vecAngularVel = rVec; }

		/**
		 * @brief Get the angular damping value for the object.
		 */
		Real GetAngularDamping() { return m_fAngularDamping; }

		/**
		 * @brief Set the orientation of the body to the values in a new quaternion.
		 *
		 * @param	rQuat	A const reference to the quaternion to copy values from.
		 */
		void SetOrientation(const Quaternion& rQuat) { m_qOrientation = rQuat; }

		/**
		 * @brief Apply torque by providing a point in world coordinates to apply at.
		 */
		void AddForceAtPoint(const Vector3& rForce, const Vector3& rPoint);

		/**
		 * @brief Clear both (linear and torque) force accumulators.
		 */
		void ClearAccumulators()
		{
			m_vecForceAccum = Vector3::ZERO;
			m_vecTorqueAccum = Vector3::ZERO;
		}


		/**
		 * @brief Function for updating the internal state when accessible values are changed
		 *
		 * This function needs to be called whenever some attribute of the body is altered, though
		 * it is called automatically in the Update function.
		 */
		void UpdateDerivedData();

		/**
		 * @brief Overrided update function that integrates the motion of the body.
		 */
		void Update(unsigned long iTime);

	private:
		// Torque/inertia stuff
		Vector3	 m_vecTorqueAccum;
		Matrix3	 m_mxInvInertia; // Inverse of given inertia tensor
		Matrix3	 m_mxInvInertiaWorld; // The above tensor in world coords
		// Angular stuff
		Quaternion  m_qOrientation;
		Vector3	 m_vecAngularVel; // Direction is axis, magnitude is rate.
		Vector3	 m_vecAngularAccel;
		Real		m_fAngularDamping;
		// A matrix for transforming between local (body) space and world space, needed for
		// some of our calculations.
		Matrix4	 m_mxTransform;

		// We also need to remember the acceleration from the last frame.
		Vector3	 m_vecLastFrameAccel;

		/**
		 * @brief Internal function to do an intertia tensor transform by a quaternion.
		 *
		 * This function is copied from the Cyclone engine; see the Copyright notice at the top
		 * of the source file. Note that the implementation of this function was created by an
		 * automated code-generator and optimizer.
		 */
		static inline void TransformInertiaTensor(Matrix3 &iitWorld,
												  const Quaternion &q,
												  const Matrix3 &iitBody,
												  const Matrix4 &rotmat);

		/**
		 * @brief Inline function that creates a transform matrix from a position and orientation.
		 *
		 * This function is copied from the Cyclone engine; see the Copyright notice at the top
		 * of the source file.
		 */
		static inline void CalculateTransformMatrix(Matrix4 &transformMatrix,
													const Vector3 &position,
													const Quaternion &orientation);
	};
}

#endif // RIGIDBODY_H_INCLUDED
