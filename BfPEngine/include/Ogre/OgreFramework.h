#ifndef GAMEFRAMEWORK_H_INCLUDED
#define GAMEFRAMEWORK_H_INCLUDED

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreControllerManager.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <Ogre/OgreLoadingBar.h>

#include <Util/Singleton.h>

/**
 * @brief A framework class to act as an intermediary between our engine and OGRE.
 */
class OgreFramework : public JFUtil::Singleton<OgreFramework>
{
public:
	OgreFramework();
	~OgreFramework();

	void initOgre(Ogre::String wndTitle);

	void updateOgre(unsigned long iTime);

	bool TakeScreenShot();

	bool TogglePolyMode();

	bool ToggleOverlayMode();

	//void moveCamera();
	//void getInput();

	bool isOgreToBeShutDown() const { return m_bShutDownOgre; }

	// Get methods
	Ogre::Root* GetOgreRoot() { return m_pRoot; }
	Ogre::SceneManager* GetSceneManager() { return m_pSceneMgr; }
	Ogre::RenderWindow* GetRenderWindow() { return m_pRenderWnd; }
	Ogre::Camera* GetCamera() { return m_pCamera; }
	Ogre::Viewport* GetViewport() { return m_pViewport; }
	Ogre::Log* GetLog() { return m_pLog; }
	Ogre::Timer* GetTimer() { return m_pTimer; }

private:
	OgreFramework(const OgreFramework&);
	OgreFramework& operator= (const OgreFramework&);

	Ogre::Root*					m_pRoot;
	Ogre::SceneManager*			m_pSceneMgr;
	Ogre::RenderWindow*			m_pRenderWnd;
	Ogre::Camera*				m_pCamera;
	Ogre::Viewport*				m_pViewport;
	Ogre::Log*					m_pLog;
	Ogre::Timer*				m_pTimer;

	Ogre::Overlay*				m_pDebugOverlay;
	Ogre::Overlay*				m_pInfoOverlay;
	int							m_iNumScreenShots;
	LoadingBar				  m_oLoadingBar;

	bool						m_bShutDownOgre;

	void updateStats();

};

#endif // GAMEFRAMEWORK_H_INCLUDED
