#ifndef BFPENGINEDEFS_H_INCLUDED
#define BFPENGINEDEFS_H_INCLUDED

#include <OgrePlane.h>
#include <OgreString.h>
#include <OgreMatrix3.h>
#include <OgreMatrix4.h>
#include <OgreVector2.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>
#include <OgreStringVector.h>
#include <OgreException.h>

// TODO: We want to gradually phase out the use of these Ogre classes in favour
// of custom-written classes.

// Right now we are just doing "using" directives for various utility
// classes from Ogre.
using Ogre::StringVector;
using Ogre::Quaternion;
using Ogre::Vector2;
using Ogre::Vector3;
using Ogre::Vector4;
using Ogre::Matrix3;
using Ogre::Matrix4;
using Ogre::String;
using Ogre::Degree;
using Ogre::Radian;
using Ogre::Plane;
using Ogre::Real;
using Ogre::Math;

#endif // BFPENGINEDEFS_H_INCLUDED
