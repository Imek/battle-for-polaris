#include <Game/GameScene.h>

/**
 * GameScene static code
 */

void GameScene::DestroyAttachedObjects(Ogre::SceneNode* pNode)
{
	Ogre::SceneManager* pSceneMgr = OgreFramework::GetInstance().GetSceneManager();

	while (pNode->numAttachedObjects() > 0)
	{
		Ogre::MovableObject* pObj = pNode->detachObject((unsigned short)0);
		// NOTE: Dirty hack - because the MovableText library I got doesn't register a movable object factory with ogre, I have to check for its type and delete it manually.
		if (pObj->getMovableType() == "MovableText") delete pObj;
		else pSceneMgr->destroyMovableObject(pObj);
	}
}

/**
 * GameScene code
 */

GameScene::GameScene(GAME_SCENE_TYPE iType, String strName)
: m_pSceneNode(NULL)
, m_pOgre(OgreFramework::GetInstancePtr())
, m_pSceneMgr(m_pOgre->GetSceneManager())
, m_iType(iType)
, m_strName(strName)
, m_bAttached(false)
{
	m_pSceneNode = m_pSceneMgr->createSceneNode(strName);
}

GameScene::~GameScene()
{
	if (m_bAttached) Detach();
	m_pSceneMgr->destroySceneNode(m_strName);
}

void GameScene::Attach()
{
	if (m_bAttached)
	{
		throw JFUtil::Exception("GameScene", "Attach",
			"Called when m_bAttached was true");
	}
	assert(m_pSceneNode);

	Ogre::SceneManager* pSceneMgr =
		OgreFramework::GetInstance().GetSceneManager();
	pSceneMgr->getRootSceneNode()->addChild(m_pSceneNode);

	m_bAttached = true;
}

void GameScene::Detach()
{
	if (!m_bAttached)
	{
		throw JFUtil::Exception("GameScene", "Detach",
			"Called when m_bAttached was false");
	}

	Ogre::SceneManager* pSceneMgr =
		OgreFramework::GetInstance().GetSceneManager();
	pSceneMgr->getRootSceneNode()->removeChild(m_strName);

	m_bAttached = false;
}
