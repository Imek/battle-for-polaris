#include <Game/GameConfig.h>

// Singleton instance of GameConfig
template<> GameConfig* JFUtil::Singleton<GameConfig>::s_instance = NULL;

const String GameConfig::CFG_PATH = "data/config/";

/**
 * BaseConfig code
 */

BaseConfig::BaseConfig(const String& rPath)
: m_strPath(GameConfig::CFG_PATH+rPath)
{
	m_oConfig.load(m_strPath);
}

String* BaseConfig::GetString(String strKey, String strSection) const
{
	assert(!strKey.empty());
	String strValue;
	// Could return blank (not found)
	if (strSection.empty())
		strValue = m_oConfig.getSetting(strKey);
	else
		strValue = m_oConfig.getSetting(strKey, strSection);

	if (strValue.empty())
		return NULL;
	else
		return new String(strValue);
}

/**
 * ObjectType code
 */

ObjectType::ObjectType(String strName, const BaseConfig* pConfig)
: m_strName(strName)
{
	assert(!m_strName.empty());
	assert(pConfig);

	String* pReadable = pConfig->GetString(strName + "ReadableName");
	m_strReadableName = pReadable ? *pReadable : "NONAME"; // A default doesn't exist for now
	delete pReadable;
}

/**
 * EntityType code
 */
EntityType::EntityType(String strName, const BaseConfig* pConfig)
: ObjectType(strName, pConfig)
{
	// Basic bounding sphere has a radius; defaults to 0, which means the object doesn't collide
	Real* pBoundingRadius = pConfig->GetValue<Real>(GetName() + "BoundingRadius");
	m_fBoundingRadius = pBoundingRadius ? *pBoundingRadius : 0;
	delete pBoundingRadius;
	assert(m_fBoundingRadius >= 0);

	m_pBV = new BoundingSphere(Vector3::ZERO, m_fBoundingRadius);

	// Physical properties
	Real* pMass = pConfig->GetValue<Real>(GetName() + "Mass");
	m_fMass = pMass ? *pMass : 1;
	delete pMass;
	assert(m_fMass > 0);

	Real* pDamping = pConfig->GetValue<Real>(GetName() + "Damping");
	m_fDamping = pDamping ? *pDamping : 1; // 1 = no damping effect
	delete pDamping;

	Real* pMaxSpeed = pConfig->GetValue<Real>(GetName() + "MaxSpeed");
	m_fMaxSpeed = pMaxSpeed ? *pMaxSpeed : 0.01;
	delete pMaxSpeed;

	Real* pMaxThrust = pConfig->GetValue<Real>(GetName() + "MaxThrust");
	m_fMaxThrust = pMaxThrust ? *pMaxThrust : 0;
	delete pMaxThrust;

	Real* pAngularDamping = pConfig->GetValue<Real>(GetName() + "AngularDamping");
	m_fAngularDamping = pAngularDamping ? *pAngularDamping : 1; // 1 = no damping effect
	delete pAngularDamping;

	Real fRadius = ((BoundingSphere*)m_pBV)->GetSphere().m_fRadius;
	Real fTwoFifthsMRSquared = ((Real)2/(Real)5) * m_fMass * fRadius * fRadius;
	// TODO: Inertia tensors - static value chosen here with a name in the config (use this basic sphere one for now)
	m_mxInertiaTensor = Matrix3 (
		fTwoFifthsMRSquared, 0, 0,
		0, fTwoFifthsMRSquared, 0,
		0, 0, fTwoFifthsMRSquared);

	bool* pInvincible = pConfig->GetValue<bool>(GetName() + "Invincible");
	m_bInvincible = pInvincible ? *pInvincible : true; // Invincible default
	delete pInvincible;

	unsigned* pMaxHP = pConfig->GetValue<unsigned>(GetName() + "MaxHP");
	m_iMaxHP = pMaxHP ? *pMaxHP : 1; // 1hp default
	delete pMaxHP;


	String* pTrailParticles = pConfig->GetString(strName + "TrailParticles");
	m_strTrailParticles = pTrailParticles ? *pTrailParticles : "default"; // default
	delete pTrailParticles;

	String* pExplosionParticles = pConfig->GetString(GetName() + "ExplosionParticles");
	m_strExplosionParticles = pExplosionParticles ? *pExplosionParticles : "default";
	delete pExplosionParticles;

	unsigned long* pExplodeDuration = pConfig->GetValue<unsigned long>(strName + "ExplodeDuration");
	m_iExplodeDuration = pExplodeDuration ? *pExplodeDuration : 2500;
	delete pExplodeDuration;

	unsigned long* pExplodeDelay = pConfig->GetValue<unsigned long>(strName + "ExplodeDelay");
	m_iExplodeDelay = pExplodeDelay ? *pExplodeDelay : 1500;
	delete pExplodeDelay;
}

//bool EntityType::Validate() const
//{
//	if (!ObjectType::Validate()) return false;
//	if (m_fMass <= 0) return false;
//	if (m_fDamping < 0 || m_fDamping > 1) return false;
//	if (m_fMaxSpeed < 0) return false;
//	if (m_fAngularDamping < 0 || m_fAngularDamping > 1) return false;
//
//	return true;
//}

/**
 * GameConfig code
 */

GameConfig::~GameConfig()
{
	for (ConfigMapIt it = m_mConfigs.begin(); it != m_mConfigs.end(); ++it)
	{
		delete it->second;
	}
	m_mConfigs.clear();
}

const BaseConfig* GameConfig::GetConfig(const String& rType) const
{
	ConfigMapIt it = m_mConfigs.find(rType);
	if (it == m_mConfigs.end())
		throw JFUtil::Exception("GameConfig", "GetConfig",
			"Tried to get an invalid config object", rType);
	return it->second;
}

//bool GameConfig::Validate() const
//{
//	for (ConfigMapIt it = m_mConfigs.begin(); it != m_mConfigs.end(); ++it)
//	{
//		BaseConfig* pConfig = it->second;
//		if (!pConfig->Validate()) return false;
//	}
//
//	return true;
//}

