#include <State/GameState.h>

/**
 * ObjState code
 */

ObjState::ObjState(String strType)
: m_strType(strType)
, m_iID( ObjState::GetNextID() )
{}

unsigned ObjState::GetID() const
{
	return m_iID;
}


/**
 * DynamicObjState code
 */

DynamicObjState::DynamicObjState(String strType)
: ObjState(strType)
, m_bNew(true)
, m_bRemove(false)
{}


/**
 * EntityState code
 */

EntityState::EntityState(String strType, string strSubType, String strFactionID,
	Vector3 vecPos, Quaternion qOrientation)
: DynamicObjState(strType)
// Rely on the sub class to get HP (because for some reason I can't call GetEntityType in the constructor)
, m_iHP(1)
, m_iExplodeTimer(0)
, m_bTriggerExplode(false)
, m_pGenericType(NULL) // This is only used if we instantiated a generic EntityState without a subtype
, m_strSubType(strSubType)
, m_strFactionID(strFactionID)
, m_vecPos(vecPos)
, m_qOrientation(qOrientation)
, m_vecVel(Vector3::ZERO)
, m_vecAccel(Vector3::ZERO)
, m_fBoundingRadius(0)
, m_pBV(NULL)
{
}

EntityState::EntityState(const EntityState& rState)
: DynamicObjState(rState)
, m_iHP(rState.m_iHP)
, m_iExplodeTimer(rState.m_iExplodeTimer)
, m_bTriggerExplode(rState.m_bTriggerExplode)
, m_pGenericType(rState.m_pGenericType)
, m_strSubType(rState.m_strSubType)
, m_strFactionID(rState.m_strFactionID)
, m_vecPos(rState.m_vecPos)
, m_qOrientation(rState.m_qOrientation)
, m_vecVel(rState.m_vecVel)
, m_vecAccel(rState.m_vecAccel)
, m_fBoundingRadius(rState.m_fBoundingRadius)
, m_pBV(rState.m_pBV)
{
}

const EntityType* EntityState::GetEntityType() const
{
	// We don't want this function to be pure virtual, so we throw an exception if this code is called.
	if (m_pGenericType) return m_pGenericType;
	else throw JFUtil::Exception("EntityState", "GetEntityType",
		"Called GetEntityType on a generic EntityState (or wasn't implemented in subclass) for entity type", GetType());
}

void EntityState::SetEntityType(const EntityType* pType)
{
	if (m_pGenericType)
		throw JFUtil::Exception("EntityState", "SetEntityType",
			"Called SetEntityType when m_pEntityType was already set", GetType());
	m_pGenericType = pType;
}


void EntityState::SetFaceDir(const Vector3& rVec)
{
	Quaternion qRot = GetFaceDir().getRotationTo(rVec);
	m_qOrientation = qRot * m_qOrientation;
}

void EntityState::ApplyDamage(int iAmount)
{
	const EntityType* pType = GetEntityType();
	// If we're invincible, do nothing.
	if (pType->GetInvincible()) return;
	// Modify the HP value and make sure it's not below 0 or above the maximum
	m_iHP += iAmount;
	if (m_iHP < 0) m_iHP = 0;
	if (m_iHP > (long)pType->GetMaxHP()) m_iHP = pType->GetMaxHP();

	// The entity was destroyed, so trigger the explosion effect.
	if (m_iHP <= 0) SetDestroyed();

}

void EntityState::SetDestroyed()
{
	const EntityType* pType = GetEntityType();

	m_iHP = 0;
	m_bTriggerExplode = true;
	m_iExplodeTimer = pType->GetExplodeDelay();
}

bool EntityState::CollidingWith(const EntityState* pEntity) const
{
	if (!IsCollidable() || !pEntity->IsCollidable()) return false;

	if (m_pBV)
	{
		if (m_fBoundingRadius > 0 && !pEntity->CollidingWith(m_vecPos, m_fBoundingRadius))
			return false;
		return pEntity->CollidingWith(m_pBV);
	}
	else // We don't have a BV but we are collidable so we must have a radius..
	{
		assert(m_fBoundingRadius > 0);
		return pEntity->CollidingWith(m_vecPos, m_fBoundingRadius);
	}
}

bool EntityState::CollidingWith(const BoundingVolume* pBV) const
{
	assert(pBV);
	if (!IsCollidable()) return false;

	if (m_fBoundingRadius > 0)
	{
		BoundingVolume* pSphere = new BoundingSphere(m_vecPos, m_fBoundingRadius);
		if (!pBV->CheckBoundingVolume(pSphere))
		{
			delete pSphere;
			return false;
		}
		else delete pSphere;
	}

	// If we get here it was within the sphere. So if we don't have a BV, then just return true.
	bool bReturn = true;
	if (m_pBV)
	{
		BoundingVolume* pMyBV = BVFactory::Get().MakeCopy( m_pBV );
		pMyBV->SetPosition( m_vecPos );
		pMyBV->SetOrientation( m_qOrientation );
		bReturn = pMyBV->CheckBoundingVolume(pBV);
		delete pMyBV;
	}
	return bReturn;
}

bool EntityState::CollidingWith(const Vector3& rPos, const Real& rRadius) const
{
	assert(rRadius >= 0);
	if (!IsCollidable()) return false;
	if (m_fBoundingRadius > 0)
	{
		Real fDistSq = m_vecPos.squaredDistance(rPos);
		Real fMaxDistSq = rRadius + m_fBoundingRadius;
		fMaxDistSq *= fMaxDistSq;
		if (fDistSq > fMaxDistSq) return false;
	}

	// If we get here it was within the sphere. So if we don't have a BV, then just return true.
	if (!m_pBV) return true;
	BoundingVolume* pSphere = new BoundingSphere(rPos, rRadius);
	BoundingVolume* pMyBV = BVFactory::Get().MakeCopy( m_pBV );
	pMyBV->SetPosition( m_vecPos );
	pMyBV->SetOrientation( m_qOrientation );
	bool bReturn = pMyBV->CheckBoundingVolume(pSphere);
	delete pMyBV;
	delete pSphere;
	return bReturn;
}

void EntityState::SetBoundingRadius(const Real& rRadius)
{
	assert(rRadius >= 0);
	m_fBoundingRadius = rRadius;
}

void EntityState::SetBoundingVolume(const BoundingVolume* pBV)
{
	assert(pBV);
	m_pBV = pBV;
}

void EntityState::Update(unsigned long iTime)
{
	// The EntityState update handles entity destruction and explosions.
	// Use two bools so that it goes through two updates with m_bTrigger explode on
	// Handle triggers and timers
	if (m_iHP <= 0 && m_iExplodeTimer > 0)
	{
		if (m_bTriggerExplode) m_bTriggerExplode = false;

		m_iExplodeTimer -= iTime;
		if (m_iExplodeTimer < 0) m_iExplodeTimer = 0;
	}
}
