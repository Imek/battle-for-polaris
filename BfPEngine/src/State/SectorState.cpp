#include <State/SectorState.h>

// SectorState constructor
SectorState::SectorState(const String& rName, const StringVector& rObjTypes)
: m_strName(rName)
, m_rObjTypes(rObjTypes)
{
	// Set up an object list in the map for each given type.
	for (StringVector::const_iterator it = rObjTypes.begin(); it != rObjTypes.end(); it++)
	{
		m_mObjects[*it] = ObjList();
	}
}


SectorState::SectorState(const SectorState& rState)
: m_strName(rState.m_strName)
, m_rObjTypes(rState.m_rObjTypes)
{
	// TODO: Untested - do we even need to copy sectors?
	assert(!m_strName.empty());

	// TODO: We need an object copy factory class for this to work generically.
	// Right now, BfpSectorState handles copying of its objects...
	// Copy each object list to this object
	/*ObjListMap::const_iterator itMap;
	for (itMap = m_mObjects.begin(); it != m_mObjects.end(); ++it)
	{
		const String& rType = itMap->first;
		const ObjList& rObjs = itMap->second;
		for (ObjList::const_iterator it = rShips.begin(); it != rShips.end(); ++it)
		{
			Obj* pShip = (ShipState*)*it;
			//cout << "copy ship" << endl;
			rMyShips.push_back( new ShipState(*pShip) );
		}
	}*/
}


SectorState::~SectorState()
{
	// Delete all objects from all lists
	for (ObjListMap::iterator itList = m_mObjects.begin();
		 itList != m_mObjects.end(); ++itList)
	{
		ObjList& rList = itList->second;
		for (ObjList::iterator itObj = rList.begin();
			 itObj != rList.end();)
		{
			delete *itObj;
			itObj = rList.erase(itObj);
		}
	}
}


const ObjList& SectorState::GetObjectList(String strType) const
{
	ObjListMap::const_iterator it = m_mObjects.find(strType);
	if (it == m_mObjects.end())
		throw JFUtil::Exception("SectorState", "GetObjectList",
			"ObjListMap not found for given type", strType);
	return it->second;
}

ObjList& SectorState::GetObjectList(String strType)
{
	ObjListMap::iterator it = m_mObjects.find(strType);
	if (it == m_mObjects.end())
		throw JFUtil::Exception("SectorState", "GetObjectList",
			"ObjListMap not found for given type", strType);
	assert(m_mObjects.find(strType) != m_mObjects.end());
	return it->second;
}


const ObjState* SectorState::GetObjState(String strType, unsigned iID) const
{
	const ObjList& rObjs = GetObjectList(strType);
	for (ObjList::const_iterator it = rObjs.begin();
		 it != rObjs.end(); ++it)
	{
		const ObjState* pObject = *it;
		assert(pObject);
		assert(pObject->GetType() == strType);
		if (pObject->GetID() == iID)
			return pObject;
	}

	return NULL;
}

ObjState* SectorState::GetObjState(String strType, unsigned iID)
{
	ObjList& rObjs = GetObjectList(strType);
	for (ObjList::iterator it = rObjs.begin();
		 it != rObjs.end(); ++it)
	{
		ObjState* pObject = *it;
		assert(pObject);
		assert(pObject->GetType() == strType);
		if (pObject->GetID() == iID)
			return pObject;
	}

	return NULL;
}


DynObjList SectorState::GetNewObjects(String strType)
{
	ObjList& rObjs = GetObjectList(strType);
	DynObjList vReturn;
	for (ObjList::iterator it = rObjs.begin();
		 it != rObjs.end(); ++it)
	{
		ObjState* pObj = *it;
		assert(pObj->GetType() == strType);
		if (pObj->IsDynamic())
		{
			DynamicObjState* pDObj = (DynamicObjState*)pObj;
			if (pDObj->GetNew())
			{
				//pProj->m_bNew = false;
				vReturn.push_back(pDObj);
			}
		}

	}
	return vReturn;
}

DynObjList SectorState::GetRemovedObjects(String strType)
{
	ObjList& rObjs = GetObjectList(strType);
//	cout << "things in sector " << GetSectorType()->GetName() << ": " << rObjs.size() << endl;
	DynObjList vReturn;
	for (ObjList::iterator it = rObjs.begin();
		 it != rObjs.end();)
	{
		ObjState* pObj (*it);
		assert(pObj->GetType() == strType);
		if (pObj->IsDynamic())
		{
			DynamicObjState* pDObj = (DynamicObjState*)pObj;
			if (pDObj->GetRemoved())
			{
				it = rObjs.erase(it);
				vReturn.push_back(pDObj);
			}
			else
				++it;
		}
		else
			++it;
	}
//	cout << "things removed: " << vReturn.size() << endl;
//	cout << "things left: " << rObjs.size() << endl;
	return vReturn;
}


bool SectorState::ObjectExists(String strType, unsigned iID) const
{
	const ObjState* pObj;
	pObj = GetObjState(strType, iID);
	if (!pObj) return false;
	else
	{
		assert(pObj->GetType() == strType);
		return true;
	}
}

