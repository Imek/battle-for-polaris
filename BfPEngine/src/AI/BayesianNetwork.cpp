#include <AI/BayesianNetwork.h>

BayesianNetworkNode::BayesianNetworkNode(NodeLabel* label)
: m_label(label)
, m_isOrigin(true)
{
	assert(m_label != NULL);
}

BayesianNetworkNode::~BayesianNetworkNode()
{
	delete m_label;
	for (EntryMap::iterator it = m_stateEntries.begin(); it != m_stateEntries.end(); it++)
	{
		delete it->second;
	}
	// Note: the BayesianNetwork itself manages the memory for these.
}

bool BayesianNetworkNode::AddStateEntry(unsigned stateID, StateEntry* newEntry)
{
	assert(newEntry);
	if (m_stateEntries.find(stateID) != m_stateEntries.end())
	{
		m_stateEntries[stateID] = newEntry;
		return true; // Successful insert
	}
	else
	{
		return false; // Key already used
	}
}

bool BayesianNetworkNode::AttachNode(BayesianNetworkNode& child)
{
	const String& nodeLabelStr = child.GetLabel()->GetString();
	if (m_childNodes.find(nodeLabelStr) != m_childNodes.end())
	{
		return false; // Already got a link to that node
	}
	else
	{
		m_childNodes[nodeLabelStr] = &child;
		child.OnAttachedToNode(*this);
		return true;
	}
}

void BayesianNetworkNode::OnAttachedToNode(const BayesianNetworkNode& parent)
{
	assert(parent != *this);
	m_isOrigin = false;
}

bool BayesianNetworkNode::HasChild(const BayesianNetworkNode& child) const
{
	return m_childNodes.find(child.GetLabel()->GetString()) != m_childNodes.end();
}

BayesianNetwork::~BayesianNetwork()
{
	// Destroy all nodes managed by this network
	for (NodePtrMap::iterator it = m_nodesByLabel.begin(); it != m_nodesByLabel.end(); it++)
	{
		delete it->second;
	}
}

BayesianNetworkNode& BayesianNetwork::GetNode(const String& labelStr)
{
	return *m_nodesByLabel[labelStr];
}

bool BayesianNetwork::AddNode(BayesianNetworkNode* newNode)
{
	assert(newNode);
	const String& nodeLabelStr = newNode->GetLabel()->GetString();
	if (m_nodesByLabel.find(nodeLabelStr) != m_nodesByLabel.end())
	{
		return false; // Already got a node with that label
	}
	else
	{
		m_nodesByLabel[nodeLabelStr] = newNode;
		return true;
	}
}

NodePtrList* BayesianNetwork::MakeOriginNodeList() const
{
	// Just iterate and grab those with the flag set.
	// NOTE: We might want to cache the result of this somewhere (on the caller, checking a dirty
	// flag on the network itself?)
	NodePtrList* originNodes = new NodePtrList;
	for (NodePtrMap::const_iterator it = m_nodesByLabel.begin(); it != m_nodesByLabel.end(); it++)
	{
		originNodes->push_back(it->second);
	}
	return originNodes;
}

void BayesianNetworkUpdater::UpdateNetwork(BayesianNetwork& network)
{
	// NOTE: This relies on the network already having been validated to not contain cycles.
	NodePtrList* nodeQueue = network.MakeOriginNodeList();
	BayesianNetworkNode* calcNode;
	NodePtrMap* children;
	while (!nodeQueue->empty())
	{
		calcNode = *(nodeQueue->begin());
		// Perform our strategy-specific behaviour (propagation)
		PropagateToChildren(*calcNode);
		// Enqueue this node's children for processing.
		// NOTE: Could optimise by having PropagateToChildren push the children for us - but then
		// we might want a better way to split the base class code from sub-class implementation?
		children = calcNode->GetChildNodes();
		for (NodePtrMap::iterator it = children->begin(); it != children->end(); it++)
		{
			nodeQueue->push_back(it->second);
		}
		// Removes calcNode from the queue (pop doesn't return popped value?! What a country!)
		nodeQueue->pop_front();
	}

	delete nodeQueue;
}

void EvenSplitUpdater::PropagateToChildren(BayesianNetworkNode& parent)
{
	// Determine the number of affected children, bailing early if there's nothing to do.
	size_t numBetween = parent.GetNumChildren() + 1;
	assert(numBetween >= 1);
	if (numBetween == 1)
	{
		return;
	}

	NodePtrMap* children = parent.GetChildNodes();
	EntryMap* fromEntries = parent.GetStateEntries();

	// For each state entry, split its probability evenly between batching entries in all children.
	Real splitAmount;
	EntryMap* toEntries;
	EntryMap::iterator itToEntry;
	for (EntryMap::iterator itEntry = fromEntries->begin(); itEntry != fromEntries->end(); itEntry++)
	{
		splitAmount = itEntry->second->GetProbability() / numBetween;
		for (NodePtrMap::iterator itChild = children->begin(); itChild != children->end(); itChild++)
		{
			toEntries = itChild->second->GetStateEntries();
			itToEntry = toEntries->find(itEntry->first);
			if (itToEntry != toEntries->end())
			{
				itToEntry->second->AlterProbability(splitAmount);
			}
		}
		itEntry->second->SetProbability(splitAmount);
	}
}
