#include <Ogre/OgreFramework.h>

#include <Game/GameConfig.h>

template<> OgreFramework* JFUtil::Singleton<OgreFramework>::s_instance = NULL;

OgreFramework::OgreFramework()
{
	m_bShutDownOgre		= false;
	m_iNumScreenShots	= 0;

	m_pRoot				= 0;
	m_pSceneMgr			= 0;
	m_pRenderWnd		= 0;
	m_pCamera			= 0;
	m_pViewport			= 0;
	m_pLog				= 0;
	m_pTimer			= 0;

	m_pDebugOverlay		= 0;
	m_pInfoOverlay		= 0;
}

void OgreFramework::initOgre(Ogre::String wndTitle)
{
	// Managed as a singleton (so don't worry about deleting manually)
	new Ogre::LogManager();

	m_pLog = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
	m_pLog->setDebugOutputEnabled(true);

	Ogre::String pluginFileName = GameConfig::CFG_PATH+"plugins.cfg";

	m_pRoot = new Ogre::Root(pluginFileName);

	// TODO: Check config to see if we want to show the config dialogue
	m_pRoot->showConfigDialog();
	m_pRenderWnd = m_pRoot->initialise(true, wndTitle);

	m_pSceneMgr = m_pRoot->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
	m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7, 0.7, 0.7));


	m_pCamera = m_pSceneMgr->createCamera("Camera");
	m_pCamera->setPosition(Ogre::Vector3(0, 0, -60));
	m_pCamera->lookAt(Ogre::Vector3(0,0,0));
	m_pCamera->setNearClipDistance(1);

	m_pViewport = m_pRenderWnd->addViewport(m_pCamera);
	m_pViewport->setBackgroundColour(Ogre::ColourValue(0.8, 0.7, 0.6, 1.0));

	m_pCamera->setAspectRatio(
		Ogre::Real(m_pViewport->getActualWidth()) /
		Ogre::Real(m_pViewport->getActualHeight()));

	m_pViewport->setCamera(m_pCamera);

	Ogre::String secName, typeName, archName;
	Ogre::ConfigFile cf;
		cf.load(GameConfig::CFG_PATH+"resources.cfg");

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		while (seci.hasMoreElements())
		{
			secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;
			for (i = settings->begin(); i != settings->end(); ++i)
			{
				typeName = i->first;
				archName = i->second;
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
			}
		}

	// We use a loading bar here, as this is where the resources for the specific game are loaded.
	m_oLoadingBar.start(m_pRenderWnd, 1, 1, 0.75);

	// Turn off rendering of everything except overlays
	m_pSceneMgr->clearSpecialCaseRenderQueues();
	m_pSceneMgr->addSpecialCaseRenderQueue(RENDER_QUEUE_OVERLAY);
	m_pSceneMgr->setSpecialCaseRenderQueueMode(SceneManager::SCRQM_INCLUDE);

	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	// Pre-load the "General" resource group (which contains pretty much everything..) so that we don't get pauses in the game
	// NOTE: If we cant to move loading of resources to a later point, we need to comment/move this line
	Ogre::ResourceGroupManager::getSingleton().loadResourceGroup("General", true, true);

	m_pTimer = new Ogre::Timer();
	m_pTimer->reset();

	m_pDebugOverlay =
		Ogre::OverlayManager::getSingleton().getByName("Core/DebugOverlay");
	//m_pDebugOverlay->show();

	m_pRenderWnd->setActive(true);

	// Back to full rendering
	m_pSceneMgr->clearSpecialCaseRenderQueues();
	m_pSceneMgr->setSpecialCaseRenderQueueMode(SceneManager::SCRQM_EXCLUDE);

	m_oLoadingBar.finish();
}

OgreFramework::~OgreFramework()
{
	delete m_pRoot;
}

void OgreFramework::updateOgre(unsigned long iTime)
{
	updateStats();
}

bool OgreFramework::TakeScreenShot()
{
	std::ostringstream ss;
	ss << "screenshot_" << ++m_iNumScreenShots << ".png";
	m_pRenderWnd->writeContentsToFile(ss.str());
	return true;
}

bool OgreFramework::TogglePolyMode()
{
	static int mode = 0;

	if(mode == 2)
	{
		m_pCamera->setPolygonMode(Ogre::PM_SOLID);
		mode = 0;
	}
	else if(mode == 0)
	{
		 m_pCamera->setPolygonMode(Ogre::PM_WIREFRAME);
		 mode = 1;
	}
	else if(mode == 1)
	{
		m_pCamera->setPolygonMode(Ogre::PM_POINTS);
		mode = 2;
	}
	return true;
}

bool OgreFramework::ToggleOverlayMode()
{
	if(m_pDebugOverlay)
	{
		if(!m_pDebugOverlay->isVisible())
			m_pDebugOverlay->show();
		else
			m_pDebugOverlay->hide();
	}
	return true;
}

void OgreFramework::updateStats()
{
	static Ogre::String currFps = "Current FPS: ";
	static Ogre::String avgFps = "Average FPS: ";
	static Ogre::String bestFps = "Best FPS: ";
	static Ogre::String worstFps = "Worst FPS: ";
	static Ogre::String tris = "Triangle Count: ";
	static Ogre::String batches = "Batch Count: ";

	Ogre::OverlayManager* pOverlay = Ogre::OverlayManager::getSingletonPtr();

	Ogre::OverlayElement* guiAvg = pOverlay->getOverlayElement("Core/AverageFps");
	Ogre::OverlayElement* guiCurr = pOverlay->getOverlayElement("Core/CurrFps");
	Ogre::OverlayElement* guiBest = pOverlay->getOverlayElement("Core/BestFps");
	Ogre::OverlayElement* guiWorst = pOverlay->getOverlayElement("Core/WorstFps");

	const Ogre::RenderTarget::FrameStats& stats = m_pRenderWnd->getStatistics();
	guiAvg->setCaption(avgFps + Ogre::StringConverter::toString(stats.avgFPS));
	guiCurr->setCaption(currFps + Ogre::StringConverter::toString(stats.lastFPS));
	guiBest->setCaption(bestFps + Ogre::StringConverter::toString(stats.bestFPS)
		+" "+Ogre::StringConverter::toString(stats.bestFrameTime)+" ms");
	guiWorst->setCaption(worstFps + Ogre::StringConverter::toString(stats.worstFPS)
		+" "+Ogre::StringConverter::toString(stats.worstFrameTime)+" ms");

	Ogre::OverlayElement* guiTris = pOverlay->getOverlayElement("Core/NumTris");
	guiTris->setCaption(tris + Ogre::StringConverter::toString(stats.triangleCount));

	Ogre::OverlayElement* guiBatches = pOverlay->getOverlayElement("Core/NumBatches");
	guiBatches->setCaption(batches + Ogre::StringConverter::toString(stats.batchCount));

	Ogre::OverlayElement* guiDbg = pOverlay->getOverlayElement("Core/DebugText");
	guiDbg->setCaption("");
}
