/**
 * PhysicsSystem.cpp - implementations for PhysicsSystem.h
 */

#include <Physics/Intersection.h>

#include <Physics/PhysicsSystem.h>

using namespace JFPhysics;

/**
 * EntityPhysics code
 */

EntityPhysics::EntityPhysics(JFPhysics::Particle* pObject,
	SectorState* pWorld, const String& rEntityType, unsigned iEntityID,
	const BoundingVolume* pBV)
: m_pObject(pObject)
, m_pWorld(pWorld), m_strEntityType(rEntityType), m_iEntityID(iEntityID)
, m_pBV(BVFactory::Get().MakeCopy(pBV))
{
	assert(m_pWorld);
}

EntityPhysics::~EntityPhysics()
{
	delete m_pObject;
	delete m_pBV;
}

EntityState* EntityPhysics::GetState()
{
	return (EntityState*)m_pWorld->GetObjState(m_strEntityType, m_iEntityID);
}

CollisionData* EntityPhysics::CollidingWith(const BoundingVolume* pBV)
{
	if (!m_pBV || !pBV)
		return NULL; // No collision possible without both bounding volumes
	else
		return m_pBV->CheckBoundingVolume(pBV);
}

CollisionData* EntityPhysics::CollidingWith(const EntityPhysics* pObj)
{
	if (!m_pBV || !pObj->m_pBV)
		return NULL; // No collision possible without both bounding volumes
	else
		return m_pBV->CheckBoundingVolume(pObj->m_pBV);
}

/**
 * ParticleForceManager code
 */

void ParticleForceManager::Add(JFPhysics::Particle* pParticle, Force *pForce)
{
	assert(pParticle && pForce);
	ParticleForce oNewOne (pParticle, pForce);
	m_vParticleForces.push_back(oNewOne);
}

void ParticleForceManager::Remove(JFPhysics::Particle* pParticle, Force *pForce)
{
	for (std::vector<ParticleForce>::iterator it = m_vParticleForces.begin();
		 it != m_vParticleForces.end();)
	{
		if (it->m_pParticle == pParticle &&
			it->m_pForce == pForce)
		{
			it = m_vParticleForces.erase(it);
		}
		else
		{
			++it;
		}
	}
}

void ParticleForceManager::Update(unsigned long iTime)
{
	for (std::vector<ParticleForce>::iterator it = m_vParticleForces.begin();
		 it != m_vParticleForces.end(); ++it)
	{
		it->m_pForce->Update(it->m_pParticle, iTime);
	}
}

string ParticleForceManager::GetString() const
{
	std::stringstream sout;
	sout << m_vParticleForces.size();
	return sout.str();
}
