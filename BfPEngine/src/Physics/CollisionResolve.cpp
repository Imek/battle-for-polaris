#include <Physics/CollisionResolve.h>

/**
 * ParticleCollision code
 */

using JFPhysics::ParticleCollision;

ParticleCollision::ParticleCollision(
	Particle* pOne, Particle* pTwo, Real fRestitution, const CollisionData& rData)
	: m_pOne(pOne), m_pTwo(pTwo), m_fRestitution(fRestitution)
	, m_vecContNormal(rData.m_vecNormal), m_vecPenetration(rData.m_vecPenetration)
{
	m_vecContNormal.normalise();
	//assert(m_vecContNormal.length() == 1);
}

// Resolve the collision, altering the attributes of the particle(s) accordingly.
void ParticleCollision::Resolve(unsigned long iTime)
{
	ResolveVelocity(iTime);
	ResolvePenetration(iTime);
}


Real ParticleCollision::CalcSeparatingVelocity() const
{
	Vector3 vecRelVel = m_pOne->GetVel();
	if (m_pTwo) vecRelVel -= m_pTwo->GetVel();
	return vecRelVel.dotProduct(m_vecContNormal);
}

// Resolve velocity: calculate a new velocity for the particle(s) given the attributes of this
// collision.
void ParticleCollision::ResolveVelocity(unsigned long iTime)
{
	// Resolve the velocity of the first particle.

	Real fSepVel = CalcSeparatingVelocity();

	// The particles are moving apart from each other??
	if (fSepVel > 0) return;

	Real fNewSepVel = -fSepVel * m_fRestitution;

	Vector3 vecCausedVel = m_pOne->GetAccel();
	if (m_pTwo) vecCausedVel -= m_pTwo->GetAccel();
	Real fCausedSepVel = vecCausedVel.dotProduct(m_vecContNormal) * iTime;

	if (fCausedSepVel < 0)
	{
		fNewSepVel += m_fRestitution * fCausedSepVel;
		if (fNewSepVel < 0) fNewSepVel = 0;
	}

	Real fDeltaVel = fNewSepVel - fSepVel;

	Real fTotalInvMass = m_pOne->GetInvMass();
	if (m_pTwo) fTotalInvMass += m_pTwo->GetInvMass();

	// All particles have infinite pass, so we can't do much.
	if (fTotalInvMass <= 0) return;

	// Calculate the impulse to apply
	Real fImpulse = fDeltaVel / fTotalInvMass;
	Vector3 vecImpulsePerInvMass = m_vecContNormal * fImpulse;

	//cout << "vel before: " << m_pOne->GetVel().GetString() << endl;
	// Apply impulses: they are applied in the direction of the contact,
	// and are proportional to the inverse mass.
	m_pOne->SetVel(
		(m_pOne->GetVel() +
		vecImpulsePerInvMass) * m_pOne->GetInvMass()
	);
	//cout << "vel after: " << m_pOne->GetVel().GetString() << endl << endl;

	if (m_pTwo)
	{
		// The second particle is pretty much the same, except in the opposite direction.
		m_pTwo->SetVel(Vector3(m_pTwo->GetVel() +
			vecImpulsePerInvMass * -m_pTwo->GetInvMass())
			);
	}

}

// Resolve penetration: the CollisionData object contains a vector describing how fat the
// first object is penetrating into the second; we need to alter its position to avoid this.
// For our purposes, it should be sufficient just to move the first particle all the way.
void ParticleCollision::ResolvePenetration(unsigned long iTime)
{
	m_pOne->SetPos(m_pOne->GetPos() - m_vecPenetration);
}

