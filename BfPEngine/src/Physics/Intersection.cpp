#include <cstdlib>

#include <Physics/Intersection.h>

using namespace JFPhysics;

/**
 * Quad code
 */

Quaternion Quad::m_qRot;

Quad::Quad()
{

}

Quad::Quad(Vector3 vecTL, Vector3 vecTR, Vector3 vecBR, Vector3 vecBL, Vector3 vecN)
: TL(vecTL), TR(vecTR), BR(vecBR), BL(vecBL), N(vecN)
{
	// TODO: Add some validation here (low priority)
}

void Quad::Translate(const Vector3& rVec)
{
	TL += rVec;
	TR += rVec;
	BR += rVec;
	BL += rVec;
}

void Quad::Rotate(const Quaternion& rQuat)
{
	TL = rQuat * TL;
	TR = rQuat * TR;
	BR = rQuat * BR;
	BL = rQuat * BL;
	N = rQuat * N;
}

void Quad::Rotate(const Vector3& rAxis, Degree fAngle)
{
	m_qRot.FromAngleAxis(Radian(fAngle), rAxis);
	Rotate(m_qRot);
}

Quad Quad::GetRotated(const Quaternion& rQuat) const
{
	Quad qCopy (*this);
	qCopy.Rotate(rQuat);
	return qCopy;
}

Quad Quad::GetRotated(const Vector3& rAxis, Degree fAngle) const
{
	m_qRot.FromAngleAxis(Radian(fAngle), rAxis);
	return GetRotated(m_qRot);
}


/**
 * HierarchicalBV code
 */

HierarchicalBV::HierarchicalBV(const HierarchicalBV& rBV)
: BoundingVolume(BV_HIERARCHICAL)
, m_pVolume(BVFactory::Get().MakeCopy(rBV.m_pVolume))
{
	assert(m_pVolume->GetType() != BV_HIERARCHICAL);
	for (BVVector::const_iterator it = rBV.GetChildren().begin();
		 it != rBV.GetChildren().end(); ++it)
	{
		const BVFactory&  rFact = BVFactory::Get();
		m_vChildren.push_back(rFact.MakeCopy(*it));
	}
}

void HierarchicalBV::Translate(const Vector3& rVec)
{
	// Traverse the hierarchy by calling Translate on each child
	m_pVolume->Translate(rVec);
	for (BVVector::iterator it = m_vChildren.begin();
		 it != m_vChildren.end(); ++it)
	{
		BoundingVolume* pChild = *it;
		pChild->Translate(rVec);
	}
}

void HierarchicalBV::SetPosition(const Vector3& rPoint)
{
	// Traverse the hierarchy by calling Translate on each child
	const Vector3& rAdd = rPoint - m_pVolume->GetCentre();
	m_pVolume->SetPosition(rPoint);
	for (BVVector::iterator it = m_vChildren.begin();
		 it != m_vChildren.end(); ++it)
	{
		BoundingVolume* pChild = *it;
		pChild->Translate(rAdd);
	}
}

// TODO: I'm pretty sure HierarchicalBV's don't even work yet. (low priority, not required for project)
// TODO: Set orientation by rotating about the HBV's actual centre.
void HierarchicalBV::SetOrientation(const Quaternion& rAmount, const Vector3* pCentre)
{
	// Traverse the hierarchy by calling Rotate on each child
	if (!pCentre)
		pCentre = new Vector3(m_pVolume->GetCentre());
	for (BVVector::iterator it = m_vChildren.begin();
		 it != m_vChildren.end(); ++it)
	{
		BoundingVolume* pChild = *it;
		pChild->SetOrientation(rAmount, pCentre);
	}
}

bool HierarchicalBV::CheckPoint(const Vector3& rPoint) const
{
	// First check our volume, then traverse the tree by calling it on each child.
	if (m_pVolume->CheckPoint(rPoint))
	{
		if (m_vChildren.empty()) return true; // Nothing else to do

		for (BVVector::const_iterator it = m_vChildren.begin();
			 it != m_vChildren.end(); ++it)
		{
			const BoundingVolume* pChild = *it;
			if (pChild->CheckPoint(rPoint)) return true;
		}
	}

	return false;
}

CollisionData* HierarchicalBV::CheckBoundingVolume(const BoundingVolume* pVolume) const
{
	// Traverse the hierarchy like CheckPoint
	if (m_pVolume->CheckBoundingVolume(pVolume))
	{
		for (BVVector::const_iterator it = m_vChildren.begin();
			 it != m_vChildren.end(); ++it)
		{
			const BoundingVolume* pChild = *it;
			CollisionData* pData = pChild->CheckBoundingVolume(pVolume);
			if (pData) return pData;
		}
	}

	return NULL;
}

/**
 * BVFactory code
 */
BVFactory* BVFactory::m_pInstance = NULL;

/**
 * BoundingSphere code
 */

BoundingSphere::BoundingSphere(Vector3 vecPos, Real fRadius)
: BoundingVolume(BV_SPHERE), m_oVol(vecPos, fRadius)
{

}

BoundingSphere::BoundingSphere(const BoundingSphere& rBV)
: BoundingVolume(BV_SPHERE)
, m_oVol(rBV.m_oVol)
{
}

const Vector3& BoundingSphere::GetCentre() const { return m_oVol.m_vecPos; }

const Sphere& BoundingSphere::GetSphere() const { return m_oVol; }

void BoundingSphere::Translate(const Vector3& rVec)
{
	m_oVol.m_vecPos += rVec;
}

void BoundingSphere::SetPosition(const Vector3& rPoint)
{
	m_oVol.m_vecPos = rPoint;
}

void BoundingSphere::SetOrientation(const Quaternion& rQuat, const Vector3* pCentre)
{
	// TODO: Implement once we're properly using hierarchical BVs (rotate our centre around pCentre)
}

bool BoundingSphere::CheckPoint(const Vector3& rVec) const
{
	Real fDistSq = (rVec - m_oVol.m_vecPos).squaredLength();
	return fDistSq <= m_oVol.m_fRadius*m_oVol.m_fRadius;
}

CollisionData* BoundingSphere::CheckBoundingVolume(const BoundingVolume* pVolume) const
{
	assert(pVolume);
	// BV_HIERARCHICAL, BV_AABB, BV_OBB, BV_SPHERE
	switch (pVolume->GetType())
	{
	case BV_HIERARCHICAL:
		{
			CollisionData* pData = pVolume->CheckBoundingVolume(this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;
		}
	case BV_AABB:
		return Intersection::SphereAndBox(m_oVol,
			((BoundingBox*)pVolume));
	case BV_OBB:
		return Intersection::SphereAndBox(m_oVol,
			((BoundingBox*)pVolume));
	case BV_SPHERE:
		return Intersection::SphereAndSphere(m_oVol,
			((BoundingSphere*)pVolume)->GetSphere());
	default:
		throw JFUtil::Exception("BoundingSphere", "CheckBoundingVolume",
			"Invalid bounding volume type?");
	}
	return NULL;
}

/**
 * BoundingBox code
 */

BoundingBox::BoundingBox(const BoundingBox& rBV)
: BoundingVolume(rBV.GetType())
, m_vecCentre(rBV.m_vecCentre)
, m_vecFBL(rBV.m_vecFBL)
, m_vecFTL(rBV.m_vecFTL)
, m_vecFTR(rBV.m_vecFTR)
, m_vecFBR(rBV.m_vecFBR)
, m_vecBBL(rBV.m_vecBBL)
, m_vecBTL(rBV.m_vecBTL)
, m_vecBTR(rBV.m_vecBTR)
, m_vecBBR(rBV.m_vecBBR)
{
}

/**
 * OrientedBoundingBox code
 */

 // TODO: Testing required here?
OrientedBoundingBox::OrientedBoundingBox(Vector3 vecMin, Vector3 vecMax)
: BoundingBox(BV_OBB)
{
	// Valid min and max vectors provided?
	/*if (!(*vecMin.x < *vecMax.x &&
		   *vecMin.y < *vecMax.y &&
		   *vecMin.z < *vecMax.z))
	{
		   cout << "WHAT????" << endl;
	}*/
	assert(vecMin.x < vecMax.x &&
		   vecMin.y < vecMax.y &&
		   vecMin.z < vecMax.z);

	// Set the centre
	m_vecCentre = vecMin + (vecMax-vecMin)/2;

	m_arrPlanes = new Plane[6];
	// Set the normals and points, with Y as the up/down axis, X as the left/right axis
	// and Z as the depth axis, and using vecMin/vecMax as points.
	m_arrPlanes[TOP].redefine(Vector3::UNIT_Y, vecMax); // Top (Y increasing)
	m_arrPlanes[BOTTOM].redefine(Vector3::NEGATIVE_UNIT_Y, vecMin); // Bottom (Y decreasing)
	m_arrPlanes[LEFT].redefine(Vector3::NEGATIVE_UNIT_X, vecMin); // Left (X decreasing)
	m_arrPlanes[RIGHT].redefine(Vector3::UNIT_X, vecMax); // Right (X increasing)
	m_arrPlanes[FRONT].redefine(Vector3::NEGATIVE_UNIT_Z, vecMin); // Front (Z decreasing)
	m_arrPlanes[RIGHT].redefine(Vector3::UNIT_Z, vecMax); // Back (Z increasing)

	// Set the corners
	m_vecFBL = vecMin;
	m_vecFTL = Vector3(vecMin.x, vecMax.y, vecMin.z);
	m_vecFTR = Vector3(vecMin.x, vecMax.y, vecMax.z);
	m_vecFBR = Vector3(vecMin.x, vecMin.y, vecMax.z);
	m_vecBBL = Vector3(vecMax.x, vecMin.y, vecMin.z);
	m_vecBTL = Vector3(vecMax.x, vecMax.y, vecMin.z);
	m_vecBTR = vecMax;
	m_vecBBR = Vector3(vecMax.x, vecMin.y, vecMin.z);

}

OrientedBoundingBox::OrientedBoundingBox(const OrientedBoundingBox& rBV)
: BoundingBox(rBV)
, m_arrPlanes(new Plane[6])
{
	for (unsigned i = 0; i < 6; ++i)
		m_arrPlanes[i] = Plane(rBV.m_arrPlanes[i]);
}

OrientedBoundingBox::~OrientedBoundingBox()
{
	delete [] m_arrPlanes;
}

void OrientedBoundingBox::Rotate(const Quaternion& rQuat)
{
	// Rotate each corner
	Vector3 vecCalc;
	vecCalc = rQuat * (m_vecFBL - m_vecCentre);
	m_vecFBL = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecFTL - m_vecCentre);
	m_vecFTL = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecFTR - m_vecCentre);
	m_vecFTR = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecFBR - m_vecCentre);
	m_vecFBR = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecBBL - m_vecCentre);
	m_vecBBL = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecBTL - m_vecCentre);
	m_vecBTL = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecBTR - m_vecCentre);
	m_vecBTR = m_vecCentre + vecCalc;

	vecCalc = rQuat * (m_vecBBR - m_vecCentre);
	m_vecBBR = m_vecCentre + vecCalc;

	for (unsigned i = 0; i < 6; ++i)
	{
		Vector3 vecPoint = (i == 0 || i == 3 || i == 5) ? m_vecBTR : m_vecFBL;
		// Rotate the normal.
		Vector3 vecNewNormal = m_arrPlanes[i].normal;
		vecNewNormal = rQuat * vecNewNormal;

		// Can't rotate a point, so get the vector of p to that point and rotate it.
		Vector3 vecCentreToP = vecPoint - m_vecCentre;
		vecCentreToP = rQuat * vecCentreToP;
		vecPoint = m_vecCentre + vecCentreToP;

		// Redefine the plane with these new values.
		m_arrPlanes[i].redefine(vecNewNormal, vecPoint);
	}
}

void OrientedBoundingBox::Translate(const Vector3& rVec)
{
	m_vecCentre += rVec;
	m_vecFBL += rVec;
	m_vecFTL += rVec;
	m_vecFTR += rVec;
	m_vecFBR += rVec;
	m_vecBBL += rVec;
	m_vecBTL += rVec;
	m_vecBTR += rVec;
	m_vecBBR += rVec;

	for (unsigned i = 0; i < 6; ++i)
	{
		// Move the point used to define each plane and redefine that plane thusly.
		Vector3 vecPoint = (i == 0 || i == 3 || i == 5) ? m_vecBTR : m_vecFBL;
		vecPoint += rVec;

	}
}

void OrientedBoundingBox::SetPosition(const Vector3& rPoint)
{
	Vector3 vecAdd = rPoint - m_vecCentre;
	Translate(vecAdd);
}

void OrientedBoundingBox::SetOrientation(const Quaternion& rQuat, const Vector3* pCentre)
{
	m_vecCentre = Vector3(*pCentre);
	m_qOrientation = Quaternion(rQuat);
}

bool OrientedBoundingBox::CheckPoint(const Vector3& rVec) const
{
	OrientedBoundingBox oBox (*this);
	oBox.Rotate(m_qOrientation);

	return (oBox.m_arrPlanes[TOP].getDistance(rVec) <= 0 &&
			oBox.m_arrPlanes[BOTTOM].getDistance(rVec) <= 0 &&
			oBox.m_arrPlanes[LEFT].getDistance(rVec) <= 0 &&
			oBox.m_arrPlanes[RIGHT].getDistance(rVec) <= 0 &&
			oBox.m_arrPlanes[FRONT].getDistance(rVec) <= 0 &&
			oBox.m_arrPlanes[BACK].getDistance(rVec) <= 0);

}

// We should also be able to check any other type of bounding volume against this one,
// returning a pointer to a CollisionData object.
CollisionData* OrientedBoundingBox::CheckBoundingVolume(const BoundingVolume* pVolume) const
{
	assert(pVolume != NULL);

	// BV_HIERARCHICAL, BV_AABB, BV_OBB, BV_SPHERE
	switch (pVolume->GetType())
	{
	case BV_HIERARCHICAL:
		{
			CollisionData* pData = pVolume->CheckBoundingVolume(this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;
		}
	case BV_AABB:
		{
			CollisionData* pData = Intersection::BoxAndOBB(
				((BoundingBox*)pVolume), *this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;

		}
	case BV_OBB:
		return Intersection::BoxAndOBB((BoundingBox*)this,
			(*((OrientedBoundingBox*)pVolume)));
	case BV_SPHERE:
		{
			CollisionData* pData = Intersection::SphereAndBox(
				((BoundingSphere*)pVolume)->GetSphere(), (BoundingBox*)this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;

		}
	default:
		throw JFUtil::Exception("OrientedBoundingBox", "CheckBoundingVolume",
			"Invalid bounding volume type?");
	}

}

Plane* OrientedBoundingBox::GetPlaneWithNormal(Vector3 vecNormal) const
{
	for (int i=0; i<6; ++i)
	{
		if (m_arrPlanes[i].normal == vecNormal)
			return new Plane(m_arrPlanes[i]);
	}

	return NULL;

}

Plane OrientedBoundingBox::GetPlane(BOX_SIDE iSide) const
{
	return m_arrPlanes[iSide];
}


/**
 * AxisAlignedBoundingBox code
 */

AxisAlignedBoundingBox::AxisAlignedBoundingBox(Vector3 vecMin, Vector3 vecMax)
: BoundingBox(BV_AABB), m_vecMin(vecMin), m_vecMax(vecMax)
{
	m_vecFBL = Vector3(vecMin);
	m_vecFTL = Vector3(vecMin.x, vecMax.y, vecMin.z);
	m_vecFTR = Vector3(vecMin.x, vecMax.y, vecMax.z);
	m_vecFBR = Vector3(vecMin.x, vecMin.y, vecMax.z);
	m_vecBBL = Vector3(vecMax.x, vecMin.y, vecMin.z);
	m_vecBTL = Vector3(vecMax.x, vecMax.y, vecMin.z);
	m_vecBTR = Vector3(vecMax);
	m_vecBBR = Vector3(vecMax.x, vecMin.y, vecMin.z);

	// Check validity (min < max)
	if (m_vecMin.x > m_vecMax.x ||
		m_vecMin.y > m_vecMax.y ||
		m_vecMin.z > m_vecMax.z)
	{
		throw JFUtil::Exception("AxisAlignedBoundingBox", "AxisAlignedBoundingBox",
			"Invalid minimum vector given.");
	}

	m_vecCentre = m_vecMin.midPoint(m_vecMax);
	m_vecMaxFromCentre = m_vecMax - m_vecCentre;
}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rBV)
: BoundingBox(rBV)
, m_vecMin(rBV.m_vecMin)
, m_vecMax(rBV.m_vecMax)
, m_vecMaxFromCentre(rBV.m_vecMaxFromCentre)
{
}

void AxisAlignedBoundingBox::Translate(const Vector3& rVec)
{
	m_vecMin += rVec;
	m_vecMax += rVec;
	m_vecCentre += rVec;
}

void AxisAlignedBoundingBox::SetPosition(const Vector3& rPoint)
{
	Vector3 vecAdd = rPoint - m_vecCentre;
	Translate(vecAdd);
}

void AxisAlignedBoundingBox::SetOrientation(const Quaternion& rQuat, const Vector3* pCentre)
{
	// TODO: Reorient around pCentre
}


bool AxisAlignedBoundingBox::CheckPoint(const Vector3& rVec) const
{
	return (rVec.x >= m_vecMin.x && rVec.x <= m_vecMax.x &&
			rVec.y >= m_vecMin.y && rVec.y <= m_vecMax.y &&
			rVec.z >= m_vecMin.z && rVec.z <= m_vecMax.z);
}

CollisionData* AxisAlignedBoundingBox::CheckBoundingVolume(const BoundingVolume* pVolume) const
{
	assert(pVolume != NULL);

	// BV_HIERARCHICAL, BV_AABB, BV_OBB, BV_SPHERE
	switch (pVolume->GetType())
	{
	case BV_HIERARCHICAL:
		{
			// Let the HBV handle detection, then flip the result.
			CollisionData* pData = pVolume->CheckBoundingVolume(this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;
		}
	case BV_AABB:
		return CheckAABB(*((AxisAlignedBoundingBox*)pVolume));
	case BV_OBB:
		{
			return Intersection::BoxAndOBB((BoundingBox*)this,
				*((OrientedBoundingBox*)pVolume));
		}
		throw JFUtil::Exception("AxisAlignedBoundingBox", "CheckBoundingVolume",
			"Box/Box collision not implemented!");
		break;
	case BV_SPHERE:
		{
			const Sphere& rSphere = ((BoundingSphere*)pVolume)->GetSphere();
			CollisionData* pData =
				Intersection::SphereAndBox(rSphere, (BoundingBox*)this);
			// Remains NULL if it was NULL
			Intersection::FlipCollision(pData);
			return pData;
		}
	default:
		throw JFUtil::Exception("AxisAlignedBoundingBox", "CheckBoundingVolume",
			"Invalid bounding volume type?");
	}

}

Plane* AxisAlignedBoundingBox::GetPlaneWithNormal(Vector3 vecNormal) const
{
	// Note: for efficiency maybe just assert here.
	vecNormal.normalise();

	// Top
	if (vecNormal == Vector3::UNIT_Y)
		return new Plane(vecNormal, m_vecMax);
	// Bottom
	if (vecNormal == Vector3::NEGATIVE_UNIT_Y)
		return new Plane(vecNormal, m_vecMin);

	// Left
	if (vecNormal == Vector3::NEGATIVE_UNIT_X)
		return new Plane(vecNormal, m_vecMin);
	// Right
	if (vecNormal == Vector3::UNIT_Y)
		return new Plane(vecNormal, m_vecMax);

	// Front
	if (vecNormal == Vector3::NEGATIVE_UNIT_Z)
		return new Plane(vecNormal, m_vecMin);
	// Back
	if (vecNormal == Vector3::UNIT_Z)
		return new Plane(vecNormal, m_vecMax);


	return NULL;

}

Plane AxisAlignedBoundingBox::GetPlane(BOX_SIDE iSide) const
{
	switch (iSide)
	{
	case TOP: // Top
		return Plane( Vector3::UNIT_Y, m_vecMax );
	case BOTTOM:	// Bottom
		return Plane( Vector3::NEGATIVE_UNIT_Y, m_vecMin );
	case LEFT:	// Left
		return Plane( Vector3::NEGATIVE_UNIT_X, m_vecMin );
	case RIGHT:	// Right
		return Plane( Vector3::UNIT_X, m_vecMax );
	case FRONT:	// Front
		return Plane( Vector3::NEGATIVE_UNIT_Z, m_vecMin );
	case BACK:	// Back
		return Plane( Vector3::UNIT_Z, m_vecMax );
	default:
		throw JFUtil::Exception("AxisAlignedBoundingBox", "GetPlane",
			"Invalid parameter provided to AABB GetPlane (LAST?)");
	}

}


CollisionData* AxisAlignedBoundingBox::CheckAABB(const AxisAlignedBoundingBox& rBox) const
{
	// Vector from this box's centre to that box's centre.
	Vector3 vecBetween = rBox.m_vecCentre - m_vecCentre;

	if	 (m_vecMin.x <= rBox.m_vecMax.x && m_vecMax.x >= rBox.m_vecMin.x &&
		  m_vecMin.y <= rBox.m_vecMax.y && m_vecMax.y >= rBox.m_vecMin.y &&
		  m_vecMin.z <= rBox.m_vecMax.z && m_vecMax.z >= rBox.m_vecMin.z)
	{
		// Work out which direction the contact normal should be by deciding what surface
		// it's most likely to have hit.
		Vector3 vecNormal;
		Vector3 vecCentreToContact;
		Vector3 vecContactToOtherCentre;
		Real fXRatio = vecBetween.x / m_vecMaxFromCentre.x;
		Real fYRatio = vecBetween.y / m_vecMaxFromCentre.y;
		Real fZRatio = vecBetween.z / m_vecMaxFromCentre.z;
		Real fXRatioAbs = abs(fXRatio);
		Real fYRatioAbs = abs(fYRatio);
		Real fZRatioAbs = abs(fZRatio);

		// Work out which side of the box was hit.
		if (fXRatioAbs >= fYRatioAbs && fXRatioAbs >= fZRatioAbs)
		{
			if (fXRatio >= 0)
			{
				vecNormal = Vector3(1, 0, 0);
				vecCentreToContact = Vector3(m_vecMaxFromCentre.x, 0, 0);
				vecContactToOtherCentre = Vector3(rBox.m_vecMaxFromCentre.x, 0, 0);
			}
			else
			{
				vecNormal = Vector3(-1, 0, 0);
				vecCentreToContact = Vector3(-m_vecMaxFromCentre.x, 0, 0);
				vecContactToOtherCentre = Vector3(-rBox.m_vecMaxFromCentre.x, 0, 0);
			}
		}
		else if (fYRatioAbs >= fXRatioAbs && fYRatioAbs >= fZRatioAbs)
		{
			if (fYRatio >= 0)
			{
				vecNormal = Vector3(0, 1, 0);
				vecCentreToContact = Vector3(0, m_vecMaxFromCentre.y, 0);
				vecContactToOtherCentre = Vector3(0, rBox.m_vecMaxFromCentre.y, 0);
			}
			else
			{
				vecNormal = Vector3(0, -1, 0);
				vecCentreToContact = Vector3(0, -m_vecMaxFromCentre.y, 0);
				vecContactToOtherCentre = Vector3(0, -rBox.m_vecMaxFromCentre.y, 0);
			}
		}
		else if (fZRatioAbs >= fXRatioAbs && fZRatioAbs >= fYRatioAbs)
		{
			if (fZRatio >= 0)
			{
				vecNormal = Vector3(0, 0, 1);
				vecCentreToContact = Vector3(0, 0, m_vecMaxFromCentre.z);
				vecContactToOtherCentre = Vector3(0, 0, rBox.m_vecMaxFromCentre.z);
			}
			else
			{
				vecNormal = Vector3(0, 0, -1);
				vecCentreToContact = Vector3(0, 0, -m_vecMaxFromCentre.z);
				vecContactToOtherCentre = Vector3(0, 0, -rBox.m_vecMaxFromCentre.z);
			}
		}
		else
		{
			throw JFUtil::Exception("AxisAlignedBoundingBox", "CheckAABB",
				"Couldn't work out a contact normal.");
		}

		// Construct the data object with the point and normal.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = vecNormal;
		pData->m_vecPoint = m_vecCentre + vecCentreToContact;

		// Penetration - how much further the box has moved into this one beyond contact.
		Real fHitDist = (vecCentreToContact + vecContactToOtherCentre).length();
		Real fActualDist = vecBetween.dotProduct(vecNormal);

		assert (fHitDist >= fActualDist);
		pData->m_vecPenetration = -(vecNormal * (fHitDist-fActualDist));


		return pData;

	}
	else // Otherwise, no hit.
	{
		return NULL;
	}
}


/**
 * HierarchicalBV code
 */

HierarchicalBV::HierarchicalBV(BoundingVolume* pVolume)
: BoundingVolume(BV_HIERARCHICAL)
, m_pVolume(pVolume)
{
	// Doesn't make sense to have a node in the tree that is another hierarchical BV.
	assert(pVolume->GetType() != BV_HIERARCHICAL);
}

HierarchicalBV::~HierarchicalBV()
{
	for (BVVector::iterator it = m_vChildren.begin();
		 it != m_vChildren.end();)
	{
		delete *it;
		it = m_vChildren.erase(it);
	}
	delete m_pVolume;
}

const BoundingVolume* HierarchicalBV::GetNodeVolume() const
{
	return m_pVolume;
}

const BVVector& HierarchicalBV::GetChildren() const
{
	return m_vChildren;
}

const Vector3& HierarchicalBV::GetCentre() const
{
	assert(m_pVolume->GetType() != BV_HIERARCHICAL); // Infinite loop!
	return m_pVolume->GetCentre();
}

void HierarchicalBV::AddChild(BoundingVolume* pVol)
{
	// Memory for these is managed in this class!
	m_vChildren.push_back(pVol);
}


/**
 * BVFactory code
 */

const BVFactory& BVFactory::Get()
{
	if (!m_pInstance)
		m_pInstance = new BVFactory();
	return *m_pInstance;
}

BoundingVolume* BVFactory::MakeCopy(const BoundingVolume* pVol) const
{
	if (!pVol) return NULL; // Null parameter, might have been intentional...

	switch (pVol->GetType())
	{
	case BV_AABB :
		{
			AxisAlignedBoundingBox* pBox = (AxisAlignedBoundingBox*)pVol;
			return new AxisAlignedBoundingBox(*pBox);
			break;
		}
	case BV_HIERARCHICAL :
		{
			HierarchicalBV* pBV = (HierarchicalBV*)pVol;
			return new HierarchicalBV(*pBV);
			break;
		}
	case BV_OBB :
		{
			OrientedBoundingBox* pBV = (OrientedBoundingBox*)pVol;
			return new OrientedBoundingBox(*pBV);
			break;
		}
	case BV_SPHERE :
		{
			BoundingSphere* pBV = (BoundingSphere*)pVol;
			return new BoundingSphere(*pBV);
			break;
		}
	default:
		throw JFUtil::Exception("BVFactory", "MakeCopy",
			"Invalid BoundingVolume type given.");

	}
}

/**
 * Intersection code
 */

// Detecting collisions between spheres is trivial.
CollisionData* Intersection::SphereAndSphere(const Sphere &rOne, const Sphere &rTwo)
{
	Vector3 vecTwoToOne = (rOne.m_vecPos - rTwo.m_vecPos);
	Real fDistSq = vecTwoToOne.squaredLength();
	Real fRadSum = rOne.m_fRadius + rTwo.m_fRadius;
	if (fDistSq > fRadSum*fRadSum)
	{
		return NULL; // No hit
	}
	else
	{
		// Hit, so make the data object and return it.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = vecTwoToOne.normalisedCopy();
		pData->m_vecPoint = rTwo.m_vecPos + pData->m_vecNormal*rTwo.m_fRadius;
		// How much further sphere 1 has passed into sphere 2 than it should have.
		Real fPenetration = fRadSum - sqrtf(fDistSq);
		if(fPenetration < 0) fPenetration = 0;
		pData->m_vecPenetration = vecTwoToOne.normalisedCopy() * -fPenetration;
		return pData;
	}
}

// Detecting collisions between a sphere and a plane is pretty easy.
CollisionData* Intersection::SphereAndPlane(const Sphere &rSphere, const Plane &rPlane)
{
	//assert(rPlane.N.length() == 1);
	Real fDist = rPlane.getDistance(rSphere.m_vecPos);
	if (fDist > rSphere.m_fRadius)
	{
		return NULL; // No hit
	}
	else
	{
		// Hit, so make the data object and return it.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = rPlane.normal;
		pData->m_vecPoint = rSphere.m_vecPos - (rPlane.normal * fDist);
		Real fPenetration = rSphere.m_fRadius - fDist;
		if (fPenetration < 0) fPenetration = 0;
		pData->m_vecPenetration = rPlane.normal * -fPenetration;
		return pData;
	}
}

// This involves first checking against the quad's plane, then finding the contact point
// and seeing if that is within the 2D coordinates of the quad.
CollisionData* Intersection::SphereAndQuad(const Sphere& rSphere, const Quad& rQuad)
{
	// First, check against the plane.
	Plane p (rQuad.N, rQuad.TL);
	Real fPlDist = abs(p.getDistance(rSphere.m_vecPos));
	if (fPlDist > rSphere.m_fRadius) return NULL;

	// Second, get the point of impact and check if that's within the face.
	//assert(rQuad.N.length() == 1);
	Vector3 vecContactPoint = rSphere.m_vecPos - rQuad.N*fPlDist;

	// Normal vectors for each plane we're checking against - pointing inwards from each edge.
	//cout << "Normal: " << rQuad.N.GetString() << endl;
	Vector3 TN = (rQuad.TR - rQuad.TL).crossProduct(rQuad.N);
	Vector3 BN = (rQuad.BL - rQuad.BR).crossProduct(rQuad.N);
	Vector3 LN = (rQuad.TL - rQuad.BL).crossProduct(rQuad.N);
	Vector3 RN = (rQuad.BR - rQuad.TR).crossProduct(rQuad.N);
	// Planes to check against - if the point's positive for all of them, then it's within the face.
	Plane T (TN, rQuad.TL);
	Plane B (BN, rQuad.BR);
	Plane L (LN, rQuad.BL);
	Plane R (RN, rQuad.TR);

	// See if it's within the quad, while at the same time finding the closest plane and its distance
	// in case it's not within the quad but still hitting it.
	Real fSmallest = T.getDistance(rSphere.m_vecPos);
	Plane& rClosest = T;

	Real fBDist = B.getDistance(rSphere.m_vecPos);
	if (fSmallest > fBDist)
	{
		fSmallest = fBDist;
		rClosest = B;
	}

	Real fLDist = L.getDistance(rSphere.m_vecPos);
	if (fSmallest > fLDist)
	{
		fSmallest = fLDist;
		rClosest = L;
	}

	Real fRDist = R.getDistance(rSphere.m_vecPos);
	if (fSmallest > fRDist)
	{
		fSmallest = fRDist;
		rClosest = R;
	}

	if (fSmallest > 0)
	{
		// Landed within the quad - return impact point on plane.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = p.normal;
		pData->m_vecPoint = vecContactPoint;
		Real fPenetration = rSphere.m_fRadius - fPlDist;
		if (fPenetration < 0) fPenetration = 0;
		pData->m_vecPenetration = p.normal * -fPenetration;
		return pData;
	}

	// Didn't, but could still be hitting the edge.
	Vector3 vecDown = vecContactPoint - rSphere.m_vecPos;
	Vector3 vecIn = rClosest.normal * fSmallest;
	Vector3 vecToEdge = vecDown + vecIn;
	if (vecToEdge.length() > rSphere.m_fRadius)
	{
		return NULL; // No hit.
	}
	else
	{
		// Hit with the edge.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = -vecToEdge.normalisedCopy();
		pData->m_vecPoint = rSphere.m_vecPos + vecToEdge;
		Real fPenetration = rSphere.m_fRadius - vecToEdge.length();
		if (fPenetration < 0) fPenetration = 0;
		pData->m_vecPenetration = vecToEdge.normalisedCopy() * fPenetration;
		return pData;
	}
}

CollisionData* Intersection::SphereAndBox(const Sphere& rSphere, const BoundingBox* pBox)
{
	const Real& rRadius = rSphere.m_fRadius;
	const Vector3& rSphereC = rSphere.m_vecPos;

	// First, check against an AABB for the sphere. Only continue if they overlap.
	Vector3 vecSphereMin (rSphereC.x - rRadius,
						  rSphereC.y - rRadius,
						  rSphereC.z - rRadius);
	Vector3 vecSphereMax (rSphereC.x + rRadius,
						  rSphereC.y + rRadius,
						  rSphereC.z + rRadius);
	AxisAlignedBoundingBox oSphereBox (vecSphereMin, vecSphereMax);


	// Simple if it's hitting the AABB - return data for hitting a flat side.
	CollisionData* pBoxHit = pBox->CheckBoundingVolume(&oSphereBox);
	if (!pBoxHit) return NULL;

	// The simplest case is the ball hitting a flat face, for which we can use the
	// CollisionData made for the box collision.
	Vector3 vecToFace = -(pBoxHit->m_vecNormal)*rRadius;
	if (pBox->CheckPoint(rSphereC + vecToFace))
	{
		return pBoxHit;
	}

	// Otherwise, do it by edge.
	// Work out the normal of the closest edge (two closest planes)
	Real fMin;
	Plane* pNextPlane = NULL;
	Plane* pHitPlane = pBox->GetPlaneWithNormal(pBoxHit->m_vecNormal);
	assert(pHitPlane);
	for (int i = 0; i < (int)BoundingBox::LAST; ++i)
	{
		BoundingBox::BOX_SIDE iFace = (BoundingBox::BOX_SIDE)i;
		Plane* pPlane = new Plane(pBox->GetPlane(iFace));
		Real fDist = fabs( pPlane->getDistance(rSphereC) );
		if (!pNextPlane || fDist < fMin)
		{
			fMin = fDist;
			delete pNextPlane;
			pNextPlane = pPlane;
		}
	}

	assert(pNextPlane);
	Vector3 vecEdgeNormal = (pHitPlane->normal + pNextPlane->normal).normalisedCopy();
	// With the normal, we can find where the point must be hitting the normal. If it is, use the
	// results for the box collision but change the normal to the edge normal.
	Vector3 vecToEdge = -vecEdgeNormal*rRadius;
	if (!pBox->CheckPoint(rSphereC + vecToEdge)) return NULL;

	pBoxHit->m_vecNormal = vecEdgeNormal;

	delete pNextPlane;
	delete pHitPlane;
	return pBoxHit;
}


CollisionData* Intersection::BoxAndOBB(const BoundingBox* pBox, const OrientedBoundingBox& rOBB)
{
	// To check a box against another box:

	// For each edge of the Box, see if it intersects the surface.

	Vector3* pContact = NULL;

	// FBL - FTL
	int iFace = -1;
	pContact = LineAndOBB(pBox->GetFBL(), pBox->GetFTL(), rOBB, &iFace);
	// FTL - FTR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFTL(), pBox->GetFTR(), rOBB, &iFace);
	// FTR - FBR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFTR(), pBox->GetFBR(), rOBB, &iFace);
	// FBR - FBL
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFBR(), pBox->GetFBL(), rOBB, &iFace);

	// BBL - BTL
	if (!pContact) pContact =
		LineAndOBB(pBox->GetBBL(), pBox->GetBTL(), rOBB, &iFace);
	// BTL - BTR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetBTL(), pBox->GetBTR(), rOBB, &iFace);
	// BTR - BBR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetBTR(), pBox->GetBBR(), rOBB, &iFace);
	// BBR - BBL
	if (!pContact) pContact =
		LineAndOBB(pBox->GetBBR(), pBox->GetBBL(), rOBB, &iFace);

	// FBL - BBL
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFBL(), pBox->GetBBL(), rOBB, &iFace);
	// FTL - BTL
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFTL(), pBox->GetBTL(), rOBB, &iFace);
	// FTR - BTR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFTR(), pBox->GetBTR(), rOBB, &iFace);
	// FBR - BBR
	if (!pContact) pContact =
		LineAndOBB(pBox->GetFBR(), pBox->GetBBR(), rOBB, &iFace);

	// No intersections found
	if (!pContact) return NULL;

	assert(iFace >= 0 && iFace < BoundingBox::LAST);
	// Otherwise, work out the contact normal and penetration and return.
	Plane oFace = rOBB.GetPlane((BoundingBox::BOX_SIDE)iFace);

	CollisionData* pData = new CollisionData();
	pData->m_vecNormal = Vector3(oFace.normal);
	pData->m_vecPenetration = Vector3::ZERO; // NOTE: Put penetration here if it's needed.
	pData->m_vecPoint = Vector3(*pContact);
	delete pContact;

	return pData;

}

Vector3* Intersection::LineAndOBB(const Vector3& rLS, const Vector3& rLE,
		const OrientedBoundingBox& rOBB, int* pHitFace)
{
	// Check using Cohen-Sutherland whether the line might be intersecting the box.
	int iLSCode = 0;
	int iLECode = 0;
	// For each face, set the bit to 1 if the point is outside and 0 if inside
	vector<BoundingBox::BOX_SIDE> vFacesCrossed;
	vector<Real> vLSDists;
	//vector<Real> vLEDists;
	int iBit = 1;
	for (int i = 0; i < (int)BoundingBox::LAST; ++i)
	{
		BoundingBox::BOX_SIDE iFace = (BoundingBox::BOX_SIDE)i;
		Real fLSDist = rOBB.GetPlane(iFace).getDistance(rLS);
		Real fLEDist = rOBB.GetPlane(iFace).getDistance(rLE);
		vLSDists.push_back(fLSDist);
		//vLEDists.push_back(fLEDist);
		bool bLSOutside = fLSDist > 0;
		bool bLEOutside = fLEDist > 0;
		if (bLSOutside)	iLSCode += iBit;
		if (bLEOutside)	iLECode += iBit;
		if (bLSOutside != bLEOutside) vFacesCrossed.push_back(iFace);
		iBit *= 2;
	}

	if (vFacesCrossed.empty()) return NULL;
	if ((iLSCode & iLECode) != 0) return NULL; // Definitely outside the box.

	// NOTE: We just check the intersection point against the quad, so it's right on the edge; test if it works!
	assert(!vFacesCrossed.empty());
	Vector3 vec = rLE - rLS;
	for (vector<BoundingBox::BOX_SIDE>::const_iterator it = vFacesCrossed.begin();
		 it != vFacesCrossed.end(); ++it)
	{
		BoundingBox::BOX_SIDE iFace = *it;
		Real fLSDist = vLSDists[iFace];
		const Vector3& rNormal = rOBB.GetPlane(iFace).normal;
		//assert(rNormal.length() == 1); // NOTE: This may not work due to floating point error
		Real fTotalLength = vec.dotProduct(rNormal);
		Vector3* pContactPoint =
			new Vector3( rLS + vec*(fLSDist/fTotalLength) );

		// Return the contact point if it's on the surface of the box.
		if (rOBB.CheckPoint(*pContactPoint))
		{
			*pHitFace = iFace;
			return pContactPoint;
		}
	}

	return NULL;

}


CollisionData* Intersection::AABBAndQuad(const AxisAlignedBoundingBox& rBox, const Quad& rQuad)
{
	// To check a box against a quad: first check if it's even close enough to collide.
	// Then first check every line against the quad's plane.
	// if one crosses the plane, get the point at which it crosses. Check if that point
	// lies within the quad's coordinates.

	// Get the plane and check the distance is within the maximum.
	Plane p (rQuad.N, rQuad.TL);
	Real fPlDist = abs(p.getDistance(rBox.GetCentre()));
	bool bPositiveSide = fPlDist > 0;
	const Vector3& rMaxFromCentre = rBox.GetMaxFromCentre();
	if (fPlDist*fPlDist > rMaxFromCentre.squaredLength()) return NULL;

	// Check that each corner of the box is on the same side; if not, they're overlapping.
	const Vector3& rMax = rBox.GetMax();
	const Vector3& rMin = rBox.GetMin();
	Vector3 vecTTT (rMax);
	Vector3 vecTTB (rMax.x, rMax.y, rMin.z);
	Vector3 vecTBT (rMax.x, rMin.y, rMax.z);
	Vector3 vecTBB (rMax.x, rMin.y, rMin.z);
	Vector3 vecBTT (rMin.x, rMax.y, rMax.z);
	Vector3 vecBTB (rMin.x, rMax.y, rMin.z);
	Vector3 vecBBT (rMin.x, rMin.y, rMax.z);
	Vector3 vecBBB (rMin);

	// See if they're overlapping, and keep track of the one that penetrates the most.
	bool bOverlapping = false;
	Real fFarthest = 0;
	Vector3* pFarthestCorner = NULL;
	Real fTTTDist = p.getDistance(vecTTT);
	Real fTTBDist = p.getDistance(vecTTB);
	Real fTBTDist = p.getDistance(vecTBT);
	Real fTBBDist = p.getDistance(vecTBB);
	Real fBTTDist = p.getDistance(vecBTT);
	Real fBTBDist = p.getDistance(vecBTB);
	Real fBBTDist = p.getDistance(vecBBT);
	Real fBBBDist = p.getDistance(vecBBB);

	if (bPositiveSide != (fTTTDist > 0))
	{
		bOverlapping = true;
		if (abs(fTTTDist) >= abs(fFarthest))
		{
			fFarthest = fTTTDist;
			pFarthestCorner = &vecTTT;
		}
	}
	if (bPositiveSide != (p.getDistance(vecTTB) > 0))
	{
		bOverlapping = true;
		if (abs(fTTBDist) >= abs(fFarthest))
		{
			fFarthest = fTTBDist;
			pFarthestCorner = &vecTTB;
		}
	}
	if (bPositiveSide != (p.getDistance(vecTBT) > 0))
	{
		bOverlapping = true;
		if (abs(fTBTDist) >= abs(fFarthest))
		{
			fFarthest = fTBTDist;
			pFarthestCorner = &vecTBT;
		}
	}
	if (bPositiveSide != (p.getDistance(vecTBB) > 0))
	{
		bOverlapping = true;
		if (abs(fTBBDist) >= abs(fFarthest))
		{
			fFarthest = fTBBDist;
			pFarthestCorner = &vecTBB;
		}
	}
	if (bPositiveSide != (p.getDistance(vecBTT) > 0))
	{
		bOverlapping = true;
		if (abs(fBTTDist) >= abs(fFarthest))
		{
			fFarthest = fBTTDist;
			pFarthestCorner = &vecBTT;
		}
	}
	if (bPositiveSide != (p.getDistance(vecBTB) > 0))
	{
		bOverlapping = true;
		if (abs(fBTBDist) >= abs(fFarthest))
		{
			fFarthest = fBTBDist;
			pFarthestCorner = &vecBTB;
		}
	}
	if (bPositiveSide != (p.getDistance(vecBBT) > 0))
	{
		bOverlapping = true;
		if (abs(fBBTDist) >= abs(fFarthest))
		{
			fFarthest = fBBTDist;
			pFarthestCorner = &vecBBT;
		}
	}
	if (bPositiveSide != (p.getDistance(vecBBB) > 0))
	{
		bOverlapping = true;
		if (abs(fBBBDist) >= abs(fFarthest))
		{
			fFarthest = fBBBDist;
			pFarthestCorner = &vecBBB;
		}
	}

	if (!bOverlapping) return NULL; // All points on the same side of the plane.
	assert(pFarthestCorner);

	// Finally, get the point of impact and check if that's within the face.
	//assert(rQuad.N.length() == 1);
	Vector3 vecContactPoint = rBox.GetCentre() - rQuad.N*fPlDist;

	// Normal vectors for each plane we're checking against - pointing inwards from each edge.
	Vector3 TN = (rQuad.TR - rQuad.TL).crossProduct(rQuad.N);
	Vector3 BN = (rQuad.BL - rQuad.BR).crossProduct(rQuad.N);
	Vector3 LN = (rQuad.TL - rQuad.BL).crossProduct(rQuad.N);
	Vector3 RN = (rQuad.BR - rQuad.TR).crossProduct(rQuad.N);
	// Planes to check against - if the point's positive for all of them, then it's within the face.
	Plane T (TN, rQuad.TL);
	Plane B (BN, rQuad.BR);
	Plane L (LN, rQuad.BL);
	Plane R (RN, rQuad.TR);

	if (T.getDistance(vecContactPoint) < 0 ||
		B.getDistance(vecContactPoint) < 0 ||
		L.getDistance(vecContactPoint) < 0 ||
		R.getDistance(vecContactPoint) < 0)
	{
		return NULL;
	}
	else
	{
		// We're hitting! Take the contact normal using the normal
		// of the plane and the centre of the box.
		CollisionData* pData = new CollisionData();
		pData->m_vecNormal = bPositiveSide ? p.normal : -p.normal;
		pData->m_vecPoint = vecContactPoint;
		// Penetration - using the deepest penetrating corner.
		Vector3 vecFromFarthest = rBox.GetCentre() - *pFarthestCorner;
		Real fPenetration = vecFromFarthest.length() - fPlDist;
		if (fPenetration < 0) fPenetration = 0;
		pData->m_vecPenetration = vecFromFarthest.normalisedCopy() * fPenetration;

		return pData;
	}
}

void Intersection::FlipCollision(CollisionData* pData)
{
	if (!pData) return;
	pData->m_vecNormal = -pData->m_vecNormal;
	pData->m_vecPenetration = -pData->m_vecPenetration;
}
