#include <Physics/Particle.h>

using namespace JFPhysics;

/**
 * Particle code
 */

Particle::Particle(Vector3 vecPos, Vector3 vecVel, Vector3 vecAccel,
		 Real fInvMass, Real fDamping, Real fMaxSpeed,
		 Real fImmovableMass)
: m_vecPos(vecPos)
, m_vecVel(vecVel)
, m_vecAccel(vecAccel)
, m_fInvMass(fInvMass)
, m_fDamping(fDamping)
, m_fMaxSpeed(fMaxSpeed)
, m_vecForceAccum(0,0,0)
, m_fImmovableMass(fImmovableMass)
{

}

void Particle::AddForce(const Vector3& rForce)
{
	m_vecForceAccum += rForce;
}

void Particle::ZeroForces()
{
	m_vecForceAccum = Vector3::ZERO;
}

void Particle::AddContactNormal(const Vector3& rNormal)
{
	m_vContactNormals.push_back(rNormal);
}

Real Particle::GetInvMass() const
{
	return m_fInvMass;
}

Real Particle::GetMass() const
{
	assert(m_fInvMass >= 0);
	if (m_fInvMass == 0) return m_fImmovableMass;
	else return (Real)1/m_fInvMass;
}

void Particle::SetMass(Real fVal)
{
	assert(fVal > 0);
	m_fInvMass = 1/fVal;
}

void Particle::Update(unsigned long iTime)
{
	assert(iTime >= 0);

	// Add any accumulated force to our acceleration, then clear the accumulator.
	Vector3 vecResultingAccel = m_vecAccel;
	vecResultingAccel += m_vecForceAccum * m_fInvMass;


	// We do the integration here the more simple/efficient way.
	m_vecVel +=  vecResultingAccel * (Real)iTime;

	//if (m_vecForceAccum.length() != 0) cout << m_vecVel.length() << " > " << m_fMaxSpeed <<  "?" << endl;
	if (m_vecVel.length() > m_fMaxSpeed)
		m_vecVel = (m_vecVel.normalisedCopy() * m_fMaxSpeed);

	// If we have a contact normal, we're touching a surface. Don't move further in that direction.
	if (!m_vContactNormals.empty())
	{
		//cout << "size: " << m_vContactNormals.size() << endl;
		for (std::vector<Vector3>::iterator it = m_vContactNormals.begin();
			 it != m_vContactNormals.end(); ++it)
		{
			Vector3& rNormal = *it;
			assert(rNormal.length() == 1);

			//cout << "direction: " << v.GetString() << ", normal: " << rNormal.GetString() << endl;

			Real fNormComp = m_vecVel.dotProduct(-rNormal);
			//cout << "amount going through wall: " << fNormComp << endl;
			if (fNormComp > 0) m_vecVel += rNormal * fNormComp;

		}
	}

	// Apply damping.
	m_vecVel *= pow(m_fDamping, (int)iTime);
	// Update the position with the averaged motion vector.
	m_vecPos += m_vecVel * (Real)iTime;

	// Clear the collision normals and force accumulator.
	m_vContactNormals.clear();
	//m_oCollisionCont.Update();
	m_vecForceAccum = Vector3::ZERO;
}
