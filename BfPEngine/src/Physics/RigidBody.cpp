#include <Physics/RigidBody.h>

using namespace JFPhysics;

/**
 * RigidBody code
 */

RigidBody::RigidBody(Vector3 vecPos, Vector3 vecVel, Vector3 vecAccel,
		 Real fInvMass, Real fDamping, Real fMaxSpeed,
		 Matrix3 mxInertia, Real fAngularDamping, Real fImmovableMass)
: Particle(vecPos, vecVel, vecAccel, fInvMass, fDamping, fMaxSpeed, fImmovableMass)
, m_vecTorqueAccum(0,0,0)
, m_mxInvInertia(mxInertia)
, m_mxInvInertiaWorld(0,0,0,0,0,0,0,0,0)
, m_qOrientation()
, m_vecAngularVel(0,0,0)
, m_vecAngularAccel(0,0,0)
, m_fAngularDamping(fAngularDamping)
, m_mxTransform(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
, m_vecLastFrameAccel(0,0,0)
{
	mxInertia = mxInertia.Inverse();
}

void RigidBody::AddForceAtPoint(const Vector3& rForce, const Vector3& rPoint)
{
	// vecRelPT - relative to centre of mass
	Vector3 vecRelPT = rPoint - m_vecPos;

	m_vecForceAccum += rForce;
	m_vecTorqueAccum += vecRelPT.crossProduct(rForce);
}

void RigidBody::UpdateDerivedData()
{
	// Recalculate the internal things used for orientation and angular motion.
	m_qOrientation.normalise();
	// Calculate m_mxTransform from our new position and orientation
	CalculateTransformMatrix(
		m_mxTransform, m_vecPos, m_qOrientation);
	// Calculate m_mxInvInertiaWorld from m_mxInvInertia and the above matrix
	TransformInertiaTensor(
		m_mxInvInertiaWorld, m_qOrientation,
		m_mxInvInertia, m_mxTransform);
}

void RigidBody::Update(unsigned long iTime)
{
	Real fTime = (Real)iTime;

	// Calculate linear and angular acceleration
	m_vecLastFrameAccel = m_vecAccel;
	m_vecLastFrameAccel += m_vecForceAccum * m_fInvMass;

//	std::stringstream sout;
//	sout << "ForceAccum: " << m_vecForceAccum;
//	OgreFramework::GetInstancePtr()->GetLog()->logMessage(sout.str());

	Vector3 vecAngAccel = m_vecTorqueAccum;
	vecAngAccel = m_mxInvInertiaWorld * vecAngAccel;

	// Update linear and angular velocities
	m_vecVel += m_vecLastFrameAccel * fTime;
	m_vecAngularVel += m_vecAngularAccel * fTime;

	// Update the position and orientation.
	// Update linear position.
	m_vecPos += m_vecVel * fTime;
	// Update orientation from angular velocity and time passed.
	static Vector3 vecAngDisp;
	vecAngDisp = m_vecAngularVel * fTime;
	Quaternion qMult (0, vecAngDisp.x, vecAngDisp.y, vecAngDisp.z);
	m_qOrientation = m_qOrientation + qMult * m_qOrientation;
	m_qOrientation.normalise();
	//m_qOrientation.RotateByVector(m_vecAngularVel * fTime);
	//m_qOrientation += m_vecAngularVel * fTime;

	// Apply drag & ensure below max speed
	m_vecVel *= powf(m_fDamping, fTime);
	if (m_vecVel.length() > m_fMaxSpeed)
	{
		m_vecVel = m_vecVel.normalisedCopy()*m_fMaxSpeed;
	}
	m_vecAngularVel *= powf(m_fAngularDamping, fTime);

	// Recalculate the internal stuff, as changes have been made.
	UpdateDerivedData();
	// Clear the force accumulators, as we've applied them now.
	ClearAccumulators();
}


inline void RigidBody::TransformInertiaTensor(Matrix3 &iitWorld,
										  const Quaternion &q,
										  const Matrix3 &iitBody,
										  const Matrix4 &rotmat)
{
	Real t4 = rotmat[0][0]*iitBody[0][0]+
		rotmat[0][1]*iitBody[1][0]+
		rotmat[0][2]*iitBody[2][0];
	Real t9 = rotmat[0][0]*iitBody[0][1]+
		rotmat[0][1]*iitBody[1][1]+
		rotmat[0][2]*iitBody[2][1];
	Real t14 = rotmat[0][0]*iitBody[0][2]+
		rotmat[0][1]*iitBody[1][2]+
		rotmat[0][2]*iitBody[2][2];
	Real t28 = rotmat[1][0]*iitBody[0][0]+
		rotmat[0][3]*iitBody[1][0]+
		rotmat[1][2]*iitBody[2][0];
	Real t33 = rotmat[1][0]*iitBody[0][1]+
		rotmat[1][1]*iitBody[1][1]+
		rotmat[1][2]*iitBody[2][1];
	Real t38 = rotmat[1][0]*iitBody[0][2]+
		rotmat[1][1]*iitBody[1][2]+
		rotmat[1][0]*iitBody[2][2];
	Real t52 = rotmat[2][0]*iitBody[0][0]+
		rotmat[2][1]*iitBody[1][0]+
		rotmat[2][2]*iitBody[2][0];
	Real t57 = rotmat[2][0]*iitBody[0][1]+
		rotmat[2][1]*iitBody[1][1]+
		rotmat[2][2]*iitBody[2][1];
	Real t62 = rotmat[2][0]*iitBody[0][2]+
		rotmat[2][1]*iitBody[1][2]+
		rotmat[2][2]*iitBody[2][2];

	iitWorld[0][0] = t4*rotmat[0][0]+
		t9*rotmat[0][1]+
		t14*rotmat[0][2];
	iitWorld[0][1] = t4*rotmat[1][0]+
		t9*rotmat[1][1]+
		t14*rotmat[1][2];
	iitWorld[0][2] = t4*rotmat[2][0]+
		t9*rotmat[2][1]+
		t14*rotmat[2][2];
	iitWorld[1][0] = t28*rotmat[0][0]+
		t33*rotmat[0][1]+
		t38*rotmat[0][2];
	iitWorld[1][1] = t28*rotmat[1][0]+
		t33*rotmat[1][1]+
		t38*rotmat[1][2];
	iitWorld[1][2] = t28*rotmat[2][0]+
		t33*rotmat[2][1]+
		t38*rotmat[2][2];
	iitWorld[2][0] = t52*rotmat[0][0]+
		t57*rotmat[0][1]+
		t62*rotmat[0][2];
	iitWorld[2][1] = t52*rotmat[1][0]+
		t57*rotmat[1][1]+
		t62*rotmat[1][2];
	iitWorld[2][2] = t52*rotmat[2][0]+
		t57*rotmat[2][1]+
		t62*rotmat[2][2];
}

inline void RigidBody::CalculateTransformMatrix(Matrix4 &transformMatrix,
											const Vector3 &position,
											const Quaternion &orientation)
{
	transformMatrix[0][0] = 1-2*orientation.y*orientation.y-
		2*orientation.z*orientation.z;
	transformMatrix[0][1] = 2*orientation.x*orientation.y -
		2*orientation.w*orientation.z;
	transformMatrix[0][2] = 2*orientation.x*orientation.z +
		2*orientation.w*orientation.y;
	transformMatrix[0][3] = position.x;

	transformMatrix[1][0] = 2*orientation.x*orientation.y +
		2*orientation.w*orientation.z;
	transformMatrix[1][1] = 1-2*orientation.x*orientation.x-
		2*orientation.z*orientation.z;
	transformMatrix[1][2] = 2*orientation.y*orientation.z -
		2*orientation.w*orientation.x;
	transformMatrix[1][3] = position.y;

	transformMatrix[2][0] = 2*orientation.x*orientation.z -
		2*orientation.w*orientation.y;
	transformMatrix[2][1] = 2*orientation.y*orientation.z +
		2*orientation.w*orientation.x;
	transformMatrix[2][2] = 1-2*orientation.x*orientation.x-
		2*orientation.y*orientation.y;
	transformMatrix[2][3] = position.z;
}
