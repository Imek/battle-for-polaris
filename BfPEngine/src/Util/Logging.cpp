#include <Util/DebugPrint.h>

#include <sstream>

#ifdef _WIN32
#include <windows.h>
#endif // _WIN32

static const char* LOG_LEVEL_STR[] { "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL" };

void Logging::Log(LOG_LEVEL logLevel, const char* category, const char* message)
{
#ifndef NO_DEBUG_PRINT

	// TODO: More powerful, with format strings?
	std::stringstream sout;
	sout << "[" << LOG_LEVEL_STR[logLevel] << ":" << category << "] " << message;
#if defined _WIN32
	OutputDebugString(sout.str().c_str());
#else
	printf(sout.str());
#endif

# endif // NO_DEBUG_PRINT
}
