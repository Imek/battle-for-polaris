#include <Util/String.h>

bool JFUtil::StringEquals(const String& rOne, const String& rTwo)
{
	// Convert both strings to lower case and compare
	String strOne (rOne);
	String strTwo (rTwo);

	for (size_t i = 0; i < rOne.size(); ++i)
		strOne[i] = tolower( strOne[i] );
	for (size_t i = 0; i < rTwo.size(); ++i)
		strTwo[i] = tolower( strTwo[i] );

	return strOne == strTwo;
}

Vector3 JFUtil::VectorFromString(const String& rString)
{
	Vector3 o;

	// Format: Vector3(x, y, z)
	String strVal (rString);
	size_t iSize = strVal.size();
	assert(rString.substr(0, 1) == "(");
	assert(rString.substr(iSize-1) == ")");
	strVal = strVal.substr(1,iSize-2);
	// strVal should now be "x, y, z"
	std::stringstream ss;
	size_t iPos1 = strVal.find_first_of(",");
	size_t iPos2 = strVal.find_first_of(",", iPos1+1);
	assert(iPos1 != strVal.npos && iPos2 != strVal.npos);
//	cout << "X: " << strVal.substr(0, iPos1) << endl;
//	cout << "Y: " << strVal.substr(iPos1+1, iPos2-iPos1-1) << endl;
//	cout << "Z: " << strVal.substr(iPos2+1, iSize-iPos2-2) << endl;
	if ((ss << strVal.substr(0, iPos1)).fail())
		throw JFUtil::Exception("JFUtil", "VectorFromString",
			"Invalid X value given for vector", strVal);

	ss >> o.x;
	ss.str(""); ss.clear();
	if((ss << strVal.substr(iPos1+1, iPos2-iPos1-1)).fail())
		throw JFUtil::Exception("JFUtil", "VectorFromString",
			"Invalid Y value given for vector", strVal);

	ss >> o.y;
	ss.str(""); ss.clear();

	if((ss << strVal.substr(iPos2+1)).fail())
		throw JFUtil::Exception("JFUtil", "VectorFromString",
			"Invalid Z value given for vector", strVal);
	ss >> o.z;
	ss.str(""); ss.clear();

	return o;
}

String JFUtil::StringFromVector(const Vector3& rVector)
{
	std::stringstream sout;
	sout << "(" << rVector.x << "," << rVector.y << "," << rVector.z << ")";
	return sout.str();
}
