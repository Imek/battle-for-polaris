#include <Util/Events.h>

EventHandler* GlobalEvents::m_pInstance = NULL;

/**
 * EventHandler code
 */

EventHandler::EventHandler(const EventHandler& rHandler)
{
	throw JFUtil::Exception("EventHandler", "EventHandler",
		"Attempted to use copy constructor (use factory instead)");
}

EventHandler::~EventHandler()
{
	for (EventList::iterator it = m_lEvents.begin();
		 it != m_lEvents.end();)
	{
		delete *it;
		it = m_lEvents.erase(it);
	}
}

void EventHandler::PushEvent(Event* pEvent)
{
	if (!pEvent)
	{
		throw JFUtil::Exception("EventHandler", "PushEvent",
				"NULL pointer provided");
	}
	m_lEvents.push_back(pEvent);
}

Event* EventHandler::PopEvent()
{
	if (m_lEvents.empty()) return NULL;

	Event* pEv = m_lEvents.front();
	m_lEvents.pop_front();
	return pEv;
}

EventList EventHandler::PopEventsOfType(const string& rType)
{
	EventList vEventsToReturn;
	for (EventList::iterator it = m_lEvents.begin(); it != m_lEvents.end();)
	{
		Event* pEvent = *it;
		if (pEvent->GetType() == rType)
		{
			vEventsToReturn.push_back(pEvent);
			it = m_lEvents.erase(it);
		}
		else ++it;

	}
	return vEventsToReturn;
}

void EventHandler::Clear()
{
	for (EventList::iterator it = m_lEvents.begin();
		 it != m_lEvents.end();)
	{
		delete *it;
		it = m_lEvents.erase(it);
	}
}

bool EventHandler::Empty() const
{
	return m_lEvents.empty();
}

/**
 * EventSequence code
 */
EventSequence::EventSequence(const string& rType)
: Event("EV_SEQUENCE")
, m_strSeqType(rType)
{

}

EventSequence::~EventSequence()
{
	for (EventVector::iterator it = m_vEvents.begin(); it != m_vEvents.end();)
	{
		delete *it;
		it = m_vEvents.erase(it);
	}
}

EventSequence::EventSequence(const EventSequence& rObj)
: Event("EV_SEQUENCE")
, m_strSeqType(rObj.m_strSeqType)
{
	throw JFUtil::Exception("EventSequence", "EventSequence",
		"Whoops! Attempted to use copy constructor (use factory instead)");
}

void EventSequence::AddEvent(Event* pEvent)
{
	if (!pEvent)
		throw JFUtil::Exception("EventSequence", "AddEvent", "Null pointer given");
	m_vEvents.push_back(pEvent);
}

string EventSequence::GetString() const
{
	// Call GetString on each sub-event
	std::stringstream sout;
	sout << "EV_SEQUENCE( ";
	for (EventVector::const_iterator it = m_vEvents.begin(); it != m_vEvents.end(); ++it)
	{
		const Event* pEvent = *it;
		sout << endl << "	" << pEvent->GetString();
	}
	sout << " )";
	return sout.str();
}

void EventSequence::Resolve(unsigned long iTime)
{
	// Resolve the sub-events in order.
	for (EventVector::iterator it = m_vEvents.begin(); it != m_vEvents.end(); ++it)
		(*it)->Resolve(iTime);
}
