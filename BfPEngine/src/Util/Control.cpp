#include <Util/Control.h>

Real QuadraticEquationLargestRoot(Real A, Real B, Real C)
{
  return ( B + std::sqrt(B*B-4*A*C) ) / (2*A);
}

Vector3 CalcInterceptVector(const Vector3& rOrigin, Real fProjVel, const Vector3& rTarget, const Vector3& rTargVel)
{
  Real a = fProjVel*fProjVel - rTargVel.dotProduct(rTargVel);
  Real b = -2*rTargVel.dotProduct(rTarget-rOrigin);
  Real c = -(rTarget-rOrigin).dotProduct(rTarget-rOrigin);

  return rTarget + QuadraticEquationLargestRoot(a,b,c) * rTargVel;
}

Vector3 UtilControl::LeadTarget(
	const Vector3& rOrigin, const Vector3& rMyVel, Real fAccel, Real fProjVel, unsigned iLifetime,
	const Vector3& rTarget, const Vector3& rTargVel, bool* pInRange, Vector3* pHitPos)
{
	// TODO: Incorporate acceleration into this (currently only homing missile
	// has accel, so it's not so important)

	// Calculate the hit position (intercept)
	Vector3 vTargVel = rTargVel - rMyVel; // Their motion relative to us
	*pHitPos = CalcInterceptVector(rOrigin, fProjVel, rTarget, vTargVel);

	// Calculate the aim vector from that
	Vector3 v = *pHitPos-rOrigin;

	// v is now origin to intercept point, so see if it's in-range.
	if ( v.length() <= (fProjVel*iLifetime) )
	{
		*pInRange = true;
	}
	// if not in-range, there is no hit position.
	else
	{
		*pHitPos = Vector3::ZERO;
		*pInRange = true;
	}

	// Either way finish calculating the aim vector and return.
	v = fProjVel / std::sqrt(v.dotProduct(v))*v;
	v.normalise(); // TODO: Optimise?
	return v;
}

bool UtilControl::CollisionCourse(
	const Vector3& rPositionA, const Vector3& rVelA, const Vector3& rAccelA, const Real& rRadiusA,
	const Vector3& rPositionB, const Vector3& rVelB, const Vector3& rAccelB, const Real& rRadiusB,
	unsigned iMaxTime, const Real& rMaxDist, unsigned iTimeStep)
{
	assert(iTimeStep > 0);
	Real fCollideDistSq = rRadiusA + rRadiusB;
	fCollideDistSq *= fCollideDistSq;

	// Basic case is that the provided values already collide!
	Real fThisDistSq = rPositionA.squaredDistance(rPositionB);
	if (fThisDistSq <= fCollideDistSq) return true;
	// Check rMaxDist to see if we should proceed
	if (fThisDistSq > rMaxDist * rMaxDist) return false;

	static Vector3 vecPositionA;
	static Vector3 vecVelA;
	static Vector3 vecPositionB;
	static Vector3 vecVelB;

	vecPositionA = rPositionA;
	vecVelA = rVelA;
	vecPositionB = rPositionB;
	vecVelB = rVelB;

	Real fTimeStep = (Real)iTimeStep;
	Real fMaxTime = (Real)iMaxTime;

	for (Real fTime = fTimeStep; fTime <= fMaxTime; fTime += fTimeStep)
	{
		// Here We don't bother with 0.5*a*t ^2; we assume the time step is small enough to render it insignificant
		vecVelA += rAccelA*fTimeStep;
		vecVelB += rAccelB*fTimeStep;
		vecPositionA += vecVelA*fTimeStep;
		vecPositionB += vecVelB*fTimeStep;
		// TODO: For the moment we assume that the time step is small enough so that we don't need to do any "in-between-steps" checks
		fThisDistSq = vecPositionA.squaredDistance(vecPositionB);
		if (fThisDistSq <= fCollideDistSq)
		{
			return true;
		}
	}

	return false;

}

Vector2 UtilControl::GetNeededTurn(
	const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetDir)
{
	if (rMyFacing == rTargetDir) return Vector2::ZERO; // No turning needed

	// Get the axis and work out its components in terms of yaw pitch and roll
	// NOTE: Maybe more efficient to keep track of the right vector rather than recalculate it
	Vector3 vecRight = rMyFacing.crossProduct(rMyNormal);
	//Vector3 vecAxis = rMyFacing.crossProduct(rTargetDir);
	//vecAxis.normalise();

	// Do it using planes. (TODO: This isn't ideal, especially if we end up using the Real values in the returned vector2 for anything other than positive/negative check)
	Plane oPitch (Vector3::ZERO-rMyNormal, Vector3::ZERO);

	Real fPitch = oPitch.getDistance(rTargetDir);

	Plane oYawRoll (vecRight, Vector3::ZERO);
	Real fYaw = oYawRoll.getDistance(rTargetDir);

	// TODO: If we're facing in the wrong direction entirely add 1 to yaw so that we don't stop facing the wrong way
	//if (fPitch == 0 && fYaw == 0)
//	{
//		Plane oFace (rMyFacing, Vector3::ZERO);
//		if (oFace.getDistance(rTargetDir) < 0)
//			return Vector2::UNIT_SCALE;
//	}

	return Vector2(fYaw, fPitch);
}

Real UtilControl::GetNeededRoll(
	const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetUp)
{
	if (rMyNormal == rTargetUp) return 0; // No rolling needed

	// Get the axis and work out its components in terms of yaw pitch and roll
	// NOTE: Maybe more efficient to keep track of the right vector rather than recalculate it
	Vector3 vecRight = rMyFacing.crossProduct(rMyNormal);
	//Vector3 vecTargetRight = rMyFacing.crossProduct(rTargetUp);
//	Vector3 vecAxis = rMyFacing.crossProduct(pTargetDir);
//	vecAxis.normalise();

	// Do it using planes.
	Plane oYawRoll (vecRight, Vector3::ZERO);
	return -oYawRoll.getDistance(rTargetUp);
}


void UtilControl::GetTurnAngle(
	const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetDir,
	Degree& rToYaw, Degree& rToPitch)
{
	// NOTE: Assume normalised vectors provided
	// Work out the axes about which to rotate
	Vector3 vecAxis = rMyFacing.crossProduct(rTargetDir);
	Vector3 vecRight = rMyFacing.crossProduct(rMyNormal);
	Real fYawComponent = -vecAxis.dotProduct(rMyNormal);
	Real fPitchComponent = vecAxis.dotProduct(vecRight);

	// TODO: proper angles
//	rToYaw = Degree(fYawComponent);
//	rToPitch = Degree(fPitchComponent);

	// Convert the component into an angle.
	// NOTE: This method isn't very accurate, but we only care about accuracy for small angles to do smooth turning.
	Plane oFace (rMyFacing, Vector3::ZERO);
	bool bBehind = (oFace.getDistance(rTargetDir) < 0);
	//bool bFullTurn = false;

	assert(fYawComponent >= -1 &&fYawComponent <= 1); // Should be true if vectors were normalised
	rToYaw = Degree( Ogre::Math::ASin(fYawComponent) );
	assert(fPitchComponent >= -1 && fPitchComponent <= 1); // Should be true if vectors were normalised
	rToPitch = Degree( Ogre::Math::ASin(fPitchComponent) );

	// In the case that it's behind us, we use yaw to turn to face the right direction.
	if (bBehind) rToYaw = Degree(rToYaw.valueDegrees() > 0 ? 180 : -180) - rToYaw;
	//if (bBehind) rToPitch = Degree(rToPitch.valueDegrees() > 0 ? 180 : -180) - rToPitch;

}

Degree UtilControl::GetRollAngle(
	const Vector3& rMyFacing, const Vector3& rMyNormal, const Vector3& rTargetUp)
{
	Degree degToRoll (0);
	if (rMyNormal == rTargetUp) return degToRoll; // No rolling needed

	// Get the axis and work out its components in terms of yaw pitch and roll
	// NOTE: Maybe more efficient to keep track of the right vector rather than recalculate it
	Vector3 vecRight = rMyFacing.crossProduct(rMyNormal);

	// Do it using planes, then use inverse sine to convert them to angles.
	Plane oFace (rMyNormal, Vector3::ZERO);
	Plane oYawRoll (vecRight, Vector3::ZERO);
	//bool bUpsideDown = (oFace.getDistance(rTargetUp) < 0);

	Real fRoll = oYawRoll.getDistance(rTargetUp);
	assert(fRoll >= -1 && fRoll <= 1); // Should be true if vectors were normalised
	degToRoll = Degree(fRoll);
	//if (bUpsideDown) degToRoll = Degree(degToRoll.valueDegrees() > 0 ? 180 : -180) - degToRoll;

	return degToRoll;
}
