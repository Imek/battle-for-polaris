#include <Util/Maths.h>

Real JFUtil::RandomBetween(const Real& rOne, const Real& rTwo)
{
	if (rOne == rTwo) return rOne;

	Real fRand = (Real)rand() / RAND_MAX;
	return rOne + ( fRand * (rTwo - rOne) );
}

Vector3 JFUtil::RandomVectorFromCentre(const Vector3& rCentre, Degree degMinAngle, Degree degMaxAngle)
{
	static Quaternion qRot;
	static Vector3 vecCalc;

	// Randomise the new vector based on the amounts
	Degree degRand1 = Degree( JFUtil::RandomBetween(0.0f, 360.0f) );
	Degree degRand2 = Degree( JFUtil::RandomBetween(degMinAngle.valueDegrees(), degMaxAngle.valueDegrees()) );

	if (rand() % 2 == 0) // 50% chance to make it negative
		degRand2 = -degRand2;
	Vector3 vecReturn (rCentre);
	vecCalc = vecReturn.perpendicular(); // m_vecCalc used as axis

	qRot.FromAngleAxis(degRand1, vecReturn);
	vecCalc = qRot * vecCalc;
	qRot.FromAngleAxis(degRand2, vecCalc);
	vecReturn = qRot * vecReturn;
	return vecReturn;
}
